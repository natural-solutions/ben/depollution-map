const { generate } = require("@graphql-codegen/cli");

generate(
  {
    schema: [
      {
        "http://localhost:8888/v1/graphql": {
          headers: {
            "x-hasura-role": "admin",
            "x-hasura-admin-secret": process.env.HASURA_GRAPHQL_ADMIN_SECRET,
          },
        },
      },
    ],
    watch: false,
    overwrite: true,
    generates: {
      [process.cwd() + "/src/graphql/types.ts"]: {
        plugins: ["typescript"],
      },
      [process.cwd() + "/src/graphql/schema.json"]: {
        plugins: ["introspection"],
      },
    },
  },
  true
);
