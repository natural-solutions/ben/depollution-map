import * as Minio from "minio";
import { IMAGOR_PRESETS } from "./media";
import { NextRequest } from "next/server";

export const getMinioClient = (env: typeof process.env) => {
  return new Minio.Client({
    endPoint: "minio",
    port: 8888,
    useSSL: false,
    accessKey: env.MINIO_ROOT_USER!,
    secretKey: env.MINIO_ROOT_PASSWORD!,
  });
};

export const getMinioClientDistant = (env: typeof process.env) => {
  const useSSL = env.MINIO_USE_SSL === "1";

  return new Minio.Client({
    endPoint: env.DOMAIN!,
    port: useSSL ? 443 : 8888,
    useSSL,
    accessKey: env.MINIO_ROOT_USER!,
    secretKey: env.MINIO_ROOT_PASSWORD!,
    region: env.MINIO_REGION,
  });
};

export const removeFromMinioAndImagor = async (
  env: typeof process.env,
  objectName: string
) => {
  const minio = getMinioClient(env);
  const bucketLoader = env.MINIO_BUCKET_LOADER!;
  const bucketStorage = env.MINIO_BUCKET_STORAGE!;
  const bucketResult = env.MINIO_BUCKET_RESULT!;
  try {
    await minio.removeObject(bucketLoader, objectName);
  } catch (error) {
    console.log("removeObject ERR", bucketLoader, objectName, error);
  }
  try {
    const minio = getMinioClient(env);
    await minio.removeObject(bucketStorage, objectName);
  } catch (error) {
    console.log("removeObject ERR", bucketStorage, objectName);
  }
  for (const key in IMAGOR_PRESETS) {
    try {
      await minio.removeObject(bucketResult, `${key}/${objectName}`);
    } catch (error) {
      console.log("removeObject ERR", bucketResult, `${key}/${objectName}`);
    }
  }
};

export const handleUpload = async (options: {
  req: NextRequest;
  folder: string;
}) => {
  try {
    const searchParams = options.req.nextUrl.searchParams;
    const filename = searchParams.get("filename") as string;
    const regex = /^[a-zA-Z0-9\.\-_]*$/;
    if (!filename || !regex.test(filename)) {
      return Response.json(
        {
          error:
            "Filename is required and can only contains numbers, letters without accent, and .-_",
        },
        {
          status: 400,
        }
      );
    }
    const minioClient = getMinioClientDistant(process.env);

    const bucketName = process.env.MINIO_BUCKET_LOADER!;
    const objectName = `${options.folder}/${filename}`;

    try {
      await minioClient.statObject(bucketName, objectName);
      return Response.json(`File exists: ${filename}`, {
        status: 409,
      });
    } catch (error) {}
    const url = await minioClient.presignedPutObject(
      bucketName,
      objectName,
      60
    );
    return Response.json(url);
  } catch (error) {
    console.log("presigned_put error", error);
    try {
      return Response.json((error as any).toString(), { status: 500 });
    } catch (error) {}
  }

  return Response.json({}, { status: 500 });
};
