import axios, { AxiosError } from "axios";
import { signIn } from "next-auth/react";
import { useNotify } from "react-admin";

export const useApi = () => {
  const notify = useNotify();

  const api = axios.create({
    baseURL: `/api`,
    headers: {
      "Content-Type": "application/json",
    },
  });

  api.interceptors.response.use(
    (response) => {
      //loader.hide();
      return response;
    },
    async (error) => {
      //loader.hide();
      //const config = error.config;
      if ([401, 403].includes(error.response?.status)) {
        return signIn("keycloak");
      }

      return Promise.reject(error);
    }
  );

  const postGraphql = async <
    T = {
      data?: {
        [key: string]: any;
      };
    }
  >(data: {
    operationName?: string;
    query: string;
    variables?: any;
  }) => {
    return await api.post<T>("/graphql", data);
  };

  const notifyApiError = (error: { response: any }) => {
    const data: any = error.response?.data;
    if (!data) {
      try {
        notify(error.toString(), {
          type: "error",
        });
      } catch (error2) {
        notify(String(error), {
          type: "error",
        });
      }
    }
    const errors = (
      (error.response?.data as any)?.errors as {
        message: string;
      }[]
    )
      ?.map((err) => err.message)
      ?.join("\n");

    const msg = errors || data?.errorMessage || data.error || data;

    notify(typeof msg == "string" ? msg : error.toString(), {
      type: "error",
    });
  };

  return { api, postGraphql, notifyApiError };
};
