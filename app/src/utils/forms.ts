import { AdminCleanupFormFormData } from "@/components/admin/cleanup_form/AdminCleanupFormForm";
import { FormCategory } from "@/components/admin/cleanup_form/AdminCleanupFormFormRubbishes";
import { CleanupFormPatch } from "@/graphql/queries";
import {
  Cleanup_Form_Rubbish,
  Cleanup_Form_Rubbish_Detail,
} from "@/graphql/types";
import { isNil, pick } from "lodash";
import { isEmpty, regex } from "react-admin";

export type Step = {
  name: string;
  title: string;
  isActivatable: boolean;
};

export const isPositiveInteger = regex(
  /^[0-9]*$/,
  "Doit-être un entier positif"
);

export const regexValidation =
  (pattern: RegExp, message: string) => (value: any, values?: any[]) =>
    !isEmpty(value) && typeof value === "string" && !pattern.test(value)
      ? message
      : undefined;

export const formatRubbishes = (
  formCategories: FormCategory[],
  formRubbishes: Partial<Cleanup_Form_Rubbish>[],
  formRubbishFields: (keyof Cleanup_Form_Rubbish_Detail)[],
  formRubbishDetails: Partial<Cleanup_Form_Rubbish_Detail>[]
) => {
  if (!formCategories) {
    return;
  }
  formCategories.forEach((formCategory) => {
    formCategory.rubbishes.forEach((formCategoryRubbish) => {
      let formRubbish = formRubbishes.find(
        (formRubbish) =>
          formRubbish.rubbish_id === formCategoryRubbish.formRubbish.rubbish_id
      );
      if (!formRubbish) {
        formRubbish = formCategoryRubbish.formRubbish;
        formRubbishes.push(formRubbish);
      } else {
        formRubbishFields.forEach((field) => {
          formRubbish![field] = formCategoryRubbish.formRubbish[field];
        });
      }

      [
        ["brands", "brand_id"],
        ["areas", "cleanup_area_id"],
      ].forEach((names) => {
        const listName = names[0] as keyof typeof formCategoryRubbish;
        const fieldName = names[1] as keyof Cleanup_Form_Rubbish_Detail;
        (
          formCategoryRubbish[listName] as Cleanup_Form_Rubbish_Detail[]
        )?.forEach((detail) => {
          const existing = formRubbishDetails.find(
            (rubbishDetail) =>
              rubbishDetail[fieldName] == detail[fieldName] &&
              rubbishDetail.rubbish_id == formRubbish!.rubbish_id
          );
          if (!existing) {
            detail.rubbish_id = formRubbish!.rubbish_id!;
            formRubbishDetails.push(detail);
          } else {
            formRubbishFields.forEach((field) => {
              existing[field] = detail[field];
            });
          }
        });
      });
    });
  });
};

export const getCleanupFormPatch = (formData: AdminCleanupFormFormData) => {
  const patch = {
    ...pick(formData, [
      "performed_at",
      "volunteers_number",
      "external_volunteers_number",
      "cleanup_duration",
      "comment",
      "cleanup_id",
      "wind_force",
      "rain_force",
      "status",
      "rubbish_type_picked_id",
      "cleanup_form_dois",
    ]),
    cleanup_form_pois: Object.keys(formData.poiAreas)
      .map((areaId) =>
        formData.poiAreas[areaId].map((poi) => ({
          ...poi,
          cleanup_area_id: areaId,
        }))
      )
      .flat(1),
  } as CleanupFormPatch;

  patch.cleanup_form_rubbishes = [];
  patch.cleanup_form_rubbish_details = [];
  formatRubbishes(
    formData.qtyCategories,
    patch.cleanup_form_rubbishes,
    ["quantity"],
    patch.cleanup_form_rubbish_details
  );
  formatRubbishes(
    formData.wt_volCategories,
    patch.cleanup_form_rubbishes,
    ["weight", "volume"],
    patch.cleanup_form_rubbish_details
  );

  patch.cleanup_form_rubbishes = getFilteredFormRubbishes(
    patch.cleanup_form_rubbishes
  ) as Cleanup_Form_Rubbish[];

  patch.cleanup_form_rubbish_details = (
    getFilteredFormRubbishes(
      patch.cleanup_form_rubbish_details
    ) as Cleanup_Form_Rubbish_Detail[]
  ).map((detail) => {
    return pick(detail, [
      "id",
      "cleanup_form_id",
      "rubbish_id",
      "quantity",
      "weight",
      "volume",
      "cleanup_area_id",
      "brand_id",
    ]);
  });

  //TODO ? And the same for details ?
  /* const cleanup_form_rubbishes_delete_ids = !formData.cleanup_form_rubbishes
      ? undefined
      : record?.cleanup_form_rubbishes
          ?.filter((existing) => {
            return !formData.cleanup_form_rubbishes?.some(
              (item) => item.id === existing.id
            );
          })
          .map((item) => item.id); */

  return patch;
};

export const getFilteredFormRubbishes = (
  rows: Cleanup_Form_Rubbish[] | Partial<Cleanup_Form_Rubbish_Detail>[]
) => {
  return rows.filter((row) => {
    return (
      ["quantity", "weight", "volume"] as (keyof (typeof rows)[number])[]
    ).some((field) => !isNil(row[field]));
  });
};
