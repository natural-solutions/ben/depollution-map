import { ThemeOptions } from "@mui/material";
import { defaultTheme } from "react-admin";

export const HEADER_HEIGHT_SX = 64;
export const HEADER_HEIGHT_PX = `${HEADER_HEIGHT_SX}px`;

export const themeOptions: ThemeOptions = {
  ...defaultTheme.components,
  palette: {
    primary: {
      main: "#3EA0EA",
      contrastText: "#FFF",
    },
    secondary: {
      main: "#EFF7FD",
      contrastText: "#1976d2",
    },
    text: {
      primary: "#14363F",
    },
    white: {
      main: "#FFF",
      light: "#FFF",
      dark: "#FFF",
      contrastText: "#FFF",
    },
  },
  typography: {
    fontFamily: "Montserrat",
    h2: {
      fontSize: "20px",
      fontWeight: 600,
      fontFamily: "Montserrat",
      "@media (max-width:899px)": {
        fontSize: "16px",
        fontWeight: 500,
        fontFamily: "Poppins",
      },
    },
    h3: {
      fontSize: "20px",
      fontWeight: 600,
      fontFamily: "Montserrat",
    },
    h4: {
      fontFamily: "Montserrat",
      fontSize: "16px",
      fontWeight: 600,
      "@media (min-width:900px)": {
        fontSize: "20px",
      },
    },
    h5: {
      fontFamily: "Poppins",
      fontSize: "16px",
      fontWeight: 500,
      color: "#14363F",
    },
    h6: {
      fontFamily: "Poppins",
      fontSize: "14px",
      fontWeight: 600,
    },
    caption: {
      fontFamily: "Montserrat",
      fontSize: "14px",
      fontWeight: 400,
    },
  },
  components: {
    // Name of the component
    MuiBadge: {
      styleOverrides: {
        badge: {
          backgroundColor: "#14363F99",
          color: "white",
          padding: "8px",
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        // Name of the slot
        root: {
          // Some CSS
          textTransform: "none" as const,
          fontFamily: "Montserrat",
          letterSpacing: "0.4px",
          minWidth: "auto",
        },
        contained: {
          padding: "8px 22px",
        },
        sizeLarge: {
          fontSize: "14px",
          fontWeight: 600,
        },
      },
    },
    MuiButtonGroup: {
      styleOverrides: {
        root: {
          background: "#FFFFFF",
        },
      },
    },
    MuiFormControl: {
      defaultProps: {
        variant: "filled",
      },
    },
    MuiTextField: {
      defaultProps: {
        variant: "filled",
      },
      styleOverrides: {
        root: {
          marginTop: "8px",
          marginBottom: "4px",
        },
      },
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          background: "#FAFAFA",
          "&:hover:not(.Mui-disabled, .Mui-error)": {
            background: "#FFFFFF",
          },
          "&.Mui-focused": {
            background: "#FAFAFA",
          },
        },
      },
    },
    MuiAutocomplete: {
      defaultProps: {
        componentsProps: {
          paper: {
            square: true,
          },
        },
      },
    },
    MuiSelect: {
      defaultProps: {
        MenuProps: {
          sx: {
            "& .MuiMenu-paper": {
              borderRadius: "3px",
            },
          },
        },
      },
    },
    MuiTab: {
      styleOverrides: {
        root: {
          fontFamily: "Montserrat",
          fontSize: "inherit",
          fontWeight: 500,
          letterSpacing: "0.4px",
          textTransform: "none" as const,
        },
      },
    },
  },
};
