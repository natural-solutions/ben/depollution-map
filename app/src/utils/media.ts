export enum IMAGOR_PRESET_NAME {
  "50x50" = "50x50",
  "80x80" = "80x80",
  "200x200" = "200x200",
  "200x280" = "200x280",
  "300x225" = "300x225",
  "500x375" = "500x375",
  "800x600" = "800x600",
}

export const IMAGOR_PRESETS = {
  [IMAGOR_PRESET_NAME["50x50"]]: "50x50",
  [IMAGOR_PRESET_NAME["80x80"]]: "80x80",
  [IMAGOR_PRESET_NAME["200x200"]]: "200x200",
  [IMAGOR_PRESET_NAME["200x280"]]: "200x280",
  [IMAGOR_PRESET_NAME["300x225"]]: "300x225",
  [IMAGOR_PRESET_NAME["500x375"]]: "500x375",
  [IMAGOR_PRESET_NAME["800x600"]]: "800x600",
} as const;

export type AppFile = File & {
  slugified: string;
};

export type ReactAdminImageInputReturn = {
  title: string;
  src: string;
  rawFile: AppFile;
};

export const slugify = (str: string) => {
  str = str.replace(/^\s+|\s+$/g, "");
  str = str.toLowerCase();
  const from =
    "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/,:;";
  const to =
    "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa-----";
  for (let i = 0; i < from.length; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }
  str = str.toLowerCase();
  str = str
    .replace(/\s+/g, "-")
    .replace(/[^a-z0-9\.\-_]/g, "")
    .replace(/-+/g, "-");

  return str;
};

export const getDirPath = (
  fileType: "cleanups" | "campaigns" | "partners" | "poi_types" | "resources",
  itemId: string
) => {
  return `${fileType}/${itemId}`;
};

export const getFilePath = (
  fileType: "cleanups" | "campaigns" | "partners" | "poi_types" | "resources",
  itemId: string,
  filename: string
) => {
  return `${getDirPath(fileType, itemId)}/${filename}`;
};

export const getTransformationURL = (
  fileType: "cleanups" | "campaigns" | "partners" | "poi_types",
  itemId: string,
  filename: string,
  preset: IMAGOR_PRESET_NAME
) => {
  return `/media/unsafe/${preset}/${getFilePath(fileType, itemId, filename)}`;
};

export const getFileSrcURL = (
  fileType: "resources",
  itemId: string,
  filename: string
) => {
  return `/upload/${getFilePath(fileType, itemId, filename)}`;
};

const getFileUploadURL = async (
  fileType: "cleanups" | "campaigns" | "partners" | "poi_types" | "resources",
  itemId: string,
  file: AppFile
) => {
  const presignedResp = await fetch(
    `/api/${fileType}/${itemId}/files/presigned_put?filename=${file.slugified}`
  );
  if (presignedResp.status != 200) {
    return Promise.reject(presignedResp);
  }

  return (await presignedResp.json()) as string;
};

export const uploadFile = async (
  fileType: "cleanups" | "campaigns" | "partners" | "poi_types" | "resources",
  itemId: string,
  file: AppFile
) => {
  try {
    const url = await getFileUploadURL(fileType, itemId, file);
    try {
      return await processUploadFile(file, url);
    } catch (error) {
      return Promise.reject(error);
    }
  } catch (error) {
    const msg = await (error as Response).json();
    return Promise.reject(msg);
  }
};

const processUploadFile = async (file: AppFile, uploadUrl: string) => {
  try {
    const uploadResp = await fetch(uploadUrl, {
      method: "PUT",
      body: file,
    });
    if (uploadResp.status != 200) {
      const xmlStr = await uploadResp.text();
      const xml = new window.DOMParser().parseFromString(xmlStr, "text/xml");
      const messages = xml.getElementsByTagName("Message");

      return Promise.reject(messages[0].textContent);
    }
    return uploadResp;
  } catch (error) {
    return Promise.reject(error);
  }
};
