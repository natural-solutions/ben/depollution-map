import { useSession } from "next-auth/react";
import {
  USER_ROLE,
  hasRole,
  getIsAdmin,
  canCreateCharacterization,
  getHasCampaignRole,
  getIsAdminInCampaign,
  getCanCreateCleanup,
  getCanEditCleanup,
  getCanSubmitCleanupForm,
  getCanPublishCleanupForm,
  getCanCreateCleanupForm,
  getCanEditCleanupForm,
  getCanShowEditCleanupForm,
} from "./user";
import { AppSession } from "./session";
import { Cleanup, Cleanup_Form, Cleanup_View } from "@/graphql/types";

export const useSessionContext = () => {
  const session = useSession();
  const sessionCtx: AppSession | null = session.data as any;

  return {
    session: sessionCtx,
    getIsAdmin: () => getIsAdmin(sessionCtx?.userView),
    hasRole: (role?: USER_ROLE | USER_ROLE[]) => {
      if (!role) {
        return session.status == "authenticated";
      }
      return hasRole(sessionCtx?.userView, role);
    },
    getHasCampaignRole: (campaignId: string, role: USER_ROLE) =>
      getHasCampaignRole(sessionCtx?.userView, campaignId, role),
    getIsAdminInCampaign: () => getIsAdminInCampaign(sessionCtx?.userView),
    getCanCreateCleanup: () => getCanCreateCleanup(sessionCtx?.userView),
    getCanEditCleanup: (cleanup: Cleanup) =>
      getCanEditCleanup(sessionCtx?.userView, cleanup),
    canCreateCharacterization: (
      options: Parameters<typeof canCreateCharacterization>[0]
    ) => canCreateCharacterization(options),
    getCanCreateCleanupForm: (cleanup?: Cleanup|Cleanup_View) =>
      getCanCreateCleanupForm(sessionCtx?.userView, cleanup),
    getCanEditCleanupForm: (cleanupForm?: Cleanup_Form) =>
      getCanEditCleanupForm(sessionCtx?.userView, cleanupForm),
    getCanShowEditCleanupForm: (cleanupForm?: Cleanup_Form) =>
      getCanShowEditCleanupForm(sessionCtx?.userView, cleanupForm),
    getCanSubmitCleanupForm: (cleanupForm: Cleanup_Form) =>
      getCanSubmitCleanupForm(sessionCtx?.userView, cleanupForm),
    getCanPublishCleanupForm: (cleanupForm: Cleanup_Form) =>
      getCanPublishCleanupForm(sessionCtx?.userView, cleanupForm),
  };
};
