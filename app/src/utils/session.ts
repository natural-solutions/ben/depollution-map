import { User_View } from "@/graphql/types";
import { USER_ROLE } from "./user";
import { authOptions } from "./auth";
import { getServerSession } from "next-auth";

export type AppSession = {
  user: {
    id: string;
    name: string;
    email: string;
  };
  userView: User_View;
  roles: USER_ROLE[];
  accessToken: string;
  error: string;
};

export const getValidServerSession = async (options?: {
  allowAnonymous?: boolean;
  allowedRoles?: USER_ROLE[];
}) => {
  const session = (await getServerSession(authOptions)) as AppSession;
  if (!session) {
    return options?.allowAnonymous
      ? {}
      : { errorResp: Response.json({}, { status: 401 }) };
  }

  const allowedRoles = options?.allowedRoles;
  if (
    allowedRoles &&
    !allowedRoles?.includes(session.userView.role_name as USER_ROLE)
  ) {
    return {
      errorResp: Response.json(
        { user_role: session.userView.role_name, allowedRoles },
        { status: 403 }
      ),
    };
  }

  return { session };
};
