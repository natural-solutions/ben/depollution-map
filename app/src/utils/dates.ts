export const printShortDate = (dateString: string): string => {
  return new Date(dateString).toLocaleDateString("fr-FR");
};

export const printTime = (dateString: string): string => {
  return new Date(dateString).toLocaleTimeString("fr-FR", {
    hour: "2-digit",
    minute: "2-digit",
  });
};
