import area from "@turf/area";
import length from "@turf/length";

export const MAP_LAYERS = {
  plan: {
    label: "OSM",
    type: "raster",
    tiles: ["https://a.tile.openstreetmap.org/{z}/{x}/{y}.png"],
    tileSize: 256,
    attribution: "&copy; OpenStreetMap Contributors",
    maxzoom: 19,
  },
  sat: {
    label: "Satellite",
    type: "raster",
    tiles: [
      "https://data.geopf.fr/wmts?&REQUEST=GetTile&SERVICE=WMTS" +
        "&VERSION=1.0.0&TILEMATRIXSET=PM&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&STYLE=normal&FORMAT=image/jpeg" +
        "&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}",
    ],
    tileSize: 256,
    attribution: "",
    maxzoom: 19,
  },
};

export type MapLayerName = keyof typeof MAP_LAYERS;

export const MAP_STYLE = {
  version: 8,
  sources: {
    bg_src: MAP_LAYERS.plan,
  },
  layers: [
    {
      id: "bg_layer",
      type: "raster",
      source: "bg_src", // This must match the source key above
    },
  ],
  glyphs: "https://fonts.openmaptiles.org/{fontstack}/{range}.pbf",
};

export const MAP_INIT_POINT = {
  longitude: 2.689888499324752,
  latitude: 46.820813531004205,
  zoom: 5.35,
};

type GetPolygonAreaFeatureParam = typeof area extends (param: infer P) => void
  ? P
  : never;
export const getPolygonArea = (feature: GetPolygonAreaFeatureParam) => {
  return Math.round(area(feature));
};

type GetLineStringLengthParam = any;

export const getLineStringLength = (feature: GetLineStringLengthParam): number => {
  return Math.round(length(feature.geometry, { units: "meters" }));
};

export const getGeomMeasurement = (feature: GeoJSON.Feature<GeoJSON.Geometry>): any => {

  if (feature.geometry.type === "Polygon") {
    return getPolygonArea(feature);
  }
  if (feature.geometry.type === "LineString") {
    return getLineStringLength(feature);
  }
};

export const computeTotalAreaLinear = (
  areas: any[]
): { totalArea: number; totalLinear: number } => {
  let totalArea = 0;
  let totalLinear = 0;

  areas.forEach((area: any) => {
    if (area.geom.geometry.type === "Polygon") {
      totalArea += area.size || 0;
    } 
    if (area.geom.geometry.type === "LineString") {
      totalLinear += area.size || 0;
    }
  });

  return { totalArea, totalLinear };
};

export const CLEANUP_AREA_COLORS = {
  red: "#FF0000",
  green: "#00FF00",
  blue: "#0000FF",
  yellow: "#FFFF00",
  magenta: "#FF00FF",
  cyan: "#00FFFF",
  purple: "#800080",
  orange: "#FFA500",
  pink: "#FF007F",
  brown: "#800000",
  lime: "#00FF00",
  teal: "#008080",
  indigo: "#4B0082",
  silver: "#C0C0C0",
};

export const CLEANUP_AREA_COLORS_TRANSLATIONS = {
  red: "rouge",
  green: "vert",
  blue: "bleu",
  yellow: "jaune",
  magenta: "magenta",
  cyan: "cyan",
  purple: "violet",
  orange: "orange",
  pink: "rose",
  brown: "marron",
  lime: "citron vert",
  teal: "bleu-vert",
  indigo: "indigo",
  silver: "argent",
};

export type CleanupAreaColorName = keyof typeof CLEANUP_AREA_COLORS;
