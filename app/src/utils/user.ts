import { dangerousPostAsAdmin } from "@/graphql/client";
import { GET_USER } from "@/graphql/queries";
import {
  Cleanup,
  Cleanup_Form,
  Cleanup_View,
  Maybe,
  User_View,
} from "@/graphql/types";
import { DefaultSession } from "next-auth";
import { CLEANUP_FORM_STATUS, getCleanupForm } from "./cleanups";

export enum USER_ROLE {
  "ADMIN" = "admin",
  "EDITOR" = "editor",
  "PUBLIC" = "public",
}
// Put all above roles in the following array, in "herarchical" order
export const USER_AVAILABLES_ROLES = [
  USER_ROLE.ADMIN,
  USER_ROLE.EDITOR,
  USER_ROLE.PUBLIC,
] as const;

const ROLE_DISPLAY_NAMES = {
  [USER_ROLE.ADMIN]: "Chargé de projet",
  [USER_ROLE.EDITOR]: "Bénévole",
  [USER_ROLE.PUBLIC]: "Public", 
};

export const USER_ROLE_CHOICES = [USER_ROLE.ADMIN, USER_ROLE.EDITOR].map(
  (role) => ({
    id: role,
    name: ROLE_DISPLAY_NAMES[role],
  })
);

export type SessionUser = DefaultSession["user"] & {
  id: string;
};

export const getIsInRole = (roleA: any, roleB: any) => {
  return (
    USER_AVAILABLES_ROLES.indexOf(roleA) <= USER_AVAILABLES_ROLES.indexOf(roleB)
  );
};

export const getIsAdmin = (user?: User_View) => {
  return hasRole(user, USER_ROLE.ADMIN);
};

export const hasRole = (
  user: User_View | undefined,
  role: USER_ROLE | USER_ROLE[]
) => {
  const userRole = user?.role_name as USER_ROLE;
  if (!userRole) {
    return false;
  }
  if (userRole == USER_ROLE.ADMIN) {
    return true;
  }
  if (typeof role === "string") {
    role = [role];
  }

  const requestedRoles = role as USER_ROLE[];

  return requestedRoles.some((requestedRole) => {
    return getIsInRole(userRole, requestedRole);
  });
};

export const getHasCampaignRole = (
  user: User_View | undefined,
  campaignId: string,
  role: USER_ROLE
) => {
  if (!user) {
    return false;
  }
  const isUserAdmin = getIsAdmin(user);
  if (isUserAdmin) {
    return true;
  }

  return user.campaign_users.some((campaign_user) => {
    if (campaign_user.campaign_id != campaignId) {
      return false;
    }
    return getIsInRole(campaign_user.role, role);
  });
};

export const getIsAdminInCampaign = (user?: User_View) => {
  if (getIsAdmin(user)) {
    return true;
  }
  return user?.campaign_users.some(
    (campaign_user) => campaign_user.role === USER_ROLE.ADMIN
  );
};

export const getCanCreateCleanup = (user?: User_View) => {
  if (getIsAdmin(user)) {
    return true;
  }
  return user?.campaign_users.some((campaign_user) =>
    [USER_ROLE.ADMIN, USER_ROLE.EDITOR].includes(
      campaign_user.role as USER_ROLE
    )
  );
};

export const getHasCleanupRole = (
  user: User_View | undefined,
  cleanup: Maybe<Cleanup> | Maybe<Cleanup_View> | undefined,
  role: USER_ROLE
) => {
  if (getIsAdmin(user)) {
    return true;
  }
  return cleanup?.cleanup_campaigns?.some(({ campaign_id }) =>
    user?.campaign_users.find(
      ({ campaign_id: user_campaign_id, role: userRole }) =>
        user_campaign_id === campaign_id && getIsInRole(userRole, role)
    )
  );
};

export const getCanCreateCleanupForm = (
  user?: User_View,
  cleanup?: Cleanup | Cleanup_View
) => {
  if (!user || !cleanup) {
    return false;
  }

  return getHasCleanupRole(user, cleanup, USER_ROLE.EDITOR);
};

export const getCanShowEditCleanupForm = (
  user?: User_View,
  cleanupForm?: Cleanup_Form
) => {
  if (!user || !cleanupForm) {
    return false;
  }
  const isCleanupAdmin = getHasCleanupRole(
    user,
    cleanupForm.cleanup,
    USER_ROLE.ADMIN
  );
  if (isCleanupAdmin) {
    return true;
  }

  return cleanupForm.owner_id == user?.id;
};

export const getCanEditCleanupForm = (
  user?: User_View,
  cleanupForm?: Cleanup_Form
) => {
  if (!user || !cleanupForm) {
    return false;
  }
  if (getIsAdmin(user)) {
    return true;
  }
  const isCleanupAdmin = getHasCleanupRole(
    user,
    cleanupForm.cleanup,
    USER_ROLE.ADMIN
  );

  return (
    (isCleanupAdmin || cleanupForm.owner_id == user?.id) &&
    cleanupForm.status != CLEANUP_FORM_STATUS.PUBLISHED
  );
};

export const getCanSubmitCleanupForm = (
  user?: User_View,
  cleanupForm?: Cleanup_Form
) => {
  if (!user || !cleanupForm) {
    return false;
  }
  const isCleanupAdmin = getHasCleanupRole(
    user,
    cleanupForm.cleanup,
    USER_ROLE.ADMIN
  );
  return (
    isCleanupAdmin ||
    (cleanupForm.owner_id === user?.id &&
      cleanupForm?.status != CLEANUP_FORM_STATUS.PUBLISHED)
  );
};

export const getCanPublishCleanupForm = (
  user?: User_View,
  cleanupForm?: Cleanup_Form
) => {
  if (!user || !cleanupForm) {
    return false;
  }
  if (getIsAdmin(user)) {
    return true;
  }
  return getHasCleanupRole(user, cleanupForm.cleanup, USER_ROLE.ADMIN);
};

export const getCanEditCleanup = (
  user: User_View | undefined,
  cleanup: Cleanup
) => {
  if (!user) {
    return false;
  }
  const isCleanupAdmin = getHasCleanupRole(user, cleanup, USER_ROLE.ADMIN);

  return (
    isCleanupAdmin ||
    (cleanup.owner_id === user?.id &&
      getCleanupForm(cleanup)?.status != CLEANUP_FORM_STATUS.PUBLISHED)
  );
};

export const canCreateCharacterization = (options: {
  cleanup: Cleanup;
  user?: User_View;
}) => {
  const { user: user, cleanup } = options;
  if (!user) {
    return false;
  }
  const isUserAdmin = getIsAdmin(user);
  if (isUserAdmin) {
    return true;
  }

  if (cleanup.owner_id === user.id) {
    return true;
  }

  return cleanup.cleanup_campaigns.some((cleanup_campaign) =>
    getHasCampaignRole(user, cleanup_campaign.campaign_id, USER_ROLE.EDITOR)
  );
};

export const getUserFromDB = async (id?: string) => {
  const resp = await dangerousPostAsAdmin({
    query: GET_USER,
    variables: {
      id,
    },
  });

  return resp.data?.user_view?.[0] as User_View;
};
