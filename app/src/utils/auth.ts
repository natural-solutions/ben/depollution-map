import { Session } from "next-auth";
import { JWT } from "next-auth/jwt";
import KeycloakProvider from "next-auth/providers/keycloak";
import { getUserFromDB } from "./user";

type refreshTokenParams = {
  client_id: string;
  client_secret: string;
  grant_type: string;
  refresh_token: string;
};

export const getClientToken = async () => {
  const urlencoded = new URLSearchParams();
  urlencoded.append("grant_type", "client_credentials");
  urlencoded.append("client_id", process.env.KEYCLOAK_CLIENT_ID!);
  urlencoded.append("client_secret", process.env.KEYCLOAK_CLIENT_SECRET!);

  const resp = await fetch(
    `${process.env.KEYCLOAK_ISSUER}/protocol/openid-connect/token`,
    // Why not ?
    //`http://keycloak:8080/auth/realms/${process.env.KEYCLOAK_REALM_ID}/protocol/openid-connect/token`,
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
      body: urlencoded,
    }
  );
  const token = await resp.json();

  return {
    ...token,
    bearer: `Bearer ${token.access_token}`,
  };
};

async function refreshAccessToken(token: JWT) {
  try {
    const params: refreshTokenParams = {
      client_id: process.env.KEYCLOAK_CLIENT_ID!,
      client_secret: process.env.KEYCLOAK_CLIENT_SECRET!,
      grant_type: "refresh_token",
      refresh_token: (token.refreshToken as string) ?? "",
    };

    const url = `${process.env.KEYCLOAK_ISSUER}/protocol/openid-connect/token`;
    const formBody = [];

    for (const property in params) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(
        params[property as keyof refreshTokenParams]
      );

      formBody.push(encodedKey + "=" + encodedValue);
    }

    const body = formBody.join("&");
    const response = await fetch(url, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
      body: body,
    });

    const refreshedTokens = await response.json();

    if (!response.ok) {
      throw refreshedTokens;
    }

    return {
      ...token,
      accessToken: refreshedTokens.access_token,
      accessTokenExpires: (refreshedTokens?.expires_in
        ? Date.now() + refreshedTokens?.expires_in * 1000
        : 0) as number,
      refreshToken: refreshedTokens.refresh_token ?? token.refreshToken,
    };
  } catch (e) {
    const errorToken: JWT = {
      ...token,
      error: "RefreshAccessTokenError",
    };

    return errorToken;
  }
}

const decode = function (token: string) {
  return JSON.parse(Buffer.from(token.split(".")[1], "base64").toString());
};

export const authOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.KEYCLOAK_CLIENT_ID!,
      clientSecret: process.env.KEYCLOAK_CLIENT_SECRET!,
      issuer: process.env.KEYCLOAK_ISSUER,
    }),
  ],
  events: {
    async signOut(options: any) {
      const { token } = options;
      const logOutUrl = new URL(
        `${process.env.KEYCLOAK_ISSUER}/protocol/openid-connect/logout`
      );
      logOutUrl.searchParams.set("id_token_hint", token.idToken);

      await fetch(logOutUrl);
    },
  },
  callbacks: {
    async jwt(params: any) {
      const { token, profile, user, account } = params;
      if (account && user && profile) {
        const decoded = decode(account.access_token);
        const newToken: JWT = {
          user,
          idToken: account.id_token,
          accessToken: account.access_token,
          accessTokenExpires: account?.expires_at
            ? account?.expires_at * 1000
            : Date.now(),
          refreshToken: account.refresh_token,
          roles: decoded?.realm_access?.roles || [],
        };
        return newToken;
      }

      if (Date.now() < token.accessTokenExpires) {
        return token;
      }

      return await refreshAccessToken(token);
    },
    async session(params: any) {
      const { session, token } = params;

      session.user = token.user as Session["user"];
      session.userView = await getUserFromDB(token.user.id);
      session.roles = token.roles ?? [];
      session.accessToken = token.accessToken;
      session.error = token.error;

      return session;
    },
  },
};
