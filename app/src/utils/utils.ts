export const JsonParseSafe = (value: any) => {
  try {
    return JSON.parse(value);
  } catch (error) {}
};

export const getHundredthFormat = (value: number) => {
  return Math.round(value * 100) / 100;
};

export const convertTimeToDecimal = (time: string): number => {
  let [hours, minutes] = time.split(":").map(Number);
  const convert = hours + minutes / 60;
  return Math.round(convert * 2) / 2;
};