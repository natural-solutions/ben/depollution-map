import { format, isWithinInterval, isToday, isBefore, isAfter } from "date-fns";
import { Cleanup, Cleanup_View } from "@/graphql/types";
import { isNil, round } from "lodash";

export type CleanupFilters = {
  campaigns: string[];
  cities: string[];
  departements: string[];
  regions: string[];
  characterizationLevel: number[];
  partners: string[];
  from_date: string;
  to_date: string;
};

export enum CLEANUP_FORM_STATUS {
  "DRAFT" = "draft",
  "TOCHECK" = "tocheck",
  "PUBLISHED" = "published",
}

export const DEFAULT_FILTER_VALUES = {
  campaigns: [],
  cities: [],
  departements: [],
  regions: [],
  characterizationLevel: [],
  partners: [],
  from_date: "",
  to_date: "",
};

export const getCleanupForm = (cleanup?: Cleanup_View | Cleanup) => {
  return cleanup?.cleanup_forms?.[0];
};

export const getCleanupsFormStatus = (cleanup: Cleanup_View | Cleanup) => {
  const today = new Date();

  if (!cleanup) {
    return null;
  }

  const cleanupDate = new Date(cleanup.start_at);

  if (isBefore(cleanupDate, today) && cleanup.cleanup_forms?.length > 0) {
    return "completed";
  }
  if (
    (isToday(cleanupDate) || isAfter(cleanupDate, today)) &&
    cleanup.cleanup_forms?.length === 0
  ) {
    return "upcoming";
  }
  if (
    (isAfter(cleanupDate, today) && cleanup.cleanup_forms?.length > 0) ||
    ((isToday(cleanupDate) || isBefore(cleanupDate, today)) &&
      cleanup.cleanup_forms?.length === 0)
  ) {
    return "missing";
  }
};

export const getCleanupWeightFormat = (cleanup: Cleanup | Cleanup_View) => {
  let value = cleanup.total_weight;
  const unit = value >= 1000 ? "T" : "Kg";
  value = unit === "T" ? round(value, 2) : Math.round(value);

  return {
    unit,
    value,
    label: `${value} ${unit}`,
  };
};

export const getWeightFormat = (weight: number) => {
  let value = weight;
  const unit = value >= 1000 ? "T" : "Kg";
  value =
    unit === "T"
      ? round(value / 1000, value / 1000 < 10 ? 2 : 1)
      : Math.round(value * 10) / 10;

  return {
    unit,
    unitLabel: unit == "T" ? "tonnes" : "kilos",
    value,
    label: `${printNumber(value)} ${unit}`,
  };
};

export const printNumber = (nb: number) => {
  if (!nb) {
    return "";
  }
  return nb.toLocaleString("fr-FR");
};

interface FilterProps {
  cleanups: Cleanup_View[];
  searchFilters: string | null;
}

const getCleanupsPerDate = (
  cleanupsToFilter: Cleanup_View[],
  startDate: string,
  endDate: string
) => {
  return cleanupsToFilter.filter((cleanup) => {
    const start_at = format(new Date(cleanup.start_at), "yyyy-MM-dd");
    const matchStartDate = startDate ? start_at >= startDate : false;
    const matchEndDate =
      cleanup.start_at && endDate ? start_at <= endDate : false;

    if (startDate && endDate) {
      try {
        const matchDates = isWithinInterval(new Date(start_at), {
          start: new Date(startDate),
          end: new Date(endDate),
        });

        return matchDates;
      } catch (e) {
        console.log(e);
      }
    } else if (startDate || endDate) {
      return matchStartDate || matchEndDate;
    }
    return cleanupsToFilter;
  });
};

const getCleanupsPerCampaigns = (
  cleanupsToFilter: Cleanup_View[],
  campaignsSelected?: string[]
) => {
  if (campaignsSelected && campaignsSelected.length > 0) {
    return cleanupsToFilter.filter((cleanup) => {
      return cleanup.cleanup_campaigns.some((cleanup_campaign) =>
        campaignsSelected.includes(cleanup_campaign.campaign.id)
      );
    });
  }
  return cleanupsToFilter;
};

const getCleanupsPerPartners = (
  cleanupsToFilter: Cleanup_View[],
  partnersSelected?: string[]
) => {
  if (partnersSelected?.length) {
    return cleanupsToFilter.filter((cleanup) => {
      return cleanup.cleanup_partners.some((cleanup_partner) =>
        partnersSelected.includes(
          cleanup_partner.partner_id || cleanup_partner.partner?.id
        )
      );
    });
  }
  return cleanupsToFilter;
};

const getCleanupsPerLocations = (
  cleanupsToFilter: Cleanup_View[],
  cleanupCitiesSelected?: string[],
  cleanupDepartementsSelected?: string[],
  cleanupRegionsSelected?: string[]
) => {
  return cleanupsToFilter.filter((cleanup) => {
    if (
      !cleanupCitiesSelected?.length &&
      !cleanupDepartementsSelected?.length &&
      !cleanupRegionsSelected?.length
    ) {
      return cleanupsToFilter;
    }
    const locationMatches = [
      cleanup.city && cleanupCitiesSelected?.includes(cleanup.city.id),
      cleanup.city?.departement &&
        cleanupDepartementsSelected?.includes(cleanup.city.departement.id),
      cleanup.city &&
        cleanup.city.departement?.region?.slug &&
        cleanupRegionsSelected?.includes(cleanup.city.departement.region.slug),
    ];

    return locationMatches.some((match) => match);
  });
};

const getCleanupsCharacterizationLevel = (
  cleanupsToFilter: Cleanup_View[],
  characterizationLevelsSelected?: number[]
) => {
  if (characterizationLevelsSelected?.length) {
    return cleanupsToFilter.filter((cleanup) => {
      return !isNil(cleanup.cleanup_type?.characterization_level_id)
        ? characterizationLevelsSelected.includes(
            cleanup.cleanup_type.characterization_level_id
          )
        : false;
    });
  }
  return cleanupsToFilter;
};

export const filter = async (
  cleanups: Cleanup_View[],
  filters: CleanupFilters
) => {
  if (cleanups.length === 0) {
    return [];
  }
  let cleanupsToFilter: Cleanup_View[] = getCleanupsPerDate(
    cleanups,
    filters.from_date,
    filters.to_date
  );
  cleanupsToFilter = getCleanupsPerCampaigns(
    cleanupsToFilter,
    filters.campaigns
  );
  cleanupsToFilter = getCleanupsPerLocations(
    cleanupsToFilter,
    filters.cities,
    filters.departements,
    filters.regions
  );
  cleanupsToFilter = getCleanupsCharacterizationLevel(
    cleanupsToFilter,
    filters.characterizationLevel
  );
  cleanupsToFilter = getCleanupsPerPartners(cleanupsToFilter, filters.partners);
  return cleanupsToFilter;
};

export const getFilteredFromURLCleanups = async (props: FilterProps) => {
  const { cleanups, searchFilters } = props;

  if (!searchFilters) {
    return cleanups;
  }
  const filterFromURL = getFiltersFromURL(searchFilters);
  return await filter(cleanups, filterFromURL);
};

export const getFiltersFromURL = (searchFilters: string | null) => {
  if (!searchFilters) {
    return;
  }
  return JSON.parse(searchFilters);
};

export const setFilterFromURL = (
  searchFilters: string | null,
  setCampaignsSelected: Function,
  setCleanupCitiesSelected: Function,
  setCleanupDepartementsSelected: Function,
  setCleanupRegionsSelected: Function,
  setCharacterizationLevelsSelected: Function,
  setPartnersSelected: Function,
  setStartDate: Function,
  setEndDate: Function
) => {
  try {
    if (!searchFilters) {
      return -1;
    }
    const filters = JSON.parse(searchFilters);
    setStartDate(filters.start || "");
    setEndDate(filters.end || "");
    setCampaignsSelected(filters.campaigns || []);
    setCleanupCitiesSelected(filters.cities || []);
    setCleanupDepartementsSelected(filters.departements || []);
    setCleanupRegionsSelected(filters.regions || []);
    setCharacterizationLevelsSelected(filters.cleanup_type || []);
    setPartnersSelected(filters.partners || []);
  } catch (e) {
    return -1;
  }
};
