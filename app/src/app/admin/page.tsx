"use client";

import dynamic from "next/dynamic";

const RAdmin = dynamic(() => import("@/components/r_admin/RAindex"), {
  ssr: false,
});

export default function Admin() {
  return <RAdmin context="admin" />;
}
