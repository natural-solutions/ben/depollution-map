"use client";

import dynamic from "next/dynamic";

const RAdmin = dynamic(() => import("@/components/r_admin/RAindex"), {
  ssr: false,
});

export default function FormsPage() {
  return <RAdmin context="forms" />;
}
