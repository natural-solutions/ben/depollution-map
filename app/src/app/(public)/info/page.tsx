import { Box, Button, Container, Typography } from "@mui/material";

export default function Info() {
  return (
    <Box
      sx={{
        flex: 1,
        height: "100%",
        overflowY: "auto",
      }}
    >
      <Container sx={{ py: 5 }} maxWidth="md">
        <Typography
          variant="h2"
          sx={{
            mb: 3,
            textTransform: "uppercase",
          }}
        >
          Informations
        </Typography>
        <Typography component="div">
          <p>
            Opération soutenue par l’État dans le cadre du Fonds d’Intervention
            Maritime (FIM) opéré par la Direction Générale des Affaires
            Maritimes, de la Pêche et de l’Aquaculture (DGAMPA)
          </p>
          <img src="/assets/logo-secmb.jpg" />
          <p>
            WINGS-MAP est un outil conçu afin de référencer les données
            collectées par l’association Wings of the Ocean. Ces données de
            caractérisation sont publiques et ont pour objectif d’alimenter la
            science participative afin d’étudier la pollution générée par les
            déchets sauvages. Les missions de l’association, implantés sur
            différents territoires, enregistrent leurs données sur la plateforme
            pour mettre en lumière les déchets les plus récurrents retrouvés
            dans la nature.
          </p>
          <p>
            Fondée en 2018, Wings of the Ocean est une association française qui
            agit en faveur de la protection de l’océan en luttant contre les
            déchets sauvages, notamment plastique.
            <br />
            <br />
            Association de terrain avant tout, Wings of the Ocean oeuvre sur
            l’ensemble du territoire à travers ses antennes locales et ses
            missions. L’association opère sur la côte méditerranéenne et
            atlantique, autour de l’étang de Berre, de l’étang de Thau et du
            Bassin d’Arcachon en mission fixe. Sur l’eau, 2 navires composent la
            flotte de l’association : le Kraken, emblème de l’association et le
            Scylla depuis 2022.
            <br />
            <br />
            Depuis sa création, l'association a mené plus de 1 000 dépollutions,
            ramassé plus de 135 tonnes de déchets et fédéré plus de 900
            bénévoles. Ces actions ont vocation à protéger la biodiversité
            marine et l'écosystème naturel des littoraux.
            <br />
            <br />
            Lien vers le site internet de l’association :
            <Button variant="text" href="https://www.wingsoftheocean.com/">
              https://www.wingsoftheocean.com
            </Button>
          </p>
          <p>
            L’association Wings of the Ocean effectue de nombreux ramassages de
            déchets. A la fin de la collecte, les déchets sont caractérisés,
            c’est-à-dire finement triés afin de référencer ce qui a été retrouvé
            dans la nature. L’objectif est d’effectuer un état des lieux de la
            pollution causé par les déchets en milieu sauvage afin d’accompagner
            les actions visant à les réduire et à limiter leur impact sur
            l’environnement. L’ensemble des données de caractérisation de
            l’association est, depuis 2024, référencé sur WINGS-MAP.
            <br />
            <br />
            Les Protocoles de caractérisation 1,2,3 et 4 sont issus de la
            méthodologie Zéro Déchet Sauvage pensée par l’association Mer Terre.
            Toutes les données collectées sur WINGS-MAP sont transmises sur Zéro
            Déchet Sauvage.
          </p>
        </Typography>
      </Container>
    </Box>
  );
}
