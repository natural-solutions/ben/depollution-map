"use client";

import {
  Box,
  Button,
  ButtonGroup,
  IconButton,
  Grid,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { Dashboard, FilterList, Map, VerticalSplit } from "@mui/icons-material";
import MapComponent from "@/components/map/MapComponent";
import { DashboardComponent } from "../../components/dashboard/DashboardComponent";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import {
  CleanupFilters,
  DEFAULT_FILTER_VALUES,
  getFilteredFromURLCleanups,
  getFiltersFromURL,
} from "@/utils/cleanups";
import { Cleanup_View } from "@/graphql/types";
import DialogFilterComponent from "@/components/filters/DialogFilterComponent";
import { MAP_INIT_POINT } from "@/utils/map";
import { useApi } from "@/utils/useApi";
import { GET_CLEANUPS_FOR_APP } from "@/graphql/queries";
import { add, format } from "date-fns";
import { JsonParseSafe } from "@/utils/utils";
import { pick } from "lodash";

export default function Home() {
  const { postGraphql } = useApi();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const pathnames = usePathname();
  const router = useRouter();
  const searchParams = useSearchParams();
  const searchParamsString = searchParams.toString();
  const searchFilters = searchParams.get("filters");
  const mapViewStateParam =
    JsonParseSafe(searchParams.get("map_state")) || MAP_INIT_POINT;
  const display = searchParams.get("display") || "split";

  const [filterFieldValues, setFilterFieldValues] = useState<CleanupFilters>(
    DEFAULT_FILTER_VALUES
  );
  const [cleanups, setCleanups] = useState<Cleanup_View[]>([]);
  const [cleanupsFiltered, setCleanupsFiltered] = useState<Cleanup_View[]>([]);
  const [addFilter, setAddFilter] = useState(false);
  const [mapViewState, setMapViewState] = useState(mapViewStateParam);

  useEffect(() => {
    (async () => {
      const resp_cleanup = (
        await postGraphql<{
          data: {
            cleanup_view: Cleanup_View[];
          };
        }>({
          query: GET_CLEANUPS_FOR_APP,
          variables: {
            where: {
              start_at: {
                _lt: format(
                  add(new Date(), {
                    days: 1,
                  }),
                  "yyyy-MM-dd"
                ),
              },
              cleanup_forms: { status: { _eq: "published" } },
            },
          },
        })
      ).data.data.cleanup_view;

      setCleanups(resp_cleanup);
    })();
  }, []);

  const filtersChangeListener = async () => {
    if (!cleanups) {
      return;
    }
    setCleanupsFiltered(
      await getFilteredFromURLCleanups({
        cleanups,
        searchFilters,
      })
    );
  };

  const handleFilter = () => {
    const params = new URLSearchParams(searchParamsString);

    if (filterFieldValues) {
      params.set("filters", JSON.stringify(filterFieldValues));
    } else {
      params.delete("filters");
    }

    router.replace(`${pathnames}?${params.toString()}`);
    setAddFilter(false);
  };

  const closeFilters = () => {
    setAddFilter(false);
    setFilterFieldValues(getFiltersFromURL(searchFilters));
  };

  useEffect(() => {
    filtersChangeListener();
  }, [cleanups]);

  useEffect(() => {
    setFilterFieldValues(getFiltersFromURL(searchFilters));
    filtersChangeListener();
  }, [searchFilters]);

  useEffect(() => {
    const params = new URLSearchParams(searchParamsString);

    params.set(
      "map_state",
      JSON.stringify(pick(mapViewState, "latitude", "longitude", "zoom"))
    );
    window.history.replaceState(null, "", `${pathnames}?${params.toString()}`);
  }, [mapViewState]);

  const handleChangesView = (newDisplay: string) => {
    const params = new URLSearchParams(searchParamsString);

    params.set("display", newDisplay);
    router.replace(`${pathnames}?${params.toString()}`);
  };

  const onResetFilter = () => {
    const params = new URLSearchParams(searchParamsString);

    params.delete("filters");
    router.replace(`${pathnames}?${params.toString()}`);
    setAddFilter(false);
  };

  return (
    <Box
      sx={{
        height: "100%",
        position: "relative",
        overflow: "hidden",
      }}
    >
      <DialogFilterComponent
        open={addFilter}
        onClose={() => closeFilters()}
        handleFilter={handleFilter}
        onResetFilter={onResetFilter}
        fieldValues={filterFieldValues}
        setFieldValues={setFilterFieldValues}
        whereCleanups={{ cleanup_forms: { status: { _eq: "published" } } }}
      />
      <Button
        startIcon={<FilterList />}
        variant="contained"
        sx={{
          mb: 1,
          mt: 1,
          position: "absolute",
          zIndex: 1,
          left: display === "dashboard" ? "auto" : "170px",
          right: display === "dashboard" ? "32px" : "auto",
        }}
        onClick={() => setAddFilter(true)}
      >
        Filtres
      </Button>
      {!isMobile && (
        <ButtonGroup
          variant="contained"
          sx={{
            backgroundColor: "white",
            position: "absolute",
            zIndex: 1,
            top: 8,
            left: "50%",
            transform: "translateX(-50%)",
          }}
        >
          <IconButton
            onClick={() => handleChangesView("map")}
            sx={{ color: display === "map" ? "#3DA0EA" : "default" }}
            aria-label="close"
          >
            <Map />
          </IconButton>
          <IconButton
            onClick={() => handleChangesView("split")}
            sx={{
              color: display === "split" ? "#3DA0EA" : "default",
            }}
            aria-label="close"
          >
            <VerticalSplit />
          </IconButton>
          <IconButton
            onClick={() => handleChangesView("dashboard")}
            sx={{
              color: display === "dashboard" ? "#3DA0EA" : "default",
            }}
            aria-label="close"
          >
            <Dashboard />
          </IconButton>
        </ButtonGroup>
      )}
      <Grid container sx={{ height: "100%" }}>
        {display === "map" && (
          <MapComponent
            cleanups={cleanupsFiltered}
            viewState={mapViewState}
            setViewState={setMapViewState}
          />
        )}
        {display === "dashboard" && (
          <Box sx={{ width: "100%", height: "100%", overflow: "auto" }}>
            <DashboardComponent
              cleanups={cleanupsFiltered}
              isOnlyDashboard={true}
            />
          </Box>
        )}
        {(!display || display === "split") && (
          <>
            <Grid
              item
              xs={isMobile ? false : 8}
              display={isMobile ? "none" : "block"}
            >
              <MapComponent
                cleanups={cleanupsFiltered}
                viewState={mapViewState}
                setViewState={setMapViewState}
              />
            </Grid>
            <Grid item xs={isMobile ? 12 : 4} sx={{ height: "100%" }}>
              <Box sx={{ width: "100%", height: "100%", overflow: "auto" }}>
                <DashboardComponent
                  cleanups={cleanupsFiltered}
                  isOnlyDashboard={false}
                />
              </Box>
            </Grid>
          </>
        )}
      </Grid>
    </Box>
  );
}
