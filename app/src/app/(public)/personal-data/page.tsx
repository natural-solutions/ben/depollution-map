import { Box, Container, Typography } from "@mui/material";

export default function PersonalDataProtection() {
  return (
    <Box
      sx={{
        flex: 1,
        height: "100%",
        overflowY: "auto",
      }}
    >
      <Container sx={{ py: 5 }} maxWidth="md">
        <Typography
          variant="h2"
          sx={{
            textTransform: "uppercase",
          }}
        >
          Charte de protection des données personnelles
        </Typography>
        <Typography component="div">
          <p>
            Wings of the Ocean est soucieux de préserver la confidentialité et
            la sécurité des données personnelles et attache une grande
            importance au respect et à la protection de la vie privée de ses
            Utilisateurs.
          </p>
          <Typography variant="h4">Article 1 : Objet</Typography>
          <p>
            La présente charte de protection des données personnelles (ci-après
            la « charte ») a pour objet d’informer les Utilisateurs de
            l’utilisation faite par Wings of the Ocean des Données Personnelles.
            Cette utilisation est respectueuse de la Loi n° 78-17 du 6 janvier
            1978 relative à l'informatique, aux fichiers et aux libertés,
            modifiée par la Loi n°2004-801 du 6 août 2004 et par la loi n°
            2018-493 du 20 juin 2018 (la « Loi Informatique et Libertés ») et,
            de la Directive du 12 juillet 2002 modifiée par la Directive
            2009/136/CE (la « Directive ePrivacy ») et, du Règlement (UE)
            2016/679 du Parlement européen et du Conseil du 27 avril 2016
            relatif à la protection des personnes physiques à l'égard du
            traitement des données à caractère personnel et à la libre
            circulation de ces données personnelles (le « RGPD »), et tout texte
            national de transposition ou tout texte ultérieur qui succèderait à
            ceux-ci (ensemble la « Règlementation Applicable »). <br />
            Lors de l’utilisation du site accessible à l’adresse suivante :
            wings-map.com, L’association Wings of the Ocean dont les coordonnées
            sont détaillées à l’Article 3 ci-dessous est amenée à collecter des
            données personnelles des Utilisateurs (les « Données Personnelles
            »). La charte peut être modifiée à tout moment par Wings of the
            Ocean, afin notamment de se conformer à toutes évolutions
            législatives, réglementaires, jurisprudentielles, éditoriales ou
            techniques. Les Utilisateurs doivent donc se référer avant toute
            navigation à la dernière version de la charte.
          </p>
          <Typography variant="h4">Article 2 : Définitions</Typography>
          <p>
            Les termes « données à caractère personnel » « Données Personnelles
            », « Traiter/Traitement », « Responsable du Traitement », «
            Sous-Traitant », « Destinataire(s) », « Consentement », ont la même
            signification que celle énoncée à l’Article 4 du RGPD. <br />«
            Utilisateur(s) » désigne toute personne physique accédant au Site et
            Services, et n’utilisant que certaines fonctionnalités du site ou
            Services de Wings of the Ocean à un Utilisateur ; <br />« Services »
            désignent tout ou partie des services proposés par Wings of the
            Ocean sur son site ; <br />« Site(s) » désigne le(s) site(s)
            internet édité(s) à l’adresse : wings-map.com
          </p>
          <Typography variant="h4">
            Article 3 : Finalités et Responsable de Traitement des Données
            Personnelles
          </Typography>
          <p>
            Nous attachons la plus grande importance au respect de votre vie
            privée et à la protection de vos Données Personnelles. C’est
            pourquoi Wings of the Ocean traite vos Données Personnelles, dans le
            respect de la Règlementation Applicable, à des fins : <br />
            • de prise de contact avec les Utilisateurs et de gestion de la
            relation avec les Utilisateurs; La déclaration de Consentement passe
            par la transmission volontaire de ces informations de l’Utilisateur
            dans chacun des champs prévus à cet effet du Formulaire « Nous
            rejoindre » ; <br />• de consulter et conserver les informations
            relatives à la navigation sur les Sites susceptibles d’être
            enregistrées dans des fichiers « cookies » (définis à l’Article 9
            ci-dessous) ; <br />
            Les informations constituant des Données Personnelles au sens de la
            Règlementation Applicable volontairement renseignées par
            l’Utilisateur dans le Formulaire « Nous rejoindre » et notamment le
            numéro de téléphone, l’URL Facebook, Twitter ou LinkedIn, la
            fonction, et les centres d’intérêts, permettant de mieux connaître
            le Membre. <br />
            Pour ces finalités uniquement, le Responsable de Traitement des
            Données Personnelles est Wings of the Ocean association Loi 1901,
            dont le siège social est sis 102C rue Amelot, 75011 Paris,
            identifiée sous le n°S.I.R.E.T 841181282 dont les coordonnées de
            contact se trouvent à l’Article 10 ci-dessous.
          </p>
          <Typography variant="h4">
            Article 4 : Quelles sont les Données Personnelles collectées, <br />
            pour quelles finalités ?
          </Typography>
          <p>
            <b>Objectif de traitement</b> <br />
            Consulter et conserver les informations relatives à la navigation
            sur les Sites susceptibles d’être enregistrées dans des fichiers «
            cookies » <br />
            <b>Catégories de données traitées</b> <br />
            L’ensemble des données collectées sont identifiées à l’Article 9
          </p>
          <Typography variant="h4">
            Article 5 : Les destinataires des Données Personnelles
          </Typography>
          <p>
            La base de Données Personnelles constituée à l’occasion de votre
            utilisation des Services est strictement confidentielle. Wings of
            the Océan s’engage à prendre toutes les précautions utiles, mesures
            organisationnelles et techniques appropriées pour préserver la
            sécurité, l’intégrité et la confidentialité des Données
            Personnelles, et notamment empêcher qu’elles soient déformées,
            endommagées ou que des Tiers non autorisés y aient accès.
          </p>
          <Typography variant="h5">
            5.1 Données transférées aux autorités et/ou organismes publics
          </Typography>
          <p>
            Conformément à la Réglementation Applicable, les Données
            Personnelles peuvent être transmises aux autorités compétentes sur
            requête et notamment aux organismes publics, exclusivement pour
            répondre aux obligations légales, les auxiliaires de justice, les
            officiers ministériels et les organismes chargés d’effectuer le
            recouvrement de créances.
          </p>
          <Typography variant="h5">
            5.2 Données accessibles à des Tiers
          </Typography>
          <p>
            Les Données Personnelles pourront être utilisées par Wings of the
            Océan, ses Sous-Traitants, les co-Responsables du Traitement ses
            affiliés et/ou, le cas échéant, par ses partenaires, pour les
            finalités décrites à l’Article 3 ci-dessus : <br />
            • Le personnel habilité de Wings of the Ocean de par leurs
            fonctions, les services chargés du contrôle (commissaire aux comptes
            notamment) et les sous-traitants de Wings of the Ocean auront accès
            aux Données Personnelles collectées dans le cadre de l’utilisation
            des Services ;
            <br />
            • Les partenaires de Wings of the Ocean qui l’assistent dans la
            fourniture des Services ;
            <br />
            • Réseaux sociaux : si l'Utilisateur possède un compte sur des sites
            des réseaux sociaux et qu'il accède aux Services, ceux-ci peuvent
            inclure des plugiciels (plugins) tels que des boutons « J’aime » ou
            des boutons de partage ou de redirection, permettant de publier des
            contenus des Services sur les réseaux sociaux. Le cas échéant, les
            réseaux sociaux concernés peuvent recevoir des informations
            relatives à l’utilisation des Services par l’Utilisateur ou le
            Membre ;
            <br />• HelloASso entreprise solidaire qui fournit gratuitement ses
            technologies de paiement à l’organisme Wings of the océan.
          </p>
          <Typography variant="h5">
            5.3 Transferts hors de l’Union Européenne
          </Typography>
          <p>
            Les Données Personnelles de l’Utilisateur peuvent être Traitées en
            dehors de l’Union Européenne, y compris via un accès à distance.
            Wings of the Océan s’engage à ne réaliser aucun transfert des
            Données Personnelles hors de l’Union Européenne sans mettre en œuvre
            les garanties appropriées conformément à la Réglementation
            Applicable.
          </p>
          <Typography variant="h4">
            Article 6 : Les droits de l'utilisateur et du Membre
          </Typography>
          <p>
            Sous certaines conditions, l’Utilisateur peut disposer : <br />
            • d’un droit d’accès ; <br />
            • d’un droit à la portabilité ; <br />
            • d’un droit à l’effacement ; <br />
            • d’un droit d’opposition : En cas d’exercice du droit d’opposition,
            Wings of the Océan cessera le Traitement des Données Personnelles,
            sauf en cas de motif(s) légitime(s) et impérieux pour le Traitement,
            ou pour assurer la constatation, l’exercice ou la défense de ses
            droits en justice, conformément à la Règlementation Applicable. Le
            cas échéant, Wings of the Océan informera l’Utilisateur des motifs
            pour lesquels les droits qu’il exerce ne sauraient être satisfaits
            en tout ou partie. <br />
            • d’un droit à la limitation ;
            <br />• d’un droit à la rectification ;
            <br />• d’une droit de retirer leur consentement à tout moment ;
            <br />
            • d’un droit de désigner une personne qui peut récupérer leur
            données auprès leur décès.
            <br />
            <br />
            Sous réserve d’une demande expresse et de justifier de leur identité
            auprès de Wings of the Océan, l’Utilisateur disposent également du
            droit de récupérer leurs Données Personnelles dans un format
            structuré, couramment utilisé et lisible par machine, afin de les
            transmettre à un autre Responsable du Traitement. <br />
            Le cas échéant, Wings of the Ocean informera l’Utilisateur des
            motifs pour lesquels les droits qu’il exerce ne sauraient être
            satisfaits en tout ou en partie. <br />
            A cet égard, si l’Utilisateur, estime après nous avoir contactés que
            leurs droits n’ont pas été respectés, il peut adresser sa
            réclamation soit :
            <br />• via le formulaire de la CNIL Plaintes | CNIL
            <br />• via l’adresse postale de la CNIL : 3 Place de Fontenoy TSA
            80715 75334 Paris CEDEX
          </p>
          <Typography variant="h4">
            Article 7 : La durée de conservation des Données Personnelles
          </Typography>
          <p>
            Les Données Personnelles des Utilisateurs ne sont pas conservées
            au-delà de la durée strictement nécessaire aux finalités poursuivies
            telles qu'énoncées à l’Article 3 ci-dessus, conformément à la
            Règlementation Applicable. <br />
            Notamment : <br />• Les Données Personnelles recueillies pour gérer
            les demandes d’opposition au Traitement sont stockées dans un délai
            de 5 (cinq) ans maximum à compter de la clôture de la demande
            d’opposition ; <br />• Les Données Personnelles Traitées pour gérer
            les demandes d’accès et de rectification des Données Personnelles
            sont stockées dans un délai maximum de 5 (cinq) ans maximum à
            compter de la clôture des demandes ; <br />
            • Les Données Personnelles récoltées à des fins d’envoi
            d’informations ou d’offres (notamment par l’envoi de newsletters)
            sont stockées dans un délai maximum de 3 (trois) ans après le
            dernier contact entre Wings of the Océan et l’Utilisateur ;
            <br />• Les Données Personnelles récoltées à des fins de dons dont
            les doubles des reçus remis aux donateurs sont conservées pendant 6
            ans. Les Données Personnelles pourront être archivées au-delà des
            délais décrits ci-dessus dans la mesure où ils sont nécessaires pour
            les besoins de la recherche, la constatation, et de la poursuite des
            infractions pénales dans le seul but de permettre, en tant que de
            besoin, la mise à disposition de ces Données Personnelles à
            l’autorité judiciaire, ou encore pour d’autres obligations de
            conservation, notamment à des fins comptables ou fiscales.
            L'archivage implique que ces Données Personnelles feront l’objet de
            restrictions d’accès et seront conservées sur un support autonome et
            sécurisé.
          </p>
          <Typography variant="h4">Article 8 : Sécurité</Typography>
          <p>
            Conformément à la Règlementation Applicable, Wings of the Ocean
            traite les Données Personnelles en toute sécurité et
            confidentialité. <br />
            En particulier, Wings of the Ocean met en œuvre toutes les mesures
            techniques et organisationnelles nécessaires pour garantir la
            sécurité et la confidentialité des Données Personnelles collectées
            et traitées, et notamment empêcher qu'elles soient déformées,
            endommagées ou communiquées à des Tiers non autorisés, en assurant
            un niveau de sécurité adapté aux risques liés au traitement et à la
            nature des Données Personnelles à protéger, eu égard au niveau
            technologique et au coût de mise en œuvre. <br />
            Wings of the Océan ne peut toutefois garantir la confidentialité des
            Données Personnelles rendues publiques par l’Utilisateur dans les
            parties publiques du Site et des Services. <br />
            Le Site est susceptible d’inclure des liens vers des sites Internet
            ou des sources externes. L’Utilisateur reconnait que la Politique ne
            s’applique que pour les Services et l’utilisation des Sites, et ne
            couvre en aucun cas les informations collectées et/ou Traitées sur
            les sites ou sources externes, dont le lien figure sur le Site. En
            conséquence, Wings of the Ocean ne saurait être Responsable des
            pratiques de ces sites ou sources externes en matière de collecte et
            de Traitement de Données Personnelles, qui sont régies, le cas
            échéant, par les politiques de données personnelles propre à chacun
            de ces sites ou sources externes.
          </p>
          <Typography variant="h4">Article 9 : Politique de cookies</Typography>
          <Typography variant="h5">9.1 Qu’est-ce qu’un cookie ?</Typography>
          <p>
            Parmi les Données Personnelles, Wings of the Ocean est amené à
            collecter des données résultant du dépôt de cookies. <br />
            Un cookie est un petit fichier texte, image ou logiciel, qui est
            placé et stocké sur l’ordinateur ou le smartphone de l’Utilisateur
            ou du Membre Associé, ainsi que sur tout appareil leur permettant de
            naviguer sur Internet (« Terminal »). <br />
            Très utiles, les cookies permettent à un site Internet de
            reconnaitre l’Utilisateur de signaler leur passage sur telle ou
            telle page et de leur apporter ainsi un service additionnel :
            l’amélioration de leur confort de navigation, la sécurisation de
            leur connexion ou l’adaptation du contenu d’une page à leurs centres
            d’intérêt. <br />
            Les informations enregistrées par les cookies, pendant une durée de
            validité limitée, portent notamment sur les pages visitées, les
            publicités sur lesquelles l’Utilisateur ou le Membre ont cliquées,
            le type de navigateur qu’ils utilisent, leur adresse IP, les
            informations qu’ils ont saisies sur les Services (afin d'éviter de
            les saisir à nouveau). <br />
            Les cookies ne sont pas des dossiers actifs, et ne peuvent donc pas
            héberger de virus. Pour en savoir plus, vous pouvez vous rendre sur
            www.allaboutcookies.org.
          </p>
          <Typography variant="h5">
            9.2 A quoi servent les cookies émis sur les Sites ?
          </Typography>
          <p>
            Wings of the Ocean utilise les cookies pour améliorer l’expérience
            de l’Utilisateur sur son site. L’Utilisateur peut configurer son
            navigateur pour refuser tous ou une partie des cookies en fonction
            des finalités de ces derniers. <br />
            Seul l’émetteur d’un cookie est susceptible de lire ou de modifier
            des informations qui y sont contenues.
          </p>
          <Typography variant="h5">9.2.1 Les Cookies de navigation</Typography>
          <p>
            Les Cookies de navigation permettent d’améliorer les performances de
            Wings of the Ocean afin de fournir une meilleure utilisation des
            Sites. Ces Cookies ne requièrent pas l’information de l’Utilisateur,
            ni leur accord préalable pour être déposés sur le Terminal. <br />
            Les Cookies de navigation permettent : <br />
            • d’adapter la présentation des Sites aux préférences d’affichage du
            Terminal (par exemple, langue utilisée, résolution d’affichage,
            système d’exploitation utilisé, etc.) lors des visites de
            Utilisateurs, sur les Sites, selon les matériels et les logiciels de
            visualisation ou de lecture que le Terminal comporte ;
            <br />• de mémoriser les préférences d’utilisation, les paramètres
            d’affichage et lecteurs que les Utilisateur utilisent afin de
            faciliter leur navigation lors de leur prochaine visite sur les
            Sites. Ils permettent notamment l’accès à un espace réservé soumis à
            un identifiant ou un mot de passe ;
            <br />• De mémoriser les informations saisies sur certains
            formulaires présents sur le Site afin d’éviter de les renseigner de
            nouveau lors de leur prochaine visite.
          </p>
          {/* <Typography variant="h5">9.2.2 Les Cookies statistiques</Typography>
        <p>
          Afin d’adapter les Sites aux besoins de l’Utilisateur, Wings of the
          Ocean mesure le nombre de visites, le nombre de pages consultées ainsi
          que de l’activité de l’Utilisateur sur les Sites et leur fréquence de
          retour. Ces Cookies permettent d’établir des statistiques d’analyse de
          la fréquentation à partir desquels les contenus des Sites sont
          améliorés en fonction du succès rencontré par telle ou telle page
          auprès des Utilisateurs. Les résultats de ces analyses sont traités de
          manière anonyme et à des fins exclusivement statistiques. Émetteur du
          Cookie Nom du Cookie Finalité du Cookie Opt-out (renvoi vers les
          procédures d’opt-out le cas échéant) Durée de vie du Cookie Google
          analytics _ga collecter des statistiques pour Google Analytics cliquer
          sur le bouton "je refuse" dans la bannière de cookies" 13 mois Google
          Analytics _gid distinguer les utilisateurs pour Google Analytics
          cliquer sur le bouton "je refuse" dans la bannière de cookies" 24
          heures Google Tag Manager _gat_gtag_UA_54579503_1 collecter des
          statistiques pour Google Tag Manager cliquer sur le bouton "je refuse"
          dans la bannière de cookies" 1 minute
        </p> */}
          <Typography variant="h5">
            9.3 Durée de validité des Cookies
          </Typography>
          <p>
            Les Cookies émis sur les Sites sont des Cookies de session (dont la
            durée est limitée au temps d’une connexion aux Sites) et des Cookies
            persistants (dont la durée, toutefois limitée, est supérieure à la
            durée d’une connexion). <br />
            Les Cookies de session ne sont actifs que le temps de la visite et
            sont supprimés lorsque l’Utilisateur ou le Membre ferme son
            navigateur. Les Cookies persistants restent enregistrés sur le
            disque dur du Terminal de l’Utilisateur une fois son navigateur
            fermé. <br />
            Conformément à la Règlementation Applicable et aux recommandations
            de la Commission Nationale Informatique et Libertés (« CNIL »), les
            Cookies sont conservés pour une durée maximale de 13 (treize) mois
            après leur premier dépôt. A l’expiration de ce délai, le
            Consentement de l’Utilisateur devra être recueilli de nouveau pour
            la collecte des Cookies soumis au Consentement.
          </p>
          <Typography variant="h5">
            9.4 Vos choix concernant les Cookies
          </Typography>
          <p>
            L’ensemble des droits reconnus à l’Article 6 ci-dessus sont
            également applicables à l’Utilisateur. <br />
            Plusieurs possibilités leurs sont offertes pour gérer les Cookies.
            L’Utilisateur comprend que les Cookies améliorent le confort de
            navigation sur les Sites et sont indispensables pour accéder à
            certains espaces sécurisés. Tout paramétrage que l’Utilisateur ou le
            Membre peut entreprendre sera susceptible de modifier sa navigation
            sur les Sites et ses conditions d’accès à certains services
            nécessitant l’utilisation de Cookies. Wings of the Océan ne saurait
            être tenue pour responsable des conséquences d’un fonctionnement
            moins efficace des Sites dû à l’impossibilité d’installer ou de lire
            des Cookies nécessaires à leur bon fonctionnement, dès lors que
            l’Utilisateur les a rejetés ou supprimés. <br />
            Les Utilisateurs peuvent configurer leur logiciel de navigation de
            manière à ce que les Cookies soient enregistrés dans leur Terminal
            ou, au contraire, rejetés, soit systématiquement, soit selon leur
            émetteur. Les Utilisateurs peuvent également configurer leur
            logiciel de navigation de manière à ce que l’acceptation ou le refus
            des Cookies leur soit proposés ponctuellement, avant qu’un Cookie
            soit susceptible d’être enregistré sur leur Terminal. <br />
            Le menu d'aide ou la rubrique dédiée de votre navigateur vous
            permettra de savoir de quelle manière exprimer ou modifier vos
            préférences en matière de Cookies :
            <br />• Pour Internet Explorer™ :&nbsp;
            <a href="http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies">
              http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies
            </a>
            <br />• Pour Safari™ :&nbsp;
            <a href="https://support.apple.com/fr-fr/guide/safari/sfri11471/mac">
              https://support.apple.com/fr-fr/guide/safari/sfri11471/mac
            </a>
            <br />• Pour Chrome™ :&nbsp;
            <a href="http://support.google.com/chrome/bin/answer.py?hl=fr&hlrm=en&answer=95647">
              http://support.google.com/chrome/bin/answer.py?hl=fr&hlrm=en&answer=95647
            </a>
            <br />• Pour Firefox™ :&nbsp;
            <a href="http://support.mozilla.org/fr/kb/Activer%20et%20d%C3%A9sactiver%20les%20cookies">
              http://support.mozilla.org/fr/kb/Activer%20et%20d%C3%A9sactiver%20les%20cookies
            </a>
            <br />• Pour Opera™ :&nbsp;
            <a href="https://help.opera.com/en/latest/web-preferences/#cookies">
              https://help.opera.com/en/latest/web-preferences/#cookies
            </a>
            • Pour iOS : &nbsp;
            <a href="https://support.apple.com/fr-fr/HT201265">
              https://support.apple.com/fr-fr/HT201265
            </a>
            <br />• Pour Android :&nbsp;
            <a href="https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DAndroid&hl=fr">
              https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DAndroid&hl=fr
            </a>
            <br />• Pour Blackberry :&nbsp;
            <a href="https://global.blackberry.com/fr/legal/cookies">
              https://global.blackberry.com/fr/legal/cookies
            </a>
            <br />• Pour Windows Phone :&nbsp;
            <a href="https://support.microsoft.com/fr-fr/help/17442/windows-internet-explorer-delete-manage-cookies">
              https://support.microsoft.com/fr-fr/help/17442/windows-internet-explorer-delete-manage-cookies
            </a>
            <br />
            Pour une meilleure connaissance et maîtrise des Cookies de toute
            origine et pas seulement ceux de notre Site, les Utilisateurs sont
            invités à consulter le site Youronlinechoices, édité par
            l’Interactive Advertising Bureau France (IAB) afin de connaitre les
            entreprises inscrites à cette plate-forme et qui offrent la
            possibilité de refuser ou d’accepter les Cookies utilisés par ces
            entreprises pour adapter les informations de navigation les
            publicités susceptibles d’être affichées sur le Terminal <br />
            https://www.youronlinechoices.com/be-fr/controler-ses-cookies
          </p>

          <Typography variant="h4">
            Article 10 : Contact et réclamations
          </Typography>

          <p>
            Pour toute demande ou en cas de différend entre Wings of the Ocean
            et l’Utilisateur, concernant le Traitement de leurs Données
            Personnelles, ce dernier peut adresser sa demande ou sa réclamation
            à Wings of the Océan à l’adresse de courrier électronique suivante :{" "}
            <br />
            contact-rgpd.transition-ecologique@groupe-sos.org ou à l’adresse
            postale : 50 place Mazelle 57000 METZ. <br />
            Wings of the Océan s’efforcera de trouver une solution satisfaisante
            pour assurer le respect de la Règlementation Applicable. <br />
            En l’absence de réponse de Wings of the Ocean ou si le différend
            persiste malgré la proposition du groupe l’Utilisateur a la
            possibilité d’introduire une réclamation auprès de la CNIL ou de
            l’autorité de contrôle de l’État Membre de l’Union Européenne au
            sein duquel l’Utilisateur ou le Membre réside habituellement et de
            mandater une association ou une organisation mentionnée au IV de
            l’article 43 ter de la loi n°78-17 du 6 janvier 1978 modifiée.
          </p>
        </Typography>
      </Container>
    </Box>
  );
}
