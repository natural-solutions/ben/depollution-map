import { Box, Container, Typography } from "@mui/material";

export default function LegalNotive() {
  return (
    <Box
      sx={{
        flex: 1,
        height: "100%",
        overflowY: "auto",
      }}
    >
      <Container sx={{ py: 5 }} maxWidth="md">
        <Typography
          variant="h2"
          sx={{
            mb: 4,
            textTransform: "uppercase",
          }}
        >
          MENTION LÉGALES
        </Typography>
        <Typography component="div">
          <Typography variant="h4">ÉDITEUR</Typography>
          <p>
            Wings of the Ocean <br />
            Forme juridique : Association de droit local <br />
            SIRET : 841 181 282 000 10 <br />
            APE : 3900Z <br />
            Président administrateur unique : David Morel <br />
            Adresse : 102C rue Amelot 75 011 Paris <br />
            tel: 0659603454 <br />
            <a href="https://www.wingsoftheocean.com/contact">
              Nous contacter par mail
            </a>
          </p>
          <Typography variant="h4">HÉBERGEMENT</Typography>
          <p>
            Hetzner Online GmbH <br />
            Industriestr. 25 <br />
            91710 Gunzenhausen <br />
            Allemagne
          </p>
          <Typography variant="h4">DESIGN ET DÉVELOPPEMENT</Typography>
          <p>Site réalisé par Natural Solution</p>
          <Typography variant="h4">
            CONDITIONS D'ACCÈS ET D'UTILISATION
          </Typography>
          <p>
            Toute consultation et utilisation du site www.wings-map.com implique
            l'acceptation sans restriction des termes de la réglementation
            présente dans les mentions légales ci-dessous. Par conséquent, tout
            accès au site www.wings-map.com est soumis à cette réglementation
            ainsi qu'aux lois en vigueur.
          </p>
          <Typography variant="h4">CONTENU</Typography>
          <p>
            Le site www.wings-map.com contient des informations relatives à
            Wings of the Ocean. <br /> Toute information fournie sur ce site
            l'est exclusivement à titre indicatif. <br />
            L'information contenue sur ce site est conçue pour être aussi
            exhaustive que possible. <br />
            Wings of the Ocean se réserve le droit d'apporter, à tout moment et
            sans préavis, des modifications au programme.
          </p>
          <Typography variant="h4">
            DONNÉES PERSONNELLES / DONNÉES ENTRÉES PAR L'UTILISATEUR
          </Typography>
          <p>
            D'une façon générale, vous pouvez visiter notre site sur Internet
            sans avoir à décliner votre identité et à fournir des informations
            personnelles vous concernant.
          </p>
          <Typography variant="h4">
            COPYRIGHT, AVIS DE PROPRIÉTÉ INTELLECTUELLE ET INDUSTRIELLE
          </Typography>
          <p>
            La présentation et le contenu de ce site sont intégralement protégés
            par la réglementation relative à la protection des droits de
            propriété intellectuelle et industrielle. Les informations, textes,
            images ou graphiques ne peuvent être utilisés qu'à titre strictement
            personnel et dans un but non commercial. Toute forme de
            reproduction, de modification, de transmission, d'attribution de
            licence ou de publication de tout ou partie de ce matériel, à
            quelque fin que ce soit, est interdite sans l'autorisation écrite
            préalable à Wings of the Ocean, sauf pour être visionné.
          </p>
          <Typography variant="h4">HYPERLIENS</Typography>
          <p>
            Le site www.wings-map.com peut contenir des liens hypertextes menant
            à d'autres sites Internet totalement indépendants du site
            www.wings-map.com. Dès lors, tout accès à un autre site Internet lié
            au site www.wings-map.com s'effectue sous la propre responsabilité,
            pleine et entière, de l'utilisateur.
          </p>
          <Typography variant="h4">RESPONSABILITÉ LIMITÉE</Typography>
          <p>
            Wings of the Ocean décline spécifiquement toute responsabilité pour
            les éventuels préjudices directs, indirects, accidentels,
            consécutifs ou spéciaux liés à l'accès ou à l'utilisation du site
            www.wings-map.com ou subis en conséquence de ceux-ci, y compris,
            mais sans s'y limiter, à toute perte ou dommage provoqués par des
            virus affectant l'équipement informatique de l'utilisateur ou
            résultant du crédit accordé à une information obtenue par le biais
            du site www.wings-map.com. <br />
            De même, l'éditeur du présent site ne peut être tenu responsable en
            cas de mauvaise utilisation du site par l'utilisateur ou en cas
            d'indisponibilité temporaire du site (cas de force majeure, de
            période de maintenance ou d'incident technique, quel qu'il soit).{" "}
            <br />
            L'utilisateur est le seul et unique responsable de l'usage des
            informations figurant sur le présent site et des conséquences qui
            pourraient en découler, notamment quant aux décisions prises et aux
            actions entreprises sur la base de ces informations.
          </p>
          <Typography variant="h4">
            MISE À JOUR DE LA RÉGLEMENTATION DU SITE
          </Typography>
          <p>
            Wings of the Ocean pourra, en fonction de ses impératifs
            commerciaux, modifier les mentions légales de son site. Chaque
            internaute est donc invité à visiter la présente page lors de chaque
            consultation du site afin de prendre connaissance de ladite
            réglementation.
          </p>
          <Typography variant="h4">UTILISATION DES COOKIES</Typography>
          <p>
            Les utilisateurs du présent site sont informés que, lors de l'accès
            à ce site, des informations peuvent être temporairement conservées
            en mémoire ou sur leur disque dur afin de faciliter la navigation
            sur le site. Les utilisateurs du présent site reconnaissent avoir
            été informés de cette pratique et des moyens dont ils disposent pour
            s'y opposer.
          </p>
        </Typography>
      </Container>
    </Box>
  );
}
