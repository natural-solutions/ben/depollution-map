"use client";

import {
  Box,
  Stack,
  ThemeProvider,
  createTheme,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { useSession } from "next-auth/react";
import { PropsWithChildren } from "react";
import DesktopLayout from "@/components/layouts/DesktopLayout";
import MobileLayout from "@/components/layouts/MobileLayout";
import { HEADER_HEIGHT_PX, themeOptions } from "@/utils/theme";

const customTheme = createTheme(themeOptions);

export default function PublicLayout(props: PropsWithChildren) {
  const session = useSession();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <ThemeProvider theme={customTheme}>
      <Stack
        sx={{
          height: "100%",
          background: "#F0EFEC",
        }}
      >
        {!isMobile && <DesktopLayout session={session} />}
        <Box
          sx={{
            pt: {
              md: HEADER_HEIGHT_PX,
            },
            minHeight: "100vh",
          }}
        >
          {props.children}
        </Box>
        {isMobile && <MobileLayout session={session} />}
      </Stack>
    </ThemeProvider>
  );
}
