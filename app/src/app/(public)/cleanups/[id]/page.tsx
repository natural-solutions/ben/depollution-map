"use client";

import { ButtonLink } from "@/components/ButtonLink";
import {
  GET_CLEANUP,
  GET_CLEANUPS_FOR_HISTORY,
  GET_CLEANUP_IDS,
} from "@/graphql/queries";
import { Cleanup, Cleanup_View } from "@/graphql/types";
import { useApi } from "@/utils/useApi";
import {
  Box,
  Button,
  Card,
  CardMedia,
  Grid,
  IconButton,
  List,
  Menu,
  MenuItem,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import React, { FC, useEffect, useState } from "react";
import { useParams, useRouter, useSearchParams } from "next/navigation";
import { IMAGOR_PRESET_NAME, getTransformationURL } from "@/utils/media";
import { HasRole } from "@/components/HasRole";
import { CleanupPhotos } from "@/components/cleanup/CleanupPhotos";
import { useSessionContext } from "@/utils/useSessionContext";
import { CleanupDetailStats } from "@/components/cleanup/CleanupDetailStats";
import { InfoPaper } from "@/components/cleanup/InfoPaper";
import { CleanupPlanning } from "@/components/cleanup/CleanupPlanning";
import { CleanupHistoryRow } from "@/components/cleanup/CleanupHistorical";
import { getCleanupForm } from "@/utils/cleanups";
import { CleanupButtsStats } from "@/components/cleanup/CleanupButtsStats";
import { CleanupDialogHistory } from "@/components/cleanup/CleanupDialogHistory";
import { ArrowBackIos, MoreVert } from "@mui/icons-material";
import Link from "next/link";
import { useSession } from "next-auth/react";

const CleanupFormBtn: FC<{
  id: string;
  cleanup: Cleanup;
  canCreateCleanupForm?: boolean;
  canEditCleanupForm: boolean;
}> = ({ id, cleanup, canCreateCleanupForm, canEditCleanupForm }) => {
  const router = useRouter();
  const session = useSession();
  return canCreateCleanupForm ? (
    <Button
      variant="contained"
      onClick={() => {
        router.push(`/forms#/cleanup_form/create?cleanup_id=${id}`);
      }}
      size={"large"}
    >
      Créer la caractérisation
    </Button>
  ) : getCleanupForm(cleanup)?.status === "published" &&
    session.status === "authenticated" ? (
    <Button
      variant="contained"
      onClick={() => {
        router.push(`/forms#/cleanup_form/${cleanup.cleanup_forms[0].id}`);
      }}
      size={"medium"}
    >
      Voir la caractérisation
    </Button>
  ) : (
    canEditCleanupForm &&
    getCleanupForm(cleanup)?.status !== "published" && (
      <Button
        variant="contained"
        onClick={() => {
          router.push(`/forms#/cleanup_form/${cleanup.cleanup_forms[0].id}`);
        }}
        size={"medium"}
      >
        Éditer la caractérisation
      </Button>
    )
  );
};

const CleanupPage: FC = () => {
  const { id } = useParams<{ id: string }>();
  const api = useApi();
  const sessionContext = useSessionContext();
  const theme = useTheme();
  const router = useRouter();
  const params = useSearchParams();
  const backUrl = params.get("back_url");

  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const [cleanup, setCleanup] = useState<Cleanup>();
  const [allPhotos, setAllPhotos] = useState<string[]>([]);
  const [open, setOpen] = useState(false);
  const [currentFilename, setCurrentFilename] = useState<string>("");
  const [cleanupHistorical, setCleanupHistorical] = useState<Cleanup_View[]>(
    []
  );
  const [hasCleanupAfter, setHasCleanupAfter] = useState(false);
  const [isOpenHistorical, setIsOpenHistorical] = useState(false);
  const [totalHistory, setTotalHistory] = useState(0);
  const [mobileMenuAnchor, setMobileMenuAnchor] =
    useState<HTMLButtonElement | null>(null);

  const cleanupForm = getCleanupForm(cleanup);
  const canCreateCleanupForm =
    !cleanupForm && sessionContext.getCanCreateCleanupForm(cleanup);
  const canEditCleanupForm = sessionContext.getCanEditCleanupForm(cleanupForm);

  useEffect(() => {
    (async () => {
      const cleanup = await api.postGraphql<{
        data: {
          cleanup_by_pk: Cleanup;
        };
      }>({
        query: GET_CLEANUP,
        variables: { id: id },
      });

      const cleanupData = cleanup.data.data.cleanup_by_pk;
      if (cleanupData.cleanup_place_id) {
        const place_historical = await api.postGraphql<{
          data: {
            cleanup_view: Cleanup_View[];
            total: {
              aggregate: {
                count: number;
              };
            };
          };
        }>({
          query: GET_CLEANUPS_FOR_HISTORY,
          variables: {
            where: {
              cleanup_place_id: {
                _eq: cleanupData.cleanup_place_id,
              },
              start_at: {
                _lt: cleanupData.start_at,
              },
            },
            order_by: { start_at: "desc" },
            limit: 3,
          },
        });
        setTotalHistory(
          place_historical.data?.data?.total?.aggregate?.count || 0
        );
        if (place_historical.data?.data?.cleanup_view?.length) {
          setCleanupHistorical(place_historical.data.data.cleanup_view);
        }
        if (place_historical.data?.data?.total?.aggregate?.count <= 3) {
          const resp = await api.postGraphql<{
            data: { cleanup: Cleanup[] };
          }>({
            query: GET_CLEANUP_IDS,
            variables: {
              where: {
                cleanup_place_id: {
                  _eq: cleanupData.cleanup_place_id,
                },
                start_at: {
                  _gt: cleanupData.start_at,
                },
              },
              limit: 1,
            },
          });
          if (resp.data?.data?.cleanup?.length) {
            setHasCleanupAfter(true); // We have to show the "more button"
          }
        }
      }

      setCleanup(cleanupData);
      setAllPhotos(
        cleanupData?.cleanup_photos?.map((photo) => photo.filename) || []
      );
    })();
  }, [id]);

  const handleClickPhotos = (filename: string) => {
    setOpen(true);
    setCurrentFilename(filename);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleHistoryBack = () => {
    if (backUrl === "back") {
      router.back();
    } else {
      router.push("/cleanups");
    }
  };

  const handleCloseMobileMenu = () => {
    setMobileMenuAnchor(null);
  };

  return (
    <Box
      sx={{
        paddingX: 2,
        pt: 2,
        pb: isMobile ? 8 : 0,
        flex: 1,
        height: "100%",
        overflowY: "auto",
      }}
    >
      {cleanup && (
        <>
          <CleanupDialogHistory
            placeId={cleanup.cleanup_place_id!}
            isOpen={isOpenHistorical}
            setIsOpen={setIsOpenHistorical}
          />
          {open && (
            <CleanupPhotos
              allPhotos={allPhotos}
              filename={currentFilename}
              onClose={handleClose}
              cleanupId={id}
            />
          )}
          <Menu
            anchorEl={mobileMenuAnchor}
            open={Boolean(mobileMenuAnchor)}
            onClose={handleCloseMobileMenu}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            sx={{
              "& .MuiMenu-paper": {
                borderRadius: "3px",
              },
            }}
          >
            <MenuItem
              component={Link}
              href={`/forms#/cleanup/create?from_cleanup_id=${id}`}
              onClick={handleCloseMobileMenu}
            >
              Dupliquer
            </MenuItem>
            <MenuItem
              component={Link}
              href={`/forms#/cleanup/${id}`}
              onClick={handleCloseMobileMenu}
            >
              Éditer
            </MenuItem>
          </Menu>
          <Stack
            direction={"row"}
            justifyContent="space-between"
            sx={{
              position: {
                xs: "sticky",
                md: "static",
              },
              top: {
                xs: 0,
                md: "auto",
              },
              background: "#F0EFEC",
              mx: -2,
              px: 2,
              zIndex: 3,
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                width: "100%",
              }}
            >
              {isMobile ? (
                <IconButton
                  size="large"
                  edge="start"
                  color="primary"
                  onClick={() => handleHistoryBack()}
                >
                  <ArrowBackIos />
                </IconButton>
              ) : (
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => handleHistoryBack()}
                >
                  Retour
                </Button>
              )}
              <Typography
                variant="h2"
                sx={{ display: "inline-block", paddingLeft: 2 }}
              >
                {cleanup.label}
              </Typography>
            </Box>
            <HasRole orCanEditCleanup={cleanup}>
              {isMobile ? (
                <IconButton
                  color="primary"
                  onClick={(e) => {
                    setMobileMenuAnchor(e.currentTarget as any);
                  }}
                >
                  <MoreVert />
                </IconButton>
              ) : (
                <Stack direction="row" spacing={2}>
                  <ButtonLink
                    variant="contained"
                    color="secondary"
                    href={`/forms#/cleanup/create?from_cleanup_id=${id}`}
                  >
                    Dupliquer
                  </ButtonLink>
                  <ButtonLink
                    variant="contained"
                    color="primary"
                    href={`/forms#/cleanup/${id}`}
                  >
                    Éditer
                  </ButtonLink>
                </Stack>
              )}
            </HasRole>
          </Stack>
          <Grid
            container
            spacing={2}
            direction={isMobile ? "column" : "row"}
            wrap={isMobile ? "nowrap" : "wrap"}
          >
            <Grid
              item
              xs={4}
              sx={{
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Box
                sx={{
                  pl: {
                    xs: 0,
                    md: 2,
                  },
                }}
              >
                {!isMobile ? (
                  <CleanupPlanning cleanup={cleanup} />
                ) : (
                  <Card
                    square
                    sx={{ mt: 2, cursor: "pointer" }}
                    onClick={() =>
                      handleClickPhotos(cleanup?.cleanup_photos[0]?.filename)
                    }
                  >
                    <CardMedia
                      component="img"
                      sx={{
                        height: "22vh",
                      }}
                      image={
                        getTransformationURL(
                          "cleanups",
                          cleanup.id,
                          cleanup?.cleanup_photos[0]?.filename,
                          IMAGOR_PRESET_NAME["800x600"]
                        ) || ""
                      }
                    />
                  </Card>
                )}
              </Box>
            </Grid>
            <Grid item xs={4}>
              <Stack
                direction={"column"}
                spacing={2}
                sx={{
                  flex: 1,
                  marginTop: 2,
                  paddingBottom: "10px",
                }}
              >
                <Typography variant="h5">Informations</Typography>
                {cleanup?.cleanup_campaigns.map((campaign, index) => (
                  <InfoPaper
                    key={index}
                    title="Entités wings"
                    content={campaign.campaign.label}
                  />
                ))}
                <InfoPaper
                  title="Commune"
                  content={cleanup?.city?.label ?? ""}
                />
                <InfoPaper
                  title="Lieu"
                  content={cleanup?.cleanup_place?.label ?? ""}
                />
                <InfoPaper
                  title="Type de milieu"
                  content={cleanup?.area_type?.label ?? ""}
                />
                <InfoPaper
                  title="Niveau de caractérisation"
                  content={
                    cleanup?.cleanup_type?.characterization_level?.label || ""
                  }
                />
                <InfoPaper
                  title="Description du ramassage"
                  content={
                    cleanup?.description ?? "Aucune description fournie."
                  }
                />
                {!isMobile ? (
                  <>
                    {cleanup.cleanup_photos.length > 0 && (
                      <Card
                        square
                        sx={{ mt: 2, cursor: "pointer" }}
                        onClick={() =>
                          handleClickPhotos(
                            cleanup?.cleanup_photos[0]?.filename
                          )
                        }
                      >
                        <CardMedia
                          component="img"
                          sx={{
                            height: "220px",
                          }}
                          image={
                            getTransformationURL(
                              "cleanups",
                              cleanup.id,
                              cleanup?.cleanup_photos[0]?.filename,
                              IMAGOR_PRESET_NAME["800x600"]
                            ) || ""
                          }
                        />
                      </Card>
                    )}
                  </>
                ) : (
                  <CleanupPlanning cleanup={cleanup} />
                )}
              </Stack>
            </Grid>
            <Grid item xs={4}>
              <Stack
                direction={"column"}
                spacing={2}
                sx={{
                  flex: 1,
                  marginTop: 2,
                  paddingBottom: "10px",
                }}
              >
                <Typography variant="h5">
                  Historique des ramassages sur ce lieu
                </Typography>
                {cleanupHistorical.length ? (
                  <List>
                    {cleanupHistorical.map((clp) => (
                      <CleanupHistoryRow key={clp.id} cleanup={clp} />
                    ))}
                  </List>
                ) : !hasCleanupAfter ? (
                  <Typography variant="body2">
                    Aucun autre ramassage n’a été effectué sur ce lieu.
                  </Typography>
                ) : (
                  <Typography variant="body2">
                    Aucun ramassage antérieur n'est répertorié sur ce lieu.
                  </Typography>
                )}
                {(totalHistory > 3 || hasCleanupAfter) && (
                  <Box sx={{ display: "flex", justifyContent: "center" }}>
                    <Button
                      onClick={() => setIsOpenHistorical(true)}
                      color="primary"
                      size="medium"
                    >
                      Tout afficher
                    </Button>
                  </Box>
                )}

                <Stack
                  direction="row"
                  sx={{
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Typography variant="h5">Données du ramassages</Typography>
                  <Box
                    sx={{
                      display: {
                        xs: "none",
                        sm: "block",
                        md: "none",
                        xl: "block",
                      },
                    }}
                  >
                    <CleanupFormBtn
                      {...{
                        id,
                        cleanup,
                        canCreateCleanupForm,
                        canEditCleanupForm,
                      }}
                    />
                  </Box>
                </Stack>

                <Paper square sx={{ p: 2 }}>
                  <Box
                    sx={{
                      mb: 1,
                      textAlign: "center",
                      display: {
                        xs: "block",
                        sm: "none",
                        md: "block",
                        xl: "none",
                      },
                    }}
                  >
                    <CleanupFormBtn
                      {...{
                        id,
                        cleanup,
                        canCreateCleanupForm,
                        canEditCleanupForm,
                      }}
                    />
                  </Box>
                  {cleanup.cleanup_type_id === "butts" ? (
                    <CleanupButtsStats cleanup={cleanup} />
                  ) : (
                    <CleanupDetailStats cleanup={cleanup} />
                  )}
                </Paper>
              </Stack>
            </Grid>
          </Grid>
        </>
      )}
    </Box>
  );
};

export default CleanupPage;
