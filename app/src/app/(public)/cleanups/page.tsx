"use client";

import {
  Box,
  Button,
  Paper,
  Pagination,
  Stack,
  Tab,
  Tabs,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import CleanupCard from "@/components/cleanup/CleanupCard";
import { Cleanup_View } from "@/graphql/types";
import { FC, useEffect, useState } from "react";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { FilterComponent } from "@/components/filters/FilterComponent";
import { CleanupFilters, getFiltersFromURL } from "@/utils/cleanups";
import { useApi } from "@/utils/useApi";
import {
  DEFAULT_FILTER_VALUES,
  getFilteredFromURLCleanups,
} from "@/utils/cleanups";
import { GET_CLEANUPS_FOR_APP } from "@/graphql/queries";
import { useSession } from "next-auth/react";
import { HasRole } from "@/components/HasRole";
import { startOfDay } from "date-fns";
import DialogFilterComponent from "@/components/filters/DialogFilterComponent";
import { FilterList } from "@mui/icons-material";
import { BadgeInline } from "@/components/BadgeInline";

const PAGE_SIZE = 30;

const Cleanups: FC = () => {
  const api = useApi();
  const session = useSession();
  const router = useRouter();
  const today = startOfDay(new Date());
  const searchParams = useSearchParams();
  const eventStatus = searchParams.get("event_status");
  const searchFilters = searchParams.get("filters");
  const pageIndex = Number(searchParams.get("page")) || 1;
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const pathName = usePathname();
  const [filterFieldValues, setFilterFieldValues] = useState<CleanupFilters>(
    DEFAULT_FILTER_VALUES
  );
  const [cleanups, setCleanups] = useState<Cleanup_View[]>([]);
  const [cleanupsFiltered, setCleanupsFiltered] = useState<Cleanup_View[]>([]);

  const [cleanupCount, setCleanupCount] = useState<{
    upcommingCount: number;
    completedCount: number;
  }>({
    upcommingCount: 0,
    completedCount: 0,
  });
  const [cleanupElements, setCleanupElements] = useState<Cleanup_View[]>([]);
  const isAuthentified = session.status === "authenticated";
  const isUpcommingCleanups = !eventStatus
    ? isAuthentified
    : eventStatus === "upcomming"
    ? true
    : false;

  const pageCount = isUpcommingCleanups
    ? Math.ceil(cleanupCount.upcommingCount / PAGE_SIZE)
    : Math.ceil(cleanupCount.completedCount / PAGE_SIZE);
  const [showFilterMobile, setShowFilterMobile] = useState(false);

  const filtersChangeListener = async () => {
    if (!cleanups) {
      return;
    }
    setCleanupsFiltered(
      await getFilteredFromURLCleanups({
        cleanups,
        searchFilters,
      })
    );
  };
  
  const handleFilter = () => {
    let url = `${pathName}?page=${pageIndex}`;

    if (filterFieldValues) {
      url += `&filters=${JSON.stringify(filterFieldValues)}`;
    }

    if (eventStatus) {
      url += `&event_status=${eventStatus}`;
    }

    router.replace(url);
  };

  const handleChangeEvent = (newEventStatus: "completed" | "upcomming") => {
    let url = `/cleanups?page=${pageIndex}`;

    if (searchFilters) {
      url += `&filters=${searchFilters}`;
    }

    url += `&event_status=${newEventStatus}`;

    router.replace(url);
  };

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    let url = `${pathName}?page=${value}`;

    if (searchFilters) {
      url += `&filters=${searchFilters}`;
    }

    if (eventStatus) {
      url += `&event_status=${eventStatus}`;
    }

    router.replace(url);
  };

  useEffect(() => {
    (async () => {
      const resp = await api.postGraphql<{
        data: {
          cleanup_view: Cleanup_View[];
        };
      }>({
        query: GET_CLEANUPS_FOR_APP,
        variables: {
          order_by: { start_at: "desc" },
        },
      });
      setCleanups(resp.data.data.cleanup_view);
    })();
  }, []);

  useEffect(() => {
    filtersChangeListener();
  }, [cleanups]);

  useEffect(() => {
    setFilterFieldValues(getFiltersFromURL(searchFilters));
    filtersChangeListener();
    if (isMobile) {
      setShowFilterMobile(false);
    }
  }, [searchFilters]);

  useEffect(() => {
    let upcommingCount = 0;
    let completedCount = 0;
    let displayedCleanups: Cleanup_View[] = [];

    cleanupsFiltered.forEach((cleanup: Cleanup_View) => {
      if (cleanup.start_at) {
        const start_at = new Date(cleanup.start_at);

        if (start_at > today && cleanup.cleanup_forms.length === 0) {
          upcommingCount++;
          if (isUpcommingCleanups) {
            displayedCleanups.push(cleanup);
          }
        } else if (
          start_at < today &&
          cleanup.cleanup_forms.length > 0 &&
          cleanup.cleanup_forms.some((cf) => cf.status === "published")
        ) {
          completedCount++;
          if (!isUpcommingCleanups) {
            displayedCleanups.push(cleanup);
          }
        }
      }
    });

    setCleanupCount({ upcommingCount, completedCount });
    setCleanupElements(displayedCleanups);
  }, [isUpcommingCleanups, cleanupsFiltered]);

  const onResetFilter = () => {
    if (eventStatus) {
      router.replace(`/cleanups?page=${pageIndex}&event_status=${eventStatus}`);
    } else {
      router.replace(`/cleanups?page=${pageIndex}`);
    }
  };

  return (
    <Box
      sx={{
        background: theme.palette.text.primary,
        ...(isMobile
          ? {
              px: 1,
              height: "calc(100% - 56px)",
              overflowX: "hidden",
              overflowY: "auto",
              pb: "80px",
            }
          : {
              flex: 1,
              height: "100%",
              overflow: "hidden",
              px: 2,
              pb: 1,
            }),
      }}
    >
      <Stack
        sx={{
          ...(!isMobile && {
            height: "100%",
            overflow: "hidden",
            maxWidth: "1200px",
            marginX: "auto",
          }),
        }}
      >
        {isMobile && (
          <Button
            size="large"
            variant="contained"
            color="primary"
            startIcon={<FilterList />}
            onClick={() => setShowFilterMobile(!showFilterMobile)}
            sx={{
              zIndex: 1,
              position: "absolute",
              bottom: "66px",
              left: "50%",
              transform: "translateX(-50%)",
            }}
          >
            Filtrer
          </Button>
        )}
        <Typography
          variant="h4"
          sx={{
            py: {
              xs: 2,
              md: 3,
            },
            pl: 1,
            color: "#FFF",
          }}
        >
          {isAuthentified ? (
            "Ramassages"
          ) : (
            <>
              Ramassages réalisés
              <BadgeInline
                badgeContent={cleanupCount.completedCount}
                sx={{
                  "& .MuiBadge-badge": {
                    color: theme.palette.text.primary,
                    background: "#FFF",
                  },
                }}
              />
            </>
          )}
        </Typography>
        {isMobile && isAuthentified && (
          <Box
            sx={{
              textAlign: "center",
              mb: 1,
            }}
          >
            <Button
              size="large"
              variant="contained"
              color="primary"
              href="/forms#/cleanup/create"
            >
              Créer un ramassage
            </Button>
          </Box>
        )}
        <Stack
          direction={{
            xs: "column",
            md: "row",
          }}
          spacing={{
            md: 1,
          }}
          sx={
            isMobile
              ? {
                  overflow: "visible",
                }
              : {
                  flex: 1,
                  overflow: "hidden",
                  justifyContent: "space-between",
                }
          }
        >
          <Stack
            spacing={2}
            sx={{
              width: "464px",
              px: "2px",
            }}
          >
            <Box
              sx={{
                flex: 1,
                overflow: "hidden",
                p: "2px",
                display: {
                  xs: "none",
                  md: "block",
                },
              }}
            >
              <Paper
                sx={{
                  maxHeight: "100%",
                  p: 2,
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Stack spacing={2} sx={{ flex: 1, overflowY: "hidden" }}>
                  <HasRole orCanCreateCleanup={true}>
                    <Stack
                      sx={{
                        alignItems: "center",
                      }}
                    >
                      <Button
                        variant="contained"
                        color="primary"
                        href="/forms#/cleanup/create"
                      >
                        Créer un ramassage
                      </Button>
                    </Stack>
                  </HasRole>
                  <Typography variant="h5" sx={{ display: "inline-block" }}>
                    Filtres
                  </Typography>

                  <Stack
                    sx={{
                      flex: 1,
                      overflow: "hidden",
                      pb: "2px",
                    }}
                  >
                    <FilterComponent
                      handleFilter={handleFilter}
                      fieldValues={filterFieldValues}
                      setFieldValues={setFilterFieldValues}
                      onResetFilter={onResetFilter}
                    />
                  </Stack>
                </Stack>
              </Paper>
            </Box>
          </Stack>

          <Stack
            sx={{
              width: {
                md: "580px",
              },
              height: {
                md: "100%",
              },
              overflow: {
                md: "hidden",
              },
            }}
          >
            {isAuthentified && (
              <Box
                sx={{
                  width: "100%",
                  zIndex: 1,
                  position: {
                    xs: "sticky",
                    md: "static",
                  },
                  top: {
                    xs: 0,
                    md: "auto",
                  },
                  backgroundColor: "#F0EFEC",
                  borderBottom: 1,
                  borderColor: "divider",
                }}
              >
                <Tabs
                  sx={{
                    width: "100%",
                    fontSize: "14px",
                  }}
                  value={isUpcommingCleanups ? 0 : 1}
                  onChange={(_, value) => {
                    const newEventStatus =
                      value === 1 ? "completed" : "upcomming";
                    handleChangeEvent(newEventStatus);
                  }}
                >
                  <Tab
                    value={0}
                    label={
                      <Box // TODO component
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          justifyContent: "center",
                        }}
                      >
                        A venir
                        <BadgeInline
                          badgeContent={cleanupCount.upcommingCount}
                        />
                      </Box>
                    }
                    sx={{
                      minWidth: "50%",
                    }}
                  />
                  <Tab
                    value={1}
                    label={
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          justifyContent: "center",
                        }}
                      >
                        Réalisés
                        <BadgeInline
                          badgeContent={cleanupCount.completedCount}
                        />
                      </Box>
                    }
                    sx={{
                      minWidth: "50%",
                    }}
                  />
                </Tabs>
              </Box>
            )}
            <Box
              sx={{
                flex: {
                  md: 1,
                },
                overflowY: {
                  md: "auto",
                },
                pt: isAuthentified ? 1 : 0,
              }}
            >
              {cleanupElements
                .slice((pageIndex - 1) * PAGE_SIZE, pageIndex * PAGE_SIZE)
                .map((cleanup) => (
                  <Box key={cleanup.id} sx={{ mb: 1, pr: "4px" }}>
                    <CleanupCard cleanup={cleanup} />
                  </Box>
                ))}
            </Box>
            {pageCount > 1 && (
              <Box sx={{ display: "flex", justifyContent: "center", pt: 1 }}>
                <Pagination
                  count={pageCount}
                  page={pageIndex}
                  onChange={handleChangePage}
                  color="white"
                  /* sx={{
                    ".MuiPagination-ul button": {
                      color: "#FFF",
                    },
                  }} */
                />
              </Box>
            )}
          </Stack>
        </Stack>
        <DialogFilterComponent
          open={showFilterMobile}
          onClose={() => setShowFilterMobile(false)}
          handleFilter={handleFilter}
          onResetFilter={onResetFilter}
          fieldValues={filterFieldValues}
          setFieldValues={setFilterFieldValues}
        />
      </Stack>
    </Box>
  );
};

export default Cleanups;
