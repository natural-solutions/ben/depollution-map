"use client";

import MapComponent from "@/components/map/MapComponent";
import { GET_CLEANUPS_FOR_APP } from "@/graphql/queries";
import { Cleanup, Cleanup_View } from "@/graphql/types";
import {
  CleanupFilters,
  DEFAULT_FILTER_VALUES,
  getFilteredFromURLCleanups,
  getFiltersFromURL,
} from "@/utils/cleanups";
import { MAP_INIT_POINT } from "@/utils/map";
import { useApi } from "@/utils/useApi";
import { Box, Button } from "@mui/material";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { add, format } from "date-fns";
import DialogFilterComponent from "@/components/filters/DialogFilterComponent";
import { FilterList } from "@mui/icons-material";

export default function Home() {
  const { postGraphql } = useApi();
  const pathnames = usePathname();
  const router = useRouter();
  const searchParams = useSearchParams();
  const searchFilters = searchParams.get("filters");

  const searchParamsString = searchParams.toString();
  const display = searchParams.get("display") || "split";

  const [filterFieldValues, setFilterFieldValues] = useState<CleanupFilters>(
    DEFAULT_FILTER_VALUES
  );
  const [cleanups, setCleanups] = useState<Cleanup_View[]>([]);
  const [cleanupsFiltered, setCleanupsFiltered] = useState<Cleanup_View[]>([]);
  const [addFilter, setAddFilter] = useState(false);
  const [mapViewState, setMapViewState] = useState(MAP_INIT_POINT);

  useEffect(() => {
    (async () => {
      const resp_cleanup = (
        await postGraphql<{
          data: {
            cleanup_view: Cleanup_View[];
          };
        }>({
          query: GET_CLEANUPS_FOR_APP,
          variables: {
            where: {
              start_at: {
                _lt: format(
                  add(new Date(), {
                    days: 1,
                  }),
                  "yyyy-MM-dd"
                ),
              },
              "cleanup_forms":{"status":{"_eq":"published"}},
            },
          },
        })
      ).data.data.cleanup_view;

      setCleanups(resp_cleanup);
    })();
  }, []);

  const filtersChangeListener = async () => {
    if (!cleanups) {
      return;
    }
    setCleanupsFiltered(
      await getFilteredFromURLCleanups({
        cleanups,
        searchFilters,
      })
    );
  };

  const handleFilter = () => {
    const params = new URLSearchParams(searchParamsString);

    params.set("filters", JSON.stringify(filterFieldValues));
    router.replace(`${pathnames}?${params.toString()}`);
    setAddFilter(false);
  };

  const closeFilters = () => {
    setAddFilter(false);
    setFilterFieldValues(getFiltersFromURL(searchFilters));
  };

  useEffect(() => {
    filtersChangeListener();
  }, [cleanups]);

  useEffect(() => {
    setFilterFieldValues(getFiltersFromURL(searchFilters));
    filtersChangeListener();
  }, [searchFilters]);

  const onResetFilter = () => {
    const params = new URLSearchParams(searchParamsString);

    params.delete("filters");
    router.replace(`${pathnames}?${params.toString()}`);
    setAddFilter(false);
  };

  return (
    <Box
      sx={{
        height: "100%",
        position: "relative",
        overflow: "hidden",
      }}
    >
      <DialogFilterComponent
        open={addFilter}
        onClose={() => closeFilters()}
        handleFilter={handleFilter}
        onResetFilter={onResetFilter}
        fieldValues={filterFieldValues}
        setFieldValues={setFilterFieldValues}
      />
      <Button
        startIcon={<FilterList />}
        variant="contained"
        sx={{ mb: 1, mt: 1, position: "absolute", zIndex: 1, left: "170px" }}
        onClick={() => setAddFilter(true)}
      >
        Filtres
      </Button>
      <MapComponent
        cleanups={cleanupsFiltered}
        viewState={mapViewState}
        setViewState={setMapViewState}
      />
    </Box>
  );
}
