import { FormatListBulleted } from "@mui/icons-material";
import { Box, Button } from "@mui/material";

export default function Profil() {
  return (
    <Box
      sx={{
        mb: 10,
        paddingX: 2,
        pt: 2,
      }}
    >
      <Button
        color="primary"
        href="/user/me/cleanups"
        variant="contained"
        endIcon={<FormatListBulleted />}
      >
        Mes ramassages
      </Button>
    </Box>
  );
}
