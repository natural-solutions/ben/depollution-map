"use client";

import { GET_CLEANUPS_FOR_APP } from "@/graphql/queries";
import { Cleanup_View } from "@/graphql/types";
import { useApi } from "@/utils/useApi";
import {
  Box,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Pagination,
  Stack,
  Tab,
  Tabs,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { FC, useEffect, useRef, useState } from "react";
import CleanupCard from "@/components/cleanup/CleanupCard";
import { USER_ROLE } from "@/utils/user";
import { MoreVert, Warning } from "@mui/icons-material";
import { StyledBadge } from "@/components/cleanup/CleanupTab";
import { useSessionContext } from "@/utils/useSessionContext";
import { ResourceDialog } from "@/components/resource/ResourceDialog";
import { getCleanupForm } from "@/utils/cleanups";

const PAGE_SIZE = 30;

const MyCleanups: FC = () => {
  const api = useApi();
  const router = useRouter();
  const sessionContext = useSessionContext();

  const params = useSearchParams();
  const cleanupsFormTab = params.get("cleanupsformtab");
  const page = Number(params.get("page")) || 1;

  const [isOpenResources, setIsOpenResources] = useState(false);
  const [cleanupsOwnByUser, setCleanupsOwnByUser] = useState<Cleanup_View[]>(
    []
  );
  const [cleanupsForm, setCleanupsForm] = useState<Cleanup_View[]>([]);
  const [valueByCategory, setValueByCategory] = useState({
    uncoming: 0,
    draft: 0,
    tocheck: 0,
    tovalide: 0,
  });
  const [userRole, setUserRole] = useState<string | null>(null);
  const [mobileMenuAnchor, setMobileMenuAnchor] =
    useState<HTMLButtonElement | null>(null);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  const pathname = usePathname();
  const tabContent = useRef<HTMLDivElement>();

  const handleCleanupsFormTabs = (_: React.ChangeEvent<{}>, value: string) => {
    router.replace(`${pathname}/?cleanupsformtab=${value}`);
  };

  const handleChangePage = (e: any, value: number) => {
    const searchParams = new URLSearchParams(params.toString());

    searchParams.set("page", String(value || 1));
    router.replace(`/profil?${searchParams.toString()}`);
  };

  const handleCloseMobileMenu = () => {
    setMobileMenuAnchor(null);
  };

  useEffect(() => {
    (isMobile ? window : tabContent.current)?.scrollTo({
      top: 0,
      behavior: "instant",
    });
  }, [page]);

  useEffect(() => {
    if (!sessionContext.session) {
      return;
    }
    (async () => {
      const user_id = sessionContext.session?.userView.id;
      const resp_cleanups = await api.postGraphql<{
        data: {
          cleanup_view: Cleanup_View[];
        };
      }>({
        query: GET_CLEANUPS_FOR_APP,
        variables: {
          where: {
            cleanup_campaigns: {
              campaign: {
                campaign_users: {
                  user_view_id: {
                    _eq: user_id,
                  },
                },
              },
            },
          },
          order_by: { start_at: "desc" },
        },
      });
      let cleanupsOwnByUser: Cleanup_View[] = [];
      if (resp_cleanups.data.data.cleanup_view) {
        cleanupsOwnByUser = resp_cleanups.data.data.cleanup_view.filter(
          (cleanup) => cleanup.owner_id === user_id
        );
      }
      setCleanupsOwnByUser(cleanupsOwnByUser);
      setCleanupsForm(resp_cleanups.data.data.cleanup_view);

      let draft = 0;
      let tocheck = 0;
      let tovalide = 0;
      let isCampaignAdmin = false;

      resp_cleanups.data.data.cleanup_view.forEach((cleanup) => {
        const status = getCleanupForm(cleanup)?.status;
        if (!status) {
          return;
        }
        if (status == "draft") {
          draft += 1;
        } else if (status == "tocheck") {
          tocheck += 1;
        }
        cleanup.cleanup_campaigns.forEach((cleanup_campaign) => {
          if (
            !sessionContext.getHasCampaignRole(
              cleanup_campaign.campaign_id,
              USER_ROLE.ADMIN
            )
          ) {
            return;
          }
          isCampaignAdmin = true;
          if (status == "tocheck") {
            tovalide += 1;
          }
        });
      });

      setUserRole(isCampaignAdmin ? "missionOfficer" : "missionVolunteer");

      setValueByCategory({
        uncoming: cleanupsOwnByUser.length,
        draft: draft,
        tocheck: tocheck,
        tovalide: tovalide,
      });
    })();
  }, []);

  const cleanupsFormToDisplay = cleanupsForm.filter((cleanup) => {
    const status = getCleanupForm(cleanup)?.status;
    if (!status) {
      return false;
    }

    if (userRole === "missionOfficer" && status == "tocheck") {
      return cleanup.cleanup_campaigns.some((cleanup_campaign) => {
        if (
          sessionContext.getHasCampaignRole(
            cleanup_campaign.campaign_id,
            USER_ROLE.ADMIN
          )
        ) {
          return true;
        } else {
          return false;
        }
      });
    }
    if (userRole === "missionVolunteer") {
      if (!cleanupsFormTab || cleanupsFormTab === "saved") {
        if (status == "draft") {
          return true;
        }
      } else if (status == "tocheck") {
        return true;
      }
    }
  });

  const pageCount = Math.ceil(cleanupsFormToDisplay.length / PAGE_SIZE);

  return (
    <>
      <Stack
        sx={{
          pt: 1,
          ...(isMobile
            ? {
                px: 1,
                height: "calc(100% - 56px)",
                overflowX: "hidden",
                overflowY: "auto",
                pb: "80px",
              }
            : {
                flex: 1,
                height: "100%",
                overflow: "hidden",
                px: 2,
                pb: 1,
              }),
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            pb: 2,
          }}
        >
          <Typography
            variant="h3"
            sx={{
              fontSize: {
                xs: "20px",
                md: "34px",
              },
            }}
          >
            Bienvenue {sessionContext.session?.user.name} !
          </Typography>
          {!isMobile &&
            (valueByCategory.tovalide > 0 ||
              (valueByCategory.draft > 0 && userRole !== "missionOfficer")) && (
              <Box
                sx={{
                  border: 1,
                  borderColor: "#F59731",
                  borderRadius: 1,
                  background: "#FEF2E2",
                  display: "flex",
                  boxShadow: 6,
                  p: 3,
                }}
              >
                <Warning sx={{ color: "#F59731" }} />
                <Typography sx={{ mt: "3px", ml: 1 }} variant="body2">
                  {userRole === "missionVolunteer" &&
                    `Vous avez ${valueByCategory.draft} caractérisation${
                      valueByCategory.draft > 1 ? "s" : ""
                    } enregistrée${
                      valueByCategory.draft > 1 ? "s" : ""
                    }. Pensez à les soumettre.`}
                  {userRole === "missionOfficer" &&
                    `Vous avez ${valueByCategory.tovalide} caractérisation${
                      valueByCategory.tovalide > 1 ? "s" : ""
                    } à valider.`}
                </Typography>
              </Box>
            )}
          {isMobile ? (
            <>
              <IconButton
                color="primary"
                onClick={(e) => {
                  setMobileMenuAnchor(e.currentTarget as any);
                }}
              >
                <MoreVert />
              </IconButton>
              <Menu
                anchorEl={mobileMenuAnchor}
                open={Boolean(mobileMenuAnchor)}
                onClose={handleCloseMobileMenu}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
                sx={{
                  "& .MuiMenu-paper": {
                    borderRadius: "3px",
                  },
                }}
              >
                <MenuItem
                  onClick={() => {
                    handleCloseMobileMenu();
                    setIsOpenResources(true);
                  }}
                >
                  Afficher les ressources
                </MenuItem>
              </Menu>
            </>
          ) : (
            <Box sx={{ display: "flex", justifyContent: "center" }}>
              <Button
                onClick={() => setIsOpenResources(true)}
                color="primary"
                variant="contained"
                sx={{ minHeight: 40, maxHeight: 40, borderRadius: 2 }}
              >
                Afficher les ressources
              </Button>
            </Box>
          )}

          <ResourceDialog
            isOpenResources={isOpenResources}
            setIsOpenResources={setIsOpenResources}
          />
        </Box>
        <Stack
          direction={{
            xs: "column",
            md: "row",
          }}
          spacing={{
            xs: 3,
            md: 1,
          }}
          sx={
            isMobile
              ? {
                  overflow: "visible",
                }
              : {
                  flex: 1,
                  overflow: "hidden",
                  maxWidth: "1350px",
                  justifyContent: "space-between",
                }
          }
        >
          <Stack
            sx={{
              width: {
                md: "580px",
              },
              px: "2px",
              overflow: "hidden",
            }}
          >
            <Typography variant="h4" sx={{ margin: 1 }}>
              Mes ramassages{" "}
              <StyledBadge
                badgeContent={valueByCategory.uncoming}
                showZero
                sx={{ marginLeft: 3 }}
              />
            </Typography>
            <Stack
              direction={{
                xs: "row",
                md: "column",
              }}
              sx={{
                width: "100%",
                overflow: "auto",
                flexWrap: "nowrap",
              }}
            >
              {cleanupsOwnByUser.length ? (
                <>
                  {cleanupsOwnByUser.map((cleanup) => {
                    return (
                      <Box key={cleanup!.id} sx={{ mb: 1, pr: "4px" }}>
                        <CleanupCard cleanup={cleanup!} />
                      </Box>
                    );
                  })}
                </>
              ) : (
                <Typography variant="body2" sx={{ m: 1, mt: 3 }}>
                  Aucun ramassage à venir.
                </Typography>
              )}
            </Stack>
          </Stack>

          <Stack
            sx={{
              width: {
                md: "580px",
              },
              height: {
                md: "100%",
              },
              overflow: {
                md: "hidden",
              },
            }}
          >
            <Typography variant="h4" sx={{ margin: 1 }}>
              {isMobile &&
                (valueByCategory.tovalide > 0 ||
                  (valueByCategory.draft > 0 &&
                    userRole !== "missionOfficer")) && (
                  <Warning
                    sx={{ color: "#F59731", mr: 1, verticalAlign: "middle" }}
                  />
                )}
              Caractérisations {userRole === "missionOfficer" && "à valider "}
              {userRole === "missionOfficer" && (
                <StyledBadge
                  badgeContent={valueByCategory.tovalide}
                  showZero
                  sx={{ marginLeft: 3 }}
                />
              )}
            </Typography>
            {userRole === "missionVolunteer" && (
              <Tabs
                value={cleanupsFormTab || "saved"}
                onChange={handleCleanupsFormTabs}
                sx={{
                  borderBottom: 1,
                  borderColor: "divider",
                  fontSize: "14px",
                  background: "#F0EFEC",
                  zIndex: 1,
                  position: {
                    xs: "sticky",
                    md: "static",
                  },
                  top: {
                    xs: "-8px",
                    md: "auto",
                  },
                }}
              >
                <Tab
                  label={
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        width: "100%",
                        justifyContent: "center",
                      }}
                    >
                      Enregistrées{" "}
                      <StyledBadge
                        badgeContent={valueByCategory.draft}
                        showZero
                        sx={{ marginLeft: 3 }}
                      />
                    </Box>
                  }
                  value="saved"
                  sx={{ minWidth: "50%" }}
                />
                <Tab
                  label={
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        width: "100%",
                        justifyContent: "center",
                      }}
                    >
                      En attente{" "}
                      <StyledBadge
                        badgeContent={valueByCategory.tocheck}
                        showZero
                        sx={{ marginLeft: 3 }}
                      />
                    </Box>
                  }
                  value="to_check"
                  sx={{ minWidth: "50%" }}
                />
              </Tabs>
            )}
            <Box
              ref={tabContent}
              sx={{
                pt: 1,
                flex: {
                  md: 1,
                },
                overflowY: {
                  md: "auto",
                },
              }}
            >
              {cleanupsFormToDisplay
                .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE)
                .map((cleanup) => (
                  <Box key={cleanup.id} sx={{ mb: 1, pr: "4px" }}>
                    <CleanupCard cleanup={cleanup!} />
                  </Box>
                ))}
            </Box>
            {pageCount > 1 && (
              <Box sx={{ display: "flex", justifyContent: "center", pt: 1 }}>
                <Pagination
                  count={pageCount}
                  page={page}
                  onChange={handleChangePage}
                  color="primary"
                />
              </Box>
            )}
          </Stack>
        </Stack>
      </Stack>
    </>
  );
};

export default MyCleanups;
