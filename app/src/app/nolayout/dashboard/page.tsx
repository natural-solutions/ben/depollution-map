"use client";

import { GET_CLEANUPS_FOR_APP } from "@/graphql/queries";
import { Cleanup_View } from "@/graphql/types";
import { useApi } from "@/utils/useApi";
import { Box } from "@mui/material";
import { useEffect, useState } from "react";
import { add, format } from "date-fns";
import { DashboardComponent } from "../../../components/dashboard/DashboardComponent";

export default function NoLayoutDashboard() {
  const { postGraphql } = useApi();

  const [cleanups, setCleanups] = useState<Cleanup_View[]>([]);

  useEffect(() => {
    (async () => {
      const resp_cleanup = (
        await postGraphql<{
          data: {
            cleanup_view: Cleanup_View[];
          };
        }>({
          query: GET_CLEANUPS_FOR_APP,
          variables: {
            where: {
              start_at: {
                _lt: format(
                  add(new Date(), {
                    days: 1,
                  }),
                  "yyyy-MM-dd"
                ),
              },
              "cleanup_forms":{"status":{"_eq":"published"}},
            },
          },
        })
      ).data.data.cleanup_view;

      setCleanups(resp_cleanup);
    })();
  }, []);

  return (
    <Box
      sx={{
        height: "100%",
        position: "relative",
        overflow: "auto",
      }}
    >
      <DashboardComponent
        title=""
        cleanups={cleanups}
        isOnlyDashboard={false}
      />
    </Box>
  );
}
