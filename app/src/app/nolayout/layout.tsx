"use client";

import { ThemeProvider, createTheme } from "@mui/material";
import { PropsWithChildren } from "react";
import { themeOptions } from "@/utils/theme";

const customTheme = createTheme(themeOptions);

export default function PublicLayout(props: PropsWithChildren) {
  return <ThemeProvider theme={customTheme}>{props.children}</ThemeProvider>;
}
