"use client";

import { GET_CLEANUPS_FOR_APP } from "@/graphql/queries";
import { Cleanup_View } from "@/graphql/types";
import { MAP_INIT_POINT } from "@/utils/map";
import { useApi } from "@/utils/useApi";
import { Box } from "@mui/material";
import { useEffect, useState } from "react";
import { add, format } from "date-fns";
import MapComponent from "../../../components/map/MapComponent";

export default function NoLayoutMap() {
  const { postGraphql } = useApi();

  const [cleanups, setCleanups] = useState<Cleanup_View[]>([]);
  const [mapViewState, setMapViewState] = useState(MAP_INIT_POINT);

  useEffect(() => {
    (async () => {
      const resp_cleanup = (
        await postGraphql<{
          data: {
            cleanup_view: Cleanup_View[];
          };
        }>({
          query: GET_CLEANUPS_FOR_APP,
          variables: {
            where: {
              start_at: {
                _lt: format(
                  add(new Date(), {
                    days: 1,
                  }),
                  "yyyy-MM-dd"
                ),
              },
              "cleanup_forms":{"status":{"_eq":"published"}},
            },
          },
        })
      ).data.data.cleanup_view;

      setCleanups(resp_cleanup);
    })();
  }, []);

  return (
    <Box
      sx={{
        height: "100%",
        position: "relative",
        overflow: "hidden",
      }}
    >
      <MapComponent
        cleanups={cleanups}
        viewState={mapViewState}
        setViewState={setMapViewState}
      />
    </Box>
  );
}
