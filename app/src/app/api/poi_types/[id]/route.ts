import { GET_POI_TYPE, UPDATE_POI_TYPE } from "@/graphql/queries";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { removeFromMinioAndImagor } from "@/utils/minio";
import { getFilePath } from "@/utils/media";
import { getValidServerSession } from "@/utils/session";
import { Poi_Type_Set_Input } from "@/graphql/types";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const _set: Poi_Type_Set_Input = await req.json();

  try {
    const resp_poi_type = await dangerousPostAsAdmin({
      query: GET_POI_TYPE,
      variables: { id },
    });

    if (!resp_poi_type.data.poi_type_by_pk) {
      return Response.json({}, { status: 404 });
    }

    const logo = resp_poi_type.data.poi_type_by_pk.logo;

    if ((logo && !_set.logo) || logo !== _set.logo) {
      await removeFromMinioAndImagor(
        process.env,
        getFilePath("poi_types", id, logo)
      );
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_POI_TYPE,
      variables: { id, _set },
    });

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
