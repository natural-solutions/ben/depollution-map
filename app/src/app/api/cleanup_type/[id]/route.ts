import {
  UPDATE_CLEANUP_TYPE,
  UPDATE_CLEANUP_TYPE_RUBBISHES,
} from "@/graphql/queries";
import pick from "lodash/pick";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { Cleanup_Type } from "@/graphql/types";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const payload: Cleanup_Type = await req.json();

  const manyToManies = [
    {
      name: "cleanup_type_rubbishes",
      query: UPDATE_CLEANUP_TYPE_RUBBISHES,
      objects: payload?.cleanup_type_rubbishes
        ?.filter((row) => row.rubbish_id && (row.has_qty || row.has_wt_vol))
        .map((row) => ({
          ...pick(row, ["rubbish_id", "has_qty", "has_wt_vol"]),
          cleanup_type_id: id,
        })),
    },
  ];

  try {
    for (const manyToMany of manyToManies) {
      if (manyToMany.objects) {
        await dangerousPostAsAdmin({
          query: manyToMany.query,
          variables: {
            id,
            objects: manyToMany.objects,
          },
        });
      }

      delete (payload as any)?.[manyToMany.name];
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_CLEANUP_TYPE,
      variables: {
        id,
        _set: payload,
      },
    });

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
