import { GET_PARTNER, UPDATE_PARTNER } from "@/graphql/queries";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { removeFromMinioAndImagor } from "@/utils/minio";
import { getFilePath } from "@/utils/media";
import { getValidServerSession } from "@/utils/session";
import { Partner_Set_Input } from "@/graphql/types";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const _set: Partner_Set_Input = await req.json();

  try {
    const resp_partner = await dangerousPostAsAdmin({
      query: GET_PARTNER,
      variables: { id },
    });

    if (!resp_partner.data.partner_by_pk) {
      return Response.json({}, { status: 404 });
    }

    const logo = resp_partner.data.partner_by_pk.logo;

    if ((logo && !_set.logo) || logo !== _set.logo) {
      await removeFromMinioAndImagor(
        process.env,
        getFilePath("partners", id, logo)
      );
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_PARTNER,
      variables: { id, _set },
    });

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
