import {
  RubbishPatch,
  UPDATE_RUBBISH,
  UPDATE_RUBBISH_CLEANUP_TYPES,
  UPDATE_RUBBISH_TAGS,
} from "@/graphql/queries";
import pick from "lodash/pick";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const _set: RubbishPatch = await req.json();

  const manyToManies = [
    {
      name: "tags",
      query: UPDATE_RUBBISH_TAGS,
      objects: _set?.tags?.map((row) => ({
        ...pick(row, ["rubbish_tag_id"]),
        rubbish_id: id,
      })),
    },
    {
      name: "cleanup_type_rubbishes",
      query: UPDATE_RUBBISH_CLEANUP_TYPES,
      objects: _set?.cleanup_type_rubbishes
        ?.filter(
          (row) => row.cleanup_type_id && (row.has_qty || row.has_wt_vol)
        )
        .map((row) => ({
          ...pick(row, ["cleanup_type_id", "has_qty", "has_wt_vol"]),
          rubbish_id: id,
        })),
    },
  ];

  try {
    for (const manyToMany of manyToManies) {
      if (manyToMany.objects) {
        await dangerousPostAsAdmin({
          query: manyToMany.query,
          variables: {
            id,
            objects: manyToMany.objects,
          },
        });
      }

      delete (_set as any)?.[manyToMany.name];
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_RUBBISH,
      variables: {
        id,
        _set,
      },
    });

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
