import { UPDATE_CHARACTERIZATION_LEVEL } from "@/graphql/queries";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { Characterization_Level } from "@/graphql/types";
import { USER_ROLE } from "@/utils/user";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN],
  });
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const payload: Characterization_Level = await req.json();

  try {
    const resp = await dangerousPostAsAdmin({
      query: UPDATE_CHARACTERIZATION_LEVEL,
      variables: {
        id,
        _set: payload,
      },
    });

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
