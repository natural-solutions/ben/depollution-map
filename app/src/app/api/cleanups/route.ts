import { dangerousPostAsAdmin } from "@/graphql/client";
import { INSERT_CLEANUP } from "@/graphql/queries";
import { Cleanup, Cleanup_Insert_Input } from "@/graphql/types";
import { getValidServerSession } from "@/utils/session";
import { USER_ROLE } from "@/utils/user";
import { pick } from "lodash";
import { NextRequest } from "next/server";

export async function POST(req: NextRequest) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const payload = (await req.json()) as Cleanup;

  if (session.userView.role_name != USER_ROLE.ADMIN) {
    for (const cleanup_campaign of payload.cleanup_campaigns || []) {
      const exists = session.userView.campaign_users.some(
        (campaign_user) =>
          campaign_user.campaign_id == cleanup_campaign.campaign_id &&
          [USER_ROLE.ADMIN, USER_ROLE.EDITOR].includes(
            campaign_user.role as USER_ROLE
          )
      );
      if (!exists) {
        return Response.json({}, { status: 403 });
      }
    }
  }

  const resp = await dangerousPostAsAdmin({
    query: INSERT_CLEANUP,
    variables: {
      _set: {
        ...(pick(payload, [
          "area_type_id",
          "city_id",
          "cleanup_type_id",
          "cleanup_place_id",
          "environment_type_id",
          "label",
          "start_at",
          "start_point",
          "total_area",
          "total_linear",
          "description",
        ]) as any),
        owner_id: session.userView.id,
        cleanup_partners: {
          data: payload.cleanup_partners || [],
        },
        cleanup_areas: {
          data:
            payload.cleanup_areas?.map((area) => {
              delete area.id;
              delete area.cleanup_id;

              if (area.geom) {
                area.geom.id = Math.random().toString(32).replaceAll(".", "8b");
              }
              return area;
            }) || [],
        },
        cleanup_photos: {
          data: payload.cleanup_photos || [],
        },
        cleanup_campaigns: {
          data:
            payload.cleanup_campaigns
              ?.filter((row) => row.campaign_id)
              .map(({ campaign_id }) => ({
                campaign_id,
              })) || [],
        },
      } as Cleanup_Insert_Input,
    },
  });

  if (resp.errors) {
    return Response.json(resp, { status: 400 });
  }

  return Response.json(resp.data.insert_cleanup_one);
}
