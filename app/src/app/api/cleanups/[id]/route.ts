import {
  UPDATE_CLEANUP,
  UPSERT_CLEANUP_AREA,
  UPDATE_CLEANUP_CAMPAIGNS,
  DELETE_CLEANUP_AREAS_BY_IDS,
  DELETE_CLEANUP_PHOTOS_BY_IDS,
  UPSERT_CLEANUP_PHOTO,
  CleanupPatch,
  GET_CLEANUP_PHOTOS_MANY,
  UPDATE_CLEANUP_PARTNERS,
  GET_CLEANUP,
} from "@/graphql/queries";
import { dangerousPostAsAdmin } from "@/graphql/client";
import pick from "lodash/pick";
import {
  Cleanup,
  CleanupCleanup_PhotosArgs,
  Cleanup_Photo,
} from "@/graphql/types";
import { removeFromMinioAndImagor } from "@/utils/minio";
import { getFilePath } from "@/utils/media";
import { getValidServerSession } from "@/utils/session";
import { USER_ROLE, getCanEditCleanup } from "@/utils/user";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN, USER_ROLE.EDITOR],
  });

  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const _set: CleanupPatch = await req.json();

  const cleanup: Cleanup = (
    await dangerousPostAsAdmin({
      query: GET_CLEANUP,
      variables: {
        id,
      },
    })
  ).data.cleanup_by_pk;

  if (!cleanup) {
    return Response.json({}, { status: 404 });
  }

  if (!getCanEditCleanup(session.userView, cleanup)) {
    return Response.json({}, { status: 403 });
  }

  const nesteds = [
    {
      name: "cleanup_areas",
      deleteQuery: DELETE_CLEANUP_AREAS_BY_IDS,
      upsertQuery: UPSERT_CLEANUP_AREA,
      deleteIds: _set?.cleanup_areas_delete_ids,
      upserts: _set?.cleanup_areas?.map((row) => ({
        ...pick(row, ["id", "label", "fill_color", "geom", "size"]),
        cleanup_id: id,
      })),
    },
    {
      name: "cleanup_photos",
      deleteQuery: DELETE_CLEANUP_PHOTOS_BY_IDS,
      upsertQuery: UPSERT_CLEANUP_PHOTO,
      deleteIds: _set?.cleanup_photos_delete_ids,
      upserts: _set?.cleanup_photos?.map((row) => ({
        ...pick(row, ["id", "filename", "comment", "cleanup_area_id", "rank"]),
        cleanup_id: id,
      })),
    },
    {
      name: "cleanup_campaigns",
      query: UPDATE_CLEANUP_CAMPAIGNS,
      objects: _set?.cleanup_campaigns?.map((row) => ({
        ...pick(row, ["campaign_id"]),
        cleanup_id: id,
      })),
    },
    {
      name: "cleanup_partners",
      query: UPDATE_CLEANUP_PARTNERS,
      objects: _set?.cleanup_partners?.map((row) => ({
        ...pick(row, ["partner_id"]),
        cleanup_id: id,
      })),
    },
  ];

  try {
    if (_set?.cleanup_photos_delete_ids?.length) {
      const respDeletedPhotos = await dangerousPostAsAdmin({
        query: GET_CLEANUP_PHOTOS_MANY,
        variables: {
          where: {
            id: {
              _in: _set.cleanup_photos_delete_ids,
            },
          },
        } as CleanupCleanup_PhotosArgs,
      });
      (respDeletedPhotos.data?.cleanup_photo as Cleanup_Photo[])?.forEach(
        (photo) => {
          removeFromMinioAndImagor(
            process.env,
            getFilePath("cleanups", id, photo.filename)
          );
        }
      );
    }

    for (const nested of nesteds) {
      if (nested.query) {
        if (nested.objects) {
          await dangerousPostAsAdmin({
            query: nested.query,
            variables: {
              id,
              objects: nested.objects,
            },
          });
        }
      } else {
        const deleteIds = nested.deleteIds;
        if (deleteIds?.length) {
          await dangerousPostAsAdmin({
            query: nested.deleteQuery!,
            variables: {
              ids: deleteIds,
            },
          });
        }

        const objects = nested.upserts;
        if (objects?.length) {
          await dangerousPostAsAdmin({
            query: nested.upsertQuery!,
            variables: {
              objects,
            },
          });
        }
      }
      delete (_set as any)?.[nested.name];
      delete (_set as any)?.[`${nested.name}_delete_ids`];
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_CLEANUP,
      variables: {
        id,
        _set,
      },
    });

    if (resp.errors) {
      return Response.json(resp, { status: 400 });
    }
    return Response.json(resp);
  } catch (error) {
    console.error("\n\n\n", error, "\n\n\n");
    return Response.json({ error }, { status: 500 });
  }
}
