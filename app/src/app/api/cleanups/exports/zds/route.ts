import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { USER_ROLE, getIsAdminInCampaign } from "@/utils/user";
import { parse } from "url";
import { add, format } from "date-fns";
import { pick, trim } from "lodash";
import { Cleanup, Cleanup_Bool_Exp } from "@/graphql/types";
import { NextResponse } from "next/server";
import { JsonParseSafe, convertTimeToDecimal, getHundredthFormat } from "@/utils/utils";
import * as ExcelJS from "exceljs";

export async function GET(req: Request) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN, USER_ROLE.EDITOR],
  });

  if (!session) {
    return errorResp;
  }

  if (!getIsAdminInCampaign(session.userView)) {
    return Response.json(
      { message: "User is not an admin in the campaign" },
      { status: 403 }
    );
  }

  const variables =
    JsonParseSafe(parse(req.url, true).query.variables as string) || {};

  if (!variables.where?._and) {
    variables.where = { _and: variables.where ? [variables.where] : [] };
  }

  variables.where._and.push({
    cleanup_forms: {
      id: { _is_null: false },
    },
    start_at: {
      _is_null: false,
      _gt: "2000-01-01",
      _lt: format(add(new Date(), { days: 1 }), "yyyy-MM-dd"),
    },
  } as Cleanup_Bool_Exp);

  const cleanups: Cleanup[] = (
    await dangerousPostAsAdmin({
      query: getQuery(),
      variables,
    })
  ).data?.cleanup;

  const rows = cleanups.map((cleanup) => {
    const start_at = format(new Date(cleanup.start_at), "dd/MM/yyyy");
    const cleanupForm = cleanup.cleanup_forms[0];
    const agg_rubbish_categories: {
      [key: string]: {
        sum_quantity?: number | null;
        sum_volume: number;
        sum_weight: number;
      };
    } = {};
    cleanup.cleanup_agg_rubbish_category_views.forEach(
      ({ rubbish_category_id, sum_quantity, sum_volume, sum_weight }) => {
        agg_rubbish_categories[rubbish_category_id!] = {
          sum_quantity,
          sum_volume,
          sum_weight,
        };
      }
    );
    const zdsRubbishes: string[] = [];
    const otherRubbishes: string[] = [];
    cleanupForm.cleanup_form_rubbishes.forEach(({ quantity, rubbish }) => {
      if (!quantity) {
        return;
      }
      if (rubbish.id_zds) {
        zdsRubbishes.push(`${rubbish.id_zds}:${quantity}`);
      } else {
        otherRubbishes.push(`${rubbish.label}:${quantity}`);
      }
    });
    const brands: string[] = cleanupForm.cleanup_form_rubbish_details
      .filter(({ quantity, brand_id }) => quantity && brand_id)
      .map(({ quantity, brand }) => `${brand?.label}:${quantity}`);

    return {
      STRUCTURE_NOM: "Wings of the Ocean",
      STRUCTURE_ID: "146",
      EV_NOM: cleanup.label,
      EV_DESCR:
        trim(cleanup.description || "") ||
        "Ramassage réalisé par l'association Wings of the Ocean",
      EV_DATE_DEBUT: start_at,
      EV_DATE_FIN: start_at,
      EV_POINT_GPS_LAT: cleanup.start_point?.lat,
      EV_POINT_GPS_LON: cleanup.start_point?.lng,
      EV_NIVEAU: cleanup.cleanup_type?.characterization_level_id,
      CONTEXTE_TYPE_MILIEU_UUID: cleanup.environment_type?.id_zds,
      CONTEXTE_TYPE_LIEU_UUID: cleanup.area_type?.id_zds,
      CONTEXTE_TYPE_DECHET_UUID: cleanupForm.rubbish_type_picked?.id_zds,
      CONTEXTE_NOM_ZONE: cleanup.cleanup_place?.label,
      CONTEXTE_DUREE: convertTimeToDecimal(cleanupForm.cleanup_duration),
      CONTEXTE_PARTICIPANT_ORGA: cleanupForm.volunteers_number,
      CONTEXTE_PARTICIPANT_EXT: cleanupForm.external_volunteers_number,
      CONTEXTE_GEOMETRIE: {
        type: "FeatureCollection",
        features: cleanup.cleanup_areas?.map(({ geom }) => {
          return pick(geom, ["type", "geometry", "properties"]);
        }),
      },
      DATA_GLOBAL_VOL_TOTAL: getHundredthFormat(cleanup.total_volume),
      DATA_GLOBAL_VOL_PLASTIQUE:
        getHundredthFormat(agg_rubbish_categories["plastic"]?.sum_volume) || 0,
      DATA_GLOBAL_VOL_BOIS:
        getHundredthFormat(agg_rubbish_categories["wood"]?.sum_volume) || 0,
      DATA_GLOBAL_VOL_TEXTILE:
        getHundredthFormat(agg_rubbish_categories["textile"]?.sum_volume) || 0,
      DATA_GLOBAL_VOL_PAPIER:
        getHundredthFormat(agg_rubbish_categories["paper_board"]?.sum_volume) ||
        0,
      DATA_GLOBAL_VOL_METAL:
        getHundredthFormat(agg_rubbish_categories["metal"]?.sum_volume) || 0,
      DATA_GLOBAL_VOL_VERRE:
        getHundredthFormat(agg_rubbish_categories["glass"]?.sum_volume) || 0,
      DATA_GLOBAL_VOL_AUTRE:
        getHundredthFormat(agg_rubbish_categories["other_waste"]?.sum_volume) ||
        0,
      DATA_SACS_VOL_TOTAL: 0,
      DATA_DV_VOL_TOTAL: 0,
      DATA_SACS_POIDS_TOTAL: getHundredthFormat(cleanup.total_weight),
      DATA_SACS_POIDS_PLASTIQUE:
        getHundredthFormat(agg_rubbish_categories["plastic"]?.sum_weight) || 0,
      DATA_SACS_POIDS_BOIS:
        getHundredthFormat(agg_rubbish_categories["wood"]?.sum_weight) || 0,
      DATA_SACS_POIDS_TEXTILE:
        getHundredthFormat(agg_rubbish_categories["textile"]?.sum_weight) || 0,
      DATA_SACS_POIDS_PAPIER:
        getHundredthFormat(agg_rubbish_categories["paper_board"]?.sum_weight) ||
        0,
      DATA_SACS_POIDS_METAL:
        getHundredthFormat(agg_rubbish_categories["metal"]?.sum_weight) || 0,
      DATA_SACS_POIDS_VERRE:
        getHundredthFormat(agg_rubbish_categories["glass"]?.sum_weight) || 0,
      DATA_SACS_POIDS_AUTRE:
        getHundredthFormat(agg_rubbish_categories["other_waste"]?.sum_weight) ||
        0,
      DATA_DECHETS_INDICATEURS: zdsRubbishes.join(","),
      DATA_DECHETS_AUTRES: otherRubbishes.join(","),
      DATA_DECHETS_MARQUES: brands.join(","),
    };
  });

  const workbook = new ExcelJS.Workbook();
  const worksheet = workbook.addWorksheet(
    `Export ${format(new Date(), "dd-MM-yyyy")}`,
    {
      properties: { defaultColWidth: 16 },
    }
  );
  worksheet.columns = getCols().map((key) => ({
    key,
    header: key,
  }));
  worksheet.addRows(rows);

  const buffer = await workbook.csv.writeBuffer({
    formatterOptions: {
      delimiter: ";",
      quote: '"',
    },
  });

  const res = new NextResponse("\ufeff" + buffer);
  res.headers.set("content-type", "application/csv");
  res.headers.set(
    "Content-Disposition",
    'attachment;filename="export-zds.csv"'
  );

  return res;
}

function getQuery() {
  return `query Export($order_by: [cleanup_order_by!] = {}, $where: cleanup_bool_exp = {}) {
    cleanup(order_by: $order_by, where: $where) {
      label
      description
      cleanup_type {
        characterization_level_id
      }
      total_weight
      total_volume
      start_at
      start_point
      environment_type {
        id_zds
      }
      area_type {
        id_zds
      }
      cleanup_areas {
        geom
      }
      cleanup_place {
        label
      }
      cleanup_forms {
        cleanup_duration
        volunteers_number
        external_volunteers_number
        cleanup_form_rubbishes {
          quantity
          volume
          weight
          rubbish_id
          rubbish {
            label
            id_zds
          }
        }
        cleanup_form_rubbish_details(where: {
          brand_id: { _is_null: false },
          rubbish_id: { _is_null: false },
          quantity: { _is_null: false }
        }) {
          brand_id
          brand {
            label
          }
          rubbish_id
          quantity
        }
        rubbish_type_picked {
          id_zds
        }
      }
      cleanup_agg_rubbish_category_views {
        rubbish_category_id
        sum_quantity
        sum_volume
        sum_weight
        ratio_weight
      }
    }
  }`;
}

function getCols() {
  return [
    "STRUCTURE_NOM",
    "STRUCTURE_ID",
    "EV_NOM",
    "EV_DESCR",
    "EV_DATE_DEBUT",
    "EV_DATE_FIN",
    "EV_POINT_GPS_LAT",
    "EV_POINT_GPS_LON",
    "EV_NIVEAU",
    /* "EV_ENVERGURE_ID",
    "EV_INITIATIVES_IDS",
    "EV_SPOT_NOM",
    "EV_SPOT_ID", */
    "CONTEXTE_TYPE_MILIEU_UUID",
    "CONTEXTE_TYPE_LIEU_UUID",
    "CONTEXTE_TYPE_DECHET_UUID",
    "CONTEXTE_NOM_ZONE",
    "CONTEXTE_DUREE",
    "CONTEXTE_PARTICIPANT_ORGA",
    "CONTEXTE_PARTICIPANT_EXT",
    "CONTEXTE_GEOMETRIE",
    "DATA_GLOBAL_VOL_TOTAL",
    "DATA_GLOBAL_VOL_PLASTIQUE",
    //"DATA_GLOBAL_VOL_CAOUTCHOUC",
    "DATA_GLOBAL_VOL_BOIS",
    "DATA_GLOBAL_VOL_TEXTILE",
    "DATA_GLOBAL_VOL_PAPIER",
    "DATA_GLOBAL_VOL_METAL",
    "DATA_GLOBAL_VOL_VERRE",
    "DATA_GLOBAL_VOL_AUTRE",
    /* "DATA_SACS_VOL_TOTAL",
    "DATA_SACS_VOL_PLASTIQUE",
    "DATA_SACS_VOL_CAOUTCHOUC",
    "DATA_SACS_VOL_BOIS",
    "DATA_SACS_VOL_TEXTILE",
    "DATA_SACS_VOL_PAPIER",
    "DATA_SACS_VOL_METAL",
    "DATA_SACS_VOL_VERRE",
    "DATA_SACS_VOL_AUTRE",
    "DATA_DV_VOL_TOTAL",
    "DATA_DV_VOL_PLASTIQUE",
    "DATA_DV_VOL_CAOUTCHOUC",
    "DATA_DV_VOL_BOIS",
    "DATA_DV_VOL_TEXTILE",
    "DATA_DV_VOL_PAPIER",
    "DATA_DV_VOL_METAL",
    "DATA_DV_VOL_VERRE",
    "DATA_DV_VOL_AUTRE", */
    "DATA_SACS_POIDS_TOTAL",
    "DATA_SACS_POIDS_PLASTIQUE",
    //"DATA_SACS_POIDS_CAOUTCHOUC",
    "DATA_SACS_POIDS_BOIS",
    "DATA_SACS_POIDS_TEXTILE",
    "DATA_SACS_POIDS_PAPIER",
    "DATA_SACS_POIDS_METAL",
    "DATA_SACS_POIDS_VERRE",
    "DATA_SACS_POIDS_AUTRE",
    "DATA_DECHETS_INDICATEURS",
    "DATA_DECHETS_AUTRES",
    "DATA_DECHETS_MARQUES",
    /* "COMMENTAIRE_DV",
    "COMMENTAIRE_GENERAL", */
  ];
}
