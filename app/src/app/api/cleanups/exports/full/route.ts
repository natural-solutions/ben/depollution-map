import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { USER_ROLE, getIsAdminInCampaign } from "@/utils/user";
import { parse } from "url";
import { NextResponse } from "next/server";
import { JsonParseSafe } from "@/utils/utils";

export async function GET(req: Request) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN, USER_ROLE.EDITOR],
  });

  if (!session) {
    return errorResp;
  }

  if (!getIsAdminInCampaign(session.userView)) {
    return Response.json(
      { message: "User must be admin in at least one campaign" },
      { status: 403 }
    );
  }

  const variables =
    JsonParseSafe(parse(req.url, true).query.variables as string) || {};

  const cleanups = await dangerousPostAsAdmin(
    {
      query: getQuery(),
      variables,
    },
    "blob"
  );

  const res = new NextResponse(cleanups);
  res.headers.set("content-type", "application/json");
  res.headers.set(
    "Content-Disposition",
    'attachment;filename="export-cleanups-full.json"'
  );

  return res;
}

function getQuery() {
  return `query Export($order_by: [cleanup_order_by!] = {}, $where: cleanup_bool_exp = {}) {
    cleanups: cleanup(order_by: $order_by, where: $where) {
      cleanup_campaigns {
        campaign {
          id
          label
        }
      }
      cleanup_type {
        id
        label
        characterization_level_id
      }
      cleanup_partners {
        partner {
          id
          label
        }
      }
      city {
        departement {
          id
          label
          region {
            code
            label
          }
        }
        id
        label
        code
        slug
      }
      cleanup_areas(order_by: {label: asc}) {
        geom
        id
        label
        fill_color
        size
      }
      cleanup_photos(order_by: {rank: asc}) {
        id
        filename
        cleanup_area_id
      }
      environment_type {
        id
        label
      }
      area_type {
        id
        label
      }
      cleanup_forms(order_by: {created_at: asc}) {
        cleanup_form_rubbishes(distinct_on: [rubbish_id], order_by: [{rubbish_id: asc}, {created_at: desc}]) {
          rubbish {
            rubbish_category {
              id
              label
              id
              created_at
              updated_at
              label
              color
            }
            id
            label
            created_at
            updated_at
            icon
            parent_id
            rubbish_category_id
            is_support_brand
            is_support_doi
          }
          id
          label
          comment
          volume
          quantity
          weight
          rubbish_id
        }
        cleanup_form_rubbish_details(distinct_on: [rubbish_id, cleanup_area_id, brand_id], order_by: [{rubbish_id: asc, cleanup_area_id: asc, brand_id: asc}, {updated_at: asc}]) {
          rubbish {
            rubbish_category {
              id
              created_at
              updated_at
              label
              color
            }
            id
            label
            created_at
            updated_at
            icon
            parent_id
            rubbish_category_id
            is_support_brand
            is_support_doi
          }
          cleanup_area {
            id
            label
            fill_color
            id
            label
            fill_color
            size
            cleanup_id
            geom
          }
          id
          created_at
          updated_at
          cleanup_form_id
          rubbish_id
          quantity
          weight
          volume
          brand_id
          cleanup_area_id
        }
        cleanup_form_pois(distinct_on: [poi_type_id, cleanup_area_id], order_by: [{poi_type_id: asc, cleanup_area_id: asc}, {updated_at: asc}]) {
          cleanup_area {
            id
            label
            fill_color
            id
            label
            fill_color
            size
            cleanup_id
            geom
          }
          poi_type {
            id
            label
            id
            created_at
            updated_at
            label
            logo
          }
          id
          created_at
          updated_at
          poi_type_id
          cleanup_form_id
          cleanup_area_id
          nb
        }
        cleanup_form_dois(order_by: [{rubbish: {label: asc}}]) {
          rubbish {
            id
            label
            id
            label
            created_at
            updated_at
            icon
            parent_id
            rubbish_category_id
            is_support_brand
            is_support_doi
          }
          id
          created_at
          updated_at
          cleanup_form_id
          rubbish_id
          expiration
        }
        comment
        id
        created_at
        updated_at
        performed_at
        volunteers_number
        external_volunteers_number
        status
        cleanup_id
        rain_force
        wind_force
        owner_id
        rubbish_type_picked_id
        cleanup_duration
      }
      cleanup_type {
        id
        created_at
        updated_at
        label
        characterization_level_id
      }
      cleanup_place {
        id
        created_at
        updated_at
        label
        city_id
      }
      description
      id
      label
      total_area
      total_linear
      total_weight
      total_volume
      start_at
      start_point
      city_id
      environment_type_id
      area_type_id
      cleanup_type_id
      cleanup_place_id
      owner_id
    }
  }`;
}
