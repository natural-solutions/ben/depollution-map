import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { USER_ROLE, getIsAdminInCampaign } from "@/utils/user";
import { parse } from "url";
import { NextResponse } from "next/server";
import { JsonParseSafe } from "@/utils/utils";

export async function GET(req: Request) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN, USER_ROLE.EDITOR],
  });

  if (!session) {
    return errorResp;
  }

  if (!getIsAdminInCampaign(session.userView)) {
    return Response.json(
      { message: "User must be admin in at least one campaign" },
      { status: 403 }
    );
  }

  const variables =
    JsonParseSafe(parse(req.url, true).query.variables as string) || {};

  const cleanups = await dangerousPostAsAdmin(
    {
      query: getQuery(),
      variables,
    },
    "blob"
  );

  const res = new NextResponse(cleanups);
  res.headers.set("content-type", "application/json");
  res.headers.set(
    "Content-Disposition",
    'attachment;filename="export-cleanups-areas.json"'
  );

  return res;
}

function getQuery() {
  return `query Export($order_by: [cleanup_order_by!] = {}, $where: cleanup_bool_exp = {}) {
    cleanups: cleanup(order_by: $order_by, where: $where) {
      id
      label
      start_at
      start_point
      total_weight
      total_volume
      cleanup_areas {
        geom
      }
      cleanup_agg_rubbish_category_views {
        rubbish_category_id
        sum_quantity
        sum_volume
        sum_weight
        ratio_weight
      }
    }
  }`;
}
