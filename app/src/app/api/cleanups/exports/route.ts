import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { USER_ROLE, getIsAdminInCampaign } from "@/utils/user";
import { parse } from "url";
import { pick } from "lodash";
import {
  Brand,
  Cleanup,
  Cleanup_Type,
  Rubbish,
  Rubbish_Category,
  Rubbish_Tag,
} from "@/graphql/types";
import { NextResponse } from "next/server";
import { JsonParseSafe } from "@/utils/utils";
import * as ExcelJS from "exceljs";

export async function GET(req: Request) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN, USER_ROLE.EDITOR],
  });

  if (!session) {
    return errorResp;
  }

  if (!getIsAdminInCampaign(session.userView)) {
    return Response.json(
      { message: "User is not an admin in the campaign" },
      { status: 403 }
    );
  }

  const variables =
    JsonParseSafe(parse(req.url, true).query.variables as string) || {};

  if (!variables.where?._and) {
    variables.where = { _and: variables.where ? [variables.where] : [] };
  }

  variables.where._and.push({
    cleanup_forms: { id: { _is_null: false } },
  });

  const aggByTypes: Cleanup[] = (
    await dangerousPostAsAdmin({
      query: `query ExportAggByTypes($where: cleanup_bool_exp = {}) {
      cleanup(where: $where, distinct_on: cleanup_type_id) {
        cleanup_type_id
        cleanup_type {
          id
          cleanup_type_rubbishes {
            has_qty
            has_wt_vol
            rubbish_id
            rubbish {
              id
              label
              is_support_brand
              is_support_doi
              rubbish_category {
                id
                label
              }
              tags {
                rubbish_tag {
                  id
                  label
                }
              }
            }
          }
        }
      }
    }`,
      variables: {
        where: variables.where,
      },
    })
  ).data?.cleanup;

  const usedRubbishes: Rubbish[] = [];
  aggByTypes.forEach((cleanup) => {
    cleanup.cleanup_type?.cleanup_type_rubbishes.forEach(
      (cleanup_type_rubbish) => {
        const isExists = usedRubbishes.some(
          (existing) => existing.id === cleanup_type_rubbish.rubbish.id
        );
        if (!isExists) {
          usedRubbishes.push({
            ...cleanup_type_rubbish.rubbish,
          });
        }
      }
    );
  });
  usedRubbishes.sort((a, b) => {
    if (a.label < b.label) {
      return -1;
    } else if (a.label > b.label) {
      return 1;
    }
    return 0;
  });

  const usedCategories = (
    usedRubbishes
      .map((rubbish) => rubbish.rubbish_category)
      .filter((x) => x) as Rubbish_Category[]
  ).reduce<Rubbish_Category[]>((uniques, item) => {
    return uniques.some((unique) => unique.id === item.id)
      ? uniques
      : [...uniques, item];
  }, []);
  usedCategories.sort((a, b) => {
    if (a.label < b.label) {
      return -1;
    } else if (a.label > b.label) {
      return 1;
    }
    return 0;
  });

  const usedTags = usedRubbishes
    .map((rubbish) => rubbish.tags)
    .flat(1)
    .map((val) => val.rubbish_tag)
    .filter((x) => x)
    .reduce<Rubbish_Tag[]>((uniques, item) => {
      return uniques.some((unique) => unique.id === item.id)
        ? uniques
        : [...uniques, item];
    }, []);
  usedTags.sort((a, b) => {
    if (a.label < b.label) {
      return -1;
    } else if (a.label > b.label) {
      return 1;
    }
    return 0;
  });

  const cleanupTypes = aggByTypes
    .map((cleanup) => cleanup.cleanup_type)
    .filter((x) => x) as Cleanup_Type[];

  const qtySheetRubbishIds = usedRubbishes
    .filter((rubbish) =>
      cleanupTypes.some((cleanupType) =>
        cleanupType.cleanup_type_rubbishes.some(
          (cleanup_type_rubbish) =>
            cleanup_type_rubbish.rubbish_id === rubbish.id &&
            cleanup_type_rubbish.has_qty
        )
      )
    )
    .map(({ id }) => id);

  const wtSheetRubbishIds = usedRubbishes
    .filter((rubbish) =>
      cleanupTypes.some((cleanupType) =>
        cleanupType.cleanup_type_rubbishes.some(
          (cleanup_type_rubbish) =>
            cleanup_type_rubbish.rubbish_id === rubbish.id &&
            cleanup_type_rubbish.has_wt_vol
        )
      )
    )
    .map(({ id }) => id);

  const volSheetRubbishIds = [...wtSheetRubbishIds];

  const cleanups: Cleanup[] = (
    await dangerousPostAsAdmin({
      query: `query Export($order_by: [cleanup_order_by!] = {}, $where: cleanup_bool_exp = {}) {
        cleanup(order_by: $order_by, where: $where) {
          id
          label
          cleanup_type {
            label
          }
          total_weight
          total_volume
          start_at
          city {
            label
            code
          }
          start_point
          area_type {
            label
          }
          total_area
          total_linear
          total_participants
          cleanup_place {
            label
          }
    			cleanup_campaigns {
            campaign {
              label
            }
          }
          cleanup_forms {
            status
            cleanup_form_rubbishes {
              quantity
              volume
              weight
              rubbish_id
            }
            cleanup_form_rubbish_details(where: {
              brand_id: { _is_null: false },
              rubbish_id: { _is_null: false },
              quantity: { _is_null: false }
            }) {
              brand_id
              rubbish_id
              quantity
            }
          }
          cleanup_agg_rubbish_category_views {
            rubbish_category_id
            sum_quantity
            sum_volume
            sum_weight
            ratio_weight
          }
          cleanup_agg_rubbish_tag_views {
            rubbish_tag_id
            sum_quantity
            sum_volume
            sum_weight
          }
        }
      }`,
      variables,
    })
  ).data?.cleanup;

  const qtyColKeys = new Set<string>();
  const wtColKeys = new Set<string>();
  const volColKeys = new Set<string>();

  const summaries: any[] = [];
  const categories: any[] = [];
  const tags: any[] = [];
  const qties: any[] = [];
  const wts: any[] = [];
  const vols: any[] = [];

  type Col = Partial<ExcelJS.Column>;

  const brandCols: {
    [key: string]: {
      id: string;
      label: string;
      cols: Col[];
    };
  } = {} as any;

  const brands: Brand[] =
    (
      await dangerousPostAsAdmin({
        query: `query {
          brand(order_by: {label: asc}) {
            id
            label
          }
        }`,
      })
    ).data?.brand || [];
  brands.forEach((brand) => {
    brandCols[brand.id] = {
      ...brand,
      cols: [],
    };
  });
  const brandRows: any[] = [];

  cleanups.forEach((cleanup: Cleanup) => {
    const base = {
      ...pick(cleanup, ["label"]),
      cleanup_type: cleanup.cleanup_type?.label,
    };
    const summary: any = {
      ...base,
      status: cleanup.cleanup_forms?.[0].status,
      campaign: cleanup.cleanup_campaigns
        .map((cc) => cc.campaign.label)
        .join(", "),
      place: cleanup.cleanup_place?.label,
      startAt: new Date(cleanup.start_at),
      city: cleanup.city?.label,
      zipcode: cleanup.city?.code,
      lng: cleanup.start_point?.lng,
      lat: cleanup.start_point?.lat,
      area_type: cleanup.area_type?.label,
      total_area: cleanup.total_area,
      total_linear: cleanup.total_linear,
      total_participants: cleanup.total_participants,
      total_weight: cleanup.total_weight, // || 0,
      total_volume: cleanup.total_volume, // || 0,
    };
    summaries.push(summary);

    const category: any = { ...base };
    usedCategories.forEach(({ id }) => {
      const cleanup_agg_rubbish_category_view =
        cleanup.cleanup_agg_rubbish_category_views.find(
          ({ rubbish_category_id }) => rubbish_category_id === id
        );
      category[`${id}_qty`] = cleanup_agg_rubbish_category_view?.sum_quantity; // || 0;
      category[`${id}_wt`] = cleanup_agg_rubbish_category_view?.sum_weight; // || 0;
      category[`${id}_wt_ratio`] =
        cleanup_agg_rubbish_category_view?.ratio_weight; // || 0;
      category[`${id}_vol`] = cleanup_agg_rubbish_category_view?.sum_volume; // || 0;
    });
    categories.push(category);

    const tag: any = { ...base };
    usedTags.forEach(({ id }) => {
      const cleanup_agg_rubbish_tag_view =
        cleanup.cleanup_agg_rubbish_tag_views.find(
          ({ rubbish_tag_id }) => rubbish_tag_id === id
        );
      tag[`${id}_qty`] = cleanup_agg_rubbish_tag_view?.sum_quantity; // || 0;
      tag[`${id}_wt`] = cleanup_agg_rubbish_tag_view?.sum_weight; // || 0;
      tag[`${id}_vol`] = cleanup_agg_rubbish_tag_view?.sum_volume; // || 0;
    });
    tags.push(tag);

    const qty: any = { ...base };
    const wt: any = { ...base };
    const vol: any = { ...base };
    const brand: any = { ...base };

    const cleanupForm = cleanup.cleanup_forms[0];
    if (cleanupForm) {
      usedRubbishes.forEach(({ id, label }) => {
        const cleanup_form_rubbish = cleanupForm?.cleanup_form_rubbishes.find(
          ({ rubbish_id }) => rubbish_id === id
        );
        if (qtySheetRubbishIds.includes(id)) {
          qty[id] = cleanup_form_rubbish?.quantity; // || 0;
          qtyColKeys.add(id);
        }
        if (wtSheetRubbishIds.includes(id)) {
          wt[id] = cleanup_form_rubbish?.weight; // || 0;
          wtColKeys.add(id);
        }
        if (volSheetRubbishIds.includes(id)) {
          vol[id] = cleanup_form_rubbish?.volume; // || 0;
          volColKeys.add(id);
        }

        const cleanupFormDetail = cleanupForm.cleanup_form_rubbish_details.find(
          ({ rubbish_id, brand_id }) => brand_id && rubbish_id === id
        );
        if (cleanupFormDetail) {
          const brand_id = cleanupFormDetail.brand_id;
          const colKey = `${brand_id}_${id}`;
          brand[colKey] = cleanupFormDetail.quantity;
          brandCols[brand_id!].cols.push({
            key: colKey,
            header: label,
          });
        }
      });
    }
    qties.push(qty);
    vols.push(vol);
    wts.push(wt);
    brandRows.push(brand);
  });

  const usedBrands = brands
    .filter(({ id }) => brandCols[id].cols.length)
    .map(({ id }) => brandCols[id]);

  const wsDefaultProps = {
    properties: { defaultColWidth: 16 },
  };

  const baseCols = [
    {
      key: "label",
      header: "Ramassage",
    },
    {
      key: "cleanup_type",
      header: "Type",
    },
  ];

  const workbook = new ExcelJS.Workbook();
  const wsSummaries = workbook.addWorksheet("Résumé", { ...wsDefaultProps });
  wsSummaries.columns = [
    {
      key: "campaign",
      header: "Mission",
    },
    ...baseCols,
    {
      key: "status",
      header: "Status carac.",
    },
    {
      key: "place",
      header: "Lieu",
    },
    {
      key: "startAt",
      header: "Date",
      width: 15,
      style: { numFmt: "dd/mm/yyyy" },
    },
    {
      key: "city",
      header: "Commune",
    },
    {
      key: "zipcode",
      header: "Code postale",
    },
    {
      key: "lng",
      header: "Longitude",
    },
    {
      key: "lat",
      header: "Latitude",
    },
    {
      key: "area_type",
      header: "Type de zone",
    },
    {
      key: "total_area",
      header: "Surface",
    },
    {
      key: "total_linear",
      header: "Linéaire",
    },
    {
      key: "total_participants",
      header: "Nb. participants",
    },
    {
      key: "total_weight",
      header: "Poids",
    },
    {
      key: "total_volume",
      header: "Volume",
    },
  ];
  wsSummaries.addRows(summaries);

  [
    {
      label: "par Tag",
      cols: usedTags,
      rows: tags,
    },
    {
      label: "par Catégorie",
      cols: usedCategories,
      rows: categories,
      hasWtRate: true,
    },
  ].forEach(({ label, cols, rows, hasWtRate }) => {
    const ws = workbook.addWorksheet(label, { ...wsDefaultProps });
    ws.columns = [...baseCols].concat(
      cols
        .map((col) => {
          const result = [
            {
              key: `${col.id}_qty`,
              header: "Quantité",
              width: 12,
            },
            {
              key: `${col.id}_wt`,
              header: "Poids",
              width: 12,
            },
            {
              key: `${col.id}_vol`,
              header: "Volume",
              width: 12,
            },
          ];
          if (hasWtRate) {
            result.splice(2, 0, {
              key: `${col.id}_wt_ratio`,
              header: "Poids (%)",
              width: 12,
              style: { numFmt: "0%" },
            } as any);
          }
          return result;
        })
        .flat(1)
    );
    ws.addRows(rows);
    ws.insertRow(1, {});
    let mergeColIndex = baseCols.length + 1;
    const mergeColNb = hasWtRate ? 4 : 3;
    const colors = ["FFEEEEEE", "FFAAAAAA"];
    cols.forEach((col, i) => {
      const cell = ws.getCell(1, mergeColIndex);
      fmtHeadingCell(cell, col.label, colors[i % 2]);
      ws.mergeCells(1, mergeColIndex, 1, mergeColIndex + mergeColNb - 1);
      mergeColIndex += mergeColNb;
    });
  });

  type WsCategory = {
    label: string;
    categories: {
      label: string;
      cols: Col[];
    }[];
    rows: any[];
    colKeys: Set<string>;
  };
  const rubbishSheets: WsCategory[] = [
    {
      label: "Quantité Déchet",
      categories: [],
      rows: qties,
      colKeys: qtyColKeys,
    },
    {
      label: "Poids Déchet",
      categories: [],
      rows: wts,
      colKeys: wtColKeys,
    },
    {
      label: "Volume Déchet",
      categories: [],
      rows: vols,
      colKeys: volColKeys,
    },
  ];
  usedCategories.forEach((category) => {
    rubbishSheets.forEach((rubbishSheet) => {
      rubbishSheet.categories.push({
        label: category.label,
        cols: [],
      });
    });

    usedRubbishes
      .filter((rubbish) => rubbish.rubbish_category?.id === category.id)
      .forEach((rubbish) => {
        rubbishSheets.forEach((rubbishSheet) => {
          if (rubbishSheet.colKeys.has(rubbish.id)) {
            rubbishSheet.categories[
              rubbishSheet.categories.length - 1
            ].cols.push({
              key: rubbish.id,
              header: rubbish.label,
            });
          }
        });
      });
  });

  rubbishSheets.forEach((rubbishSheet) => {
    rubbishSheet.categories = rubbishSheet.categories.filter(
      (category) => category.cols.length > 0
    );

    const ws = workbook.addWorksheet(rubbishSheet.label, {
      ...wsDefaultProps,
    });
    ws.columns = [
      ...baseCols,
      ...rubbishSheet.categories.map((category) => category.cols).flat(1),
    ];
    ws.addRows(rubbishSheet.rows);
    createHeadingRow(rubbishSheet.categories, ws, baseCols.length + 1);
  });

  const wsBrand = workbook.addWorksheet("Marques", {
    ...wsDefaultProps,
  });
  wsBrand.columns = [
    ...baseCols,
    ...usedBrands.map((brand) => brand.cols).flat(1),
  ];
  wsBrand.addRows(brandRows);
  createHeadingRow(usedBrands, wsBrand, baseCols.length + 1);

  const buffer = await workbook.xlsx.writeBuffer();

  const res = new NextResponse(buffer);
  res.headers.set("content-type", "application/vnd.ms-excel");
  res.headers.set(
    "Content-Disposition",
    'attachment;filename="export-ramassages.xlsx"'
  );

  return res;
}

function createHeadingRow(
  cols: any[],
  ws: ExcelJS.Worksheet,
  initIndex: number
) {
  ws.insertRow(1, {});
  let mergeColIndex = initIndex;
  const colors = ["FFEEEEEE", "FFAAAAAA"];
  cols.forEach((col, i) => {
    const cell = ws.getCell(1, mergeColIndex);
    fmtHeadingCell(cell, col.label, colors[i % 2]);
    ws.mergeCells(1, mergeColIndex, 1, mergeColIndex + col.cols.length - 1);
    mergeColIndex += col.cols.length;
  });
}

function fmtHeadingCell(cell: ExcelJS.Cell, label: string, color: string) {
  cell.value = label;
  cell.alignment = { vertical: "middle", horizontal: "center" };
  cell.fill = {
    type: "pattern",
    pattern: "solid",
    fgColor: {
      argb: color,
    },
  };
}
