import { dangerousPostAsAdmin } from "@/graphql/client";
import { UPDATE_CITY } from "@/graphql/queries";
import { getValidServerSession } from "@/utils/session";
import { USER_ROLE } from "@/utils/user";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN],
  });
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const payload = await req.json();

  try {
    const resp = await dangerousPostAsAdmin({
      query: UPDATE_CITY,
      variables: {
        id,
        _set: payload,
      },
    });
    return Response.json(resp, { status: 200 });
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
