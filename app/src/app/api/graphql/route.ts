import { postAsUser } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";

export async function POST(req: Request) {
  const payload = await req.json();
  const { session, errorResp } = await getValidServerSession({
    allowAnonymous: true,
  });
  if (errorResp) {
    return errorResp;
  }

  try {
    const resp = await postAsUser(payload, session?.userView);

    return Response.json(resp);
  } catch (error) {
    return Response.json({ error }, { status: 500 });
  }
}
