import { UPDATE_USER_CAMPAIGNS, UserFormData } from "@/graphql/queries";
import { getClientToken } from "@/utils/auth";
import { getValidServerSession } from "@/utils/session";
import { USER_AVAILABLES_ROLES, USER_ROLE } from "@/utils/user";
import { pick } from "lodash";
import { dangerousPostAsAdmin } from "@/graphql/client";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN],
  });
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const payload: UserFormData = await req.json();

  const clientToken = await getClientToken();
  const headers = {
    "Content-Type": "application/json",
    Authorization: clientToken.bearer,
  };

  const KeycloackAdminURL = `${process.env.CANONICAL_URL}/auth/admin/realms/${process.env.KEYCLOAK_REALM_ID}`;
  // Why not ?
  //const KeycloackAdminURL = `http://keycloak:8080/auth/admin/realms/${process.env.KEYCLOAK_REALM_ID}`;

  try {
    const resp = await fetch(`${KeycloackAdminURL}/users/${id}`, {
      headers,
      method: "PUT",
      body: JSON.stringify({
        enabled: true,
        firstName: payload.first_name,
        lastName: payload.last_name,
        email: payload.email,
        credentials: !payload.password
          ? undefined
          : [
              {
                type: "password",
                value: payload.password,
                temporary: false,
              },
            ],
      }),
    });

    if (resp.status >= 300) {
      let msg: string = "";
      try {
        msg = await resp.json();
      } catch (error) {
        msg = await resp.text();
      }
      return Response.json(msg, { status: 400 });
    }
  } catch (error) {
    return Response.json(error, { status: 500 });
  }

  if (payload.role_name) {
    try {
      const resp = await fetch(
        `${KeycloackAdminURL}/users/${id}/role-mappings/realm`,
        {
          headers,
          method: "GET",
        }
      );
      const userRoles = (await resp.json()) as any[];
      const userRole = userRoles?.find?.((role) =>
        USER_AVAILABLES_ROLES.includes(role.name)
      );

      if (userRole?.name != payload.role_name) {
        // delete role
        await fetch(`${KeycloackAdminURL}/users/${id}/role-mappings/realm`, {
          headers,
          method: "DELETE",
        });
        // then get specificRole with id
        const resp = await fetch(`${KeycloackAdminURL}/roles`, {
          headers,
          method: "GET",
        });

        const roles = await resp.json();
        const specificRole = roles.find(
          (role: any) => role.name === payload.role_name
        );
        // and add new role, need role id AND role name !
        await fetch(`${KeycloackAdminURL}/users/${id}/role-mappings/realm`, {
          headers,
          method: "POST",
          body: JSON.stringify([
            {
              clientRole: false,
              composite: false,
              containerId: process.env.PROJECT,
              id: specificRole.id,
              name: payload.role_name,
            },
          ]),
        });
      }
    } catch (error) {
      return Response.json(error, { status: 500 });
    }

    if (payload.campaign_users) {
      await dangerousPostAsAdmin({
        query: UPDATE_USER_CAMPAIGNS,
        variables: {
          id,
          objects: payload.campaign_users.map((campaign_user) => ({
            ...pick(campaign_user, ["campaign_id", "role"]),
            user_view_id: id,
          })),
        },
      });
    }
  }

  return Response.json({});
}
