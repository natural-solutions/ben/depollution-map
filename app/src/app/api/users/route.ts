import { UPDATE_USER_CAMPAIGNS, UserFormData } from "@/graphql/queries";
import { getClientToken } from "@/utils/auth";
import { getValidServerSession } from "@/utils/session";
import { pick } from "lodash";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { USER_ROLE } from "@/utils/user";

const KeycloackAdminURL = `${process.env.CANONICAL_URL}/auth/admin/realms/${process.env.KEYCLOAK_REALM_ID}`;

export async function POST(req: Request) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN],
  });
  if (!session) {
    return errorResp;
  }
  const payload: UserFormData = await req.json();

  const clientToken = await getClientToken();
  const headers = {
    "Content-Type": "application/json",
    Authorization: clientToken.bearer,
  };

  let id: string;
  try {
    const resp = await fetch(`${KeycloackAdminURL}/users`, {
      headers,
      method: "POST",
      body: JSON.stringify({
        enabled: true,
        username: payload.username,
        firstName: payload.first_name,
        lastName: payload.last_name,
        email: payload.email,
        credentials: [
          {
            type: "password",
            value: payload.password,
            temporary: false,
          },
        ],
      }),
    });
    if (resp.status >= 300) {
      let msg: string = "";
      try {
        msg = await resp.json();
      } catch (error) {
        msg = await resp.text();
      }
      return Response.json(msg, { status: 400 });
    }
  } catch (error) {
    return Response.json(error, { status: 500 });
  }

  // then get user id
  const res = await fetch(
    `${KeycloackAdminURL}/users?username=${payload.username}&exact=true`,
    {
      headers,
      method: "GET",
    }
  );
  const user = await res.json();
  id = user[0].id;

  if (payload.role_name) {
    try {
      const resp = await fetch(`${KeycloackAdminURL}/roles`, {
        headers,
        method: "GET",
      });

      const roles = await resp.json();
      const specificRole = roles.find(
        (role: any) => role.name === payload.role_name
      );
      if (specificRole) {
        // and add new role, need role id AND role name !
        await fetch(`${KeycloackAdminURL}/users/${id}/role-mappings/realm`, {
          headers: headers,
          method: "POST",
          body: JSON.stringify([
            {
              clientRole: false,
              composite: false,
              containerId: process.env.PROJECT,
              id: specificRole.id,
              name: payload.role_name,
            },
          ]),
        });
      }
    } catch (error) {
      return Response.json(error, { status: 500 });
    }

    if (payload.campaign_users) {
      await dangerousPostAsAdmin({
        query: UPDATE_USER_CAMPAIGNS,
        variables: {
          id,
          objects: payload.campaign_users.map((campaign_user) => ({
            ...pick(campaign_user, ["campaign_id", "role"]),
            user_view_id: id,
          })),
        },
      });
    }
  }

  return Response.json({});
}

export async function DELETE(req: Request) {
  const { session, errorResp } = await getValidServerSession({
    allowedRoles: [USER_ROLE.ADMIN],
  });
  if (!session) {
    return errorResp;
  }
  try {
    const payload: {
      ids: string[];
    } = await req.json();

    const { ids } = payload;
    if (!ids.length) {
      return Response.json({});
    }

    const clientToken = await getClientToken();
    const headers = {
      "Content-Type": "application/json",
      Authorization: clientToken.bearer,
    };
    for (const id of ids) {
      await fetch(`${KeycloackAdminURL}/users/${id}`, {
        headers,
        method: "DELETE",
      });
    }
    return Response.json({ ids });
  } catch (error) {
    return Response.json({ error }, { status: 500 });
  }
}
