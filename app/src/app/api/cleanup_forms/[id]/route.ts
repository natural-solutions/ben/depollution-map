import {
  CleanupFormPatch,
  DELETE_CLEANUP_FORM_DOI_BY_IDS,
  DELETE_CLEANUP_FORM_POI_BY_IDS,
  DELETE_CLEANUP_FORM_RUBBISH_BY_IDS,
  DELETE_CLEANUP_FORM_RUBBISH_DETAIL_BY_IDS,
  GET_CLEANUP_FORM,
  UPDATE_CLEANUP,
  UPDATE_CLEANUP_FORM,
  UPSERT_CLEANUP_FORM_DOI,
  UPSERT_CLEANUP_FORM_POI,
  UPSERT_CLEANUP_FORM_RUBBISH,
  UPSERT_CLEANUP_FORM_RUBBISH_DETAIL,
} from "@/graphql/queries";
import pick from "lodash/pick";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { getCanEditCleanupForm, getCanPublishCleanupForm } from "@/utils/user";
import { Cleanup_Form } from "@/graphql/types";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const {
    data: cleanupFormResp,
  }: {
    data: {
      cleanup_form_by_pk: Cleanup_Form;
    };
  } = await dangerousPostAsAdmin({
    query: GET_CLEANUP_FORM,
    variables: {
      id,
    },
  });
  const cleanupForm = cleanupFormResp.cleanup_form_by_pk;

  if (!cleanupForm) {
    return Response.json({}, { status: 404 });
  }
  if (
    cleanupForm.status === "published" &&
    !getCanPublishCleanupForm(session.userView, cleanupForm)
  ) {
    return Response.json({ error: "Can't unpublished" }, { status: 403 });
  }
  if (
    cleanupForm.status !== "published" &&
    !getCanEditCleanupForm(session.userView, cleanupForm)
  ) {
    return Response.json({ error: "Can't Edit" }, { status: 403 });
  }
  const payload: CleanupFormPatch = await req.json();

  const upsertables = [
    {
      name: "cleanup_form_rubbishes",
      deleteQuery: DELETE_CLEANUP_FORM_RUBBISH_BY_IDS,
      upsertQuery: UPSERT_CLEANUP_FORM_RUBBISH,
      deleteIds: payload?.cleanup_form_rubbishes_delete_ids,
      upserts: payload?.cleanup_form_rubbishes?.map((row) => ({
        ...pick(row, [
          "id",
          "label",
          "rubbish_id",
          "quantity",
          "weight",
          "volume",
          "comment",
          "brand_id",
          "cleanup_area_id",
        ]),
        cleanup_form_id: id,
      })),
    },
    {
      name: "cleanup_form_rubbish_details",
      deleteQuery: DELETE_CLEANUP_FORM_RUBBISH_DETAIL_BY_IDS,
      upsertQuery: UPSERT_CLEANUP_FORM_RUBBISH_DETAIL,
      deleteIds: payload?.cleanup_form_rubbish_details_delete_ids,
      upserts: payload?.cleanup_form_rubbish_details?.map((row) => ({
        ...pick(row, [
          "id",
          "label",
          "rubbish_id",
          "quantity",
          "weight",
          "volume",
          "brand_id",
          "cleanup_area_id",
        ]),
        cleanup_form_id: id,
      })),
    },
    {
      name: "cleanup_form_pois",
      deleteQuery: DELETE_CLEANUP_FORM_POI_BY_IDS,
      upsertQuery: UPSERT_CLEANUP_FORM_POI,
      deleteIds: payload?.cleanup_form_pois_delete_ids,
      upserts: payload?.cleanup_form_pois?.map((row) => ({
        ...pick(row, ["id", "cleanup_area_id", "poi_type_id", "nb"]),
        cleanup_form_id: id,
      })),
    },
    {
      name: "cleanup_form_dois",
      deleteQuery: DELETE_CLEANUP_FORM_DOI_BY_IDS,
      upsertQuery: UPSERT_CLEANUP_FORM_DOI,
      deleteIds: payload?.cleanup_form_dois_delete_ids,
      upserts: payload?.cleanup_form_dois?.map((row) => ({
        ...pick(row, ["id", "rubbish_id", "expiration"]),
        cleanup_form_id: id,
      })),
    },
  ];

  try {
    // if (cleanupForm.status === "published" && payload.status !== "published") {
    //   const resp = await dangerousPostAsAdmin({
    //     query: UPDATE_CLEANUP_FORM,
    //     variables: {
    //       id,
    //       _set: pick(payload, ["status"]),
    //     },
    //   });
    //   if (resp.errors) {
    //     return Response.json(resp, { status: 400 });
    //   }
    // }
    for (const item of upsertables) {
      const deleteIds = item.deleteIds;
      if (deleteIds?.length) {
        const resp = await dangerousPostAsAdmin({
          query: item.deleteQuery!,
          variables: {
            ids: deleteIds,
          },
        });
        if (resp.errors) {
          return Response.json(resp, { status: 400 });
        }
      }

      const objects = item.upserts;
      if (objects?.length) {
        const resp = await dangerousPostAsAdmin({
          query: item.upsertQuery!,
          variables: {
            objects,
          },
        });
        if (resp.errors) {
          return Response.json(resp, { status: 400 });
        }
      }

      delete (payload as any)?.[item.name];
      delete (payload as any)?.[`${item.name}_delete_ids`];
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_CLEANUP_FORM,
      variables: {
        id,
        _set: payload,
      },
    });

    if (resp.errors) {
      return Response.json(resp, { status: 400 });
    }

    await computeCleanup(cleanupForm);

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}

async function computeCleanup(cleanupForm: Cleanup_Form) {
  const cleanupForms: Cleanup_Form[] =
    (
      await dangerousPostAsAdmin({
        query: `query MyQuery($cleanupId: uuid) {
          cleanup_form(where: {cleanup_id: {_eq: $cleanupId}}) {
            volunteers_number
            external_volunteers_number
            cleanup_form_rubbishes {
              quantity
              weight
              volume
              rubbish {
                rubbish_category_id
                tags {
                  rubbish_tag_id
                }
              }
            }
          }
        }`,
        variables: {
          cleanupId: cleanupForm.cleanup_id,
        },
      })
    ).data.cleanup_form || [];

  const cleanupFormRubbishes =
    cleanupForms
      .map((cleanup_form) => cleanup_form.cleanup_form_rubbishes)
      .flat(1) || [];

  let totalWeight = 0;
  let totalVolume = 0;
  const aggCategories: any = {};
  cleanupFormRubbishes.forEach((cleanupFormRubbish) => {
    const categoryId = cleanupFormRubbish?.rubbish?.rubbish_category_id;
    if (!categoryId) {
      return;
    }
    if (!aggCategories[categoryId]) {
      aggCategories[categoryId] = {
        quantity: 0,
        weight: 0,
        volume: 0,
      };
    }
    aggCategories[categoryId].quantity += cleanupFormRubbish.quantity;
    aggCategories[categoryId].weight += cleanupFormRubbish.weight;
    aggCategories[categoryId].volume += cleanupFormRubbish.volume;

    totalWeight += cleanupFormRubbish.weight;
    totalVolume += cleanupFormRubbish.volume;
  });

  for (const [key, value] of Object.entries<any>(aggCategories)) {
    value.ratio_weight = value.weight / totalWeight;
  }

  let totalParticipants = cleanupForms.reduce((total, cleanupForm) => {
    return (
      total +
      (cleanupForm.volunteers_number || 0) +
      (cleanupForm.external_volunteers_number || 0)
    );
  }, 0);

  const resp = await dangerousPostAsAdmin({
    query: UPDATE_CLEANUP,
    variables: {
      id: cleanupForm.cleanup_id,
      _set: {
        total_weight: totalWeight,
        total_volume: totalVolume,
        total_participants: totalParticipants,
      },
    },
  });

  await dangerousPostAsAdmin({
    query: `mutation DeleteAggCategories($cleanupId: uuid) {
      delete_cleanup_agg_rubbish_category(where: {cleanup_id: {_eq: $cleanupId}}) {
        affected_rows
      }
    }`,
    variables: {
      cleanupId: cleanupForm.cleanup_id,
    },
  });

  await dangerousPostAsAdmin({
    query: `mutation InsertAggCategories($objects: [cleanup_agg_rubbish_category_insert_input!] = {}) {
      insert_cleanup_agg_rubbish_category(objects: $objects) {
        returning {
          id
          rubbish_category_id
          sum_quantity
          sum_weight
          sum_volume
          ratio_weight
        }
      }
    }`,
    variables: {
      objects: Object.keys(aggCategories).map((categoryId) => ({
        cleanup_id: cleanupForm.cleanup_id,
        rubbish_category_id: categoryId,
        sum_quantity: aggCategories[categoryId].quantity,
        sum_weight: aggCategories[categoryId].quantity,
        sum_volume: aggCategories[categoryId].volume,
        ratio_weight: aggCategories[categoryId].ratio_weight,
      })),
    },
  });

  const aggTags: any = {};
  cleanupFormRubbishes.forEach((cleanupFormRubbish) => {
    cleanupFormRubbish?.rubbish?.tags
      ?.map((tag) => tag.rubbish_tag_id)
      .forEach((tagId) => {
        if (!aggTags[tagId]) {
          aggTags[tagId] = {
            quantity: 0,
            weight: 0,
            volume: 0,
          };
        }
        aggTags[tagId].quantity += cleanupFormRubbish.quantity;
        aggTags[tagId].weight += cleanupFormRubbish.weight;
        aggTags[tagId].volume += cleanupFormRubbish.volume;
      });
  });

  await dangerousPostAsAdmin({
    query: `mutation DeleteAggTag($cleanupId: uuid) {
      delete_cleanup_agg_rubbish_tag(where: {cleanup_id: {_eq: $cleanupId}}) {
        affected_rows
      }
    }`,
    variables: {
      cleanupId: cleanupForm.cleanup_id,
    },
  });

  await dangerousPostAsAdmin({
    query: `mutation InsertAggTag($objects: [cleanup_agg_rubbish_tag_insert_input!] = {}) {
      insert_cleanup_agg_rubbish_tag(objects: $objects) {
        returning {
          id
          rubbish_tag_id
          sum_quantity
          sum_weight
          sum_volume
        }
      }
    }`,
    variables: {
      objects: Object.keys(aggTags).map((tagId) => ({
        cleanup_id: cleanupForm.cleanup_id,
        rubbish_tag_id: tagId,
        sum_quantity: aggTags[tagId].quantity,
        sum_weight: aggTags[tagId].quantity,
        sum_volume: aggTags[tagId].volume,
      })),
    },
  });
}
