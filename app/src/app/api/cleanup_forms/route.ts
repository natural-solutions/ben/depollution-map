import { GET_CLEANUP, INSERT_CLEANUP_FORM } from "@/graphql/queries";
import pick from "lodash/pick";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { getValidServerSession } from "@/utils/session";
import { Cleanup, Cleanup_Form } from "@/graphql/types";
import {
  USER_ROLE,
  getCanCreateCleanupForm,
  getHasCleanupRole,
  getIsAdmin,
} from "@/utils/user";
import { getCleanupForm } from "@/utils/cleanups";

export async function POST(req: Request) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const formData: Cleanup_Form = await req.json();
  const isAdmin = getIsAdmin(session.userView);

  if (!isAdmin && !formData.cleanup_id) {
    return Response.json({ error: "Missing cleanup_id" }, { status: 400 });
  }

  if (formData.cleanup_id) {
    const {
      data: cleanupResp,
    }: {
      data: {
        cleanup_by_pk: Cleanup;
      };
    } = await dangerousPostAsAdmin({
      query: GET_CLEANUP,
      variables: {
        id: formData.cleanup_id,
      },
    });
    if (!cleanupResp.cleanup_by_pk) {
      return Response.json(
        { error: `Invalid cleanup_id: ${formData.cleanup_id}` },
        { status: 400 }
      );
    }
    const cleanup = cleanupResp.cleanup_by_pk;

    if (!getCanCreateCleanupForm(session.userView, cleanup)) {
      return Response.json(
        { error: `Forbidden cleanup_id: ${formData.cleanup_id}` },
        { status: 403 }
      );
    }

    if (getCleanupForm(cleanup)) {
      return Response.json(
        { error: "Une caractérisation existe déjà pour ce ramassage" },
        { status: 409 }
      );
    }

    const isCleanupAdmin = getHasCleanupRole(
      session.userView,
      cleanup,
      USER_ROLE.EDITOR
    );

    formData.status = !isCleanupAdmin ? "draft" : formData.status || "draft";
  }

  try {
    const resp = await dangerousPostAsAdmin({
      query: INSERT_CLEANUP_FORM,
      variables: {
        _set: {
          ...pick(formData, [
            "performed_at",
            "volunteers_number",
            "external_volunteers_number",
            "comment",
            "cleanup_id",
            "rubbish_type_picked_id",
            "wind_force",
            "rain_force",
            "status",
            "cleanup_duration",
          ]),
          owner_id: session.userView.id,
          cleanup_form_rubbishes: {
            data: formData.cleanup_form_rubbishes.map((detail) => {
              return pick(detail, [
                "rubbish_id",
                "quantity",
                "weight",
                "volume",
              ]);
            }),
          },
          cleanup_form_rubbish_details: {
            data: formData.cleanup_form_rubbish_details.map((detail) => {
              return pick(detail, [
                "rubbish_id",
                "quantity",
                "weight",
                "volume",
                "cleanup_area_id",
                "brand_id",
              ]);
            }),
          },
        },
      },
    });

    if (resp.errors) {
      return Response.json(resp, { status: 400 });
    }

    return Response.json(resp.data.insert_cleanup_form_one);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
