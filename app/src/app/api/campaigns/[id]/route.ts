import {
  GET_CAMPAIGN,
  UPDATE_CAMPAIGN,
  UPDATE_CAMPAIGN_USERS,
} from "@/graphql/queries";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { removeFromMinioAndImagor } from "@/utils/minio";
import { getFilePath } from "@/utils/media";
import { getValidServerSession } from "@/utils/session";
import { Campaign } from "@/graphql/types";
import { pick } from "lodash";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const _set: Campaign = await req.json();

  const replaceables = [
    {
      name: "campaign_users",
      query: UPDATE_CAMPAIGN_USERS,
      objects: _set?.campaign_users?.map((row) => ({
        ...pick(row, ["user_view_id", "role"]),
        campaign_id: id,
      })),
    },
  ];

  try {
    for (const replaceable of replaceables) {
      if (replaceable.objects) {
        await dangerousPostAsAdmin({
          query: replaceable.query,
          variables: {
            id,
            objects: replaceable.objects,
          },
        });
      }
      delete (_set as any)?.[replaceable.name];
    }

    const resp_campaign = await dangerousPostAsAdmin({
      query: GET_CAMPAIGN,
      variables: { id },
    });

    if (!resp_campaign.data.campaign_by_pk) {
      return Response.json({}, { status: 404 });
    }

    const logo = resp_campaign.data.campaign_by_pk.logo;

    if ((logo && !_set.logo) || logo !== _set.logo) {
      await removeFromMinioAndImagor(
        process.env,
        getFilePath("campaigns", id, logo)
      );
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_CAMPAIGN,
      variables: { id, _set },
    });

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
