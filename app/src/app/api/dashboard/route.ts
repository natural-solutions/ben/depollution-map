import {
  COUNT_VOLUNTEERS,
  GET_RUBBISH_VALUE,
  GET_CAMPAIGN_RUBBISH_METRICS,
  RECURRENT_RUBBISH,
  GET_RUBBISH_TAGS_METRICS,
  GET_RUBBISHES_PER_TAGS,
} from "@/graphql/queries";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { parse } from "url";
import { getPool } from "@/utils/pg-client";
import { Campaign_Rubbish_Metrics_View } from "@/graphql/types";

export async function POST(req: Request) {
  const { query } = parse(req.url, true);
  const payload = await req.json();
  const cleanups_id = payload.ids || [];
  const isSmallDashboard = query.isSmallDashboard ? true : false;

  try {
    const { volunteers, totalDuration } = await getVolunteersAndDuration(
      cleanups_id
    );
    const rubbishValue = await getRubbishValue(cleanups_id);
    const rubbishPerCampaign =
      rubbishValue.kg === 0
        ? { series: {}, stack: [] }
        : await getRubbishPerCampaign(cleanups_id, rubbishValue.kg);
    const rubbishPerCategory =
      rubbishValue.kg === 0
        ? { data: [], totalWeight: 0 }
        : await getRubbishPerCategory(cleanups_id);
    const mainRubbish = isSmallDashboard
      ? []
      : await getMainRubbish(cleanups_id);
    const mainRubbishTags = isSmallDashboard
      ? await getMainRubbishTags(cleanups_id)
      : [];

    const basicStats: { [key: string]: number } = {
      butts: 0,
      food_packaging: 0,
      liters: rubbishValue.vol,
      kgPerVolunteer: volunteers ? rubbishValue.kg / volunteers : 0,
      mermaid_tears: 0,
      rubbishesKg: rubbishValue.kg,
      totalHours: Math.round(totalDuration),
      volunteers: volunteers,
    };

    const recurrentRubbish = await getRecurrentRubbish(cleanups_id);
    for (const r of recurrentRubbish) {
      const r_qty = r.cleanup_form_rubbishes_aggregate.aggregate.sum.quantity;
      basicStats[r.id] = r_qty || 0;
    }

    const resp = {
      basicStats: basicStats,
      rubbishPerCampaign: rubbishPerCampaign,
      rubbishPerCategory: rubbishPerCategory,
      mainRubbish: mainRubbish,
      mainRubbishTags: mainRubbishTags,
    };

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}

const getVolunteersAndDuration = async (cleanups_id: string[]) => {
  const res = await dangerousPostAsAdmin({
    query: COUNT_VOLUNTEERS,
    variables: { where: { cleanup_id: { _in: cleanups_id } } },
  });

  let totalVolunteers = 0;
  let totalPersonHours = 0;

  res.data.cleanup_form.forEach((node: any) => {
    const [hours, minutes, seconds] = node.cleanup_duration
      .split(":")
      .map(Number);
    const durationInHours = hours + minutes / 60 + seconds / 3600;

    const volunteersForThisCleanup =
      node.volunteers_number + node.external_volunteers_number;
    totalVolunteers += volunteersForThisCleanup;

    totalPersonHours += durationInHours * volunteersForThisCleanup;
  });

  const totalDuration = +totalPersonHours.toFixed(2);

  return { volunteers: totalVolunteers, totalDuration };
};

const getRubbishPerCampaign = async (
  cleanups_id: string[],
  rubbishWeight: number
) => {
  let res = (
    await dangerousPostAsAdmin({
      query: GET_CAMPAIGN_RUBBISH_METRICS,
      variables: {
        where: {
          campaign: { cleanup_campaigns: { cleanup_id: { _in: cleanups_id } } },
        },
        order_by: [
          { campaign_id: "asc" },
          {
            rubbish_category: {
              label: "asc",
            },
          },
        ],
      },
    })
  ).data.campaign_rubbish_metrics_view as Campaign_Rubbish_Metrics_View[];

  let totalWeight = 0;

  const campaigns = res.reduce<any[]>((acc, curr) => {
    return acc.find(({ id }) => id == curr.campaign_id)
      ? acc
      : [
          ...acc,
          {
            id: curr.campaign_id,
            label: curr.campaign?.label,
          },
        ];
  }, []);
  const campaign_ids = campaigns.map(({ id }) => id);

  const series: {
    id: string;
    label: string;
    color: string;
    weights: number[];
    totalWeight: number;
  }[] = [];

  res.forEach((item) => {
    if (!item.rubbish_category) {
      return;
    }
    let serie = series.find(({ id }) => id == item.category_id);
    if (!serie) {
      serie = {
        id: item.category_id as string,
        label: item.rubbish_category.label,
        color: item.rubbish_category.color!,
        weights: [],
        totalWeight: 0,
      };
      series.push(serie);
    }
    serie.weights[campaign_ids.indexOf(item.campaign_id)] = item.sum_weight;
    serie.totalWeight += item.sum_weight;

    totalWeight += item.sum_weight;
  });

  series.sort((a, b) => {
    return b.totalWeight - a.totalWeight;
  });

  const rubbishPerCampaign: {
    [label: string]: { weights: number[]; color: string };
  } = {};
  series.forEach((serie) => {
    rubbishPerCampaign[serie.label] = {
      weights: serie.weights,
      color: serie.color,
    };
  });

  return {
    series: rubbishPerCampaign,
    stack: campaigns.map(({ label }) => label),
  };
};

const getRubbishPerCategory = async (cleanups_id: string[]) => {
  const pool = getPool();
  const res =
    await pool.query(`SELECT carcv.rubbish_category_id, sum(carcv.sum_weight) AS sum_weight
      FROM cleanup_agg_rubbish_category_view carcv
      where carcv.sum_weight is not null 
      ${
        !cleanups_id.length
          ? ""
          : `and carcv.cleanup_id in ('${cleanups_id.join("','")}')`
      }
      GROUP BY carcv.rubbish_category_id`);

  const metas = await pool.query(
    `select rc.id, rc."label", rc.color from rubbish_category rc where rc.id = ANY($1)`,
    [res.rows.map(({ rubbish_category_id }) => rubbish_category_id)]
  );

  const result = {
    data: res.rows.map((row: any) => {
      const meta = metas.rows.find(({ id }) => id == row.rubbish_category_id);
      return {
        id: row.rubbish_category_id,
        value: Number(row.sum_weight),
        label: meta?.label,
        color: meta?.color,
        unit: "kg",
      };
    }),
    totalWeight: res.rows.reduce((prev, curr) => {
      return prev + Number(curr.sum_weight);
    }, 0),
  };

  result.data.sort((a, b) => {
    return b.value - a.value;
  });

  return result;
};

const getRubbishValue = async (cleanups_id: string[]) => {
  const resp = await dangerousPostAsAdmin({
    query: GET_RUBBISH_VALUE,
    variables: {
      where: { cleanup_form: { cleanup_id: { _in: cleanups_id } } },
    },
  });

  return {
    kg: resp.data.cleanup_form_rubbish_aggregate.aggregate.sum.weight || 0,
    qty: resp.data.cleanup_form_rubbish_aggregate.aggregate.sum.quantity || 0,
    vol: resp.data.cleanup_form_rubbish_aggregate.aggregate.sum.volume || 0,
  };
};

const getMainRubbish = async (cleanups_id: string[]) => {
  const pool = getPool();
  const res =
    await pool.query(`SELECT cfr.rubbish_id, sum(cfr.quantity) AS sum_quantity
      FROM cleanup_form_rubbish cfr
      join cleanup_form cf on cf.id = cfr.cleanup_form_id 
      where rubbish_id not in ('butts', 'mermaid_tears', 'food_packaging') and cfr.quantity is not null 
      ${
        !cleanups_id.length
          ? ""
          : `and cf.cleanup_id in ('${cleanups_id.join("','")}')`
      }
      GROUP BY cfr.rubbish_id
      order by sum(cfr.quantity) desc limit 9`);

  const labels = await pool.query(
    `select id, "label" from rubbish r where r.id = ANY($1)`,
    [res.rows.map(({ rubbish_id }) => rubbish_id)]
  );

  const mainRubbish = res.rows.map((row: any) => ({
    id: row.rubbish_id,
    label: labels.rows.find(({ id }) => id == row.rubbish_id)?.label,
    value: Number(row.sum_quantity),
  }));

  return mainRubbish;
};

const getMainRubbishTags = async (cleanups_id: string[]) => {
  const res = await dangerousPostAsAdmin({
    query: GET_RUBBISH_TAGS_METRICS,
    variables: {
      where: { cleanup_id: { _in: cleanups_id } },
      limit: 5,
    },
  });
  const mainRubbishTags = res.data.rubbish_tag_metrics_query;
  const rubbishe_id: string[] = mainRubbishTags.map(
    (rubbish: any) => rubbish.rubbish_tag_id
  );

  const res_rub = await dangerousPostAsAdmin({
    query: GET_RUBBISHES_PER_TAGS,
    variables: {
      where: {
        id: { _in: rubbishe_id },
      },
    },
  });

  const rubbishData = res_rub.data.rubbish_tag;
  const labels: object[] = [];
  const data: number[] = [];

  mainRubbishTags.forEach((tag: any) => {
    const correspondingRubbish = rubbishData.find(
      (rubbish: any) => rubbish.id === tag.rubbish_tag_id
    );
    const rubbishes = correspondingRubbish
      ? correspondingRubbish.rubbishes.map(
          (rubbish: any) => rubbish.rubbish.label
        )
      : [];
    labels.push({
      label: tag.rubbish_tag_label,
      rubbishes: rubbishes,
    });
    data.push(tag.quantity);
  });

  return { xaxis: labels, series: [{ data: data }] };
};

const getRecurrentRubbish = async (cleanups_id: string[]) => {
  const res = await dangerousPostAsAdmin({
    query: RECURRENT_RUBBISH,
    variables: {
      whereRubbish: {
        id: { _in: ["butts", "mermaid_tears", "food_packaging"] },
      },
      whereForm: { cleanup_form: { cleanup_id: { _in: cleanups_id } } },
    },
  });
  return res.data.rubbish;
};
