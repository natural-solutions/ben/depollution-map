import { dangerousPostAsAdmin } from "@/graphql/client";

export async function GET(req: Request) {
  // Keep it to prevent build process from caching this route
  const { searchParams } = new URL(req.url);
  const { data: cleanupFormResp } = await dangerousPostAsAdmin({
    query: `query CleanupAggregateView {
        cleanup_aggregate(where: {cleanup_forms: {status: {_eq: "published"}}}) {
          aggregate {
            sum {
              total_weight
            }
            count
          }
        }
      }`,
  });

  const cleanupData = {
    total_weight: cleanupFormResp.cleanup_aggregate.aggregate.sum.total_weight,
    count: cleanupFormResp.cleanup_aggregate.aggregate.count,
  };

  return Response.json(cleanupData);
}
