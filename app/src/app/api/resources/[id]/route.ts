import { GET_RESOURCE, UPDATE_RESOURCE } from "@/graphql/queries";
import { dangerousPostAsAdmin } from "@/graphql/client";
import { removeFromMinioAndImagor } from "@/utils/minio";
import { getFilePath } from "@/utils/media";
import { getValidServerSession } from "@/utils/session";
import { Resource_Set_Input } from "@/graphql/types";

export async function PATCH(
  req: Request,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const { session, errorResp } = await getValidServerSession();
  if (!session) {
    return errorResp;
  }

  const id = params.id;
  const _set: Resource_Set_Input = await req.json();

  try {
    const resp_resource = await dangerousPostAsAdmin({
      query: GET_RESOURCE,
      variables: { id },
    });

    if (!resp_resource.data?.resource_by_pk) {
      return Response.json({}, { status: 404 });
    }

    const filename = resp_resource.data.resource_by_pk.filename;

    if ((filename && !_set.filename) || filename !== _set.filename) {
      await removeFromMinioAndImagor(
        process.env,
        getFilePath("resources", id, filename)
      );
    }

    const resp = await dangerousPostAsAdmin({
      query: UPDATE_RESOURCE,
      variables: { id, _set },
    });

    return Response.json(resp);
  } catch (error) {
    console.log(error);
    return Response.json({ error }, { status: 500 });
  }
}
