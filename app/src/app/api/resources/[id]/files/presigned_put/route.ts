import { getServerSession } from "next-auth";
import { handleUpload } from "@/utils/minio";
import { NextRequest } from "next/server";
import { authOptions } from "@/utils/auth";
import { getDirPath } from "@/utils/media";

export async function GET(
  req: NextRequest,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  const session = await getServerSession(authOptions);
  if (!session) {
    return Response.json({}, { status: 401 });
  }

  return handleUpload({
    req,
    folder: getDirPath("resources", params.id),
  });
}
