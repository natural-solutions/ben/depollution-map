import { IMAGOR_PRESETS, IMAGOR_PRESET_NAME } from "@/utils/media";
import { NextRequest } from "next/server";

export async function GET(req: NextRequest) {
  // Keep this comment to remember how to handle session
  //const session = await unstable_getServerSession(req, res, authOptions);
  let uri = req.headers.get("x-forwarded-uri");
  if (!uri) {
    return Response.json("missing or invalid x-forwarded-uri header", {
      status: 403,
    });
  }
  if (uri.charAt(0) == "/") {
    uri = uri.slice(1);
  }
  const reqPreset = uri.split("/")[2] as IMAGOR_PRESET_NAME;
  const preset = IMAGOR_PRESETS[reqPreset];
  if (!preset) {
    return Response.json(`invalid preset: ${reqPreset}`, {
      status: 403,
    });
  }

  return Response.json({});
}
