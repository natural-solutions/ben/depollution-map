import { CssBaseline } from "@mui/material";
import type { Metadata } from "next";
import "@/style/global.css";
import { getServerSession } from "next-auth";
import { authOptions } from "@/utils/auth";
import ClientSessionProvider from "@/context/ClientSessionProvider";
import "maplibre-gl/dist/maplibre-gl.css";
import "@/style/mapbox-gl-draw.css";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import "@/style/font-montserrat.css";
import "@/style/font-poppins.css";

export const metadata: Metadata = {
  title: "Wings Map",
  description: "Appli de gestion des ramassages",
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await getServerSession(authOptions);
  return (
    <html lang="en">
      <CssBaseline />
      <body>
        <ClientSessionProvider session={session}>
          {children}
        </ClientSessionProvider>
      </body>
    </html>
  );
}
