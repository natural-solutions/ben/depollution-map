import { PropsWithChildren } from "react";
import { Box } from "@mui/material";

export default function FormsLayout(props: PropsWithChildren) {
  return (
    <Box
      sx={{
        paddingX: 1,
        overflow: "auto",
        background: "#F0EFEC",
      }}
    >
      {props.children}
    </Box>
  );
}
