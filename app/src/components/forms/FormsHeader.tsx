import React, { FC } from "react";
import { useRAContext } from "@/components/r_admin/RAindex";
import {
  AppBar,
  Box,
  Button,
  IconButton,
  MobileStepper,
  Toolbar,
  Typography,
} from "@mui/material";
import {
  ArrowBackIos,
  KeyboardArrowLeft,
  KeyboardArrowRight,
} from "@mui/icons-material";
import { useRouter } from "next/navigation";
import { HEADER_HEIGHT_SX } from "@/utils/theme";
import { Step } from "../../utils/forms";

type FormsHeaderProps = {
  backUrl?: string;
  title?: string;
  steps?: Step[];
  activeStep?: number;
  onActiveStepChange?: (index: number) => void;
};

export const FormsHeader: FC<FormsHeaderProps> = ({
  backUrl,
  title,
  steps,
  activeStep = 0,
  onActiveStepChange,
}) => {
  const raContext = useRAContext();
  const router = useRouter();

  const handleBack = () => {
    onActiveStepChange?.(activeStep - 1);
  };

  const handleNext = () => {
    onActiveStepChange?.(activeStep + 1);
  };

  const nbSteps = steps?.length || 0;

  return (
    <>
      {/* KEEP-IT could be usefull <Stack
        direction="row"
        sx={{
          display: {
            xs: "none",
            md: "flex",
          },
          py: 3,
          ml: 2,
        }}
        spacing={4}
      >
        {Boolean(backUrl) && (
          <Button
            size="small"
            onClick={() => {
              router.push(backUrl!);
            }}
          >
            Retour
          </Button>
        )}
        {Boolean(title) && (
          <Typography variant="h4" fontFamily="Montserrat">
            {title}
          </Typography>
        )}
      </Stack> */}
      <AppBar
        position="fixed"
        sx={{
          display: {
            md: "none",
          },
          background: "#FFF",
          color: "#000",
          minHeight: "auto",
          height: "48px",
          boxShadow: nbSteps ? "none" : undefined,
        }}
      >
        <Toolbar
          sx={{
            minHeight: {
              xs: "48px",
              sm: "48px",
            },
          }}
        >
          {Boolean(backUrl) && (
            <IconButton
              size="large"
              edge="start"
              color="primary"
              onClick={() => {
                router.push(backUrl!);
              }}
            >
              <ArrowBackIos />
            </IconButton>
          )}
          {Boolean(title) && <Typography variant="h6">{title}</Typography>}
        </Toolbar>
      </AppBar>
      {Boolean(nbSteps) && (
        <>
          {raContext.name != "admin" && <Box sx={{ height: "56px" }}></Box>}
          <MobileStepper
            variant="text"
            steps={nbSteps}
            position={raContext.name == "admin" ? "static" : "top"}
            activeStep={activeStep}
            sx={{
              backgroundColor: "#F4F5FA",
              top: HEADER_HEIGHT_SX,
              height: HEADER_HEIGHT_SX,
            }}
            backButton={
              <Button
                size="small"
                onClick={handleBack}
                disabled={activeStep === 0}
                startIcon={<KeyboardArrowLeft />}
              >
                {steps?.[activeStep - 1]?.title || ""}
              </Button>
            }
            nextButton={
              <Button
                size="small"
                onClick={handleNext}
                disabled={activeStep === nbSteps - 1}
                endIcon={<KeyboardArrowRight />}
              >
                {steps?.[activeStep + 1]?.title || ""}
              </Button>
            }
          />
        </>
      )}
    </>
  );
};
