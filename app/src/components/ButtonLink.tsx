import { Button, ButtonProps } from "@mui/material";
import Link from "next/link";
import React, { FC } from "react";

export const ButtonLink: FC<ButtonProps> = ({ children, ...props }) => {
  return (
    <Button {...props} LinkComponent={Link}>
      {children}
    </Button>
  );
};
