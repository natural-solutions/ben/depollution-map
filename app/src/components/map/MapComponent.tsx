"use client";

import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import { useCallback, useEffect, useRef, useState } from "react";
import { Map, MapLayerMouseEvent, MapRef, Popup } from "react-map-gl/maplibre";
import { Cleanup_View } from "@/graphql/types";
import { Layer, LayerProps, NavigationControl, Source } from "react-map-gl";
import { useRouter } from "next/navigation";
import {
  MAP_INIT_POINT,
  MAP_LAYERS,
  MAP_STYLE,
  MapLayerName,
} from "@/utils/map";
import { cloneDeep } from "lodash";
import CleanupCard from "../cleanup/CleanupCard";

type ViewState = typeof MAP_INIT_POINT;

interface MapComponentProps {
  cleanups: Cleanup_View[];
  viewState: ViewState;
  setViewState: (value: ViewState) => void;
}

export default function MapComponent(props: MapComponentProps) {
  const { cleanups, viewState, setViewState } = props;

  const mapRef = useRef<MapRef>(null);
  const router = useRouter();

  const [cleanupDetails, setCleanupDetails] = useState<Cleanup_View>();
  const [cursor, setCursor] = useState<string>("auto");

  const [mapStyle, setMapStyle] = useState<any>({});
  const [baseMap, setBaseMap] = useState<MapLayerName>("plan");

  const onMouseEnter = useCallback(() => setCursor("pointer"), []);
  const onMouseLeave = useCallback(() => setCursor("auto"), []);

  const onMapClick = (e: MapLayerMouseEvent) => {
    const feature = e.features?.[0];

    if (feature) {
      const coords = (feature?.geometry as any).coordinates;

      if (feature.properties.cleanup) {
        const cleanup = JSON.parse(feature.properties.cleanup);
        setCleanupDetails(cleanup as Cleanup_View);

        mapRef.current?.panTo(coords, { duration: 500 });
      } else {
        mapRef.current?.easeTo({
          center: coords,
          zoom: mapRef.current?.getZoom() + 1,
          duration: 500,
        });
      }
    }
  };

  const clusters: LayerProps = {
    id: "clusters",
    type: "circle",
    source: "earthquakes",
    filter: ["has", "point_count"],
    paint: {
      "circle-color": [
        "step",
        ["get", "point_count"],
        "#51bbd6",
        100,
        "#f1f075",
        750,
        "#f28cb1",
      ],
      "circle-radius": ["step", ["get", "point_count"], 20, 100, 30, 750, 40],
    },
  };

  const clustersCount: LayerProps = {
    id: "cluster-count",
    type: "symbol",
    source: "earthquakes",
    filter: ["has", "point_count"],
    layout: {
      "text-field": "{point_count_abbreviated}",
      "text-font": ["Open Sans Bold"],
      "text-size": 12,
    },
  };

  const unclusteredPointLayer: LayerProps = {
    id: "unclustered-point",
    type: "symbol",
    layout: {
      "icon-image": "icon-pin",
      "icon-anchor": "bottom",
    },
    source: "earthquakes",
    filter: ["!", ["has", "point_count"]],
    paint: {
      "icon-color": ["get", "main_color"],
      "icon-halo-color": "#000000",
      "icon-halo-width": 5,
      "icon-halo-blur": 0,
    },
  };

  const handleBaseMap = (value: MapLayerName) => {
    setBaseMap(value);
  };

  useEffect(() => {
    const newMapStyle = cloneDeep(MAP_STYLE);
    newMapStyle.sources.bg_src = MAP_LAYERS[baseMap];
    setMapStyle(newMapStyle);
  }, [baseMap]);

  const onMapLoad = useCallback(() => {
    if (!mapRef.current) {
      return;
    }
    mapRef.current.loadImage("/assets/pin.png", (error, image) => {
      if (error) {
        console.log("Load image error:", error);
        return;
      }
      mapRef.current?.addImage("icon-pin", image!, {
        sdf: true,
      });
    });
  }, []);

  return (
    <>
      <Map
        {...viewState}
        onLoad={onMapLoad}
        onMove={(e) => setViewState(e.viewState)}
        onMouseLeave={onMouseLeave}
        onMouseEnter={onMouseEnter}
        onClick={onMapClick}
        style={{ height: "100%" }}
        ref={mapRef}
        mapStyle={mapStyle}
        cursor={cursor}
        interactiveLayerIds={["clusters", "unclustered-point"]}
      >
        <NavigationControl showCompass={false} />
        <ToggleButtonGroup
          size="small"
          color="primary"
          value={baseMap}
          exclusive
          onChange={(e, value) => {
            handleBaseMap(value);
          }}
          sx={{
            position: "absolute",
            background: "white",
            top: 8,
            left: 8,
          }}
        >
          <ToggleButton value="plan">Plan</ToggleButton>
          <ToggleButton value="sat">Satellite</ToggleButton>
        </ToggleButtonGroup>
        <Source
          id="earthquakes"
          type="geojson"
          data={{
            type: "FeatureCollection",
            features: cleanups.map((cleanup) => ({
              type: "Feature",
              properties: {
                cleanup,
                main_color:
                  cleanup.cleanup_campaigns?.[0]?.campaign?.main_color ||
                  "#F0EFEC",
              },
              geometry: {
                type: "Point",
                coordinates: [
                  cleanup.start_point.lng,
                  cleanup.start_point.lat,
                  0,
                ],
              },
            })),
          }}
          cluster={true}
          clusterMaxZoom={14}
          clusterRadius={20}
        >
          <Layer {...clusters} />
          <Layer {...clustersCount} />
          <Layer {...unclusteredPointLayer} />
        </Source>

        {cleanupDetails && (
          <Popup
            anchor="bottom"
            offset={[0, -15] as any}
            latitude={cleanupDetails.start_point.lat}
            longitude={cleanupDetails.start_point.lng}
            onClose={() => setCleanupDetails(undefined)}
            style={{ maxWidth: 600 }}
          >
            <CleanupCard cleanup={cleanupDetails} canShowHistory={true} />
          </Popup>
        )}
      </Map>
    </>
  );
}
