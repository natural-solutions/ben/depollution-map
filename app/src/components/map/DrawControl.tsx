import MapboxDraw from "@mapbox/mapbox-gl-draw";
import { forwardRef, useImperativeHandle } from "react";
import { useControl } from "react-map-gl";

import type { ControlPosition } from "react-map-gl";

export type DrawControlEvent = { features: any[]; action?: string };

type DrawControlProps = ConstructorParameters<typeof MapboxDraw>[0] & {
  position?: ControlPosition;

  onSelect?: (evt: DrawControlEvent) => void;
  onCreate?: (evt: DrawControlEvent) => void;
  onUpdate?: (evt: DrawControlEvent) => void;
  onDelete?: (evt: DrawControlEvent) => void;
};

MapboxDraw.constants.classes.CONTROL_BASE = "maplibregl-ctrl" as any;
MapboxDraw.constants.classes.CONTROL_PREFIX = "maplibregl-ctrl-" as any;
MapboxDraw.constants.classes.CONTROL_GROUP = "maplibregl-ctrl-group" as any;

const DrawControl = forwardRef((props: DrawControlProps, ref) => {
  const drawRef = useControl<MapboxDraw>(
    () => new MapboxDraw(props),
    (context) => {
      if (props.onSelect) {
        context.map.on("draw.selectionchange", props.onSelect);
      }
      if (props.onCreate) {
        context.map.on("draw.create", props.onCreate);
      }
      if (props.onUpdate) {
        context.map.on("draw.update", props.onUpdate);
      }
      if (props.onDelete) {
        context.map.on("draw.delete", props.onDelete);
      }
    },
    (context) => {
      if (props.onSelect) {
        context.map.off("draw.selectionchange", props.onSelect);
      }
      if (props.onCreate) {
        context.map.off("draw.create", props.onCreate);
      }
      if (props.onUpdate) {
        context.map.off("draw.update", props.onUpdate);
      }
      if (props.onDelete) {
        context.map.off("draw.delete", props.onDelete);
      }
    },
    {
      position: props.position,
    }
  );

  useImperativeHandle(ref, () => drawRef, [drawRef]);

  return null;
});

DrawControl.displayName = "Search";

export default DrawControl;

/* DrawControl.defaultProps = {
  onCreate: () => {},
  onUpdate: () => {},
  onDelete: () => {},
}; */
