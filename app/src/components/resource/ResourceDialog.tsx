import { GET_RESOURCES } from "@/graphql/queries";
import { Resource } from "@/graphql/types";
import { getFileSrcURL } from "@/utils/media";
import { useApi } from "@/utils/useApi";
import { Close, PictureAsPdf, Visibility } from "@mui/icons-material";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  IconButton,
  Typography,
  Box,
  Stack,
} from "@mui/material";
import { useEffect, useState } from "react";

interface ResourceDialogProps {
  isOpenResources: boolean;
  setIsOpenResources: React.Dispatch<React.SetStateAction<boolean>>;
}

export const ResourceDialog = (props: ResourceDialogProps) => {
  const { isOpenResources, setIsOpenResources } = props;

  const api = useApi();
  const [resources, setResources] = useState<Resource[]>([]);

  useEffect(() => {
    (async () => {
      const resource = await api.postGraphql<{
        data: { resource: Resource[] };
      }>({
        query: GET_RESOURCES,
      });
      setResources(resource.data.data.resource);
    })();
  }, []);

  return (
    <>
      <Dialog
        open={isOpenResources}
        onClose={() => setIsOpenResources(false)}
        fullWidth
        maxWidth="md"
      >
        <DialogTitle fontSize={15} variant="h6">
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography variant="h4">Ressources</Typography>
            <IconButton edge="end" onClick={() => setIsOpenResources(false)}>
              <Close />
            </IconButton>
          </Box>
        </DialogTitle>
        <DialogContent sx={{ pl: 1 }}>
          <Box sx={{ width: "100%" }}>
            {resources.length > 0 ? (
              resources.map((resource) => (
                <Box
                  key={resource.id}
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Box sx={{ flex: 1, display: "flex", alignItems: "center" }}>
                    <PictureAsPdf />
                    <Stack
                      direction="column"
                      spacing={1}
                      sx={{ paddingLeft: 2 }}
                    >
                      <Typography>{resource.label}</Typography>
                      <Typography variant="body2" sx={{ color: "#14363F99" }}>
                        {resource.description}
                      </Typography>
                    </Stack>
                  </Box>
                  <IconButton
                    color="primary"
                    href={
                      getFileSrcURL(
                        "resources",
                        resource.id,
                        resource.filename ?? ""
                      ) || ""
                    }
                    target="_blank"
                  >
                    <Visibility />
                  </IconButton>
                </Box>
              ))
            ) : (
              <Typography>Aucune ressource disponible</Typography>
            )}
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
};
