import { Badge, BadgeProps } from "@mui/material";
import React, { FC } from "react";

type BadgeInlineProps = BadgeProps;

export const BadgeInline: FC<BadgeInlineProps> = (props) => {
  const params: any = { ...props };
  const sx: any = params.sx;
  const sxBadge = sx?.["& .MuiBadge-badge"];
  delete params.sx;
  delete sx?.["& .MuiBadge-badge"];
  return (
    <Badge
      showZero
      sx={{
        ml: 1,
        "& .MuiBadge-badge": {
          position: "static",
          transform: "none",
          ...sxBadge,
        },
        ...sx,
      }}
      {...params}
    />
  );
};
