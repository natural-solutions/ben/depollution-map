import { DataProvider } from "react-admin";
import { createContext, useContext, useEffect, useState } from "react";
import buildHasuraProvider, { BuildFields, buildFields } from "ra-data-hasura";
import { from, createHttpLink } from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import gql from "graphql-tag";
import { signIn, useSession } from "next-auth/react";
import { Button, Stack } from "@mui/material";
import { useRouter } from "next/navigation";
import { ResourcesAdmin } from "./ResourcesAdmin";
import { ResourcesForms } from "./ResourcesForms";
import { FETCH_TYPE, RESOURCE_NAME, getFields } from "@/graphql/resources";
import {
  useI18nextProvider,
  convertRaTranslationsToI18next,
} from "ra-i18n-i18next";
import frenchMessages from "ra-language-french";
import graphqlSchema from "../../graphql/schema.json";

/**
 * Extracts just the fields from a GraphQL AST.
 * @param {GraphQL AST} queryAst
 */
const extractFieldsFromQuery = (queryAst: any) => {
  return queryAst.definitions[0].selectionSet.selections;
};

const customBuildFields: BuildFields = (type, fetchType) => {
  const resourceName = type.name;

  const fields = getFields(
    resourceName as RESOURCE_NAME,
    (fetchType as any) || FETCH_TYPE.GET_MANY
  );

  if (fields) {
    try {
      return extractFieldsFromQuery(
        gql`
       { ${fields}}
      `
      );
    } catch (error) {
      throw new Error(`{ ${fields}}`);
    }
  }

  return buildFields(type, fetchType);
};

interface RAdminProps {
  context: string;
}

export const RAContext = createContext<{
  name: "admin" | "forms";
  hasNoLayout: boolean;
}>({ name: "admin", hasNoLayout: false });
export const useRAContext = () => useContext(RAContext);

const RAdmin = (props: RAdminProps) => {
  const { context } = props;

  const session = useSession();
  const router = useRouter();
  const [dataProvider, setDataProvider] = useState<DataProvider | null>(null);
  const [isLoading, setIsLoading] = useState(true);

  const i18nProvider = useI18nextProvider({
    options: {
      lng: "fr",
      resources: {
        fr: {
          translation: convertRaTranslationsToI18next(frenchMessages),
        },
      },
    },
  });

  useEffect(() => {
    const buildDataProvider = async () => {
      const errorLink = onError(({ networkError, graphQLErrors }) => {
        if (networkError) {
          const status = (networkError as any).statusCode;
          if ([401, 403].includes(status)) {
            router.push("/");
          }
          return;
        }
        if (graphQLErrors) {
          for (let err of graphQLErrors) {
            console.log("err", err);
          }
        }
      });

      const httpLink = createHttpLink({
        uri: "/api/graphql",
      });
      const dataProvider = await buildHasuraProvider(
        {
          clientOptions: {
            link: from([errorLink, httpLink]),
          },
          introspection: {
            schema: graphqlSchema.__schema as any,
          },
        },
        { buildFields: customBuildFields }
      );
      setDataProvider(() => dataProvider);
      setTimeout(() => {
        setIsLoading(false);
      }, 1000);
    };
    buildDataProvider();
  }, []);

  if (isLoading || !dataProvider || !i18nProvider) {
    return <p>Loading...</p>;
  } else if (context === "admin") {
    if (session.status === "authenticated") {
      return (
        <RAContext.Provider value={{ name: context, hasNoLayout: false }}>
          <ResourcesAdmin
            dataProvider={dataProvider}
            i18nProvider={i18nProvider}
          />
        </RAContext.Provider>
      );
    } else if (!isLoading) {
      return (
        <Stack
          sx={{
            height: "100%",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Button variant="contained" onClick={() => signIn()}>
            Login
          </Button>
        </Stack>
      );
    }
  } else if (context == "forms") {
    return (
      <RAContext.Provider value={{ name: context, hasNoLayout: true }}>
        <ResourcesForms
          dataProvider={dataProvider}
          i18nProvider={i18nProvider}
        />
      </RAContext.Provider>
    );
  }
};

export default RAdmin;
