import {
  Admin,
  Resource,
  ListGuesser,
  DataProvider,
  I18nProvider,
} from "react-admin";
import FormsLayout from "../forms/FormsLayout";
import { AdminCleanupEdit } from "../admin/cleanup/AdminCleanupEdit";
import { AdminCleanupCreate } from "../admin/cleanup/AdminCleanupCreate";
import { AdminCampaignEdit } from "../admin/campaign/AdminCampaignEdit";
import { AdminCampaignCreate } from "../admin/campaign/AdminCampaignCreate";
import { AdminRubbishTagCreate } from "../admin/rubbish_tag/AdminRubbishTagCreate";
import { AdminRubbishCreate } from "../admin/rubbish/AdminRubbishCreate";
import { AdminRubbishEdit } from "../admin/rubbish/AdminRubbishEdit";
import { AdminBrandCreate } from "../admin/brand/AdminBrandCreate";
import { AdminBrandEdit } from "../admin/brand/AdminBrandEdit";
import { AdminCleanupFormCreate } from "../admin/cleanup_form/AdminCleanupFormCreate";
import { AdminCleanupFormEdit } from "../admin/cleanup_form/AdminCleanupFormEdit";
import { AdminPartnerEdit } from "../admin/partner/AdminPartnerEdit";
import { AdminPartnerCreate } from "../admin/partner/AdminPartnerCreate";
import { themeOptions } from "@/utils/theme";

interface RFormsProps {
  dataProvider: DataProvider;
  i18nProvider: I18nProvider;
}

export const ResourcesForms = (props: RFormsProps) => {
  const { dataProvider, i18nProvider } = props;

  return (
    <Admin
      theme={themeOptions}
      layout={FormsLayout}
      dataProvider={dataProvider}
      i18nProvider={i18nProvider}
    >
      <Resource name="cleanup_type" list={ListGuesser} />
      <Resource
        name="campaign"
        list={undefined}
        edit={AdminCampaignEdit}
        create={AdminCampaignCreate}
      />
      <Resource
        name="cleanup"
        edit={AdminCleanupEdit}
        create={AdminCleanupCreate}
      />
      <Resource
        name="cleanup_form"
        list={undefined}
        create={AdminCleanupFormCreate}
        edit={AdminCleanupFormEdit}
      />
      <Resource
        name="partner"
        list={undefined}
        create={AdminPartnerCreate}
        edit={AdminPartnerEdit}
      />
      <Resource name="cleanup_area" list={ListGuesser} />
      <Resource
        name="rubbish"
        list={undefined}
        edit={AdminRubbishEdit}
        create={AdminRubbishCreate}
      />
      <Resource
        name="rubbish_tag"
        list={undefined}
        create={AdminRubbishTagCreate}
      />
      <Resource
        name="brand"
        list={undefined}
        create={AdminBrandCreate}
        edit={AdminBrandEdit}
      />
    </Admin>
  );
};
