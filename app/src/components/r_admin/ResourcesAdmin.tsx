import {
  Admin,
  Resource,
  ListGuesser,
  DataProvider,
  I18nProvider,
  CustomRoutes,
} from "react-admin";
import { Route } from "react-router-dom";
import { AdminLayout } from "../admin/navigation/AdminLayout";
import { AdminCleanupList } from "../admin/cleanup/AdminCleanupList";
import { AdminCleanupEdit } from "../admin/cleanup/AdminCleanupEdit";
import { AdminCleanupCreate } from "../admin/cleanup/AdminCleanupCreate";
import { AdminCampaignList } from "../admin/campaign/AdminCampaignList";
import { AdminCampaignEdit } from "../admin/campaign/AdminCampaignEdit";
import { AdminCampaignCreate } from "../admin/campaign/AdminCampaignCreate";
import { AdminRubbishTagList } from "../admin/rubbish_tag/AdminRubbishTagList";
import { AdminRubbishTagCreate } from "../admin/rubbish_tag/AdminRubbishTagCreate";
import { AdminRubbishList } from "../admin/rubbish/AdminRubbishList";
import { AdminRubbishCreate } from "../admin/rubbish/AdminRubbishCreate";
import { AdminRubbishEdit } from "../admin/rubbish/AdminRubbishEdit";
import { AdminBrandList } from "../admin/brand/AdminBrandList";
import { AdminBrandCreate } from "../admin/brand/AdminBrandCreate";
import { AdminBrandEdit } from "../admin/brand/AdminBrandEdit";
import { AdminCleanupFormList } from "../admin/cleanup_form/AdminCleanupFormList";
import { AdminCleanupFormCreate } from "../admin/cleanup_form/AdminCleanupFormCreate";
import { AdminCleanupFormEdit } from "../admin/cleanup_form/AdminCleanupFormEdit";
import { AdminPartnerList } from "../admin/partner/AdminPartnerList";
import { AdminPartnerEdit } from "../admin/partner/AdminPartnerEdit";
import { AdminPartnerCreate } from "../admin/partner/AdminPartnerCreate";
import { AdminUserList } from "../admin/user_view/AdminUserList";
import { AdminRubbishTagEdit } from "../admin/rubbish_tag/AdminRubbishTagEdit";
import { AdminPoiTypeList } from "../admin/poi_type/AdminPoiTypeList";
import { AdminPoiTypeCreate } from "../admin/poi_type/AdminPoiTypeCreate";
import { AdminPoiTypeEdit } from "../admin/poi_type/AdminPoiTypeEdit";
import { AdminRubbishCategoryList } from "../admin/rubbish_category/AdminRubbishCategoryList";
import { AdminRubbishCategoryEdit } from "../admin/rubbish_category/AdminRubbishCategoryEdit";
import { AdminRubbishCategoryCreate } from "../admin/rubbish_category/AdminRubbishCategoryCreate";
import { AdminCleanupTypeList } from "../admin/cleanup_type/AdminCleanupTypeList";
import { AdminCleanupTypeEdit } from "../admin/cleanup_type/AdminCleanupTypeEdit";
import { AdminCleanupTypeCreate } from "../admin/cleanup_type/AdminCleanupTypeCreate";
import { AdminCleanupPlaceList } from "../admin/cleanup_place/AdminCleanupPlaceList";
import { AdminCleanupPlaceEdit } from "../admin/cleanup_place/AdminCleanupPlaceEdit";
import { AdminCleanupPlaceCreate } from "../admin/cleanup_place/AdminCleanupPlaceCreate";
import { useSessionContext } from "@/utils/useSessionContext";
import { AdminUserEdit } from "../admin/user_view/AdminUserEdit";
import { AdminUserCreate } from "../admin/user_view/AdminUserCreate";
import { AdminResourceList } from "../admin/resource/AdminResourceList";
import { AdminResourceCreate } from "../admin/resource/AdminResourceCreate";
import { AdminResourceEdit } from "../admin/resource/AdminResourceEdit";
import { CleanupComparePage } from "../admin/cleanup/CleanupComparePage";
import { AdminCharacterizationLevelList } from "../admin/characterization_level/AdminCharacterizationLevelList";
import { AdminCharacterizationLevelEdit } from "../admin/characterization_level/AdminCharacterizationLevelEdit";
import { AdminCharacterizationLevelCreate } from "../admin/characterization_level/AdminCharacterizationLevelCreate";
import { AdminCityList } from "../admin/city/AdminCityList";
import { AdminCityCreate } from "../admin/city/AdminCityCreate";
import { AdminCityEdit } from "../admin/city/AdminCityEdit";

interface RAdminProps {
  dataProvider: DataProvider;
  i18nProvider: I18nProvider;
}

export const ResourcesAdmin = (props: RAdminProps) => {
  const { dataProvider, i18nProvider } = props;

  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <Admin
      layout={AdminLayout}
      dataProvider={dataProvider}
      i18nProvider={i18nProvider}
    >
      <Resource
        name="user_view"
        list={AdminUserList}
        create={isAuthorized ? AdminUserCreate : undefined}
        edit={isAuthorized ? AdminUserEdit : undefined}
        options={{
          label: "Utilisateurs",
        }}
      />
      <Resource
        name="campaign"
        list={AdminCampaignList}
        edit={isAuthorized ? AdminCampaignEdit : undefined}
        create={isAuthorized ? AdminCampaignCreate : undefined}
        options={{
          label: "Entités",
        }}
      />
      <Resource
        name="cleanup"
        list={AdminCleanupList}
        edit={AdminCleanupEdit}
        create={AdminCleanupCreate}
        options={{
          label: "Ramassages",
        }}
      />
      <CustomRoutes>
        <Route path="/cleanup-compare" element={<CleanupComparePage />} />
      </CustomRoutes>
      <Resource
        name="characterization_level"
        list={AdminCharacterizationLevelList}
        edit={isAuthorized ? AdminCharacterizationLevelEdit : undefined}
        create={isAuthorized ? AdminCharacterizationLevelCreate : undefined}
        options={{
          label: "Niveau de caractérisation",
        }}
      />
      <Resource
        name="cleanup_type"
        list={AdminCleanupTypeList}
        edit={isAuthorized ? AdminCleanupTypeEdit : undefined}
        create={isAuthorized ? AdminCleanupTypeCreate : undefined}
        options={{
          label: "Type de ramassages",
        }}
      />
      <Resource
        name="cleanup_place"
        list={AdminCleanupPlaceList}
        edit={isAuthorized ? AdminCleanupPlaceEdit : undefined}
        create={AdminCleanupPlaceCreate}
        options={{
          label: "Lieux des ramassages",
        }}
      />
      <Resource
        name="cleanup_form"
        list={AdminCleanupFormList}
        create={AdminCleanupFormCreate}
        edit={AdminCleanupFormEdit}
        options={{
          label: "Caractérisations",
        }}
      />
      <Resource
        name="partner"
        list={AdminPartnerList}
        create={isAuthorized ? AdminPartnerCreate : undefined}
        edit={isAuthorized ? AdminPartnerEdit : undefined}
        options={{
          label: "Partenaires",
        }}
      />
      <Resource name="cleanup_area" list={ListGuesser} />
      <Resource
        name="rubbish"
        list={AdminRubbishList}
        edit={isAuthorized ? AdminRubbishEdit : undefined}
        create={isAuthorized ? AdminRubbishCreate : undefined}
        options={{
          label: "Déchets",
        }}
      />
      <Resource
        name="rubbish_category"
        list={AdminRubbishCategoryList}
        edit={isAuthorized ? AdminRubbishCategoryEdit : undefined}
        create={isAuthorized ? AdminRubbishCategoryCreate : undefined}
        options={{
          label: "Catégories de déchets",
        }}
      />
      <Resource
        name="rubbish_tag"
        list={AdminRubbishTagList}
        edit={isAuthorized ? AdminRubbishTagEdit : undefined}
        create={isAuthorized ? AdminRubbishTagCreate : undefined}
        options={{
          label: "Tags de déchets",
        }}
      />
      <Resource
        name="brand"
        list={AdminBrandList}
        create={isAuthorized ? AdminBrandCreate : undefined}
        edit={isAuthorized ? AdminBrandEdit : undefined}
        options={{
          label: "Brands",
        }}
      />
      <Resource
        name="poi_type"
        list={AdminPoiTypeList}
        create={isAuthorized ? AdminPoiTypeCreate : undefined}
        edit={isAuthorized ? AdminPoiTypeEdit : undefined}
        options={{
          label: "Points d'intérêts",
        }}
      />
      <Resource
        name="resource"
        list={AdminResourceList}
        create={isAuthorized ? AdminResourceCreate : undefined}
        edit={isAuthorized ? AdminResourceEdit : undefined}
        options={{ label: "Resources" }}
      />
      <Resource
        name="city"
        list={AdminCityList}
        create={isAuthorized ? AdminCityCreate : undefined}
        edit={isAuthorized ? AdminCityEdit : undefined}
        options={{ label: "Communes" }}
      />
    </Admin>
  );
};
