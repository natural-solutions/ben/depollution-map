import { AccountCircle, Logout } from "@mui/icons-material";
import AdminPanelSettingsRoundedIcon from "@mui/icons-material/AdminPanelSettingsRounded";
import {
  Button,
  ClickAwayListener,
  IconButton,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import React, { FC, useState } from "react";

type SessionBtnProps = {
  isMobile?: boolean;
};

export const SessionBtn: FC<SessionBtnProps> = (props) => {
  const [isTooltipOpen, setIsTooltipOpen] = useState(false);
  const router = useRouter();

  const session = useSession();
  const handleSignOut = () => {
    signOut();
    router.push("/");
  };

  return (
    <>
      {session.status !== "authenticated" || !Boolean(session.data) ? (
        <></>
      ) : (
        <ClickAwayListener onClickAway={() => setIsTooltipOpen(false)}>
          <div>
            <Tooltip
              onClose={() => setIsTooltipOpen(false)}
              open={isTooltipOpen}
              arrow
              placement="bottom-end"
              PopperProps={{
                className: "tooltip-light",
                disablePortal: true,
              }}
              disableFocusListener
              disableHoverListener
              disableTouchListener
              title={
                <Stack spacing={1} sx={{ p: 1 }}>
                  <Typography>{session.data.user?.name}</Typography>
                  {props.isMobile && (
                    <Button variant="contained" color="primary" href="/profil">
                      Profil
                    </Button>
                  )}
                  <Button
                    variant="contained"
                    color="success"
                    href="/admin"
                    endIcon={<AdminPanelSettingsRoundedIcon />}
                  >
                    Espace Admin
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    onClick={handleSignOut}
                    endIcon={<Logout />}
                  >
                    Déconnexion
                  </Button>
                </Stack>
              }
            >
              <IconButton
                size="medium"
                style={
                  props.isMobile
                    ? { marginRight: "30px", marginTop: "7px" }
                    : { marginRight: "-12px" }
                }
                color="inherit"
                onClick={() => setIsTooltipOpen(!isTooltipOpen)}
              >
                <AccountCircle color="inherit" />
              </IconButton>
            </Tooltip>
          </div>
        </ClickAwayListener>
      )}
    </>
  );
};
