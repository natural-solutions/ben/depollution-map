"use client";

import dynamic from "next/dynamic";
import type { ApexOptions } from "apexcharts";
import { useMediaQuery, useTheme } from "@mui/material";
import { FC } from "react";

const AppReactApexCharts = dynamic(
  () => import("@/libs/styles/AppReactApexCharts")
);

export interface BarChartProps {
  data: {
    series: { data: number[]; name?: string; color: string }[];
    xaxis: any;
  };
  height: string | number;
  isCampaignRubbishGraph?: boolean;
  isOriginOfRubbishGraph?: boolean;
  yAxisLabelsFormatter?: Required<ApexYAxis>["labels"]["formatter"];
  tooltipXFormatter?: Required<ApexTooltip>["x"]["formatter"];
  tooltipYFormatter?: ApexTooltipY["formatter"];
  hideYAxis?: boolean;
  isStacked?: boolean;
  yAxis?: ApexYAxis | ApexYAxis[];
  legendFormatter?: Required<ApexOptions>["legend"]["formatter"];
}

const BarChart: FC<BarChartProps> = ({
  data,
  height,
  isCampaignRubbishGraph,
  isOriginOfRubbishGraph,
  yAxisLabelsFormatter,
  tooltipYFormatter,
  tooltipXFormatter,
  hideYAxis,
  isStacked = true,
  yAxis,
  legendFormatter,
}) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  const disabledText = "#b0aeb3";

  const options: ApexOptions = {
    chart: {
      stacked: isStacked,
      parentHeightOffset: 0,
      toolbar: { show: false },
    },
    plotOptions: {
      bar: {
        borderRadius: 10,
        columnWidth: isCampaignRubbishGraph ? "15%" : "50%",
        borderRadiusApplication: "around",
        borderRadiusWhenStacked: "all",
      },
    },
    colors: ["#3EA0EA"],
    grid: {
      borderColor: isCampaignRubbishGraph ? disabledText : "white",
      strokeDashArray: 10,
      padding: { top: isMobile ? -35 : -40, left: -3, bottom: -5 },
    },
    dataLabels: { enabled: false },
    legend: {
      itemMargin: { horizontal: isMobile ? 0 : 15 },
      fontSize: "15",
      offsetY: 8,
      position: "bottom",
      formatter: legendFormatter,
    },
    stroke: {
      width: 6,
      curve: "smooth",
      lineCap: "round",
      colors: [theme.palette.background.paper],
    },
    states: {
      hover: {
        filter: { type: "none" },
      },
      active: {
        filter: { type: "none" },
      },
    },
    tooltip: {
      enabled: true,
      y: {
        formatter: tooltipYFormatter
          ? tooltipYFormatter
          : (value) =>
              isCampaignRubbishGraph ? `${value}%` : `${value} unités`,
      },
      x: {
        formatter: tooltipXFormatter
          ? tooltipXFormatter
          : (val) => {
              if (isOriginOfRubbishGraph) {
                const item = data.xaxis.find((item: any) => item.label === val);
                const rubbishes = item
                  ? item.rubbishes.slice(0, 14).join("<br>")
                  : "";
                return (
                  val +
                  (rubbishes.length > 0 ? `<br>Déchets :<br>${rubbishes}` : "")
                );
              }
              return val.toString();
            },
      },
    },
    xaxis: {
      axisTicks: { show: false },
      axisBorder: { show: false },
      categories: isOriginOfRubbishGraph
        ? data.xaxis.map((item: any) => item.label)
        : data.xaxis,
      labels: {
        trim: true,
        rotate: 0,
        hideOverlappingLabels: false,
        style: {
          colors: disabledText,
          fontSize: theme.typography.body2.fontSize as string,
        },
      },
    },
    yaxis: yAxis || {
      show: !hideYAxis, //isCampaignRubbishGraph,
      min: 0,
      labels: {
        formatter: yAxisLabelsFormatter
          ? yAxisLabelsFormatter
          : (value: number) => value.toString(),
        style: {
          colors: disabledText,
          fontSize: theme.typography.body2.fontSize as string,
        },
      },
    },
    responsive: [
      {
        breakpoint: 1350,
        options: {
          plotOptions: {
            bar: {
              columnWidth: "65%",
            },
          },
        },
      },
      {
        breakpoint: theme.breakpoints.values.lg,
        options: {
          plotOptions: {
            bar: {
              columnWidth: "65%",
            },
          },
        },
      },
      {
        breakpoint: theme.breakpoints.values.md,
        options: {
          plotOptions: {
            bar: {
              borderRadius: 8,
              columnWidth: "50%",
            },
          },
        },
      },
      {
        breakpoint: 700,
        options: {
          plotOptions: {
            bar: {
              borderRadius: 6,
              columnWidth: "55%",
            },
          },
        },
      },
      {
        breakpoint: theme.breakpoints.values.sm,
        options: {
          plotOptions: {
            bar: {
              borderRadius: 8,
              columnWidth: "40%",
            },
          },
        },
      },
      {
        breakpoint: 500,
        options: {
          plotOptions: {
            bar: {
              columnWidth: "55%",
            },
          },
        },
      },
      {
        breakpoint: 400,
        options: {
          plotOptions: {
            bar: {
              borderRadius: 7,
              columnWidth: "60%",
            },
          },
        },
      },
    ],
  };

  return (
    <AppReactApexCharts
      type="bar"
      height={height}
      width="100%"
      series={data.series}
      options={options}
    />
  );
};

export default BarChart;
