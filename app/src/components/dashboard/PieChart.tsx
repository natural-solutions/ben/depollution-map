"use client";

import dynamic from "next/dynamic";
import { useTheme } from "@mui/material/styles";
import type { ApexOptions } from "apexcharts";
import { Box, Grid, Typography, useMediaQuery } from "@mui/material";
import { Circle, TripOrigin } from "@mui/icons-material";
import { getWeightFormat } from "@/utils/cleanups";
import { round } from "lodash";

const AppReactApexCharts = dynamic(
  () => import("@/libs/styles/AppReactApexCharts")
);

interface PieChartProps {
  data: {
    label: string;
    value: number;
  }[];
  isOnlyDashboard?: boolean;
  totalWeight: number;
}

const PieChart = (props: PieChartProps) => {
  const { data, isOnlyDashboard, totalWeight } = props;

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("lg"));

  const isEmpty = data.filter((item) => item.value !== 0).length === 0;

  const options: ApexOptions = {
    chart: { sparkline: { enabled: true } },
    colors: data.map((item: any) => item.color),
    stroke: { width: 0 },
    legend: { show: false },
    dataLabels: { enabled: false },
    labels: data.map((item: any) => item.label),

    tooltip: {
      enabled: true,
      y: {
        formatter: (value) => `${((value * 100) / totalWeight).toFixed(2)}%`,
      },
    },
    plotOptions: {
      pie: {
        customScale: 0.9,
        donut: {
          size: "70%",
          labels: {
            show: true,
            name: { offsetY: 25 },
            value: {
              offsetY: -15,
              fontWeight: 500,
              fontSize: "24px",
              formatter: (value) => getWeightFormat(parseInt(value)).label,
            },
            total: {
              show: true,
              label: "",
              formatter: (value) => {
                const totalWeight = value.globals.seriesTotals.reduce(
                  (total: number, num: number) => total + num
                );
                return getWeightFormat(totalWeight).label;
              },
            },
          },
        },
      },
    },
    responsive: [
      {
        breakpoint: 1300,
        options: { chart: { height: 257 } },
      },
      {
        breakpoint: theme.breakpoints.values.lg,
        options: { chart: { height: 276 } },
      },
      {
        breakpoint: 1050,
        options: { chart: { height: 250 } },
      },
    ],
  };

  return (
    <Grid container spacing={1}>
      <Grid item xs={isOnlyDashboard || isMobile ? 12 : 7}>
        {isEmpty ? (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              position: "relative",
              height: 250,
              width: "100%",
            }}
          >
            <Typography
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
              }}
            >
              x kg
            </Typography>
            <TripOrigin
              sx={{
                color: "#F0F2F8",
                height: 250,
                width: "100%",
                position: "absolute",
              }}
            />
          </Box>
        ) : (
          <AppReactApexCharts
            type="donut"
            height={isMobile ? 10 : 250}
            width="100%"
            series={data.map((item: any) => item.value)}
            options={options}
          />
        )}
      </Grid>
      <Grid
        item
        xs={isOnlyDashboard || isMobile ? 12 : 5}
        sx={{ mt: isOnlyDashboard || isMobile ? 0 : 4 }}
      >
        <Grid container spacing={1}>
          {data.map((item: any, index: number) => (
            <Grid key={item.label} item xs={12}>
              <Box display="flex">
                <Circle
                  sx={{ color: item.color, fontSize: "15px", mt: "5px" }}
                />
                <Typography variant="body2" sx={{ ml: 1 }}>
                  {item.labelHTML}
                  {/* <b>{round((item.value / totalWeight) * 100, 2)}%</b> */}
                </Typography>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default PieChart;
