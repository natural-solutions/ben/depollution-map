import { Cleanup_View } from "@/graphql/types";
import { useApi } from "@/utils/useApi";
import {
  Box,
  Card,
  Grid,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { FC, useEffect, useState } from "react";
import BarChart from "./BarChart";
import PieChart from "./PieChart";
import { round } from "lodash";
import { getWeightFormat, printNumber } from "@/utils/cleanups";

interface DashboardComponentProps {
  title?: string;
  cleanups: Cleanup_View[];
  isOnlyDashboard?: boolean;
}

const FocusItem: FC<{
  nb: number;
  unit?: string;
  label: string;
  icon: string;
  isOnlyDashboard?: boolean;
}> = ({ nb, unit, label, icon, isOnlyDashboard }) => (
  <Grid
    item
    xs={6}
    sm={4}
    md={isOnlyDashboard ? 3 : 6}
    xl={4}
    sx={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    }}
  >
    <img
      style={{
        width: "60px",
        height: "54px",
        display: "block",
        objectFit: "contain",
      }}
      src={`/assets/icons/dashboard/${icon}`}
    />
    <Typography
      variant="caption"
      sx={{ textAlign: "center", lineHeight: "20px", mt: 1 }}
    >
      <b style={{ whiteSpace: "nowrap", display: "block" }}>
        {printNumber(nb)}
        {unit}
      </b>{" "}
      {label}
    </Typography>
  </Grid>
);

const InlineItem: FC<{
  nb: number;
  label: string;
  icon: string;
}> = ({ nb, label, icon }) => (
  <Stack direction="row" spacing={1}>
    <img
      src={`/assets/icons/rubbish/${icon}`}
      style={{
        display: "block",
        width: `22px`,
        height: "22px",
        objectFit: "contain",
      }}
      onError={(e) => {
        (e.target as HTMLImageElement).style.visibility = "hidden";
      }}
    />
    <Typography>
      <b
        style={{
          fontWeight: 600,
          whiteSpace: "nowrap",
        }}
      >
        {printNumber(nb)}
      </b>{" "}
      {label}
    </Typography>
  </Stack>
);

export const DashboardComponent: FC<DashboardComponentProps> = ({
  title = "Dashboard",
  cleanups,
  isOnlyDashboard,
}) => {
  const { api } = useApi();

  const [basicStats, setBasicStats] = useState({
    butts: 0,
    food_packaging: 0,
    liters: 0,
    kgPerVolunteer: 0,
    mermaid_tears: 0,
    rubbishesKg: 0,
    totalHours: 0,
  });
  const [rubbishPerCampaign, setRubbishPerCampaign] = useState({
    xaxis: [""],
    series: [{ name: "", data: [0] }],
  });
  const [rubbishPerCategory, setRubbishPerCategory] = useState({
    data: [],
    totalWeight: 0,
  });
  const [mainRubbish, setMainRubbish] = useState<
    { id: string; label: string; value: number }[]
  >([]);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  const buildBarChart = () => {
    const label: string[] = [];
    const data: number[] = [];

    if (!mainRubbish.length) {
      label.push("Pas de données");
      data.push(0);
    } else {
      mainRubbish.forEach((r) => {
        label.push(r.label);
        data.push(r.value);
      });
    }
    return { xaxis: label.splice(0, 5), series: [{ data: data.splice(0, 5) }] };
  };

  const barChartData = buildBarChart();

  useEffect(() => {
    (async () => {
      const cleanups_id = cleanups.map((cleanup) => cleanup.id);

      const resp = await api.post("/dashboard", {
        ids: cleanups_id,
      });
      const respBasicStats = resp.data.basicStats;
      const respRubbishPerCampaign = resp.data.rubbishPerCampaign;
      const respRubbishPerCategory = resp.data.rubbishPerCategory;
      respRubbishPerCategory.data = respRubbishPerCategory.data.map(
        (item: any) => ({
          ...item,
          label: item.label,
          labelHTML: (
            <>
              {item.label} <b>{getWeightFormat(item.value).label}</b>
            </>
          ),
        })
      );
      const respMainRubbish = resp.data.mainRubbish;

      const respSeries = Object.entries(respRubbishPerCampaign.series);
      const totalPerMission = respRubbishPerCampaign.stack.map(
        (_: any, index: number) => {
          return respSeries
            .map((item: any) => item[1].weights[index] || 0)
            .reduce((acc, val) => acc + val);
        }
      );
      const series = respSeries.map((item: any) => {
        let data: any = [];
        if (Array.isArray(item[1].weights)) {
          data = item[1].weights.map((i: number, id: number) =>
            round((i * 100) / totalPerMission[id], 1)
          );
        }
        const catData = respRubbishPerCategory.data.find(
          (cat: any) => cat.label.trim() === item[0].trim()
        );
        if (!catData) {
          return {
            name: item[0],
            data: data,
            color: item[1].color,
          };
        }
        return {
          name: catData.label,
          data: data,
          color: item[1].color,
        };
      });

      setBasicStats(respBasicStats);
      setRubbishPerCampaign({
        xaxis: respRubbishPerCampaign.stack,
        series: series,
      });
      setRubbishPerCategory(respRubbishPerCategory);
      setMainRubbish(respMainRubbish);
    })();
  }, [cleanups]);

  return (
    <Box
      sx={{
        p: 2,
        width: "100%",
      }}
    >
      {Boolean(title) && (
        <Typography variant="h4" sx={{ mb: isOnlyDashboard ? 5 : 4 }}>
          {title}
        </Typography>
      )}

      {/**
       * ======================================================================
       * Basic stats
       * ======================================================================
       */}
      {Boolean(cleanups?.length) ? (
        <>
          <Box
            sx={{
              background: "#e2f3ff",
            }}
          >
            <Grid
              container
              spacing={2}
              rowSpacing={2}
              sx={{
                pb: 2,
                width: "100%",
                justifyContent: "center",
              }}
            >
              <FocusItem
                icon="focus_nombre_ramassage-min.png"
                nb={cleanups.length}
                unit=" actions"
                label="de dépollution"
                isOnlyDashboard={isOnlyDashboard}
              />
              <FocusItem
                icon="focus_poids_ramasse-min.png"
                nb={getWeightFormat(basicStats.rubbishesKg).value}
                unit={" " + getWeightFormat(basicStats.rubbishesKg).unitLabel}
                label="ramassées"
                isOnlyDashboard={isOnlyDashboard}
              />
              <FocusItem
                icon="focus_nombre_participants-min.png"
                nb={basicStats.totalHours}
                unit=" heures"
                label="de ramassage"
                isOnlyDashboard={isOnlyDashboard}
              />
            </Grid>
          </Box>

          <Grid container spacing={2} sx={{ mt: 0 }}>
            {/**
             * ======================================================================
             * Rubbish category chart
             * ======================================================================
             */}
            <Grid
              item
              xs={isOnlyDashboard && !isMobile ? 3 : 12}
              sx={{
                display: isOnlyDashboard ? "flex" : "block",
              }}
            >
              <Card
                sx={{
                  p: 2,
                  width: "100%",
                  //height: isMobile || isOnlyDashboard ? 510 : "auto",
                }}
              >
                <Typography variant="h5">Déchets</Typography>
                <PieChart
                  data={rubbishPerCategory.data}
                  isOnlyDashboard={isOnlyDashboard}
                  totalWeight={rubbishPerCategory.totalWeight}
                />
              </Card>
            </Grid>

            {/**
             * ======================================================================
             * Waste per campaign
             * ======================================================================
             */}
            {(isOnlyDashboard || isMobile) && (
              <Grid
                item
                xs={isMobile ? 12 : 9}
                sx={{
                  display: isOnlyDashboard ? "flex" : "block",
                }}
              >
                <Card
                  sx={{
                    //height: 510,
                    width: "100%",
                    mt: isOnlyDashboard ? 0 : 2,
                    p: 2,
                  }}
                >
                  <Typography variant="h5">
                    Déchets par entités wings
                  </Typography>
                  {rubbishPerCampaign.xaxis.length > 0 && (
                    <BarChart
                      data={{
                        ...rubbishPerCampaign,
                        xaxis: rubbishPerCampaign.xaxis.slice(0, 8),
                        series: rubbishPerCampaign.series.map((serie) => ({
                          ...serie,
                          data: serie.data.slice(0, 8),
                          color:
                            (
                              serie as {
                                name: string;
                                data: number[];
                                color?: string;
                              }
                            ).color ?? "#3DA0EA",
                        })),
                      }}
                      height={450}
                      isCampaignRubbishGraph={true}
                      hideYAxis={true}
                    />
                  )}
                </Card>
              </Grid>
            )}

            {/**
             * ======================================================================
             * Frequent waste
             * ======================================================================
             */}
            <Grid
              item
              xs={isOnlyDashboard && !isMobile ? 3 : 12}
              sx={{ display: "flex" }}
            >
              <Card sx={{ p: 2, width: "100%" }}>
                <Stack direction="column" spacing={2}>
                  <Typography variant="h5">Déchets récurents</Typography>
                  <InlineItem
                    nb={basicStats.mermaid_tears}
                    label="larmes de sirènes"
                    icon="mermaid_tears.png"
                  />
                  <InlineItem
                    nb={basicStats.butts}
                    label="mégots"
                    icon="butts.png"
                  />
                  <InlineItem
                    nb={basicStats.food_packaging}
                    label="emballages"
                    icon="food_packaging.png"
                  />
                </Stack>
              </Card>
            </Grid>

            {/**
             * ======================================================================
             * Top 5 waste indicators
             * ======================================================================
             */}
            {(!isOnlyDashboard || isMobile) && (
              <Grid item xs={12}>
                <Card sx={{ mt: 1, p: 2 }}>
                  <Typography variant="h5">
                    Top 5 des déchets indicateurs
                  </Typography>
                  <BarChart
                    data={{
                      ...barChartData,
                      series: barChartData.series.map((serie) => ({
                        ...serie,
                        color: "#3DA0EA",
                      })),
                    }}
                    height={isMobile ? 400 : 310}
                  />
                </Card>
              </Grid>
            )}

            {/**
             * ======================================================================
             * Top waste
             * ======================================================================
             */}
            {isOnlyDashboard && !isMobile && (
              <Grid item xs={9} sx={{ display: "flex" }}>
                <Card sx={{ p: 2, width: "100%" }}>
                  <Typography variant="h5">
                    Top des déchets indicateurs
                  </Typography>
                  <Grid container>
                    {mainRubbish
                      .filter((r) => r.value)
                      .map((r) => (
                        <Grid
                          key={r.id}
                          item
                          xs={4}
                          display="flex"
                          sx={{ mt: 2 }}
                        >
                          <InlineItem
                            nb={r.value}
                            label={r.label}
                            icon={`${r.id}.png`}
                          />
                        </Grid>
                      ))}
                  </Grid>
                </Card>
              </Grid>
            )}
          </Grid>
        </>
      ) : (
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            height: "80vh",
          }}
        >
          <Typography sx={{ mb: 1, fontWeight: "bold" }} variant="h5">
            Aucun résultat
          </Typography>
          <Typography variant="body2">
            Modifiez ou supprimez certains de vos filtres
          </Typography>
        </Box>
      )}
      <Typography
        sx={{
          textAlign: "center",
          fontSize: "10px",
          fontFamily: "Poppins",
          mt: 2,
        }}
      >
        <a
          href="https://www.natural-solutions.eu"
          target="_blank"
          style={{
            textDecoration: "none",
            color: "rgba(0, 0, 0, 0.87)",
          }}
        >
          Développé par Natural Solutions
        </a>
      </Typography>
    </Box>
  );
};
