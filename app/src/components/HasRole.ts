import { Cleanup } from "@/graphql/types";
import { useSessionContext } from "@/utils/useSessionContext";
import { USER_ROLE } from "@/utils/user";
import { FC, PropsWithChildren } from "react";

type HasRoleProps = PropsWithChildren & {
  role?: USER_ROLE | USER_ROLE[];
  orCanCreateCleanup?: boolean;
  orCanEditCleanup?: Cleanup;
};

export const HasRole: FC<HasRoleProps> = ({
  role,
  orCanCreateCleanup,
  orCanEditCleanup,
  children,
}) => {
  const sessionCtx = useSessionContext();
  if (role && sessionCtx.hasRole(role)) {
    return children;
  }
  if (orCanCreateCleanup && sessionCtx.getCanCreateCleanup()) {
    return children;
  }
  if (orCanEditCleanup && sessionCtx.getCanEditCleanup(orCanEditCleanup)) {
    return children;
  }
  return null;
};
