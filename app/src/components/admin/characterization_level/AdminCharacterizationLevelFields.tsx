import { Characterization_Level } from "@/graphql/types";
import { Box, Stack, Typography } from "@mui/material";
import React, { FC } from "react";
import {
  SaveButton,
  TextInput,
  Toolbar,
  required,
  useRecordContext,
} from "react-admin";

type AdminCharacterizationLevelFieldsProps = {};

export const AdminCharacterizationLevelFields: FC<
  AdminCharacterizationLevelFieldsProps
> = ({}) => {
  const record = useRecordContext<Characterization_Level>();
  return (
    <Stack p={2}>
      <Stack alignItems={"start"}>
        {record?.id ? (
          <Typography>ID: {record.id}</Typography>
        ) : (
          <TextInput source="id" validate={required()} />
        )}

        <TextInput source="label" label="Nom" validate={required()} />
      </Stack>
      <Box>
        <Toolbar>
          <SaveButton alwaysEnable={true} />
        </Toolbar>
      </Box>
    </Stack>
  );
};
