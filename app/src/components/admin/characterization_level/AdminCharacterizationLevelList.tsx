import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  Datagrid,
  EditButton,
  Filter,
  List,
  TextField,
  TextInput,
} from "react-admin";

const Filters = (props: any) => (
  <Filter {...props}>
    <TextInput label="Nom" source="label" />
  </Filter>
);

type AdminCharacterizationLevelListProps = {};

export const AdminCharacterizationLevelList: FC<
  AdminCharacterizationLevelListProps
> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <List filters={<Filters />}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Nom" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
