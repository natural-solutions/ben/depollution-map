import { useApi } from "@/utils/useApi";
import { EditBase, Form } from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { AdminCharacterizationLevelFields } from "./AdminCharacterizationLevelFields";
import { Characterization_Level } from "@/graphql/types";

export const AdminCharacterizationLevelEdit = () => {
  const { id } = useParams();
  const { api } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (formData: Characterization_Level) => {
    if (!id) {
      return;
    }

    try {
      await api.patch(`/characterization_level/${id}`, {
        ...pick(formData, ["label"]),
      });

      navigate("/characterization_level");
    } catch (error) {}
  };

  return (
    <EditBase>
      <Form onSubmit={onSubmit as any}>
        <AdminCharacterizationLevelFields />
      </Form>
    </EditBase>
  );
};
