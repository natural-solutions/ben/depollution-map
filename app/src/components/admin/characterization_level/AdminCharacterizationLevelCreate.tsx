import { INSERT_CHARACTERIZATION_LEVEL } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { CreateBase, Form } from "react-admin";
import { useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { AdminCharacterizationLevelFields } from "./AdminCharacterizationLevelFields";
import { Characterization_Level } from "@/graphql/types";

export const AdminCharacterizationLevelCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (formData: Characterization_Level) => {
    try {
      const response = await postGraphql<any>({
        query: INSERT_CHARACTERIZATION_LEVEL,
        variables: {
          _set: {
            ...pick(formData, ["id", "label"]),
          },
        },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      navigate("/characterization_level");
    } catch (error) {}
  };

  return (
    <CreateBase>
      <Form onSubmit={onSubmit as any}>
        <AdminCharacterizationLevelFields />
      </Form>
    </CreateBase>
  );
};
