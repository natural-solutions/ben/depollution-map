import { useApi } from "@/utils/useApi";
import { Edit } from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { Resource_Set_Input } from "@/graphql/types";
import { slugify, uploadFile } from "@/utils/media";
import { AdminResourceForm } from "./AdminResourceForm";

export const AdminResourceEdit = () => {
  const { api, notifyApiError } = useApi();
  const { id } = useParams();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    if (!id) {
      return;
    }

    if (_set.attachments?.title === "drop_file") {
      _set.filename = null;
    }

    const file = _set.attachments ? _set.attachments?.rawFile : null;
    if (file) {
      file.slugified = slugify(file.name);
      _set.filename = file.slugified;
      delete _set.attachments;
    }

    try {
      const response = await api.patch(`/resources/${id}`, {
        ...pick(_set, ["label", "description", "filename"]),
      } as Resource_Set_Input);

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      if (file) {
        await uploadFile("resources", id, file);
      }

      navigate("/resource");
    } catch (error: any) {
      notifyApiError(error);
    }
  };

  return (
    <Edit>
      <AdminResourceForm onSubmit={onSubmit} />
    </Edit>
  );
};
