import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  Datagrid,
  DateField,
  EditButton,
  List,
  TextField,
  TextInput,
} from "react-admin";

const filters = [<TextInput key="a" label="Nom" source="label" />];

type AdminResourceListProps = {};

export const AdminResourceList: FC<AdminResourceListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Nom" />
        <TextField source="filename" label="Fichier" />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
