import { INSERT_RESOURCE } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { Create } from "react-admin";
import { useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { Resource } from "@/graphql/types";
import { slugify, uploadFile } from "@/utils/media";
import { AdminResourceForm } from "./AdminResourceForm";

export const AdminResourceCreate = () => {
  const { postGraphql } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    const file = _set.attachments?.rawFile;
    if (file) {
      file.slugified = slugify(file.name);
      _set.filename = file.slugified;
      delete _set.attachments;
    }

    try {
      const { data } = await postGraphql<{
        data: {
          insert_resource_one: Resource;
        };
      }>({
        query: INSERT_RESOURCE,
        variables: {
          _set: pick(_set, ["id", "label", "description", "filename"]),
        },
      });

      const id = data.data.insert_resource_one.id;

      if (file) {
        try {
          await uploadFile("resources", id, file);
        } catch (error) {
          console.error("Erreur lors du téléchargement du fichier :", error);
        }
      }

      navigate("/resource");
    } catch (error) {
      console.error("Erreur lors de la création de la ressource :", error);
    }
  };

  return (
    <Create>
      <AdminResourceForm onSubmit={onSubmit} />
    </Create>
  );
};
