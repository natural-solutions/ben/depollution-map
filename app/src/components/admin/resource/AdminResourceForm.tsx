import {
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
  useRecordContext,
} from "react-admin";
import { useFormContext } from "react-hook-form";
import { Partner } from "@/graphql/types";
import { FC } from "react";
import { ReactAdminImageInputReturn } from "@/utils/media";
import { AdminPdfComponent } from "../shared/AdminPdfComponent";

export type AdminResourceFormData = Partner & {
  selectLogo?: ReactAdminImageInputReturn;
};

type AdminResourceFormProps = {
  onSubmit: (data: AdminResourceFormData) => void;
};

const FormComponent: FC<AdminResourceFormProps> = (props) => {
  const formContext = useFormContext();
  const record = useRecordContext<Partner>();

  return (
    <>
      <TextInput source="label" label="Nom" validate={required()} />
      <TextInput source="description" multiline />
      <AdminPdfComponent formContext={formContext} record={record} />
    </>
  );
};

export const AdminResourceForm: FC<AdminResourceFormProps> = (props) => (
  <SimpleForm
    sx={{
      maxWidth: "600px",
    }}
    onSubmit={props.onSubmit as any}
    toolbar={
      <Toolbar>
        <SaveButton alwaysEnable />
      </Toolbar>
    }
  >
    <FormComponent {...props} />
  </SimpleForm>
);
