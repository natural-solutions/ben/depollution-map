import { UPDATE_CLEANUP_PLACE } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import {
  AutocompleteInput,
  Edit,
  ReferenceInput,
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
} from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";

export const AdminCleanupPlaceEdit = () => {
  const { id } = useParams();
  const { postGraphql } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    if (!id) {
      return;
    }

    try {
      await postGraphql({
        query: UPDATE_CLEANUP_PLACE,
        variables: { id, _set: pick(_set, ["id", "label", "city_id"]) },
      });

      navigate("/cleanup_place");
    } catch (error) {}
  };

  return (
    <Edit>
      <SimpleForm
        onSubmit={onSubmit}
        toolbar={
          <Toolbar>
            <SaveButton />
          </Toolbar>
        }
      >
        <TextInput source="id" disabled />
        <TextInput source="label" label="Nom du lieu" validate={required()} />
        <ReferenceInput
          source="city_id"
          reference="city"
          sort={{ field: "label", order: "ASC" }}
        >
          <AutocompleteInput
            label="Commune"
            optionText={(choice) =>
              `${choice.label} ${choice.departement_id ? `(${choice.departement_id})` : ""}`
            }
            optionValue="id"
            filterToQuery={(search) =>
              search ? { "label@_ilike": search } : {}
            }
            validate={required()}
          />
        </ReferenceInput>
      </SimpleForm>
    </Edit>
  );
};
