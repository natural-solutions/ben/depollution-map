import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  AutocompleteArrayInput,
  CreateButton,
  Datagrid,
  DateField,
  EditButton,
  ExportButton,
  FilterButton,
  List,
  ReferenceArrayInput,
  ReferenceField,
  TextField,
  TextInput,
  TopToolbar,
} from "react-admin";

const filters = [
  <TextInput key="a" label="Lieu" source="label" />,
  <ReferenceArrayInput
    key="b"
    label="Commune"
    source="city_id"
    reference="city"
    sort={{ field: "label", order: "ASC" }}
  >
    <AutocompleteArrayInput
      label="Commune"
      optionText="label"
      optionValue="id"
      filterToQuery={(search) => (search ? { "label@_ilike": search } : {})}
    />
  </ReferenceArrayInput>,
];

type AdminCleanupPlaceListProps = {};

export const AdminCleanupPlaceList: FC<AdminCleanupPlaceListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  const AdminCleanupPlaceListActions = () => (
    <TopToolbar>
      <FilterButton filters={filters} />
      <CreateButton />
      {isAuthorized && <ExportButton />}
    </TopToolbar>
  );

  return (
    <List filters={filters} actions={<AdminCleanupPlaceListActions/>}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Lieu" />
        <ReferenceField source="city_id" reference="city" label="Commune">
          <TextField source="label" />
        </ReferenceField>
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
