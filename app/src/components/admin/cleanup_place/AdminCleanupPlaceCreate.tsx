import { INSERT_CLEANUP_PLACE } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import {
  AutocompleteInput,
  Create,
  ReferenceInput,
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
} from "react-admin";
import { pick } from "lodash";
import { useNavigate } from "react-router-dom";

export const AdminCleanupPlaceCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    try {
      const response = await postGraphql<any>({
        query: INSERT_CLEANUP_PLACE,
        variables: { _set: pick(_set, ["id", "label", "city_id"]) },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      navigate("/cleanup_place");
    } catch (error) {}
  };

  return (
    <Create>
      <SimpleForm
        sx={{
          maxWidth: "400px",
        }}
        onSubmit={onSubmit}
        toolbar={
          <Toolbar>
            <SaveButton />
          </Toolbar>
        }
      >
        <TextInput
          source="id"
          validate={required()}
          parse={(value) => value.trim()}
        />
        <TextInput source="label" label="Nom du lieu" validate={required()} />
        <ReferenceInput
          source="city_id"
          reference="city"
          sort={{ field: "label", order: "ASC" }}
        >
          <AutocompleteInput
            label="Commune"
            optionText={(choice) =>
              `${choice.label} (${choice.departement_id})`
            }
            optionValue="id"
            filterToQuery={(search) =>
              search ? { "label@_ilike": search } : {}
            }
            validate={required()}
          />
        </ReferenceInput>
      </SimpleForm>
    </Create>
  );
};
