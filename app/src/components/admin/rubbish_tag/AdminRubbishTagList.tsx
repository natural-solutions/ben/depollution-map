import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  Datagrid,
  DateField,
  EditButton,
  List,
  TextField,
  TextInput,
} from "react-admin";

const filters = [<TextInput key="a" label="Tags de déchets" source="label" />];

type AdminRubbishTagListProps = {};

export const AdminRubbishTagList: FC<AdminRubbishTagListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Tags de déchets" />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
