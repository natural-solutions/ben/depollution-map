import { UPDATE_RUBBISH_TAG } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import {
  Edit,
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
} from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";

export const AdminRubbishTagEdit = () => {
  const { id } = useParams();
  const { postGraphql } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    if (!id) {
      return;
    }

    try {
      await postGraphql({
        query: UPDATE_RUBBISH_TAG,
        variables: { id, _set: pick(_set, ["id", "label"]) },
      });

      navigate("/rubbish_tag");
    } catch (error) {}
  };

  return (
    <Edit>
      <SimpleForm
        sx={{
          maxWidth: "400px",
        }}
        onSubmit={onSubmit}
        toolbar={
          <Toolbar>
            <SaveButton />
          </Toolbar>
        }
      >
        <TextInput source="id" validate={required()} disabled />
        <TextInput
          source="label"
          label="Nom du tags de déchets"
          validate={required()}
        />
      </SimpleForm>
    </Edit>
  );
};
