import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  ArrayField,
  Datagrid,
  DateField,
  DateInput,
  EditButton,
  List,
  ReferenceArrayInput,
  ReferenceInput,
  SelectArrayInput,
  SelectInput,
  TextField,
  TextInput,
  WithListContext,
} from "react-admin";

const filters = [
  <TextInput key="a" label="Déchet" source="label" />,
  <ReferenceInput
  key="b"
  label="Catégorie"
  reference="rubbish_category"
  source="rubbish_category#id"
  perPage={10000000}
  >
    <SelectInput label="Catégorie" optionText="label" optionValue="id" />
  </ReferenceInput>,
  <ReferenceArrayInput
  key="c"
  label="Tags"
  reference="rubbish_tag"
  source="tags#rubbish_tag#id"
  perPage={10000000}
  >
    <SelectArrayInput label="Tag" optionText="label" optionValue="id" />
  </ReferenceArrayInput>,
  <DateInput key="d" label="Date de création =" source="created_at" />,
  <DateInput key="e" label="Date de création >=" source="created_at@_gte" />,
  <DateInput key="f" label="Date de création <" source="created_at@_lte" />,
];

type AdminRubbishListProps = {};

export const AdminRubbishList: FC<AdminRubbishListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Déchet" />
        <TextField label="Catégorie" source="rubbish_category.label" />
        <ArrayField source="tags" label="Tags" perPage={3} sortable={false}>
          <WithListContext
            render={(params) => {
              const nbMore = params.total - params.data.length;
              const perPage = 3;
              return (
                <span
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    params.setPerPage(
                      params.perPage != perPage ? perPage : 999999
                    );
                  }}
                >
                  {params.data.map((item) => item.rubbish_tag.label).join(", ")}
                  {nbMore > 0 && ` +${nbMore}`}
                </span>
              );
            }}
          />
        </ArrayField>
        <ArrayField
          source="cleanup_type_rubbishes"
          label="Type de formulaire"
          perPage={3}
          sortable={false}
        >
          <WithListContext
            render={(params) => {
              const nbMore = params.total - params.data.length;
              const perPage = 3;
              return (
                <span
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    params.setPerPage(
                      params.perPage != perPage ? perPage : 999999
                    );
                  }}
                >
                  {params.data.map((item) => item.cleanup_type.id).join(", ")}
                  {nbMore > 0 && ` +${nbMore}`}
                </span>
              );
            }}
          />
        </ArrayField>
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
