import { useApi } from "@/utils/useApi";
import { EditBase } from "react-admin";
import { useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { AdminRubbishForm } from "./AdminRubbishForm";
import { useParams } from "react-router-dom";
import { Rubbish } from "@/graphql/types";
import { RubbishPatch } from "@/graphql/queries";

export const AdminRubbishEdit = () => {
  const { id } = useParams();
  const { api } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: Rubbish) => {
    try {
      await api.patch(`/rubbishes/${id}`, {
        ...pick(_set, [
          "id",
          "id_zds",
          "label",
          "icon",
          "parent_id",
          "comment",
          "is_support_brand",
          "is_support_doi",
          "rubbish_category_id",
        ]),
        tags: _set.tags
          ?.filter((row) => row.rubbish_tag_id)
          .map(({ rubbish_tag_id }) => ({
            rubbish_tag_id,
          })),
        cleanup_type_rubbishes: _set.cleanup_type_rubbishes
          ?.filter(
            (row) => row.cleanup_type_id && (row.has_qty || row.has_wt_vol)
          )
          .map(({ cleanup_type_id, has_qty, has_wt_vol }) => ({
            cleanup_type_id,
            has_qty,
            has_wt_vol,
          })),
      } as RubbishPatch);

      navigate("/rubbish");
    } catch (error) {}
  };

  return (
    <EditBase>
      <AdminRubbishForm onSubmit={onSubmit} />
    </EditBase>
  );
};
