import { INSERT_RUBBISH } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { Create } from "react-admin";
import { useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { AdminRubbishForm } from "./AdminRubbishForm";
import { Rubbish } from "@/graphql/types";

export const AdminRubbishCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: Rubbish) => {
    try {
      const response = await postGraphql<any>({
        query: INSERT_RUBBISH,
        variables: {
          _set: {
            ...pick(_set, [
              "id",
              "id_zds",
              "label",
              "icon",
              "parent_id",
              "comment",
              "rubbish_category_id",
              "is_support_brand",
              "is_support_doi",
            ]),
            tags: {
              data: (_set.tags || [])
                .filter((row) => row.rubbish_tag_id)
                .map(({ rubbish_tag_id }) => ({
                  rubbish_tag_id,
                })),
            },
            cleanup_type_rubbishes: {
              data: (_set.cleanup_type_rubbishes || [])
                .filter(
                  (row) =>
                    row.cleanup_type_id && (row.has_qty || row.has_wt_vol)
                )
                .map(({ cleanup_type_id, has_qty, has_wt_vol }) => ({
                  cleanup_type_id,
                  has_qty,
                  has_wt_vol,
                })),
            },
          },
        },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      navigate("/rubbish");
    } catch (error) {}
  };

  return (
    <Create>
      <AdminRubbishForm onSubmit={onSubmit} />
    </Create>
  );
};
