import { Rubbish } from "@/graphql/types";
import { Box, Stack } from "@mui/material";
import React, { FC } from "react";
import {
  ArrayInput,
  BooleanInput,
  Form,
  ReferenceInput,
  SaveButton,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  Toolbar,
  required,
} from "react-admin";

interface IProps {
  onSubmit: (data: Rubbish) => void;
}

const FormComponent: FC<IProps> = () => {
  return (
    <Stack p={2}>
      <Stack>
        <TextInput
          source="id"
          validate={required()}
          parse={(value) => value.trim()}
        />
        <TextInput source="id_zds" parse={(value) => value.trim()} />
        <TextInput source="label" label="Nom du déchet" validate={required()} />
        <TextInput source="icon" />
        <ReferenceInput
          source={"rubbish_category_id"}
          reference="rubbish_category"
        >
          <SelectInput label="Catégorie" optionText="label" />
        </ReferenceInput>
        <ReferenceInput source={"parent_id"} reference="rubbish">
          <SelectInput optionText="label" />
        </ReferenceInput>
        <ArrayInput source="tags" label="Tags">
          <SimpleFormIterator inline>
            <ReferenceInput source="rubbish_tag_id" reference="rubbish_tag">
              <SelectInput
                label="Sélectionner un tag"
                optionText="label"
                validate={required()}
              />
            </ReferenceInput>
          </SimpleFormIterator>
        </ArrayInput>
        <BooleanInput source="is_support_brand" label="Marque" />
        <BooleanInput source="is_support_doi" label="Date" />
        <ArrayInput source="cleanup_type_rubbishes" label="Type de formulaire">
          <SimpleFormIterator inline>
            <ReferenceInput
              source="cleanup_type_id"
              reference="cleanup_type"
              sort={{ field: "id", order: "ASC" }}
            >
              <SelectInput
                label="Sélectionner"
                optionText="label"
                validate={required()}
              />
            </ReferenceInput>
            <BooleanInput source="has_qty" label="Quantité" />
            <BooleanInput source="has_wt_vol" label="Volume" />
          </SimpleFormIterator>
        </ArrayInput>
        <TextInput source="comment" label="Commentaire" multiline />
      </Stack>
      <Box>
        <Toolbar>
          <SaveButton alwaysEnable={true} />
        </Toolbar>
      </Box>
    </Stack>
  );
};

export const AdminRubbishForm: FC<IProps> = (props) => (
  <Form onSubmit={props.onSubmit as any}>
    <FormComponent {...props} />
  </Form>
);
