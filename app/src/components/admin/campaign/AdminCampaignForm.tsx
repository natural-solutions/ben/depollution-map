import {
  ArrayInput,
  AutocompleteInput,
  FormDataConsumer,
  ReferenceInput,
  SaveButton,
  SelectInput,
  SimpleForm,
  SimpleFormIterator,
  TextInput,
  Toolbar,
  email,
  required,
  useRecordContext,
} from "react-admin";
import { useFormContext } from "react-hook-form";
import { Campaign, Campaign_User } from "@/graphql/types";
import { FC, useEffect, useState } from "react";
import { ReactAdminImageInputReturn } from "@/utils/media";
import { AdminPhotosComponent } from "../shared/AdminPhotosComponent";
import { USER_ROLE_CHOICES } from "@/utils/user";
import { Box, Fab, Stack, Tooltip, Typography } from "@mui/material";
import { regexValidation } from "@/utils/forms";

export type AdminCampaingFormData = Campaign & {
  selectLogo?: ReactAdminImageInputReturn;
};

type AdminCampaignFormProps = {
  onSubmit: (data: AdminCampaingFormData) => void;
  onRecordReady?: (record: Campaign) => void;
};

const FormComponent: FC<AdminCampaignFormProps> = (props) => {
  const formContext = useFormContext();
  const [isRecordReady, setIsRecordReady] = useState(false);
  const record = useRecordContext<Campaign>();
  const main_color = formContext.watch("main_color");

  useEffect(() => {
    if (record && !isRecordReady) {
      setIsRecordReady(true);
      props.onRecordReady?.(record);
    }
  }, [record, isRecordReady]);

  const colors = {
    Antenne: "#F0EFEC",
    BA: "#51B8CB",
    EDB: "#8FC3DC",
    EDT: "#2C5B8B",
    Kraken: "#4178E1",
    Scylla: "#5189CB",
  };

  return (
    <>
      <TextInput
        source="id"
        validate={required()}
        fullWidth
        parse={(value) => value.trim()}
      />
      <TextInput
        source="label"
        label="Nom de l'entité"
        validate={required()}
        fullWidth
      />
      <TextInput source="email" validate={[required(), email()]} fullWidth />
      <Stack direction="row" spacing={2}>
        {(Object.keys(colors) as (keyof typeof colors)[]).map((key) => (
          <Tooltip key={key} title={key} placement="top">
            <Fab
              size="small"
              sx={{
                background: colors[key],
                "&:hover": {
                  background: colors[key],
                },
              }}
              onClick={() => {
                formContext.setValue("main_color", colors[key]);
              }}
            ></Fab>
          </Tooltip>
        ))}
      </Stack>
      <Stack direction="row" sx={{ width: "100%" }}>
        <TextInput
          source="main_color"
          label="Couleur"
          sx={{ flex: 1 }}
          validate={regexValidation(
            /^#[0-9a-fA-F]{6}/,
            "Doit commencer par # suivit de 6 caractères"
          )}
        />
        <Box
          sx={{
            bgcolor: main_color,
            width: "48px",
            height: "48px",
            mt: 1,
            ml: 1,
            border: "1px solid #666666",
          }}
        ></Box>
      </Stack>

      <TextInput source="description" multiline fullWidth />
      <ArrayInput
        source="campaign_users"
        label="Utilisateurs"
        style={{
          width: "100%",
        }}
      >
        <SimpleFormIterator inline fullWidth disableClear disableReordering>
          <FormDataConsumer<Campaign_User>>
            {({ getSource, scopedFormData }) => (
              <>
                {scopedFormData?.campaign_id ? (
                  <Typography
                    sx={{
                      pt: 3,
                      flex: 1,
                    }}
                  >
                    {scopedFormData?.user_view?.fullname}
                  </Typography>
                ) : (
                  <ReferenceInput
                    source={getSource("user_view_id")}
                    reference="user_view"
                    sort={{ field: "fullname", order: "ASC" }}
                  >
                    <AutocompleteInput
                      label="Utilisateur"
                      optionText="fullname"
                      optionValue="id"
                      filterToQuery={(search) =>
                        search ? { "fullname@_like": search } : {}
                      }
                      style={{
                        flex: 1,
                      }}
                    />
                  </ReferenceInput>
                )}
              </>
            )}
          </FormDataConsumer>

          <SelectInput
            source="role"
            label="Rôle"
            choices={USER_ROLE_CHOICES}
            validate={required()}
            style={{
              flex: 1,
            }}
          />
        </SimpleFormIterator>
      </ArrayInput>
      <AdminPhotosComponent
        formContext={formContext}
        record={record}
        resourceName="campaigns"
      />
      <ReferenceInput source="default_cleanup_type_id" reference="cleanup_type">
        <SelectInput
          label="Formulaire de carac. par défaut"
          optionText="label"
          fullWidth
        />
      </ReferenceInput>
    </>
  );
};

export const AdminCampaignForm: FC<AdminCampaignFormProps> = (props) => (
  <SimpleForm
    sx={{
      maxWidth: "600px",
    }}
    onSubmit={props.onSubmit as any}
    toolbar={
      <Toolbar>
        <SaveButton alwaysEnable />
      </Toolbar>
    }
  >
    <FormComponent {...props} />
  </SimpleForm>
);
