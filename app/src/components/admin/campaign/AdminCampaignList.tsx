import React, { FC } from "react";
import {
  Datagrid,
  DateField,
  EditButton,
  List,
  TextField,
  TextInput,
  EmailField,
  DateInput,
  ArrayField,
  FunctionField,
} from "react-admin";
import { AdminCellArray } from "../shared/AdminCellArray";
import { useSessionContext } from "@/utils/useSessionContext";
import { Avatar, Stack, Typography } from "@mui/material";
import { Campaign } from "@/graphql/types";

const filters = [
  <TextInput key="a" label="Entités wings" source="label" />,
  <TextInput key="c" label="Email" source="email" />,
  <TextInput
    key="d"
    label="Utilsateurs"
    source="campaign_users#user_view#fullname@_ilike"
  />,
  <DateInput key="e" label="Date de création =" source="created_at" />,
  <DateInput key="f" label="Date de création >=" source="created_at@_gte" />,
  <DateInput key="g" label="Date de création <" source="created_at@_lte" />,
];

type AdminCampaignListProps = {};

export const AdminCampaignList: FC<AdminCampaignListProps> = () => {
  const session = useSessionContext();
  const isAuthorized = session.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Entités wings" />
        <EmailField source="email" />
        <FunctionField
          label="Couleur"
          render={(record: Campaign) => {
            return !record.main_color ? null : (
              <Stack direction="row" sx={{ alignItems: "center" }}>
                {record.main_color}
                <Avatar sx={{ bgcolor: record.main_color, ml: 1 }}>{""}</Avatar>
              </Stack>
            );
          }}
        />
        <ArrayField source="campaign_users" label="Utilisateurs" perPage={3} sortable={false}>
          <AdminCellArray labelPath="user_view.fullname" />
        </ArrayField>
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
