import { INSERT_CAMPAIGN } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { Create } from "react-admin";
import { useNavigate } from "react-router-dom";
import { AdminCampaignForm, AdminCampaingFormData } from "./AdminCampaignForm";
import { Campaign_Insert_Input } from "@/graphql/types";
import { slugify, uploadFile } from "@/utils/media";
import { pick } from "lodash";

export const AdminCampaignCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: AdminCampaingFormData) => {
    const file = _set.selectLogo?.rawFile;
    if (file) {
      file.slugified = slugify(file.name);
      _set.logo = file.slugified;
      delete _set.selectLogo;
    }

    try {
      const response = await postGraphql<any>({
        query: INSERT_CAMPAIGN,
        variables: {
          _set: {
            ...pick(_set, [
              "id",
              "label",
              "email",
              "main_color",
              "description",
              "logo",
              "default_cleanup_type_id",
            ]),
            campaign_users: !_set.campaign_users?.length
              ? undefined
              : {
                  data: _set.campaign_users?.map(({ user_view_id, role }) => ({
                    user_view_id,
                    role,
                  })),
                },
          } as Campaign_Insert_Input,
        },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      const id = response.data.data?.insert_campaign_one.id;

      if (id && file) {
        try {
          await uploadFile("campaigns", id, file);
        } catch (error) {}
      }

      navigate("/campaign");
    } catch (error) {}
  };

  return (
    <Create>
      <AdminCampaignForm onSubmit={onSubmit} />
    </Create>
  );
};
