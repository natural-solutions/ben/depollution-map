import { Edit, useNotify } from "react-admin";
import { useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { useApi } from "@/utils/useApi";
import { AdminCampaignForm, AdminCampaingFormData } from "./AdminCampaignForm";
import { slugify, uploadFile } from "@/utils/media";
import { Campaign } from "@/graphql/types";

export const AdminCampaignEdit = () => {
  const { api } = useApi();
  const { id } = useParams();
  const navigate = useNavigate();
  const notify = useNotify();
  const [_, setRecord] = useState<Campaign>();

  const onSubmit = async (_set: AdminCampaingFormData) => {
    if (!id) {
      return;
    }

    if (_set.selectLogo?.title === "drop_logo") {
      _set.logo = null;
    }

    const file = _set.selectLogo ? _set.selectLogo?.rawFile : null;
    if (file) {
      file.slugified = slugify(file.name);
      _set.logo = file.slugified;
      delete _set.selectLogo;
    }

    try {
      await api.patch(`/campaigns/${id}`, {
        ...pick(_set, [
          "label",
          "email",
          "main_color",
          "description",
          "logo",
          "default_cleanup_type_id",
        ]),
        campaign_users: _set.campaign_users.map(({ user_view_id, role }) => ({
          user_view_id,
          role,
        })),
      } as Campaign);

      if (file) {
        await uploadFile("campaigns", id, file);
      }

      navigate("/campaign");
    } catch (error: any) {
      notify(error, {
        type: "error",
      });
    }
  };

  return (
    <Edit>
      <AdminCampaignForm
        onSubmit={onSubmit}
        onRecordReady={(record) => {
          setRecord(record);
        }}
      />
    </Edit>
  );
};
