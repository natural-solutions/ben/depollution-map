import { useApi } from "@/utils/useApi";
import { Create, SaveButton, SimpleForm, Toolbar } from "react-admin";
import { useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { AdminUserFields } from "./AdminUserFields";
import { UserFormData } from "@/graphql/queries";

export const AdminUserCreate = () => {
  const { api, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (formData: UserFormData) => {
    try {
      await api.post("/users", {
        ...pick(formData, [
          "username",
          "first_name",
          "last_name",
          "email",
          "role_name",
          "password",
        ]),
        campaign_users: formData.campaign_users?.map((campaign_user) =>
          pick(campaign_user, ["campaign_id", "role"])
        ),
      } as UserFormData);
      navigate("/user_view");
    } catch (error) {
      notifyApiError(error as any);
    }
  };

  return (
    <Create>
      <SimpleForm
        onSubmit={onSubmit as any}
        toolbar={
          <Toolbar>
            <SaveButton alwaysEnable />
          </Toolbar>
        }
      >
        <AdminUserFields isNew={true} />
      </SimpleForm>
    </Create>
  );
};
