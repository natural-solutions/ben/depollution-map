import { useApi } from "@/utils/useApi";
import { Edit, SaveButton, SimpleForm, Toolbar } from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { AdminUserFields } from "./AdminUserFields";
import { UserFormData } from "@/graphql/queries";

export const AdminUserEdit = () => {
  const { id } = useParams();
  const { api, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (formData: UserFormData) => {
    if (!id) {
      return;
    }
    try {
      await api.patch(`/users/${id}`, {
        ...pick(formData, [
          "first_name",
          "last_name",
          "email",
          "role_name",
          "password",
        ]),
        campaign_users: formData.campaign_users?.map((campaign_user) =>
          pick(campaign_user, ["campaign_id", "role"])
        ),
      } as UserFormData);
      navigate("/user_view");
    } catch (error) {
      notifyApiError(error as any);
    }
  };

  return (
    <Edit>
      <SimpleForm
        onSubmit={onSubmit as any}
        toolbar={
          <Toolbar>
            <SaveButton alwaysEnable />
          </Toolbar>
        }
      >
        <AdminUserFields />
      </SimpleForm>
    </Edit>
  );
};
