import React, { FC, useState } from "react";
import {
  Button,
  Confirm,
  useListContext,
  useRefresh,
  useTranslate,
  useUnselectAll,
} from "react-admin";
import { Delete } from "@mui/icons-material";
import { useApi } from "@/utils/useApi";

type UserDeleteBtnProps = {};

export const UserDeleteBtn: FC<UserDeleteBtnProps> = (props) => {
  const { selectedIds } = useListContext();
  const translate = useTranslate();
  const refresh = useRefresh();
  const unselectAll = useUnselectAll("user_view");
  const { api } = useApi();

  const [isDeleteConfirmOpen, setIsDeleteConfirmOpen] = useState(false);

  const handleDelete = async () => {
    setIsDeleteConfirmOpen(false);
    await api.delete("/users", {
      data: {
        ids: selectedIds,
      },
    });
    unselectAll();
    refresh();
  };

  return (
    <>
      <Button
        label="Supprimer"
        color="error"
        startIcon={<Delete />}
        onClick={() => setIsDeleteConfirmOpen(true)}
        disabled={selectedIds.length < 1}
      />
      <Confirm
        title={translate("ra.message.bulk_delete_title", {
          name:
            selectedIds.length < 2
              ? `${selectedIds.length} utilisateur`
              : "utilisateurs",
          smart_count: selectedIds.length,
        })}
        content={translate("ra.message.bulk_delete_content", {
          smart_count: selectedIds.length,
        })}
        isOpen={isDeleteConfirmOpen}
        onConfirm={handleDelete}
        onClose={() => setIsDeleteConfirmOpen(false)}
      />
    </>
  );
};
