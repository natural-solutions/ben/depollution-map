import { HasRole } from "@/components/HasRole";
import { useSessionContext } from "@/utils/useSessionContext";
import { USER_ROLE } from "@/utils/user";
import React, { FC } from "react";
import {
  CreateButton,
  Datagrid,
  EditButton,
  ExportButton,
  FilterButton,
  List,
  SelectColumnsButton,
  TextField,
  TextInput,
  TopToolbar,
} from "react-admin";
import { UserDeleteBtn } from "./UserDeleteBtn";

const filters = [<TextInput key="a" label="Nom" source="fullname" />];

type AdminUserListProps = {};

export const AdminUserList: FC<AdminUserListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  const AdminUserListActions = () => (
    <TopToolbar>
      <SelectColumnsButton />
      {isAuthorized && <CreateButton />}
      <FilterButton filters={filters} />
      <ExportButton />
    </TopToolbar>
  );
  return (
    <>
      <List filters={filters} actions={<AdminUserListActions />}>
        <Datagrid
          isRowSelectable={() => isAuthorized || false}
          bulkActionButtons={
            <>
              <UserDeleteBtn />
            </>
          }
        >
          <TextField source="id" />
          <TextField source="fullname" label="Nom" />
          <TextField source="email" />
          <HasRole role={USER_ROLE.ADMIN}>
            <EditButton />
          </HasRole>
        </Datagrid>
      </List>
    </>
  );
};
