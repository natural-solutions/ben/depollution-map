import {
  ArrayInput,
  PasswordInput,
  RadioButtonGroupInput,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  required,
  useGetList,
} from "react-admin";
import { Campaign } from "@/graphql/types";
import { USER_ROLE_CHOICES } from "@/utils/user";
import { FC } from "react";
import { Typography } from "@mui/material";

type AdminUserFieldsProps = {
  isNew?: boolean;
};

export const AdminUserFields: FC<AdminUserFieldsProps> = ({ isNew }) => {
  const { data: campaigns, isLoading: isLoadingCampaigns } =
    useGetList<Campaign>("campaign", {
      pagination: {
        page: 1,
        perPage: 1000000,
      },
      sort: {
        field: "label",
        order: "ASC",
      },
    });

  return (
    <div style={{ maxWidth: "400px" }}>
      <TextInput
        fullWidth
        label="Username"
        source="username"
        disabled={!isNew}
      />
      <TextInput
        fullWidth
        source="first_name"
        label="Prénom"
        validate={required()}
      />
      <TextInput
        fullWidth
        source="last_name"
        label="Nom"
        validate={required()}
      />
      <TextInput fullWidth source="email" label="Email" />
      <Typography variant="h6">
        {isNew ? "Créer un mot de passe" : "Modifier le mot de passe"}
      </Typography>
      <PasswordInput
        fullWidth
        source="password"
        label="Mot de passe"
        validate={isNew ? required() : undefined}
      />
      <RadioButtonGroupInput
        label="Admin ?"
        source="role_name"
        choices={[
          { id: "admin", name: "Oui" },
          { id: "editor", name: "Non" },
        ]}
        defaultValue="editor"
      />
      <ArrayInput source="campaign_users" label="Entités Wings">
        <SimpleFormIterator inline disableClear disableReordering>
          <SelectInput
            source="campaign_id"
            label="Sélectionner une entité wings"
            choices={campaigns}
            optionValue="id"
            optionText="label"
            validate={required()}
            isLoading={isLoadingCampaigns}
          />
          <SelectInput
            source="role"
            label="Rôle"
            choices={USER_ROLE_CHOICES}
            validate={required()}
          />
        </SimpleFormIterator>
      </ArrayInput>
    </div>
  );
};
