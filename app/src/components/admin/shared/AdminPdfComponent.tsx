import { Delete } from "@mui/icons-material";
import { Box, IconButton, Stack, Tooltip, Typography } from "@mui/material";
import { useState } from "react";
import { FileField, FileInput } from "react-admin";

interface AdminPdfComponentProps {
  formContext: any;
  record: any;
}

export const AdminPdfComponent = (props: AdminPdfComponentProps) => {
  const { formContext, record } = props;

  const [isFileDelete, setIsFileDelete] = useState(false);

  return (
    <Stack direction="column" sx={{ width: "100%" }}>
      {record?.filename && !isFileDelete ? (
        <>
          <Box sx={{ width: 400 }}>
            <Typography>{record.filename}</Typography>
            <Tooltip title="Supprimer">
                <IconButton
                    aria-label="close"
                    onClick={() => {
                    formContext.setValue("attachments.title", "drop_file");
                    setIsFileDelete(true);
                    }}
                >
                    <Delete />
                </IconButton>
            </Tooltip>
          </Box>
        </>
      ) : (
        <></>
      )}
      <FileInput
        label={false}
        placeholder={
          record?.filename
            ? "Sélectionner un autre fichier"
            : "Sélectionner un fichier"
        }
        source="attachments"
        accept="application/pdf"
        sx={{
          width: "100%",
          height: "auto",
          "& .RaFileInput-dropZone": {
            backgroundColor: "#3EA0EA",
            color: "white",
            "&:hover": {
              backgroundColor: "#2C8DCD",
            },
          },
        }}
        multiple={false}
      >
        <FileField source="src" title="title" />
      </FileInput>
    </Stack>
  );
};
