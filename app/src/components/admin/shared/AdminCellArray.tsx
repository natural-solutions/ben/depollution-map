import { get } from "lodash";
import React, { FC } from "react";
import { WithListContext } from "react-admin";

type AdminCellArrayProps = {
  labelPath: string;
};

export const AdminCellArray: FC<AdminCellArrayProps> = (props) => {
  return (
    <WithListContext
      render={(params) => {
        const nbMore = params.total - params.data.length;
        const perPage = 3;
        return (
          <span
            style={{ cursor: "pointer" }}
            onClick={() => {
              params.setPerPage(params.perPage != perPage ? perPage : 999999);
            }}
          >
            {params.data.map((item) => get(item, props.labelPath)).join(", ")}
            {nbMore > 0 && ` +${nbMore}`}
          </span>
        );
      }}
    />
  );
};
