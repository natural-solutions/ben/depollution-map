import * as React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { useInput, FieldTitle } from "ra-core";
import {
  InputHelperText,
  ResettableTextField,
  TextInput,
  TextInputProps,
  sanitizeInputRestProps,
} from "react-admin";

export const AppDecimalInput = (props: TextInputProps) => {
  const {
    className,
    defaultValue,
    label,
    format,
    helperText,
    onBlur,
    onChange,
    parse,
    resource,
    source,
    validate,
    ...rest
  } = props;
  const {
    field,
    fieldState: { error, invalid, isTouched },
    formState: { isSubmitted },
    id,
    isRequired,
  } = useInput({
    defaultValue,
    format,
    parse,
    resource,
    source,
    type: "text",
    validate,
    onBlur,
    onChange,
    ...rest,
  });

  const renderHelperText =
    helperText !== false || ((isTouched || isSubmitted) && invalid);

  return (
    <ResettableTextField
      id={id}
      {...field}
      inputProps={{
        inputMode: "decimal",
      }}
      onChange={(e) => {
        const value = e.target.value;
        field.onChange(
          typeof value !== "string" || !value.includes(",")
            ? value
            : value.replace(",", ".")
        );
      }}
      className={clsx("ra-input", `ra-input-${source}`, className)}
      label={
        label !== "" && label !== false ? (
          <FieldTitle
            label={label}
            source={source}
            resource={resource}
            isRequired={isRequired}
          />
        ) : null
      }
      error={(isTouched || isSubmitted) && invalid}
      helperText={
        renderHelperText ? (
          <InputHelperText
            touched={isTouched || isSubmitted}
            error={error?.message}
            helperText={helperText}
          />
        ) : null
      }
      {...sanitizeInputRestProps(rest)}
    />
  );
};

TextInput.propTypes = {
  className: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.element,
  ]),
  resource: PropTypes.string,
  source: PropTypes.string,
};
