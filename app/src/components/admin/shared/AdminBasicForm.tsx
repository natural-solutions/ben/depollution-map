import {
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
  useRecordContext,
} from "react-admin";
import { useFormContext } from "react-hook-form";
import { Partner } from "@/graphql/types";
import { FC, useEffect, useState } from "react";
import { ReactAdminImageInputReturn } from "@/utils/media";
import { AdminPhotosComponent } from "./AdminPhotosComponent";

export type AdminBasicFormData = Partner & {
  selectLogo?: ReactAdminImageInputReturn;
};

type AdminBasicFormProps = {
  onSubmit: (data: AdminBasicFormData) => void;
  onRecordReady?: (record: Partner) => void;
  resourceName: "partners" | "poi_types";
};

const FormComponent: FC<AdminBasicFormProps> = (props) => {
  const { resourceName } = props;
  const formContext = useFormContext();
  const [isRecordReady, setIsRecordReady] = useState(false);
  const record = useRecordContext<Partner>();

  useEffect(() => {
    if (record && !isRecordReady) {
      setIsRecordReady(true);
      props.onRecordReady?.(record);
    }
  }, [record, isRecordReady]);

  return (
    <>
      <TextInput
        label="Id"
        source="id"
        validate={required()}
        parse={(value) => value.trim()}
      />
      <TextInput source="label" validate={required()} label="Nom" />
      <AdminPhotosComponent
        formContext={formContext}
        record={record}
        resourceName={resourceName}
      />
    </>
  );
};

export const AdminBasicForm: FC<AdminBasicFormProps> = (props) => (
  <SimpleForm
    sx={{
      maxWidth: "600px",
    }}
    onSubmit={props.onSubmit as any}
    toolbar={
      <Toolbar>
        <SaveButton alwaysEnable />
      </Toolbar>
    }
  >
    <FormComponent {...props} />
  </SimpleForm>
);
