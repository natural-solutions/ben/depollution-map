import { IMAGOR_PRESET_NAME, getTransformationURL } from "@/utils/media";
import { Delete } from "@mui/icons-material";
import { Avatar, Box, IconButton, Stack, Typography } from "@mui/material";
import { useState } from "react";
import { ImageField, ImageInput } from "react-admin";

interface AdminPhotosComponentProps {
  formContext: any;
  record: any;
  resourceName: "cleanups" | "campaigns" | "partners" | "poi_types";
}

export const AdminPhotosComponent = (props: AdminPhotosComponentProps) => {
  const { formContext, record, resourceName } = props;

  const [isAvatarDelete, setIsAvatarDelete] = useState(false);

  return (
    <Stack direction="row" sx={{ width: "100%" }}>
      {record?.logo ? (
        <>
          {isAvatarDelete ? (
            <></>
          ) : (
            <>
              <Box sx={{ width: 200 }}>
                <Avatar
                  sx={{ width: "100%", height: "auto" }}
                  variant="square"
                  src={getTransformationURL(
                    resourceName,
                    record.id,
                    record.logo,
                    IMAGOR_PRESET_NAME["200x200"]
                  )}
                />

                <IconButton
                  aria-label="close"
                  onClick={() => {
                    formContext.setValue("selectLogo.title", "drop_logo");
                    setIsAvatarDelete(true);
                  }}
                >
                  <Delete />
                </IconButton>
              </Box>
              <Box sx={{ flex: 1, pl: 2 }}>
                <Typography>{record.logo}</Typography>
              </Box>
            </>
          )}
        </>
      ) : (
        <></>
      )}
      <ImageInput
        label={false}
        placeholder={
          record?.logo
            ? "Sélectionner un autre fichier"
            : "Sélectionner un fichier"
        }
        source="selectLogo"
        sx={{
          width: "100%",
          height: "auto",
          "& .RaFileInput-dropZone": {
            backgroundColor: "#3EA0EA",
            color: "white",
            "&:hover": {
              backgroundColor: "#2C8DCD",
            },
          },
        }}
        multiple={false}
      >
        <ImageField source="src" sx={{ width: "100%", height: "auto" }} />
      </ImageInput>
    </Stack>
  );
};
