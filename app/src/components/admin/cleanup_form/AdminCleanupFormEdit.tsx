import { CleanupFormPatch } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { EditBase, useRefresh } from "react-admin";
import { useParams } from "react-router-dom";
import {
  AdminCleanupFormForm,
  AdminCleanupFormFormApi,
  AdminCleanupFormFormData,
} from "./AdminCleanupFormForm";
import { useState } from "react";
import { getCleanupFormPatch } from "@/utils/forms";
import { useRouter } from "next/navigation";
import { useNavigate } from "react-router-dom";
import { AdminCleanupFormPopup } from "./AdminCleanupFormSuccessPopup";
import { useRAContext } from "@/components/r_admin/RAindex";

export type SuccessMessage = "saved" | "tocheck" | "published" | "unpublished";

export const AdminCleanupFormEdit = () => {
  const router = useRouter();
  const navigate = useNavigate();
  const raContext = useRAContext();
  const refresh = useRefresh();

  const [formApi, setFormApi] = useState<AdminCleanupFormFormApi>();
  const { api, notifyApiError } = useApi();
  const { id } = useParams();
  const [successMessage, setSuccessMessage] = useState<string>();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const onSubmit = async (
    formData: AdminCleanupFormFormData,
    successMessage?: SuccessMessage,
    gotoStep?: number
  ) => {
    if (isSubmitting) return;
    setIsSubmitting(true);
    const patch = getCleanupFormPatch(formData);
    // TODO
    // if (!sessionCtx.getCanEditCleanupForm(formData)) {
    //   return;
    // }
    try {
      const { data } = await api.patch(`/cleanup_forms/${id}`, {
        ...patch,
        //cleanup_form_rubbishes_delete_ids,
      } as CleanupFormPatch);
      //formApi?.refresh(data.data.update_cleanup_form_by_pk);
      const successMsgs = {
        saved: "Caractérisation enregistrée avec succès",
        tocheck: "Caractérisation soumise au chargé de projet avec succès",
        published: "Caractérisation validée avec succès",
        unpublished: "Caractérisation dépubliée",
      };
      if (successMessage) {
        setSuccessMessage(successMsgs[successMessage]);
      } else {
        // Necessary to inject created cleanup_form_rubbishes (and co.) ids in form
        refresh();
      }
      if (gotoStep) {
        navigate(`/cleanup_form/${id}?step=${gotoStep}`, {
          replace: true,
        });
      }
    } catch (error) {
      notifyApiError(error as any);
    }
    setIsSubmitting(false);
  };

  const onClose = () => {
    setSuccessMessage("");
    const formData = formApi?.getFormContext().getValues();
    if (raContext.name === "forms") {
      router.push(`/cleanups/${formData?.cleanup_id || ""}`);
    } else {
      navigate("/cleanup_form");
    }
  };

  return (
    <>
      <AdminCleanupFormPopup
        open={Boolean(successMessage)}
        onClose={onClose}
        msg={successMessage}
      />
      <EditBase>
        <AdminCleanupFormForm
          onSubmit={onSubmit}
          onReady={setFormApi}
          disabled={isSubmitting}
        />
      </EditBase>
    </>
  );
};
