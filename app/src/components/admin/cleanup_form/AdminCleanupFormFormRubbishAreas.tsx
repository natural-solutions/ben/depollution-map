import { Cleanup_Form, Cleanup_Form_Rubbish_Detail } from "@/graphql/types";
import { isPositiveInteger } from "@/utils/forms";
import { Box, Stack, Typography } from "@mui/material";
import React, { FC } from "react";
import {
  ArrayInput,
  FormDataConsumer,
  SimpleFormIterator,
  TextInput,
  minValue,
  number,
  required,
  useRecordContext,
} from "react-admin";
import { AppDecimalInput } from "../shared/AppDecimalInput";

type AdminCleanupFormFormRubbishAreasProps = {
  getSource: (source: string) => string;
  fielset: "qty" | "wt_vol";
};

export const AdminCleanupFormFormRubbishAreas: FC<
  AdminCleanupFormFormRubbishAreasProps
> = (props) => {
  const { getSource, fielset } = props;
  const record = useRecordContext<Cleanup_Form>();
  const readOnly = record?.status === "published" ? true : false;

  return (
    <Box>
      <ArrayInput
        source={getSource("areas")}
        label=""
        sx={{ pl: "0px !important" }}
        helperText={false}
      >
        <SimpleFormIterator
          fullWidth
          sx={{
            ".RaSimpleFormIterator-line": {
              py: 1,
            },
          }}
          disableReordering
          disableClear
          disableAdd
          disableRemove
        >
          <FormDataConsumer<Partial<Cleanup_Form_Rubbish_Detail>>>
            {({ scopedFormData, getSource }) => {
              return (
                scopedFormData && (
                  <Stack
                    direction="row"
                    spacing={{
                      xs: 1,
                      sm: 2,
                    }}
                    sx={{ width: "100%" }}
                  >
                    <Box>
                      <Typography
                        sx={{
                          mt: 3,
                          width: {
                            xs: fielset == "wt_vol" ? "100px" : "140px",
                            sm: "140px",
                          },
                        }}
                      >
                        {scopedFormData.cleanup_area?.label}
                      </Typography>
                    </Box>
                    {fielset == "qty" && (
                      <Box>
                        <TextInput
                          label="Nombre"
                          source={getSource("quantity")}
                          sx={{ maxWidth: "120px" }}
                          validate={[required(), isPositiveInteger]}
                          inputProps={{ inputMode: "numeric", readonly: readOnly ? "" : null }}
                        />
                      </Box>
                    )}
                    {fielset == "wt_vol" && (
                      <>
                        <Box>
                          <AppDecimalInput
                            label="Poids"
                            source={getSource("weight")}
                            validate={[required(), number(), minValue(0)]}
                            inputProps={{ readonly: readOnly ? "" : null }}
                          />
                        </Box>
                        <Box>
                          <AppDecimalInput
                            label="Volume"
                            source={getSource("volume")}
                            validate={[required(), number(), minValue(0)]}
                            inputProps={{ readonly: readOnly ? "" : null }}
                          />
                        </Box>
                      </>
                    )}
                  </Stack>
                )
              );
            }}
          </FormDataConsumer>
        </SimpleFormIterator>
      </ArrayInput>
    </Box>
  );
};
