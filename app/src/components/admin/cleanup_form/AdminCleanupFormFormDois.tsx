import { Cleanup_Form, Cleanup_Form_Poi, Rubbish } from "@/graphql/types";
import { Add } from "@mui/icons-material";
import { Button, Grid, Typography } from "@mui/material";
import React, { FC } from "react";
import {
  ArrayInput,
  FormDataConsumer,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  required,
  useGetList,
  useRecordContext,
} from "react-admin";

type AdminCleanupFormFormDoisProps = {};

export const AdminCleanupFormFormDois: FC<AdminCleanupFormFormDoisProps> = (
  props
) => {
  const record = useRecordContext<Cleanup_Form>();
  const readOnly = record?.status === "published" ? true : false;

  const { data: rubbishes, isLoading: isLoadingRubbishes } =
    useGetList<Rubbish>("rubbish", {
      pagination: {
        page: 1,
        perPage: 1000000,
      },
      sort: {
        field: "label",
        order: "ASC",
      },
      filter: {
        is_support_doi: true,
      },
    });

  return (
    <>
      <Typography variant="h4" color="primary">
        Dates remarquables
      </Typography>
      <Typography sx={{ mb: 2 }}>
        Si un déchet avec une date remarquable à été repéré, le notifier.
      </Typography>
      <ArrayInput source="cleanup_form_dois" label=" ">
        <SimpleFormIterator
          inline
          fullWidth
          disableClear
          disableReordering
          disableAdd={readOnly}
          disableRemove={readOnly}
          addButton={
            <Button variant="contained" size="small" startIcon={<Add />}>
              Ajouter une date
            </Button>
          }
        >
          <FormDataConsumer<Cleanup_Form_Poi>>
            {({ getSource, scopedFormData }) => (
              <Grid container spacing={1}>
                <Grid item xs={6}>
                  <SelectInput
                    fullWidth
                    source={getSource("rubbish_id")}
                    label="Type de déchet"
                    choices={rubbishes}
                    optionValue="id"
                    optionText="label"
                    validate={required()}
                    isLoading={isLoadingRubbishes}
                    inputProps={{
                      inputMode: "numeric",
                      readonly: readOnly ? "" : null,
                    }}
                    sx={{ pointerEvents: readOnly ? "none" : "auto" }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextInput
                    fullWidth
                    source={getSource("expiration")}
                    label="Date"
                    type="date"
                    validate={[required()]}
                    inputProps={{ readonly: readOnly ? "" : null }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
              </Grid>
            )}
          </FormDataConsumer>
        </SimpleFormIterator>
      </ArrayInput>
    </>
  );
};
