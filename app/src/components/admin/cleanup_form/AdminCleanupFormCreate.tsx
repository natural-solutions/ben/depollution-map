import { useApi } from "@/utils/useApi";
import { Create, useNotify } from "react-admin";
import { pick } from "lodash";
import {
  AdminCleanupFormForm,
  AdminCleanupFormFormData,
} from "./AdminCleanupFormForm";
import { formatRubbishes, getFilteredFormRubbishes } from "@/utils/forms";
import { useNavigate } from "react-router-dom";

export const AdminCleanupFormCreate = () => {
  const { api, notifyApiError } = useApi();
  const notify = useNotify();
  const navigate = useNavigate();

  const onSubmit = async (formData: AdminCleanupFormFormData) => {
    const cleanup_form_rubbishes: any[] = [];
    const cleanup_form_rubbish_details: any[] = [];
    formatRubbishes(
      formData.qtyCategories,
      cleanup_form_rubbishes,
      ["quantity"],
      cleanup_form_rubbish_details
    );
    formatRubbishes(
      formData.wt_volCategories,
      cleanup_form_rubbishes,
      ["weight", "volume"],
      cleanup_form_rubbish_details
    );

    try {
      const { data } = await api.post("/cleanup_forms", {
        ...pick(formData, [
          "performed_at",
          "volunteers_number",
          "external_volunteers_number",
          "cleanup_duration",
          "rubbish_type_picked_id",
          "comment",
          "cleanup_id",
          "wind_force",
          "rain_force",
        ]),
        cleanup_form_rubbishes: getFilteredFormRubbishes(
          cleanup_form_rubbishes
        ).map((detail) => {
          return pick(detail, ["rubbish_id", "quantity", "weight", "volume"]);
        }),
        cleanup_form_rubbish_details: getFilteredFormRubbishes(
          cleanup_form_rubbish_details
        ).map((detail) => {
          return pick(detail, [
            "rubbish_id",
            "quantity",
            "weight",
            "volume",
            "cleanup_area_id",
            "brand_id",
          ]);
        }),
        cleanup_form_pois: formData.cleanup_form_pois?.map((cleanup_form_poi) =>
          pick(cleanup_form_poi, ["poi_type_id", "cleanup_area_id", "nb"])
        ),
        cleanup_form_dois: formData.cleanup_form_dois?.map((cleanup_form_doi) =>
          pick(cleanup_form_doi, ["rubbish_id", "expiration"])
        ),
      });
      notify("Données sauvegardées", {
        type: "success",
        anchorOrigin: { vertical: "top", horizontal: "center" },
      });

      navigate(`/cleanup_form/${data.id}?step=2`, {
        replace: true,
      });
    } catch (error) {
      notifyApiError(error as any);
    }
  };

  return (
    <>
      <Create>
        <AdminCleanupFormForm isNew={true} onSubmit={onSubmit} />
      </Create>
    </>
  );
};
