import { Close } from "@mui/icons-material";
import { Button, Dialog, DialogActions, DialogTitle } from "@mui/material";

interface AdminCleanupFormPopupProps {
  open: boolean;
  onClose: Function;
  msg?: string;
}

export function AdminCleanupFormPopup(props: AdminCleanupFormPopupProps) {
  const { open, onClose, msg } = props;

  return (
    <Dialog open={open}>
      <DialogTitle sx={{ mb: 3, mt: 1, textAlign: "center" }}>
        {msg}
      </DialogTitle>
      <DialogActions>
        <Button
          endIcon={<Close />}
          sx={{
            mb: 5,
            position: "absolute",
          }}
          onClick={() => onClose()}
        >
          Fermer
        </Button>
      </DialogActions>
    </Dialog>
  );
}
