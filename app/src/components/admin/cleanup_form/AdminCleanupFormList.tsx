import { CleanupFormCharacterizationStatus } from "@/components/cleanup/CleanupCharacterizationStatus";
import { Cleanup_Form } from "@/graphql/types";
import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  CreateButton,
  DatagridConfigurable,
  DateField,
  DateInput,
  EditButton,
  FilterButton,
  FunctionField,
  List,
  SelectColumnsButton,
  SelectInput,
  TextField,
  TextInput,
  TopToolbar,
  useRecordContext,
} from "react-admin";

const windForces = [
  { id: "none", name: "Je ne sais pas" },
  { id: "low", name: "Nul / très faible (< 11 km/h)" },
  { id: "middle", name: "Modéré (12 - 30 km/h)" },
  { id: "strong", name: "Fort (> 30 km/h)" },
];

const rainForces = [
  { id: "not_known", name: "Je ne sais pas" },
  { id: "none", name: "Pas de pluie" },
  { id: "low", name: "Faible et continue (1 - 3 mm/h)" },
  { id: "middle", name: "Modérée (3 - 7 mm/h)" },
  { id: "strong", name: "Forte (> 8 mm/h)" },
];

const filters = [
  <TextInput key="e" label="Ramassage" source="cleanup#label@_ilike" />,
  <SelectInput
    key="c"
    label="Force du vent"
    source="wind_force"
    choices={windForces}
  />,
  <SelectInput
    key="f"
    label="Force de la pluie"
    source="rain_force"
    choices={rainForces}
  />,
  <DateInput key="g" label="Date de réalisation =" source="performed_at" />,
  <DateInput
    key="h"
    label="Date de réalisation >="
    source="performed_at@_gte"
  />,
  <DateInput
    key="d"
    label="Date de réalisation <"
    source="performed_at@_lte"
  />,
];

type AdminCleanupFormListProps = {};

export const AdminCleanupFormList: FC<AdminCleanupFormListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();
  const recordContext = useRecordContext<Cleanup_Form>();

  const getWindForceValue = (id: typeof recordContext.rain_force) => {
    const value = windForces.find((c) => c.id === id);
    return value ? value.name : id;
  };

  const getRainForceValue = (id: typeof recordContext.rain_force) => {
    const value = rainForces.find((c) => c.id === id);
    return value ? value.name : id;
  };

  const AdminCleanupFormListActions = () => (
    <TopToolbar>
      <SelectColumnsButton />
      {isAuthorized && <CreateButton />}
      <FilterButton filters={filters} />
    </TopToolbar>
  );

  return (
    <List filters={filters} actions={<AdminCleanupFormListActions />}>
      <DatagridConfigurable isRowSelectable={() => isAuthorized}>
        <TextField source="id" />
        <TextField source="cleanup.label" label="Ramassage" />
        <FunctionField
          label="Force du vent"
          render={(record: Cleanup_Form) =>
            getWindForceValue(record.wind_force)
          }
        />
        <FunctionField
          label="Force de la pluie"
          render={(record: Cleanup_Form) =>
            getRainForceValue(record.rain_force)
          }
        />
        <FunctionField
          label="Statut"
          render={(cleanupForm: Cleanup_Form) => {
            return (
              <span>
                <CleanupFormCharacterizationStatus cleanupForm={cleanupForm} />
              </span>
            );
          }}
        />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifé le" />
        <FunctionField
          render={(record: Cleanup_Form) => (
            sessionCtx.getCanEditCleanupForm(record) && <EditButton />
          )}
        />
      </DatagridConfigurable>
    </List>
  );
};

export { windForces, rainForces };
