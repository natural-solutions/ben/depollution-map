import { Cleanup_Form, Cleanup_Form_Rubbish_Detail } from "@/graphql/types";
import { isPositiveInteger } from "@/utils/forms";
import { Box, Button, Stack } from "@mui/material";
import React, { FC } from "react";
import {
  ArrayInput,
  FormDataConsumer,
  ReferenceInput,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  required,
  useRecordContext,
} from "react-admin";

type AdminCleanupFormFormRubbishBrandsProps = {
  getSource: (source: string) => string;
};

export const AdminCleanupFormFormRubbishBrands: FC<
  AdminCleanupFormFormRubbishBrandsProps
> = (props) => {
  const { getSource } = props;
  const record = useRecordContext<Cleanup_Form>();
  const readOnly = record?.status === "published" ? true : false;

  return (
    <ArrayInput
      source={getSource("brands")}
      label=""
      sx={{ pl: "0 !important", mb: 4 }}
      helperText={false}
    >
      <SimpleFormIterator
        className="charac-brands"
        fullWidth
        sx={{
          ".RaSimpleFormIterator-line": {
            py: 1,
          },
        }}
        disableReordering
        disableClear
        disableAdd={readOnly}
        disableRemove={readOnly}
        addButton={
          <Button variant="contained" size="small">
            Ajouter une marque
          </Button>
        }
      >
        <FormDataConsumer<Partial<Cleanup_Form_Rubbish_Detail>>>
          {({ scopedFormData, getSource }) => {
            return (
              scopedFormData && (
                <Stack direction="row" spacing={2}>
                  <Box sx={{ flex: 1 }}>
                    <ReferenceInput
                      reference="brand"
                      source={getSource("brand_id")}
                      perPage={1000000}
                      sort={{ field: "label", order: "ASC" }}
                    >
                      <SelectInput
                        label="Marque"
                        optionText="label"
                        optionValue="id"
                        fullWidth
                        validate={required()}
                        sx={{ pointerEvents: readOnly ? "none" : "auto" }}
                      />
                    </ReferenceInput>
                  </Box>
                  <Box>
                    <TextInput
                      label="Nombre"
                      source={getSource("quantity")}
                      sx={{ maxWidth: "120px" }}
                      validate={[required(), isPositiveInteger]}
                      inputProps={{
                        inputMode: "numeric",
                        readonly: readOnly ? "" : null,
                      }}
                    />
                  </Box>
                </Stack>
              )
            );
          }}
        </FormDataConsumer>
      </SimpleFormIterator>
    </ArrayInput>
  );
};
