import { Confirm, Form, useRecordContext } from "react-admin";
import { useFormContext } from "react-hook-form";
import { useSearchParams, useNavigate } from "react-router-dom";
import {
  Cleanup,
  Cleanup_Form,
  Cleanup_Form_Poi,
  Cleanup_Form_Rubbish,
  Cleanup_Form_Rubbish_Detail,
} from "@/graphql/types";
import { FC, useEffect, useState } from "react";
import { Button, Fab, Paper, Stack, Typography } from "@mui/material";
import isNil from "lodash/isNil";
import {
  AdminCleanupFormFormRubbishes,
  FormCategory,
} from "./AdminCleanupFormFormRubbishes";
import { useApi } from "@/utils/useApi";
import { GET_CLEANUP } from "@/graphql/queries";
import { FormsHeader } from "@/components/forms/FormsHeader";
import { AdminCleanupFormFormBase } from "./AdminCleanupFormFormBase";
import { useRAContext } from "@/components/r_admin/RAindex";
import { HEADER_HEIGHT_SX } from "@/utils/theme";
import {
  ArrowForwardRounded,
  Save,
  Send,
  VisibilityOff,
} from "@mui/icons-material";
import { useSessionContext } from "@/utils/useSessionContext";
import { CLEANUP_FORM_STATUS, getCleanupForm } from "@/utils/cleanups";
import { SuccessMessage } from "./AdminCleanupFormEdit";
import { usePathname, useRouter } from "next/navigation";
import { AdminCleanupFormFormPois } from "./AdminCleanupFormFormPois";
import { AdminCleanupFormFormDois } from "./AdminCleanupFormFormDois";
import { Step } from "../../../utils/forms";
import { useSession } from "next-auth/react";
import { ButtonLink } from "@/components/ButtonLink";

export type AdminCleanupFormFormData = Cleanup_Form & {
  qtyCategories: FormCategory[];
  wt_volCategories: FormCategory[];
  poiAreas: {
    [key: string]: Cleanup_Form_Poi[];
  };
};

export type AdminCleanupFormFormApi = {
  getFormContext: () => ReturnType<typeof useFormContext>;
  refresh: (data: Cleanup_Form) => void;
};

type AdminCleanupFormFormProps = {
  disabled?: boolean;
  isNew?: boolean;
  onSubmit: (
    data: AdminCleanupFormFormData,
    successMessage?: SuccessMessage,
    gotoStep?: number
  ) => void;
  onReady?: (api: AdminCleanupFormFormApi) => void;
};

type StepName = "info" | "qty" | "wt_vol" | "doi";

const STEPS: Step[] = [
  {
    name: "info",
    title: "Informations",
    isActivatable: true,
  },
  {
    name: "qty",
    title: "Comptage",
    isActivatable: false,
  },
  {
    name: "wt_vol",
    title: "Poids/Volumes",
    isActivatable: false,
  },
  {
    name: "doi",
    title: "Dates remarquables",
    isActivatable: true,
  },
] as const;

const FormComponent: FC<AdminCleanupFormFormProps> = (props) => {
  const sessionContext = useSessionContext();
  const [searchParams] = useSearchParams();
  const pathname = usePathname();
  const session = useSession();
  const navigate = useNavigate();
  const router = useRouter();
  const record = useRecordContext<Cleanup_Form>();
  const formContext = useFormContext();
  const raContext = useRAContext();
  const { postGraphql } = useApi();

  const [isRecordReady, setIsRecordReady] = useState(false);
  const [forbiddenMsg, setForbiddenMsg] = useState<"forbidden" | "exists">();
  const [steps, setSteps] = useState([...STEPS]);

  const readOnly = record?.status === "published" ? true : false;
  const cleanupId = record?.cleanup_id || searchParams.get("cleanup_id");
  const activeStep = Math.max(0, Number(searchParams.get("step") || 1)) - 1;

  const canSubmit = sessionContext.getCanSubmitCleanupForm(record);
  const canPublish = sessionContext.getCanPublishCleanupForm(record);

  const [isConfirmDialogOpen, setIsConfirmDialogOpen] =
    useState<boolean>(false);

  useEffect(() => {
    (async () => {
      if (props.isNew) {
        if (cleanupId) {
          await initDefaultData(cleanupId);
        }
        setIsRecordReady(true);
      }
      props.onReady?.({
        getFormContext: () => {
          return formContext;
        },
        refresh: (data) => {
          computeForm(data);
        },
      });
    })();
  }, []);

  const getStep = (name: StepName) => {
    return steps.find((step) => step.name == name) as Step;
  };

  const computeForm = (data: Cleanup_Form) => {
    const qtyCategories = getFormCategories(data, "has_qty");
    const wt_volCategories = getFormCategories(data, "has_wt_vol");

    const qtyStep = getStep("qty");
    const wt_volStep = getStep("wt_vol");
    qtyStep.isActivatable = Boolean(qtyCategories.length);
    wt_volStep.isActivatable = Boolean(wt_volCategories.length);
    setSteps([...steps]);

    const poiAreas = getPoiAreas(data);

    // without setTimeout the formArray doesn't update, maybe because of the "set state" done before
    setTimeout(() => {
      formContext.setValue("qtyCategories", qtyCategories);
      formContext.setValue("wt_volCategories", wt_volCategories);
      formContext.setValue("poiAreas", poiAreas);
    });
  };

  useEffect(() => {
    if (!props.isNew && record && record?.cleanup_form_rubbishes) {
      if (session.status !== "authenticated") {
        setForbiddenMsg("forbidden");
        return;
      }

      if (readOnly) {
        computeForm(record);
      } else if (sessionContext.getCanEditCleanupForm(record)) {
        computeForm(record);
      } else {
        setForbiddenMsg("forbidden");
      }

      setIsRecordReady(true);
    }
  }, [record, readOnly, session.status]);

  const submit = (successMessage: SuccessMessage) => {
    if (readOnly && !canPublish) {
      return;
    }
    formContext.handleSubmit((data) => {
      props.onSubmit(data as AdminCleanupFormFormData, successMessage);
    })();
  };

  const changeStep = () => {
    if (readOnly) {
      navigate(`?step=${activeStep + 2}`, {
        replace: true,
      });
    } else {
      formContext.handleSubmit((data) => {
        if (props.isNew) {
          return props.onSubmit(data as AdminCleanupFormFormData);
        }
        props.onSubmit(
          data as AdminCleanupFormFormData,
          undefined,
          activeStep + 2
        );
      })();
    }
  };

  const initDefaultData = async (cleanupId: string) => {
    const resp = await postGraphql<{
      data: {
        cleanup_by_pk: Cleanup;
      };
    }>({
      query: GET_CLEANUP,
      variables: {
        id: cleanupId,
      },
    });
    const initData: Partial<Cleanup_Form> = {
      cleanup: resp.data.data.cleanup_by_pk,
    };
    if (!sessionContext.getCanCreateCleanupForm(initData.cleanup as Cleanup)) {
      setForbiddenMsg("forbidden");
      return;
    }
    if (getCleanupForm(initData.cleanup as Cleanup)) {
      return setForbiddenMsg("exists");
    }
    const qtyCategories = getFormCategories(initData, "has_qty");
    const wt_volCategories = getFormCategories(initData, "has_wt_vol");
    const poiAreas = getPoiAreas(initData);
    formContext.setValue("cleanup_id", cleanupId);
    formContext.setValue("performed_at", initData.cleanup?.start_at);
    formContext.setValue("qtyCategories", qtyCategories);
    formContext.setValue("wt_volCategories", wt_volCategories);
    formContext.setValue("poiAreas", poiAreas);

    const qtyStep = getStep("qty");
    const wt_volStep = getStep("wt_vol");
    qtyStep.isActivatable = Boolean(qtyCategories.length);
    wt_volStep.isActivatable = Boolean(wt_volCategories.length);
    setSteps([...steps]);
  };

  const getPoiAreas = (data: Partial<Cleanup_Form>) => {
    const areas: any = {};
    data.cleanup?.cleanup_areas.forEach((cleanup_area) => {
      areas[cleanup_area.id] =
        data.cleanup_form_pois?.filter(
          ({ cleanup_area_id }) => cleanup_area_id == cleanup_area.id
        ) || [];
    });
    return areas;
  };

  const getFormCategories = (
    initData: Partial<Cleanup_Form>,
    field: "has_qty" | "has_wt_vol"
  ): FormCategory[] => {
    if (!initData?.cleanup?.cleanup_type?.cleanup_type_rubbishes?.length) {
      return [];
    }

    const fields = {
      has_qty: ["quantity"],
      has_wt_vol: ["weight", "volume"],
    }[field];
    const existingFormRubbishes = (
      initData.cleanup_form_rubbishes || []
    ).filter((formRubbish) => {
      let isSupported = fields.some(
        (field) => !isNil((formRubbish as any)[field])
      );
      if (!isSupported) {
        isSupported = Boolean(
          initData.cleanup?.cleanup_type?.cleanup_type_rubbishes?.some(
            (cleanup_type_rubbish) =>
              cleanup_type_rubbish.rubbish_id === formRubbish.rubbish_id &&
              cleanup_type_rubbish[field]
          )
        );
      }
      return isSupported && formRubbish.rubbish?.rubbish_category;
    });
    const mergedFormRubbishes: Cleanup_Form_Rubbish[] = [];
    const formCategories: FormCategory[] = [];
    initData.cleanup?.cleanup_type?.cleanup_type_rubbishes
      .filter(
        (rubbishCleanupType) =>
          rubbishCleanupType[field] &&
          rubbishCleanupType.rubbish.rubbish_category
      )
      .forEach((rubbishCleanupType) => {
        let formCategory = formCategories.find(
          (formCategory) =>
            formCategory.category.id ===
            rubbishCleanupType.rubbish.rubbish_category_id
        );
        if (!formCategory) {
          formCategory = {
            category: rubbishCleanupType.rubbish.rubbish_category!,
            rubbishes: [],
          };
          formCategories.push(formCategory);
        }

        const rubbish = rubbishCleanupType.rubbish;
        let formRubbish: Partial<Cleanup_Form_Rubbish> = {
          rubbish_id: rubbish.id,
          rubbish,
        };
        const existingFormRubbish = existingFormRubbishes.find(
          (existingFormRubbish) => existingFormRubbish.rubbish_id === rubbish.id
        );

        if (existingFormRubbish) {
          formRubbish = existingFormRubbish;
          mergedFormRubbishes.push(existingFormRubbish);
        }

        formCategory.rubbishes.push({
          formRubbish,
          brands: (initData.cleanup_form_rubbish_details || []).filter(
            (rubbishDetail) =>
              rubbishDetail.brand_id &&
              rubbishDetail.rubbish_id === formRubbish.rubbish_id
          ),
          areas: getRubbishAreas(initData, formRubbish),
        });
      });

    const notMergedFormRubbishes = existingFormRubbishes.filter(
      (existingFormRubbish) => {
        return !mergedFormRubbishes.some(
          (mergedFormRubbish) => mergedFormRubbish.id == existingFormRubbish.id
        );
      }
    );
    notMergedFormRubbishes
      .filter((formRubbish) => {
        let isSupported = fields.some(
          (field) => !isNil((formRubbish as any)[field])
        );
        if (!isSupported) {
          const cleanupLevelId = initData.cleanup?.cleanup_type_id;
          isSupported = Boolean(
            cleanupLevelId &&
              formRubbish.rubbish?.cleanup_type_rubbishes?.some(
                (rubbishCleanupTypes) =>
                  cleanupLevelId === rubbishCleanupTypes.cleanup_type_id &&
                  rubbishCleanupTypes[field]
              )
          );
        }
        return isSupported && formRubbish.rubbish?.rubbish_category;
      })
      .forEach((formRubbish) => {
        let formCategory = formCategories.find(
          (formCategory) =>
            formCategory.category.id ===
            formRubbish.rubbish?.rubbish_category_id
        );
        if (!formCategory) {
          formCategory = {
            category: formRubbish.rubbish?.rubbish_category!,
            rubbishes: [],
          };
          formCategories.push(formCategory);
        }
        formCategory.rubbishes.push({
          formRubbish,
          brands: (initData.cleanup_form_rubbish_details || []).filter(
            (rubbishDetail) =>
              rubbishDetail.brand_id &&
              rubbishDetail.rubbish_id === formRubbish.rubbish_id
          ),
          areas: getRubbishAreas(initData, formRubbish),
        } as any);
      });

    formCategories.sort((a, b) => {
      if (a.category.rank == b.category.rank) {
        if (a.category.label < b.category.label) {
          return -1;
        }
        if (a.category.label > b.category.label) {
          return 1;
        }
        return 0;
      }
      return (a.category.rank || 999999) - (b.category.rank || 999999);
    });

    return formCategories;
  };

  const getRubbishAreas = (
    initData: Partial<Cleanup_Form>,
    formRubbish: Partial<Cleanup_Form_Rubbish>
  ) => {
    const rubbishAreas: Partial<Cleanup_Form_Rubbish_Detail>[] = (
      initData.cleanup_form_rubbish_details || []
    ).filter(
      (rubbishDetail) =>
        rubbishDetail.cleanup_area_id &&
        rubbishDetail.rubbish_id === formRubbish.rubbish_id
    );

    initData.cleanup?.cleanup_areas
      ?.filter((cleanupArea) => cleanupArea.geom?.geometry?.type === "Polygon")
      .forEach((cleanupArea) => {
        const isExisting = rubbishAreas.some(
          (rubbishArea) => rubbishArea.cleanup_area_id == cleanupArea.id
        );
        if (!isExisting) {
          rubbishAreas.push({
            cleanup_area_id: cleanupArea.id,
            cleanup_area: cleanupArea,
            rubbish_id: formRubbish.rubbish_id,
          });
        }
      });

    return rubbishAreas;
  };

  const activatableSteps = steps.filter((def) => def.isActivatable);
  const mapSteps: {
    [key in StepName]?: Step;
  } = {};
  activatableSteps.forEach((step) => {
    mapSteps[step.name as StepName] = step;
  });

  const isStepActive = (name: StepName) => {
    const stepIndex = activatableSteps.findIndex((step) => step.name === name);

    return stepIndex < 0 || activeStep != stepIndex ? false : true;
  };

  if (!isRecordReady) {
    return <Typography>Loading...</Typography>;
  }

  if (forbiddenMsg) {
    return (
      <Stack sx={{ alignItems: "center" }}>
        <Paper
          sx={{
            m: 6,
            p: 3,
          }}
        >
          <Typography variant="subtitle1" component="strong">
            Impossible de poursuivre.
          </Typography>
          <Typography>
            {
              {
                forbidden: "Vous n'avez pas les autorisations requises.",
                exists:
                  "Une caractérisation est déjà enregistrée pour ce ramassage.",
              }[forbiddenMsg]
            }
          </Typography>
          <Stack
            sx={{
              alignItems: "center",
              mt: 3,
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
                if (raContext.name === "admin") {
                  navigate("/cleanup_form");
                } else {
                  router.push(`/cleanups/${cleanupId || ""}`);
                }
              }}
            >
              Annuler
            </Button>
          </Stack>
        </Paper>
      </Stack>
    );
  }

  return (
    <Stack
      sx={{
        paddingTop: {
          xs: raContext.hasNoLayout ? `${HEADER_HEIGHT_SX * 2}px` : 0,
          md: 0,
        },
        paddingBottom: {
          xs: `${HEADER_HEIGHT_SX}px`,
          md: "64px",
        },
        width: {
          xs: "100%",
          md: "auto",
        },
      }}
    >
      <FormsHeader
        backUrl={cleanupId ? `/cleanups/${cleanupId}` : undefined}
        title={
          readOnly
            ? "Voir une caractérisation"
            : props.isNew
            ? "Créer une caractérisation"
            : "Éditer une caractérisation"
        }
        steps={activatableSteps}
        activeStep={activeStep}
        onActiveStepChange={(index) => {
          if (readOnly) {
            navigate(`?step=${index + 1}`, {
              replace: true,
            });
          } else {
            if (props.disabled) return;
            formContext.handleSubmit((data) => {
              props.onSubmit(data as any, undefined, index + 1);
            })();
          }
        }}
      />
      <Stack
        sx={{
          alignItems: "center",
          pt: 5,
        }}
      >
        {isStepActive("info") && (
          <Stack
            sx={{
              /* display: getStepDisplay("info"), */
              width: "100%",
              maxWidth: "600px",
              mb: 3,
            }}
          >
            <Typography variant="h4" color="primary" sx={{ mb: 2 }}>
              {mapSteps.info?.title}
            </Typography>
            <AdminCleanupFormFormBase record={record} />
          </Stack>
        )}
        {isStepActive("qty") && (
          <Stack
            sx={{
              /* display: getStepDisplay("qty"), */
              width: "100%",
              maxWidth: "600px",
            }}
          >
            <Typography variant="h4" color="primary" sx={{ mb: 2 }}>
              {mapSteps.qty?.title}
            </Typography>
            <AdminCleanupFormFormRubbishes formCategoryFieldset="qty" />
            {record?.cleanup?.cleanup_type_id == "butts" && (
              <AdminCleanupFormFormPois cleanupId={cleanupId} />
            )}
          </Stack>
        )}
        {isStepActive("wt_vol") && (
          <Stack
            sx={{
              /* display: getStepDisplay("wt_vol"), */
              width: "100%",
              maxWidth: "600px",
            }}
          >
            <Typography variant="h4" color="primary" sx={{ mb: 2 }}>
              {mapSteps.wt_vol?.title}
            </Typography>
            <AdminCleanupFormFormRubbishes formCategoryFieldset="wt_vol" />
          </Stack>
        )}
        {isStepActive("doi") && (
          <Stack
            sx={{
              /* display: getStepDisplay("doi"), */
              width: "100%",
              maxWidth: "600px",
            }}
          >
            <AdminCleanupFormFormDois />
          </Stack>
        )}
      </Stack>
      <Stack
        sx={{
          position: "fixed",
          bottom: {
            xs: 10,
            md: 10,
          },
          right: {
            xs: 10,
            sm: "auto",
          },
          left: {
            sm: "50%",
          },
          transform: {
            sm: "translateX(-50%)",
          },
          zIndex: 2,
        }}
      >
        {!props.isNew && activeStep === activatableSteps.length - 1 ? (
          <Stack direction="row">
            {!readOnly && (
              <Button
                endIcon={<Save />}
                size="large"
                variant="contained"
                onClick={() => {
                  submit("saved");
                }}
                sx={{ mr: 2 }}
              >
                Enregistrer
              </Button>
            )}
            {canSubmit &&
              !canPublish &&
              !["tocheck", "published"].includes(record?.status as string) && (
                <>
                  <Button
                    onClick={() => setIsConfirmDialogOpen(true)}
                    endIcon={<Send />}
                    variant="contained"
                    color="success"
                  >
                    Soumettre
                  </Button>
                  <Confirm
                    isOpen={isConfirmDialogOpen}
                    title={`Soumettre la caractérisation ?`}
                    content="Êtes vous sûr de vouloir soumettre ce formulaire de carcatérisation ? Il restera modifiable jusqu'à sa validation par le chargé de projet."
                    onConfirm={() => {
                      formContext.setValue(
                        "status",
                        CLEANUP_FORM_STATUS.TOCHECK
                      );
                      submit("tocheck");
                    }}
                    onClose={() => setIsConfirmDialogOpen(false)}
                    confirm="Soumettre"
                  />
                </>
              )}
            {canPublish && record?.status !== "published" && (
              <>
                <Button
                  onClick={() => setIsConfirmDialogOpen(true)}
                  endIcon={<Send />}
                  variant="contained"
                  color="success"
                >
                  Valider
                </Button>
                <Confirm
                  isOpen={isConfirmDialogOpen}
                  title={`Valider la caractérisation ?`}
                  content="Êtes vous sûr de vouloir valider ce formulaire de caractérisation ? Il ne sera plus modifiable et les données seront rendus publiques."
                  onConfirm={() => {
                    formContext.setValue(
                      "status",
                      CLEANUP_FORM_STATUS.PUBLISHED
                    );
                    submit("published");
                  }}
                  onClose={() => setIsConfirmDialogOpen(false)}
                  confirm="Valider"
                />
              </>
            )}
            {readOnly && pathname === "/forms" && (
              <ButtonLink
                href={`/cleanups/${record?.cleanup_id}`}
                variant="contained"
                color="primary"
                endIcon={<ArrowForwardRounded />}
              >
                Revenir au ramassage
              </ButtonLink>
            )}
            {canPublish && record?.status === "published" && (
              <Button
                endIcon={<VisibilityOff />}
                size="large"
                variant="text"
                color="error"
                onClick={() => {
                  formContext.setValue("status", CLEANUP_FORM_STATUS.TOCHECK);
                  submit("unpublished");
                }}
              >
                Dépublier
              </Button>
            )}
          </Stack>
        ) : (
          <>
            <Button
              endIcon={<ArrowForwardRounded />}
              size="large"
              variant="contained"
              onClick={() => changeStep()}
              sx={{
                display: {
                  xs: "none",
                  sm: "flex",
                },
              }}
              disabled={props.disabled}
            >
              Continuer
            </Button>
            <Fab
              color="primary"
              onClick={() => changeStep()}
              sx={{
                display: {
                  sm: "none",
                },
              }}
            >
              <ArrowForwardRounded />
            </Fab>
          </>
        )}
      </Stack>
    </Stack>
  );
};

export const AdminCleanupFormForm: FC<AdminCleanupFormFormProps> = (props) => (
  <Form onSubmit={props.onSubmit as any} disabled={props.disabled}>
    <FormComponent {...props} />
  </Form>
);
