import {
  Cleanup_Form,
  Cleanup_Form_Rubbish,
  Cleanup_Form_Rubbish_Detail,
  Rubbish_Category,
} from "@/graphql/types";
import {
  Box,
  Button,
  FormControlLabel,
  Stack,
  Switch,
  Typography,
} from "@mui/material";
import React, { FC, useEffect, useState } from "react";
import {
  ArrayInput,
  FormDataConsumer,
  SimpleFormIterator,
  TextInput,
  minValue,
  number,
  required,
  useRecordContext,
} from "react-admin";
import { useFormContext } from "react-hook-form";
import { AdminCleanupFormFormRubbishBrands } from "./AdminCleanupFormFormRubbishBrands";
import { AdminCleanupFormFormRubbishAreas } from "./AdminCleanupFormFormRubbishAreas";
import { isPositiveInteger } from "@/utils/forms";
import { AppDecimalInput } from "../shared/AppDecimalInput";
import Image from "next/image";

export type FormCategory = {
  category: Rubbish_Category;
  rubbishes: {
    formRubbish: Partial<Cleanup_Form_Rubbish>;
    brands: Partial<Cleanup_Form_Rubbish_Detail>[];
    areas: Partial<Cleanup_Form_Rubbish_Detail>[];
  }[];
};

type AdminCleanupFormFormRubbishesProps = {
  formCategoryFieldset: "qty" | "wt_vol";
};

export const AdminCleanupFormFormRubbishes: FC<
  AdminCleanupFormFormRubbishesProps
> = (props) => {
  const { formCategoryFieldset } = props;
  const formContext = useFormContext();
  const value = formContext.watch(`${formCategoryFieldset}Categories`);
  const cleanupForm = useRecordContext<Cleanup_Form>();
  const isButtsCleanup =
    cleanupForm?.cleanup?.cleanup_type?.id === "butts" ? true : false;

  const readOnly = cleanupForm?.status === "published" ? true : false;

  const calculateTotals = (
    scopedFormData: any,
    getSource: any,
    type: string
  ) => {
    const total = scopedFormData.areas.reduce((acc: any, area: any) => {
      return acc + parseFloat(area[type]);
    }, 0);
    formContext.setValue(getSource(`formRubbish.${type}`), total);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return !value?.length ? null : (
    <ArrayInput
      source={`${formCategoryFieldset}Categories`}
      label=""
      helperText={false}
      sx={{ pl: "0 !important" }}
    >
      <SimpleFormIterator
        fullWidth
        sx={{
          ".RaSimpleFormIterator-line": {
            py: 1,
          },
        }}
        disableReordering
        disableClear
        disableAdd
        disableRemove
      >
        <FormDataConsumer<FormCategory>>
          {({ scopedFormData, getSource }) => {
            return !scopedFormData ? null : (
              <Box sx={{ width: "100%" }}>
                <Typography
                  variant="h6"
                  color="primary"
                  sx={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    fontWeight: 600,
                  }}
                >
                  <Image
                    width={24}
                    height={24}
                    alt={`Catégorie ${scopedFormData?.category.label}`}
                    src={`/assets/icons/rubbish-category/${scopedFormData?.category.id}.png`}
                    style={{
                      verticalAlign: "middle",
                      marginRight: "8px",
                    }}
                    onError={(e) => {
                      const img = e.target as HTMLImageElement;
                      img.onerror = null;
                      img.srcset = "";
                      img.alt = "";
                      img.width = 0;
                      img.height = 0;
                    }}
                  />
                  {scopedFormData?.category.label}
                </Typography>
                <ArrayInput
                  source={getSource("rubbishes")}
                  label=""
                  helperText={false}
                  sx={{ pl: "0 !important" }}
                >
                  <SimpleFormIterator
                    fullWidth
                    sx={{
                      ".RaSimpleFormIterator-line": {
                        py: 1,
                      },
                    }}
                    disableReordering
                    disableClear
                    disableAdd
                    disableRemove
                  >
                    <FormDataConsumer<FormCategory["rubbishes"][number]>>
                      {({ scopedFormData, getSource }) => {
                        const [showBrands, setShowBrands] = useState(false);
                        const [showAreas, setShowAreas] = useState(false);
                        const hasMultiAreas = Boolean(
                          formContext.getValues(getSource("areas"))?.length > 1
                        );
                        return !scopedFormData ? null : (
                          <Stack sx={{ width: "100%" }}>
                            <Typography>
                              {scopedFormData?.formRubbish?.rubbish?.label}
                            </Typography>
                            {scopedFormData?.formRubbish?.rubbish
                              ?.is_support_brand &&
                              formCategoryFieldset == "qty" && (
                                <>
                                  {!readOnly && (
                                    <Box>
                                      <FormControlLabel
                                        label={
                                          showBrands
                                            ? "Masquer les marques"
                                            : "Afficher les marques"
                                        }
                                        value={showBrands}
                                        control={
                                          <Switch
                                            checked={showBrands}
                                            onChange={(e, isChecked) =>
                                              setShowBrands(isChecked)
                                            }
                                          />
                                        }
                                      />
                                    </Box>
                                  )}
                                  {(showBrands ||
                                    (readOnly &&
                                      formContext.getValues(getSource("brands"))
                                        .length > 0)) && (
                                    <AdminCleanupFormFormRubbishBrands
                                      getSource={getSource}
                                    />
                                  )}
                                </>
                              )}
                            {isButtsCleanup ? (
                              <Box>
                                <Typography>Zones du ramassages</Typography>
                              </Box>
                            ) : (
                              hasMultiAreas && (
                                <Box>
                                  <FormControlLabel
                                    label={
                                      showAreas
                                        ? "Masquer les zones"
                                        : "Afficher les zones"
                                    }
                                    checked={showAreas}
                                    control={
                                      <Switch
                                        value={showAreas}
                                        onChange={(e, isChecked) =>
                                          setShowAreas(isChecked)
                                        }
                                      />
                                    }
                                  />
                                </Box>
                              )
                            )}
                            {(showAreas || isButtsCleanup) && (
                              <AdminCleanupFormFormRubbishAreas
                                getSource={getSource}
                                fielset={formCategoryFieldset}
                              />
                            )}
                            {formCategoryFieldset == "qty" && (
                              <Box>
                                {showAreas || isButtsCleanup ? (
                                  <Box
                                    sx={{
                                      display: "flex",
                                      alignItems: "center",
                                      gap: 2,
                                    }}
                                  >
                                    {!readOnly && (
                                      <Button
                                        variant="contained"
                                        onClick={() =>
                                          calculateTotals(
                                            scopedFormData,
                                            getSource,
                                            "quantity"
                                          )
                                        }
                                      >
                                        Total des zones
                                      </Button>
                                    )}
                                    <TextInput
                                      label="Nombre total des zones"
                                      source={getSource("formRubbish.quantity")}
                                      value={formContext.watch(
                                        getSource("formRubbish.quantity")
                                      )}
                                      validate={[
                                        required(),
                                        number(),
                                        isPositiveInteger,
                                      ]}
                                      inputProps={{
                                        inputMode: "numeric",
                                        readonly: readOnly ? "" : null,
                                      }}
                                    />
                                  </Box>
                                ) : (
                                  <TextInput
                                    label="Nombre total"
                                    source={getSource("formRubbish.quantity")}
                                    fullWidth
                                    inputProps={{
                                      inputMode: "numeric",
                                      readonly: readOnly ? "" : null,
                                    }}
                                    validate={[
                                      required(),
                                      number(),
                                      isPositiveInteger,
                                    ]}
                                  />
                                )}
                              </Box>
                            )}
                            {formCategoryFieldset == "wt_vol" && (
                              <Stack
                                direction={{
                                  xs: "column",
                                  sm: "row",
                                }}
                                spacing={{
                                  xs: 0,
                                  sm: 2,
                                }}
                              >
                                <Box
                                  sx={{
                                    width: { xs: "100%", sm: "50%" },
                                  }}
                                >
                                  {showAreas && !readOnly && (
                                    <Button
                                      variant="contained"
                                      onClick={() =>
                                        calculateTotals(
                                          scopedFormData,
                                          getSource,
                                          "weight"
                                        )
                                      }
                                    >
                                      Calculer Poids Total
                                    </Button>
                                  )}
                                  <AppDecimalInput
                                    label="Poids total (kg)"
                                    source={getSource("formRubbish.weight")}
                                    value={formContext.watch(
                                      getSource("formRubbish.weight")
                                    )}
                                    fullWidth
                                    validate={[
                                      required(),
                                      number(),
                                      minValue(0),
                                    ]}
                                    inputProps={{
                                      readonly: readOnly ? "" : null,
                                    }}
                                  />
                                </Box>
                                <Box
                                  sx={{
                                    width: { xs: "100%", sm: "50%" },
                                  }}
                                >
                                  {showAreas && !readOnly && (
                                    <Button
                                      variant="contained"
                                      onClick={() =>
                                        calculateTotals(
                                          scopedFormData,
                                          getSource,
                                          "volume"
                                        )
                                      }
                                    >
                                      Calculer Volume Total
                                    </Button>
                                  )}
                                  <AppDecimalInput
                                    label="Volume total (L)"
                                    source={getSource("formRubbish.volume")}
                                    value={formContext.watch(
                                      getSource("formRubbish.volume")
                                    )}
                                    fullWidth
                                    validate={[
                                      required(),
                                      number(),
                                      minValue(0),
                                    ]}
                                    inputProps={{
                                      readonly: readOnly ? "" : null,
                                    }}
                                  />
                                </Box>
                              </Stack>
                            )}
                          </Stack>
                        );
                      }}
                    </FormDataConsumer>
                  </SimpleFormIterator>
                </ArrayInput>
              </Box>
            );
          }}
        </FormDataConsumer>
      </SimpleFormIterator>
    </ArrayInput>
  );
};
