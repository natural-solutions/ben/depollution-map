import {
  Cleanup_Area,
  Cleanup_Form,
  Cleanup_Form_Poi,
  Poi_Type,
} from "@/graphql/types";
import { isPositiveInteger } from "@/utils/forms";
import { Add } from "@mui/icons-material";
import { Button, Grid, Typography } from "@mui/material";
import React, { FC } from "react";
import {
  ArrayInput,
  FormDataConsumer,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  required,
  useGetList,
  useRecordContext,
} from "react-admin";

type AdminCleanupFormFormPoisProps = { cleanupId: string };

export const AdminCleanupFormFormPois: FC<AdminCleanupFormFormPoisProps> = ({
  cleanupId,
}) => {
  const { data: poi_types, isLoading: isLoadingPoiTypes } =
    useGetList<Poi_Type>("poi_type", {
      pagination: {
        page: 1,
        perPage: 1000000,
      },
      sort: {
        field: "label",
        order: "ASC",
      },
    });

  const { data: allAreas, isLoading: isLoadingAreas } =
    useGetList<Cleanup_Area>(
      "cleanup_area",
      {
        pagination: {
          page: 1,
          perPage: 1000000,
        },
        sort: {
          field: "label",
          order: "ASC",
        },
        filter: { cleanup_id: cleanupId },
      },
      {
        retry: false,
      }
    );

  const areas: Cleanup_Area[] =
    allAreas?.filter((area) => area.geom?.geometry?.type === "Polygon") || [];

  const record = useRecordContext<Cleanup_Form>();
  const readOnly = record?.status === "published" ? true : false;

  return (
    <>
      <Typography variant="h4" sx={{ mb: 2 }}>
        Points d'intérêt
      </Typography>
      {record.cleanup?.cleanup_areas
        ?.filter(({ geom }) => geom.geometry.type === "Polygon")
        .map((cleanup_area) => (
          <ArrayInput
            key={cleanup_area.id}
            source={`poiAreas.${cleanup_area.id}`}
            label={
              <Typography variant="h4" sx={{ color: "#000000", mb: 1 }}>
                {cleanup_area.label || ""}
              </Typography>
            }
          >
            <SimpleFormIterator
              inline
              fullWidth
              disableClear
              disableReordering
              disableAdd={readOnly}
              disableRemove={readOnly}
              addButton={
                <Button size="small" variant="contained" startIcon={<Add />}>
                  Ajouter un point d'intérêt
                </Button>
              }
            >
              <FormDataConsumer<Cleanup_Form_Poi>>
                {({ getSource, scopedFormData }) => (
                  <Grid container spacing={1}>
                    <Grid item xs>
                      <SelectInput
                        fullWidth
                        source={getSource("poi_type_id")}
                        label="Point d'intérêt"
                        choices={poi_types}
                        optionValue="id"
                        optionText="label"
                        validate={required()}
                        isLoading={isLoadingPoiTypes}
                        inputProps={{
                          inputMode: "numeric",
                          readonly: readOnly ? "" : null,
                        }}
                        sx={{ pointerEvents: readOnly ? "none" : "auto" }}
                      />
                    </Grid>
                    <Grid item xs>
                      <TextInput
                        fullWidth
                        source={getSource("nb")}
                        label="Nombre"
                        inputProps={{
                          inputMode: "numeric",
                          readonly: readOnly ? "" : null,
                        }}
                        validate={[required(), isPositiveInteger]}
                      />
                    </Grid>
                  </Grid>
                )}
              </FormDataConsumer>
            </SimpleFormIterator>
          </ArrayInput>
        ))}
    </>
  );
};
