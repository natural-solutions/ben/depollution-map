import {
  AutocompleteInput,
  DateInput,
  SelectInput,
  TextInput,
  required,
  useGetList,
} from "react-admin";
import { Cleanup, Cleanup_Form, Rubbish_Type_Picked } from "@/graphql/types";
import { FC, useEffect, useState } from "react";
import { rainForces, windForces } from "./AdminCleanupFormList";
import { Typography } from "@mui/material";
import { isPositiveInteger, regexValidation } from "@/utils/forms";
import { useApi } from "@/utils/useApi";
import {
  GET_CLEANUP_AREA_ENVIRONMENT_ID,
  GET_RUBBISH_TYPE_PICKED,
} from "@/graphql/queries";
import { useLocation } from "react-router-dom";

const InputCleanup = ({
  onCleanupChange,
}: {
  onCleanupChange: (value: string) => void;
}) => {
  const { data: choices } = useGetList("cleanup", {
    pagination: { page: 1, perPage: 1000000 },
  });

  // TODO Autocomplete server side
  /* const [filter, setFilter] = useState({ label: "" });
  const id = useWatch({ name: "cleanup_id" });
  const dataProvider = useDataProvider();
  const { data: choises, isLoading: isLoadingChoices } = useGetList("cleanup", {
    filter: filter,
  });

  useEffect(() => {
    if (!id) {
      return;
    }
    (async () => {
      console.log("call id", id);

      const resp = await dataProvider.getOne("cleanup", { id });
      console.log(resp);
    })();
  }, [id]);
  
  const { data: current, isLoading: isLoadingCurrent } = useGetOne("cleanup", {
    id: id || "",
  });
  const choicesWithCurrent = choises
    ? choises.find((choice) => choice.id === id)
      ? choises
      : [...choises, current]
    : []; */

  return (
    <AutocompleteInput
      label="Ramassage"
      source="cleanup_id"
      choices={choices}
      optionText="label"
      optionValue="id"
      validate={required()}
      onChange={(value) => {
        onCleanupChange(value);
      }}
    />
  );
};

export const AdminCleanupFormFormBase: FC<{
  record: Cleanup_Form;
}> = (props) => {
  const { record } = props;
  const api = useApi();
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const initialCleanupId =
    searchParams.get("cleanup_id") || record?.cleanup_id || null;
  const [cleanupId, setCleanupId] = useState<string | null>(initialCleanupId);
  const [rubbishTypes, setRubbishTypes] = useState<Rubbish_Type_Picked[]>([]);
  const readOnly = record?.status === "published" ? true : false;

  const handleCleanupChange = (value: string) => {
    setCleanupId(value);
  };

  useEffect(() => {
    if (cleanupId) {
      (async () => {
        const resp_cleanup = await api.postGraphql<{
          data: {
            cleanup_by_pk: Cleanup;
          };
        }>({
          query: GET_CLEANUP_AREA_ENVIRONMENT_ID,
          variables: { id: cleanupId },
        });

        if (resp_cleanup.data.data.cleanup_by_pk) {
          const rubbishTypePicked = await api.postGraphql<{
            data: {
              rubbish_type_picked: Rubbish_Type_Picked[];
            };
          }>({
            query: GET_RUBBISH_TYPE_PICKED,
            variables: {
              where: {
                environment_area_type_rubbish_type_pickeds: {
                  environment_area_type: {
                    area_type_id: {
                      _eq: resp_cleanup.data.data.cleanup_by_pk.area_type_id,
                    },
                    environment_type_id: {
                      _eq: resp_cleanup.data.data.cleanup_by_pk
                        .environment_type_id,
                    },
                  },
                },
              },
            },
          });
          setRubbishTypes(rubbishTypePicked.data.data.rubbish_type_picked);
        }
      })();
    }
  }, [cleanupId]);

  return (
    <>
      <DateInput
        defaultValue={record?.cleanup?.start_at}
        source="performed_at"
        label="Réalisé le"
        validate={required()}
        inputProps={{ readonly: readOnly ? "" : null }}
      />
      {initialCleanupId === null && (
        <InputCleanup onCleanupChange={handleCleanupChange} />
      )}
      <SelectInput
        label="Type de déchets ramassés"
        source="rubbish_type_picked_id"
        optionText="label"
        optionValue="id"
        choices={rubbishTypes}
        validate={required()}
        sx={{ pointerEvents: readOnly ? "none" : "auto" }}
      />
      <TextInput
        label="Nombre de bénévole de l'association"
        source="volunteers_number"
        inputProps={{ inputMode: "numeric", readonly: readOnly ? "" : null }}
        validate={[required(), isPositiveInteger]}
      />
      <TextInput
        label="Nombre de participants externes"
        source="external_volunteers_number"
        inputProps={{ inputMode: "numeric", readonly: readOnly ? "" : null }}
        validate={[required(), isPositiveInteger]}
      />
      <TextInput
        label="Durée du ramassage"
        source="cleanup_duration"
        type="time"
        validate={[
          required(),
          regexValidation(/^(?!00:00$)/, "Saisir une durée"),
        ]}
        defaultValue="00:00"
        inputProps={{ readonly: readOnly ? "" : null }}
      />
      <TextInput
        source="comment"
        label="Commentaire"
        multiline
        inputProps={{ readonly: readOnly ? "" : null }}
      />
      <Typography variant="h5" sx={{ mb: 1 }}>
        Conditions météorologiques
      </Typography>
      <SelectInput
        label="Force du vent"
        source="wind_force"
        choices={windForces}
        validate={required()}
        sx={{ pointerEvents: readOnly ? "none" : "auto" }}
      />
      <SelectInput
        label="Force de la pluie"
        source="rain_force"
        choices={rainForces}
        validate={required()}
        sx={{ pointerEvents: readOnly ? "none" : "auto" }}
      />
    </>
  );
};
