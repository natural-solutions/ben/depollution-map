import { getCleanupForm } from "@/utils/cleanups";
import BarChart, {
  BarChartProps,
} from "../../../components/dashboard/BarChart";
import { Cleanup, Rubbish } from "@/graphql/types";
import { useApi } from "@/utils/useApi";
import {
  Alert,
  Box,
  Checkbox,
  FormControlLabel,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  Switch,
  Typography,
} from "@mui/material";
import React, { FC, useEffect, useState } from "react";

type CleanupCompareDisplayMode = "grouped" | "split_by_rubbish";

type CleanupCompareProps = {
  ids: string[];
  selectedRubbishIds?: string[];
  onSelectedRubbishIdsChange?: (ids: string[]) => void;
  displayMode?: CleanupCompareDisplayMode;
  onDisplayModeChange?: (value: CleanupCompareDisplayMode) => void;
};

export const CleanupCompare: FC<CleanupCompareProps> = ({
  ids,
  onSelectedRubbishIdsChange,
  selectedRubbishIds = [],
  displayMode = "grouped",
  onDisplayModeChange,
}) => {
  const { postGraphql } = useApi();
  const [rubbishes, setRubbishes] = useState<Rubbish[]>([]);
  const [groupedChartData, setGroupedChartData] =
    useState<BarChartProps["data"]>();
  const [splittedCharts, setSplittedCharts] = useState<
    {
      rubbish: Rubbish;
      total: number;
      data: BarChartProps["data"];
    }[]
  >();

  useEffect(() => {
    (async () => {
      const resp = await postGraphql({
        query: `query GetRubbihesByCleanups($ids: [uuid!]) {
          cleanup_form_rubbish(where: {cleanup_form: {cleanup_id: {_in: $ids}}}, distinct_on: rubbish_id, order_by: [{rubbish_id: asc}]) {
            rubbish {
              id
              label
            }
          }
        }`,
        variables: {
          ids,
        },
      });
      const rubbishes = resp.data.data?.cleanup_form_rubbish?.map(
        (item: any) => ({
          id: item.rubbish.id,
          label: item.rubbish.label,
        })
      ) as Rubbish[];
      rubbishes.sort((a, b) => {
        if (a.label < b.label) {
          return -1;
        }
        if (a.label > b.label) {
          return 1;
        }
        return 0;
      });
      setRubbishes(rubbishes);
    })();
  }, [ids]);

  const toggleRubbish = (rubbish: Rubbish) => {
    const itemIndex = selectedRubbishIds.indexOf(rubbish.id);

    const newValues = [...(selectedRubbishIds || [])];

    if (itemIndex === -1) {
      newValues.push(rubbish.id);
    } else {
      newValues.splice(itemIndex, 1);
    }

    onSelectedRubbishIdsChange?.(newValues);
  };

  useEffect(() => {
    (async () => {
      const resp = await postGraphql({
        query: `query GetDashboardCleanups($ids: [uuid!], $rubbishIds: [String!]) {
          cleanup(where: {id: {_in: $ids}}, order_by: {start_at: asc}) {
            id
            start_at
            label
            cleanup_forms {
              id
              cleanup_form_rubbishes(where: {rubbish_id: {_in: $rubbishIds}}) {
                rubbish {
                  id
                  label
                }
                quantity
                weight
                volume
              }
            }
          }
        }`,
        variables: {
          ids,
          rubbishIds: selectedRubbishIds,
        },
      });
      const cleanups: Cleanup[] = resp.data?.data?.cleanup;
      if (!cleanups?.length) {
        setGroupedChartData(undefined);
        setSplittedCharts(undefined);

        return;
      }

      const groupedChartSeries = getSelectedRubbishes().map((rubbish) => ({
        name: rubbish.label,
        data: cleanups.map(
          (cleanup) =>
            getCleanupForm(cleanup)?.cleanup_form_rubbishes?.find(
              (cleanup_form_rubbish) =>
                cleanup_form_rubbish.rubbish.id === rubbish.id
            )?.quantity || 0
        ),
      }));

      groupedChartSeries.forEach((chartSerie) => {
        const total = chartSerie.data.reduce((a, b) => a + b, 0);
        chartSerie.name = `${chartSerie.name} (${total})`;
      });

      setGroupedChartData({
        xaxis: cleanups.map((cleanup) => cleanup.label),
        series: groupedChartSeries.map((chartSerie) => ({
          ...chartSerie,
          color: "#3DA0EA",
        })),
      });

      setSplittedCharts(
        getSelectedRubbishes().map((rubbish) => {
          const series = [
            {
              name: rubbish.label,
              data: cleanups.map(
                (cleanup) =>
                  getCleanupForm(cleanup)?.cleanup_form_rubbishes?.find(
                    (cleanup_form_rubbish) =>
                      cleanup_form_rubbish.rubbish.id === rubbish.id
                  )?.quantity || 0
              ),
              color: "#3DA0EA",
            },
          ];

          return {
            rubbish,
            total: series[0].data.reduce((a, b) => a + b, 0),
            data: {
              xaxis: cleanups.map((cleanup) => cleanup.label),
              series,
            },
          };
        })
      );
    })();
  }, [selectedRubbishIds]);

  const getSelectedRubbishes = () => {
    return selectedRubbishIds
      .map((selectedId) => rubbishes.find(({ id }) => id === selectedId))
      .filter((x) => x) as Rubbish[];
  };

  return (
    <Stack
      sx={{
        alignItems: "flex-start",
      }}
    >
      <Stack direction="row" sx={{ width: "100%" }}>
        <Box>
          <List sx={{ bgcolor: "background.paper", width: "250px" }}>
            {rubbishes.map((rubbish) => (
              <ListItem key={rubbish.id} disablePadding>
                <ListItemButton onClick={() => toggleRubbish(rubbish)} dense>
                  <ListItemIcon
                    sx={{
                      minWidth: "24px",
                    }}
                  >
                    <Checkbox
                      edge="start"
                      checked={selectedRubbishIds.includes(rubbish.id)}
                      tabIndex={-1}
                      disableRipple
                    />
                  </ListItemIcon>
                  <ListItemText primary={rubbish.label} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Box>
        <Stack sx={{ ml: 3, flex: 1 }}>
          {!Boolean(selectedRubbishIds.length) ? (
            <Alert severity="warning">
              <Typography>Sélectionner un ou plusieurs déchets</Typography>
            </Alert>
          ) : !Boolean(groupedChartData) ? (
            <Alert severity="warning">
              <Typography>Pas de données pour cette sélection</Typography>
            </Alert>
          ) : (
            <Stack>
              <Stack direction="row" sx={{ mb: 3 }}>
                <FormControlLabel
                  control={
                    <Switch
                      checked={displayMode === "split_by_rubbish"}
                      onChange={(e: any, checked: boolean) =>
                        onDisplayModeChange?.(
                          checked ? "split_by_rubbish" : "grouped"
                        )
                      }
                    />
                  }
                  label="Un graph par déchet"
                />
              </Stack>
              {displayMode == "grouped" ? (
                <BarChart
                  data={groupedChartData!}
                  height={450}
                  yAxisLabelsFormatter={(value) => value?.toString()}
                  tooltipYFormatter={(value) => `${value} unités`}
                />
              ) : (
                <Stack spacing={4}>
                  {splittedCharts?.map((chart) => (
                    <Stack key={chart.rubbish.id}>
                      <Typography
                        variant="h6"
                        sx={{
                          textAlign: "center",
                        }}
                      >
                        {chart.rubbish.label} ({chart.total})
                      </Typography>
                      <BarChart
                        data={chart.data}
                        height={450}
                        yAxisLabelsFormatter={(value) => value?.toString()}
                        tooltipYFormatter={(value) => `${value} unités`}
                      />
                    </Stack>
                  ))}
                </Stack>
              )}
            </Stack>
          )}
        </Stack>
      </Stack>
    </Stack>
  );
};
