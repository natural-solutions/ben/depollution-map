import pick from "lodash/pick";
import { useState } from "react";
import { EditBase, useNotify, useRefresh } from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { CleanupPatch } from "@/graphql/queries";
import { Cleanup } from "@/graphql/types";
import { AppFile, slugify, uploadFile } from "@/utils/media";
import { useApi } from "@/utils/useApi";
import { AdminCleanupForm, AdminCleanupFormData } from "./AdminCleanupForm";
import { usePathname, useRouter } from "next/navigation";
import { useSessionContext } from "@/utils/useSessionContext";
import { Typography } from "@mui/material";

export const AdminCleanupEdit = () => {
  const notify = useNotify();
  const [record, setRecord] = useState<Cleanup>();
  const { id } = useParams();
  const { api } = useApi();
  const navigate = useNavigate();
  const router = useRouter();
  const path = usePathname();
  const refresh = useRefresh();
  const sessionCtx = useSessionContext();
  const [isForbidden, setIsForbidden] = useState(false);

  const onRecordReady = (record: Cleanup) => {
    if (!sessionCtx.getCanEditCleanup(record)) {
      return setIsForbidden(true);
    }
    setRecord(record);
  };

  const onSubmit = async (_set: AdminCleanupFormData) => {
    if (!id) {
      return;
    }

    const newPhotos: AppFile[] = [];
    _set.cleanup_photos?.forEach((photo) => {
      const rawFile = photo.file?.rawFile;
      if (rawFile) {
        rawFile.slugified = slugify(rawFile.name);
        photo.filename = rawFile.slugified;
        newPhotos.push(rawFile);
        delete photo.file;
      }
    });

    const cleanup_photos_delete_ids = !_set.cleanup_photos
      ? undefined
      : record?.cleanup_photos
          ?.filter((existing) => {
            return !_set.cleanup_photos?.some(
              (photo) => photo.id === existing.id
            );
          })
          .map((item) => item.id);

    const cleanup_areas_delete_ids = !_set.cleanup_areas
      ? undefined
      : record?.cleanup_areas
          ?.filter((existing) => {
            return !_set.cleanup_areas?.some((area) => area.id === existing.id);
          })
          .map((item) => item.id);

    try {
      await api.patch(`/cleanups/${id}`, {
        ...pick(_set, [
          "label",
          "total_area",
          "total_linear",
          "start_at",
          "start_point",
          "cleanup_areas",
          "cleanup_partners",
          "cleanup_type_id",
          "city_id",
          "cleanup_place_id",
          "environment_type_id",
          "area_type_id",
          "description",
        ]),
        cleanup_areas_delete_ids,
        cleanup_photos_delete_ids,
        cleanup_photos: _set.cleanup_photos?.map((cleanup_photo, index) => ({
          id: cleanup_photo.id,
          filename: cleanup_photo.filename,
          comment: cleanup_photo.comment,
          cleanup_area_id: cleanup_photo.cleanup_area_id,
          rank: index + 1,
          cleanup_id: id,
        })),
        cleanup_campaigns: _set.cleanup_campaigns
          ?.filter((row) => row.campaign_id)
          .map(({ campaign_id }) => ({
            campaign_id,
          })),
      } as CleanupPatch);

      for (const file of newPhotos) {
        try {
          await uploadFile("cleanups", id, file);
        } catch (error: any) {
          notify(error, {
            type: "error",
          });
        }
      }
      if (path === "/admin") {
        refresh();
        navigate("/cleanup");
      } else {
        router.replace(`/cleanups/${id}`);
      }
    } catch (error) {}
  };

  return (
    <EditBase>
      {isForbidden ? (
        <Typography>Vous ne pouvez pas editer ce ramassage.</Typography>
      ) : (
        <AdminCleanupForm onSubmit={onSubmit} onRecordReady={onRecordReady} />
      )}
    </EditBase>
  );
};
