import { Download } from "@mui/icons-material";
import { buildGetListVariables } from "ra-data-hasura/dist/buildVariables/buildGetListVariables";
import React, { FC } from "react";
import { useListContext } from "react-admin";
import schema from "../../../graphql/schema.json";
import { FetchType } from "ra-data-hasura";
import { Button, Stack } from "@mui/material";
import { TooltipLight } from "@/components/TooltipLight";

type CleanupExportBtnProps = {};

export const CleanupExportBtn: FC<CleanupExportBtnProps> = (props) => {
  const { filter, filterValues, sort, selectedIds } = useListContext();

  const res = schema.__schema.types.find(
    (type) => type.kind === "OBJECT" && type.name === "cleanup"
  );
  const variables = buildGetListVariables(schema as any)(
    { type: res } as any,
    FetchType.GET_LIST,
    {
      filter: filterValues,
      sort,
    }
  );

  if (selectedIds.length) {
    variables.where = {
      id: {
        _in: selectedIds,
      },
    };
  }

  return (
    <TooltipLight
      slotProps={{
        popper: {
          modifiers: [
            {
              name: "offset",
              options: {
                offset: [0, -10],
              },
            },
          ],
        },
      }}
      title={
        <Stack alignItems="start">
          <Button
            size="small"
            href={`/api/cleanups/exports?variables=${JSON.stringify(
              variables
            )}`}
            target="_blank"
            sx={{ textTransform: "none" }}
          >
            WINGS .xlsx
          </Button>
          <Button
            size="small"
            href={`/api/cleanups/exports/zds?variables=${JSON.stringify(
              variables
            )}`}
            target="_blank"
            sx={{ textTransform: "none" }}
          >
            ZDS .csv
          </Button>
          <Button
            size="small"
            href={`/api/cleanups/exports/areas?variables=${JSON.stringify(
              variables
            )}`}
            target="_blank"
            sx={{ textTransform: "none" }}
          >
            ZONES .json
          </Button>
        </Stack>
      }
    >
      <Button
        sx={{
          paddingTop: 0,
          paddingBottom: "1px",
        }}
        startIcon={<Download />}
        size="small"
      >
        Exporter
      </Button>
    </TooltipLight>
  );
};
