import { useSearchParams, useNavigate } from "react-router-dom";
import React, { FC } from "react";
import { JsonParseSafe } from "@/utils/utils";
import { Box } from "@mui/material";
import { CleanupCompare } from "./CleanupCompare";

type CleanupComparePageProps = {};

export const CleanupComparePage: FC<CleanupComparePageProps> = (props) => {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const cleanupIds = JsonParseSafe(searchParams.get("ids")) || [];
  const selectedRubbishIds =
    JsonParseSafe(searchParams.get("selected_rubbish_ids")) || [];
  const displayMode = searchParams.get("diplay_mode");

  const setQs = (name: string, value: string) => {
    const params = new URLSearchParams(searchParams);
    params.set(name, value);
    navigate(`/cleanup-compare?${params.toString()}`, {
      replace: true,
    });
  };

  return (
    <Box sx={{ p: 3 }}>
      <CleanupCompare
        ids={cleanupIds}
        selectedRubbishIds={selectedRubbishIds}
        onSelectedRubbishIdsChange={(ids) => {
          setQs("selected_rubbish_ids", JSON.stringify(ids));
        }}
        displayMode={(displayMode as any) || "grouped"}
        onDisplayModeChange={(value) => {
          setQs("diplay_mode", value);
        }}
      />
    </Box>
  );
};
