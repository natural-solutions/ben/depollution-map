import pick from "lodash/pick";
import { Cleanup, Cleanup_Insert_Input } from "@/graphql/types";
import { CreateBase, useNotify, useRefresh } from "react-admin";
import { useNavigate } from "react-router-dom";
import { AdminCleanupForm, AdminCleanupFormData } from "./AdminCleanupForm";
import { useApi } from "@/utils/useApi";
import { GET_CLEANUP } from "@/graphql/queries";
import { AppFile, slugify, uploadFile } from "@/utils/media";
import { usePathname, useRouter } from "next/navigation";
import { useLocation } from "react-router-dom";
import queryString from "query-string";
import { useEffect, useState } from "react";
import { useSessionContext } from "@/utils/useSessionContext";
import { USER_ROLE } from "@/utils/user";

export const AdminCleanupCreate = () => {
  const { postGraphql, api } = useApi();
  const notify = useNotify();
  const navigate = useNavigate();
  const router = useRouter();
  const path = usePathname();
  const refresh = useRefresh();
  const sessionCtx = useSessionContext();
  const location = useLocation();
  const params = queryString.parse(location.search);
  const from_cleanup_id = params.from_cleanup_id;

  const [cleanupToDuplicate, setCleanupToDuplicate] = useState<Cleanup>();

  useEffect(() => {
    if (from_cleanup_id) {
      (async () => {
        const resp = await postGraphql<{
          data: {
            cleanup_by_pk: Cleanup;
          };
        }>({
          query: GET_CLEANUP,
          variables: { id: from_cleanup_id },
        });

        const cleanup = resp.data.data.cleanup_by_pk;
        cleanup.start_at = null;

        cleanup.cleanup_partners.forEach((cleanup_partner: any) => {
          delete cleanup_partner.cleanup_id;
          delete cleanup_partner.partner;
        });

        delete (cleanup as any).cleanup_photos;

        cleanup.cleanup_campaigns = cleanup.cleanup_campaigns.filter(
          ({ campaign_id }) =>
            sessionCtx.getHasCampaignRole(campaign_id, USER_ROLE.EDITOR)
        );

        setCleanupToDuplicate(cleanup);
      })();
    }
  }, []);

  const onSubmit = async (_set: AdminCleanupFormData) => {
    const uploads: AppFile[] = [];
    _set.cleanup_photos?.forEach((photo) => {
      const rawFile = photo.file?.rawFile;
      if (rawFile) {
        rawFile.slugified = slugify(rawFile.name);
        photo.filename = rawFile.slugified;
        uploads.push(rawFile);
        delete photo.file;
      }
    });

    try {
      const { data } = await api.post(`/cleanups`, {
        ...(pick(_set, [
          "area_type_id",
          "city_id",
          "cleanup_type_id",
          "cleanup_place_id",
          "environment_type_id",
          "label",
          "start_at",
          "start_point",
          "total_area",
          "total_linear",
          "cleanup_partners",
          "cleanup_areas",
          "description",
        ]) as any),
        cleanup_photos: _set.cleanup_photos?.map((cleanup_photo, index) => {
          return {
            ...pick(cleanup_photo, ["comment", "filename"]),
            rank: index + 1,
          };
        }),
        cleanup_campaigns:
          _set.cleanup_campaigns
            ?.filter((row) => row.campaign_id)
            .map(({ campaign_id }) => ({
              campaign_id,
            })) || [],
      } as Cleanup_Insert_Input);

      if (data.errors) {
        // TODO "Le formulaire n'est pas valide. Veuillez vérifier les erreurs."
        notify(data.errors[0].message, {
          type: "error",
        });
        return;
      }

      for (const file of uploads) {
        try {
          await uploadFile("cleanups", data.id, file);
        } catch (error) {}
      }
      if (path === "/admin") {
        refresh();
        navigate("/cleanup");
      } else {
        router.replace(`/cleanups/${data.id}`);
      }
    } catch (error) {}
  };

  return (
    <CreateBase>
      <AdminCleanupForm
        initialData={cleanupToDuplicate}
        isNew={true}
        onSubmit={onSubmit}
      />
    </CreateBase>
  );
};
