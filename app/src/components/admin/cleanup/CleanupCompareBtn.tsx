import { Compare } from "@mui/icons-material";
import React, { FC } from "react";
import { Button, useListContext } from "react-admin";

type CleanupCompareBtnProps = {
  onClick: (ids: string[]) => void;
};

export const CleanupCompareBtn: FC<CleanupCompareBtnProps> = ({ onClick }) => {
  const { selectedIds } = useListContext();

  const handleClick = () => {
    onClick(selectedIds);
  };

  return (
    <Button
      startIcon={<Compare />}
      label="Comparer"
      onClick={handleClick}
      disabled={selectedIds.length < 2}
    />
  );
};
