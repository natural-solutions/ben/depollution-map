import {
  CleanupCharacterizationStatus,
  CleanupFormCharacterizationStatus,
} from "@/components/cleanup/CleanupCharacterizationStatus";
import { Cleanup } from "@/graphql/types";
import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  AutocompleteArrayInput,
  ArrayField,
  DateField,
  DateInput,
  EditButton,
  List,
  ReferenceArrayInput,
  TextField,
  TextInput,
  WithListContext,
  FunctionField,
  SelectColumnsButton,
  TopToolbar,
  FilterButton,
  DatagridConfigurable,
  CreateButton,
  BulkDeleteWithConfirmButton,
  WrapperField,
  Identifier,
  IconButtonWithTooltip,
  SelectArrayInput,
} from "react-admin";
import { useNavigate } from "react-router-dom";
import { CleanupCompareBtn } from "./CleanupCompareBtn";
import { useRouter } from "next/navigation";
import { Visibility } from "@mui/icons-material";
import { Box, Stack } from "@mui/material";
import { CleanupExportBtn } from "./CleanupExportBtn";
import { getCleanupForm } from "@/utils/cleanups";

const filters = [
  <TextInput label="Ramassage" source="label" />,
  <ReferenceArrayInput
    label="Commune"
    source="city_id"
    reference="city"
    sort={{ field: "label", order: "ASC" }}
  >
    <AutocompleteArrayInput
      label="Commune"
      optionText="label"
      optionValue="id"
      filterToQuery={(search) => (search ? { "label@_ilike": search } : {})}
    />
  </ReferenceArrayInput>,
  <TextInput label="Département" source="city#departement#id@_ilike" />,
  <TextInput label="Lieux du ramassage" source="cleanup_place#label@_ilike" />,
  <TextInput label="Zone" source="environment_type#label@_ilike" />,
  <TextInput label="Milieu" source="area_type#label@_ilike" />,
  <ReferenceArrayInput
    label="Niveau de carac."
    source="cleanup_type#characterization_level#id"
    reference="characterization_level"
    sort={{ field: "label", order: "ASC" }}
  >
    <SelectArrayInput
      label="Niveau de carac."
      optionText="label"
      optionValue="id"
    />
  </ReferenceArrayInput>,
  <TextInput label="Formulaire de carac." source="cleanup_type#label@_ilike" />,
  <SelectArrayInput
    label="Statut carac."
    source="cleanup_forms#status"
    choices={[
      {
        id: "draft",
        name: "Incomplète",
      },
      {
        id: "tocheck",
        name: "Envoyée",
      },
      ,
      {
        id: "published",
        name: "Publiée",
      },
    ]}
  />,
  <ReferenceArrayInput
    label="Entités wings"
    source="cleanup_campaigns#campaign#id"
    reference="campaign"
    sort={{ field: "label", order: "ASC" }}
  >
    <AutocompleteArrayInput
      label="Entités wings"
      optionText="label"
      optionValue="id"
      filterToQuery={(search) => (search ? { "label@_ilike": search } : {})}
    />
  </ReferenceArrayInput>,
  <ReferenceArrayInput
    label="Partenaires"
    reference="partner"
    source="cleanup_partners#partner#id"
    perPage={10000000}
    sort={{ field: "label", order: "ASC" }}
  >
    <AutocompleteArrayInput
      label="Partenaires"
      optionText="label"
      optionValue="id"
      filterToQuery={(search) => (search ? { "label@_ilike": search } : {})}
    />
  </ReferenceArrayInput>,
  <DateInput label="Date de création =" source="created_at" />,
  <DateInput label="Date du ramassage" source="start_at" />,
  <DateInput label="Date du ramassage >=" source="start_at@_gte" />,
];

type AdminCleanupListProps = {};

export const AdminCleanupList: FC<AdminCleanupListProps> = () => {
  const router = useRouter();
  const sessionCtx = useSessionContext();
  const navigate = useNavigate();
  const isAuthorized = sessionCtx.getIsAdmin();
  const IsAdminInCampaign = sessionCtx.getIsAdminInCampaign();

  const rowClick = (id: Identifier) => {
    router.push(`/cleanups/${id}`);
  };

  const AdminCleanupListActions = () => (
    <TopToolbar>
      {IsAdminInCampaign && <CleanupExportBtn />}
      <SelectColumnsButton />
      <CreateButton />
      <FilterButton filters={filters} />
    </TopToolbar>
  );

  return (
    <List filters={filters} actions={<AdminCleanupListActions />}>
      <DatagridConfigurable
        bulkActionButtons={
          <>
            <CleanupCompareBtn
              onClick={(ids) => {
                navigate(`/cleanup-compare?ids=${JSON.stringify(ids)}`);
              }}
            />
            {IsAdminInCampaign && <CleanupExportBtn />}
            {isAuthorized && (
              <BulkDeleteWithConfirmButton mutationMode="pessimistic" />
            )}
          </>
        }
      >
        <TextField source="id" />
        <TextField source="label" label="Ramassage" />
        <TextField source="city.label" label="Commune" />
        <TextField source="city.departement_id" label="Département" />
        <TextField source="cleanup_place.label" label="Lieux du ramassage" />
        <TextField source="environment_type.label" label="Zone" />
        <TextField source="area_type.label" label="Milieu" />
        <TextField
          source="cleanup_type.characterization_level.label"
          label="Niveau de carac."
        />
        <TextField source="cleanup_type.label" label="Formulaire de carac." />
        <ArrayField source="cleanup_campaigns" label="Entités wings" perPage={3} sortable={false}>
          <WithListContext
            render={(params) => {
              const nbMore = params.total - params.data.length;
              const perPage = 3;
              return (
                <span
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    params.setPerPage(
                      params.perPage != perPage ? perPage : 999999
                    );
                  }}
                >
                  {params.data.map((item) => item.campaign.label).join(", ")}
                  {nbMore > 0 && ` +${nbMore}`}
                </span>
              );
            }}
          />
        </ArrayField>
        <ArrayField source="cleanup_partners" label="Partenaires" perPage={3} sortable={false}>
          <WithListContext
            render={(params) => {
              const nbMore = params.total - params.data.length;
              const perPage = 3;
              return (
                <span
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    params.setPerPage(
                      params.perPage != perPage ? perPage : 999999
                    );
                  }}
                >
                  {params.data.map((item) => item.partner.label).join(", ")}
                  {nbMore > 0 && ` +${nbMore}`}
                </span>
              );
            }}
          />
        </ArrayField>
        <FunctionField
          render={(cleanup: Cleanup) => {
            return (
              <Box>
                <CleanupCharacterizationStatus cleanup={cleanup} />
                <CleanupFormCharacterizationStatus
                  cleanupForm={getCleanupForm(cleanup)}
                />
              </Box>
            );
          }}
          label="Caractérisation"
        />
        <DateField source="start_at" label="Date du ramassage" />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        <WrapperField label="Actions">
          <Stack direction="row" spacing={2}>
            <FunctionField
              render={(record: any) => (
                <IconButtonWithTooltip
                  label="Voir"
                  onClick={() => rowClick(record.id)}
                >
                  <Visibility />
                </IconButtonWithTooltip>
              )}
            />
            {isAuthorized && <EditButton />}
          </Stack>
        </WrapperField>
      </DatagridConfigurable>
    </List>
  );
};
