"use client";

import {
  Alert,
  Avatar,
  Box,
  Button,
  Grid,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import React, { FC, useEffect, useState } from "react";
import {
  ArrayInput,
  AutocompleteInput,
  DateTimeInput,
  Form,
  FormDataConsumer,
  ImageField,
  ImageInput,
  ReferenceInput,
  SaveButton,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  required,
  useCreate,
  useGetList,
  useNotify,
  useRecordContext,
} from "react-admin";
import { useFormContext } from "react-hook-form";
import { Campaign, Cleanup, Cleanup_Photo } from "@/graphql/types";
import {
  AdminCleanupFormMap,
  AdminCleanupFormMapApi,
  AdminCleanupFormMapOnReadyParams,
} from "./AdminCleanupFormMap";
import {
  IMAGOR_PRESET_NAME,
  ReactAdminImageInputReturn,
  getTransformationURL,
  slugify,
} from "@/utils/media";
import {
  AdminCleanupFormMapDialog,
  AdminCleanupFormMapDialogProps,
} from "./AdminCleanupFormMapDialog";
import { useRAContext } from "@/components/r_admin/RAindex";
import { FormsHeader } from "@/components/forms/FormsHeader";
import { MAP_INIT_POINT } from "@/utils/map";
import { useSessionContext } from "@/utils/useSessionContext";
import { GET_LINEAR_AREA } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";

export type AdminCleanupFormPhoto = Cleanup_Photo & {
  file?: ReactAdminImageInputReturn;
};

export type AdminCleanupFormData = Omit<Cleanup, "cleanup_photos"> & {
  cleanup_photos?: AdminCleanupFormPhoto[];
};

type AdminCleanupFormProps = {
  initialData?: Cleanup;
  isNew?: boolean;
  onSubmit: (data: AdminCleanupFormData) => void;
  onRecordReady?: (record: Cleanup) => void;
  mandatoryLinearArea?: {
    isLinear: boolean | null;
    isArea: boolean | null;
  };
  setMandatoryLinearArea?: React.Dispatch<
    React.SetStateAction<{
      isLinear: boolean | null;
      isArea: boolean | null;
    }>
  >;
};

const FormComponent: FC<
  AdminCleanupFormProps & {
    onMapReady?: (params: AdminCleanupFormMapOnReadyParams) => void;
    onMandatoryLinearArea?: (value: any) => void;
  }
> = (props) => {
  const {
    initialData,
    isNew,
    onRecordReady,
    mandatoryLinearArea,
    setMandatoryLinearArea,
  } = props;

  const api = useApi();
  const sessionCtx = useSessionContext();
  const [create] = useCreate("cleanup_place");
  const record = useRecordContext<Cleanup>();
  const formContext = useFormContext();
  const raContext = useRAContext();

  const [mapApi, setMapApi] = useState<AdminCleanupFormMapApi>();
  const [isRecordReady, setIsRecordReady] = useState(false);
  const [mapDialogMode, setMapDialogMode] =
    useState<AdminCleanupFormMapDialogProps["mode"]>("hidden");
  const [environmentTypeId, setEnvironmentTypeId] =
    useState<typeof record.environment_type_id>();
  const [enivronmentAreaTypeId, setEnivronmentAreaTypeId] =
    useState<typeof record.area_type_id>();
  const [cityId, setCityId] = useState(record ? record.city_id : "");
  const [mapViewState, setMapViewState] = useState(MAP_INIT_POINT);
  const [hasInitAreas, setHasInitAreas] = useState(false);
  const [mainCampaign, setMainCampaign] = useState<Campaign>();
  const [isCleanupTypeTouched, setIsCleanupTypeTouched] = useState(!isNew);
  const [showWarningDefaultForm, setShowWarningDefaultForm] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  const { data: campaigns, isLoading: isLoadingCampaigns } =
    useGetList<Campaign>("campaign", {
      pagination: {
        page: 1,
        perPage: 1000000,
      },
      sort: {
        field: "label",
        order: "ASC",
      },
      filter: sessionCtx.getIsAdmin()
        ? undefined
        : {
            "campaign_users#user_view_id": sessionCtx.session?.userView.id,
          },
    });

  const totalArea = formContext.watch("total_area");
  const totalLinear = formContext.watch("total_linear");
  const startPoint = formContext.watch("start_point");
  const areas = formContext.watch("cleanup_areas");
  const cleanupTypeId = formContext.watch("cleanup_type_id");
  const cleanupCampaigns = formContext.watch("cleanup_campaigns");

  useEffect(() => {
    if (areas?.length && !hasInitAreas) {
      mapApi?.redrawAreas();
      setHasInitAreas(true);
    }
  }, [areas]);

  useEffect(() => {
    if (initialData) {
      formContext.reset(initialData);

      if (initialData.start_point) {
        setMapViewState({
          zoom: 10,
          latitude: initialData.start_point.lat,
          longitude: initialData.start_point.lng,
        });
      }
    }
  }, [initialData]);

  useEffect(() => {
    if (record && !isRecordReady) {
      setIsRecordReady(true);
      setEnvironmentTypeId(record.environment_type_id);
      if (record.cleanup_type_id) {
        setIsCleanupTypeTouched(true);
      }
      setMainCampaign(record.cleanup_campaigns?.[0]?.campaign);
      onRecordReady?.(record);
      if (record.start_point) {
        setMapViewState({
          zoom: 10,
          latitude: record.start_point.lat,
          longitude: record.start_point.lng,
        });
      }
    }
  }, [record, isRecordReady]);

  useEffect(() => {
    onMainCampaignIdChange(cleanupCampaigns?.[0]?.campaign_id);
  }, [cleanupCampaigns]);

  useEffect(() => {
    const default_cleanup_type_id = mainCampaign?.default_cleanup_type_id;

    if (!isCleanupTypeTouched) {
      formContext.setValue("cleanup_type_id", default_cleanup_type_id);
    }
  }, [mainCampaign]);

  useEffect(() => {
    const default_cleanup_type_id = mainCampaign?.default_cleanup_type_id;

    setShowWarningDefaultForm(
      Boolean(default_cleanup_type_id) &&
        Boolean(cleanupTypeId) &&
        default_cleanup_type_id !== cleanupTypeId
    );
  }, [mainCampaign, cleanupTypeId]);

  const onMapReady = (params: AdminCleanupFormMapOnReadyParams) => {
    setMapApi(params.api);
    props.onMapReady?.(params);
  };

  const onCampaignIdChange = (e: any) => {
    if (e.target.name !== "cleanup_campaigns.0.campaign_id") {
      return;
    }
    onMainCampaignIdChange(e.target.value);
  };

  const onMainCampaignIdChange = (id?: string) => {
    const campaign = campaigns?.find((campaign) => campaign.id == id);
    setMainCampaign(campaign);
  };

  const onCleanupTypeIdChange = () => {
    setIsCleanupTypeTouched(true);
  };

  useEffect(() => {
    if (record && record.area_type_id) {
      setEnivronmentAreaTypeId(record.area_type_id);
    }
  }, [record]);

  useEffect(() => {
    if (enivronmentAreaTypeId && environmentTypeId) {
      (async () => {
        const envAreaType = await api.postGraphql<{
          data: {
            environment_area_type: Array<{
              is_linear: boolean;
              is_area: boolean;
            }>;
          };
        }>({
          query: GET_LINEAR_AREA,
          variables: {
            where: {
              area_type_id: { _eq: enivronmentAreaTypeId },
              environment_type_id: { _eq: environmentTypeId },
            },
          },
        });
        if (envAreaType.data.data?.environment_area_type[0]) {
          setMandatoryLinearArea?.({
            isLinear: envAreaType.data.data?.environment_area_type[0].is_linear,
            isArea: envAreaType.data.data?.environment_area_type[0].is_area,
          });
        }
      })();
    }
  }, [enivronmentAreaTypeId]);

  return (
    <Stack
      sx={{
        pt: {
          xs: raContext.hasNoLayout ? "64px" : 0,
          md: 0,
        },
        pb: {
          xs: "64px",
        },
        px: isMobile ? 2 : 10,
      }}
    >
      <FormsHeader
        backUrl="/cleanups"
        title={isNew ? "Créer un ramassage" : "Éditer un ramassage"}
      />

      <Grid
        container
        spacing={4}
        sx={{
          pt: 1,
        }}
      >
        <Grid item xs={12} md={4}>
          <Stack>
            <Typography variant="h5" sx={{ textAlign: "center", mb: 2 }}>
              Informations
            </Typography>
            {/*<TextInput disabled label="Id" source="id" />  This field cause infite loop error !!! */}
            <TextInput
              source="label"
              label="Nom du ramassage"
              validate={required()}
            />
            <ArrayInput
              source="cleanup_campaigns"
              label="Entités wings"
              validate={required()}
            >
              <SimpleFormIterator inline>
                <SelectInput
                  source="campaign_id"
                  label="Sélectionner une entités wings"
                  choices={campaigns}
                  optionValue="id"
                  optionText="label"
                  validate={required()}
                  isLoading={isLoadingCampaigns}
                  onChange={onCampaignIdChange}
                />
              </SimpleFormIterator>
            </ArrayInput>
            {showWarningDefaultForm && (
              <Alert severity="warning">
                Le formulaire de caractérisation est différent du type par
                défaut de la mission principal :{" "}
                {mainCampaign?.default_cleanup_type?.label}
              </Alert>
            )}
            <ReferenceInput source="cleanup_type_id" reference="cleanup_type">
              <SelectInput
                label="Formulaire de caractérisation"
                optionText="label"
                validate={required()}
                onChange={onCleanupTypeIdChange}
              />
            </ReferenceInput>
            <ReferenceInput
              source="city_id"
              reference="city"
              sort={{ field: "label", order: "ASC" }}
            >
              <AutocompleteInput
                label="Commune"
                optionText={(choice) =>
                  `${choice.label} ${
                    choice.departement_id ? `(${choice.departement_id})` : ""
                  }`
                }
                optionValue="id"
                filterToQuery={(search) =>
                  search ? { "label@_ilike": search } : {}
                }
                validate={required()}
                onChange={(value) => {
                  setCityId(value);
                  formContext.setValue("cleanup_place_id", null);
                }}
              />
            </ReferenceInput>
            <ReferenceInput
              source="cleanup_place_id"
              reference="cleanup_place"
              sort={{ field: "label", order: "ASC" }}
              filter={cityId ? { city_id: cityId } : { id: "none" }}
            >
              <SelectInput
                validate={required()}
                createLabel={
                  cityId ? "Créer un lieu" : "Sélectionner une commune"
                }
                onCreate={async () => {
                  if (!cityId) {
                    return;
                  }
                  const newCleanupPlaceLabel = prompt(
                    "Saisir le nom du lieu à créer"
                  );
                  if (newCleanupPlaceLabel) {
                    const newCleanupPlace = await create(
                      "cleanup_place",
                      {
                        data: {
                          id: slugify(newCleanupPlaceLabel),
                          label: newCleanupPlaceLabel,
                          city_id: cityId,
                        },
                      },
                      { returnPromise: true }
                    );
                    return newCleanupPlace;
                  }
                }}
                source="cleanup_place"
                label="Lieu du ramassage"
                optionText="label"
              />
            </ReferenceInput>
            <ReferenceInput
              source="environment_type_id"
              reference="environment_type"
            >
              <SelectInput
                label="Type de zone"
                optionText="label"
                validate={required()}
                onChange={(event) => {
                  formContext.setValue("area_type_id", null);
                  setEnvironmentTypeId(event.target.value);
                }}
              />
            </ReferenceInput>
            <ReferenceInput
              source="area_type_id"
              reference="area_type"
              filter={{
                "environment_area_types#environment_type_id": environmentTypeId
                  ? environmentTypeId
                  : "",
              }}
            >
              <SelectInput
                label="Type de milieux"
                optionText="label"
                validate={required()}
                onChange={(event) => {
                  setEnivronmentAreaTypeId(event.target.value);
                }}
              />
            </ReferenceInput>
            <ArrayInput source="cleanup_partners" label="Partenaires">
              <SimpleFormIterator inline disableReordering>
                <ReferenceInput
                  source="partner_id"
                  reference="partner"
                  sort={{ field: "label", order: "ASC" }}
                >
                  <AutocompleteInput
                    label="Partenaire"
                    source="partner_id"
                    optionText="label"
                    optionValue="id"
                    filterToQuery={(search) =>
                      search ? { "label@_like": search } : {}
                    }
                  />
                </ReferenceInput>
              </SimpleFormIterator>
            </ArrayInput>
          </Stack>
        </Grid>
        <Grid item xs={12} md={4}>
          <Box>
            <Typography variant="h5" sx={{ textAlign: "center", mb: 2 }}>
              Planification
            </Typography>
            <Box>
              <DateTimeInput
                source="start_at"
                label="Date et heure de début"
                validate={required()}
                fullWidth
              />
              <Stack direction="row" spacing={1}>
                <Box
                  sx={{
                    width: "50%",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <Typography variant="h6">Point de départ</Typography>
                  <Button
                    variant="contained"
                    onClick={() => setMapDialogMode("startPoint")}
                    sx={{ mb: 1 }}
                  >
                    Éditer
                  </Button>
                  {Boolean(startPoint) ? (
                    <>
                      <Typography noWrap variant="body2">
                        Lat: {startPoint.lat}
                      </Typography>
                      <Typography noWrap variant="body2">
                        Lng: {startPoint.lng}
                      </Typography>
                    </>
                  ) : (
                    <Typography variant="body2" sx={{ color: "#14363F99" }}>
                      Sélectionner un point de départ
                    </Typography>
                  )}
                </Box>
                <Box
                  sx={{
                    width: "50%",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <Typography variant="h6">Zones du ramassage</Typography>
                  <Button
                    variant="contained"
                    onClick={() => setMapDialogMode("areas")}
                    sx={{ mb: 1 }}
                  >
                    Éditer
                  </Button>
                  {(!totalArea || !totalLinear) && (
                    <Typography variant="body2" sx={{ color: "#14363F99" }}>
                      Sélectionner une zone
                    </Typography>
                  )}
                </Box>
              </Stack>
              {enivronmentAreaTypeId && (
                <Alert severity="info">
                  Pour ce type de lieu :
                  {mandatoryLinearArea?.isLinear && mandatoryLinearArea?.isArea
                    ? " Linéaire obligatoire et Surface obligatoire"
                    : `${
                        mandatoryLinearArea?.isLinear
                          ? " Linéaire obligatoire"
                          : ""
                      } ${
                        mandatoryLinearArea?.isArea
                          ? " Surface obligatoire"
                          : ""
                      }`}
                </Alert>
              )}
            </Box>
            <Box
              sx={{
                mt: 1,
                height: {
                  xs: "300px",
                  md: "calc(100vh - 430px)",
                },
              }}
            >
              <AdminCleanupFormMap
                viewState={mapViewState}
                onMove={(e) => setMapViewState(e.viewState)}
                onReady={onMapReady}
                startPoint={startPoint}
              />
            </Box>
            <AdminCleanupFormMapDialog
              mode={mapDialogMode}
              onClose={() => {
                if (mapDialogMode == "areas") {
                  mapApi?.redrawAreas();
                }
                setMapDialogMode("hidden");
                if (startPoint) {
                  setMapViewState({
                    zoom: 10,
                    latitude: startPoint.lat,
                    longitude: startPoint.lng,
                  });
                }
              }}
            ></AdminCleanupFormMapDialog>
            <Box sx={{ display: "flex", flexDirection: "column" }}>
              <Typography>
                Données surfaciques totales : {totalArea ? totalArea : "0"} m²
              </Typography>
              <Typography>
                Données linéaires totales : {totalLinear ? totalLinear : "0"} m
              </Typography>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} md={4}>
          <Box>
            <Typography variant="h5" sx={{ textAlign: "center", mb: 2 }}>
              Photos
            </Typography>
            <ArrayInput source="cleanup_photos" label="">
              <SimpleFormIterator
                fullWidth
                sx={{
                  ".RaSimpleFormIterator-line": {
                    py: 1,
                  },
                }}
                addButton={
                  <Button size="small" variant="contained">
                    Ajouter une photo
                  </Button>
                }
              >
                <FormDataConsumer<Cleanup_Photo>>
                  {({ scopedFormData, getSource }) => (
                    <Stack direction={{ xs: "column", lg: "row" }} spacing={2}>
                      <Box sx={{ width: 200 }}>
                        {scopedFormData?.filename ? (
                          <Avatar
                            sx={{ width: "100%", height: "auto" }}
                            variant="square"
                            src={getTransformationURL(
                              "cleanups",
                              record.id,
                              scopedFormData.filename,
                              IMAGOR_PRESET_NAME["200x200"]
                            )}
                          />
                        ) : (
                          <ImageInput
                            label={false}
                            placeholder={"Sélectionner une photo"}
                            helperText={false}
                            source={getSource("file")}
                            sx={{
                              width: "100%",
                              height: "auto",
                              "& .RaFileInput-dropZone": {
                                backgroundColor: "#3EA0EA",
                                color: "white",
                                "&:hover": {
                                  backgroundColor: "#2C8DCD",
                                },
                              },
                            }}
                            multiple={false}
                          >
                            <ImageField
                              source="src"
                              sx={{ width: "100%", height: "auto" }}
                            />
                          </ImageInput>
                        )}
                      </Box>
                      <Box sx={{ flex: 1 }}>
                        <Typography>{scopedFormData?.filename}</Typography>
                        <TextInput
                          source={getSource("comment")}
                          label="Commentaire"
                          multiline
                          fullWidth
                        />
                        {record && (
                          <ReferenceInput
                            source={getSource("cleanup_area_id")}
                            reference="cleanup_area"
                            filter={{
                              cleanup_id: record.id,
                            }}
                          >
                            <SelectInput optionText="label" fullWidth />
                          </ReferenceInput>
                        )}
                      </Box>
                    </Stack>
                  )}
                </FormDataConsumer>
              </SimpleFormIterator>
            </ArrayInput>
            <Typography variant="h5" sx={{ textAlign: "center", mb: 2 }}>
              Description
            </Typography>
            <TextInput
              source="description"
              label="Description du ramassage"
              multiline
              sx={{ width: "100%" }}
            />
          </Box>
        </Grid>
      </Grid>
      <SaveButton
        alwaysEnable={true}
        label="Enregistrer"
        size="large"
        sx={{
          position: "fixed",
          bottom: {
            xs: 10,
            md: 10,
          },
          left: "50%",
          transform: "translateX(-50%)",
          zIndex: 2,
        }}
      />
    </Stack>
  );
};

export const AdminCleanupForm: FC<AdminCleanupFormProps> = (props) => {
  const notify = useNotify();
  const [mandatoryLinearArea, setMandatoryLinearArea] = useState<any>({
    isLinear: null,
    isArea: null,
  });

  const handleSubmit = (data: AdminCleanupFormData) => {
    let errorMessages: string[] = [];

    if (
      mandatoryLinearArea.isLinear &&
      (data.total_linear === 0 ||
        data.total_linear === null ||
        data.total_linear === undefined)
    ) {
      errorMessages.push("Définir au moins un linéaire pour ce type de lieu");
    }

    if (
      mandatoryLinearArea.isArea &&
      (data.total_area === 0 ||
        data.total_area === null ||
        data.total_area === undefined)
    ) {
      errorMessages.push("Définir au moins une surface pour ce type de lieu");
    }

    if (errorMessages.length > 0) {
      let errorMessage =
        `Veuillez corriger ${
          errorMessages.length === 1
            ? "l'erreur suivante"
            : "les erreurs suivantes"
        } : \n\n` + errorMessages.join("\n");
      notify(errorMessage, { multiLine: true, type: "error" });
      return;
    }
    if (!data.start_point) {
      notify("Sélectionner un point de depart", {
        type: "error",
      });
      return;
    }
    props.onSubmit(data);
  };

  return (
    <Form onSubmit={handleSubmit as any}>
      <FormComponent
        {...props}
        mandatoryLinearArea={mandatoryLinearArea}
        setMandatoryLinearArea={setMandatoryLinearArea}
      />
    </Form>
  );
};
