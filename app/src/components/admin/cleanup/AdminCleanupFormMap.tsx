import { FC, useEffect, useRef, useState } from "react";
import {
  LngLat,
  Map,
  MapLayerMouseEvent,
  Marker,
  MarkerDragEvent,
  ViewState,
  ViewStateChangeEvent,
} from "react-map-gl/maplibre";
import { useFormContext } from "react-hook-form";
import { MapLibreEvent } from "maplibre-gl";
import { Cleanup_Area_Insert_Input } from "@/graphql/types";
import {
  CLEANUP_AREA_COLORS,
  CleanupAreaColorName,
  MAP_LAYERS,
  MAP_STYLE,
  MapLayerName,
} from "@/utils/map";
import DrawControl, { DrawControlEvent } from "@/components/map/DrawControl";
import { NavigationControl } from "react-map-gl";
import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import { cloneDeep } from "lodash";

export type AdminCleanupFormMapOnReadyParams = { api: AdminCleanupFormMapApi };

export type AdminCleanupFormMapApi = {
  getDrawnFeatures: () => ReturnType<MapboxDraw["getAll"]> | undefined;
  getDraw: () => MapboxDraw | null;
  redrawAreas: () => void;
  onAreaColorChange: () => void;
};

type AdminCleanupFormMapProps = {
  isEditable?: boolean;
  onReady?: (params: AdminCleanupFormMapOnReadyParams) => void;
  onLoad?: (e: MapLibreEvent) => void;
  onClick?: (e: MapLayerMouseEvent) => void;
  onDrawSelect?: (e: DrawControlEvent) => void;
  onDrawCreate?: (e: DrawControlEvent) => void;
  onDrawUpdate?: (e: DrawControlEvent) => void;
  onDrawDelete?: (e: DrawControlEvent) => void;
  onMove: (e: ViewStateChangeEvent) => void;
  onStartPointDrag?: (e: MarkerDragEvent) => void;
  startPoint?: LngLat;
  viewState: Partial<ViewState>;
};

const paint: any[] = ["case"];
for (const key in CLEANUP_AREA_COLORS) {
  const color = CLEANUP_AREA_COLORS[key as CleanupAreaColorName];
  paint.push(["==", ["get", "user_fill_color"], key], color);
}
paint.push("#fbb03b");

export const AdminCleanupFormMap: FC<AdminCleanupFormMapProps> = ({
  isEditable,
  onReady,
  onLoad,
  onClick,
  onDrawSelect,
  onDrawCreate,
  onDrawUpdate,
  onDrawDelete,
  onMove,
  onStartPointDrag,
  startPoint,
  viewState,
}) => {
  const drawRef = useRef<MapboxDraw>(null);
  const formContext = useFormContext();

  const [mapStyle, setMapStyle] = useState<any>({});
  const [baseMap, setBaseMap] = useState<MapLayerName>("plan");

  useEffect(() => {
    onReady?.({
      api: {
        getDrawnFeatures: () => {
          return drawRef.current?.getAll();
        },
        getDraw: () => drawRef.current,
        redrawAreas: () => {
          redrawAreas();
        },
        onAreaColorChange: () => {
          onAreaColorChange();
        },
      },
    });
  }, []);

  const getFormAreas = () => {
    return (formContext.getValues("cleanup_areas") ||
      []) as Cleanup_Area_Insert_Input[];
  };

  const onAreaColorChange = () => {
    const draw = drawRef.current;
    if (!draw) {
      return;
    }
    getFormAreas().forEach((area) => {
      draw.setFeatureProperty(area.id, "fill_color", area.fill_color);
      const feature = draw.get(area.id);
      draw.add(feature!);
    });
  };

  const redrawAreas = () => {
    const draw = drawRef.current;
    if (!draw) {
      return;
    }
    draw.deleteAll();
    getFormAreas()
      .filter((area) => area.geom)
      .map((area) =>
        draw.add({
          ...area.geom,
          id: area.id,
          properties: {
            fill_color: area.fill_color,
          },
        })
      );
  };

  const handleBaseMap = (value: MapLayerName) => {
    setBaseMap(value);
  };

  useEffect(() => {
    const newMapStyle = cloneDeep(MAP_STYLE);
    newMapStyle.sources.bg_src = MAP_LAYERS[baseMap];
    setMapStyle(newMapStyle);
  }, [baseMap]);

  return (
    <Map
      {...viewState}
      onLoad={(e) => {
        redrawAreas();
        onLoad?.(e);
      }}
      onClick={onClick}
      onMove={onMove}
      style={{ width: "100%", height: "100%" }}
      mapStyle={mapStyle}
    >
      <NavigationControl showCompass={false} />
      {startPoint && (
        <Marker
          draggable={Boolean(onStartPointDrag)}
          latitude={startPoint.lat}
          longitude={startPoint.lng}
          onDrag={onStartPointDrag}
        ></Marker>
      )}
      <ToggleButtonGroup
        size="small"
        color="primary"
        value={baseMap}
        exclusive
        onChange={(e, value) => {
          handleBaseMap(value);
        }}
        sx={{
          position: "absolute",
          background: "white",
          bottom: 8,
          left: 8,
        }}
      >
        <ToggleButton value="plan">Plan</ToggleButton>
        <ToggleButton value="sat">Satellite</ToggleButton>
      </ToggleButtonGroup>
      <DrawControl
        ref={drawRef}
        position="top-left"
        displayControlsDefault={false}
        controls={
          !isEditable
            ? {}
            : {
                polygon: true,
                line_string: true,
                trash: true,
              }
        }
        onSelect={onDrawSelect}
        onCreate={onDrawCreate}
        onUpdate={onDrawUpdate}
        onDelete={onDrawDelete}
        userProperties={true}
        styles={[
          {
            id: "gl-draw-polygon-fill-inactive",
            type: "fill",
            filter: [
              "all",
              ["==", "active", "false"],
              ["==", "$type", "Polygon"],
              ["!=", "mode", "static"],
            ],
            paint: {
              "fill-color": paint,
              "fill-outline-color": paint,
              "fill-opacity": 0.1,
            },
          },
          {
            id: "gl-draw-polygon-fill-active",
            type: "fill",
            filter: [
              "all",
              ["==", "active", "true"],
              ["==", "$type", "Polygon"],
            ],
            paint: {
              "fill-color": paint,
              "fill-outline-color": paint,
              "fill-opacity": 0.1,
            },
          },
          {
            id: "gl-draw-polygon-midpoint",
            type: "circle",
            filter: [
              "all",
              ["==", "$type", "Point"],
              ["==", "meta", "midpoint"],
            ],
            paint: {
              "circle-radius": 3,
              "circle-color": "#000",
              "circle-stroke-width": 2,
              "circle-stroke-color": "#FFF",
            },
          },
          {
            id: "gl-draw-polygon-stroke-inactive",
            type: "line",
            filter: [
              "all",
              ["==", "active", "false"],
              ["==", "$type", "Polygon"],
              ["!=", "mode", "static"],
            ],
            layout: {
              "line-cap": "round",
              "line-join": "round",
            },
            paint: {
              "line-color": paint,
              "line-width": 2,
            },
          },
          {
            id: "gl-draw-polygon-stroke-active",
            type: "line",
            filter: [
              "all",
              ["==", "active", "true"],
              ["==", "$type", "Polygon"],
            ],
            layout: {
              "line-cap": "round",
              "line-join": "round",
            },
            paint: {
              "line-color": paint,
              "line-dasharray": [0.2, 2],
              "line-width": 2,
            },
          },
          {
            id: "gl-draw-line-inactive",
            type: "line",
            filter: [
              "all",
              ["==", "active", "false"],
              ["==", "$type", "LineString"],
              ["!=", "mode", "static"],
            ],
            layout: {
              "line-cap": "round",
              "line-join": "round",
            },
            paint: {
              "line-color": paint,
              "line-width": 2,
            },
          },
          {
            id: "gl-draw-line-active",
            type: "line",
            filter: [
              "all",
              ["==", "$type", "LineString"],
              ["==", "active", "true"],
            ],
            layout: {
              "line-cap": "round",
              "line-join": "round",
            },
            paint: {
              "line-color": paint,
              "line-dasharray": [0.2, 2],
              "line-width": 2,
            },
          },
          {
            id: "gl-draw-polygon-and-line-vertex-stroke-inactive",
            type: "circle",
            filter: [
              "all",
              ["==", "meta", "vertex"],
              ["==", "$type", "Point"],
              ["!=", "mode", "static"],
            ],
            paint: {
              "circle-radius": 6,
              "circle-color": "#fff",
            },
          },
          {
            id: "gl-draw-polygon-and-line-vertex-inactive",
            type: "circle",
            filter: [
              "all",
              ["==", "meta", "vertex"],
              ["==", "$type", "Point"],
              ["!=", "mode", "static"],
            ],
            paint: {
              "circle-radius": 4,
              "circle-color": "#fbb03b",
            },
          },
          {
            id: "gl-draw-point-point-stroke-inactive",
            type: "circle",
            filter: [
              "all",
              ["==", "active", "false"],
              ["==", "$type", "Point"],
              ["==", "meta", "feature"],
              ["!=", "mode", "static"],
            ],
            paint: {
              "circle-radius": 5,
              "circle-opacity": 1,
              "circle-color": "#fff",
            },
          },
          {
            id: "gl-draw-point-inactive",
            type: "circle",
            filter: [
              "all",
              ["==", "active", "false"],
              ["==", "$type", "Point"],
              ["==", "meta", "feature"],
              ["!=", "mode", "static"],
            ],
            paint: {
              "circle-radius": 3,
              "circle-color": "#3bb2d0",
            },
          },
          {
            id: "gl-draw-point-stroke-active",
            type: "circle",
            filter: [
              "all",
              ["==", "$type", "Point"],
              ["==", "active", "true"],
              ["!=", "meta", "midpoint"],
            ],
            paint: {
              "circle-radius": 7,
              "circle-color": "#fff",
            },
          },
          {
            id: "gl-draw-point-active",
            type: "circle",
            filter: [
              "all",
              ["==", "$type", "Point"],
              ["!=", "meta", "midpoint"],
              ["==", "active", "true"],
            ],
            paint: {
              "circle-radius": 5,
              "circle-color": "#fbb03b",
            },
          },
          {
            id: "gl-draw-polygon-fill-static",
            type: "fill",
            filter: [
              "all",
              ["==", "mode", "static"],
              ["==", "$type", "Polygon"],
            ],
            paint: {
              "fill-color": "#404040",
              "fill-outline-color": "#404040",
              "fill-opacity": 0.1,
            },
          },
          {
            id: "gl-draw-polygon-stroke-static",
            type: "line",
            filter: [
              "all",
              ["==", "mode", "static"],
              ["==", "$type", "Polygon"],
            ],
            layout: {
              "line-cap": "round",
              "line-join": "round",
            },
            paint: {
              "line-color": "#404040",
              "line-width": 2,
            },
          },
          {
            id: "gl-draw-line-static",
            type: "line",
            filter: [
              "all",
              ["==", "mode", "static"],
              ["==", "$type", "LineString"],
            ],
            layout: {
              "line-cap": "round",
              "line-join": "round",
            },
            paint: {
              "line-color": "#404040",
              "line-width": 2,
            },
          },
          {
            id: "gl-draw-point-static",
            type: "circle",
            filter: ["all", ["==", "mode", "static"], ["==", "$type", "Point"]],
            paint: {
              "circle-radius": 5,
              "circle-color": "#404040",
            },
          },
        ]}
      />
    </Map>
  );
};
