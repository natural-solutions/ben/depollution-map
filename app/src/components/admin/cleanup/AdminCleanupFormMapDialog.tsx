import {
  Box,
  Button,
  Dialog,
  IconButton,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import React, { FC, useEffect, useState } from "react";
import {
  AdminCleanupFormMap,
  AdminCleanupFormMapApi,
  AdminCleanupFormMapOnReadyParams,
} from "./AdminCleanupFormMap";
import { Cleanup_Area, Cleanup_Area_Insert_Input } from "@/graphql/types";
import { DrawControlEvent } from "@/components/map/DrawControl";
import {
  ArrayInput,
  FormDataConsumer,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  useNotify,
} from "react-admin";
import { useFormContext } from "react-hook-form";
import {
  CLEANUP_AREA_COLORS,
  CLEANUP_AREA_COLORS_TRANSLATIONS,
  MAP_INIT_POINT,
  computeTotalAreaLinear,
  getGeomMeasurement,
} from "@/utils/map";
import { MapLayerMouseEvent } from "maplibre-gl";
import { MarkerDragEvent } from "react-map-gl/maplibre";
import { CheckCircle, Close } from "@mui/icons-material";
import { printNumber } from "@/utils/cleanups";

export type AdminCleanupFormMapDialogProps = {
  mode: "hidden" | "startPoint" | "areas";
  onClose: () => void;
};

const PAPER_BG_COLOR = "#FFFFFFCC";

export const AdminCleanupFormMapDialog: FC<AdminCleanupFormMapDialogProps> = (
  props
) => {
  const { mode, onClose } = props;
  const formContext = useFormContext();
  const notify = useNotify();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const [mapViewState, setMapViewState] = useState(MAP_INIT_POINT);
  const [showAreaParams, setShowAreaParams] = useState(false);
  const [mapApi, setMapApi] = useState<AdminCleanupFormMapApi>();

  const totalArea = formContext.watch("total_area");
  const totalLinear = formContext.watch("total_linear");
  const startPoint = formContext.watch("start_point");

  useEffect(() => {
    if (mode === "hidden") {
      return;
    }
    setShowAreaParams(mode === "areas" && !isMobile);
    if (startPoint) {
      setMapViewState({
        zoom: 16,
        latitude: startPoint.lat,
        longitude: startPoint.lng,
      });
    }
  }, [mode]);

  const getGeoms = () => {
    return (formContext.getValues("cleanup_areas") ||
      []) as Cleanup_Area_Insert_Input[];
  };

  const onDrawCreate = (e: DrawControlEvent) => {
    const feature = e.features?.[0];
    const id = feature?.id;
    if (!id) {
      return;
    }
    const geoms = getGeoms();
    const geom = geoms?.find((geom) => geom.id === id);
    if (!geom) {
      geoms.push({
        id,
        geom: { ...feature },
        size: getGeomMeasurement(feature),
      });
      formContext.setValue("cleanup_areas", [...geoms]);
      computeGeoms(geoms);
    }
  };

  const computeGeoms = (areas: any) => {
    const { totalArea: area, totalLinear: line } =
      computeTotalAreaLinear(areas);
    formContext.setValue("total_area", area);
    formContext.setValue("total_linear", line);
  };

  const onDrawDelete = (e: DrawControlEvent) => {
    const features = mapApi?.getDrawnFeatures();
    const geoms = getGeoms().filter((area) => {
      return features?.features.some((feat) => feat.id === area.id);
    });

    formContext.setValue("cleanup_areas", geoms);
    computeGeoms(geoms);
  };

  const onDrawUpdate = (e: DrawControlEvent) => {
    const feature = e.features?.[0];
    const id = feature?.id;
    if (!id) {
      return;
    }
    const geoms = getGeoms();
    const geom = geoms?.find((geom) => geom.id === id);
    if (!geom) {
      return;
    }
    geom.size = getGeomMeasurement(feature);
    geom.geom = { ...feature };
    formContext.setValue("cleanup_areas", [...geoms]);
    computeGeoms(geoms);
  };

  const onMapReady = (params: AdminCleanupFormMapOnReadyParams) => {
    setMapApi(params.api);
  };

  const onMapClick = (e: MapLayerMouseEvent) => {
    if (mode === "startPoint") {
      formContext.setValue("start_point", e.lngLat);
    }
  };

  const onStartPointDrag = (e: MarkerDragEvent) => {
    formContext.setValue("start_point", e.lngLat);
  };

  const onAreaColorChange = () => {
    mapApi?.onAreaColorChange();
  };

  const handleValidateClick = () => {
    const geoms = getGeoms();

    for (const geom of geoms) {
      if (!geom.label) {
        notify("Veuillez donner un nom à chaque zone", {
          type: "error",
        });
        return;
      } else if (!geom.fill_color) {
        notify("Veuillez choisir une couleur pour chaque zone", {
          type: "error",
        });
        return;
      }
    }
    onClose();
  };

  return (
    <Dialog fullScreen open={mode !== "hidden"} onClose={onClose}>
      <Button
        sx={{
          position: "absolute",
          zIndex: 1,
          bottom: {
            xs: 35,
            sm: "auto",
          },
          top: {
            xs: "auto",
            sm: 10,
          },
          left: "50%",
          transform: "translateX(-50%)",
        }}
        variant="contained"
        size="large"
        startIcon={<CheckCircle />}
        onClick={handleValidateClick}
      >
        Valider
      </Button>
      {mode === "areas" && !showAreaParams && (
        <Button
          variant="contained"
          color="info"
          sx={{
            position: "absolute",
            top: 10,
            left: 50,
            zIndex: 1,
          }}
          onClick={() => setShowAreaParams(true)}
        >
          Paramètres des zones
        </Button>
      )}
      {showAreaParams && (
        <Paper
          sx={{
            position: "absolute",
            top: { xs: 5, sm: 10 },
            left: { xs: 5, sm: 50 },
            zIndex: 3,
            p: 1,
            maxHeight: { xs: "calc(100% - 10px)", sm: "none" },
            display: "flex",
            flexDirection: "column",
          }}
        >
          {!Boolean(getGeoms().length) ? (
            <Typography>
              Utiliser ces outils pour créer
              <br />
              une zone ou une ligne
            </Typography>
          ) : (
            <>
              <Stack
                direction="row"
                sx={{
                  justifyContent: "space-between",
                }}
              >
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  {totalArea !== 0 && (
                    <Typography>
                      Données surfaciques totales :{" "}
                      {printNumber(totalArea)} m²
                    </Typography>
                  )}
                  {totalLinear !== 0 && (
                    <Typography>
                      Données linéaires totales :{" "}
                      {printNumber(totalLinear)} m
                    </Typography>
                  )}
                </Box>
                <IconButton
                  color="error"
                  onClick={() => setShowAreaParams(false)}
                >
                  <Close />
                </IconButton>
              </Stack>
              <Box sx={{ flex: 1, overflow: "auto" }}>
                <ArrayInput sx={{ zIndex: 1 }} source="cleanup_areas" label="">
                  <SimpleFormIterator
                    inline
                    disableAdd
                    disableClear
                    disableRemove
                    disableReordering
                  >
                    <FormDataConsumer<Cleanup_Area>>
                      {({ scopedFormData }) => (
                        <Box
                          sx={{
                            pt: 1,
                            display: {
                              sm: "none",
                            },
                          }}
                        >
                          <Typography variant="body2">
                            Aire {scopedFormData?.size} m<sup>2</sup>
                          </Typography>
                        </Box>
                      )}
                    </FormDataConsumer>
                    <TextInput source="label" label="Nom de la zone" />
                    <SelectInput
                      source="fill_color"
                      label="Couleur"
                      choices={Object.keys(CLEANUP_AREA_COLORS).map(
                        (colorName) => ({
                          id: colorName,
                          name:
                            CLEANUP_AREA_COLORS_TRANSLATIONS[
                              colorName as keyof typeof CLEANUP_AREA_COLORS_TRANSLATIONS
                            ] || colorName,
                        })
                      )}
                      onChange={onAreaColorChange}
                    />
                    <FormDataConsumer<Cleanup_Area>>
                      {({ scopedFormData }) => (
                        <Box
                          sx={{
                            pt: 1,
                            display: {
                              xs: "none",
                              sm: null,
                            },
                          }}
                        >
                          <Typography variant="caption">Aire</Typography>
                          <Typography>
                            {scopedFormData?.size} m<sup>2</sup>
                          </Typography>
                        </Box>
                      )}
                    </FormDataConsumer>
                  </SimpleFormIterator>
                </ArrayInput>
              </Box>
            </>
          )}
        </Paper>
      )}
      {mode == "startPoint" && (
        <Paper
          sx={{
            position: "absolute",
            top: 10,
            left: 10,
            zIndex: 1,
            backgroundColor: PAPER_BG_COLOR,
            p: 3,
          }}
        >
          {!Boolean(startPoint) && (
            <Typography variant="h6">
              Cliquer sur la carte pour créer un point de départ.
            </Typography>
          )}
          {Boolean(startPoint) && (
            <>
              <Typography variant="h6">Point de départ</Typography>
              <Typography>
                <b>Déplacer le point de départ pour le modifier</b>
              </Typography>
              <Typography>Lat: {startPoint.lat}</Typography>
              <Typography>Lng: {startPoint.lng}</Typography>
              <Button
                color="error"
                variant="contained"
                onClick={() => {
                  formContext.setValue("start_point", null);
                }}
              >
                Supprimer le point de départ
              </Button>
            </>
          )}
        </Paper>
      )}
      <AdminCleanupFormMap
        isEditable={mode === "areas"}
        viewState={mapViewState}
        onMove={(e) => setMapViewState(e.viewState)}
        onReady={onMapReady}
        onClick={onMapClick}
        onDrawCreate={onDrawCreate}
        onDrawUpdate={onDrawUpdate}
        onDrawDelete={onDrawDelete}
        onStartPointDrag={mode === "startPoint" ? onStartPointDrag : undefined}
        startPoint={startPoint}
      />
    </Dialog>
  );
};
