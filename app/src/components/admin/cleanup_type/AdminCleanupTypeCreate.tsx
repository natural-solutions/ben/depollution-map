import { GET_CLEANUP_TYPE, INSERT_CLEANUP_TYPE } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { CreateBase, Form } from "react-admin";
import { useNavigate, useSearchParams } from "react-router-dom";
import { pick } from "lodash";
import { AdminCleanupTypeFields } from "./AdminCleanupTypeFields";
import { Cleanup_Type } from "@/graphql/types";
import { useEffect, useState } from "react";

export const AdminCleanupTypeCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();
  const [params] = useSearchParams();
  const copy_id = params.get("copy_id");

  const [initialData, setInitialData] = useState<Cleanup_Type>();

  useEffect(() => {
    if (copy_id) {
      (async () => {
        const resp = await postGraphql<{
          data: {
            cleanup_type_by_pk: Cleanup_Type;
          };
        }>({
          query: GET_CLEANUP_TYPE,
          variables: { id: copy_id },
        });

        setInitialData(resp.data.data.cleanup_type_by_pk);
      })();
    }
  }, []);

  const onSubmit = async (formData: Cleanup_Type) => {
    try {
      const cleanup_type_rubbishes = formData?.cleanup_type_rubbishes
        ?.filter((row) => row.has_qty || row.has_wt_vol)
        .map((row) => pick(row, ["rubbish_id", "has_qty", "has_wt_vol"]));

      const response = await postGraphql<any>({
        query: INSERT_CLEANUP_TYPE,
        variables: {
          _set: {
            ...pick(formData, ["id", "label", "characterization_level_id"]),
            cleanup_type_rubbishes: Boolean(cleanup_type_rubbishes?.length) && {
              data: cleanup_type_rubbishes,
            },
          },
        },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      navigate("/cleanup_type");
    } catch (error) {}
  };

  return (
    <CreateBase>
      <Form onSubmit={onSubmit as any}>
        <AdminCleanupTypeFields initialData={initialData} />
      </Form>
    </CreateBase>
  );
};
