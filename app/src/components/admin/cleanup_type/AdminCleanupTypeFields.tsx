import { Cleanup_Type } from "@/graphql/types";
import { Box, Stack, Typography } from "@mui/material";
import React, { FC, useEffect } from "react";
import {
  ArrayInput,
  AutocompleteInput,
  BooleanInput,
  ReferenceInput,
  SaveButton,
  SelectInput,
  SimpleFormIterator,
  TextInput,
  Toolbar,
  required,
  useRecordContext,
} from "react-admin";
import { useFormContext } from "react-hook-form";

type AdminCleanupTypeFieldsProps = {
  initialData?: Cleanup_Type;
};

export const AdminCleanupTypeFields: FC<AdminCleanupTypeFieldsProps> = ({
  initialData,
}) => {
  const record = useRecordContext<Cleanup_Type>();
  const formContext = useFormContext();

  useEffect(() => {
    if (initialData) {
      delete (initialData as any).id;
      delete initialData.created_at;
      delete initialData.updated_at;

      formContext.reset(initialData);
    }
  }, [initialData]);

  return (
    <Stack p={2}>
      <Stack alignItems={"start"}>
        {record?.id ? (
          <Typography>ID: {record.id}</Typography>
        ) : (
          <TextInput
            source="id"
            validate={required()}
            parse={(value) => value.trim()}
          />
        )}
        <ReferenceInput
          source="characterization_level_id"
          reference="characterization_level"
          sort={{ field: "label", order: "ASC" }}
        >
          <SelectInput
            label="Niveau de caractérisation"
            source="characterization_level"
            optionText="label"
          />
        </ReferenceInput>
        <TextInput
          source="label"
          label="Nom du formulaire"
          validate={required()}
        />

        <ArrayInput source="cleanup_type_rubbishes" label="Déchets">
          <SimpleFormIterator inline disableClear disableReordering>
            <ReferenceInput
              source="rubbish_id"
              reference="rubbish"
              sort={{ field: "label", order: "ASC" }}
            >
              <AutocompleteInput
                label="Déchet"
                optionText="label"
                optionValue="id"
                validate={required()}
                filterToQuery={(search) =>
                  search ? { "label@_ilike": search } : {}
                }
                sx={{
                  width: "450px",
                }}
              />
            </ReferenceInput>
            <BooleanInput source="has_qty" label="Quantité" />
            <BooleanInput source="has_wt_vol" label="Volume" />
          </SimpleFormIterator>
        </ArrayInput>
      </Stack>
      <Box>
        <Toolbar>
          <SaveButton alwaysEnable={true} />
        </Toolbar>
      </Box>
    </Stack>
  );
};
