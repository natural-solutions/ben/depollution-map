import { useApi } from "@/utils/useApi";
import { EditBase, Form } from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { AdminCleanupTypeFields } from "./AdminCleanupTypeFields";
import { Cleanup_Type } from "@/graphql/types";

export const AdminCleanupTypeEdit = () => {
  const { id } = useParams();
  const { api } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (formData: Cleanup_Type) => {
    if (!id) {
      return;
    }

    try {
      await api.patch(`/cleanup_type/${id}`, {
        ...pick(formData, ["id", "label", "characterization_level_id"]),
        cleanup_type_rubbishes: formData?.cleanup_type_rubbishes
          ?.filter((row) => row.has_qty || row.has_wt_vol)
          .map((row) => pick(row, ["rubbish_id", "has_qty", "has_wt_vol"])),
      });

      navigate("/cleanup_type");
    } catch (error) {}
  };

  return (
    <EditBase>
      <Form onSubmit={onSubmit as any}>
        <AdminCleanupTypeFields />
      </Form>
    </EditBase>
  );
};
