import { Cleanup_Type } from "@/graphql/types";
import { useSessionContext } from "@/utils/useSessionContext";
import { ContentCopy } from "@mui/icons-material";
import { Button, Stack } from "@mui/material";
import React, { FC } from "react";
import {
  Datagrid,
  EditButton,
  Filter,
  FunctionField,
  List,
  ReferenceInput,
  SelectInput,
  TextField,
  TextInput,
} from "react-admin";
import { useNavigate } from "react-router-dom";

const Filters = (props: any) => (
  <Filter {...props}>
    <ReferenceInput
      label="Niveau de caractérisation"
      source="characterization_level_id"
      reference="characterization_level"
      sort={{ field: "label", order: "ASC" }}
    >
      <SelectInput
        label="Niveau de caractérisation"
        source="characterization_level"
        optionText="label"
      />
    </ReferenceInput>
    <TextInput label="Nom du formulaire" source="label" />
  </Filter>
);

type AdminCleanupTypeListProps = {};

export const AdminCleanupTypeList: FC<AdminCleanupTypeListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();
  const navigate = useNavigate();

  const duplicate = (record: Cleanup_Type) => {
    navigate(`/cleanup_type/create?copy_id=${record.id}`);
  };

  return (
    <List filters={<Filters />}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField
          source="characterization_level.label"
          label="Niveau de carc."
        />
        <TextField source="label" label="Nom du formulaire" />
        <FunctionField
          render={(record: Cleanup_Type) => (
            <Stack direction="row" spacing={3} sx={{ justifyContent: "end" }}>
              {isAuthorized && (
                <>
                  <EditButton />
                  <Button
                    size="small"
                    startIcon={<ContentCopy />}
                    onClick={() => duplicate(record)}
                  >
                    Dupliquer
                  </Button>
                </>
              )}
            </Stack>
          )}
        />
      </Datagrid>
    </List>
  );
};
