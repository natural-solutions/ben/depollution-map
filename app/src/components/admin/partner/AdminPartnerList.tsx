import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  Datagrid,
  DateField,
  DateInput,
  EditButton,
  List,
  TextField,
  TextInput,
} from "react-admin";

const filters = [
  <TextInput key="a" label="Partenaire" source="label" />,
  <DateInput key="e" label="Date de création =" source="created_at" />,
  <DateInput key="f" label="Date de modification =" source="updated_at" />,
];

type AdminPartnerListProps = {};

export const AdminPartnerList: FC<AdminPartnerListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Partenaire" />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
