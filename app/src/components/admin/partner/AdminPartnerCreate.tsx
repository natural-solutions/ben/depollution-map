import { INSERT_PARTNER } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { Create } from "react-admin";
import { useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { slugify, uploadFile } from "@/utils/media";
import { AdminBasicForm } from "../shared/AdminBasicForm";

export const AdminPartnerCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    const file = _set.selectLogo?.rawFile;
    if (file) {
      file.slugified = slugify(file.name);
      _set.logo = file.slugified;
      delete _set.selectLogo;
    }

    try {
      const response = await postGraphql<any>({
        query: INSERT_PARTNER,
        variables: { _set: pick(_set, ["id", "label", "logo"]) },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      const id = response.data?.data?.insert_partner_one?.id;

      if (id && file) {
        try {
          await uploadFile("partners", id, file);
        } catch (error) {}
      }

      navigate("/partner");
    } catch (error) {}
  };

  return (
    <Create>
      <AdminBasicForm onSubmit={onSubmit} resourceName="partners" />
    </Create>
  );
};
