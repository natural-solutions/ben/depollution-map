import {
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
  useRecordContext,
} from "react-admin";
import { Rubbish_Category } from "@/graphql/types";
import { FC, useEffect, useState } from "react";
import { Typography } from "@mui/material";

type AdminRubbishCategoryFormProps = {
  onSubmit: (data: Rubbish_Category) => void;
  onRecordReady?: (record: Rubbish_Category) => void;
};

const FormComponent: FC<AdminRubbishCategoryFormProps> = (props) => {
  const [isRecordReady, setIsRecordReady] = useState(false);
  const record = useRecordContext<Rubbish_Category>();

  useEffect(() => {
    if (record && !isRecordReady) {
      setIsRecordReady(true);
      props.onRecordReady?.(record);
    }
  }, [record, isRecordReady]);

  return (
    <>
      {record ? (
        <Typography>ID: {record.id}</Typography>
      ) : (
        <TextInput
          source="id"
          validate={required()}
          parse={(value) => value.trim()}
        />
      )}
      <TextInput
        source="label"
        label="Nom de la catégorie de déchets"
        validate={required()}
      />
      <TextInput source="color" />
      <TextInput
        source="rank"
        label="Position"
        inputProps={{
          inputMode: "numeric",
        }}
      />
    </>
  );
};

export const AdminRubbishCategoryForm: FC<AdminRubbishCategoryFormProps> = (
  props
) => (
  <SimpleForm
    sx={{
      maxWidth: "600px",
    }}
    onSubmit={props.onSubmit as any}
    toolbar={
      <Toolbar>
        <SaveButton />
      </Toolbar>
    }
  >
    <FormComponent {...props} />
  </SimpleForm>
);
