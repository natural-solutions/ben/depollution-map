import { useApi } from "@/utils/useApi";
import { Edit } from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { AdminRubbishCategoryForm } from "./AdminRubbishCategoryForm";
import { Rubbish_Category } from "@/graphql/types";
import { UPDATE_RUBBISH_CATEGORY } from "@/graphql/queries";
import { pick } from "lodash";

export const AdminRubbishCategoryEdit = () => {
  const { id } = useParams();
  const { postGraphql } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: Rubbish_Category) => {
    if (!id) {
      return;
    }

    try {
      await postGraphql({
        query: UPDATE_RUBBISH_CATEGORY,
        variables: {
          id,
          _set: pick(_set, ["label", "color", "rank"]),
        },
      });

      navigate("/rubbish_category");
    } catch (error) {}
  };

  return (
    <Edit>
      <AdminRubbishCategoryForm onSubmit={onSubmit} />
    </Edit>
  );
};
