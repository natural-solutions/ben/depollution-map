import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  Datagrid,
  DateField,
  EditButton,
  List,
  TextField,
  TextInput,
  DateInput,
} from "react-admin";

const filters = [
  <TextInput key="a" label="Catégorie de déchets" source="label" />,
  <DateInput key="e" label="Date de création =" source="created_at" />,
  <DateInput key="f" label="Date de création >=" source="created_at@_gte" />,
  <DateInput key="g" label="Date de création <" source="created_at@_lte" />,
];

type AdminRubbishCategoryListProps = {};

export const AdminRubbishCategoryList: FC<
  AdminRubbishCategoryListProps
> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Catégorie de déchets" />
        <TextField source="color" label="Couleur" />
        <TextField source="rank" label="Position" />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
