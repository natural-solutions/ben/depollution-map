import { INSERT_RUBBISH_CATEGORY } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import { Create } from "react-admin";
import { useNavigate } from "react-router-dom";
import { AdminRubbishCategoryForm } from "./AdminRubbishCategoryForm";
import { Rubbish_Category } from "@/graphql/types";
import { pick } from "lodash";

export const AdminRubbishCategoryCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: Rubbish_Category) => {
    try {
      const response = await postGraphql<any>({
        query: INSERT_RUBBISH_CATEGORY,
        variables: {
          _set: pick(_set, ["id", "label", "color", "rank"]),
        },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      navigate("/rubbish_category");
    } catch (error) {}
  };

  return (
    <Create>
      <AdminRubbishCategoryForm onSubmit={onSubmit} />
    </Create>
  );
};
