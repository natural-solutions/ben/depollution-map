import { City } from "@/graphql/types";
import { useSessionContext } from "@/utils/useSessionContext";
import { FC } from "react";
import {
  Datagrid,
  EditButton,
  TextField,
  TextInput,
  List,
  ReferenceField,
  AutocompleteArrayInput,
  ReferenceArrayInput,
  FunctionField,
  TopToolbar,
  CreateButton,
  FilterButton,
} from "react-admin";

const filters = [
  <TextInput key="a" label="Code" source="code" />,
  <ReferenceArrayInput
    key="b"
    label="Commune"
    source="id"
    reference="city"
    sort={{ field: "label", order: "ASC" }}
  >
    <AutocompleteArrayInput
      label="Commune"
      optionText="label"
      optionValue="id"
      filterToQuery={(search) => (search ? { "label@_ilike": search } : {})}
    />
  </ReferenceArrayInput>,
];

type AdminCityListProps = {};

export const AdminCityList: FC<AdminCityListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  const AdminCityListActions = () => {
    return (
      <TopToolbar>
        {isAuthorized && <CreateButton />}
        <FilterButton filters={filters} />
      </TopToolbar>
    );
  };
  return (
    <List actions={<AdminCityListActions />} filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Commune" />
        <TextField source="code" label="Code postal" />
        <ReferenceField
          source="departement_id"
          reference="departement"
          label="Département"
        >
          <TextField source="label" />
        </ReferenceField>
        <FunctionField
          source="coords"
          label="Coordonnées"
          render={(record: City) =>
            `${record.coords.coordinates[1]}, ${record.coords.coordinates[0]}`
          }
        />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
