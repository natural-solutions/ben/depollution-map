import { useApi } from "@/utils/useApi";
import { useParams, useNavigate } from "react-router-dom";
import { EditBase, Form } from "react-admin";
import { AdminCityFields } from "./AdminCityFields";
import { pick } from "lodash";
import { slugify } from "@/utils/media";

export const AdminCityEdit = () => {
  const { id } = useParams();
  const { api } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (formData: any) => {
    if (!id) {
      return;
    }

    try {
      const coords = {
        type: "Point",
        crs: {
          type: "name",
          properties: {
            name: "urn:ogc:def:crs:EPSG::4326",
          },
        },
        coordinates: [
          parseFloat(formData.start_point.lng),
          parseFloat(formData.start_point.lat),
        ],
      };
      const slug = slugify(formData.label);
      await api.patch(`/city/${id}`, {
        ...pick(formData, ["label", "code", "departement_id"]),
        slug,
        coords,
      });

      navigate("/city");
    } catch (error) {}
  };

  return (
    <EditBase>
      <Form onSubmit={onSubmit as any}>
        <AdminCityFields />
      </Form>
    </EditBase>
  );
};
