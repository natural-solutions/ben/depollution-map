import { Box, Stack, Typography } from "@mui/material";
import { FC, useCallback, useEffect, useState } from "react";
import {
  AutocompleteInput,
  Button,
  ReferenceInput,
  SaveButton,
  TextInput,
  Toolbar,
  number,
  required,
  useRecordContext,
} from "react-admin";
import {
  AdminCleanupFormMapDialog,
  AdminCleanupFormMapDialogProps,
} from "../cleanup/AdminCleanupFormMapDialog";
import { useFormContext } from "react-hook-form";
import { City } from "@/graphql/types";
import { Map } from "@mui/icons-material";
import { AppDecimalInput } from "../shared/AppDecimalInput";

type AdminCityFieldsProps = {};

export const AdminCityFields: FC<AdminCityFieldsProps> = () => {
  const record = useRecordContext<City>();
  const formContext = useFormContext();
  const [mapDialogMode, setMapDialogMode] =
    useState<AdminCleanupFormMapDialogProps["mode"]>("hidden");
  const [longitude, latitude] = record?.coords?.coordinates
    ? record.coords.coordinates
    : ["", ""];

  const setStartPointCoordinates = useCallback(() => {
    if (record) {
      formContext.setValue("start_point.lat", latitude);
      formContext.setValue("start_point.lng", longitude);
    }
  }, [record, latitude, longitude]);

  useEffect(() => {
    setStartPointCoordinates();
  }, [setStartPointCoordinates]);

  return (
    <Stack p={2}>
      <Stack alignItems={"start"}>
        {record && <Typography variant="h6">ID : {record.id}</Typography>}
        <TextInput
          source="label"
          label="Nom"
          validate={required()}
          parse={(value) => value.trim()}
        />
        <TextInput
          source="code"
          label="Code postal"
          validate={required()}
          parse={(value) => value.trim()}
        />
        <Box sx={{ minWidth: "221px" }}>
          <ReferenceInput
            source="departement_id"
            reference="departement"
            sort={{ field: "label", order: "ASC" }}
          >
            <AutocompleteInput
              label="departement"
              optionText={(choice) => `${choice.label} (${choice.id})`}
              optionValue="id"
              filterToQuery={(search) =>
                search ? { "label@_ilike": search } : {}
              }
            />
          </ReferenceInput>
        </Box>
        <Typography variant="h6">Coordonnées</Typography>
        <Stack direction={"column"} spacing={2}>
          <Button
            label="Afficher la carte"
            variant="contained"
            onClick={() => setMapDialogMode("startPoint")}
            endIcon={<Map />}
            size="medium"
          />
          <AppDecimalInput
            source="start_point.lat"
            label="Latitude"
            defaultValue={latitude}
            validate={[required(), number()]}
          />
          <AppDecimalInput
            source="start_point.lng"
            label="Longitude"
            defaultValue={longitude}
            validate={[required(), number()]}
          />
          <AdminCleanupFormMapDialog
            mode={mapDialogMode}
            onClose={() => {
              setMapDialogMode("hidden");
            }}
          ></AdminCleanupFormMapDialog>
        </Stack>
      </Stack>
      <Box>
        <Toolbar>
          <SaveButton alwaysEnable={true} />
        </Toolbar>
      </Box>
    </Stack>
  );
};
