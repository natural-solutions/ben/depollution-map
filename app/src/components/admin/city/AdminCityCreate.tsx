import { useApi } from "@/utils/useApi";
import { CreateBase, Form } from "react-admin";
import { useNavigate } from "react-router-dom";
import { AdminCityFields } from "./AdminCityFields";
import { pick } from "lodash";
import { INSERT_CITY } from "@/graphql/queries";
import { slugify } from "@/utils/media";

export const AdminCityCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (formData: any) => {
    const coords = {
      type: "Point",
      crs: {
        type: "name",
        properties: {
          name: "urn:ogc:def:crs:EPSG::4326",
        },
      },
      coordinates: [
        parseFloat(formData.start_point.lng),
        parseFloat(formData.start_point.lat),
      ],
    };
    const id = slugify(formData.label + "_" + formData.code);
    const slug = slugify(formData.label);

    try {
      const response = await postGraphql<any>({
        query: INSERT_CITY,
        variables: {
          _set: {
            ...pick(formData, ["label", "code", "departement_id"]),
            id,
            slug,
            coords,
          },
        },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      navigate("/city");
    } catch (error) {}
  };

  return (
    <CreateBase>
      <Form onSubmit={onSubmit as any}>
        <AdminCityFields />
      </Form>
    </CreateBase>
  );
};
