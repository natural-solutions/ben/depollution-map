import { useApi } from "@/utils/useApi";
import { Edit, useNotify } from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";
import { Poi_Type_Set_Input } from "@/graphql/types";
import { slugify, uploadFile } from "@/utils/media";
import { AdminBasicForm } from "../shared/AdminBasicForm";

export const AdminPoiTypeEdit = () => {
  const { api } = useApi();
  const { id } = useParams();
  const navigate = useNavigate();
  const notify = useNotify();

  const onSubmit = async (_set: any) => {
    if (!id) {
      return;
    }

    if (_set.selectLogo?.title === "drop_logo") {
      _set.logo = null;
    }

    const file = _set.selectLogo ? _set.selectLogo?.rawFile : null;
    if (file) {
      file.slugified = slugify(file.name);
      _set.logo = file.slugified;
      delete _set.selectLogo;
    }

    try {
      await api.patch(`/poi_types/${id}`, {
        ...pick(_set, ["label", "logo"]),
      } as Poi_Type_Set_Input);

      if (file) {
        await uploadFile("poi_types", id, file);
      }

      navigate("/poi_type");
    } catch (error: any) {
      notify(error, {
        type: "error",
      });
    }
  };

  return (
    <Edit>
      <AdminBasicForm onSubmit={onSubmit} resourceName="poi_types" />
    </Edit>
  );
};
