import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  Datagrid,
  DateField,
  EditButton,
  List,
  TextField,
  TextInput,
} from "react-admin";

const filters = [<TextInput key="a" label="Point d'intérêt" source="label" />];

type AdminPoiTypeListProps = {};

export const AdminPoiTypeList: FC<AdminPoiTypeListProps> = () => {
  const sessionCtx = useSessionContext();
  const isAuthorized = sessionCtx.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Point d'intérêt" />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
