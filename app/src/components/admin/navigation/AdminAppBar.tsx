import { AppBar } from "react-admin";
import { Box, IconButton, Typography } from "@mui/material";
import { Home } from "@mui/icons-material";
import { SessionBtn } from "@/components/SessionBtn";

export const AdminAppBar = () => (
  <AppBar>
    <Typography id="react-admin-title" />
    <Box flex="1" />
    <SessionBtn />
    <Box sx={{ mr: 2 }} />
    <IconButton color="inherit" href="/">
      <Home />
    </IconButton>
  </AppBar>
);
