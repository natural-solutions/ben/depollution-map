import { Menu } from "react-admin";
import {
  Campaign,
  DeleteForever,
  DeleteSweep,
  Feed,
  FolderDelete,
  FormatListNumbered,
  InsertDriveFile,
  Interests,
  LocationCity,
  Museum,
  Person,
  PersonAddRounded,
  PlaceRounded,
} from "@mui/icons-material";

export const AdminMenu = () => (
  <Menu>
    <Menu.Item
      to="/user_view"
      primaryText="Utilisateurs"
      leftIcon={<Person />}
    />

    <Menu.Item to="/campaign" primaryText="Entités" leftIcon={<Museum />} />

    <Menu.Item
      to="/partner"
      primaryText="Partenaires"
      leftIcon={<PersonAddRounded />}
    />

    <Menu.Item to="/cleanup" primaryText="Ramassages" leftIcon={<Campaign />} />

    <Menu.Item
      to="/characterization_level"
      primaryText="Niveau de caractérisation"
      leftIcon={<FormatListNumbered />}
    />
    <Menu.Item
      to="/cleanup_type"
      primaryText="Type de ramassages"
      leftIcon={<Campaign />}
    />
    <Menu.Item
      to="/cleanup_place"
      primaryText="Lieux des ramassages"
      leftIcon={<PlaceRounded />}
    />

    <Menu.Item
      to="/rubbish_tag"
      primaryText="Tags de déchets"
      leftIcon={<FolderDelete />}
    />

    <Menu.Item
      to="/rubbish_category"
      primaryText="Catégories de déchets"
      leftIcon={<FolderDelete />}
    />

    <Menu.Item to="/rubbish" primaryText="Déchets" leftIcon={<DeleteSweep />} />

    <Menu.Item
      to="/cleanup_form"
      primaryText="Caractérisations"
      leftIcon={<Feed />}
    />

    <Menu.Item to="/brand" primaryText="Marques" leftIcon={<DeleteForever />} />

    <Menu.Item
      to="/poi_type"
      primaryText="Points d'intérêt"
      leftIcon={<Interests />}
    />
    <Menu.Item
      to="/resource"
      primaryText="Ressources"
      leftIcon={<InsertDriveFile />}
    />
    <Menu.Item to="/city" primaryText="Communes" leftIcon={<LocationCity />} />
  </Menu>
);
