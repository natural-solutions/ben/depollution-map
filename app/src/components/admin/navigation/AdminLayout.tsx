import { Layout, LayoutProps } from "react-admin";
import { AdminMenu } from "./AdminMenu";
import { AdminAppBar } from "./AdminAppBar";

export const AdminLayout = (props: LayoutProps) => (
  <Layout {...props} appBar={AdminAppBar} menu={AdminMenu} />
);
