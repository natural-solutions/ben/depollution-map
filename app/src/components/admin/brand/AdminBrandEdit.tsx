import { UPDATE_BRAND } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import {
  Edit,
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
} from "react-admin";
import { useParams, useNavigate } from "react-router-dom";
import { pick } from "lodash";

export const AdminBrandEdit = () => {
  const { id } = useParams();
  const { postGraphql } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    if (!id) {
      return;
    }

    try {
      await postGraphql({
        query: UPDATE_BRAND,
        variables: { id, _set: pick(_set, ["id", "label"]) },
      });

      navigate("/brand");
    } catch (error) {}
  };

  return (
    <Edit>
      <SimpleForm
        onSubmit={onSubmit}
        toolbar={
          <Toolbar>
            <SaveButton />
          </Toolbar>
        }
      >
        <TextInput label="Id" source="id" />
        <TextInput
          source="label"
          label="Nom de la marque"
          validate={required()}
        />
      </SimpleForm>
    </Edit>
  );
};
