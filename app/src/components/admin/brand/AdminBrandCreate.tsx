import { INSERT_BRAND } from "@/graphql/queries";
import { useApi } from "@/utils/useApi";
import {
  Create,
  SaveButton,
  SimpleForm,
  TextInput,
  Toolbar,
  required,
} from "react-admin";
import { useNavigate } from "react-router-dom";
import { pick } from "lodash";

export const AdminBrandCreate = () => {
  const { postGraphql, notifyApiError } = useApi();
  const navigate = useNavigate();

  const onSubmit = async (_set: any) => {
    try {
      const response = await postGraphql<any>({
        query: INSERT_BRAND,
        variables: { _set: pick(_set, ["id", "label"]) },
      });

      if (response.data.errors) {
        return notifyApiError({ response });
      }

      navigate("/brand");
    } catch (error) {}
  };

  return (
    <Create>
      <SimpleForm
        sx={{
          maxWidth: "400px",
        }}
        onSubmit={onSubmit}
        toolbar={
          <Toolbar>
            <SaveButton />
          </Toolbar>
        }
      >
        <TextInput
          source="id"
          validate={required()}
          fullWidth
          parse={(value) => value.trim()}
        />
        <TextInput
          source="label"
          label="Nom de la marque"
          validate={required()}
        />
      </SimpleForm>
    </Create>
  );
};
