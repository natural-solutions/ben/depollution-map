import { useSessionContext } from "@/utils/useSessionContext";
import React, { FC } from "react";
import {
  Datagrid,
  DateField,
  EditButton,
  List,
  TextField,
  TextInput,
} from "react-admin";

const filters = [<TextInput key="a" label="Marque" source="label" />];

type AdminBrandListProps = {};

export const AdminBrandList: FC<AdminBrandListProps> = () => {
  const session = useSessionContext();
  const isAuthorized = session.getIsAdmin();

  return (
    <List filters={filters}>
      <Datagrid isRowSelectable={() => isAuthorized || false}>
        <TextField source="id" />
        <TextField source="label" label="Marque" />
        <DateField source="created_at" label="Créé le" />
        <DateField source="updated_at" label="Modifié le" />
        {isAuthorized && <EditButton />}
      </Datagrid>
    </List>
  );
};
