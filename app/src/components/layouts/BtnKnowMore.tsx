import { Button, IconButton, Menu, MenuItem } from "@mui/material";
import Link from "next/link";
import { FC, useState } from "react";
import { InfoOutlined } from "@mui/icons-material";
import { usePathname } from "next/navigation";

type BtnKnowMoreProps = {
  layout: "desktop" | "mobile";
};

export const BtnKnowMore: FC<BtnKnowMoreProps> = ({ layout }) => {
  const path = usePathname().split("/")[1];

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      {layout == "desktop" ? (
        <Button
          sx={{
            mr: 2,
            fontSize: "inherit",
            color: "inherit",
          }}
          onClick={(e) => {
            e.preventDefault();
            setAnchorEl(e.currentTarget as any);
          }}
        >
          En savoir plus
        </Button>
      ) : (
        <IconButton
          sx={{ px: 2 }}
          onClick={(e) => setAnchorEl(e.currentTarget)}
        >
          <InfoOutlined />
        </IconButton>
      )}
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: layout == "desktop" ? "bottom" : "top",
          horizontal: layout == "desktop" ? "left" : "right",
        }}
        sx={{
          "& .MuiMenu-paper": {
            marginTop: layout == "desktop" ? 0 : "-30px",
            borderRadius: "3px",
          },
        }}
      >
        <MenuItem
          component={Link}
          href="/info"
          onClick={handleClose}
          selected={path == "info"}
        >
          Informations
        </MenuItem>
        <MenuItem
          component={Link}
          href="/legal-notice"
          onClick={handleClose}
          selected={path == "legal-notice"}
        >
          Mentions légales
        </MenuItem>
        <MenuItem
          component={Link}
          href="/personal-data"
          onClick={handleClose}
          selected={path == "personal-data"}
        >
          Protection des données
        </MenuItem>
      </Menu>
    </>
  );
};
