import {
  BottomNavigation,
  BottomNavigationAction,
  Button,
  Paper,
} from "@mui/material";
import Link from "next/link";
import Dashboard from "@mui/icons-material/Dashboard";
import Map from "@mui/icons-material/Map";
import DateRange from "@mui/icons-material/DateRange";
import LoginRoundedIcon from "@mui/icons-material/LoginRounded";
import { usePathname } from "next/navigation";
import { SessionBtn } from "@/components/SessionBtn";
import { signIn } from "next-auth/react";
import { BtnKnowMore } from "./BtnKnowMore";

export default function MobileLayout(session: any) {
  const path = usePathname().split("/")[1];

  if (path === "forms") {
    return null;
  }

  return (
    <>
      <Paper
        sx={{ position: "fixed", bottom: 0, left: 0, right: 0, zIndex: 100 }}
      >
        <BottomNavigation value={path}>
          <BottomNavigationAction
            component={Link}
            href="/"
            icon={<Dashboard />}
            value=""
            sx={{
              minWidth: 0,
            }}
          />
          <BottomNavigationAction
            component={Link}
            href="/map"
            icon={<Map />}
            value="map"
            sx={{
              minWidth: 0,
            }}
          />
          <BottomNavigationAction
            component={Link}
            href="/cleanups"
            icon={<DateRange />}
            value="cleanups"
            sx={{
              minWidth: 0,
            }}
          />
          <BtnKnowMore layout="mobile" />
          {session.session.status === "authenticated" ? (
            <SessionBtn isMobile={true} />
          ) : (
            <Button
              endIcon={<LoginRoundedIcon />}
              onClick={() => signIn("keycloak")}
            />
          )}
        </BottomNavigation>
      </Paper>
    </>
  );
}
