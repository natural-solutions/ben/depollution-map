import { SessionBtn } from "@/components/SessionBtn";
import {
  AppBar,
  Box,
  Stack,
  Toolbar,
  Tab,
  Tabs,
  Button,
  useTheme,
  Typography,
} from "@mui/material";
import { signIn } from "next-auth/react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { BtnKnowMore } from "./BtnKnowMore";
import Image from "next/image";

const ButtonInTabs = (props: any) => {
  const { className, onClick, children } = props;
  return <Button className={className} onClick={onClick} children={children} />;
};

export default function DesktopLayout(session: any) {
  const path = usePathname().split("/")[1];
  const theme = useTheme();

  const selectedTab = ["info", "legal-notice", "personal-data"].includes(path)
    ? "more"
    : path;

  return (
    <AppBar position="fixed" className="header-public" color="default">
      <Toolbar
        sx={{
          paddingRight: "0px",
          paddingBlockEnd: "0px",
          justifyContent: "space-between",
          background: theme.palette.text.primary,
          color: "#FFF",
        }}
      >
        <Stack
          direction="row"
          spacing={3}
          sx={{
            ml: 6,
            alignItems: "center",
          }}
        >
          <Image
            src="/assets/logo-wings.png"
            width={46}
            height={46}
            alt="Logo"
            style={{
              display: "block",
            }}
          />
          <Typography
            variant="h4"
            component="h1"
            sx={{
              fontSize: "16px",
              "@media (min-width:900px)": {
                fontSize: "16px",
              },
            }}
          >
            Wings Map
          </Typography>
        </Stack>

        <Stack
          direction={"row"}
          sx={{
            flex: 1,
            justifyContent: "center",
          }}
        >
          <Tabs
            value={selectedTab}
            sx={{
              fontSize: "16px",
              "& .MuiTabs-indicator": {
                backgroundColor: "transparent",
              },
            }}
          >
            <Tab
              label="Accueil"
              value=""
              component={Link}
              href="/"
              sx={{
                color: "inherit",
                mr: 5,
              }}
            />
            <Tab
              label="Ramassages"
              value="cleanups"
              component={Link}
              href="/cleanups"
              sx={{
                color: "inherit",
                mr: 5,
              }}
            />
            {session.session.status === "authenticated" && [
              <Tab
                key="profil"
                label="Profil"
                value="profil"
                component={Link}
                href="/profil"
                sx={{
                  color: "inherit",
                }}
              />,
            ]}
          </Tabs>
        </Stack>
        <Stack direction="row">
          <BtnKnowMore layout="desktop" />
          {session.session.status !== "authenticated" && (
            <Button
              variant="outlined"
              color="white"
              onClick={() => signIn("keycloak")}
              sx={{ fontWeight: 600 }}
            >
              Connexion
            </Button>
          )}
          <SessionBtn />
        </Stack>
      </Toolbar>
    </AppBar>
  );
}
