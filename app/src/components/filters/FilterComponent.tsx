import {
  GET_CAMPAIGNS,
  GET_CHARACTERIZATION_LEVELS,
  GET_CLEANUP_CITIES,
  GET_PARTNERS,
} from "@/graphql/queries";
import {
  Campaign,
  Characterization_Level,
  City,
  Departement,
  Partner,
  Region,
} from "@/graphql/types";
import { useApi } from "@/utils/useApi";
import { Button, Stack, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import SelectFilter from "./SelectFilter";
import { SelectFilterLocation } from "./SelectFilterLocation";
import { CleanupFilters, DEFAULT_FILTER_VALUES } from "@/utils/cleanups";

export interface FilterComponentProps {
  handleFilter: () => void;
  onResetFilter: () => void;
  fieldValues?: CleanupFilters;
  setFieldValues: (values: any) => void;
  whereCleanups?: any;
}

export const FilterComponent = (props: FilterComponentProps) => {
  const { handleFilter, onResetFilter, fieldValues, setFieldValues } = props;

  const api = useApi();

  const [campaigns, setCampaigns] = useState<Campaign[]>([]);
  const [cities, setCities] = useState<Partial<City>[]>([]);
  const [departements, setDepartements] = useState<Departement[]>([]);
  const [regions, setRegions] = useState<Region[]>([]);
  const [characterizationLevel, setCharacterizationLevel] = useState<
    Characterization_Level[]
  >([]);
  const [partners, setPartners] = useState<Partner[]>([]);

  useEffect(() => {
    (async () => {
      const resp_characterization_level = await api.postGraphql<{
        data: { characterization_level: Characterization_Level[] };
      }>({
        query: GET_CHARACTERIZATION_LEVELS,
        variables: { order_by: { label: "asc" } },
      });
      setCharacterizationLevel(
        resp_characterization_level.data.data.characterization_level
      );

      const resp_campaigns = await api.postGraphql<{
        data: { campaign: Campaign[] };
      }>({
        query: GET_CAMPAIGNS,
        variables: { order_by: { label: "asc" } },
      });
      setCampaigns(resp_campaigns.data.data.campaign);

      const resp_partners = await api.postGraphql<{
        data: { partner: Partner[] };
      }>({
        query: GET_PARTNERS,
        variables: { order_by: { label: "asc" } },
      });
      setPartners(resp_partners.data.data.partner);
      const resp_cities = await api.postGraphql<{
        data: {
          city: City[];
        };
      }>({
        query: GET_CLEANUP_CITIES,
        variables: {
          /* where: !props.whereCleanups
            ? { cleanups: { id: { _is_null: false } } }
            : { cleanups: { cleanup_forms: { status: { _eq: "published" } } } }, */
          where: {
            cleanups: props.whereCleanups || { id: { _is_null: false } },
          },
        },
      });

      const combinedData = resp_cities.data.data.city.reduce(
        (acc: any[], city) => {
          const cityIndex = acc.findIndex(
            (item) =>
              item.city.id === city.id &&
              (city.departement
                ? item.departement.id === city.departement?.id
                : true) &&
              (city.departement?.region
                ? item.region.slug === city.departement?.region?.slug
                : true)
          );

          if (cityIndex === -1) {
            acc.push({
              city: {
                id: city.id,
                label: city.label,
              },
              departement: city.departement
                ? {
                    id: city.departement?.id,
                    label: city.departement?.label,
                  }
                : null,
              region: city.departement?.region
                ? {
                    id: city.departement?.region?.slug,
                    label: city.departement?.region?.label,
                  }
                : null,
            });
          }

          return acc;
        },
        []
      );

      let groupedCities: {
        [cityKey: string]: {
          label: string;
          city_id: string;
        };
      } = {};

      let groupedDepartements: {
        [departementKey: string]: {
          label: string;
          departement_id: any;
        };
      } = {};

      let groupedRegions: {
        [regionKey: string]: {
          region: string;
          region_id: string;
        };
      } = {};

      combinedData.forEach((item) => {
        if (item.region && !groupedRegions[item.region.id]) {
          groupedRegions[item.region.id] = {
            region: item.region.label,
            region_id: item.region.id,
          };
        }

        if (item.departement && !groupedDepartements[item.departement.id]) {
          groupedDepartements[item.departement.id] = {
            label: item.departement.label,
            departement_id: item.departement.id,
          };
        }

        if (!groupedCities[item.city.id]) {
          groupedCities[item.city.id] = {
            label: item.city.label,
            city_id: item.city.id,
          };
        }
      });
      setCities(
        Object.values(groupedCities)
          .sort((a, b) => a.label.localeCompare(b.label))
          .map((city) => ({
            label: city.label,
            id: city.city_id,
          }))
      );

      setDepartements(
        Object.values(groupedDepartements)
          .sort((a, b) => a.label.localeCompare(b.label))
          .map((departement) => ({
            label: departement.label,
            id: departement.departement_id,
          }))
      );

      setRegions(
        Object.values(groupedRegions)
          .sort((a, b) => a.region.localeCompare(b.region))
          .map((region) => ({
            __typename: "region",
            code: region.region_id,
            departements: [],
            departements_aggregate: { nodes: [] },
            label: region.region,
            slug: region.region_id,
          }))
      );
    })();
  }, []);

  const onFilterChange = (fieldName: string, value: any) => {
    setFieldValues({
      ...fieldValues,
      [fieldName]: value,
    });
  };

  const getStringValue = (fieldName: keyof typeof DEFAULT_FILTER_VALUES) => {
    return fieldValues?.[fieldName] || DEFAULT_FILTER_VALUES[fieldName];
  };

  const getArrayValue = (fieldName: keyof typeof DEFAULT_FILTER_VALUES) => {
    return (fieldValues?.[fieldName] ||
      DEFAULT_FILTER_VALUES[fieldName]) as string[];
  };

  return (
    <Stack
      spacing={2}
      sx={{ maxHeight: "100%", overflow: "hidden", pb: "2px" }}
    >
      <Stack sx={{ flex: 1, overflowY: "auto" }}>
        <Stack spacing={2} direction={{ xs: "column", lg: "row" }}>
          <TextField
            label="Date de début"
            onChange={(event: any) =>
              onFilterChange("from_date", event.target.value)
            }
            sx={{
              width: { xs: "100%" },
            }}
            type="date"
            value={getStringValue("from_date")}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            label="Date de fin"
            onChange={(event: any) =>
              onFilterChange("to_date", event.target.value)
            }
            sx={{
              width: { xs: "100%" },
            }}
            type="date"
            value={getStringValue("to_date")}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Stack>
        <SelectFilterLocation
          setSelectedValues={(values) => onFilterChange("cities", values)}
          label="Communes"
          selectedValue={getArrayValue("cities")}
          values={cities}
        />
        <SelectFilterLocation
          setSelectedValues={(values) => onFilterChange("departements", values)}
          label="Départements"
          selectedValue={getArrayValue("departements")}
          values={departements}
        />
        <SelectFilterLocation
          setSelectedValues={(values) => onFilterChange("regions", values)}
          label="Région"
          selectedValue={getArrayValue("regions")}
          values={regions}
        />
        <SelectFilter
          setSelectedValues={(values) => onFilterChange("campaigns", values)}
          label="Missions"
          selectedValue={getArrayValue("campaigns")}
          values={campaigns}
        />
        <SelectFilter
          setSelectedValues={(values) => onFilterChange("partners", values)}
          label="Partenaires"
          selectedValue={getArrayValue("partners")}
          values={partners}
        />
        <SelectFilter
          setSelectedValues={(values) =>
            onFilterChange("characterizationLevel", values)
          }
          label="Niveau de caractérisation"
          selectedValue={getArrayValue("characterizationLevel")}
          values={characterizationLevel}
        />
      </Stack>
      <Stack direction="row" sx={{ justifyContent: "center" }} spacing={2}>
        <Button variant="contained" onClick={handleFilter}>
          Filtrer
        </Button>
        <Button variant="contained" color="secondary" onClick={onResetFilter}>
          Tout effacer
        </Button>
      </Stack>
    </Stack>
  );
};
