"use client";

import { Autocomplete, Chip, TextField } from "@mui/material";

interface SelectFilterlprops {
  setSelectedValues: (values: string[]) => void;
  label: string;
  selectedValue: any[];
  values: any[];
}

export default function SelectFilter(props: SelectFilterlprops) {
  const { setSelectedValues, label, selectedValue, values } = props;

  const allOptions: { id: string; label: string }[] = values.map((value) => {
    let options = { label: value.label, id: value.id };
    return options;
  });
  const handleSelectedValues = (selectedValue: any[]) => {
    setSelectedValues(selectedValue.map((value) => value.id));
  };

  return (
    <Autocomplete
      multiple
      options={allOptions}
      getOptionLabel={(option) => option.label}
      value={allOptions.filter((option) => selectedValue.includes(option.id))}
      onChange={(event, newValue) => {
        handleSelectedValues(newValue);
      }}
      renderOption={(props, option) => (
        <li {...props} key={option.id}>
          {option.label}
        </li>
      )}
      renderTags={(tagValue, getTagProps) =>
        tagValue.map((option, index) => (
          <Chip
            {...getTagProps({ index })}
            key={option.id}
            label={option.label}
          />
        ))
      }
      renderInput={(params) => (
        <TextField {...params} label={label} variant="filled" />
      )}
    />
  );
}
