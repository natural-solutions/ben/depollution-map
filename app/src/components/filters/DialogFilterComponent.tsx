"use client";

import {
  Dialog,
  DialogTitle,
  DialogContent,
  IconButton,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { Close } from "@mui/icons-material";
import { FilterComponent, FilterComponentProps } from "./FilterComponent";

interface DialogFilterComponentProps {
  open: boolean;
  onClose: Function;
}

export default function DialogFilterComponent(
  props: DialogFilterComponentProps & FilterComponentProps
) {
  const {
    open,
    onClose,
    handleFilter,
    onResetFilter,
    fieldValues,
    setFieldValues,
    whereCleanups,
  } = props;

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <Dialog
      open={open}
      onClose={() => onClose()}
      fullScreen={isMobile}
      fullWidth={true}
      maxWidth={"sm"}
      sx={{
        p: {
          md: 2,
        },
      }}
      PaperProps={{
        square: isMobile,
      }}
    >
      <DialogTitle sx={{ textAlign: "center" }}>
        Filtres
        <IconButton
          edge="end"
          color="inherit"
          onClick={() => onClose()}
          aria-label="close"
          sx={{
            position: "absolute",
            right: theme.spacing(3),
            top: theme.spacing(1),
          }}
        >
          <Close />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={{ p: 2 }}>
        <FilterComponent
          handleFilter={handleFilter}
          fieldValues={fieldValues}
          setFieldValues={setFieldValues}
          onResetFilter={onResetFilter}
          whereCleanups={whereCleanups}
        />
      </DialogContent>
    </Dialog>
  );
}
