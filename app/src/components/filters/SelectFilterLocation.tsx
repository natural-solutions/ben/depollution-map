import { Chip } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";

interface SelectFilterLocationProps {
  setSelectedValues: (values: string[]) => void;
  label: string;
  selectedValue: string[];
  values: any[];
}

export const SelectFilterLocation: React.FC<SelectFilterLocationProps> = (
  props
) => {
  const { setSelectedValues, label, selectedValue, values } = props;

  const allOptions: { name: string; id: string }[] = values.map((location) => {
    let options = { name: location.label, id: location.id || location.code };
    return options;
  });

  const handleSelectedValues = (selectedValues: any[]) => {
    setSelectedValues(selectedValues.map((value) => value.id));
  };

  return (
    <Autocomplete
      multiple
      options={allOptions}
      getOptionLabel={(option) => option.name}
      value={allOptions.filter((option) => selectedValue.includes(option.id))}
      onChange={(_, newValue) => {
        handleSelectedValues(newValue);
      }}
      renderOption={(props, option) => (
        <li {...props} key={option.id}>
          {option.name}
        </li>
      )}
      renderTags={(tagValue, getTagProps) =>
        tagValue.map((option, index) => (
          <Chip
            {...getTagProps({ index })}
            key={option.id}
            label={option.name}
          />
        ))
      }
      renderInput={(params) => (
        <TextField {...params} label={label} variant="filled" />
      )}
    />
  );
};
