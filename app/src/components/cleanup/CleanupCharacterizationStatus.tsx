import React from "react";
import Event from "@mui/icons-material/Event";
import { Cleanup, Cleanup_Form, Cleanup_View } from "@/graphql/types";
import { getCleanupsFormStatus } from "@/utils/cleanups";
import { CheckCircle, Publish, Save, Send, Warning } from "@mui/icons-material";
import Tooltip from "@mui/material/Tooltip";

interface CleanupCharacterizationStatusProps {
  cleanup: Cleanup | Cleanup_View;
}

export const CleanupCharacterizationStatus: React.FC<
  CleanupCharacterizationStatusProps
> = ({ cleanup }) => {
  const status = getCleanupsFormStatus(cleanup);
  const statusIconMap = {
    completed: (
      <Tooltip title="Réalisés">
        <CheckCircle style={{ color: "green" }} />
      </Tooltip>
    ),
    upcoming: (
      <Tooltip title="A venir">
        <Event style={{ color: "#2196f3" }} />
      </Tooltip>
    ),
    missing: (
      <Tooltip title="Info manquante">
        <Warning style={{ color: "#fed24e" }} />
      </Tooltip>
    ),
  };

  let icon = status ? statusIconMap[status] : null;
  return icon;
};

interface CleanupFormCharacterizationStatusProps {
  cleanupForm?: Cleanup_Form;
}
export const CleanupFormCharacterizationStatus: React.FC<
  CleanupFormCharacterizationStatusProps
> = ({ cleanupForm }) => {
  const status = cleanupForm?.status;
  const statusIconMap: any = {
    draft: (
      <Tooltip title="Enregistré">
        <Save style={{ color: "#2196f3" }} />
      </Tooltip>
    ),
    tocheck: (
      <Tooltip title="À vérifier">
        <Send style={{ color: "#fed24e" }} />
      </Tooltip>
    ),
    published: (
      <Tooltip title="Publié">
        <Publish style={{ color: "green" }} />
      </Tooltip>
    ),
  };

  let icon = status ? statusIconMap[status] : null;
  return icon;
};
