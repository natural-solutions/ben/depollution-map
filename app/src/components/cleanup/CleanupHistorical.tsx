import { Cleanup_View } from "@/graphql/types";
import { getCleanupWeightFormat, printNumber } from "@/utils/cleanups";
import { Circle } from "@mui/icons-material";
import { Link, List, ListItem, ListItemIcon, Typography } from "@mui/material";
import { format } from "date-fns";

export const CleanupHistoryRow = (props: { cleanup: Cleanup_View }) => {
  const { cleanup } = props;
  const weightFmt = getCleanupWeightFormat(cleanup);

  return (
    <ListItem key={cleanup.id}>
      <ListItemIcon sx={{ minWidth: "30px" }}>
        <Circle color="primary" fontSize="small" />
      </ListItemIcon>
      <Link
        color="black"
        href={`/cleanups/${cleanup.id}?back_url=back`}
        underline="none"
      >
        {cleanup.start_at
          ? format(new Date(cleanup.start_at), "dd/MM/yyyy")
          : ""}{" "}
        -{" "}
        {cleanup.cleanup_type_id === "butts"
          ? `${printNumber(cleanup.total_butts)} Mégots`
          : weightFmt.label}{" "}
        - {cleanup.label}
      </Link>
    </ListItem>
  );
};

export const CleanupHistoricalToDisplay = (props: {
  cleanupHistorical: Cleanup_View[];
  isCategorized: boolean;
}) => {
  const { cleanupHistorical, isCategorized } = props;

  const today = new Date();

  const displayFuturCleanup: Cleanup_View[] = [];
  const displayPastCleanup: Cleanup_View[] = [];

  if (isCategorized) {
    cleanupHistorical.forEach((clp) => {
      if (new Date(clp.start_at) < today) {
        displayPastCleanup.push(clp);
      } else {
        displayFuturCleanup.push(clp);
      }
    });
  }

  return (
    <List>
      {isCategorized ? (
        <>
          {displayFuturCleanup.length > 0 && (
            <>
              <ListItem>
                <Typography variant="h6">Futur</Typography>
              </ListItem>
              {displayFuturCleanup.map((clp) => (
                <CleanupHistoryRow key={clp.id} cleanup={clp} />
              ))}
            </>
          )}

          {displayPastCleanup.length > 0 && (
            <>
              <ListItem>
                <Typography variant="h6">Passé</Typography>
              </ListItem>
              {displayPastCleanup.map((clp) => (
                <CleanupHistoryRow key={clp.id} cleanup={clp} />
              ))}
            </>
          )}
        </>
      ) : (
        <>
          {cleanupHistorical.map((clp) => (
            <CleanupHistoryRow key={clp.id} cleanup={clp} />
          ))}
        </>
      )}
    </List>
  );
};
