import { Cleanup } from "@/graphql/types";
import { FC, ReactNode, useEffect, useState } from "react";
import { useApi } from "@/utils/useApi";
import {
  Box,
  Grid,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import PieChart from "../dashboard/PieChart";
import BarChart from "../dashboard/BarChart";
import { round } from "lodash";
import { getWeightFormat, printNumber } from "@/utils/cleanups";

const FocusItem: FC<{
  label: ReactNode;
  icon: string;
}> = ({ label, icon }) => (
  <Grid
    item
    xs={6}
    sm={4}
    md={6}
    lg={4}
    sx={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    }}
  >
    <img
      style={{
        width: "60px",
        height: "54px",
        display: "block",
        objectFit: "contain",
      }}
      src={`/assets/icons/dashboard/${icon}`}
    />
    <Typography
      variant="caption"
      sx={{ textAlign: "center", lineHeight: "20px" }}
    >
      {label}
    </Typography>
  </Grid>
);

const MainItem: FC<{
  label: ReactNode;
  icon: string;
}> = ({ label, icon }) => (
  <Grid
    item
    xs={12}
    sm={4}
    md={6}
    lg={6}
    xl={4}
    sx={{
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    }}
  >
    <img
      src={`/assets/icons/rubbish/${icon}.png`}
      style={{
        width: "28px",
        height: "auto",
      }}
    />
    <Typography variant="caption" sx={{ ml: 1 }}>
      {label}
    </Typography>
  </Grid>
);

export const CleanupDetailStats = (props: { cleanup: Cleanup }) => {
  const { cleanup } = props;

  const { api } = useApi();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  const [basicStats, setBasicStats] = useState({
    butts: 0,
    food_packaging: 0,
    liters: 0,
    kgPerVolunteer: 0,
    mermaid_tears: 0,
    rubbishesKg: 0,
    volunteers: 0,
  });
  const [rubbishPerCategory, setRubbishPerCategory] = useState({
    data: [{ label: "", value: 0 }],
    totalWeight: 0,
  });
  const [mainRubbishTags, setMainRubbishTags] = useState({
    xaxis: [],
    series: [{ data: [0], color: "" }],
  });

  const isEmpty = mainRubbishTags.xaxis.length === 0;

  useEffect(() => {
    (async () => {
      const resp = await api.post(`/dashboard?isSmallDashboard=true`, {
        ids: [cleanup.id],
      });
      const respBasicStats = resp.data.basicStats;
      const respRubbishPerCategory = resp.data.rubbishPerCategory;
      const respMainRubbishTags = resp.data.mainRubbishTags;

      if (!respRubbishPerCategory.data.length) {
        setRubbishPerCategory({
          data: [
            { label: "Plastique", value: 0 },
            { label: "Métal", value: 0 },
            { label: "Textile", value: 0 },
            { label: "Autre", value: 0 },
          ],
          totalWeight: 0,
        });
      } else {
        respRubbishPerCategory.data = respRubbishPerCategory.data.map(
          (item: any) => ({
            ...item,
            label: `${item.label} ${getWeightFormat(item.value).label}`,
            labelHTML: (
              <>
                {item.label}{" "}
                <b>
                  {round(
                    (item.value / respRubbishPerCategory.totalWeight) * 100,
                    2
                  )}
                  %
                </b>
              </>
            ),
          })
        );
        setRubbishPerCategory(respRubbishPerCategory);
      }

      setBasicStats(respBasicStats);
      setMainRubbishTags(respMainRubbishTags);
    })();
  }, []);

  return (
    <>
      <Box color={cleanup.cleanup_forms.length > 0 ? "" : "#b7b7b7"}>
        <Box
          sx={{
            background: `${
              cleanup.cleanup_forms.length > 0 ? "#e2f3ff" : "#f0f9ff"
            }`,
            py: 2,
          }}
        >
          <Grid
            container
            spacing={1}
            sx={{
              /* m: 0,
              p: 1, 
              width: "100%",*/
              justifyContent: "center",
            }}
          >
            <FocusItem
              icon="focus_poids_ramasse-min.png"
              label={
                <>
                  <b style={{ whiteSpace: "nowrap", display: "block" }}>
                    {printNumber(round(basicStats.rubbishesKg, 1))} kg
                  </b>{" "}
                  ramassés
                </>
              }
            />
            <FocusItem
              icon="focus_nombre_participants-min.png"
              label={
                <>
                  <b style={{ whiteSpace: "nowrap", display: "block" }}>
                    {printNumber(basicStats.volunteers)} bénévoles
                  </b>{" "}
                  mobilisés
                </>
              }
            />
            <FocusItem
              icon="focus_poids_par_participant-min.png"
              label={
                <>
                  <span style={{ whiteSpace: "nowrap", display: "block" }}>
                    <b>
                      {printNumber(
                        round(
                          basicStats.rubbishesKg / (basicStats.volunteers || 1),
                          1
                        )
                      )}{" "}
                      kg
                    </b>{" "}
                    par
                  </span>
                  participant
                </>
              }
            />
          </Grid>
        </Box>
        <Grid item xs={12} sx={{ mt: 1 }}>
          <PieChart
            data={rubbishPerCategory.data}
            totalWeight={rubbishPerCategory.totalWeight}
          />
        </Grid>
        <Grid
          item
          xs={12}
          sx={{
            mt: {
              xs: 4,
              xl: 1,
            },
          }}
        >
          <Grid
            container
            spacing={1}
            sx={{
              justifyContent: "center",
            }}
          >
            <MainItem
              icon="mermaid_tears"
              label={
                <>
                  <span style={{ whiteSpace: "nowrap" }}>
                    <b>{printNumber(basicStats.mermaid_tears)}</b> larmes
                  </span>{" "}
                  de sirènes
                </>
              }
            />
            <MainItem
              icon="food_packaging"
              label={
                <>
                  <span style={{ whiteSpace: "nowrap" }}>
                    <b>{printNumber(basicStats.food_packaging)}</b> emballages
                  </span>{" "}
                  alimentaires
                </>
              }
            />
            <MainItem
              icon="butts"
              label={
                <span style={{ whiteSpace: "nowrap" }}>
                  <b>{printNumber(basicStats.butts)}</b> mégots
                </span>
              }
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sx={{ mt: 3 }}>
          <Typography variant="h5" sx={{ mb: 2 }}>
            Provenance des déchets
          </Typography>
          {!isEmpty && (
            <BarChart
              data={mainRubbishTags}
              height={isMobile ? 400 : 310}
              isOriginOfRubbishGraph={true}
            />
          )}
        </Grid>
      </Box>
    </>
  );
};
