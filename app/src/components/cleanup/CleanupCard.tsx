import {
  Box,
  Button,
  CircularProgress,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { Cleanup, Cleanup_View } from "@/graphql/types";
import { format } from "date-fns";
import { CleanupCharacterizationStatus } from "./CleanupCharacterizationStatus";
import { getCleanupsFormStatus } from "@/utils/cleanups";
import { ArrowForward, History } from "@mui/icons-material";
import { CleanupCardThumb } from "./CleanupCardThumb";
import { useEffect, useState } from "react";
import { useApi } from "@/utils/useApi";
import { GET_CLEANUP_IDS } from "@/graphql/queries";
import { useSessionContext } from "@/utils/useSessionContext";
import { CleanupDialogHistory } from "./CleanupDialogHistory";
import { useSession } from "next-auth/react";

interface CleanupCardProps {
  cleanup: Cleanup_View;
  canShowHistory?: boolean;
}

function CleanupCard({ cleanup, canShowHistory }: CleanupCardProps) {
  const api = useApi();
  const theme = useTheme();
  const session = useSession();
  const isXs = useMediaQuery(theme.breakpoints.only("xs"));
  const cleanupMissing = getCleanupsFormStatus(cleanup);
  const sessionCtx = useSessionContext();
  const isAdmin = sessionCtx.getIsAdmin();
  const [historyIds, setHistoryIds] = useState<Cleanup[]>([]);
  const [isLoadingHistory, setIsLoadingHistory] = useState(false);
  const [isHistoryOpen, setIsHistoryOpen] = useState(false);

  useEffect(() => {
    if (!canShowHistory || !cleanup.cleanup_place_id) {
      return;
    }
    setIsLoadingHistory(true);
    (async () => {
      const resp = await api.postGraphql<{
        data: { cleanup: Cleanup[] };
      }>({
        query: GET_CLEANUP_IDS,
        variables: {
          where: {
            cleanup_place_id: { _eq: cleanup.cleanup_place_id },
            cleanup_forms: { status: { _eq: "published" } },
          },
          order_by: { start_at: "desc" },
          limit: 2,
        },
      });
      setHistoryIds(resp.data.data.cleanup);
      setIsLoadingHistory(false);
    })();
  }, [cleanup]);

  const openHistory = async () => {
    setIsHistoryOpen(true);
  };

  return (
    <>
      <Paper
        sx={{
          display: "flex",
          height: 150,
          overflow: "hidden",
        }}
        variant="outlined"
      >
        <CleanupCardThumb cleanup={cleanup} />
        <Box
          sx={{
            flex: 1,
            overflow: "hidden",
          }}
        >
          <Stack
            sx={{
              pt: 1,
              pl: {
                xs: 1,
                md: 2,
              },
              pr: 1,
              height: "100%",
              fontSize: "12px",
              fontFamily: "Poppins",
              lineHeight: {
                xs: "auto",
                lg: "24px",
              },
              overflow: "hidden",
            }}
          >
            <Typography
              variant="h6"
              sx={{
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
              }}
            >
              {cleanup.label}{" "}
              {cleanupMissing === "missing" && (
                <CleanupCharacterizationStatus cleanup={cleanup} />
              )}
            </Typography>
            <Typography
              color="primary"
              sx={{
                fontFamily: "inherit",
                fontSize: "inherit",
                lineHeight: "inherit",
              }}
            >
              {cleanup.cleanup_campaigns[0]?.campaign.label}
            </Typography>
            <Stack direction="row">
              <Typography
                color="text.secondary"
                sx={{
                  fontFamily: "inherit",
                  fontSize: "inherit",
                  lineHeight: "inherit",
                }}
              >
                {cleanup?.start_at
                  ? format(new Date(cleanup.start_at), "dd/MM/yyyy")
                  : ""}
              </Typography>

              <Box
                sx={{
                  position: "relative",
                  ml: 2,
                  mt: "-4px",
                  minWidth: "24px",
                }}
              >
                {historyIds.length > 1 && (
                  <Button
                    variant="outlined"
                    size="small"
                    startIcon={<History />}
                    disabled={isLoadingHistory}
                    onClick={openHistory}
                  >
                    historique
                  </Button>
                )}
                {isLoadingHistory && (
                  <CircularProgress
                    size={24}
                    sx={{
                      color: "#2C5B8B",
                      position: "absolute",
                      top: "50%",
                      left: "50%",
                      marginTop: "-12px",
                      marginLeft: "-12px",
                    }}
                  />
                )}
              </Box>
            </Stack>

            <Typography
              color="text.secondary"
              sx={{
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
                fontFamily: "inherit",
                fontSize: "inherit",
                lineHeight: "inherit",
              }}
            >
              {cleanup?.city?.label}
              {cleanup?.city?.departement?.region?.label
                ? `, ${cleanup?.city?.departement?.region?.label}`
                : ""}
            </Typography>
            <Box
              /* direction="row"
              spacing={{
                xs: 0,
                md: 2,
              }} */
              sx={{
                mt: "auto",
                pb: "2px",
              }}
            >
              <Button
                size="small"
                color="secondary"
                sx={{
                  color: "#6CA080",
                  fontFamily: "inherit",
                }}
                href={`/cleanups/${cleanup.id}?back_url=back`}
                endIcon={isXs ? null : <ArrowForward />}
              >
                En savoir {isXs ? "+" : "plus"}
              </Button>
              {cleanup?.cleanup_forms.length === 0 &&
              sessionCtx.getCanCreateCleanupForm(cleanup) ? (
                <Button
                  size="small"
                  color="secondary"
                  sx={{
                    color: "#6CA080",
                    fontFamily: "inherit",
                  }}
                  href={`/forms#/cleanup_form/create?cleanup_id=${cleanup.id}`}
                >
                  Carac{isXs ? "." : "térisation"}
                </Button>
              ) : isAdmin ||
                (cleanup.cleanup_forms[0] &&
                  cleanup?.cleanup_forms[0].status !== "published" &&
                  sessionCtx.getCanEditCleanupForm(cleanup.cleanup_forms[0])) ||
                (cleanup.cleanup_forms[0] &&
                  cleanup?.cleanup_forms[0].status === "published" &&
                  session.status === "authenticated") ? (
                <Button
                  size="small"
                  color="secondary"
                  sx={{
                    color: "#6CA080",
                    fontFamily: "inherit",
                  }}
                  href={`/forms#/cleanup_form/${cleanup.cleanup_forms[0].id}`}
                >
                  Carac{isXs ? "." : "térisation"}
                </Button>
              ) : null}
            </Box>
          </Stack>
        </Box>
      </Paper>
      <CleanupDialogHistory
        placeId={cleanup.cleanup_place_id!}
        isOpen={isHistoryOpen}
        setIsOpen={setIsHistoryOpen}
      />
    </>
  );
}

export default CleanupCard;
