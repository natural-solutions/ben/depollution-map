"use client";

import { Cleanup, Cleanup_Area } from "@/graphql/types";
import {
  Box,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { Map } from "react-map-gl/maplibre";
import { Layer, Marker, NavigationControl, Source } from "react-map-gl";
import { InfoPaper } from "./InfoPaper";
import { format } from "date-fns";
import { useEffect, useState } from "react";
import { MAP_INIT_POINT, MAP_STYLE } from "@/utils/map";
import { printNumber } from "@/utils/cleanups";

export const CleanupPlanning = ({ cleanup }: { cleanup: Cleanup }) => {
  const [mapViewState, setMapViewState] = useState(MAP_INIT_POINT);
  const [area, setArea] = useState<number>(0);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  useEffect(() => {
    setArea(cleanup.total_area || 0);

    if (cleanup.start_point) {
      setMapViewState({
        longitude: cleanup.start_point.lng,
        latitude: cleanup.start_point.lat,
        zoom: 14,
      });
    }
  }, []);

  return (
    <Stack
      direction={"column"}
      spacing={2}
      sx={{
        flex: 1,
        marginTop: 2,
        paddingBottom: "10px",
      }}
    >
      <Typography variant="h5">Planification</Typography>
      <InfoPaper
        title="Date du ramassage"
        content={
          cleanup.start_at
            ? format(new Date(cleanup.start_at), "dd/MM/yyyy - HH:mm")
            : "Non planifié"
        }
      />
      <Typography variant="h5">Zone du ramassage</Typography>
      <Paper
        square
        variant="outlined"
        sx={{
          height: "440px",
        }}
      >
        <Map
          {...mapViewState}
          mapStyle={MAP_STYLE as any}
          onMove={(e: any) => setMapViewState(e.viewState)}
          style={{ height: "100%" }}
        >
          <NavigationControl showCompass={false} />
          {cleanup.start_point && (
            <Marker
              key={cleanup.id}
              latitude={cleanup.start_point.lat}
              longitude={cleanup.start_point.lng}
              style={{
                cursor: "pointer",
              }}
            />
          )}
          {cleanup.cleanup_areas.map((area: Cleanup_Area) => (
            <Source data={area.geom} key={area.id} type="geojson">
              <Layer
                type="line"
                source="cleanup-areas"
                paint={{
                  "line-color": area.fill_color || "#FF6F00",
                  "line-width":
                    area.geom?.geometry?.type === "LineString" ? 3 : 2,
                }}
              />
              {area.geom?.geometry?.type !== "LineString" && (
                <Layer
                  type="fill"
                  source="cleanup-areas"
                  paint={{
                    "fill-color": area.fill_color || "#FF6F00",
                    "fill-opacity": 0.2,
                  }}
                />
              )}
            </Source>
          ))}
        </Map>
      </Paper>
      {cleanup.cleanup_areas.length > 0 && (
        <Paper
          square
          variant="outlined"
          sx={{
            p: 2,
            background: "#FAFAFA",
          }}
        >
          <Typography variant="h6" sx={{ color: "7E8989" }}>
            Superficie globale : {printNumber(area)} m²
          </Typography>

          <Box>
            {cleanup.cleanup_areas.map((cleanup_area) => (
              <Typography
                key={cleanup_area.id}
                color={
                  cleanup_area.fill_color ? cleanup_area.fill_color : "black"
                }
                variant="body2"
                sx={{ mt: 1 }}
              >
                {cleanup_area.label} : {printNumber(cleanup_area.size ?? 0)}{" "}
                {cleanup_area.geom.geometry.type === "LineString" ? "m" : "m²"}
              </Typography>
            ))}
          </Box>
        </Paper>
      )}
    </Stack>
  );
};
