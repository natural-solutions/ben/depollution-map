import { Cleanup_View } from "@/graphql/types";
import { IMAGOR_PRESET_NAME, getTransformationURL } from "@/utils/media";
import {
  Box,
  CardMedia,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";

interface CleanupCardProps {
  cleanup: Cleanup_View;
}

export const CleanupCardThumb = ({ cleanup }: CleanupCardProps) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const cleanupFormStatus =
    cleanup.cleanup_forms && cleanup.cleanup_forms.length > 0
      ? cleanup.cleanup_forms[0].status
      : "";
  const totalWeight =
    cleanup.total_weight >= 1000
      ? cleanup.total_weight / 1000
      : cleanup.total_weight;
  const totalWeightUnit = cleanup.total_weight >= 1000 ? "T" : "Kg";
  const isButts = cleanup.cleanup_type_id === "butts";

  const imgW = isMobile
    ? cleanupFormStatus === "published"
      ? "75px"
      : "140px"
    : cleanupFormStatus === "published"
    ? "108px"
    : "192px";

  return (
    <>
      <CardMedia
        component="img"
        sx={{
          minWidth: imgW,
          maxWidth: imgW,
          height: "100%",
        }}
        image={
          !cleanup.cleanup_photo?.filename
            ? "/assets/card-placeholder.jpg"
            : getTransformationURL(
                "cleanups",
                cleanup.id,
                cleanup.cleanup_photo.filename,
                IMAGOR_PRESET_NAME["200x200"]
              )
        }
      />
      {cleanupFormStatus === "published" && (
        <Box
          sx={{
            minWidth: isMobile ? 75 : 86,
            maxWidth: isMobile ? 75 : 86,
            height: "100%",
            backgroundColor:
              cleanup?.cleanup_campaigns[0]?.campaign.main_color || "#2C5B8B",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Box
            sx={{
              color: "#FFFFFF",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            {isButts ? (
              <>
                <Typography
                  variant="h2"
                  sx={{
                    color: "inherit",
                    fontSize: isMobile ? "0.8rem" : "22px",
                    fontWeight: 900,
                  }}
                >
                  {cleanup.total_butts}
                </Typography>
                <Typography
                  variant="h2"
                  sx={{
                    color: "inherit",
                    fontSize: isMobile ? "0.8rem" : "18px",
                    fontWeight: 700,
                    textTransform: "none",
                  }}
                >
                  Mégots
                </Typography>
              </>
            ) : (
              <>
                <Typography
                  variant="h2"
                  sx={{
                    color: "inherit",
                    fontSize: isMobile ? "1rem" : "28px",
                    fontWeight: 800,
                  }}
                >
                  {totalWeightUnit === "T"
                    ? Number(totalWeight).toFixed(2).replace(".", ",")
                    : Math.round(totalWeight)}
                </Typography>
                <Typography
                  variant="h2"
                  sx={{
                    color: "inherit",
                    fontSize: isMobile ? "0.8rem" : "18px",
                    fontWeight: 900,
                  }}
                >
                  {totalWeightUnit}
                </Typography>
              </>
            )}
          </Box>
        </Box>
      )}
    </>
  );
};
