import { Box, Paper, Typography, Button } from "@mui/material";
import { useState } from "react";

export const InfoPaper = ({
  title,
  content,
}: {
  title: string;
  content: string | number;
}) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const contentString = content.toString();
  const isContentLong = contentString.length > 100;

  const displayContent =
    isContentLong && !isExpanded
      ? contentString.slice(0, 100) + "..."
      : contentString;

  return (
    <Box>
      <Paper
        square
        variant="outlined"
        sx={{
          padding: "10px",
          backgroundColor: "#fafafa",
          border: "none",
          minHeight: "50px",
        }}
      >
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Typography variant="h6" sx={{ color: "#7E8989" }}>
            {title}
          </Typography>
          {isContentLong && (
            <Button size="small" onClick={() => setIsExpanded(!isExpanded)}>
              {isExpanded ? "Voir moins" : "Voir plus"}
            </Button>
          )}
        </Box>
        <Typography>{displayContent}</Typography>
      </Paper>
    </Box>
  );
};
