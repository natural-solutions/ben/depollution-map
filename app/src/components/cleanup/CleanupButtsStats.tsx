import { Cleanup, Cleanup_Area } from "@/graphql/types";
import { getCleanupForm, printNumber } from "@/utils/cleanups";
import { Box, Stack, Typography, useMediaQuery, useTheme } from "@mui/material";
import BarChart, { BarChartProps } from "../dashboard/BarChart";
import { FC } from "react";

const FocusItem: FC<{
  nb: number;
  unit?: string;
  label: string;
  icon: string;
}> = ({ nb, unit, label, icon }) => (
  <Stack direction="row" spacing={1}>
    <img
      src={`/assets/icons/dashboard/${icon}`}
      style={{
        display: "block",
        width: `22px`,
        height: "22px",
        objectFit: "contain",
      }}
    />
    <Typography>
      <b
        style={{
          fontWeight: 600,
          whiteSpace: "nowrap",
        }}
      >
        {printNumber(nb)}
        {unit}
      </b>{" "}
      {label}
    </Typography>
  </Stack>
);

export const CleanupButtsStats = (props: { cleanup: Cleanup }) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  const { cleanup } = props;
  const cleanupForm = getCleanupForm(cleanup);
  if (!cleanupForm) {
    return;
  }

  const butts = cleanupForm.cleanup_form_rubbishes?.[0]?.quantity || 0;
  const basicStats = {
    butts: butts,
    liters: butts * 3,
    volunteers:
      (cleanupForm.volunteers_number || 0) +
      (cleanupForm.external_volunteers_number || 0),
  };

  const detailAreas =
    cleanupForm.cleanup_form_rubbish_details?.filter(
      (item) => item.cleanup_area_id && item.rubbish_id === "butts"
    ) || [];
  const detailPois =
    cleanupForm.cleanup_form_pois?.filter(({ cleanup_area }) => cleanup_area) ||
    [];
  const areas = detailAreas
    .map(({ cleanup_area }) => cleanup_area as Cleanup_Area)
    .concat(detailPois.map((item) => item.cleanup_area as Cleanup_Area))
    .reduce<Cleanup_Area[]>((uniques, item) => {
      return uniques.some(({ id }) => id == item.id)
        ? uniques
        : [...uniques, item];
    }, []);
  //const pois = cleanupForm.cleanup_form_pois.map((item) => item.poi_type) || [];

  const titles = {
    qty: "Mégots",
    poi: "Points d'intérêts",
  };

  const chartData: BarChartProps["data"] = {
    xaxis: areas.map((_, i) => (i + 1).toString()),
    series: [
      {
        name: titles.qty,
        data: areas.map(({ id }) =>
          detailAreas
            .filter(({ cleanup_area_id }) => cleanup_area_id == id)
            .reduce((total, item) => total + (item.quantity || 0), 0)
        ),
        color: "#3DA0EA",
      },
      {
        name: titles.poi,
        data: areas.map(({ id }) =>
          detailPois
            .filter(({ cleanup_area_id }) => cleanup_area_id == id)
            .reduce((total, item) => total + (item.nb || 0), 0)
        ),
        color: "#3DA0EA",
      },
    ],
  };

  return (
    <Box color={cleanup.cleanup_forms.length > 0 ? "" : "#b7b7b7"}>
      <Typography sx={{ fontWeight: 500 }}>
        Nombre de mégots et de lieux indicateurs par zone
      </Typography>

      <Typography variant="body2" sx={{ mt: 2 }}>
        La présence de mégots est liée à des “lieux indicateurs”*.
      </Typography>

      <Box sx={{ mt: 2 }}>
        <BarChart
          isStacked={false}
          data={chartData}
          height={isMobile ? 400 : 310}
          tooltipYFormatter={(val, _) => val.toString()}
          tooltipXFormatter={(val) => {
            return areas[Number(val) - 1].label || val.toString();
          }}
          yAxis={[
            {
              seriesName: titles.qty,
              min: 0,
            },
            {
              seriesName: titles.poi,
              min: 0,
              opposite: true,
            },
          ]}
        />
      </Box>

      <Box
        sx={{
          background: `${
            cleanup.cleanup_forms.length > 0 ? "#e2f3ff" : "#f0f9ff"
          }`,
          mt: 2,
          py: 1,
          px: 2,
        }}
      >
        <FocusItem
          icon="participants-min.png"
          nb={basicStats.volunteers}
          label="participants"
        />
        <Stack
          direction={{
            xs: "column",
            sm: "row",
            md: "column",
            lg: "row",
          }}
          spacing={2}
          sx={{ mt: 2, justifyContent: "space-between", maxWidth: "440px" }}
        >
          <FocusItem
            icon="megots-min.png"
            nb={basicStats.butts}
            label="mégots"
          />
          <FocusItem
            icon="litres_preserves-min.png"
            nb={basicStats.liters}
            unit=" L"
            label="d'eau préservés**"
          />
        </Stack>
      </Box>

      <Typography variant="body2" sx={{ mt: 2 }}>
        *Éléments observés qui peuvent avoir une influence sur la présence de
        mégots au sol
      </Typography>
      <Typography variant="body2" sx={{ mt: 2 }}>
        **1 mégot = 500 L d’eau polluée. Dans l'éventualité que l'ensemble des
        mégots aient fini leur course dans l'eau.
      </Typography>
    </Box>
  );
};
