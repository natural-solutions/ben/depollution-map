import { useEffect, useState } from "react";
import Dialog from "@mui/material/Dialog";
import IconButton from "@mui/material/IconButton";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import CloseIcon from "@mui/icons-material/Close";
import {
  Avatar,
  Box,
  Button,
  Stack,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { IMAGOR_PRESET_NAME, getTransformationURL } from "@/utils/media";
import { HEADER_HEIGHT_PX } from "@/utils/theme";

interface PhotoCarouselProps {
  allPhotos: string[];
  filename: string;
  onClose: () => void;
  cleanupId: string;
}

export const CleanupPhotos = ({
  allPhotos,
  filename,
  onClose,
  cleanupId,
}: PhotoCarouselProps) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const initialPhotoIndex = allPhotos.findIndex((photo) => photo === filename);
  const [currentPhotoIndex, setCurrentPhotoIndex] = useState(initialPhotoIndex);

  useEffect(() => {
    const currentPhoto = allPhotos[currentPhotoIndex];
    const index = allPhotos.findIndex((photo) => photo === currentPhoto);
    setCurrentPhotoIndex(index);
  }, [allPhotos, currentPhotoIndex]);

  const handlePrevious = () => {
    setCurrentPhotoIndex(
      (currentPhotoIndex - 1 + allPhotos.length) % allPhotos.length
    );
  };

  const handleNext = () => {
    setCurrentPhotoIndex((currentPhotoIndex + 1) % allPhotos.length);
  };

  return (
    <Dialog
      fullScreen
      open={true}
      onClose={onClose}
      PaperProps={{
        square: true,
      }}
    >
      <Stack direction="row" sx={{ pt: 1, px: 1 }}>
        <Button
          color="primary"
          variant="contained"
          onClick={onClose}
          endIcon={<CloseIcon />}
          sx={{ ml: "auto" }}
        >
          Fermer
        </Button>
      </Stack>
      {isMobile ? (
        <>
          <Stack
            direction={"column"}
            spacing={2}
            sx={{
              justifyContent: "space-around",
              minHeight: "calc(100vh - 60px)",
            }}
          >
            <Box
              sx={{
                width: "100%",
                height: "90vh",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Avatar
                sx={{
                  width: "100%",
                  height: "max-content",
                  maxWidth: "100vw",
                  maxHeight: `calc(100vh - ${HEADER_HEIGHT_PX})`,
                  objectFit: "contain",
                }}
                variant="square"
                src={getTransformationURL(
                  "cleanups",
                  cleanupId,
                  allPhotos[currentPhotoIndex],
                  IMAGOR_PRESET_NAME["800x600"]
                )}
                alt={allPhotos[currentPhotoIndex]}
              />
            </Box>
            <Box
              sx={{
                width: "100%",
                height: "auto",
                display: "flex",
                px: 2,
                justifyContent: "space-between",
              }}
            >
              <IconButton
                edge="start"
                color="inherit"
                onClick={handlePrevious}
                aria-label="previous photo"
                sx={{ width: "auto", height: "auto" }}
              >
                <ArrowBackIcon />
              </IconButton>
              <IconButton
                edge="start"
                color="inherit"
                onClick={handleNext}
                aria-label="next photo"
              >
                <ArrowForwardIcon />
              </IconButton>
            </Box>
          </Stack>
        </>
      ) : (
        <Stack
          direction="row"
          spacing={2}
          sx={{
            p: 1,
            maxWidth: "100vw",
            maxHeight: "100vh",
            overflow: "auto",
            width: "100vw",
            height: "100vh",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <IconButton
            edge="start"
            color="inherit"
            onClick={handlePrevious}
            aria-label="previous photo"
            sx={{ width: "auto", height: "auto" }}
          >
            <ArrowBackIcon />
          </IconButton>
          <Avatar
            sx={{
              width: "auto",
              height: "auto",
              maxWidth: "calc(100vw - Xpx)",
              maxHeight: "calc(100vh - Ypx)",
              objectFit: "contain",
            }}
            variant="square"
            src={getTransformationURL(
              "cleanups",
              cleanupId,
              allPhotos[currentPhotoIndex],
              IMAGOR_PRESET_NAME["800x600"]
            )}
            alt={allPhotos[currentPhotoIndex]}
          />
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleNext}
            aria-label="next photo"
          >
            <ArrowForwardIcon />
          </IconButton>
        </Stack>
      )}
    </Dialog>
  );
};
