import { Close } from "@mui/icons-material";
import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  useTheme,
} from "@mui/material";
import React, { FC, useEffect, useState } from "react";
import { CleanupHistoricalToDisplay } from "./CleanupHistorical";
import { Cleanup_View } from "@/graphql/types";
import { useApi } from "@/utils/useApi";
import { GET_CLEANUPS_FOR_HISTORY } from "@/graphql/queries";

type CleanupDialogHistoryProps = {
  placeId?: string;
  isOpen: boolean;
  setIsOpen: (value: boolean) => void;
};

const PAGE_SIZE = 100;

export const CleanupDialogHistory: FC<CleanupDialogHistoryProps> = ({
  placeId,
  isOpen,
  setIsOpen,
}) => {
  const api = useApi();
  const theme = useTheme();
  const [pageIndex, setPageIndex] = useState(0);
  const [total, setTotal] = useState(0);
  const [rows, setRows] = useState<Cleanup_View[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!placeId || !isOpen) {
      return;
    }
    (async () => {
      setIsLoading(true);
      const resp = await api.postGraphql<{
        data: {
          cleanup_view: Cleanup_View[];
          total: {
            aggregate: {
              count: number;
            };
          };
        };
      }>({
        query: GET_CLEANUPS_FOR_HISTORY,
        variables: {
          where: {
            cleanup_place_id: {
              _eq: placeId,
            },
          },
          limit: PAGE_SIZE,
          offset: PAGE_SIZE * pageIndex,
        },
      });
      setIsLoading(false);
      const newRows = resp.data?.data?.cleanup_view || [];
      setRows(newRows);
      setTotal(resp.data?.data?.total?.aggregate?.count || 0);
  
    })();
  }, [placeId, isOpen, pageIndex]);

  const handlePageChange = (direction: "next" | "previous") => {
    if (direction === "next") {
      setPageIndex((prevPageIndex) => prevPageIndex + 1);
    } else {
      setPageIndex((prevPageIndex) => Math.max(prevPageIndex - 1, 0));
    }
  };

  return (
    <Dialog open={isOpen} onClose={() => setIsOpen(false)}>
      <DialogTitle fontSize={15} variant="h6">
        Historique de ramassage complet
        <IconButton
          edge="end"
          onClick={() => setIsOpen(false)}
          sx={{
            position: "absolute",
            right: theme.spacing(3),
            top: theme.spacing(1),
          }}
        >
          <Close />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={{ pl: 1 }}>
        {isLoading && <CircularProgress />}
        <CleanupHistoricalToDisplay
          cleanupHistorical={rows}
          isCategorized={true}
        />
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            width: "100%",
            alignItems: "center",
          }}
        >
          <Button
            type="button"
            disabled={pageIndex === 0}
            onClick={() => handlePageChange("previous")}
          >
            Page Precedente
          </Button>
          <Button
            type="button"
            disabled={rows.length < PAGE_SIZE}
            onClick={() => handlePageChange("next")}
          >
            Page Suivante
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};
