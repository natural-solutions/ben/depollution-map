import { Badge, Box, styled, Tab } from "@mui/material";

export const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    backgroundColor: "#14363F99",
    color: "white",
    padding: theme.spacing(1),
  },
}));
