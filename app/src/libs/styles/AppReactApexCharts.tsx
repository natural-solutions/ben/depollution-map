"use client";

import Box from "@mui/material/Box";
import { styled } from "@mui/material/styles";
import type { BoxProps } from "@mui/material/Box";
import type { Props } from "react-apexcharts";
import ReactApexcharts from "@/libs/ApexCharts";

type ApexChartWrapperProps = Props & {
  boxProps?: BoxProps;
};

const ApexChartWrapper = styled(Box)<BoxProps>(({ theme }) => ({
  "& .apexcharts-canvas": {
    /* "& line[stroke='transparent']": {
      display: "none",
    }, */
    "& .apexcharts-tooltip": {
      borderColor: theme.palette.divider,
      background: theme.palette.background.paper,
      "& .apexcharts-tooltip-title": {
        fontWeight: 600,
        borderColor: theme.palette.divider,
        background: theme.palette.background.paper,
      },
      "&.apexcharts-theme-light": {
        color: theme.palette.text.primary,
      },
      "&.apexcharts-theme-dark": {
        color: theme.palette.common.white,
      },
      "& .apexcharts-tooltip-series-group:first-of-type": {
        paddingBottom: 0,
      },
      "& .bar-chart": {
        padding: theme.spacing(2, 2.5),
      },
    },
    "& .apexcharts-xaxistooltip": {
      borderColor: theme.palette.divider,

      background: theme.palette.mode === theme.palette.grey[50],

      "&:after": {
        borderBottomColor: theme.palette.mode === theme.palette.grey[50],
      },
      "&:before": {
        borderBottomColor: theme.palette.divider,
      },
    },
    "& .apexcharts-yaxistooltip": {
      borderColor: theme.palette.divider,

      background: theme.palette.mode === theme.palette.grey[50],
      "&:after": {
        borderLeftColor: theme.palette.mode === theme.palette.grey[50],
      },
      "&:before": {
        borderLeftColor: theme.palette.divider,
      },
    },
    "& .apexcharts-xaxistooltip-text, & .apexcharts-yaxistooltip-text": {
      color: theme.palette.text.primary,
    },
    "& .apexcharts-yaxis .apexcharts-yaxis-texts-g .apexcharts-yaxis-label": {
      textAnchor: theme.direction === "rtl" ? "start" : undefined,
    },
    "& .apexcharts-text, & .apexcharts-tooltip-text, & .apexcharts-datalabel-label, & .apexcharts-datalabel, & .apexcharts-xaxistooltip-text, & .apexcharts-yaxistooltip-text, & .apexcharts-legend-text":
      {
        fontFamily: `${theme.typography.fontFamily} !important`,
      },
    "& .apexcharts-pie-label": {
      filter: "none",
    },
    "& .apexcharts-marker": {
      boxShadow: "none",
    },
  },
}));

const AppReactApexCharts = (props: ApexChartWrapperProps) => {
  const { boxProps, ...rest } = props;

  return (
    <ApexChartWrapper {...boxProps}>
      <ReactApexcharts {...rest} />
    </ApexChartWrapper>
  );
};

export default AppReactApexCharts;
