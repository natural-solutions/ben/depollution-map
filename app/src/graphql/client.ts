import { User_View } from "./types";

export const dangerousPostAsAdmin = async (
  body: {
    operationName?: string;
    query: string;
    variables?: any;
  },
  responseType?: "json" | "blob"
) => {
  const resp = await fetch("http://hasura:8080/v1/graphql", {
    method: "POST",
    headers: {
      "x-hasura-admin-secret": process.env.HASURA_GRAPHQL_ADMIN_SECRET!,
      "x-hasura-use-backend-only-permissions": "true",
    },
    body: JSON.stringify(body),
  });

  return responseType == "blob" ? await resp.blob() : await resp.json();
};

export const postAsUser = async (
  body: {
    operationName?: string;
    query: string;
    variables?: any;
  },
  user?: User_View
) => {
  const userRole = user?.role_name;
  const userId = user?.id;

  const resp = await fetch("http://hasura:8080/v1/graphql", {
    method: "POST",
    headers: !userRole
      ? undefined
      : {
          "x-hasura-admin-secret": process.env.HASURA_GRAPHQL_ADMIN_SECRET!,
          "x-hasura-role": userRole,
          ...(userId ? { "x-hasura-user-id": userId } : undefined),
        },
    body: JSON.stringify(body),
  });

  return await resp.json();
};
