import { FetchType } from "ra-data-hasura";

export enum FETCH_TYPE {
  GET_BASE = "GET_BASE",
  GET_ONE = FetchType.GET_ONE,
  GET_LIST = FetchType.GET_LIST,
  GET_MANY = FetchType.GET_MANY,
  UPDATE_COLUMNS = "UPDATE_COLUMNS",
}

export enum RESOURCE_NAME {
  AREA_TYPE = "area_type",
  ENVIRONMENT_TYPE = "environment_type",
  ENVIRONMENT_AREA_TYPE = "environment_area_type",
  BRAND = "brand",
  CAMPAIGN = "campaign",
  CAMPAIGN_USER = "campaign_user",
  CHARACTERIZATION_LEVEL = "characterization_level",
  CLEANUP = "cleanup",
  CLEANUP_AREA = "cleanup_area",
  CLEANUP_FORM = "cleanup_form",
  CLEANUP_FORM_RUBBISH = "cleanup_form_rubbish",
  CLEANUP_FORM_RUBBISH_DETAIL = "cleanup_form_rubbish_detail",
  CLEANUP_FORM_POI = "cleanup_form_poi",
  CLEANUP_FORM_DOI = "cleanup_form_doi",
  CLEANUP_CAMPAIGN = "cleanup_campaign",
  CLEANUP_PARTNER = "cleanup_partner",
  CLEANUP_PHOTO = "cleanup_photo",
  CLEANUP_TYPE = "cleanup_type",
  CLEANUP_PLACE = "cleanup_place",
  CITY = "city",
  DEPARTEMENT = "departement",
  REGION = "region",
  PARTNER = "partner",
  POI_TYPE = "poi_type",
  RESOURCE = "resource",
  RUBBISH = "rubbish",
  RUBBISH_CATEGORY = "rubbish_category",
  CLEANUP_TYPE_RUBBISH = "cleanup_type_rubbish",
  RUBBISH_RUBBISH_TAG = "rubbish_rubbish_tag",
  RUBBISH_TAG = "rubbish_tag",
  USER_VIEW = "user_view",
}

export const GRAPHQL_RESOURCES: {
  [resourceName in RESOURCE_NAME]: {
    base: string[];
  } & {
    [key in FETCH_TYPE | string]?: string[];
  };
} = {
  brand: {
    base: ["id", "created_at", "updated_at", "label"],
    GET_MANY: ["id", "label"],
  },
  user_view: {
    base: [
      "id",
      "username",
      "email",
      "first_name",
      "last_name",
      "fullname",
      "role_id",
      "role_name",
    ],
    GET_ONE: ["campaign_users:campaign_user.GET_FOR_ONE_USER_VIEW"],
    GET_MANY: ["id", "fullname"],
  },
  partner: {
    base: ["id", "created_at", "updated_at", "label", "logo"],
    GET_ONE: ["cleanup_partners:cleanup_partner.GET_FOR_ONE_PARTNER"],
    GET_MANY: ["id", "label"],
  },
  characterization_level: {
    base: ["id", "created_at", "updated_at", "label"],
  },
  cleanup_type: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "label",
      "characterization_level_id",
    ],
    GET_ONE: [
      "cleanup_type_rubbishes(order_by: {rubbish: {label: asc}}):cleanup_type_rubbish.GET_FOR_ONE_CLEANUP_TYPE",
      "characterization_level:GET_ONE",
    ],
    GET_ONE_SHORT: [
      "cleanup_type_rubbishes(order_by: {rubbish: {label: asc}}):cleanup_type_rubbish.GET_FOR_ONE_SHORT_CLEANUP_TYPE",
      "characterization_level:GET_ONE",
    ],
    GET_LIST: ["characterization_level:GET_MANY"],
    GET_MANY: ["id", "label", "characterization_level_id"],
  },
  region: {
    base: ["code", "label", "slug"],
  },
  departement: {
    base: ["id", "label", "region_id", "region:GET_MANY"],
    GET_MANY: ["id", "label"],
  },
  city: {
    base: ["id", "label", "departement_id", "code", "coords", "slug"],
    GET_ONE: ["departement:GET_ONE"],
    GET_MANY: ["id", "label", "departement_id"],
  },
  environment_type: {
    base: ["id", "label"],
    GET_ONE: [
      "environment_area_types:environment_area_type.GET_FOR_ENVIRONMENT_TYPE",
    ],
    GET_LIST: [
      "environment_area_types:environment_area_type.GET_FOR_ENVIRONMENT_TYPE",
    ],
  },
  area_type: {
    base: ["id", "label"],
    GET_ONE: ["environment_area_types:environment_area_type.GET_FOR_AREA_TYPE"],
    GET_LIST: [
      "environment_area_types:environment_area_type.GET_FOR_AREA_TYPE",
    ],
  },
  environment_area_type: {
    base: ["area_type_id", "environment_type_id", "is_linear", "is_area"],
    GET_FOR_ENVIRONMENT_TYPE: ["area_type_id", "area_type:GET_MANY"],
    GET_FOR_AREA_TYPE: ["environment_type_id", "environment_type:GET_MANY"],
  },
  cleanup_partner: {
    base: ["cleanup_id", "partner_id"],
    GET_FOR_ONE_PARTNER: ["cleanup_id", "cleanup:GET_ONE"],
    GET_FOR_ONE_CLEANUP: ["partner_id", "partner:GET_ONE"],
    GET_FOR_LIST_CLEANUP: ["partner_id", "partner:GET_MANY"],
  },
  cleanup_place: {
    base: ["id", "created_at", "updated_at", "label", "city_id"],
    GET_MANY: ["id", "label"],
  },
  campaign: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "label",
      "logo",
      "email",
      "main_color",
      "default_cleanup_type_id",
    ],
    GET_ONE: [
      "description",
      "cleanup_campaigns:cleanup_campaign.GET_FOR_ONE_CAMPAIGN",
      "campaign_users:campaign_user.GET_FOR_ONE_CAMPAIGN",
    ],
    GET_FOR_CLEANUP_CAMPAIGN: [
      "cleanup_campaigns:cleanup_campaign.GET_FOR_LIST_CAMPAIGN",
    ],
    GET_LIST: [
      "default_cleanup_type:cleanup_type.GET_MANY",
      "campaign_users:campaign_user.GET_FOR_LIST_CAMPAIGN",
    ],
    GET_MANY: ["id", "label"],
  },
  campaign_user: {
    base: ["campaign_id", "user_view_id", "role"],
    GET_FOR_ONE_CAMPAIGN: ["user_view:GET_BASE"],
    GET_FOR_LIST_CAMPAIGN: ["user_view:GET_BASE"],
    GET_FOR_ONE_USER_VIEW: ["campaign:GET_BASE"],
  },
  cleanup_campaign: {
    base: ["cleanup_id", "campaign_id"],
    GET_FOR_ONE_CAMPAIGN: ["cleanup_id", "cleanup:GET_ONE"],
    GET_FOR_ONE_CLEANUP: ["campaign_id", "campaign:GET_FOR_CLEANUP_CAMPAIGN"],
    GET_FOR_LIST_CAMPAIGN: ["cleanup_id", "cleanup:GET_LIST"],
    GET_FOR_LIST_CLEANUP: ["campaign_id", "campaign:GET_LIST"],
    GET_FOR_CLEANUPS_FOR_APP: ["campaign_id", "campaign:GET_BASE"],
  },
  cleanup: {
    base: [
      "id",
      "label",
      "total_area",
      "total_linear",
      "total_weight",
      "total_volume",
      "start_at",
      "start_point",
      "city_id",
      "environment_type_id",
      "area_type_id",
      "cleanup_type_id",
      "cleanup_place_id",
      "owner_id",
    ],
    GET_ONE: [
      "cleanup_campaigns:cleanup_campaign.GET_FOR_ONE_CLEANUP",
      "cleanup_type:GET_ONE_SHORT",
      "cleanup_partners:cleanup_partner.GET_FOR_ONE_CLEANUP",
      "city:GET_ONE",
      "cleanup_areas(order_by: {label: asc}):cleanup_area.GET_ONE",
      "cleanup_photos(order_by: {rank: asc}):cleanup_photo.GET_ONE",
      "environment_type:GET_BASE",
      "area_type:GET_BASE",
      "cleanup_forms(order_by: {created_at: asc}):cleanup_form.GET_ONE",
      "cleanup_type:GET_BASE",
      "cleanup_place:GET_BASE",
      "description",
    ],
    GET_ONE_SHORT: [
      "cleanup_campaigns:cleanup_campaign.GET_FOR_LIST_CLEANUP",
      "cleanup_type:GET_ONE_SHORT",
      "cleanup_partners:cleanup_partner.GET_FOR_LIST_CLEANUP",
      "city:GET_ONE",
      "cleanup_areas(order_by: {label: asc}):cleanup_area.GET_ONE",
      "cleanup_photos(order_by: {rank: asc}):cleanup_photo.GET_ONE",
      "environment_type:GET_BASE",
      "area_type:GET_BASE",
      "cleanup_place:GET_BASE",
    ],
    GET_LIST: [
      `cleanup_campaigns:cleanup_campaign.GET_FOR_LIST_CLEANUP`,
      `cleanup_forms:cleanup_form.GET_LIST`,
      "cleanup_type:GET_LIST",
      "cleanup_partners:cleanup_partner.GET_FOR_LIST_CLEANUP",
      "city:GET_ONE",
      "environment_type:GET_BASE",
      "area_type:GET_BASE",
      "cleanup_place:GET_BASE",
      "cleanup_photos:cleanup_photo.GET_MANY",
      "cleanup_forms:cleanup_form.GET_BASE",
      "created_at",
      "updated_at",
    ],
    GET_LIST_FOR_APP: [
      `cleanup_campaigns:cleanup_campaign.GET_FOR_CLEANUPS_FOR_APP`,
      `cleanup_forms:cleanup_form.GET_FOR_CLEANUPS_FOR_APP`,
      "cleanup_type:GET_LIST",
      "cleanup_partners:cleanup_partner.GET_FOR_LIST_CLEANUP",
      "city:GET_ONE",
      "cleanup_place:GET_BASE",
      "cleanup_photos:cleanup_photo.GET_MANY",
      "cleanup_forms:cleanup_form.GET_BASE",
      "created_at",
      "updated_at",
    ],
    GET_MANY: ["id", "label"],
  },
  cleanup_area: {
    base: ["id", "label", "fill_color", "size", "cleanup_id", "geom"],
    GET_ONE: ["geom"],
    GET_MANY: ["id", "label", "fill_color"],
    UPDATE_COLUMNS: ["cleanup_id", "fill_color", "geom", "label", "size"],
  },
  cleanup_photo: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "filename",
      "cleanup_area_id",
      "cleanup_area:GET_MANY",
    ],
    GET_MANY: ["id", "filename"],
    UPDATE_COLUMNS: [
      "cleanup_id",
      "filename",
      "comment",
      "cleanup_area_id",
      "rank",
    ],
  },
  cleanup_form: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "performed_at",
      "volunteers_number",
      "external_volunteers_number",
      "status",
      "cleanup_id",
      "rain_force",
      "wind_force",
      "owner_id",
      "rubbish_type_picked_id",
      "cleanup_duration",
    ],
    GET_ONE: [
      "cleanup:GET_ONE_SHORT",
      "cleanup_form_rubbishes(order_by: [{rubbish: {label: asc}}, {created_at: desc}]):cleanup_form_rubbish.GET_LIST",
      "cleanup_form_rubbish_details(distinct_on: [rubbish_id,cleanup_area_id,brand_id], order_by: [{rubbish_id: asc,cleanup_area_id:asc,brand_id:asc}, {updated_at: asc}]):cleanup_form_rubbish_detail.GET_LIST",
      "cleanup_form_pois(distinct_on: [poi_type_id,cleanup_area_id], order_by: [{poi_type_id: asc,cleanup_area_id:asc}, {updated_at: asc}]):cleanup_form_poi.GET_LIST",
      "cleanup_form_dois(order_by: [{rubbish: {label: asc}}]):cleanup_form_doi.GET_LIST",
      "comment",
    ],
    GET_LIST: ["cleanup:GET_MANY"],
    GET_MANY: ["id", "cleanup:GET_MANY", "performed_at"],
    GET_FOR_CLEANUPS_FOR_APP: [
      'cleanup_form_rubbishes(where:{rubbish_id: {_eq: "butts"}}, distinct_on: [rubbish_id], order_by: [{rubbish_id: asc}, {created_at: desc}]):cleanup_form_rubbish.GET_BASE',
    ],
  },
  cleanup_form_rubbish: {
    base: [
      "id",
      "label",
      "comment",
      "volume",
      "quantity",
      "weight",
      "rubbish_id",
    ],
    GET_ONE: ["brand:GET_ONE", "rubbish:GET_ONE", "cleanup_area:GET_MANY"],
    GET_LIST: ["rubbish:GET_BASE_WITH_CATEGORY"],
    UPDATE_COLUMNS: [
      "rubbish_id",
      "label",
      "quantity",
      "weight",
      "volume",
      "comment",
    ],
  },
  cleanup_form_rubbish_detail: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "cleanup_form_id",
      "rubbish_id",
      "quantity",
      "weight",
      "volume",
      "brand_id",
      "cleanup_area_id",
    ],
    GET_LIST: ["rubbish:GET_BASE_WITH_CATEGORY", "cleanup_area:GET_MANY"],
    UPDATE_COLUMNS: [
      "cleanup_area_id",
      "rubbish_id",
      "quantity",
      "weight",
      "volume",
      "brand_id",
    ],
  },
  cleanup_form_poi: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "poi_type_id",
      "cleanup_form_id",
      "cleanup_area_id",
      "nb",
    ],
    GET_LIST: ["cleanup_area:GET_MANY", "poi_type:GET_MANY"],
    UPDATE_COLUMNS: ["cleanup_area_id", "poi_type_id", "nb"],
  },
  cleanup_form_doi: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "cleanup_form_id",
      "rubbish_id",
      "expiration",
    ],
    GET_LIST: ["rubbish:GET_MANY"],
    UPDATE_COLUMNS: ["rubbish_id", "expiration"],
  },
  rubbish_category: {
    base: ["id", "created_at", "updated_at", "label", "color", "rank"],
    GET_ONE: ["rubbishes:rubbish.GET_ONE"],
    GET_MANY: ["id", "label"],
  },
  rubbish: {
    base: [
      "id",
      "id_zds",
      "label",
      "created_at",
      "updated_at",
      "icon",
      "parent_id",
      "rubbish_category_id",
      "is_support_brand",
      "is_support_doi",
    ],
    GET_ONE: [
      "comment",
      "cleanup_type_rubbishes:cleanup_type_rubbish.GET_FOR_ONE_RUBBISH",
      "rubbish_category:GET_ONE",
      "tags:rubbish_rubbish_tag.GET_FOR_ONE_RUBBISH",
    ],
    GET_FOR_CLEANUP_TYPE_RUBBISH: [
      "comment",
      "rubbish_category:GET_MANY",
      "tags:rubbish_rubbish_tag.GET_FOR_LIST_RUBBISH",
    ],
    GET_LIST: [
      "rubbish_category:GET_MANY",
      "cleanup_type_rubbishes:cleanup_type_rubbish.GET_FOR_LIST_RUBBISH",
      "tags:rubbish_rubbish_tag.GET_FOR_LIST_RUBBISH",
    ],
    GET_MANY: ["id", "label"],
    GET_BASE_WITH_CATEGORY: ["rubbish_category:GET_MANY"],
  },
  cleanup_type_rubbish: {
    base: ["rubbish_id", "cleanup_type_id", "has_qty", "has_wt_vol"],
    GET_FOR_ONE_CLEANUP_TYPE: ["rubbish:GET_ONE"],
    GET_FOR_ONE_SHORT_CLEANUP_TYPE: ["rubbish:GET_FOR_CLEANUP_TYPE_RUBBISH"],
    GET_FOR_ONE_RUBBISH: [
      "has_qty",
      "has_wt_vol",
      "cleanup_type_id",
      "cleanup_type:GET_ONE",
    ],
    GET_FOR_LIST_RUBBISH: [
      "has_qty",
      "has_wt_vol",
      "cleanup_type_id",
      "cleanup_type:GET_LIST",
    ],
  },
  rubbish_rubbish_tag: {
    base: ["rubbish_id", "rubbish_tag_id"],
    GET_FOR_ONE_RUBBISH: ["rubbish_tag_id", "rubbish_tag:GET_ONE"],
    GET_FOR_LIST_RUBBISH: ["rubbish_tag_id", "rubbish_tag:GET_LIST"],
    GET_FOR_ONE_RUBBISH_TAG: ["rubbish_id", "rubbish:GET_ONE"],
    GET_FOR_LIST_RUBBISH_TAG: ["rubbish_id", "rubbish:GET_LIST"],
  },
  rubbish_tag: {
    base: ["id", "created_at", "updated_at", "label"],
    GET_ONE: ["rubbishes:rubbish_rubbish_tag.GET_FOR_ONE_RUBBISH_TAG"],
    GET_MANY: ["id", "label"],
  },
  poi_type: {
    base: ["id", "created_at", "updated_at", "label", "logo"],
    GET_MANY: ["id", "label"],
  },
  resource: {
    base: [
      "id",
      "created_at",
      "updated_at",
      "label",
      "filename",
      "description",
    ],
    GET_MANY: ["id", "label"],
  },
};

const getFieldConf = (field: string) => {
  const params = field.match(/\(.*\)/g)?.[0];
  if (params) {
    field = field.replace(params, "");
  }
  const [fieldName, target] = field.split(":");
  if (!target) {
    return { fieldName };
  }
  let [resource, fetchType] = target.split(".");
  if (!fetchType) {
    fetchType = resource;
    resource = fieldName;
  }
  return {
    fieldName,
    resource,
    fetchType,
    params,
  };
};

export const getFields = (
  resource: RESOURCE_NAME,
  fetchType: FETCH_TYPE,
  visiteds: any = {}
): string => {
  const resourceConf = GRAPHQL_RESOURCES[resource];
  if (!resourceConf) {
    return "";
  }
  let fields = resourceConf[fetchType];
  if (!fields) {
    fields = resourceConf[fetchType] = [];
  }
  if (resourceConf.base) {
    fields = fields.concat(resourceConf.base);
  }

  const visited = visiteds[`${resource}.${fetchType}`] || 0;
  if (fetchType != FETCH_TYPE.GET_BASE) {
    visiteds[`${resource}.${fetchType}`] = visited + 1;
  }

  const result = fields
    .map((field) => {
      const conf = getFieldConf(field);
      if (conf.resource && visiteds[`${resource}.${fetchType}`] > 1) {
        conf.fetchType = FETCH_TYPE.GET_BASE;
      }
      return conf;
    })
    .map((conf) => {
      const { fieldName, resource, fetchType, params } = conf;
      return !resource
        ? fieldName
        : `${fieldName}${params || ""} {\n ${getFields(
            resource as any,
            fetchType as any,
            visiteds
          )}\n}`;
    })
    .join("\n");

  return result;
};
