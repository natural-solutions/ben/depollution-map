import {
  FETCH_TYPE,
  GRAPHQL_RESOURCES,
  RESOURCE_NAME,
  getFields,
} from "./resources";
import {
  Cleanup_Area,
  Cleanup_Campaign,
  Cleanup_Form_Doi,
  Cleanup_Form_Poi,
  Cleanup_Form_Rubbish,
  Cleanup_Form_Rubbish_Detail,
  Cleanup_Form_Set_Input,
  Cleanup_Partner,
  Cleanup_Photo,
  Cleanup_Set_Input,
  Cleanup_Type_Rubbish,
  Rubbish_Rubbish_Tag,
  Rubbish_Set_Input,
  User_View,
} from "./types";

export type CleanupPatch = Cleanup_Set_Input & {
  cleanup_areas_delete_ids?: Cleanup_Area["id"][];
  cleanup_areas?: Cleanup_Area[];
  cleanup_photos_delete_ids?: Cleanup_Photo["id"][];
  cleanup_photos?: Cleanup_Photo[];
  cleanup_campaigns?: Cleanup_Campaign[];
  cleanup_partners?: Cleanup_Partner[];
};

export type CleanupFormPatch = Cleanup_Form_Set_Input & {
  cleanup_form_rubbishes_delete_ids?: Cleanup_Form_Rubbish["id"][];
  cleanup_form_rubbishes?: Cleanup_Form_Rubbish[];
  cleanup_form_rubbish_details_delete_ids?: Cleanup_Form_Rubbish_Detail["id"][];
  cleanup_form_rubbish_details?: Partial<Cleanup_Form_Rubbish_Detail>[];
  cleanup_form_pois_delete_ids?: Cleanup_Form_Poi["id"][];
  cleanup_form_pois?: Partial<Cleanup_Form_Poi>[];
  cleanup_form_dois_delete_ids?: Cleanup_Form_Doi["id"][];
  cleanup_form_dois?: Partial<Cleanup_Form_Doi>[];
};

export type RubbishPatch = Rubbish_Set_Input & {
  tags?: Rubbish_Rubbish_Tag[];
  cleanup_type_rubbishes?: Cleanup_Type_Rubbish[];
};

export type UserFormData = User_View & {
  password?: string;
};

export const GET_USER = `query GetUser($id: String = "") {
  user_view(where: {id: {_eq: $id}}, limit: 1) {
    ${getFields(RESOURCE_NAME.USER_VIEW, FETCH_TYPE.GET_ONE)}
  }
}`;

export const GET_CAMPAIGNS = `query GetCampaigns($where: campaign_bool_exp = {}, $order_by: [campaign_order_by!] = {}, $limit: Int, $offset: Int) {
  campaign(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
    ${getFields(RESOURCE_NAME.CAMPAIGN, FETCH_TYPE.GET_MANY)}
  }
}`;

export const GET_CAMPAIGN = `query GetCampaign($id: String = "") {
    campaign_by_pk(id: $id) {
      ${getFields(RESOURCE_NAME.CAMPAIGN, FETCH_TYPE.GET_ONE)}
    }
  }`;

export const INSERT_CAMPAIGN = `
  mutation InsertCampaign($_set: campaign_insert_input = {}) {
    insert_campaign_one(object: $_set) {
      ${getFields(RESOURCE_NAME.CAMPAIGN, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_CAMPAIGN = `
  mutation UpdateCampaign($id: String = "", $_set: campaign_set_input = {}) {
    update_campaign_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.CAMPAIGN, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_USER_CAMPAIGNS = `
  mutation UpdateUserCampaigns($id: String = "", $objects: [campaign_user_insert_input!] = {}) {
    delete_campaign_user(where: {user_view_id: {_eq: $id}}) {
      affected_rows
    }
    insert_campaign_user(objects: $objects) {
      affected_rows
    }
  }`;

export const UPDATE_CAMPAIGN_USERS = `
  mutation UpdateCampaignUsers($id: String = "", $objects: [campaign_user_insert_input!] = {}) {
    delete_campaign_user(where: {campaign_id: {_eq: $id}}) {
      affected_rows
    }
    insert_campaign_user(objects: $objects) {
      affected_rows
    }
  }`;

export const GET_CLEANUPS = `query GetCleanups($where: cleanup_bool_exp = {}, $order_by: [cleanup_order_by!] = {}, $limit: Int, $offset: Int, $distinct_on: [cleanup_select_column!]) {
  cleanup(where: $where, order_by: $order_by, limit: $limit, offset: $offset, distinct_on: $distinct_on) {
    ${getFields(RESOURCE_NAME.CLEANUP, FETCH_TYPE.GET_LIST)}
  }
}`;

export const GET_CLEANUPS_FOR_APP = `query GetCleanupsForApp($where: cleanup_view_bool_exp = {}, $order_by: [cleanup_view_order_by!] = {}, $limit: Int, $offset: Int, $distinct_on: [cleanup_view_select_column!]) {
  cleanup_view(where: $where, order_by: $order_by, limit: $limit, offset: $offset, distinct_on: $distinct_on) {
    id
    label
    total_weight
    start_at
    start_point
    total_butts
    cleanup_place_id
    owner_id
    cleanup_place {
      label
    }
    cleanup_forms(limit:1){
      id
      owner_id
      status
    }
    cleanup_campaigns {
      campaign_id
      campaign {
        id
        label
        main_color
      }
    }
    cleanup_type_id
    cleanup_type {
      id
      characterization_level_id
    }
    cleanup_partners {
      partner_id
    }
    city_id
    city {
      id
      label
      departement {
        id
        label
        region {
          slug
          label
        }
      }
    }
    cleanup_photo {
      filename
    }
  }
}`;

export const GET_CLEANUPS_FOR_HISTORY = `query GetCleanupsForHistory($where: cleanup_view_bool_exp = {}, $order_by: [cleanup_view_order_by!] = {}, $limit: Int, $offset: Int, $distinct_on: [cleanup_view_select_column!]) {
  cleanup_view(where: $where, order_by: $order_by, limit: $limit, offset: $offset, distinct_on: $distinct_on) {
    id
    label
    start_at
    total_weight
    total_butts
  }
  total: cleanup_view_aggregate(where: $where) {
    aggregate {
      count
    }
  }
}`;

export const GET_CLEANUP_IDS = `query GetCleanupIds($where: cleanup_bool_exp = {}, $order_by: [cleanup_order_by!] = {}, $limit: Int, $offset: Int, $distinct_on: [cleanup_select_column!]) {
  cleanup(where: $where, order_by: $order_by, limit: $limit, offset: $offset, distinct_on: $distinct_on) {
    id
  }
}`;

export const GET_CLEANUP = `query GetCleanup($id: uuid = {}) {
  cleanup_by_pk(id: $id) {
    ${getFields(RESOURCE_NAME.CLEANUP, FETCH_TYPE.GET_ONE)}
  }
}`;

export const INSERT_CLEANUP = `
  mutation InsertCleanup($_set: cleanup_insert_input = {}) {
    insert_cleanup_one(object: $_set) {
      ${getFields(RESOURCE_NAME.CLEANUP, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_CLEANUP = `
  mutation UpdateCleanup($id: uuid = "", $_set: cleanup_set_input = {}) {
    update_cleanup_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.CLEANUP, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const GET_CLEANUP_CITIES = `query GetCleanupCities($where: city_bool_exp = {}) {
  city(order_by: {label: asc}, where: $where) {
    id
    label
    departement {
      id
      label
      region {
        label
        slug
      }
    }
  }
}`;

export const GET_CLEANUP_TYPE = `query GetCleanupType($id: String = "") {
  cleanup_type_by_pk(id: $id) {
    ${getFields(RESOURCE_NAME.CLEANUP_TYPE, FETCH_TYPE.GET_ONE)}
  }
}`;

export const GET_CLEANUP_TYPES = `query GetCleanupTypes($where: cleanup_type_bool_exp = {}, $order_by: [cleanup_type_order_by!] = {}, $limit: Int, $offset: Int) {
    cleanup_type(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
      ${getFields(RESOURCE_NAME.CLEANUP_TYPE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const INSERT_CLEANUP_TYPE = `mutation InsertCleanupType($_set: cleanup_type_insert_input = {}) {
  insert_cleanup_type_one(object: $_set) {
    ${getFields(RESOURCE_NAME.CLEANUP_TYPE, FETCH_TYPE.GET_MANY)}
  }
}`;

export const UPDATE_CLEANUP_TYPE = `mutation UpdateCleanupType($id: String = "", $_set: cleanup_type_set_input = {}) {
  update_cleanup_type_by_pk(pk_columns: {id: $id}, _set: $_set) {
    ${getFields(RESOURCE_NAME.CLEANUP_TYPE, FETCH_TYPE.GET_MANY)}
  }
}`;

export const GET_CHARACTERIZATION_LEVEL = `query GetCharacterizationLevel($id: Int) {
  characterization_level_by_pk(id: $id) {
    ${getFields(RESOURCE_NAME.CHARACTERIZATION_LEVEL, FETCH_TYPE.GET_ONE)}
  }
}`;

export const GET_CHARACTERIZATION_LEVELS = `query GetCharacterizationLevels($where: characterization_level_bool_exp = {}, $order_by: [characterization_level_order_by!] = {}, $limit: Int, $offset: Int) {
    characterization_level(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
      ${getFields(RESOURCE_NAME.CHARACTERIZATION_LEVEL, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const INSERT_CHARACTERIZATION_LEVEL = `mutation InsertCharacterizationLevel($_set: characterization_level_insert_input = {}) {
  insert_characterization_level_one(object: $_set) {
    ${getFields(RESOURCE_NAME.CHARACTERIZATION_LEVEL, FETCH_TYPE.GET_MANY)}
  }
}`;

export const UPDATE_CHARACTERIZATION_LEVEL = `mutation UpdateCharacterizationLevel($id: Int!, $_set: characterization_level_set_input = {}) {
  update_characterization_level_by_pk(pk_columns: {id: $id}, _set: $_set) {
    ${getFields(RESOURCE_NAME.CHARACTERIZATION_LEVEL, FETCH_TYPE.GET_MANY)}
  }
}`;

export const UPDATE_CLEANUP_TYPE_RUBBISHES = `
  mutation UpdateCleanupTypeRubbishes($id: String = "", $objects: [cleanup_type_rubbish_insert_input!] = {}) {
    delete_cleanup_type_rubbish(where: {cleanup_type_id: {_eq: $id}}) {
      affected_rows
    }
    insert_cleanup_type_rubbish(objects: $objects) {
      affected_rows
    }
  }`;

export const DELETE_CLEANUP_AREAS_BY_IDS = `
 mutation DeleteCleanupArea($ids: [uuid!]) {
    delete_cleanup_area(where: {id: {_in: $ids}}) {
      affected_rows
    }
  }`;

export const UPSERT_CLEANUP_AREA = `
  mutation UpsertCleanupArea($objects: [cleanup_area_insert_input!] = {}) {
    insert_cleanup_area(
      objects: $objects, 
      on_conflict: {
        constraint: cleanup_area_pkey, 
        update_columns: [
          ${GRAPHQL_RESOURCES.cleanup_area.UPDATE_COLUMNS?.join(",")}
        ]
      }
    ) {
      returning {
        ${getFields(RESOURCE_NAME.CLEANUP_AREA, FETCH_TYPE.GET_MANY)}
      }
    }
  }`;

export const UPDATE_CLEANUP_CAMPAIGNS = `
  mutation UpdateCleanupCampaigns($id: uuid = "", $objects: [cleanup_campaign_insert_input!] = {}) {
    delete_cleanup_campaign(where: {cleanup_id: {_eq: $id}}) {
      affected_rows
    }
    insert_cleanup_campaign(objects: $objects) {
      affected_rows
    }
  }`;

export const UPDATE_CLEANUP_PARTNERS = `
  mutation UpdateCleanupPartners($id: uuid = "", $objects: [cleanup_partner_insert_input!] = {}) {
    delete_cleanup_partner(where: {cleanup_id: {_eq: $id}}) {
      affected_rows
    }
    insert_cleanup_partner(objects: $objects) {
      affected_rows
    }
  }`;

export const GET_CLEANUP_PHOTOS_MANY = `
  query GetCleanupPhotos($where: cleanup_photo_bool_exp, $limit: Int, $offset: Int, $order_by: [cleanup_photo_order_by!]) {
    cleanup_photo(where: $where, limit: $limit, offset: $offset, order_by: $order_by) {
      ${getFields(RESOURCE_NAME.CLEANUP_PHOTO, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const DELETE_CLEANUP_PHOTOS_BY_IDS = `
  mutation DeleteCleanupPhoto($ids: [uuid!]) {
    delete_cleanup_photo(where: {id: {_in: $ids}}) {
      affected_rows
    }
  }`;

export const UPSERT_CLEANUP_PHOTO = `
  mutation UpsertCleanupPhoto($objects: [cleanup_photo_insert_input!] = {}) {
    insert_cleanup_photo(
      objects: $objects, 
      on_conflict: {
        constraint: cleanup_photo_pkey, 
        update_columns: [
          ${GRAPHQL_RESOURCES.cleanup_photo?.UPDATE_COLUMNS?.join(",")}
        ]
      }
    ) {
      returning {
        ${getFields(RESOURCE_NAME.CLEANUP_PHOTO, FETCH_TYPE.GET_MANY)}
      }
    }
  }`;

export const GET_CLEANUP_FORM = `query GetCleanupForm($id: uuid = {}) {
  cleanup_form_by_pk(id: $id) {
    ${getFields(RESOURCE_NAME.CLEANUP_FORM, FETCH_TYPE.GET_ONE)}
  }
}`;

export const INSERT_CLEANUP_FORM = `
  mutation InsertForm($_set: cleanup_form_insert_input = {}) {
    insert_cleanup_form_one(object: $_set) {
      ${getFields(RESOURCE_NAME.CLEANUP_FORM, FETCH_TYPE.GET_ONE)}
    }
  }`;

export const UPDATE_CLEANUP_FORM = `
  mutation UpdateForm($id: uuid = "", $_set: cleanup_form_set_input = {}) {
    update_cleanup_form_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.CLEANUP_FORM, FETCH_TYPE.GET_ONE)}
    }
  }`;

export const GET_CLEANUP_PLACES = `query GetCleanupPlaces($where: cleanup_place_bool_exp = {}, $order_by: [cleanup_place_order_by!] = {}, $limit: Int, $offset: Int) {
    cleanup_place(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
      ${getFields(RESOURCE_NAME.CLEANUP_PLACE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const INSERT_CLEANUP_PLACE = `
  mutation InsertCleanupPlace($_set: cleanup_place_insert_input = {}) {
    insert_cleanup_place_one(object: $_set) {
      ${getFields(RESOURCE_NAME.CLEANUP_PLACE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_CLEANUP_PLACE = `
  mutation UpdateCleanupPlace($id: String = "", $_set: cleanup_place_set_input = {}) {
    update_cleanup_place_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.CLEANUP_PLACE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const INSERT_CITY = `
  mutation InsertCity($_set: city_insert_input = {}) {
    insert_city_one(object: $_set) {
      ${getFields(RESOURCE_NAME.CITY, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_CITY = `
  mutation UpdateCity($id: String = "", $_set: city_set_input = {}) {
    update_city_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.CITY, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const GET_PARTNER = `query GetPartner($id: String = "") {
    partner_by_pk(id: $id) {
      ${getFields(RESOURCE_NAME.PARTNER, FETCH_TYPE.GET_ONE)}
    }
  }`;

export const GET_PARTNERS = `query GetPartners($where: partner_bool_exp = {}, $order_by: [partner_order_by!] = {}, $limit: Int, $offset: Int) {
    partner(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
      ${getFields(RESOURCE_NAME.PARTNER, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const INSERT_PARTNER = `
  mutation InsertPartner($_set: partner_insert_input = {}) {
    insert_partner_one(object: $_set) {
      ${getFields(RESOURCE_NAME.PARTNER, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_PARTNER = `
  mutation UpdatePartner($id: String = "", $_set: partner_set_input = {}) {
    update_partner_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.PARTNER, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const DELETE_CLEANUP_FORM_RUBBISH_BY_IDS = `
 mutation DeleteCleanupFormRubbish($ids: [uuid!]) {
    delete_cleanup_form_rubbish(where: {id: {_in: $ids}}) {
      affected_rows
    }
  }`;

export const UPSERT_CLEANUP_FORM_RUBBISH = `
  mutation UpsertCleanupFormRubbish($objects: [cleanup_form_rubbish_insert_input!] = {}) {
    insert_cleanup_form_rubbish(
      objects: $objects, 
      on_conflict: {
        constraint: cleanup_form_rubbish_pkey, 
        update_columns: [
          ${GRAPHQL_RESOURCES.cleanup_form_rubbish?.UPDATE_COLUMNS?.join(",")}
        ]
      }
    ) {
      returning {
        ${getFields(RESOURCE_NAME.CLEANUP_FORM_RUBBISH, FETCH_TYPE.GET_MANY)}
      }
    }
  }`;

export const DELETE_CLEANUP_FORM_RUBBISH_DETAIL_BY_IDS = `
 mutation DeleteCleanupFormRubbishDetail($ids: [uuid!]) {
    delete_cleanup_form_rubbish_detail(where: {id: {_in: $ids}}) {
      affected_rows
    }
  }`;

export const UPSERT_CLEANUP_FORM_RUBBISH_DETAIL = `
  mutation UpsertCleanupFormRubbishDetail($objects: [cleanup_form_rubbish_detail_insert_input!] = {}) {
    insert_cleanup_form_rubbish_detail(
      objects: $objects, 
      on_conflict: {
        constraint: cleanup_form_rubbish_detail_pkey, 
        update_columns: [
          ${GRAPHQL_RESOURCES.cleanup_form_rubbish_detail?.UPDATE_COLUMNS?.join(
            ","
          )}
        ]
      }
    ) {
      returning {
        ${getFields(
          RESOURCE_NAME.CLEANUP_FORM_RUBBISH_DETAIL,
          FETCH_TYPE.GET_MANY
        )}
      }
    }
  }`;

export const DELETE_CLEANUP_FORM_POI_BY_IDS = `
 mutation DeleteCleanupFormPoi($ids: [uuid!]) {
    delete_cleanup_form_poi(where: {id: {_in: $ids}}) {
      affected_rows
    }
  }`;

export const UPSERT_CLEANUP_FORM_POI = `
  mutation UpsertCleanupFormPoi($objects: [cleanup_form_poi_insert_input!] = {}) {
    insert_cleanup_form_poi(
      objects: $objects, 
      on_conflict: {
        constraint: cleanup_form_poi_pkey, 
        update_columns: [
          ${GRAPHQL_RESOURCES.cleanup_form_poi?.UPDATE_COLUMNS?.join(",")}
        ]
      }
    ) {
      returning {
        ${getFields(RESOURCE_NAME.CLEANUP_FORM_POI, FETCH_TYPE.GET_MANY)}
      }
    }
  }`;

export const DELETE_CLEANUP_FORM_DOI_BY_IDS = `
 mutation DeleteCleanupFormDoi($ids: [uuid!]) {
    delete_cleanup_form_doi(where: {id: {_in: $ids}}) {
      affected_rows
    }
  }`;

export const UPSERT_CLEANUP_FORM_DOI = `
  mutation UpsertCleanupFormDoi($objects: [cleanup_form_doi_insert_input!] = {}) {
    insert_cleanup_form_doi(
      objects: $objects, 
      on_conflict: {
        constraint: cleanup_form_doi_pkey, 
        update_columns: [
          ${GRAPHQL_RESOURCES.cleanup_form_doi?.UPDATE_COLUMNS?.join(",")}
        ]
      }
    ) {
      returning {
        ${getFields(RESOURCE_NAME.CLEANUP_FORM_DOI, FETCH_TYPE.GET_MANY)}
      }
    }
  }`;

export const INSERT_BRAND = `
  mutation InsertBrand($_set: brand_insert_input = {}) {
    insert_brand_one(object: $_set) {
      ${getFields(RESOURCE_NAME.BRAND, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_BRAND = `
  mutation UpdateBrand($id: String = "", $_set: brand_set_input = {}) {
    update_brand_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.BRAND, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const INSERT_RUBBISH_CATEGORY = `
  mutation InsertRubbishCategory($_set: rubbish_category_insert_input = {}) {
    insert_rubbish_category_one(object: $_set) {
      ${getFields(RESOURCE_NAME.RUBBISH_CATEGORY, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_RUBBISH_CATEGORY = `
  mutation UpdateRubbishCategory($id: String = "", $_set: rubbish_category_set_input = {}) {
    update_rubbish_category_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.RUBBISH_CATEGORY, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const GET_RUBBISHES = `query GetRubbishes($where: rubbish_bool_exp = {}, $order_by: [rubbish_order_by!] = {}, $limit: Int, $offset: Int) {
    rubbish(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
      ${getFields(RESOURCE_NAME.RUBBISH, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const INSERT_RUBBISH = `
  mutation InsertRubbish($_set: rubbish_insert_input = {}) {
    insert_rubbish_one(object: $_set) {
      ${getFields(RESOURCE_NAME.RUBBISH, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_RUBBISH = `
  mutation UpdateRubbish($id: String = "", $_set: rubbish_set_input = {}) {
    update_rubbish_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.RUBBISH, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_RUBBISH_TAGS = `
  mutation UpdateRubbishTagss($id: String = "", $objects: [rubbish_rubbish_tag_insert_input!] = {}) {
    delete_rubbish_rubbish_tag(where: {rubbish_id: {_eq: $id}}) {
      affected_rows
    }
    insert_rubbish_rubbish_tag(objects: $objects) {
      affected_rows
    }
  }`;

export const INSERT_RUBBISH_TAG = `
  mutation InsertRubbishTag($_set: rubbish_tag_insert_input = {}) {
    insert_rubbish_tag_one(object: $_set) {
      ${getFields(RESOURCE_NAME.RUBBISH_TAG, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_RUBBISH_TAG = `
  mutation UpdateRubbishTag($id: String = "", $_set: rubbish_tag_set_input = {}) {
    update_rubbish_tag_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.RUBBISH_TAG, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_RUBBISH_CLEANUP_TYPES = `
  mutation UpdateRubbishCleanupTypes($id: String = "", $objects: [cleanup_type_rubbish_insert_input!] = {}) {
    delete_cleanup_type_rubbish(where: {rubbish_id: {_eq: $id}}) {
      affected_rows
    }
    insert_cleanup_type_rubbish(objects: $objects) {
      affected_rows
    }
  }`;

export const GET_RUBBISH_TYPE_PICKED = `query GetRubbishTypePicked($where: rubbish_type_picked_bool_exp = {}, $order_by: [rubbish_type_picked_order_by!] = {}, $limit: Int, $offset: Int) {
    rubbish_type_picked(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
      id
      label
      id_zds
    }
  }`;

export const GET_POI_TYPE = `query GetPoiType($id: String = "") {
    poi_type_by_pk(id: $id) {
      ${getFields(RESOURCE_NAME.POI_TYPE, FETCH_TYPE.GET_ONE)}
    }
  }`;

export const INSERT_POI_TYPE = `
  mutation InsertPoiType($_set: poi_type_insert_input = {}) {
    insert_poi_type_one(object: $_set) {
      ${getFields(RESOURCE_NAME.POI_TYPE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_POI_TYPE = `
  mutation UpdatePoiType($id: String = "", $_set: poi_type_set_input = {}) {
    update_poi_type_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.POI_TYPE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const GET_RESOURCE = `query GetResource($id: uuid = {}) {
    resource_by_pk(id: $id) {
      ${getFields(RESOURCE_NAME.RESOURCE, FETCH_TYPE.GET_ONE)}
      }
    }`;

export const GET_RESOURCES = `query GetResources($where: resource_bool_exp = {}, $order_by: [resource_order_by!] = {}, $limit: Int, $offset: Int) {
      resource(where: $where, order_by: $order_by, limit: $limit, offset: $offset) {
        ${getFields(RESOURCE_NAME.RESOURCE, FETCH_TYPE.GET_MANY)}
      }
    }`;

export const INSERT_RESOURCE = `
  mutation InsertResource($_set: resource_insert_input = {}) {
    insert_resource_one(object: $_set) {
      ${getFields(RESOURCE_NAME.RESOURCE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const UPDATE_RESOURCE = `
  mutation UpdateResource($id: uuid = {}, $_set: resource_set_input = {}) {
    update_resource_by_pk(pk_columns: {id: $id}, _set: $_set) {
      ${getFields(RESOURCE_NAME.RESOURCE, FETCH_TYPE.GET_MANY)}
    }
  }`;

export const COUNT_CLEANUPS = `
  query CountCleanups($where: cleanup_bool_exp = {}) {
    cleanup_aggregate(where: $where) {
      aggregate {
        count
      }
    }
  }`;

export const GET_LINEAR_AREA = `query GetLinearArea($where: environment_area_type_bool_exp!) {
    environment_area_type(where: $where) {
      is_area
      is_linear
    }
  }`;

export const GET_CLEANUP_AREA_ENVIRONMENT_ID = `query GetCleanupAreaEnvironmentId($id: uuid = {}) {
    cleanup_by_pk(id: $id) {
        area_type_id
        environment_type_id
    }
  }`;

export const GET_CAMPAIGN_RUBBISH_METRICS = `
  query GetCampaignRubbishMetrics ($where: campaign_rubbish_metrics_view_bool_exp = {}, $order_by: [campaign_rubbish_metrics_view_order_by!] = [], $limit: Int) {
    campaign_rubbish_metrics_view(where: $where, order_by: $order_by, limit: $limit) {
      campaign_id
      campaign {
        label
      }
      category_id
      rubbish_category {
        label
        color
        rank
      }
      sum_qty
      sum_weight
    }
  }`;

export const COUNT_VOLUNTEERS = `
  query CountVolunteers($where: cleanup_form_bool_exp = {}) {
    cleanup_form(where: $where) {
      cleanup_duration      
      volunteers_number
      external_volunteers_number
    }
  }`;

export const GET_RUBBISH_VALUE = `
  query GetRubbishKG($where: cleanup_form_rubbish_bool_exp = {}, $limit: Int) {
    cleanup_form_rubbish_aggregate(where: $where, limit: $limit) {
      aggregate {
        sum {
          quantity
          volume
          weight
        }
      }
    }
  }`;

export const GET_RUBBISHES_FOR_TAGS = `
  query GetRubbishesForTags($where: rubbish_bool_exp = {}, $limit: Int, $order_by: [rubbish_order_by!] = []) {
    rubbish(
      where: $where,
      order_by: $order_by,
      limit: $limit
    ) {
      id
      label
      tags {
        rubbish_tag {
          label
        }
      }
      cleanup_form_rubbishes_aggregate {
        aggregate {
          sum {
            quantity
          }
        }
      }
    }
  }
`;

export const GET_RUBBISH_TAGS_METRICS = `
  query GetRubbishTagsMetrics($where: rubbish_tag_metrics_model_bool_exp_bool_exp = {}, $limit: Int) {
    rubbish_tag_metrics_query(where: $where, limit: $limit) {
        cleanup_id
        quantity
        rubbish_tag_id
        rubbish_tag_label
      }
  }
  `;

export const GET_RUBBISHES_PER_TAGS = `
  query GetRubbishesPerTags($where: rubbish_tag_bool_exp = {}, $limit: Int) {
    rubbish_tag(where: $where, limit: $limit) {
      id
    rubbishes {
      rubbish {
        id
        label
      }
    }
  }
}`;

export const RECURRENT_RUBBISH = `
  query RecurrentRubbish($whereRubbish: rubbish_bool_exp = {}, $whereForm: cleanup_form_rubbish_bool_exp = {}) {
    rubbish(where: $whereRubbish) {
      id
      cleanup_form_rubbishes_aggregate(where: $whereForm) {
        aggregate {
          sum {
            quantity
            volume
            weight
          }
        }
      }
    }
  }`;
