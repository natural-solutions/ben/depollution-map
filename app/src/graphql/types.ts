export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  bigint: { input: any; output: any; }
  date: { input: any; output: any; }
  geography: { input: any; output: any; }
  geometry: { input: any; output: any; }
  jsonb: { input: any; output: any; }
  numeric: { input: any; output: any; }
  time: { input: any; output: any; }
  timestamptz: { input: any; output: any; }
  uuid: { input: any; output: any; }
};

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Boolean']['input']>;
  _gt?: InputMaybe<Scalars['Boolean']['input']>;
  _gte?: InputMaybe<Scalars['Boolean']['input']>;
  _in?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['Boolean']['input']>;
  _lte?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<Scalars['Boolean']['input']>;
  _nin?: InputMaybe<Array<Scalars['Boolean']['input']>>;
};

/** Boolean expression to compare columns of type "Int". All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Int']['input']>;
  _gt?: InputMaybe<Scalars['Int']['input']>;
  _gte?: InputMaybe<Scalars['Int']['input']>;
  _in?: InputMaybe<Array<Scalars['Int']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['Int']['input']>;
  _lte?: InputMaybe<Scalars['Int']['input']>;
  _neq?: InputMaybe<Scalars['Int']['input']>;
  _nin?: InputMaybe<Array<Scalars['Int']['input']>>;
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['String']['input']>;
  _gt?: InputMaybe<Scalars['String']['input']>;
  _gte?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['String']['input']>;
  _in?: InputMaybe<Array<Scalars['String']['input']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['String']['input']>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['String']['input']>;
  _lt?: InputMaybe<Scalars['String']['input']>;
  _lte?: InputMaybe<Scalars['String']['input']>;
  _neq?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['String']['input']>;
  _nin?: InputMaybe<Array<Scalars['String']['input']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['String']['input']>;
};

/** columns and relationships of "area_type" */
export type Area_Type = {
  __typename?: 'area_type';
  /** An array relationship */
  environment_area_types: Array<Environment_Area_Type>;
  /** An aggregate relationship */
  environment_area_types_aggregate: Environment_Area_Type_Aggregate;
  id: Scalars['String']['output'];
  id_zds?: Maybe<Scalars['String']['output']>;
  label: Scalars['String']['output'];
};


/** columns and relationships of "area_type" */
export type Area_TypeEnvironment_Area_TypesArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};


/** columns and relationships of "area_type" */
export type Area_TypeEnvironment_Area_Types_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};

/** aggregated selection of "area_type" */
export type Area_Type_Aggregate = {
  __typename?: 'area_type_aggregate';
  aggregate?: Maybe<Area_Type_Aggregate_Fields>;
  nodes: Array<Area_Type>;
};

/** aggregate fields of "area_type" */
export type Area_Type_Aggregate_Fields = {
  __typename?: 'area_type_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Area_Type_Max_Fields>;
  min?: Maybe<Area_Type_Min_Fields>;
};


/** aggregate fields of "area_type" */
export type Area_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Area_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "area_type". All fields are combined with a logical 'AND'. */
export type Area_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Area_Type_Bool_Exp>>;
  _not?: InputMaybe<Area_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Area_Type_Bool_Exp>>;
  environment_area_types?: InputMaybe<Environment_Area_Type_Bool_Exp>;
  environment_area_types_aggregate?: InputMaybe<Environment_Area_Type_Aggregate_Bool_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  id_zds?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "area_type" */
export enum Area_Type_Constraint {
  /** unique or primary key constraint on columns "label" */
  AreaTypeLabelKey = 'area_type_label_key',
  /** unique or primary key constraint on columns "id" */
  AreaTypePkey = 'area_type_pkey'
}

/** input type for inserting data into table "area_type" */
export type Area_Type_Insert_Input = {
  environment_area_types?: InputMaybe<Environment_Area_Type_Arr_Rel_Insert_Input>;
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Area_Type_Max_Fields = {
  __typename?: 'area_type_max_fields';
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
};

/** aggregate min on columns */
export type Area_Type_Min_Fields = {
  __typename?: 'area_type_min_fields';
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
};

/** response of any mutation on the table "area_type" */
export type Area_Type_Mutation_Response = {
  __typename?: 'area_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Area_Type>;
};

/** input type for inserting object relation for remote table "area_type" */
export type Area_Type_Obj_Rel_Insert_Input = {
  data: Area_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Area_Type_On_Conflict>;
};

/** on_conflict condition type for table "area_type" */
export type Area_Type_On_Conflict = {
  constraint: Area_Type_Constraint;
  update_columns?: Array<Area_Type_Update_Column>;
  where?: InputMaybe<Area_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "area_type". */
export type Area_Type_Order_By = {
  environment_area_types_aggregate?: InputMaybe<Environment_Area_Type_Aggregate_Order_By>;
  id?: InputMaybe<Order_By>;
  id_zds?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
};

/** primary key columns input for table: area_type */
export type Area_Type_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "area_type" */
export enum Area_Type_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  Label = 'label'
}

/** input type for updating data in table "area_type" */
export type Area_Type_Set_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "area_type" */
export type Area_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Area_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Area_Type_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "area_type" */
export enum Area_Type_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  Label = 'label'
}

export type Area_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Area_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Area_Type_Bool_Exp;
};

/** Boolean expression to compare columns of type "bigint". All fields are combined with logical 'AND'. */
export type Bigint_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['bigint']['input']>;
  _gt?: InputMaybe<Scalars['bigint']['input']>;
  _gte?: InputMaybe<Scalars['bigint']['input']>;
  _in?: InputMaybe<Array<Scalars['bigint']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['bigint']['input']>;
  _lte?: InputMaybe<Scalars['bigint']['input']>;
  _neq?: InputMaybe<Scalars['bigint']['input']>;
  _nin?: InputMaybe<Array<Scalars['bigint']['input']>>;
};

/** columns and relationships of "brand" */
export type Brand = {
  __typename?: 'brand';
  /** An array relationship */
  cleanup_form_rubbish_details: Array<Cleanup_Form_Rubbish_Detail>;
  /** An aggregate relationship */
  cleanup_form_rubbish_details_aggregate: Cleanup_Form_Rubbish_Detail_Aggregate;
  /** An array relationship */
  cleanup_form_rubbishes: Array<Cleanup_Form_Rubbish>;
  /** An aggregate relationship */
  cleanup_form_rubbishes_aggregate: Cleanup_Form_Rubbish_Aggregate;
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['String']['output'];
  label: Scalars['String']['output'];
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "brand" */
export type BrandCleanup_Form_Rubbish_DetailsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "brand" */
export type BrandCleanup_Form_Rubbish_Details_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "brand" */
export type BrandCleanup_Form_RubbishesArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


/** columns and relationships of "brand" */
export type BrandCleanup_Form_Rubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};

/** aggregated selection of "brand" */
export type Brand_Aggregate = {
  __typename?: 'brand_aggregate';
  aggregate?: Maybe<Brand_Aggregate_Fields>;
  nodes: Array<Brand>;
};

/** aggregate fields of "brand" */
export type Brand_Aggregate_Fields = {
  __typename?: 'brand_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Brand_Max_Fields>;
  min?: Maybe<Brand_Min_Fields>;
};


/** aggregate fields of "brand" */
export type Brand_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Brand_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "brand". All fields are combined with a logical 'AND'. */
export type Brand_Bool_Exp = {
  _and?: InputMaybe<Array<Brand_Bool_Exp>>;
  _not?: InputMaybe<Brand_Bool_Exp>;
  _or?: InputMaybe<Array<Brand_Bool_Exp>>;
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Bool_Exp>;
  cleanup_form_rubbishes?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
  cleanup_form_rubbishes_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Aggregate_Bool_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "brand" */
export enum Brand_Constraint {
  /** unique or primary key constraint on columns "label" */
  BrandLabelKey = 'brand_label_key',
  /** unique or primary key constraint on columns "id" */
  BrandPkey = 'brand_pkey'
}

/** input type for inserting data into table "brand" */
export type Brand_Insert_Input = {
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Arr_Rel_Insert_Input>;
  cleanup_form_rubbishes?: InputMaybe<Cleanup_Form_Rubbish_Arr_Rel_Insert_Input>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Brand_Max_Fields = {
  __typename?: 'brand_max_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Brand_Min_Fields = {
  __typename?: 'brand_min_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "brand" */
export type Brand_Mutation_Response = {
  __typename?: 'brand_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Brand>;
};

/** input type for inserting object relation for remote table "brand" */
export type Brand_Obj_Rel_Insert_Input = {
  data: Brand_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Brand_On_Conflict>;
};

/** on_conflict condition type for table "brand" */
export type Brand_On_Conflict = {
  constraint: Brand_Constraint;
  update_columns?: Array<Brand_Update_Column>;
  where?: InputMaybe<Brand_Bool_Exp>;
};

/** Ordering options when selecting data from "brand". */
export type Brand_Order_By = {
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Order_By>;
  cleanup_form_rubbishes_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Aggregate_Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: brand */
export type Brand_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "brand" */
export enum Brand_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "brand" */
export type Brand_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "brand" */
export type Brand_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Brand_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Brand_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "brand" */
export enum Brand_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Brand_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Brand_Set_Input>;
  /** filter the rows which have to be updated */
  where: Brand_Bool_Exp;
};

/** columns and relationships of "campaign" */
export type Campaign = {
  __typename?: 'campaign';
  /** An array relationship */
  campaign_users: Array<Campaign_User>;
  /** An aggregate relationship */
  campaign_users_aggregate: Campaign_User_Aggregate;
  /** An array relationship */
  cleanup_campaigns: Array<Cleanup_Campaign>;
  /** An aggregate relationship */
  cleanup_campaigns_aggregate: Cleanup_Campaign_Aggregate;
  created_at: Scalars['timestamptz']['output'];
  /** An object relationship */
  default_cleanup_type?: Maybe<Cleanup_Type>;
  default_cleanup_type_id?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  label: Scalars['String']['output'];
  logo?: Maybe<Scalars['String']['output']>;
  main_color?: Maybe<Scalars['String']['output']>;
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "campaign" */
export type CampaignCampaign_UsersArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


/** columns and relationships of "campaign" */
export type CampaignCampaign_Users_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


/** columns and relationships of "campaign" */
export type CampaignCleanup_CampaignsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


/** columns and relationships of "campaign" */
export type CampaignCleanup_Campaigns_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};

/** aggregated selection of "campaign" */
export type Campaign_Aggregate = {
  __typename?: 'campaign_aggregate';
  aggregate?: Maybe<Campaign_Aggregate_Fields>;
  nodes: Array<Campaign>;
};

/** aggregate fields of "campaign" */
export type Campaign_Aggregate_Fields = {
  __typename?: 'campaign_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Campaign_Max_Fields>;
  min?: Maybe<Campaign_Min_Fields>;
};


/** aggregate fields of "campaign" */
export type Campaign_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Campaign_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "campaign". All fields are combined with a logical 'AND'. */
export type Campaign_Bool_Exp = {
  _and?: InputMaybe<Array<Campaign_Bool_Exp>>;
  _not?: InputMaybe<Campaign_Bool_Exp>;
  _or?: InputMaybe<Array<Campaign_Bool_Exp>>;
  campaign_users?: InputMaybe<Campaign_User_Bool_Exp>;
  campaign_users_aggregate?: InputMaybe<Campaign_User_Aggregate_Bool_Exp>;
  cleanup_campaigns?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
  cleanup_campaigns_aggregate?: InputMaybe<Cleanup_Campaign_Aggregate_Bool_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  default_cleanup_type?: InputMaybe<Cleanup_Type_Bool_Exp>;
  default_cleanup_type_id?: InputMaybe<String_Comparison_Exp>;
  description?: InputMaybe<String_Comparison_Exp>;
  email?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  logo?: InputMaybe<String_Comparison_Exp>;
  main_color?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "campaign" */
export enum Campaign_Constraint {
  /** unique or primary key constraint on columns "id" */
  CampaignPkey = 'campaign_pkey'
}

/** input type for inserting data into table "campaign" */
export type Campaign_Insert_Input = {
  campaign_users?: InputMaybe<Campaign_User_Arr_Rel_Insert_Input>;
  cleanup_campaigns?: InputMaybe<Cleanup_Campaign_Arr_Rel_Insert_Input>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  default_cleanup_type?: InputMaybe<Cleanup_Type_Obj_Rel_Insert_Input>;
  default_cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  main_color?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Campaign_Max_Fields = {
  __typename?: 'campaign_max_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  default_cleanup_type_id?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  logo?: Maybe<Scalars['String']['output']>;
  main_color?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Campaign_Min_Fields = {
  __typename?: 'campaign_min_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  default_cleanup_type_id?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  logo?: Maybe<Scalars['String']['output']>;
  main_color?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "campaign" */
export type Campaign_Mutation_Response = {
  __typename?: 'campaign_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Campaign>;
};

/** input type for inserting object relation for remote table "campaign" */
export type Campaign_Obj_Rel_Insert_Input = {
  data: Campaign_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Campaign_On_Conflict>;
};

/** on_conflict condition type for table "campaign" */
export type Campaign_On_Conflict = {
  constraint: Campaign_Constraint;
  update_columns?: Array<Campaign_Update_Column>;
  where?: InputMaybe<Campaign_Bool_Exp>;
};

/** Ordering options when selecting data from "campaign". */
export type Campaign_Order_By = {
  campaign_users_aggregate?: InputMaybe<Campaign_User_Aggregate_Order_By>;
  cleanup_campaigns_aggregate?: InputMaybe<Cleanup_Campaign_Aggregate_Order_By>;
  created_at?: InputMaybe<Order_By>;
  default_cleanup_type?: InputMaybe<Cleanup_Type_Order_By>;
  default_cleanup_type_id?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  email?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  logo?: InputMaybe<Order_By>;
  main_color?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: campaign */
export type Campaign_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** columns and relationships of "campaign_rubbish_metrics_view" */
export type Campaign_Rubbish_Metrics_View = {
  __typename?: 'campaign_rubbish_metrics_view';
  /** An object relationship */
  campaign?: Maybe<Campaign>;
  campaign_id?: Maybe<Scalars['String']['output']>;
  category_id?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  rubbish_category?: Maybe<Rubbish_Category>;
  sum_qty?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregated selection of "campaign_rubbish_metrics_view" */
export type Campaign_Rubbish_Metrics_View_Aggregate = {
  __typename?: 'campaign_rubbish_metrics_view_aggregate';
  aggregate?: Maybe<Campaign_Rubbish_Metrics_View_Aggregate_Fields>;
  nodes: Array<Campaign_Rubbish_Metrics_View>;
};

/** aggregate fields of "campaign_rubbish_metrics_view" */
export type Campaign_Rubbish_Metrics_View_Aggregate_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_aggregate_fields';
  avg?: Maybe<Campaign_Rubbish_Metrics_View_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Campaign_Rubbish_Metrics_View_Max_Fields>;
  min?: Maybe<Campaign_Rubbish_Metrics_View_Min_Fields>;
  stddev?: Maybe<Campaign_Rubbish_Metrics_View_Stddev_Fields>;
  stddev_pop?: Maybe<Campaign_Rubbish_Metrics_View_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Campaign_Rubbish_Metrics_View_Stddev_Samp_Fields>;
  sum?: Maybe<Campaign_Rubbish_Metrics_View_Sum_Fields>;
  var_pop?: Maybe<Campaign_Rubbish_Metrics_View_Var_Pop_Fields>;
  var_samp?: Maybe<Campaign_Rubbish_Metrics_View_Var_Samp_Fields>;
  variance?: Maybe<Campaign_Rubbish_Metrics_View_Variance_Fields>;
};


/** aggregate fields of "campaign_rubbish_metrics_view" */
export type Campaign_Rubbish_Metrics_View_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type Campaign_Rubbish_Metrics_View_Avg_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_avg_fields';
  sum_qty?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "campaign_rubbish_metrics_view". All fields are combined with a logical 'AND'. */
export type Campaign_Rubbish_Metrics_View_Bool_Exp = {
  _and?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Bool_Exp>>;
  _not?: InputMaybe<Campaign_Rubbish_Metrics_View_Bool_Exp>;
  _or?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Bool_Exp>>;
  campaign?: InputMaybe<Campaign_Bool_Exp>;
  campaign_id?: InputMaybe<String_Comparison_Exp>;
  category_id?: InputMaybe<String_Comparison_Exp>;
  rubbish_category?: InputMaybe<Rubbish_Category_Bool_Exp>;
  sum_qty?: InputMaybe<Bigint_Comparison_Exp>;
  sum_volume?: InputMaybe<Numeric_Comparison_Exp>;
  sum_weight?: InputMaybe<Numeric_Comparison_Exp>;
};

/** aggregate max on columns */
export type Campaign_Rubbish_Metrics_View_Max_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_max_fields';
  campaign_id?: Maybe<Scalars['String']['output']>;
  category_id?: Maybe<Scalars['String']['output']>;
  sum_qty?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregate min on columns */
export type Campaign_Rubbish_Metrics_View_Min_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_min_fields';
  campaign_id?: Maybe<Scalars['String']['output']>;
  category_id?: Maybe<Scalars['String']['output']>;
  sum_qty?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** Ordering options when selecting data from "campaign_rubbish_metrics_view". */
export type Campaign_Rubbish_Metrics_View_Order_By = {
  campaign?: InputMaybe<Campaign_Order_By>;
  campaign_id?: InputMaybe<Order_By>;
  category_id?: InputMaybe<Order_By>;
  rubbish_category?: InputMaybe<Rubbish_Category_Order_By>;
  sum_qty?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** select columns of table "campaign_rubbish_metrics_view" */
export enum Campaign_Rubbish_Metrics_View_Select_Column {
  /** column name */
  CampaignId = 'campaign_id',
  /** column name */
  CategoryId = 'category_id',
  /** column name */
  SumQty = 'sum_qty',
  /** column name */
  SumVolume = 'sum_volume',
  /** column name */
  SumWeight = 'sum_weight'
}

/** aggregate stddev on columns */
export type Campaign_Rubbish_Metrics_View_Stddev_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_stddev_fields';
  sum_qty?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_pop on columns */
export type Campaign_Rubbish_Metrics_View_Stddev_Pop_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_stddev_pop_fields';
  sum_qty?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_samp on columns */
export type Campaign_Rubbish_Metrics_View_Stddev_Samp_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_stddev_samp_fields';
  sum_qty?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "campaign_rubbish_metrics_view" */
export type Campaign_Rubbish_Metrics_View_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Campaign_Rubbish_Metrics_View_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Campaign_Rubbish_Metrics_View_Stream_Cursor_Value_Input = {
  campaign_id?: InputMaybe<Scalars['String']['input']>;
  category_id?: InputMaybe<Scalars['String']['input']>;
  sum_qty?: InputMaybe<Scalars['bigint']['input']>;
  sum_volume?: InputMaybe<Scalars['numeric']['input']>;
  sum_weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate sum on columns */
export type Campaign_Rubbish_Metrics_View_Sum_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_sum_fields';
  sum_qty?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregate var_pop on columns */
export type Campaign_Rubbish_Metrics_View_Var_Pop_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_var_pop_fields';
  sum_qty?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate var_samp on columns */
export type Campaign_Rubbish_Metrics_View_Var_Samp_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_var_samp_fields';
  sum_qty?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type Campaign_Rubbish_Metrics_View_Variance_Fields = {
  __typename?: 'campaign_rubbish_metrics_view_variance_fields';
  sum_qty?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** select columns of table "campaign" */
export enum Campaign_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DefaultCleanupTypeId = 'default_cleanup_type_id',
  /** column name */
  Description = 'description',
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Logo = 'logo',
  /** column name */
  MainColor = 'main_color',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "campaign" */
export type Campaign_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  default_cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  main_color?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "campaign" */
export type Campaign_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Campaign_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Campaign_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  default_cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  main_color?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "campaign" */
export enum Campaign_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DefaultCleanupTypeId = 'default_cleanup_type_id',
  /** column name */
  Description = 'description',
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Logo = 'logo',
  /** column name */
  MainColor = 'main_color',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Campaign_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Campaign_Set_Input>;
  /** filter the rows which have to be updated */
  where: Campaign_Bool_Exp;
};

/** columns and relationships of "campaign_user" */
export type Campaign_User = {
  __typename?: 'campaign_user';
  /** An object relationship */
  campaign: Campaign;
  campaign_id: Scalars['String']['output'];
  role: Scalars['String']['output'];
  /** An object relationship */
  user_view?: Maybe<User_View>;
  user_view_id: Scalars['uuid']['output'];
};

/** aggregated selection of "campaign_user" */
export type Campaign_User_Aggregate = {
  __typename?: 'campaign_user_aggregate';
  aggregate?: Maybe<Campaign_User_Aggregate_Fields>;
  nodes: Array<Campaign_User>;
};

export type Campaign_User_Aggregate_Bool_Exp = {
  count?: InputMaybe<Campaign_User_Aggregate_Bool_Exp_Count>;
};

export type Campaign_User_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Campaign_User_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Campaign_User_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "campaign_user" */
export type Campaign_User_Aggregate_Fields = {
  __typename?: 'campaign_user_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Campaign_User_Max_Fields>;
  min?: Maybe<Campaign_User_Min_Fields>;
};


/** aggregate fields of "campaign_user" */
export type Campaign_User_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Campaign_User_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "campaign_user" */
export type Campaign_User_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Campaign_User_Max_Order_By>;
  min?: InputMaybe<Campaign_User_Min_Order_By>;
};

/** input type for inserting array relation for remote table "campaign_user" */
export type Campaign_User_Arr_Rel_Insert_Input = {
  data: Array<Campaign_User_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Campaign_User_On_Conflict>;
};

/** Boolean expression to filter rows from the table "campaign_user". All fields are combined with a logical 'AND'. */
export type Campaign_User_Bool_Exp = {
  _and?: InputMaybe<Array<Campaign_User_Bool_Exp>>;
  _not?: InputMaybe<Campaign_User_Bool_Exp>;
  _or?: InputMaybe<Array<Campaign_User_Bool_Exp>>;
  campaign?: InputMaybe<Campaign_Bool_Exp>;
  campaign_id?: InputMaybe<String_Comparison_Exp>;
  role?: InputMaybe<String_Comparison_Exp>;
  user_view?: InputMaybe<User_View_Bool_Exp>;
  user_view_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "campaign_user" */
export enum Campaign_User_Constraint {
  /** unique or primary key constraint on columns "user_view_id", "campaign_id" */
  CampaignUserPkey = 'campaign_user_pkey'
}

/** input type for inserting data into table "campaign_user" */
export type Campaign_User_Insert_Input = {
  campaign?: InputMaybe<Campaign_Obj_Rel_Insert_Input>;
  campaign_id?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
  user_view?: InputMaybe<User_View_Obj_Rel_Insert_Input>;
  user_view_id?: InputMaybe<Scalars['uuid']['input']>;
};

/** aggregate max on columns */
export type Campaign_User_Max_Fields = {
  __typename?: 'campaign_user_max_fields';
  campaign_id?: Maybe<Scalars['String']['output']>;
  role?: Maybe<Scalars['String']['output']>;
  user_view_id?: Maybe<Scalars['uuid']['output']>;
};

/** order by max() on columns of table "campaign_user" */
export type Campaign_User_Max_Order_By = {
  campaign_id?: InputMaybe<Order_By>;
  role?: InputMaybe<Order_By>;
  user_view_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Campaign_User_Min_Fields = {
  __typename?: 'campaign_user_min_fields';
  campaign_id?: Maybe<Scalars['String']['output']>;
  role?: Maybe<Scalars['String']['output']>;
  user_view_id?: Maybe<Scalars['uuid']['output']>;
};

/** order by min() on columns of table "campaign_user" */
export type Campaign_User_Min_Order_By = {
  campaign_id?: InputMaybe<Order_By>;
  role?: InputMaybe<Order_By>;
  user_view_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "campaign_user" */
export type Campaign_User_Mutation_Response = {
  __typename?: 'campaign_user_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Campaign_User>;
};

/** on_conflict condition type for table "campaign_user" */
export type Campaign_User_On_Conflict = {
  constraint: Campaign_User_Constraint;
  update_columns?: Array<Campaign_User_Update_Column>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};

/** Ordering options when selecting data from "campaign_user". */
export type Campaign_User_Order_By = {
  campaign?: InputMaybe<Campaign_Order_By>;
  campaign_id?: InputMaybe<Order_By>;
  role?: InputMaybe<Order_By>;
  user_view?: InputMaybe<User_View_Order_By>;
  user_view_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: campaign_user */
export type Campaign_User_Pk_Columns_Input = {
  campaign_id: Scalars['String']['input'];
  user_view_id: Scalars['uuid']['input'];
};

/** select columns of table "campaign_user" */
export enum Campaign_User_Select_Column {
  /** column name */
  CampaignId = 'campaign_id',
  /** column name */
  Role = 'role',
  /** column name */
  UserViewId = 'user_view_id'
}

/** input type for updating data in table "campaign_user" */
export type Campaign_User_Set_Input = {
  campaign_id?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
  user_view_id?: InputMaybe<Scalars['uuid']['input']>;
};

/** Streaming cursor of the table "campaign_user" */
export type Campaign_User_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Campaign_User_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Campaign_User_Stream_Cursor_Value_Input = {
  campaign_id?: InputMaybe<Scalars['String']['input']>;
  role?: InputMaybe<Scalars['String']['input']>;
  user_view_id?: InputMaybe<Scalars['uuid']['input']>;
};

/** update columns of table "campaign_user" */
export enum Campaign_User_Update_Column {
  /** column name */
  CampaignId = 'campaign_id',
  /** column name */
  Role = 'role',
  /** column name */
  UserViewId = 'user_view_id'
}

export type Campaign_User_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Campaign_User_Set_Input>;
  /** filter the rows which have to be updated */
  where: Campaign_User_Bool_Exp;
};

/** columns and relationships of "characterization_level" */
export type Characterization_Level = {
  __typename?: 'characterization_level';
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['Int']['output'];
  label: Scalars['String']['output'];
  updated_at: Scalars['timestamptz']['output'];
};

/** aggregated selection of "characterization_level" */
export type Characterization_Level_Aggregate = {
  __typename?: 'characterization_level_aggregate';
  aggregate?: Maybe<Characterization_Level_Aggregate_Fields>;
  nodes: Array<Characterization_Level>;
};

/** aggregate fields of "characterization_level" */
export type Characterization_Level_Aggregate_Fields = {
  __typename?: 'characterization_level_aggregate_fields';
  avg?: Maybe<Characterization_Level_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Characterization_Level_Max_Fields>;
  min?: Maybe<Characterization_Level_Min_Fields>;
  stddev?: Maybe<Characterization_Level_Stddev_Fields>;
  stddev_pop?: Maybe<Characterization_Level_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Characterization_Level_Stddev_Samp_Fields>;
  sum?: Maybe<Characterization_Level_Sum_Fields>;
  var_pop?: Maybe<Characterization_Level_Var_Pop_Fields>;
  var_samp?: Maybe<Characterization_Level_Var_Samp_Fields>;
  variance?: Maybe<Characterization_Level_Variance_Fields>;
};


/** aggregate fields of "characterization_level" */
export type Characterization_Level_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Characterization_Level_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type Characterization_Level_Avg_Fields = {
  __typename?: 'characterization_level_avg_fields';
  id?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "characterization_level". All fields are combined with a logical 'AND'. */
export type Characterization_Level_Bool_Exp = {
  _and?: InputMaybe<Array<Characterization_Level_Bool_Exp>>;
  _not?: InputMaybe<Characterization_Level_Bool_Exp>;
  _or?: InputMaybe<Array<Characterization_Level_Bool_Exp>>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Int_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "characterization_level" */
export enum Characterization_Level_Constraint {
  /** unique or primary key constraint on columns "label" */
  CharacterizationLevelLabelKey = 'characterization_level_label_key',
  /** unique or primary key constraint on columns "id" */
  CharacterizationLevelPkey = 'characterization_level_pkey'
}

/** input type for incrementing numeric columns in table "characterization_level" */
export type Characterization_Level_Inc_Input = {
  id?: InputMaybe<Scalars['Int']['input']>;
};

/** input type for inserting data into table "characterization_level" */
export type Characterization_Level_Insert_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['Int']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Characterization_Level_Max_Fields = {
  __typename?: 'characterization_level_max_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['Int']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Characterization_Level_Min_Fields = {
  __typename?: 'characterization_level_min_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['Int']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "characterization_level" */
export type Characterization_Level_Mutation_Response = {
  __typename?: 'characterization_level_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Characterization_Level>;
};

/** input type for inserting object relation for remote table "characterization_level" */
export type Characterization_Level_Obj_Rel_Insert_Input = {
  data: Characterization_Level_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Characterization_Level_On_Conflict>;
};

/** on_conflict condition type for table "characterization_level" */
export type Characterization_Level_On_Conflict = {
  constraint: Characterization_Level_Constraint;
  update_columns?: Array<Characterization_Level_Update_Column>;
  where?: InputMaybe<Characterization_Level_Bool_Exp>;
};

/** Ordering options when selecting data from "characterization_level". */
export type Characterization_Level_Order_By = {
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: characterization_level */
export type Characterization_Level_Pk_Columns_Input = {
  id: Scalars['Int']['input'];
};

/** select columns of table "characterization_level" */
export enum Characterization_Level_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "characterization_level" */
export type Characterization_Level_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['Int']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate stddev on columns */
export type Characterization_Level_Stddev_Fields = {
  __typename?: 'characterization_level_stddev_fields';
  id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_pop on columns */
export type Characterization_Level_Stddev_Pop_Fields = {
  __typename?: 'characterization_level_stddev_pop_fields';
  id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_samp on columns */
export type Characterization_Level_Stddev_Samp_Fields = {
  __typename?: 'characterization_level_stddev_samp_fields';
  id?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "characterization_level" */
export type Characterization_Level_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Characterization_Level_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Characterization_Level_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['Int']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Characterization_Level_Sum_Fields = {
  __typename?: 'characterization_level_sum_fields';
  id?: Maybe<Scalars['Int']['output']>;
};

/** update columns of table "characterization_level" */
export enum Characterization_Level_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Characterization_Level_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Characterization_Level_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Characterization_Level_Set_Input>;
  /** filter the rows which have to be updated */
  where: Characterization_Level_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Characterization_Level_Var_Pop_Fields = {
  __typename?: 'characterization_level_var_pop_fields';
  id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate var_samp on columns */
export type Characterization_Level_Var_Samp_Fields = {
  __typename?: 'characterization_level_var_samp_fields';
  id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type Characterization_Level_Variance_Fields = {
  __typename?: 'characterization_level_variance_fields';
  id?: Maybe<Scalars['Float']['output']>;
};

/** columns and relationships of "city" */
export type City = {
  __typename?: 'city';
  /** An array relationship */
  cleanups: Array<Cleanup>;
  /** An aggregate relationship */
  cleanups_aggregate: Cleanup_Aggregate;
  code?: Maybe<Scalars['String']['output']>;
  coords?: Maybe<Scalars['geometry']['output']>;
  /** An object relationship */
  departement?: Maybe<Departement>;
  departement_id?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  label?: Maybe<Scalars['String']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
};


/** columns and relationships of "city" */
export type CityCleanupsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};


/** columns and relationships of "city" */
export type CityCleanups_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};

/** aggregated selection of "city" */
export type City_Aggregate = {
  __typename?: 'city_aggregate';
  aggregate?: Maybe<City_Aggregate_Fields>;
  nodes: Array<City>;
};

/** aggregate fields of "city" */
export type City_Aggregate_Fields = {
  __typename?: 'city_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<City_Max_Fields>;
  min?: Maybe<City_Min_Fields>;
};


/** aggregate fields of "city" */
export type City_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<City_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "city". All fields are combined with a logical 'AND'. */
export type City_Bool_Exp = {
  _and?: InputMaybe<Array<City_Bool_Exp>>;
  _not?: InputMaybe<City_Bool_Exp>;
  _or?: InputMaybe<Array<City_Bool_Exp>>;
  cleanups?: InputMaybe<Cleanup_Bool_Exp>;
  cleanups_aggregate?: InputMaybe<Cleanup_Aggregate_Bool_Exp>;
  code?: InputMaybe<String_Comparison_Exp>;
  coords?: InputMaybe<Geometry_Comparison_Exp>;
  departement?: InputMaybe<Departement_Bool_Exp>;
  departement_id?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "city" */
export enum City_Constraint {
  /** unique or primary key constraint on columns "id" */
  CityPk = 'city_pk'
}

/** input type for inserting data into table "city" */
export type City_Insert_Input = {
  cleanups?: InputMaybe<Cleanup_Arr_Rel_Insert_Input>;
  code?: InputMaybe<Scalars['String']['input']>;
  coords?: InputMaybe<Scalars['geometry']['input']>;
  departement?: InputMaybe<Departement_Obj_Rel_Insert_Input>;
  departement_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type City_Max_Fields = {
  __typename?: 'city_max_fields';
  code?: Maybe<Scalars['String']['output']>;
  departement_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
};

/** aggregate min on columns */
export type City_Min_Fields = {
  __typename?: 'city_min_fields';
  code?: Maybe<Scalars['String']['output']>;
  departement_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
};

/** response of any mutation on the table "city" */
export type City_Mutation_Response = {
  __typename?: 'city_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<City>;
};

/** input type for inserting object relation for remote table "city" */
export type City_Obj_Rel_Insert_Input = {
  data: City_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<City_On_Conflict>;
};

/** on_conflict condition type for table "city" */
export type City_On_Conflict = {
  constraint: City_Constraint;
  update_columns?: Array<City_Update_Column>;
  where?: InputMaybe<City_Bool_Exp>;
};

/** Ordering options when selecting data from "city". */
export type City_Order_By = {
  cleanups_aggregate?: InputMaybe<Cleanup_Aggregate_Order_By>;
  code?: InputMaybe<Order_By>;
  coords?: InputMaybe<Order_By>;
  departement?: InputMaybe<Departement_Order_By>;
  departement_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: city */
export type City_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "city" */
export enum City_Select_Column {
  /** column name */
  Code = 'code',
  /** column name */
  Coords = 'coords',
  /** column name */
  DepartementId = 'departement_id',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "city" */
export type City_Set_Input = {
  code?: InputMaybe<Scalars['String']['input']>;
  coords?: InputMaybe<Scalars['geometry']['input']>;
  departement_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "city" */
export type City_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: City_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type City_Stream_Cursor_Value_Input = {
  code?: InputMaybe<Scalars['String']['input']>;
  coords?: InputMaybe<Scalars['geometry']['input']>;
  departement_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "city" */
export enum City_Update_Column {
  /** column name */
  Code = 'code',
  /** column name */
  Coords = 'coords',
  /** column name */
  DepartementId = 'departement_id',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Slug = 'slug'
}

export type City_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<City_Set_Input>;
  /** filter the rows which have to be updated */
  where: City_Bool_Exp;
};

/** columns and relationships of "cleanup" */
export type Cleanup = {
  __typename?: 'cleanup';
  /** An object relationship */
  area_type?: Maybe<Area_Type>;
  area_type_id?: Maybe<Scalars['String']['output']>;
  briefing_date?: Maybe<Scalars['timestamptz']['output']>;
  /** An object relationship */
  city?: Maybe<City>;
  city_id?: Maybe<Scalars['String']['output']>;
  /** An array relationship */
  cleanup_agg_rubbish_category_views: Array<Cleanup_Agg_Rubbish_Category_View>;
  /** An aggregate relationship */
  cleanup_agg_rubbish_category_views_aggregate: Cleanup_Agg_Rubbish_Category_View_Aggregate;
  /** An array relationship */
  cleanup_agg_rubbish_tag_views: Array<Cleanup_Agg_Rubbish_Tag_View>;
  /** An aggregate relationship */
  cleanup_agg_rubbish_tag_views_aggregate: Cleanup_Agg_Rubbish_Tag_View_Aggregate;
  /** An object relationship */
  cleanup_agg_rubbish_view?: Maybe<Cleanup_Agg_Rubbish_View>;
  /** An array relationship */
  cleanup_areas: Array<Cleanup_Area>;
  /** An aggregate relationship */
  cleanup_areas_aggregate: Cleanup_Area_Aggregate;
  /** An array relationship */
  cleanup_campaigns: Array<Cleanup_Campaign>;
  /** An aggregate relationship */
  cleanup_campaigns_aggregate: Cleanup_Campaign_Aggregate;
  /** An array relationship */
  cleanup_forms: Array<Cleanup_Form>;
  /** An aggregate relationship */
  cleanup_forms_aggregate: Cleanup_Form_Aggregate;
  /** An array relationship */
  cleanup_partners: Array<Cleanup_Partner>;
  /** An aggregate relationship */
  cleanup_partners_aggregate: Cleanup_Partner_Aggregate;
  /** An array relationship */
  cleanup_photos: Array<Cleanup_Photo>;
  /** An aggregate relationship */
  cleanup_photos_aggregate: Cleanup_Photo_Aggregate;
  /** An object relationship */
  cleanup_place?: Maybe<Cleanup_Place>;
  cleanup_place_id?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  cleanup_type?: Maybe<Cleanup_Type>;
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  cleanup_view?: Maybe<Cleanup_View>;
  created_at: Scalars['timestamptz']['output'];
  description?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  environment_type?: Maybe<Environment_Type>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id: Scalars['uuid']['output'];
  label: Scalars['String']['output'];
  owner_id?: Maybe<Scalars['uuid']['output']>;
  start_at?: Maybe<Scalars['timestamptz']['output']>;
  start_point?: Maybe<Scalars['jsonb']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  total_area?: Maybe<Scalars['Int']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Agg_Rubbish_Category_ViewsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Agg_Rubbish_Category_Views_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Agg_Rubbish_Tag_ViewsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Agg_Rubbish_Tag_Views_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_AreasArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Area_Order_By>>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Areas_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Area_Order_By>>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_CampaignsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Campaigns_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_FormsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Forms_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_PartnersArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Partners_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_PhotosArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupCleanup_Photos_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


/** columns and relationships of "cleanup" */
export type CleanupStart_PointArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};

/** columns and relationships of "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View = {
  __typename?: 'cleanup_agg_rubbish_category_view';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  ratio_weight?: Maybe<Scalars['numeric']['output']>;
  rubbish_category_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregated selection of "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Aggregate = {
  __typename?: 'cleanup_agg_rubbish_category_view_aggregate';
  aggregate?: Maybe<Cleanup_Agg_Rubbish_Category_View_Aggregate_Fields>;
  nodes: Array<Cleanup_Agg_Rubbish_Category_View>;
};

export type Cleanup_Agg_Rubbish_Category_View_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Agg_Rubbish_Category_View_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Aggregate_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_aggregate_fields';
  avg?: Maybe<Cleanup_Agg_Rubbish_Category_View_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Agg_Rubbish_Category_View_Max_Fields>;
  min?: Maybe<Cleanup_Agg_Rubbish_Category_View_Min_Fields>;
  stddev?: Maybe<Cleanup_Agg_Rubbish_Category_View_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Agg_Rubbish_Category_View_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Agg_Rubbish_Category_View_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Agg_Rubbish_Category_View_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Agg_Rubbish_Category_View_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Agg_Rubbish_Category_View_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Agg_Rubbish_Category_View_Variance_Fields>;
};


/** aggregate fields of "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Max_Order_By>;
  min?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Agg_Rubbish_Category_View_Insert_Input>;
};

/** aggregate avg on columns */
export type Cleanup_Agg_Rubbish_Category_View_Avg_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_avg_fields';
  ratio_weight?: Maybe<Scalars['Float']['output']>;
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Avg_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_agg_rubbish_category_view". All fields are combined with a logical 'AND'. */
export type Cleanup_Agg_Rubbish_Category_View_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  ratio_weight?: InputMaybe<Numeric_Comparison_Exp>;
  rubbish_category_id?: InputMaybe<String_Comparison_Exp>;
  sum_quantity?: InputMaybe<Bigint_Comparison_Exp>;
  sum_volume?: InputMaybe<Numeric_Comparison_Exp>;
  sum_weight?: InputMaybe<Numeric_Comparison_Exp>;
};

/** input type for inserting data into table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Insert_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  ratio_weight?: InputMaybe<Scalars['numeric']['input']>;
  rubbish_category_id?: InputMaybe<Scalars['String']['input']>;
  sum_quantity?: InputMaybe<Scalars['bigint']['input']>;
  sum_volume?: InputMaybe<Scalars['numeric']['input']>;
  sum_weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Agg_Rubbish_Category_View_Max_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_max_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  ratio_weight?: Maybe<Scalars['numeric']['output']>;
  rubbish_category_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by max() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Max_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  ratio_weight?: InputMaybe<Order_By>;
  rubbish_category_id?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Agg_Rubbish_Category_View_Min_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_min_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  ratio_weight?: Maybe<Scalars['numeric']['output']>;
  rubbish_category_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by min() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Min_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  ratio_weight?: InputMaybe<Order_By>;
  rubbish_category_id?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** Ordering options when selecting data from "cleanup_agg_rubbish_category_view". */
export type Cleanup_Agg_Rubbish_Category_View_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  ratio_weight?: InputMaybe<Order_By>;
  rubbish_category_id?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** select columns of table "cleanup_agg_rubbish_category_view" */
export enum Cleanup_Agg_Rubbish_Category_View_Select_Column {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  RatioWeight = 'ratio_weight',
  /** column name */
  RubbishCategoryId = 'rubbish_category_id',
  /** column name */
  SumQuantity = 'sum_quantity',
  /** column name */
  SumVolume = 'sum_volume',
  /** column name */
  SumWeight = 'sum_weight'
}

/** aggregate stddev on columns */
export type Cleanup_Agg_Rubbish_Category_View_Stddev_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_stddev_fields';
  ratio_weight?: Maybe<Scalars['Float']['output']>;
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Stddev_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Agg_Rubbish_Category_View_Stddev_Pop_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_stddev_pop_fields';
  ratio_weight?: Maybe<Scalars['Float']['output']>;
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Stddev_Pop_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Agg_Rubbish_Category_View_Stddev_Samp_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_stddev_samp_fields';
  ratio_weight?: Maybe<Scalars['Float']['output']>;
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Stddev_Samp_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Agg_Rubbish_Category_View_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Agg_Rubbish_Category_View_Stream_Cursor_Value_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  ratio_weight?: InputMaybe<Scalars['numeric']['input']>;
  rubbish_category_id?: InputMaybe<Scalars['String']['input']>;
  sum_quantity?: InputMaybe<Scalars['bigint']['input']>;
  sum_volume?: InputMaybe<Scalars['numeric']['input']>;
  sum_weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Agg_Rubbish_Category_View_Sum_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_sum_fields';
  ratio_weight?: Maybe<Scalars['numeric']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by sum() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Sum_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate var_pop on columns */
export type Cleanup_Agg_Rubbish_Category_View_Var_Pop_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_var_pop_fields';
  ratio_weight?: Maybe<Scalars['Float']['output']>;
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Var_Pop_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Agg_Rubbish_Category_View_Var_Samp_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_var_samp_fields';
  ratio_weight?: Maybe<Scalars['Float']['output']>;
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Var_Samp_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Agg_Rubbish_Category_View_Variance_Fields = {
  __typename?: 'cleanup_agg_rubbish_category_view_variance_fields';
  ratio_weight?: Maybe<Scalars['Float']['output']>;
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_agg_rubbish_category_view" */
export type Cleanup_Agg_Rubbish_Category_View_Variance_Order_By = {
  ratio_weight?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** columns and relationships of "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View = {
  __typename?: 'cleanup_agg_rubbish_tag_view';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_tag_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregated selection of "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Aggregate = {
  __typename?: 'cleanup_agg_rubbish_tag_view_aggregate';
  aggregate?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Aggregate_Fields>;
  nodes: Array<Cleanup_Agg_Rubbish_Tag_View>;
};

export type Cleanup_Agg_Rubbish_Tag_View_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Agg_Rubbish_Tag_View_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Aggregate_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_aggregate_fields';
  avg?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Max_Fields>;
  min?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Min_Fields>;
  stddev?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Agg_Rubbish_Tag_View_Variance_Fields>;
};


/** aggregate fields of "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Max_Order_By>;
  min?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Agg_Rubbish_Tag_View_Insert_Input>;
};

/** aggregate avg on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Avg_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_avg_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Avg_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_agg_rubbish_tag_view". All fields are combined with a logical 'AND'. */
export type Cleanup_Agg_Rubbish_Tag_View_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  rubbish_tag_id?: InputMaybe<String_Comparison_Exp>;
  sum_quantity?: InputMaybe<Bigint_Comparison_Exp>;
  sum_volume?: InputMaybe<Numeric_Comparison_Exp>;
  sum_weight?: InputMaybe<Numeric_Comparison_Exp>;
};

/** input type for inserting data into table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Insert_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_tag_id?: InputMaybe<Scalars['String']['input']>;
  sum_quantity?: InputMaybe<Scalars['bigint']['input']>;
  sum_volume?: InputMaybe<Scalars['numeric']['input']>;
  sum_weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Max_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_max_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_tag_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by max() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Max_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  rubbish_tag_id?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Min_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_min_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_tag_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by min() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Min_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  rubbish_tag_id?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** Ordering options when selecting data from "cleanup_agg_rubbish_tag_view". */
export type Cleanup_Agg_Rubbish_Tag_View_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  rubbish_tag_id?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** select columns of table "cleanup_agg_rubbish_tag_view" */
export enum Cleanup_Agg_Rubbish_Tag_View_Select_Column {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  RubbishTagId = 'rubbish_tag_id',
  /** column name */
  SumQuantity = 'sum_quantity',
  /** column name */
  SumVolume = 'sum_volume',
  /** column name */
  SumWeight = 'sum_weight'
}

/** aggregate stddev on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Stddev_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_stddev_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Stddev_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Stddev_Pop_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_stddev_pop_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Stddev_Pop_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Stddev_Samp_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_stddev_samp_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Stddev_Samp_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Agg_Rubbish_Tag_View_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Agg_Rubbish_Tag_View_Stream_Cursor_Value_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_tag_id?: InputMaybe<Scalars['String']['input']>;
  sum_quantity?: InputMaybe<Scalars['bigint']['input']>;
  sum_volume?: InputMaybe<Scalars['numeric']['input']>;
  sum_weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Sum_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_sum_fields';
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
  sum_volume?: Maybe<Scalars['numeric']['output']>;
  sum_weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by sum() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Sum_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate var_pop on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Var_Pop_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_var_pop_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Var_Pop_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Var_Samp_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_var_samp_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Var_Samp_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Agg_Rubbish_Tag_View_Variance_Fields = {
  __typename?: 'cleanup_agg_rubbish_tag_view_variance_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
  sum_volume?: Maybe<Scalars['Float']['output']>;
  sum_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_agg_rubbish_tag_view" */
export type Cleanup_Agg_Rubbish_Tag_View_Variance_Order_By = {
  sum_quantity?: InputMaybe<Order_By>;
  sum_volume?: InputMaybe<Order_By>;
  sum_weight?: InputMaybe<Order_By>;
};

/** columns and relationships of "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_View = {
  __typename?: 'cleanup_agg_rubbish_view';
  /** An array relationship */
  cleanup_agg_rubbish_view_rubbishes: Array<Rubbish>;
  /** An aggregate relationship */
  cleanup_agg_rubbish_view_rubbishes_aggregate: Rubbish_Aggregate;
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
};


/** columns and relationships of "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_ViewCleanup_Agg_Rubbish_View_RubbishesArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


/** columns and relationships of "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_ViewCleanup_Agg_Rubbish_View_Rubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};

/** aggregated selection of "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_View_Aggregate = {
  __typename?: 'cleanup_agg_rubbish_view_aggregate';
  aggregate?: Maybe<Cleanup_Agg_Rubbish_View_Aggregate_Fields>;
  nodes: Array<Cleanup_Agg_Rubbish_View>;
};

/** aggregate fields of "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_View_Aggregate_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_aggregate_fields';
  avg?: Maybe<Cleanup_Agg_Rubbish_View_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Agg_Rubbish_View_Max_Fields>;
  min?: Maybe<Cleanup_Agg_Rubbish_View_Min_Fields>;
  stddev?: Maybe<Cleanup_Agg_Rubbish_View_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Agg_Rubbish_View_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Agg_Rubbish_View_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Agg_Rubbish_View_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Agg_Rubbish_View_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Agg_Rubbish_View_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Agg_Rubbish_View_Variance_Fields>;
};


/** aggregate fields of "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_View_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type Cleanup_Agg_Rubbish_View_Avg_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_avg_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "cleanup_agg_rubbish_view". All fields are combined with a logical 'AND'. */
export type Cleanup_Agg_Rubbish_View_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Agg_Rubbish_View_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Bool_Exp>>;
  cleanup_agg_rubbish_view_rubbishes?: InputMaybe<Rubbish_Bool_Exp>;
  cleanup_agg_rubbish_view_rubbishes_aggregate?: InputMaybe<Rubbish_Aggregate_Bool_Exp>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  rubbish_id?: InputMaybe<String_Comparison_Exp>;
  sum_quantity?: InputMaybe<Bigint_Comparison_Exp>;
};

/** input type for inserting data into table "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_View_Insert_Input = {
  cleanup_agg_rubbish_view_rubbishes?: InputMaybe<Rubbish_Arr_Rel_Insert_Input>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  sum_quantity?: InputMaybe<Scalars['bigint']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Agg_Rubbish_View_Max_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_max_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
};

/** aggregate min on columns */
export type Cleanup_Agg_Rubbish_View_Min_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_min_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
};

/** input type for inserting object relation for remote table "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_View_Obj_Rel_Insert_Input = {
  data: Cleanup_Agg_Rubbish_View_Insert_Input;
};

/** Ordering options when selecting data from "cleanup_agg_rubbish_view". */
export type Cleanup_Agg_Rubbish_View_Order_By = {
  cleanup_agg_rubbish_view_rubbishes_aggregate?: InputMaybe<Rubbish_Aggregate_Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  sum_quantity?: InputMaybe<Order_By>;
};

/** select columns of table "cleanup_agg_rubbish_view" */
export enum Cleanup_Agg_Rubbish_View_Select_Column {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  SumQuantity = 'sum_quantity'
}

/** aggregate stddev on columns */
export type Cleanup_Agg_Rubbish_View_Stddev_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_stddev_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Agg_Rubbish_View_Stddev_Pop_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_stddev_pop_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Agg_Rubbish_View_Stddev_Samp_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_stddev_samp_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "cleanup_agg_rubbish_view" */
export type Cleanup_Agg_Rubbish_View_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Agg_Rubbish_View_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Agg_Rubbish_View_Stream_Cursor_Value_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  sum_quantity?: InputMaybe<Scalars['bigint']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Agg_Rubbish_View_Sum_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_sum_fields';
  sum_quantity?: Maybe<Scalars['bigint']['output']>;
};

/** aggregate var_pop on columns */
export type Cleanup_Agg_Rubbish_View_Var_Pop_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_var_pop_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
};

/** aggregate var_samp on columns */
export type Cleanup_Agg_Rubbish_View_Var_Samp_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_var_samp_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type Cleanup_Agg_Rubbish_View_Variance_Fields = {
  __typename?: 'cleanup_agg_rubbish_view_variance_fields';
  sum_quantity?: Maybe<Scalars['Float']['output']>;
};

/** aggregated selection of "cleanup" */
export type Cleanup_Aggregate = {
  __typename?: 'cleanup_aggregate';
  aggregate?: Maybe<Cleanup_Aggregate_Fields>;
  nodes: Array<Cleanup>;
};

export type Cleanup_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup" */
export type Cleanup_Aggregate_Fields = {
  __typename?: 'cleanup_aggregate_fields';
  avg?: Maybe<Cleanup_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Max_Fields>;
  min?: Maybe<Cleanup_Min_Fields>;
  stddev?: Maybe<Cleanup_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Variance_Fields>;
};


/** aggregate fields of "cleanup" */
export type Cleanup_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup" */
export type Cleanup_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Max_Order_By>;
  min?: InputMaybe<Cleanup_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Variance_Order_By>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Cleanup_Append_Input = {
  start_point?: InputMaybe<Scalars['jsonb']['input']>;
};

/** columns and relationships of "cleanup_area" */
export type Cleanup_Area = {
  __typename?: 'cleanup_area';
  /** An object relationship */
  cleanup: Cleanup;
  /** An array relationship */
  cleanup_form_rubbish_details: Array<Cleanup_Form_Rubbish_Detail>;
  /** An aggregate relationship */
  cleanup_form_rubbish_details_aggregate: Cleanup_Form_Rubbish_Detail_Aggregate;
  cleanup_id: Scalars['uuid']['output'];
  /** An array relationship */
  cleanup_photos: Array<Cleanup_Photo>;
  /** An aggregate relationship */
  cleanup_photos_aggregate: Cleanup_Photo_Aggregate;
  created_at: Scalars['timestamptz']['output'];
  fill_color?: Maybe<Scalars['String']['output']>;
  geom?: Maybe<Scalars['jsonb']['output']>;
  id: Scalars['uuid']['output'];
  label?: Maybe<Scalars['String']['output']>;
  size?: Maybe<Scalars['Int']['output']>;
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "cleanup_area" */
export type Cleanup_AreaCleanup_Form_Rubbish_DetailsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "cleanup_area" */
export type Cleanup_AreaCleanup_Form_Rubbish_Details_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "cleanup_area" */
export type Cleanup_AreaCleanup_PhotosArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


/** columns and relationships of "cleanup_area" */
export type Cleanup_AreaCleanup_Photos_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


/** columns and relationships of "cleanup_area" */
export type Cleanup_AreaGeomArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};

/** aggregated selection of "cleanup_area" */
export type Cleanup_Area_Aggregate = {
  __typename?: 'cleanup_area_aggregate';
  aggregate?: Maybe<Cleanup_Area_Aggregate_Fields>;
  nodes: Array<Cleanup_Area>;
};

export type Cleanup_Area_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Area_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Area_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Area_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_area" */
export type Cleanup_Area_Aggregate_Fields = {
  __typename?: 'cleanup_area_aggregate_fields';
  avg?: Maybe<Cleanup_Area_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Area_Max_Fields>;
  min?: Maybe<Cleanup_Area_Min_Fields>;
  stddev?: Maybe<Cleanup_Area_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Area_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Area_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Area_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Area_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Area_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Area_Variance_Fields>;
};


/** aggregate fields of "cleanup_area" */
export type Cleanup_Area_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_area" */
export type Cleanup_Area_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Area_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Area_Max_Order_By>;
  min?: InputMaybe<Cleanup_Area_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Area_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Area_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Area_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Area_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Area_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Area_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Area_Variance_Order_By>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Cleanup_Area_Append_Input = {
  geom?: InputMaybe<Scalars['jsonb']['input']>;
};

/** input type for inserting array relation for remote table "cleanup_area" */
export type Cleanup_Area_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Area_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Area_On_Conflict>;
};

/** aggregate avg on columns */
export type Cleanup_Area_Avg_Fields = {
  __typename?: 'cleanup_area_avg_fields';
  size?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_area" */
export type Cleanup_Area_Avg_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_area". All fields are combined with a logical 'AND'. */
export type Cleanup_Area_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Area_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Area_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Area_Bool_Exp>>;
  cleanup?: InputMaybe<Cleanup_Bool_Exp>;
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Bool_Exp>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  cleanup_photos?: InputMaybe<Cleanup_Photo_Bool_Exp>;
  cleanup_photos_aggregate?: InputMaybe<Cleanup_Photo_Aggregate_Bool_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  fill_color?: InputMaybe<String_Comparison_Exp>;
  geom?: InputMaybe<Jsonb_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  size?: InputMaybe<Int_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_area" */
export enum Cleanup_Area_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupAreaPkey = 'cleanup_area_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Cleanup_Area_Delete_At_Path_Input = {
  geom?: InputMaybe<Array<Scalars['String']['input']>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Cleanup_Area_Delete_Elem_Input = {
  geom?: InputMaybe<Scalars['Int']['input']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Cleanup_Area_Delete_Key_Input = {
  geom?: InputMaybe<Scalars['String']['input']>;
};

/** input type for incrementing numeric columns in table "cleanup_area" */
export type Cleanup_Area_Inc_Input = {
  size?: InputMaybe<Scalars['Int']['input']>;
};

/** input type for inserting data into table "cleanup_area" */
export type Cleanup_Area_Insert_Input = {
  cleanup?: InputMaybe<Cleanup_Obj_Rel_Insert_Input>;
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Arr_Rel_Insert_Input>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_photos?: InputMaybe<Cleanup_Photo_Arr_Rel_Insert_Input>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  fill_color?: InputMaybe<Scalars['String']['input']>;
  geom?: InputMaybe<Scalars['jsonb']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Int']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Area_Max_Fields = {
  __typename?: 'cleanup_area_max_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  fill_color?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  size?: Maybe<Scalars['Int']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by max() on columns of table "cleanup_area" */
export type Cleanup_Area_Max_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  fill_color?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  size?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Area_Min_Fields = {
  __typename?: 'cleanup_area_min_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  fill_color?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  size?: Maybe<Scalars['Int']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by min() on columns of table "cleanup_area" */
export type Cleanup_Area_Min_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  fill_color?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  size?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_area" */
export type Cleanup_Area_Mutation_Response = {
  __typename?: 'cleanup_area_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Area>;
};

/** input type for inserting object relation for remote table "cleanup_area" */
export type Cleanup_Area_Obj_Rel_Insert_Input = {
  data: Cleanup_Area_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Area_On_Conflict>;
};

/** on_conflict condition type for table "cleanup_area" */
export type Cleanup_Area_On_Conflict = {
  constraint: Cleanup_Area_Constraint;
  update_columns?: Array<Cleanup_Area_Update_Column>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_area". */
export type Cleanup_Area_Order_By = {
  cleanup?: InputMaybe<Cleanup_Order_By>;
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
  cleanup_photos_aggregate?: InputMaybe<Cleanup_Photo_Aggregate_Order_By>;
  created_at?: InputMaybe<Order_By>;
  fill_color?: InputMaybe<Order_By>;
  geom?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  size?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_area */
export type Cleanup_Area_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Cleanup_Area_Prepend_Input = {
  geom?: InputMaybe<Scalars['jsonb']['input']>;
};

/** select columns of table "cleanup_area" */
export enum Cleanup_Area_Select_Column {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  FillColor = 'fill_color',
  /** column name */
  Geom = 'geom',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Size = 'size',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "cleanup_area" */
export type Cleanup_Area_Set_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  fill_color?: InputMaybe<Scalars['String']['input']>;
  geom?: InputMaybe<Scalars['jsonb']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Int']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Area_Stddev_Fields = {
  __typename?: 'cleanup_area_stddev_fields';
  size?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_area" */
export type Cleanup_Area_Stddev_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Area_Stddev_Pop_Fields = {
  __typename?: 'cleanup_area_stddev_pop_fields';
  size?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_area" */
export type Cleanup_Area_Stddev_Pop_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Area_Stddev_Samp_Fields = {
  __typename?: 'cleanup_area_stddev_samp_fields';
  size?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_area" */
export type Cleanup_Area_Stddev_Samp_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_area" */
export type Cleanup_Area_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Area_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Area_Stream_Cursor_Value_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  fill_color?: InputMaybe<Scalars['String']['input']>;
  geom?: InputMaybe<Scalars['jsonb']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Int']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Area_Sum_Fields = {
  __typename?: 'cleanup_area_sum_fields';
  size?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "cleanup_area" */
export type Cleanup_Area_Sum_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** update columns of table "cleanup_area" */
export enum Cleanup_Area_Update_Column {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  FillColor = 'fill_color',
  /** column name */
  Geom = 'geom',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Size = 'size',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Cleanup_Area_Updates = {
  /** append existing jsonb value of filtered columns with new jsonb value */
  _append?: InputMaybe<Cleanup_Area_Append_Input>;
  /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
  _delete_at_path?: InputMaybe<Cleanup_Area_Delete_At_Path_Input>;
  /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
  _delete_elem?: InputMaybe<Cleanup_Area_Delete_Elem_Input>;
  /** delete key/value pair or string element. key/value pairs are matched based on their key value */
  _delete_key?: InputMaybe<Cleanup_Area_Delete_Key_Input>;
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Area_Inc_Input>;
  /** prepend existing jsonb value of filtered columns with new jsonb value */
  _prepend?: InputMaybe<Cleanup_Area_Prepend_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Area_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Area_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Area_Var_Pop_Fields = {
  __typename?: 'cleanup_area_var_pop_fields';
  size?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_area" */
export type Cleanup_Area_Var_Pop_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Area_Var_Samp_Fields = {
  __typename?: 'cleanup_area_var_samp_fields';
  size?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_area" */
export type Cleanup_Area_Var_Samp_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Area_Variance_Fields = {
  __typename?: 'cleanup_area_variance_fields';
  size?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_area" */
export type Cleanup_Area_Variance_Order_By = {
  size?: InputMaybe<Order_By>;
};

/** input type for inserting array relation for remote table "cleanup" */
export type Cleanup_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_On_Conflict>;
};

/** aggregate avg on columns */
export type Cleanup_Avg_Fields = {
  __typename?: 'cleanup_avg_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup" */
export type Cleanup_Avg_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup". All fields are combined with a logical 'AND'. */
export type Cleanup_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Bool_Exp>>;
  area_type?: InputMaybe<Area_Type_Bool_Exp>;
  area_type_id?: InputMaybe<String_Comparison_Exp>;
  briefing_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  city?: InputMaybe<City_Bool_Exp>;
  city_id?: InputMaybe<String_Comparison_Exp>;
  cleanup_agg_rubbish_category_views?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
  cleanup_agg_rubbish_category_views_aggregate?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Aggregate_Bool_Exp>;
  cleanup_agg_rubbish_tag_views?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
  cleanup_agg_rubbish_tag_views_aggregate?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Aggregate_Bool_Exp>;
  cleanup_agg_rubbish_view?: InputMaybe<Cleanup_Agg_Rubbish_View_Bool_Exp>;
  cleanup_areas?: InputMaybe<Cleanup_Area_Bool_Exp>;
  cleanup_areas_aggregate?: InputMaybe<Cleanup_Area_Aggregate_Bool_Exp>;
  cleanup_campaigns?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
  cleanup_campaigns_aggregate?: InputMaybe<Cleanup_Campaign_Aggregate_Bool_Exp>;
  cleanup_forms?: InputMaybe<Cleanup_Form_Bool_Exp>;
  cleanup_forms_aggregate?: InputMaybe<Cleanup_Form_Aggregate_Bool_Exp>;
  cleanup_partners?: InputMaybe<Cleanup_Partner_Bool_Exp>;
  cleanup_partners_aggregate?: InputMaybe<Cleanup_Partner_Aggregate_Bool_Exp>;
  cleanup_photos?: InputMaybe<Cleanup_Photo_Bool_Exp>;
  cleanup_photos_aggregate?: InputMaybe<Cleanup_Photo_Aggregate_Bool_Exp>;
  cleanup_place?: InputMaybe<Cleanup_Place_Bool_Exp>;
  cleanup_place_id?: InputMaybe<String_Comparison_Exp>;
  cleanup_type?: InputMaybe<Cleanup_Type_Bool_Exp>;
  cleanup_type_id?: InputMaybe<String_Comparison_Exp>;
  cleanup_view?: InputMaybe<Cleanup_View_Bool_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  description?: InputMaybe<String_Comparison_Exp>;
  environment_type?: InputMaybe<Environment_Type_Bool_Exp>;
  environment_type_id?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  owner_id?: InputMaybe<Uuid_Comparison_Exp>;
  start_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  start_point?: InputMaybe<Jsonb_Comparison_Exp>;
  status?: InputMaybe<String_Comparison_Exp>;
  total_area?: InputMaybe<Int_Comparison_Exp>;
  total_linear?: InputMaybe<Int_Comparison_Exp>;
  total_participants?: InputMaybe<Int_Comparison_Exp>;
  total_volume?: InputMaybe<Numeric_Comparison_Exp>;
  total_weight?: InputMaybe<Numeric_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** columns and relationships of "cleanup_campaign" */
export type Cleanup_Campaign = {
  __typename?: 'cleanup_campaign';
  /** An object relationship */
  campaign: Campaign;
  campaign_id: Scalars['String']['output'];
  /** An object relationship */
  cleanup: Cleanup;
  cleanup_id: Scalars['uuid']['output'];
};

/** aggregated selection of "cleanup_campaign" */
export type Cleanup_Campaign_Aggregate = {
  __typename?: 'cleanup_campaign_aggregate';
  aggregate?: Maybe<Cleanup_Campaign_Aggregate_Fields>;
  nodes: Array<Cleanup_Campaign>;
};

export type Cleanup_Campaign_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Campaign_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Campaign_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_campaign" */
export type Cleanup_Campaign_Aggregate_Fields = {
  __typename?: 'cleanup_campaign_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Campaign_Max_Fields>;
  min?: Maybe<Cleanup_Campaign_Min_Fields>;
};


/** aggregate fields of "cleanup_campaign" */
export type Cleanup_Campaign_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_campaign" */
export type Cleanup_Campaign_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Campaign_Max_Order_By>;
  min?: InputMaybe<Cleanup_Campaign_Min_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_campaign" */
export type Cleanup_Campaign_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Campaign_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Campaign_On_Conflict>;
};

/** Boolean expression to filter rows from the table "cleanup_campaign". All fields are combined with a logical 'AND'. */
export type Cleanup_Campaign_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Campaign_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Campaign_Bool_Exp>>;
  campaign?: InputMaybe<Campaign_Bool_Exp>;
  campaign_id?: InputMaybe<String_Comparison_Exp>;
  cleanup?: InputMaybe<Cleanup_Bool_Exp>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_campaign" */
export enum Cleanup_Campaign_Constraint {
  /** unique or primary key constraint on columns "cleanup_id", "campaign_id" */
  CleanupCampaignPkey = 'cleanup_campaign_pkey'
}

/** input type for inserting data into table "cleanup_campaign" */
export type Cleanup_Campaign_Insert_Input = {
  campaign?: InputMaybe<Campaign_Obj_Rel_Insert_Input>;
  campaign_id?: InputMaybe<Scalars['String']['input']>;
  cleanup?: InputMaybe<Cleanup_Obj_Rel_Insert_Input>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Campaign_Max_Fields = {
  __typename?: 'cleanup_campaign_max_fields';
  campaign_id?: Maybe<Scalars['String']['output']>;
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
};

/** order by max() on columns of table "cleanup_campaign" */
export type Cleanup_Campaign_Max_Order_By = {
  campaign_id?: InputMaybe<Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Campaign_Min_Fields = {
  __typename?: 'cleanup_campaign_min_fields';
  campaign_id?: Maybe<Scalars['String']['output']>;
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
};

/** order by min() on columns of table "cleanup_campaign" */
export type Cleanup_Campaign_Min_Order_By = {
  campaign_id?: InputMaybe<Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_campaign" */
export type Cleanup_Campaign_Mutation_Response = {
  __typename?: 'cleanup_campaign_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Campaign>;
};

/** on_conflict condition type for table "cleanup_campaign" */
export type Cleanup_Campaign_On_Conflict = {
  constraint: Cleanup_Campaign_Constraint;
  update_columns?: Array<Cleanup_Campaign_Update_Column>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_campaign". */
export type Cleanup_Campaign_Order_By = {
  campaign?: InputMaybe<Campaign_Order_By>;
  campaign_id?: InputMaybe<Order_By>;
  cleanup?: InputMaybe<Cleanup_Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_campaign */
export type Cleanup_Campaign_Pk_Columns_Input = {
  campaign_id: Scalars['String']['input'];
  cleanup_id: Scalars['uuid']['input'];
};

/** select columns of table "cleanup_campaign" */
export enum Cleanup_Campaign_Select_Column {
  /** column name */
  CampaignId = 'campaign_id',
  /** column name */
  CleanupId = 'cleanup_id'
}

/** input type for updating data in table "cleanup_campaign" */
export type Cleanup_Campaign_Set_Input = {
  campaign_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
};

/** Streaming cursor of the table "cleanup_campaign" */
export type Cleanup_Campaign_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Campaign_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Campaign_Stream_Cursor_Value_Input = {
  campaign_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
};

/** update columns of table "cleanup_campaign" */
export enum Cleanup_Campaign_Update_Column {
  /** column name */
  CampaignId = 'campaign_id',
  /** column name */
  CleanupId = 'cleanup_id'
}

export type Cleanup_Campaign_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Campaign_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Campaign_Bool_Exp;
};

/** unique or primary key constraints on table "cleanup" */
export enum Cleanup_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupPkey = 'cleanup_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Cleanup_Delete_At_Path_Input = {
  start_point?: InputMaybe<Array<Scalars['String']['input']>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Cleanup_Delete_Elem_Input = {
  start_point?: InputMaybe<Scalars['Int']['input']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Cleanup_Delete_Key_Input = {
  start_point?: InputMaybe<Scalars['String']['input']>;
};

/** columns and relationships of "cleanup_form" */
export type Cleanup_Form = {
  __typename?: 'cleanup_form';
  /** An object relationship */
  cleanup?: Maybe<Cleanup>;
  cleanup_duration?: Maybe<Scalars['time']['output']>;
  /** An array relationship */
  cleanup_form_dois: Array<Cleanup_Form_Doi>;
  /** An aggregate relationship */
  cleanup_form_dois_aggregate: Cleanup_Form_Doi_Aggregate;
  /** An array relationship */
  cleanup_form_pois: Array<Cleanup_Form_Poi>;
  /** An aggregate relationship */
  cleanup_form_pois_aggregate: Cleanup_Form_Poi_Aggregate;
  /** An array relationship */
  cleanup_form_rubbish_details: Array<Cleanup_Form_Rubbish_Detail>;
  /** An aggregate relationship */
  cleanup_form_rubbish_details_aggregate: Cleanup_Form_Rubbish_Detail_Aggregate;
  /** An array relationship */
  cleanup_form_rubbishes: Array<Cleanup_Form_Rubbish>;
  /** An aggregate relationship */
  cleanup_form_rubbishes_aggregate: Cleanup_Form_Rubbish_Aggregate;
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  created_at: Scalars['timestamptz']['output'];
  external_volunteers_number?: Maybe<Scalars['Int']['output']>;
  id: Scalars['uuid']['output'];
  owner_id?: Maybe<Scalars['uuid']['output']>;
  performed_at?: Maybe<Scalars['timestamptz']['output']>;
  rain_force?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  rubbish_type_picked?: Maybe<Rubbish_Type_Picked>;
  rubbish_type_picked_id?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at: Scalars['timestamptz']['output'];
  volunteers_number?: Maybe<Scalars['Int']['output']>;
  wind_force?: Maybe<Scalars['String']['output']>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_DoisArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Doi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_Dois_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Doi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_PoisArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Poi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_Pois_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Poi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_Rubbish_DetailsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_Rubbish_Details_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_RubbishesArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


/** columns and relationships of "cleanup_form" */
export type Cleanup_FormCleanup_Form_Rubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};

/** aggregated selection of "cleanup_form" */
export type Cleanup_Form_Aggregate = {
  __typename?: 'cleanup_form_aggregate';
  aggregate?: Maybe<Cleanup_Form_Aggregate_Fields>;
  nodes: Array<Cleanup_Form>;
};

export type Cleanup_Form_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Form_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Form_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Form_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_form" */
export type Cleanup_Form_Aggregate_Fields = {
  __typename?: 'cleanup_form_aggregate_fields';
  avg?: Maybe<Cleanup_Form_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Form_Max_Fields>;
  min?: Maybe<Cleanup_Form_Min_Fields>;
  stddev?: Maybe<Cleanup_Form_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Form_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Form_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Form_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Form_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Form_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Form_Variance_Fields>;
};


/** aggregate fields of "cleanup_form" */
export type Cleanup_Form_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_form" */
export type Cleanup_Form_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Form_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Form_Max_Order_By>;
  min?: InputMaybe<Cleanup_Form_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Form_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Form_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Form_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Form_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Form_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Form_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Form_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_form" */
export type Cleanup_Form_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Form_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Form_On_Conflict>;
};

/** aggregate avg on columns */
export type Cleanup_Form_Avg_Fields = {
  __typename?: 'cleanup_form_avg_fields';
  external_volunteers_number?: Maybe<Scalars['Float']['output']>;
  volunteers_number?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_form" */
export type Cleanup_Form_Avg_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_form". All fields are combined with a logical 'AND'. */
export type Cleanup_Form_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Form_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Form_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Form_Bool_Exp>>;
  cleanup?: InputMaybe<Cleanup_Bool_Exp>;
  cleanup_duration?: InputMaybe<Time_Comparison_Exp>;
  cleanup_form_dois?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
  cleanup_form_dois_aggregate?: InputMaybe<Cleanup_Form_Doi_Aggregate_Bool_Exp>;
  cleanup_form_pois?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
  cleanup_form_pois_aggregate?: InputMaybe<Cleanup_Form_Poi_Aggregate_Bool_Exp>;
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Bool_Exp>;
  cleanup_form_rubbishes?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
  cleanup_form_rubbishes_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Aggregate_Bool_Exp>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  comment?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  external_volunteers_number?: InputMaybe<Int_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  owner_id?: InputMaybe<Uuid_Comparison_Exp>;
  performed_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  rain_force?: InputMaybe<String_Comparison_Exp>;
  rubbish_type_picked?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
  rubbish_type_picked_id?: InputMaybe<String_Comparison_Exp>;
  status?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  volunteers_number?: InputMaybe<Int_Comparison_Exp>;
  wind_force?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_form" */
export enum Cleanup_Form_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupFormPkey = 'cleanup_form_pkey'
}

/** columns and relationships of "cleanup_form_doi" */
export type Cleanup_Form_Doi = {
  __typename?: 'cleanup_form_doi';
  /** An object relationship */
  cleanup_form: Cleanup_Form;
  cleanup_form_id: Scalars['uuid']['output'];
  created_at: Scalars['timestamptz']['output'];
  expiration: Scalars['date']['output'];
  id: Scalars['uuid']['output'];
  /** An object relationship */
  rubbish: Rubbish;
  rubbish_id: Scalars['String']['output'];
  updated_at: Scalars['timestamptz']['output'];
};

/** aggregated selection of "cleanup_form_doi" */
export type Cleanup_Form_Doi_Aggregate = {
  __typename?: 'cleanup_form_doi_aggregate';
  aggregate?: Maybe<Cleanup_Form_Doi_Aggregate_Fields>;
  nodes: Array<Cleanup_Form_Doi>;
};

export type Cleanup_Form_Doi_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Form_Doi_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Form_Doi_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_form_doi" */
export type Cleanup_Form_Doi_Aggregate_Fields = {
  __typename?: 'cleanup_form_doi_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Form_Doi_Max_Fields>;
  min?: Maybe<Cleanup_Form_Doi_Min_Fields>;
};


/** aggregate fields of "cleanup_form_doi" */
export type Cleanup_Form_Doi_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Form_Doi_Max_Order_By>;
  min?: InputMaybe<Cleanup_Form_Doi_Min_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Form_Doi_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Form_Doi_On_Conflict>;
};

/** Boolean expression to filter rows from the table "cleanup_form_doi". All fields are combined with a logical 'AND'. */
export type Cleanup_Form_Doi_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Form_Doi_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Form_Doi_Bool_Exp>>;
  cleanup_form?: InputMaybe<Cleanup_Form_Bool_Exp>;
  cleanup_form_id?: InputMaybe<Uuid_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  expiration?: InputMaybe<Date_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  rubbish?: InputMaybe<Rubbish_Bool_Exp>;
  rubbish_id?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_form_doi" */
export enum Cleanup_Form_Doi_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupFormDoiPkey = 'cleanup_form_doi_pkey'
}

/** input type for inserting data into table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Insert_Input = {
  cleanup_form?: InputMaybe<Cleanup_Form_Obj_Rel_Insert_Input>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  expiration?: InputMaybe<Scalars['date']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish?: InputMaybe<Rubbish_Obj_Rel_Insert_Input>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Form_Doi_Max_Fields = {
  __typename?: 'cleanup_form_doi_max_fields';
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  expiration?: Maybe<Scalars['date']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by max() on columns of table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Max_Order_By = {
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  expiration?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Form_Doi_Min_Fields = {
  __typename?: 'cleanup_form_doi_min_fields';
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  expiration?: Maybe<Scalars['date']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by min() on columns of table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Min_Order_By = {
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  expiration?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Mutation_Response = {
  __typename?: 'cleanup_form_doi_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Form_Doi>;
};

/** on_conflict condition type for table "cleanup_form_doi" */
export type Cleanup_Form_Doi_On_Conflict = {
  constraint: Cleanup_Form_Doi_Constraint;
  update_columns?: Array<Cleanup_Form_Doi_Update_Column>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_form_doi". */
export type Cleanup_Form_Doi_Order_By = {
  cleanup_form?: InputMaybe<Cleanup_Form_Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  expiration?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  rubbish?: InputMaybe<Rubbish_Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_form_doi */
export type Cleanup_Form_Doi_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** select columns of table "cleanup_form_doi" */
export enum Cleanup_Form_Doi_Select_Column {
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Expiration = 'expiration',
  /** column name */
  Id = 'id',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Set_Input = {
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  expiration?: InputMaybe<Scalars['date']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "cleanup_form_doi" */
export type Cleanup_Form_Doi_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Form_Doi_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Form_Doi_Stream_Cursor_Value_Input = {
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  expiration?: InputMaybe<Scalars['date']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "cleanup_form_doi" */
export enum Cleanup_Form_Doi_Update_Column {
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Expiration = 'expiration',
  /** column name */
  Id = 'id',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Cleanup_Form_Doi_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Form_Doi_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Form_Doi_Bool_Exp;
};

/** input type for incrementing numeric columns in table "cleanup_form" */
export type Cleanup_Form_Inc_Input = {
  external_volunteers_number?: InputMaybe<Scalars['Int']['input']>;
  volunteers_number?: InputMaybe<Scalars['Int']['input']>;
};

/** input type for inserting data into table "cleanup_form" */
export type Cleanup_Form_Insert_Input = {
  cleanup?: InputMaybe<Cleanup_Obj_Rel_Insert_Input>;
  cleanup_duration?: InputMaybe<Scalars['time']['input']>;
  cleanup_form_dois?: InputMaybe<Cleanup_Form_Doi_Arr_Rel_Insert_Input>;
  cleanup_form_pois?: InputMaybe<Cleanup_Form_Poi_Arr_Rel_Insert_Input>;
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Arr_Rel_Insert_Input>;
  cleanup_form_rubbishes?: InputMaybe<Cleanup_Form_Rubbish_Arr_Rel_Insert_Input>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  external_volunteers_number?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  performed_at?: InputMaybe<Scalars['timestamptz']['input']>;
  rain_force?: InputMaybe<Scalars['String']['input']>;
  rubbish_type_picked?: InputMaybe<Rubbish_Type_Picked_Obj_Rel_Insert_Input>;
  rubbish_type_picked_id?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volunteers_number?: InputMaybe<Scalars['Int']['input']>;
  wind_force?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Form_Max_Fields = {
  __typename?: 'cleanup_form_max_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  external_volunteers_number?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  owner_id?: Maybe<Scalars['uuid']['output']>;
  performed_at?: Maybe<Scalars['timestamptz']['output']>;
  rain_force?: Maybe<Scalars['String']['output']>;
  rubbish_type_picked_id?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
  volunteers_number?: Maybe<Scalars['Int']['output']>;
  wind_force?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "cleanup_form" */
export type Cleanup_Form_Max_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  external_volunteers_number?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  owner_id?: InputMaybe<Order_By>;
  performed_at?: InputMaybe<Order_By>;
  rain_force?: InputMaybe<Order_By>;
  rubbish_type_picked_id?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
  wind_force?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Form_Min_Fields = {
  __typename?: 'cleanup_form_min_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  external_volunteers_number?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  owner_id?: Maybe<Scalars['uuid']['output']>;
  performed_at?: Maybe<Scalars['timestamptz']['output']>;
  rain_force?: Maybe<Scalars['String']['output']>;
  rubbish_type_picked_id?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
  volunteers_number?: Maybe<Scalars['Int']['output']>;
  wind_force?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "cleanup_form" */
export type Cleanup_Form_Min_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  external_volunteers_number?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  owner_id?: InputMaybe<Order_By>;
  performed_at?: InputMaybe<Order_By>;
  rain_force?: InputMaybe<Order_By>;
  rubbish_type_picked_id?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
  wind_force?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_form" */
export type Cleanup_Form_Mutation_Response = {
  __typename?: 'cleanup_form_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Form>;
};

/** input type for inserting object relation for remote table "cleanup_form" */
export type Cleanup_Form_Obj_Rel_Insert_Input = {
  data: Cleanup_Form_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Form_On_Conflict>;
};

/** on_conflict condition type for table "cleanup_form" */
export type Cleanup_Form_On_Conflict = {
  constraint: Cleanup_Form_Constraint;
  update_columns?: Array<Cleanup_Form_Update_Column>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_form". */
export type Cleanup_Form_Order_By = {
  cleanup?: InputMaybe<Cleanup_Order_By>;
  cleanup_duration?: InputMaybe<Order_By>;
  cleanup_form_dois_aggregate?: InputMaybe<Cleanup_Form_Doi_Aggregate_Order_By>;
  cleanup_form_pois_aggregate?: InputMaybe<Cleanup_Form_Poi_Aggregate_Order_By>;
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Order_By>;
  cleanup_form_rubbishes_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Aggregate_Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  external_volunteers_number?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  owner_id?: InputMaybe<Order_By>;
  performed_at?: InputMaybe<Order_By>;
  rain_force?: InputMaybe<Order_By>;
  rubbish_type_picked?: InputMaybe<Rubbish_Type_Picked_Order_By>;
  rubbish_type_picked_id?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
  wind_force?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_form */
export type Cleanup_Form_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** columns and relationships of "cleanup_form_poi" */
export type Cleanup_Form_Poi = {
  __typename?: 'cleanup_form_poi';
  /** An object relationship */
  cleanup_area?: Maybe<Cleanup_Area>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  /** An object relationship */
  cleanup_form: Cleanup_Form;
  cleanup_form_id: Scalars['uuid']['output'];
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['uuid']['output'];
  nb: Scalars['Int']['output'];
  /** An object relationship */
  poi_type: Poi_Type;
  poi_type_id: Scalars['String']['output'];
  updated_at: Scalars['timestamptz']['output'];
};

/** aggregated selection of "cleanup_form_poi" */
export type Cleanup_Form_Poi_Aggregate = {
  __typename?: 'cleanup_form_poi_aggregate';
  aggregate?: Maybe<Cleanup_Form_Poi_Aggregate_Fields>;
  nodes: Array<Cleanup_Form_Poi>;
};

export type Cleanup_Form_Poi_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Form_Poi_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Form_Poi_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_form_poi" */
export type Cleanup_Form_Poi_Aggregate_Fields = {
  __typename?: 'cleanup_form_poi_aggregate_fields';
  avg?: Maybe<Cleanup_Form_Poi_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Form_Poi_Max_Fields>;
  min?: Maybe<Cleanup_Form_Poi_Min_Fields>;
  stddev?: Maybe<Cleanup_Form_Poi_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Form_Poi_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Form_Poi_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Form_Poi_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Form_Poi_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Form_Poi_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Form_Poi_Variance_Fields>;
};


/** aggregate fields of "cleanup_form_poi" */
export type Cleanup_Form_Poi_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Form_Poi_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Form_Poi_Max_Order_By>;
  min?: InputMaybe<Cleanup_Form_Poi_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Form_Poi_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Form_Poi_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Form_Poi_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Form_Poi_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Form_Poi_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Form_Poi_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Form_Poi_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Form_Poi_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Form_Poi_On_Conflict>;
};

/** aggregate avg on columns */
export type Cleanup_Form_Poi_Avg_Fields = {
  __typename?: 'cleanup_form_poi_avg_fields';
  nb?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Avg_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_form_poi". All fields are combined with a logical 'AND'. */
export type Cleanup_Form_Poi_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Form_Poi_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Form_Poi_Bool_Exp>>;
  cleanup_area?: InputMaybe<Cleanup_Area_Bool_Exp>;
  cleanup_area_id?: InputMaybe<Uuid_Comparison_Exp>;
  cleanup_form?: InputMaybe<Cleanup_Form_Bool_Exp>;
  cleanup_form_id?: InputMaybe<Uuid_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  nb?: InputMaybe<Int_Comparison_Exp>;
  poi_type?: InputMaybe<Poi_Type_Bool_Exp>;
  poi_type_id?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_form_poi" */
export enum Cleanup_Form_Poi_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupFormPoiPkey = 'cleanup_form_poi_pkey'
}

/** input type for incrementing numeric columns in table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Inc_Input = {
  nb?: InputMaybe<Scalars['Int']['input']>;
};

/** input type for inserting data into table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Insert_Input = {
  cleanup_area?: InputMaybe<Cleanup_Area_Obj_Rel_Insert_Input>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form?: InputMaybe<Cleanup_Form_Obj_Rel_Insert_Input>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  nb?: InputMaybe<Scalars['Int']['input']>;
  poi_type?: InputMaybe<Poi_Type_Obj_Rel_Insert_Input>;
  poi_type_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Form_Poi_Max_Fields = {
  __typename?: 'cleanup_form_poi_max_fields';
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  nb?: Maybe<Scalars['Int']['output']>;
  poi_type_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by max() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Max_Order_By = {
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  nb?: InputMaybe<Order_By>;
  poi_type_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Form_Poi_Min_Fields = {
  __typename?: 'cleanup_form_poi_min_fields';
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  nb?: Maybe<Scalars['Int']['output']>;
  poi_type_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by min() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Min_Order_By = {
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  nb?: InputMaybe<Order_By>;
  poi_type_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Mutation_Response = {
  __typename?: 'cleanup_form_poi_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Form_Poi>;
};

/** on_conflict condition type for table "cleanup_form_poi" */
export type Cleanup_Form_Poi_On_Conflict = {
  constraint: Cleanup_Form_Poi_Constraint;
  update_columns?: Array<Cleanup_Form_Poi_Update_Column>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_form_poi". */
export type Cleanup_Form_Poi_Order_By = {
  cleanup_area?: InputMaybe<Cleanup_Area_Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form?: InputMaybe<Cleanup_Form_Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  nb?: InputMaybe<Order_By>;
  poi_type?: InputMaybe<Poi_Type_Order_By>;
  poi_type_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_form_poi */
export type Cleanup_Form_Poi_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** select columns of table "cleanup_form_poi" */
export enum Cleanup_Form_Poi_Select_Column {
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Nb = 'nb',
  /** column name */
  PoiTypeId = 'poi_type_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Set_Input = {
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  nb?: InputMaybe<Scalars['Int']['input']>;
  poi_type_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Form_Poi_Stddev_Fields = {
  __typename?: 'cleanup_form_poi_stddev_fields';
  nb?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Stddev_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Form_Poi_Stddev_Pop_Fields = {
  __typename?: 'cleanup_form_poi_stddev_pop_fields';
  nb?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Stddev_Pop_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Form_Poi_Stddev_Samp_Fields = {
  __typename?: 'cleanup_form_poi_stddev_samp_fields';
  nb?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Stddev_Samp_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Form_Poi_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Form_Poi_Stream_Cursor_Value_Input = {
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  nb?: InputMaybe<Scalars['Int']['input']>;
  poi_type_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Form_Poi_Sum_Fields = {
  __typename?: 'cleanup_form_poi_sum_fields';
  nb?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Sum_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** update columns of table "cleanup_form_poi" */
export enum Cleanup_Form_Poi_Update_Column {
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Nb = 'nb',
  /** column name */
  PoiTypeId = 'poi_type_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Cleanup_Form_Poi_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Form_Poi_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Form_Poi_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Form_Poi_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Form_Poi_Var_Pop_Fields = {
  __typename?: 'cleanup_form_poi_var_pop_fields';
  nb?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Var_Pop_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Form_Poi_Var_Samp_Fields = {
  __typename?: 'cleanup_form_poi_var_samp_fields';
  nb?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Var_Samp_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Form_Poi_Variance_Fields = {
  __typename?: 'cleanup_form_poi_variance_fields';
  nb?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_form_poi" */
export type Cleanup_Form_Poi_Variance_Order_By = {
  nb?: InputMaybe<Order_By>;
};

/** columns and relationships of "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish = {
  __typename?: 'cleanup_form_rubbish';
  /** An object relationship */
  brand?: Maybe<Brand>;
  brand_id?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  cleanup_area?: Maybe<Cleanup_Area>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  /** An object relationship */
  cleanup_form: Cleanup_Form;
  cleanup_form_id: Scalars['uuid']['output'];
  comment?: Maybe<Scalars['String']['output']>;
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['uuid']['output'];
  label?: Maybe<Scalars['String']['output']>;
  quantity?: Maybe<Scalars['Int']['output']>;
  /** An object relationship */
  rubbish: Rubbish;
  rubbish_id: Scalars['String']['output'];
  status?: Maybe<Scalars['String']['output']>;
  updated_at: Scalars['timestamptz']['output'];
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregated selection of "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Aggregate = {
  __typename?: 'cleanup_form_rubbish_aggregate';
  aggregate?: Maybe<Cleanup_Form_Rubbish_Aggregate_Fields>;
  nodes: Array<Cleanup_Form_Rubbish>;
};

export type Cleanup_Form_Rubbish_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Form_Rubbish_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Form_Rubbish_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Aggregate_Fields = {
  __typename?: 'cleanup_form_rubbish_aggregate_fields';
  avg?: Maybe<Cleanup_Form_Rubbish_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Form_Rubbish_Max_Fields>;
  min?: Maybe<Cleanup_Form_Rubbish_Min_Fields>;
  stddev?: Maybe<Cleanup_Form_Rubbish_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Form_Rubbish_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Form_Rubbish_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Form_Rubbish_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Form_Rubbish_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Form_Rubbish_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Form_Rubbish_Variance_Fields>;
};


/** aggregate fields of "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Form_Rubbish_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Form_Rubbish_Max_Order_By>;
  min?: InputMaybe<Cleanup_Form_Rubbish_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Form_Rubbish_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Form_Rubbish_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Form_Rubbish_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Form_Rubbish_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Form_Rubbish_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Form_Rubbish_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Form_Rubbish_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Form_Rubbish_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Form_Rubbish_On_Conflict>;
};

/** aggregate avg on columns */
export type Cleanup_Form_Rubbish_Avg_Fields = {
  __typename?: 'cleanup_form_rubbish_avg_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Avg_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_form_rubbish". All fields are combined with a logical 'AND'. */
export type Cleanup_Form_Rubbish_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Form_Rubbish_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Form_Rubbish_Bool_Exp>>;
  brand?: InputMaybe<Brand_Bool_Exp>;
  brand_id?: InputMaybe<String_Comparison_Exp>;
  cleanup_area?: InputMaybe<Cleanup_Area_Bool_Exp>;
  cleanup_area_id?: InputMaybe<Uuid_Comparison_Exp>;
  cleanup_form?: InputMaybe<Cleanup_Form_Bool_Exp>;
  cleanup_form_id?: InputMaybe<Uuid_Comparison_Exp>;
  comment?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  quantity?: InputMaybe<Int_Comparison_Exp>;
  rubbish?: InputMaybe<Rubbish_Bool_Exp>;
  rubbish_id?: InputMaybe<String_Comparison_Exp>;
  status?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  volume?: InputMaybe<Numeric_Comparison_Exp>;
  weight?: InputMaybe<Numeric_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_form_rubbish" */
export enum Cleanup_Form_Rubbish_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupFormRubbishPkey = 'cleanup_form_rubbish_pkey'
}

/** columns and relationships of "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail = {
  __typename?: 'cleanup_form_rubbish_detail';
  /** An object relationship */
  brand?: Maybe<Brand>;
  brand_id?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  cleanup_area?: Maybe<Cleanup_Area>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  /** An object relationship */
  cleanup_form: Cleanup_Form;
  cleanup_form_id: Scalars['uuid']['output'];
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['uuid']['output'];
  quantity?: Maybe<Scalars['Int']['output']>;
  /** An object relationship */
  rubbish: Rubbish;
  rubbish_id: Scalars['String']['output'];
  updated_at: Scalars['timestamptz']['output'];
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregated selection of "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Aggregate = {
  __typename?: 'cleanup_form_rubbish_detail_aggregate';
  aggregate?: Maybe<Cleanup_Form_Rubbish_Detail_Aggregate_Fields>;
  nodes: Array<Cleanup_Form_Rubbish_Detail>;
};

export type Cleanup_Form_Rubbish_Detail_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Form_Rubbish_Detail_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Aggregate_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_aggregate_fields';
  avg?: Maybe<Cleanup_Form_Rubbish_Detail_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Form_Rubbish_Detail_Max_Fields>;
  min?: Maybe<Cleanup_Form_Rubbish_Detail_Min_Fields>;
  stddev?: Maybe<Cleanup_Form_Rubbish_Detail_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Form_Rubbish_Detail_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Form_Rubbish_Detail_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Form_Rubbish_Detail_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Form_Rubbish_Detail_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Form_Rubbish_Detail_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Form_Rubbish_Detail_Variance_Fields>;
};


/** aggregate fields of "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Form_Rubbish_Detail_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Form_Rubbish_Detail_Max_Order_By>;
  min?: InputMaybe<Cleanup_Form_Rubbish_Detail_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Form_Rubbish_Detail_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Form_Rubbish_Detail_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Form_Rubbish_Detail_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Form_Rubbish_Detail_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Form_Rubbish_Detail_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Form_Rubbish_Detail_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Form_Rubbish_Detail_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Form_Rubbish_Detail_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Form_Rubbish_Detail_On_Conflict>;
};

/** aggregate avg on columns */
export type Cleanup_Form_Rubbish_Detail_Avg_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_avg_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Avg_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_form_rubbish_detail". All fields are combined with a logical 'AND'. */
export type Cleanup_Form_Rubbish_Detail_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Bool_Exp>>;
  brand?: InputMaybe<Brand_Bool_Exp>;
  brand_id?: InputMaybe<String_Comparison_Exp>;
  cleanup_area?: InputMaybe<Cleanup_Area_Bool_Exp>;
  cleanup_area_id?: InputMaybe<Uuid_Comparison_Exp>;
  cleanup_form?: InputMaybe<Cleanup_Form_Bool_Exp>;
  cleanup_form_id?: InputMaybe<Uuid_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  quantity?: InputMaybe<Int_Comparison_Exp>;
  rubbish?: InputMaybe<Rubbish_Bool_Exp>;
  rubbish_id?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  volume?: InputMaybe<Numeric_Comparison_Exp>;
  weight?: InputMaybe<Numeric_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_form_rubbish_detail" */
export enum Cleanup_Form_Rubbish_Detail_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupFormRubbishDetailPkey = 'cleanup_form_rubbish_detail_pkey'
}

/** input type for incrementing numeric columns in table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Inc_Input = {
  quantity?: InputMaybe<Scalars['Int']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** input type for inserting data into table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Insert_Input = {
  brand?: InputMaybe<Brand_Obj_Rel_Insert_Input>;
  brand_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_area?: InputMaybe<Cleanup_Area_Obj_Rel_Insert_Input>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form?: InputMaybe<Cleanup_Form_Obj_Rel_Insert_Input>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  quantity?: InputMaybe<Scalars['Int']['input']>;
  rubbish?: InputMaybe<Rubbish_Obj_Rel_Insert_Input>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Form_Rubbish_Detail_Max_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_max_fields';
  brand_id?: Maybe<Scalars['String']['output']>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  quantity?: Maybe<Scalars['Int']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by max() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Max_Order_By = {
  brand_id?: InputMaybe<Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Form_Rubbish_Detail_Min_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_min_fields';
  brand_id?: Maybe<Scalars['String']['output']>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  quantity?: Maybe<Scalars['Int']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by min() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Min_Order_By = {
  brand_id?: InputMaybe<Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Mutation_Response = {
  __typename?: 'cleanup_form_rubbish_detail_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Form_Rubbish_Detail>;
};

/** on_conflict condition type for table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_On_Conflict = {
  constraint: Cleanup_Form_Rubbish_Detail_Constraint;
  update_columns?: Array<Cleanup_Form_Rubbish_Detail_Update_Column>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_form_rubbish_detail". */
export type Cleanup_Form_Rubbish_Detail_Order_By = {
  brand?: InputMaybe<Brand_Order_By>;
  brand_id?: InputMaybe<Order_By>;
  cleanup_area?: InputMaybe<Cleanup_Area_Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form?: InputMaybe<Cleanup_Form_Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish?: InputMaybe<Rubbish_Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_form_rubbish_detail */
export type Cleanup_Form_Rubbish_Detail_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** select columns of table "cleanup_form_rubbish_detail" */
export enum Cleanup_Form_Rubbish_Detail_Select_Column {
  /** column name */
  BrandId = 'brand_id',
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Volume = 'volume',
  /** column name */
  Weight = 'weight'
}

/** input type for updating data in table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Set_Input = {
  brand_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  quantity?: InputMaybe<Scalars['Int']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Form_Rubbish_Detail_Stddev_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_stddev_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Stddev_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Form_Rubbish_Detail_Stddev_Pop_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_stddev_pop_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Stddev_Pop_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Form_Rubbish_Detail_Stddev_Samp_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_stddev_samp_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Stddev_Samp_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Form_Rubbish_Detail_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Form_Rubbish_Detail_Stream_Cursor_Value_Input = {
  brand_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  quantity?: InputMaybe<Scalars['Int']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Form_Rubbish_Detail_Sum_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_sum_fields';
  quantity?: Maybe<Scalars['Int']['output']>;
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by sum() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Sum_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** update columns of table "cleanup_form_rubbish_detail" */
export enum Cleanup_Form_Rubbish_Detail_Update_Column {
  /** column name */
  BrandId = 'brand_id',
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Volume = 'volume',
  /** column name */
  Weight = 'weight'
}

export type Cleanup_Form_Rubbish_Detail_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Form_Rubbish_Detail_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Form_Rubbish_Detail_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Form_Rubbish_Detail_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Form_Rubbish_Detail_Var_Pop_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_var_pop_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Var_Pop_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Form_Rubbish_Detail_Var_Samp_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_var_samp_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Var_Samp_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Form_Rubbish_Detail_Variance_Fields = {
  __typename?: 'cleanup_form_rubbish_detail_variance_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_form_rubbish_detail" */
export type Cleanup_Form_Rubbish_Detail_Variance_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** input type for incrementing numeric columns in table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Inc_Input = {
  quantity?: InputMaybe<Scalars['Int']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** input type for inserting data into table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Insert_Input = {
  brand?: InputMaybe<Brand_Obj_Rel_Insert_Input>;
  brand_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_area?: InputMaybe<Cleanup_Area_Obj_Rel_Insert_Input>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form?: InputMaybe<Cleanup_Form_Obj_Rel_Insert_Input>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  quantity?: InputMaybe<Scalars['Int']['input']>;
  rubbish?: InputMaybe<Rubbish_Obj_Rel_Insert_Input>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Form_Rubbish_Max_Fields = {
  __typename?: 'cleanup_form_rubbish_max_fields';
  brand_id?: Maybe<Scalars['String']['output']>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  quantity?: Maybe<Scalars['Int']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by max() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Max_Order_By = {
  brand_id?: InputMaybe<Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Form_Rubbish_Min_Fields = {
  __typename?: 'cleanup_form_rubbish_min_fields';
  brand_id?: Maybe<Scalars['String']['output']>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_form_id?: Maybe<Scalars['uuid']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  quantity?: Maybe<Scalars['Int']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by min() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Min_Order_By = {
  brand_id?: InputMaybe<Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Mutation_Response = {
  __typename?: 'cleanup_form_rubbish_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Form_Rubbish>;
};

/** on_conflict condition type for table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_On_Conflict = {
  constraint: Cleanup_Form_Rubbish_Constraint;
  update_columns?: Array<Cleanup_Form_Rubbish_Update_Column>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_form_rubbish". */
export type Cleanup_Form_Rubbish_Order_By = {
  brand?: InputMaybe<Brand_Order_By>;
  brand_id?: InputMaybe<Order_By>;
  cleanup_area?: InputMaybe<Cleanup_Area_Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_form?: InputMaybe<Cleanup_Form_Order_By>;
  cleanup_form_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish?: InputMaybe<Rubbish_Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_form_rubbish */
export type Cleanup_Form_Rubbish_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** select columns of table "cleanup_form_rubbish" */
export enum Cleanup_Form_Rubbish_Select_Column {
  /** column name */
  BrandId = 'brand_id',
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  Status = 'status',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Volume = 'volume',
  /** column name */
  Weight = 'weight'
}

/** input type for updating data in table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Set_Input = {
  brand_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  quantity?: InputMaybe<Scalars['Int']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Form_Rubbish_Stddev_Fields = {
  __typename?: 'cleanup_form_rubbish_stddev_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Stddev_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Form_Rubbish_Stddev_Pop_Fields = {
  __typename?: 'cleanup_form_rubbish_stddev_pop_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Stddev_Pop_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Form_Rubbish_Stddev_Samp_Fields = {
  __typename?: 'cleanup_form_rubbish_stddev_samp_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Stddev_Samp_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Form_Rubbish_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Form_Rubbish_Stream_Cursor_Value_Input = {
  brand_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_form_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  quantity?: InputMaybe<Scalars['Int']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volume?: InputMaybe<Scalars['numeric']['input']>;
  weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Form_Rubbish_Sum_Fields = {
  __typename?: 'cleanup_form_rubbish_sum_fields';
  quantity?: Maybe<Scalars['Int']['output']>;
  volume?: Maybe<Scalars['numeric']['output']>;
  weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by sum() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Sum_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** update columns of table "cleanup_form_rubbish" */
export enum Cleanup_Form_Rubbish_Update_Column {
  /** column name */
  BrandId = 'brand_id',
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupFormId = 'cleanup_form_id',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  Status = 'status',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Volume = 'volume',
  /** column name */
  Weight = 'weight'
}

export type Cleanup_Form_Rubbish_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Form_Rubbish_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Form_Rubbish_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Form_Rubbish_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Form_Rubbish_Var_Pop_Fields = {
  __typename?: 'cleanup_form_rubbish_var_pop_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Var_Pop_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Form_Rubbish_Var_Samp_Fields = {
  __typename?: 'cleanup_form_rubbish_var_samp_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Var_Samp_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Form_Rubbish_Variance_Fields = {
  __typename?: 'cleanup_form_rubbish_variance_fields';
  quantity?: Maybe<Scalars['Float']['output']>;
  volume?: Maybe<Scalars['Float']['output']>;
  weight?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_form_rubbish" */
export type Cleanup_Form_Rubbish_Variance_Order_By = {
  quantity?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** select columns of table "cleanup_form" */
export enum Cleanup_Form_Select_Column {
  /** column name */
  CleanupDuration = 'cleanup_duration',
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  ExternalVolunteersNumber = 'external_volunteers_number',
  /** column name */
  Id = 'id',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  PerformedAt = 'performed_at',
  /** column name */
  RainForce = 'rain_force',
  /** column name */
  RubbishTypePickedId = 'rubbish_type_picked_id',
  /** column name */
  Status = 'status',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  VolunteersNumber = 'volunteers_number',
  /** column name */
  WindForce = 'wind_force'
}

/** input type for updating data in table "cleanup_form" */
export type Cleanup_Form_Set_Input = {
  cleanup_duration?: InputMaybe<Scalars['time']['input']>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  external_volunteers_number?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  performed_at?: InputMaybe<Scalars['timestamptz']['input']>;
  rain_force?: InputMaybe<Scalars['String']['input']>;
  rubbish_type_picked_id?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volunteers_number?: InputMaybe<Scalars['Int']['input']>;
  wind_force?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Form_Stddev_Fields = {
  __typename?: 'cleanup_form_stddev_fields';
  external_volunteers_number?: Maybe<Scalars['Float']['output']>;
  volunteers_number?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_form" */
export type Cleanup_Form_Stddev_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Form_Stddev_Pop_Fields = {
  __typename?: 'cleanup_form_stddev_pop_fields';
  external_volunteers_number?: Maybe<Scalars['Float']['output']>;
  volunteers_number?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_form" */
export type Cleanup_Form_Stddev_Pop_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Form_Stddev_Samp_Fields = {
  __typename?: 'cleanup_form_stddev_samp_fields';
  external_volunteers_number?: Maybe<Scalars['Float']['output']>;
  volunteers_number?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_form" */
export type Cleanup_Form_Stddev_Samp_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_form" */
export type Cleanup_Form_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Form_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Form_Stream_Cursor_Value_Input = {
  cleanup_duration?: InputMaybe<Scalars['time']['input']>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  external_volunteers_number?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  performed_at?: InputMaybe<Scalars['timestamptz']['input']>;
  rain_force?: InputMaybe<Scalars['String']['input']>;
  rubbish_type_picked_id?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
  volunteers_number?: InputMaybe<Scalars['Int']['input']>;
  wind_force?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Form_Sum_Fields = {
  __typename?: 'cleanup_form_sum_fields';
  external_volunteers_number?: Maybe<Scalars['Int']['output']>;
  volunteers_number?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "cleanup_form" */
export type Cleanup_Form_Sum_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** update columns of table "cleanup_form" */
export enum Cleanup_Form_Update_Column {
  /** column name */
  CleanupDuration = 'cleanup_duration',
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  ExternalVolunteersNumber = 'external_volunteers_number',
  /** column name */
  Id = 'id',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  PerformedAt = 'performed_at',
  /** column name */
  RainForce = 'rain_force',
  /** column name */
  RubbishTypePickedId = 'rubbish_type_picked_id',
  /** column name */
  Status = 'status',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  VolunteersNumber = 'volunteers_number',
  /** column name */
  WindForce = 'wind_force'
}

export type Cleanup_Form_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Form_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Form_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Form_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Form_Var_Pop_Fields = {
  __typename?: 'cleanup_form_var_pop_fields';
  external_volunteers_number?: Maybe<Scalars['Float']['output']>;
  volunteers_number?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_form" */
export type Cleanup_Form_Var_Pop_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Form_Var_Samp_Fields = {
  __typename?: 'cleanup_form_var_samp_fields';
  external_volunteers_number?: Maybe<Scalars['Float']['output']>;
  volunteers_number?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_form" */
export type Cleanup_Form_Var_Samp_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Form_Variance_Fields = {
  __typename?: 'cleanup_form_variance_fields';
  external_volunteers_number?: Maybe<Scalars['Float']['output']>;
  volunteers_number?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_form" */
export type Cleanup_Form_Variance_Order_By = {
  external_volunteers_number?: InputMaybe<Order_By>;
  volunteers_number?: InputMaybe<Order_By>;
};

/** input type for incrementing numeric columns in table "cleanup" */
export type Cleanup_Inc_Input = {
  total_area?: InputMaybe<Scalars['Int']['input']>;
  total_linear?: InputMaybe<Scalars['Int']['input']>;
  total_participants?: InputMaybe<Scalars['Int']['input']>;
  total_volume?: InputMaybe<Scalars['numeric']['input']>;
  total_weight?: InputMaybe<Scalars['numeric']['input']>;
};

/** input type for inserting data into table "cleanup" */
export type Cleanup_Insert_Input = {
  area_type?: InputMaybe<Area_Type_Obj_Rel_Insert_Input>;
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  briefing_date?: InputMaybe<Scalars['timestamptz']['input']>;
  city?: InputMaybe<City_Obj_Rel_Insert_Input>;
  city_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_agg_rubbish_category_views?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Arr_Rel_Insert_Input>;
  cleanup_agg_rubbish_tag_views?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Arr_Rel_Insert_Input>;
  cleanup_agg_rubbish_view?: InputMaybe<Cleanup_Agg_Rubbish_View_Obj_Rel_Insert_Input>;
  cleanup_areas?: InputMaybe<Cleanup_Area_Arr_Rel_Insert_Input>;
  cleanup_campaigns?: InputMaybe<Cleanup_Campaign_Arr_Rel_Insert_Input>;
  cleanup_forms?: InputMaybe<Cleanup_Form_Arr_Rel_Insert_Input>;
  cleanup_partners?: InputMaybe<Cleanup_Partner_Arr_Rel_Insert_Input>;
  cleanup_photos?: InputMaybe<Cleanup_Photo_Arr_Rel_Insert_Input>;
  cleanup_place?: InputMaybe<Cleanup_Place_Obj_Rel_Insert_Input>;
  cleanup_place_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_type?: InputMaybe<Cleanup_Type_Obj_Rel_Insert_Input>;
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_view?: InputMaybe<Cleanup_View_Obj_Rel_Insert_Input>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  environment_type?: InputMaybe<Environment_Type_Obj_Rel_Insert_Input>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  start_at?: InputMaybe<Scalars['timestamptz']['input']>;
  start_point?: InputMaybe<Scalars['jsonb']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  total_area?: InputMaybe<Scalars['Int']['input']>;
  total_linear?: InputMaybe<Scalars['Int']['input']>;
  total_participants?: InputMaybe<Scalars['Int']['input']>;
  total_volume?: InputMaybe<Scalars['numeric']['input']>;
  total_weight?: InputMaybe<Scalars['numeric']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Max_Fields = {
  __typename?: 'cleanup_max_fields';
  area_type_id?: Maybe<Scalars['String']['output']>;
  briefing_date?: Maybe<Scalars['timestamptz']['output']>;
  city_id?: Maybe<Scalars['String']['output']>;
  cleanup_place_id?: Maybe<Scalars['String']['output']>;
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  owner_id?: Maybe<Scalars['uuid']['output']>;
  start_at?: Maybe<Scalars['timestamptz']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  total_area?: Maybe<Scalars['Int']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by max() on columns of table "cleanup" */
export type Cleanup_Max_Order_By = {
  area_type_id?: InputMaybe<Order_By>;
  briefing_date?: InputMaybe<Order_By>;
  city_id?: InputMaybe<Order_By>;
  cleanup_place_id?: InputMaybe<Order_By>;
  cleanup_type_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  environment_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  owner_id?: InputMaybe<Order_By>;
  start_at?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Min_Fields = {
  __typename?: 'cleanup_min_fields';
  area_type_id?: Maybe<Scalars['String']['output']>;
  briefing_date?: Maybe<Scalars['timestamptz']['output']>;
  city_id?: Maybe<Scalars['String']['output']>;
  cleanup_place_id?: Maybe<Scalars['String']['output']>;
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  owner_id?: Maybe<Scalars['uuid']['output']>;
  start_at?: Maybe<Scalars['timestamptz']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  total_area?: Maybe<Scalars['Int']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by min() on columns of table "cleanup" */
export type Cleanup_Min_Order_By = {
  area_type_id?: InputMaybe<Order_By>;
  briefing_date?: InputMaybe<Order_By>;
  city_id?: InputMaybe<Order_By>;
  cleanup_place_id?: InputMaybe<Order_By>;
  cleanup_type_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  environment_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  owner_id?: InputMaybe<Order_By>;
  start_at?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup" */
export type Cleanup_Mutation_Response = {
  __typename?: 'cleanup_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup>;
};

/** input type for inserting object relation for remote table "cleanup" */
export type Cleanup_Obj_Rel_Insert_Input = {
  data: Cleanup_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_On_Conflict>;
};

/** on_conflict condition type for table "cleanup" */
export type Cleanup_On_Conflict = {
  constraint: Cleanup_Constraint;
  update_columns?: Array<Cleanup_Update_Column>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup". */
export type Cleanup_Order_By = {
  area_type?: InputMaybe<Area_Type_Order_By>;
  area_type_id?: InputMaybe<Order_By>;
  briefing_date?: InputMaybe<Order_By>;
  city?: InputMaybe<City_Order_By>;
  city_id?: InputMaybe<Order_By>;
  cleanup_agg_rubbish_category_views_aggregate?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Aggregate_Order_By>;
  cleanup_agg_rubbish_tag_views_aggregate?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Aggregate_Order_By>;
  cleanup_agg_rubbish_view?: InputMaybe<Cleanup_Agg_Rubbish_View_Order_By>;
  cleanup_areas_aggregate?: InputMaybe<Cleanup_Area_Aggregate_Order_By>;
  cleanup_campaigns_aggregate?: InputMaybe<Cleanup_Campaign_Aggregate_Order_By>;
  cleanup_forms_aggregate?: InputMaybe<Cleanup_Form_Aggregate_Order_By>;
  cleanup_partners_aggregate?: InputMaybe<Cleanup_Partner_Aggregate_Order_By>;
  cleanup_photos_aggregate?: InputMaybe<Cleanup_Photo_Aggregate_Order_By>;
  cleanup_place?: InputMaybe<Cleanup_Place_Order_By>;
  cleanup_place_id?: InputMaybe<Order_By>;
  cleanup_type?: InputMaybe<Cleanup_Type_Order_By>;
  cleanup_type_id?: InputMaybe<Order_By>;
  cleanup_view?: InputMaybe<Cleanup_View_Order_By>;
  created_at?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  environment_type?: InputMaybe<Environment_Type_Order_By>;
  environment_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  owner_id?: InputMaybe<Order_By>;
  start_at?: InputMaybe<Order_By>;
  start_point?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** columns and relationships of "cleanup_partner" */
export type Cleanup_Partner = {
  __typename?: 'cleanup_partner';
  /** An object relationship */
  cleanup: Cleanup;
  cleanup_id: Scalars['uuid']['output'];
  /** An object relationship */
  partner: Partner;
  partner_id: Scalars['String']['output'];
};

/** aggregated selection of "cleanup_partner" */
export type Cleanup_Partner_Aggregate = {
  __typename?: 'cleanup_partner_aggregate';
  aggregate?: Maybe<Cleanup_Partner_Aggregate_Fields>;
  nodes: Array<Cleanup_Partner>;
};

export type Cleanup_Partner_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Partner_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Partner_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Partner_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_partner" */
export type Cleanup_Partner_Aggregate_Fields = {
  __typename?: 'cleanup_partner_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Partner_Max_Fields>;
  min?: Maybe<Cleanup_Partner_Min_Fields>;
};


/** aggregate fields of "cleanup_partner" */
export type Cleanup_Partner_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_partner" */
export type Cleanup_Partner_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Partner_Max_Order_By>;
  min?: InputMaybe<Cleanup_Partner_Min_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_partner" */
export type Cleanup_Partner_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Partner_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Partner_On_Conflict>;
};

/** Boolean expression to filter rows from the table "cleanup_partner". All fields are combined with a logical 'AND'. */
export type Cleanup_Partner_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Partner_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Partner_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Partner_Bool_Exp>>;
  cleanup?: InputMaybe<Cleanup_Bool_Exp>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  partner?: InputMaybe<Partner_Bool_Exp>;
  partner_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_partner" */
export enum Cleanup_Partner_Constraint {
  /** unique or primary key constraint on columns "cleanup_id", "partner_id" */
  CleanupPartnerPkey = 'cleanup_partner_pkey'
}

/** input type for inserting data into table "cleanup_partner" */
export type Cleanup_Partner_Insert_Input = {
  cleanup?: InputMaybe<Cleanup_Obj_Rel_Insert_Input>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  partner?: InputMaybe<Partner_Obj_Rel_Insert_Input>;
  partner_id?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Partner_Max_Fields = {
  __typename?: 'cleanup_partner_max_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  partner_id?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "cleanup_partner" */
export type Cleanup_Partner_Max_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  partner_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Partner_Min_Fields = {
  __typename?: 'cleanup_partner_min_fields';
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  partner_id?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "cleanup_partner" */
export type Cleanup_Partner_Min_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  partner_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_partner" */
export type Cleanup_Partner_Mutation_Response = {
  __typename?: 'cleanup_partner_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Partner>;
};

/** on_conflict condition type for table "cleanup_partner" */
export type Cleanup_Partner_On_Conflict = {
  constraint: Cleanup_Partner_Constraint;
  update_columns?: Array<Cleanup_Partner_Update_Column>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_partner". */
export type Cleanup_Partner_Order_By = {
  cleanup?: InputMaybe<Cleanup_Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
  partner?: InputMaybe<Partner_Order_By>;
  partner_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_partner */
export type Cleanup_Partner_Pk_Columns_Input = {
  cleanup_id: Scalars['uuid']['input'];
  partner_id: Scalars['String']['input'];
};

/** select columns of table "cleanup_partner" */
export enum Cleanup_Partner_Select_Column {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  PartnerId = 'partner_id'
}

/** input type for updating data in table "cleanup_partner" */
export type Cleanup_Partner_Set_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  partner_id?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "cleanup_partner" */
export type Cleanup_Partner_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Partner_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Partner_Stream_Cursor_Value_Input = {
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  partner_id?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "cleanup_partner" */
export enum Cleanup_Partner_Update_Column {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  PartnerId = 'partner_id'
}

export type Cleanup_Partner_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Partner_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Partner_Bool_Exp;
};

/** columns and relationships of "cleanup_photo" */
export type Cleanup_Photo = {
  __typename?: 'cleanup_photo';
  /** An object relationship */
  cleanup: Cleanup;
  /** An object relationship */
  cleanup_area?: Maybe<Cleanup_Area>;
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_id: Scalars['uuid']['output'];
  comment?: Maybe<Scalars['String']['output']>;
  created_at: Scalars['timestamptz']['output'];
  filename: Scalars['String']['output'];
  id: Scalars['uuid']['output'];
  rank?: Maybe<Scalars['Int']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at: Scalars['timestamptz']['output'];
};

/** aggregated selection of "cleanup_photo" */
export type Cleanup_Photo_Aggregate = {
  __typename?: 'cleanup_photo_aggregate';
  aggregate?: Maybe<Cleanup_Photo_Aggregate_Fields>;
  nodes: Array<Cleanup_Photo>;
};

export type Cleanup_Photo_Aggregate_Bool_Exp = {
  count?: InputMaybe<Cleanup_Photo_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Photo_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Photo_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_photo" */
export type Cleanup_Photo_Aggregate_Fields = {
  __typename?: 'cleanup_photo_aggregate_fields';
  avg?: Maybe<Cleanup_Photo_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Photo_Max_Fields>;
  min?: Maybe<Cleanup_Photo_Min_Fields>;
  stddev?: Maybe<Cleanup_Photo_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Photo_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Photo_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Photo_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Photo_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Photo_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Photo_Variance_Fields>;
};


/** aggregate fields of "cleanup_photo" */
export type Cleanup_Photo_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_photo" */
export type Cleanup_Photo_Aggregate_Order_By = {
  avg?: InputMaybe<Cleanup_Photo_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Photo_Max_Order_By>;
  min?: InputMaybe<Cleanup_Photo_Min_Order_By>;
  stddev?: InputMaybe<Cleanup_Photo_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Cleanup_Photo_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Cleanup_Photo_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Cleanup_Photo_Sum_Order_By>;
  var_pop?: InputMaybe<Cleanup_Photo_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Cleanup_Photo_Var_Samp_Order_By>;
  variance?: InputMaybe<Cleanup_Photo_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_photo" */
export type Cleanup_Photo_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Photo_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Photo_On_Conflict>;
};

/** aggregate avg on columns */
export type Cleanup_Photo_Avg_Fields = {
  __typename?: 'cleanup_photo_avg_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Avg_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "cleanup_photo". All fields are combined with a logical 'AND'. */
export type Cleanup_Photo_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Photo_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Photo_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Photo_Bool_Exp>>;
  cleanup?: InputMaybe<Cleanup_Bool_Exp>;
  cleanup_area?: InputMaybe<Cleanup_Area_Bool_Exp>;
  cleanup_area_id?: InputMaybe<Uuid_Comparison_Exp>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  comment?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  filename?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  rank?: InputMaybe<Int_Comparison_Exp>;
  status?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_photo" */
export enum Cleanup_Photo_Constraint {
  /** unique or primary key constraint on columns "cleanup_id", "filename" */
  CleanupPhotoFilenameCleanupIdKey = 'cleanup_photo_filename_cleanup_id_key',
  /** unique or primary key constraint on columns "id" */
  CleanupPhotoPkey = 'cleanup_photo_pkey'
}

/** input type for incrementing numeric columns in table "cleanup_photo" */
export type Cleanup_Photo_Inc_Input = {
  rank?: InputMaybe<Scalars['Int']['input']>;
};

/** input type for inserting data into table "cleanup_photo" */
export type Cleanup_Photo_Insert_Input = {
  cleanup?: InputMaybe<Cleanup_Obj_Rel_Insert_Input>;
  cleanup_area?: InputMaybe<Cleanup_Area_Obj_Rel_Insert_Input>;
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  rank?: InputMaybe<Scalars['Int']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Photo_Max_Fields = {
  __typename?: 'cleanup_photo_max_fields';
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  filename?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  rank?: Maybe<Scalars['Int']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by max() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Max_Order_By = {
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  filename?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  rank?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Photo_Min_Fields = {
  __typename?: 'cleanup_photo_min_fields';
  cleanup_area_id?: Maybe<Scalars['uuid']['output']>;
  cleanup_id?: Maybe<Scalars['uuid']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  filename?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  rank?: Maybe<Scalars['Int']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by min() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Min_Order_By = {
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  filename?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  rank?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_photo" */
export type Cleanup_Photo_Mutation_Response = {
  __typename?: 'cleanup_photo_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Photo>;
};

/** input type for inserting object relation for remote table "cleanup_photo" */
export type Cleanup_Photo_Obj_Rel_Insert_Input = {
  data: Cleanup_Photo_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Photo_On_Conflict>;
};

/** on_conflict condition type for table "cleanup_photo" */
export type Cleanup_Photo_On_Conflict = {
  constraint: Cleanup_Photo_Constraint;
  update_columns?: Array<Cleanup_Photo_Update_Column>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_photo". */
export type Cleanup_Photo_Order_By = {
  cleanup?: InputMaybe<Cleanup_Order_By>;
  cleanup_area?: InputMaybe<Cleanup_Area_Order_By>;
  cleanup_area_id?: InputMaybe<Order_By>;
  cleanup_id?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  filename?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  rank?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_photo */
export type Cleanup_Photo_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** select columns of table "cleanup_photo" */
export enum Cleanup_Photo_Select_Column {
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Filename = 'filename',
  /** column name */
  Id = 'id',
  /** column name */
  Rank = 'rank',
  /** column name */
  Status = 'status',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "cleanup_photo" */
export type Cleanup_Photo_Set_Input = {
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  rank?: InputMaybe<Scalars['Int']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Photo_Stddev_Fields = {
  __typename?: 'cleanup_photo_stddev_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Stddev_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Photo_Stddev_Pop_Fields = {
  __typename?: 'cleanup_photo_stddev_pop_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Stddev_Pop_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Photo_Stddev_Samp_Fields = {
  __typename?: 'cleanup_photo_stddev_samp_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Stddev_Samp_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup_photo" */
export type Cleanup_Photo_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Photo_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Photo_Stream_Cursor_Value_Input = {
  cleanup_area_id?: InputMaybe<Scalars['uuid']['input']>;
  cleanup_id?: InputMaybe<Scalars['uuid']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  rank?: InputMaybe<Scalars['Int']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Photo_Sum_Fields = {
  __typename?: 'cleanup_photo_sum_fields';
  rank?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Sum_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** update columns of table "cleanup_photo" */
export enum Cleanup_Photo_Update_Column {
  /** column name */
  CleanupAreaId = 'cleanup_area_id',
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Filename = 'filename',
  /** column name */
  Id = 'id',
  /** column name */
  Rank = 'rank',
  /** column name */
  Status = 'status',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Cleanup_Photo_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Photo_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Photo_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Photo_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Photo_Var_Pop_Fields = {
  __typename?: 'cleanup_photo_var_pop_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Var_Pop_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Photo_Var_Samp_Fields = {
  __typename?: 'cleanup_photo_var_samp_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Var_Samp_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Photo_Variance_Fields = {
  __typename?: 'cleanup_photo_variance_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup_photo" */
export type Cleanup_Photo_Variance_Order_By = {
  rank?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup */
export type Cleanup_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** columns and relationships of "cleanup_place" */
export type Cleanup_Place = {
  __typename?: 'cleanup_place';
  /** An object relationship */
  city?: Maybe<City>;
  city_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id: Scalars['String']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregated selection of "cleanup_place" */
export type Cleanup_Place_Aggregate = {
  __typename?: 'cleanup_place_aggregate';
  aggregate?: Maybe<Cleanup_Place_Aggregate_Fields>;
  nodes: Array<Cleanup_Place>;
};

/** aggregate fields of "cleanup_place" */
export type Cleanup_Place_Aggregate_Fields = {
  __typename?: 'cleanup_place_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Place_Max_Fields>;
  min?: Maybe<Cleanup_Place_Min_Fields>;
};


/** aggregate fields of "cleanup_place" */
export type Cleanup_Place_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Place_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "cleanup_place". All fields are combined with a logical 'AND'. */
export type Cleanup_Place_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Place_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Place_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Place_Bool_Exp>>;
  city?: InputMaybe<City_Bool_Exp>;
  city_id?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_place" */
export enum Cleanup_Place_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupPlacePk = 'cleanup_place_pk'
}

/** input type for inserting data into table "cleanup_place" */
export type Cleanup_Place_Insert_Input = {
  city?: InputMaybe<City_Obj_Rel_Insert_Input>;
  city_id?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Place_Max_Fields = {
  __typename?: 'cleanup_place_max_fields';
  city_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Cleanup_Place_Min_Fields = {
  __typename?: 'cleanup_place_min_fields';
  city_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "cleanup_place" */
export type Cleanup_Place_Mutation_Response = {
  __typename?: 'cleanup_place_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Place>;
};

/** input type for inserting object relation for remote table "cleanup_place" */
export type Cleanup_Place_Obj_Rel_Insert_Input = {
  data: Cleanup_Place_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Place_On_Conflict>;
};

/** on_conflict condition type for table "cleanup_place" */
export type Cleanup_Place_On_Conflict = {
  constraint: Cleanup_Place_Constraint;
  update_columns?: Array<Cleanup_Place_Update_Column>;
  where?: InputMaybe<Cleanup_Place_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_place". */
export type Cleanup_Place_Order_By = {
  city?: InputMaybe<City_Order_By>;
  city_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_place */
export type Cleanup_Place_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "cleanup_place" */
export enum Cleanup_Place_Select_Column {
  /** column name */
  CityId = 'city_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "cleanup_place" */
export type Cleanup_Place_Set_Input = {
  city_id?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "cleanup_place" */
export type Cleanup_Place_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Place_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Place_Stream_Cursor_Value_Input = {
  city_id?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "cleanup_place" */
export enum Cleanup_Place_Update_Column {
  /** column name */
  CityId = 'city_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Cleanup_Place_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Place_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Place_Bool_Exp;
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Cleanup_Prepend_Input = {
  start_point?: InputMaybe<Scalars['jsonb']['input']>;
};

/** select columns of table "cleanup" */
export enum Cleanup_Select_Column {
  /** column name */
  AreaTypeId = 'area_type_id',
  /** column name */
  BriefingDate = 'briefing_date',
  /** column name */
  CityId = 'city_id',
  /** column name */
  CleanupPlaceId = 'cleanup_place_id',
  /** column name */
  CleanupTypeId = 'cleanup_type_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  EnvironmentTypeId = 'environment_type_id',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  StartAt = 'start_at',
  /** column name */
  StartPoint = 'start_point',
  /** column name */
  Status = 'status',
  /** column name */
  TotalArea = 'total_area',
  /** column name */
  TotalLinear = 'total_linear',
  /** column name */
  TotalParticipants = 'total_participants',
  /** column name */
  TotalVolume = 'total_volume',
  /** column name */
  TotalWeight = 'total_weight',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "cleanup" */
export type Cleanup_Set_Input = {
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  briefing_date?: InputMaybe<Scalars['timestamptz']['input']>;
  city_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_place_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  start_at?: InputMaybe<Scalars['timestamptz']['input']>;
  start_point?: InputMaybe<Scalars['jsonb']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  total_area?: InputMaybe<Scalars['Int']['input']>;
  total_linear?: InputMaybe<Scalars['Int']['input']>;
  total_participants?: InputMaybe<Scalars['Int']['input']>;
  total_volume?: InputMaybe<Scalars['numeric']['input']>;
  total_weight?: InputMaybe<Scalars['numeric']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Stddev_Fields = {
  __typename?: 'cleanup_stddev_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cleanup" */
export type Cleanup_Stddev_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Stddev_Pop_Fields = {
  __typename?: 'cleanup_stddev_pop_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_pop() on columns of table "cleanup" */
export type Cleanup_Stddev_Pop_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Stddev_Samp_Fields = {
  __typename?: 'cleanup_stddev_samp_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev_samp() on columns of table "cleanup" */
export type Cleanup_Stddev_Samp_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "cleanup" */
export type Cleanup_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Stream_Cursor_Value_Input = {
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  briefing_date?: InputMaybe<Scalars['timestamptz']['input']>;
  city_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_place_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  start_at?: InputMaybe<Scalars['timestamptz']['input']>;
  start_point?: InputMaybe<Scalars['jsonb']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  total_area?: InputMaybe<Scalars['Int']['input']>;
  total_linear?: InputMaybe<Scalars['Int']['input']>;
  total_participants?: InputMaybe<Scalars['Int']['input']>;
  total_volume?: InputMaybe<Scalars['numeric']['input']>;
  total_weight?: InputMaybe<Scalars['numeric']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Sum_Fields = {
  __typename?: 'cleanup_sum_fields';
  total_area?: Maybe<Scalars['Int']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
};

/** order by sum() on columns of table "cleanup" */
export type Cleanup_Sum_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** columns and relationships of "cleanup_type" */
export type Cleanup_Type = {
  __typename?: 'cleanup_type';
  /** An object relationship */
  characterization_level?: Maybe<Characterization_Level>;
  characterization_level_id?: Maybe<Scalars['Int']['output']>;
  /** An array relationship */
  cleanup_type_rubbishes: Array<Cleanup_Type_Rubbish>;
  /** An aggregate relationship */
  cleanup_type_rubbishes_aggregate: Cleanup_Type_Rubbish_Aggregate;
  /** An array relationship */
  cleanups: Array<Cleanup>;
  /** An aggregate relationship */
  cleanups_aggregate: Cleanup_Aggregate;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id: Scalars['String']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};


/** columns and relationships of "cleanup_type" */
export type Cleanup_TypeCleanup_Type_RubbishesArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


/** columns and relationships of "cleanup_type" */
export type Cleanup_TypeCleanup_Type_Rubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


/** columns and relationships of "cleanup_type" */
export type Cleanup_TypeCleanupsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};


/** columns and relationships of "cleanup_type" */
export type Cleanup_TypeCleanups_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};

/** aggregated selection of "cleanup_type" */
export type Cleanup_Type_Aggregate = {
  __typename?: 'cleanup_type_aggregate';
  aggregate?: Maybe<Cleanup_Type_Aggregate_Fields>;
  nodes: Array<Cleanup_Type>;
};

/** aggregate fields of "cleanup_type" */
export type Cleanup_Type_Aggregate_Fields = {
  __typename?: 'cleanup_type_aggregate_fields';
  avg?: Maybe<Cleanup_Type_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Type_Max_Fields>;
  min?: Maybe<Cleanup_Type_Min_Fields>;
  stddev?: Maybe<Cleanup_Type_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_Type_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_Type_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_Type_Sum_Fields>;
  var_pop?: Maybe<Cleanup_Type_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_Type_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_Type_Variance_Fields>;
};


/** aggregate fields of "cleanup_type" */
export type Cleanup_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type Cleanup_Type_Avg_Fields = {
  __typename?: 'cleanup_type_avg_fields';
  characterization_level_id?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "cleanup_type". All fields are combined with a logical 'AND'. */
export type Cleanup_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Type_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Type_Bool_Exp>>;
  characterization_level?: InputMaybe<Characterization_Level_Bool_Exp>;
  characterization_level_id?: InputMaybe<Int_Comparison_Exp>;
  cleanup_type_rubbishes?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
  cleanup_type_rubbishes_aggregate?: InputMaybe<Cleanup_Type_Rubbish_Aggregate_Bool_Exp>;
  cleanups?: InputMaybe<Cleanup_Bool_Exp>;
  cleanups_aggregate?: InputMaybe<Cleanup_Aggregate_Bool_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_type" */
export enum Cleanup_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  CleanupTypePk = 'cleanup_type_pk'
}

/** input type for incrementing numeric columns in table "cleanup_type" */
export type Cleanup_Type_Inc_Input = {
  characterization_level_id?: InputMaybe<Scalars['Int']['input']>;
};

/** input type for inserting data into table "cleanup_type" */
export type Cleanup_Type_Insert_Input = {
  characterization_level?: InputMaybe<Characterization_Level_Obj_Rel_Insert_Input>;
  characterization_level_id?: InputMaybe<Scalars['Int']['input']>;
  cleanup_type_rubbishes?: InputMaybe<Cleanup_Type_Rubbish_Arr_Rel_Insert_Input>;
  cleanups?: InputMaybe<Cleanup_Arr_Rel_Insert_Input>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Type_Max_Fields = {
  __typename?: 'cleanup_type_max_fields';
  characterization_level_id?: Maybe<Scalars['Int']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Cleanup_Type_Min_Fields = {
  __typename?: 'cleanup_type_min_fields';
  characterization_level_id?: Maybe<Scalars['Int']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "cleanup_type" */
export type Cleanup_Type_Mutation_Response = {
  __typename?: 'cleanup_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Type>;
};

/** input type for inserting object relation for remote table "cleanup_type" */
export type Cleanup_Type_Obj_Rel_Insert_Input = {
  data: Cleanup_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Type_On_Conflict>;
};

/** on_conflict condition type for table "cleanup_type" */
export type Cleanup_Type_On_Conflict = {
  constraint: Cleanup_Type_Constraint;
  update_columns?: Array<Cleanup_Type_Update_Column>;
  where?: InputMaybe<Cleanup_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_type". */
export type Cleanup_Type_Order_By = {
  characterization_level?: InputMaybe<Characterization_Level_Order_By>;
  characterization_level_id?: InputMaybe<Order_By>;
  cleanup_type_rubbishes_aggregate?: InputMaybe<Cleanup_Type_Rubbish_Aggregate_Order_By>;
  cleanups_aggregate?: InputMaybe<Cleanup_Aggregate_Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_type */
export type Cleanup_Type_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** columns and relationships of "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish = {
  __typename?: 'cleanup_type_rubbish';
  /** An object relationship */
  cleanup_type: Cleanup_Type;
  cleanup_type_id: Scalars['String']['output'];
  has_qty?: Maybe<Scalars['Boolean']['output']>;
  has_wt_vol?: Maybe<Scalars['Boolean']['output']>;
  /** An object relationship */
  rubbish: Rubbish;
  rubbish_id: Scalars['String']['output'];
};

/** aggregated selection of "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Aggregate = {
  __typename?: 'cleanup_type_rubbish_aggregate';
  aggregate?: Maybe<Cleanup_Type_Rubbish_Aggregate_Fields>;
  nodes: Array<Cleanup_Type_Rubbish>;
};

export type Cleanup_Type_Rubbish_Aggregate_Bool_Exp = {
  bool_and?: InputMaybe<Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_And>;
  bool_or?: InputMaybe<Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_Or>;
  count?: InputMaybe<Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Count>;
};

export type Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_And = {
  arguments: Cleanup_Type_Rubbish_Select_Column_Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_And_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_Or = {
  arguments: Cleanup_Type_Rubbish_Select_Column_Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Aggregate_Fields = {
  __typename?: 'cleanup_type_rubbish_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_Type_Rubbish_Max_Fields>;
  min?: Maybe<Cleanup_Type_Rubbish_Min_Fields>;
};


/** aggregate fields of "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Cleanup_Type_Rubbish_Max_Order_By>;
  min?: InputMaybe<Cleanup_Type_Rubbish_Min_Order_By>;
};

/** input type for inserting array relation for remote table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Arr_Rel_Insert_Input = {
  data: Array<Cleanup_Type_Rubbish_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Cleanup_Type_Rubbish_On_Conflict>;
};

/** Boolean expression to filter rows from the table "cleanup_type_rubbish". All fields are combined with a logical 'AND'. */
export type Cleanup_Type_Rubbish_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_Type_Rubbish_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_Type_Rubbish_Bool_Exp>>;
  cleanup_type?: InputMaybe<Cleanup_Type_Bool_Exp>;
  cleanup_type_id?: InputMaybe<String_Comparison_Exp>;
  has_qty?: InputMaybe<Boolean_Comparison_Exp>;
  has_wt_vol?: InputMaybe<Boolean_Comparison_Exp>;
  rubbish?: InputMaybe<Rubbish_Bool_Exp>;
  rubbish_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "cleanup_type_rubbish" */
export enum Cleanup_Type_Rubbish_Constraint {
  /** unique or primary key constraint on columns "cleanup_type_id", "rubbish_id" */
  CleanupTypeRubbishPkey = 'cleanup_type_rubbish_pkey'
}

/** input type for inserting data into table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Insert_Input = {
  cleanup_type?: InputMaybe<Cleanup_Type_Obj_Rel_Insert_Input>;
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  has_qty?: InputMaybe<Scalars['Boolean']['input']>;
  has_wt_vol?: InputMaybe<Scalars['Boolean']['input']>;
  rubbish?: InputMaybe<Rubbish_Obj_Rel_Insert_Input>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Cleanup_Type_Rubbish_Max_Fields = {
  __typename?: 'cleanup_type_rubbish_max_fields';
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Max_Order_By = {
  cleanup_type_id?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Cleanup_Type_Rubbish_Min_Fields = {
  __typename?: 'cleanup_type_rubbish_min_fields';
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  rubbish_id?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Min_Order_By = {
  cleanup_type_id?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Mutation_Response = {
  __typename?: 'cleanup_type_rubbish_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Cleanup_Type_Rubbish>;
};

/** on_conflict condition type for table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_On_Conflict = {
  constraint: Cleanup_Type_Rubbish_Constraint;
  update_columns?: Array<Cleanup_Type_Rubbish_Update_Column>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};

/** Ordering options when selecting data from "cleanup_type_rubbish". */
export type Cleanup_Type_Rubbish_Order_By = {
  cleanup_type?: InputMaybe<Cleanup_Type_Order_By>;
  cleanup_type_id?: InputMaybe<Order_By>;
  has_qty?: InputMaybe<Order_By>;
  has_wt_vol?: InputMaybe<Order_By>;
  rubbish?: InputMaybe<Rubbish_Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: cleanup_type_rubbish */
export type Cleanup_Type_Rubbish_Pk_Columns_Input = {
  cleanup_type_id: Scalars['String']['input'];
  rubbish_id: Scalars['String']['input'];
};

/** select columns of table "cleanup_type_rubbish" */
export enum Cleanup_Type_Rubbish_Select_Column {
  /** column name */
  CleanupTypeId = 'cleanup_type_id',
  /** column name */
  HasQty = 'has_qty',
  /** column name */
  HasWtVol = 'has_wt_vol',
  /** column name */
  RubbishId = 'rubbish_id'
}

/** select "cleanup_type_rubbish_aggregate_bool_exp_bool_and_arguments_columns" columns of table "cleanup_type_rubbish" */
export enum Cleanup_Type_Rubbish_Select_Column_Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_And_Arguments_Columns {
  /** column name */
  HasQty = 'has_qty',
  /** column name */
  HasWtVol = 'has_wt_vol'
}

/** select "cleanup_type_rubbish_aggregate_bool_exp_bool_or_arguments_columns" columns of table "cleanup_type_rubbish" */
export enum Cleanup_Type_Rubbish_Select_Column_Cleanup_Type_Rubbish_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns {
  /** column name */
  HasQty = 'has_qty',
  /** column name */
  HasWtVol = 'has_wt_vol'
}

/** input type for updating data in table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Set_Input = {
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  has_qty?: InputMaybe<Scalars['Boolean']['input']>;
  has_wt_vol?: InputMaybe<Scalars['Boolean']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "cleanup_type_rubbish" */
export type Cleanup_Type_Rubbish_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Type_Rubbish_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Type_Rubbish_Stream_Cursor_Value_Input = {
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  has_qty?: InputMaybe<Scalars['Boolean']['input']>;
  has_wt_vol?: InputMaybe<Scalars['Boolean']['input']>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "cleanup_type_rubbish" */
export enum Cleanup_Type_Rubbish_Update_Column {
  /** column name */
  CleanupTypeId = 'cleanup_type_id',
  /** column name */
  HasQty = 'has_qty',
  /** column name */
  HasWtVol = 'has_wt_vol',
  /** column name */
  RubbishId = 'rubbish_id'
}

export type Cleanup_Type_Rubbish_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Type_Rubbish_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Type_Rubbish_Bool_Exp;
};

/** select columns of table "cleanup_type" */
export enum Cleanup_Type_Select_Column {
  /** column name */
  CharacterizationLevelId = 'characterization_level_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "cleanup_type" */
export type Cleanup_Type_Set_Input = {
  characterization_level_id?: InputMaybe<Scalars['Int']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate stddev on columns */
export type Cleanup_Type_Stddev_Fields = {
  __typename?: 'cleanup_type_stddev_fields';
  characterization_level_id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_Type_Stddev_Pop_Fields = {
  __typename?: 'cleanup_type_stddev_pop_fields';
  characterization_level_id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_Type_Stddev_Samp_Fields = {
  __typename?: 'cleanup_type_stddev_samp_fields';
  characterization_level_id?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "cleanup_type" */
export type Cleanup_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_Type_Stream_Cursor_Value_Input = {
  characterization_level_id?: InputMaybe<Scalars['Int']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_Type_Sum_Fields = {
  __typename?: 'cleanup_type_sum_fields';
  characterization_level_id?: Maybe<Scalars['Int']['output']>;
};

/** update columns of table "cleanup_type" */
export enum Cleanup_Type_Update_Column {
  /** column name */
  CharacterizationLevelId = 'characterization_level_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Cleanup_Type_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Type_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Type_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Type_Var_Pop_Fields = {
  __typename?: 'cleanup_type_var_pop_fields';
  characterization_level_id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate var_samp on columns */
export type Cleanup_Type_Var_Samp_Fields = {
  __typename?: 'cleanup_type_var_samp_fields';
  characterization_level_id?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type Cleanup_Type_Variance_Fields = {
  __typename?: 'cleanup_type_variance_fields';
  characterization_level_id?: Maybe<Scalars['Float']['output']>;
};

/** update columns of table "cleanup" */
export enum Cleanup_Update_Column {
  /** column name */
  AreaTypeId = 'area_type_id',
  /** column name */
  BriefingDate = 'briefing_date',
  /** column name */
  CityId = 'city_id',
  /** column name */
  CleanupPlaceId = 'cleanup_place_id',
  /** column name */
  CleanupTypeId = 'cleanup_type_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  EnvironmentTypeId = 'environment_type_id',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  StartAt = 'start_at',
  /** column name */
  StartPoint = 'start_point',
  /** column name */
  Status = 'status',
  /** column name */
  TotalArea = 'total_area',
  /** column name */
  TotalLinear = 'total_linear',
  /** column name */
  TotalParticipants = 'total_participants',
  /** column name */
  TotalVolume = 'total_volume',
  /** column name */
  TotalWeight = 'total_weight',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Cleanup_Updates = {
  /** append existing jsonb value of filtered columns with new jsonb value */
  _append?: InputMaybe<Cleanup_Append_Input>;
  /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
  _delete_at_path?: InputMaybe<Cleanup_Delete_At_Path_Input>;
  /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
  _delete_elem?: InputMaybe<Cleanup_Delete_Elem_Input>;
  /** delete key/value pair or string element. key/value pairs are matched based on their key value */
  _delete_key?: InputMaybe<Cleanup_Delete_Key_Input>;
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Cleanup_Inc_Input>;
  /** prepend existing jsonb value of filtered columns with new jsonb value */
  _prepend?: InputMaybe<Cleanup_Prepend_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Cleanup_Set_Input>;
  /** filter the rows which have to be updated */
  where: Cleanup_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Cleanup_Var_Pop_Fields = {
  __typename?: 'cleanup_var_pop_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_pop() on columns of table "cleanup" */
export type Cleanup_Var_Pop_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Cleanup_Var_Samp_Fields = {
  __typename?: 'cleanup_var_samp_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by var_samp() on columns of table "cleanup" */
export type Cleanup_Var_Samp_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Cleanup_Variance_Fields = {
  __typename?: 'cleanup_variance_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cleanup" */
export type Cleanup_Variance_Order_By = {
  total_area?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
};

/** columns and relationships of "cleanup_view" */
export type Cleanup_View = {
  __typename?: 'cleanup_view';
  area_type_id?: Maybe<Scalars['String']['output']>;
  briefing_date?: Maybe<Scalars['timestamptz']['output']>;
  /** An object relationship */
  city?: Maybe<City>;
  city_id?: Maybe<Scalars['String']['output']>;
  /** An array relationship */
  cleanup_campaigns: Array<Cleanup_Campaign>;
  /** An aggregate relationship */
  cleanup_campaigns_aggregate: Cleanup_Campaign_Aggregate;
  /** An array relationship */
  cleanup_forms: Array<Cleanup_Form>;
  /** An aggregate relationship */
  cleanup_forms_aggregate: Cleanup_Form_Aggregate;
  /** An array relationship */
  cleanup_partners: Array<Cleanup_Partner>;
  /** An aggregate relationship */
  cleanup_partners_aggregate: Cleanup_Partner_Aggregate;
  /** An object relationship */
  cleanup_photo?: Maybe<Cleanup_Photo>;
  /** An object relationship */
  cleanup_place?: Maybe<Cleanup_Place>;
  cleanup_place_id?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  cleanup_type?: Maybe<Cleanup_Type>;
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  owner_id?: Maybe<Scalars['uuid']['output']>;
  start_at?: Maybe<Scalars['timestamptz']['output']>;
  start_point?: Maybe<Scalars['jsonb']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  total_area?: Maybe<Scalars['Int']['output']>;
  total_butts?: Maybe<Scalars['bigint']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};


/** columns and relationships of "cleanup_view" */
export type Cleanup_ViewCleanup_CampaignsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


/** columns and relationships of "cleanup_view" */
export type Cleanup_ViewCleanup_Campaigns_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


/** columns and relationships of "cleanup_view" */
export type Cleanup_ViewCleanup_FormsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


/** columns and relationships of "cleanup_view" */
export type Cleanup_ViewCleanup_Forms_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


/** columns and relationships of "cleanup_view" */
export type Cleanup_ViewCleanup_PartnersArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


/** columns and relationships of "cleanup_view" */
export type Cleanup_ViewCleanup_Partners_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


/** columns and relationships of "cleanup_view" */
export type Cleanup_ViewStart_PointArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};

/** aggregated selection of "cleanup_view" */
export type Cleanup_View_Aggregate = {
  __typename?: 'cleanup_view_aggregate';
  aggregate?: Maybe<Cleanup_View_Aggregate_Fields>;
  nodes: Array<Cleanup_View>;
};

/** aggregate fields of "cleanup_view" */
export type Cleanup_View_Aggregate_Fields = {
  __typename?: 'cleanup_view_aggregate_fields';
  avg?: Maybe<Cleanup_View_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Cleanup_View_Max_Fields>;
  min?: Maybe<Cleanup_View_Min_Fields>;
  stddev?: Maybe<Cleanup_View_Stddev_Fields>;
  stddev_pop?: Maybe<Cleanup_View_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Cleanup_View_Stddev_Samp_Fields>;
  sum?: Maybe<Cleanup_View_Sum_Fields>;
  var_pop?: Maybe<Cleanup_View_Var_Pop_Fields>;
  var_samp?: Maybe<Cleanup_View_Var_Samp_Fields>;
  variance?: Maybe<Cleanup_View_Variance_Fields>;
};


/** aggregate fields of "cleanup_view" */
export type Cleanup_View_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cleanup_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type Cleanup_View_Avg_Fields = {
  __typename?: 'cleanup_view_avg_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_butts?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "cleanup_view". All fields are combined with a logical 'AND'. */
export type Cleanup_View_Bool_Exp = {
  _and?: InputMaybe<Array<Cleanup_View_Bool_Exp>>;
  _not?: InputMaybe<Cleanup_View_Bool_Exp>;
  _or?: InputMaybe<Array<Cleanup_View_Bool_Exp>>;
  area_type_id?: InputMaybe<String_Comparison_Exp>;
  briefing_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  city?: InputMaybe<City_Bool_Exp>;
  city_id?: InputMaybe<String_Comparison_Exp>;
  cleanup_campaigns?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
  cleanup_campaigns_aggregate?: InputMaybe<Cleanup_Campaign_Aggregate_Bool_Exp>;
  cleanup_forms?: InputMaybe<Cleanup_Form_Bool_Exp>;
  cleanup_forms_aggregate?: InputMaybe<Cleanup_Form_Aggregate_Bool_Exp>;
  cleanup_partners?: InputMaybe<Cleanup_Partner_Bool_Exp>;
  cleanup_partners_aggregate?: InputMaybe<Cleanup_Partner_Aggregate_Bool_Exp>;
  cleanup_photo?: InputMaybe<Cleanup_Photo_Bool_Exp>;
  cleanup_place?: InputMaybe<Cleanup_Place_Bool_Exp>;
  cleanup_place_id?: InputMaybe<String_Comparison_Exp>;
  cleanup_type?: InputMaybe<Cleanup_Type_Bool_Exp>;
  cleanup_type_id?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  description?: InputMaybe<String_Comparison_Exp>;
  environment_type_id?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  owner_id?: InputMaybe<Uuid_Comparison_Exp>;
  start_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  start_point?: InputMaybe<Jsonb_Comparison_Exp>;
  status?: InputMaybe<String_Comparison_Exp>;
  total_area?: InputMaybe<Int_Comparison_Exp>;
  total_butts?: InputMaybe<Bigint_Comparison_Exp>;
  total_linear?: InputMaybe<Int_Comparison_Exp>;
  total_participants?: InputMaybe<Int_Comparison_Exp>;
  total_volume?: InputMaybe<Numeric_Comparison_Exp>;
  total_weight?: InputMaybe<Numeric_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** input type for inserting data into table "cleanup_view" */
export type Cleanup_View_Insert_Input = {
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  briefing_date?: InputMaybe<Scalars['timestamptz']['input']>;
  city?: InputMaybe<City_Obj_Rel_Insert_Input>;
  city_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_campaigns?: InputMaybe<Cleanup_Campaign_Arr_Rel_Insert_Input>;
  cleanup_forms?: InputMaybe<Cleanup_Form_Arr_Rel_Insert_Input>;
  cleanup_partners?: InputMaybe<Cleanup_Partner_Arr_Rel_Insert_Input>;
  cleanup_photo?: InputMaybe<Cleanup_Photo_Obj_Rel_Insert_Input>;
  cleanup_place?: InputMaybe<Cleanup_Place_Obj_Rel_Insert_Input>;
  cleanup_place_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_type?: InputMaybe<Cleanup_Type_Obj_Rel_Insert_Input>;
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  start_at?: InputMaybe<Scalars['timestamptz']['input']>;
  start_point?: InputMaybe<Scalars['jsonb']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  total_area?: InputMaybe<Scalars['Int']['input']>;
  total_butts?: InputMaybe<Scalars['bigint']['input']>;
  total_linear?: InputMaybe<Scalars['Int']['input']>;
  total_participants?: InputMaybe<Scalars['Int']['input']>;
  total_volume?: InputMaybe<Scalars['numeric']['input']>;
  total_weight?: InputMaybe<Scalars['numeric']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Cleanup_View_Max_Fields = {
  __typename?: 'cleanup_view_max_fields';
  area_type_id?: Maybe<Scalars['String']['output']>;
  briefing_date?: Maybe<Scalars['timestamptz']['output']>;
  city_id?: Maybe<Scalars['String']['output']>;
  cleanup_place_id?: Maybe<Scalars['String']['output']>;
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  owner_id?: Maybe<Scalars['uuid']['output']>;
  start_at?: Maybe<Scalars['timestamptz']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  total_area?: Maybe<Scalars['Int']['output']>;
  total_butts?: Maybe<Scalars['bigint']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Cleanup_View_Min_Fields = {
  __typename?: 'cleanup_view_min_fields';
  area_type_id?: Maybe<Scalars['String']['output']>;
  briefing_date?: Maybe<Scalars['timestamptz']['output']>;
  city_id?: Maybe<Scalars['String']['output']>;
  cleanup_place_id?: Maybe<Scalars['String']['output']>;
  cleanup_type_id?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  owner_id?: Maybe<Scalars['uuid']['output']>;
  start_at?: Maybe<Scalars['timestamptz']['output']>;
  status?: Maybe<Scalars['String']['output']>;
  total_area?: Maybe<Scalars['Int']['output']>;
  total_butts?: Maybe<Scalars['bigint']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** input type for inserting object relation for remote table "cleanup_view" */
export type Cleanup_View_Obj_Rel_Insert_Input = {
  data: Cleanup_View_Insert_Input;
};

/** Ordering options when selecting data from "cleanup_view". */
export type Cleanup_View_Order_By = {
  area_type_id?: InputMaybe<Order_By>;
  briefing_date?: InputMaybe<Order_By>;
  city?: InputMaybe<City_Order_By>;
  city_id?: InputMaybe<Order_By>;
  cleanup_campaigns_aggregate?: InputMaybe<Cleanup_Campaign_Aggregate_Order_By>;
  cleanup_forms_aggregate?: InputMaybe<Cleanup_Form_Aggregate_Order_By>;
  cleanup_partners_aggregate?: InputMaybe<Cleanup_Partner_Aggregate_Order_By>;
  cleanup_photo?: InputMaybe<Cleanup_Photo_Order_By>;
  cleanup_place?: InputMaybe<Cleanup_Place_Order_By>;
  cleanup_place_id?: InputMaybe<Order_By>;
  cleanup_type?: InputMaybe<Cleanup_Type_Order_By>;
  cleanup_type_id?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  environment_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  owner_id?: InputMaybe<Order_By>;
  start_at?: InputMaybe<Order_By>;
  start_point?: InputMaybe<Order_By>;
  status?: InputMaybe<Order_By>;
  total_area?: InputMaybe<Order_By>;
  total_butts?: InputMaybe<Order_By>;
  total_linear?: InputMaybe<Order_By>;
  total_participants?: InputMaybe<Order_By>;
  total_volume?: InputMaybe<Order_By>;
  total_weight?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** select columns of table "cleanup_view" */
export enum Cleanup_View_Select_Column {
  /** column name */
  AreaTypeId = 'area_type_id',
  /** column name */
  BriefingDate = 'briefing_date',
  /** column name */
  CityId = 'city_id',
  /** column name */
  CleanupPlaceId = 'cleanup_place_id',
  /** column name */
  CleanupTypeId = 'cleanup_type_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  EnvironmentTypeId = 'environment_type_id',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  StartAt = 'start_at',
  /** column name */
  StartPoint = 'start_point',
  /** column name */
  Status = 'status',
  /** column name */
  TotalArea = 'total_area',
  /** column name */
  TotalButts = 'total_butts',
  /** column name */
  TotalLinear = 'total_linear',
  /** column name */
  TotalParticipants = 'total_participants',
  /** column name */
  TotalVolume = 'total_volume',
  /** column name */
  TotalWeight = 'total_weight',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate stddev on columns */
export type Cleanup_View_Stddev_Fields = {
  __typename?: 'cleanup_view_stddev_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_butts?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_pop on columns */
export type Cleanup_View_Stddev_Pop_Fields = {
  __typename?: 'cleanup_view_stddev_pop_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_butts?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_samp on columns */
export type Cleanup_View_Stddev_Samp_Fields = {
  __typename?: 'cleanup_view_stddev_samp_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_butts?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "cleanup_view" */
export type Cleanup_View_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cleanup_View_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Cleanup_View_Stream_Cursor_Value_Input = {
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  briefing_date?: InputMaybe<Scalars['timestamptz']['input']>;
  city_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_place_id?: InputMaybe<Scalars['String']['input']>;
  cleanup_type_id?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  owner_id?: InputMaybe<Scalars['uuid']['input']>;
  start_at?: InputMaybe<Scalars['timestamptz']['input']>;
  start_point?: InputMaybe<Scalars['jsonb']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  total_area?: InputMaybe<Scalars['Int']['input']>;
  total_butts?: InputMaybe<Scalars['bigint']['input']>;
  total_linear?: InputMaybe<Scalars['Int']['input']>;
  total_participants?: InputMaybe<Scalars['Int']['input']>;
  total_volume?: InputMaybe<Scalars['numeric']['input']>;
  total_weight?: InputMaybe<Scalars['numeric']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Cleanup_View_Sum_Fields = {
  __typename?: 'cleanup_view_sum_fields';
  total_area?: Maybe<Scalars['Int']['output']>;
  total_butts?: Maybe<Scalars['bigint']['output']>;
  total_linear?: Maybe<Scalars['Int']['output']>;
  total_participants?: Maybe<Scalars['Int']['output']>;
  total_volume?: Maybe<Scalars['numeric']['output']>;
  total_weight?: Maybe<Scalars['numeric']['output']>;
};

/** aggregate var_pop on columns */
export type Cleanup_View_Var_Pop_Fields = {
  __typename?: 'cleanup_view_var_pop_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_butts?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate var_samp on columns */
export type Cleanup_View_Var_Samp_Fields = {
  __typename?: 'cleanup_view_var_samp_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_butts?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type Cleanup_View_Variance_Fields = {
  __typename?: 'cleanup_view_variance_fields';
  total_area?: Maybe<Scalars['Float']['output']>;
  total_butts?: Maybe<Scalars['Float']['output']>;
  total_linear?: Maybe<Scalars['Float']['output']>;
  total_participants?: Maybe<Scalars['Float']['output']>;
  total_volume?: Maybe<Scalars['Float']['output']>;
  total_weight?: Maybe<Scalars['Float']['output']>;
};

/** ordering argument of a cursor */
export enum Cursor_Ordering {
  /** ascending ordering of the cursor */
  Asc = 'ASC',
  /** descending ordering of the cursor */
  Desc = 'DESC'
}

/** Boolean expression to compare columns of type "date". All fields are combined with logical 'AND'. */
export type Date_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['date']['input']>;
  _gt?: InputMaybe<Scalars['date']['input']>;
  _gte?: InputMaybe<Scalars['date']['input']>;
  _in?: InputMaybe<Array<Scalars['date']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['date']['input']>;
  _lte?: InputMaybe<Scalars['date']['input']>;
  _neq?: InputMaybe<Scalars['date']['input']>;
  _nin?: InputMaybe<Array<Scalars['date']['input']>>;
};

/** columns and relationships of "departement" */
export type Departement = {
  __typename?: 'departement';
  id: Scalars['String']['output'];
  label?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  region?: Maybe<Region>;
  region_id?: Maybe<Scalars['String']['output']>;
};

/** aggregated selection of "departement" */
export type Departement_Aggregate = {
  __typename?: 'departement_aggregate';
  aggregate?: Maybe<Departement_Aggregate_Fields>;
  nodes: Array<Departement>;
};

export type Departement_Aggregate_Bool_Exp = {
  count?: InputMaybe<Departement_Aggregate_Bool_Exp_Count>;
};

export type Departement_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Departement_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Departement_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "departement" */
export type Departement_Aggregate_Fields = {
  __typename?: 'departement_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Departement_Max_Fields>;
  min?: Maybe<Departement_Min_Fields>;
};


/** aggregate fields of "departement" */
export type Departement_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Departement_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "departement" */
export type Departement_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Departement_Max_Order_By>;
  min?: InputMaybe<Departement_Min_Order_By>;
};

/** input type for inserting array relation for remote table "departement" */
export type Departement_Arr_Rel_Insert_Input = {
  data: Array<Departement_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Departement_On_Conflict>;
};

/** Boolean expression to filter rows from the table "departement". All fields are combined with a logical 'AND'. */
export type Departement_Bool_Exp = {
  _and?: InputMaybe<Array<Departement_Bool_Exp>>;
  _not?: InputMaybe<Departement_Bool_Exp>;
  _or?: InputMaybe<Array<Departement_Bool_Exp>>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  region?: InputMaybe<Region_Bool_Exp>;
  region_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "departement" */
export enum Departement_Constraint {
  /** unique or primary key constraint on columns "id" */
  DepartementPk = 'departement_pk'
}

/** input type for inserting data into table "departement" */
export type Departement_Insert_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  region?: InputMaybe<Region_Obj_Rel_Insert_Input>;
  region_id?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Departement_Max_Fields = {
  __typename?: 'departement_max_fields';
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  region_id?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "departement" */
export type Departement_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  region_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Departement_Min_Fields = {
  __typename?: 'departement_min_fields';
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  region_id?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "departement" */
export type Departement_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  region_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "departement" */
export type Departement_Mutation_Response = {
  __typename?: 'departement_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Departement>;
};

/** input type for inserting object relation for remote table "departement" */
export type Departement_Obj_Rel_Insert_Input = {
  data: Departement_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Departement_On_Conflict>;
};

/** on_conflict condition type for table "departement" */
export type Departement_On_Conflict = {
  constraint: Departement_Constraint;
  update_columns?: Array<Departement_Update_Column>;
  where?: InputMaybe<Departement_Bool_Exp>;
};

/** Ordering options when selecting data from "departement". */
export type Departement_Order_By = {
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  region?: InputMaybe<Region_Order_By>;
  region_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: departement */
export type Departement_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "departement" */
export enum Departement_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  RegionId = 'region_id'
}

/** input type for updating data in table "departement" */
export type Departement_Set_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  region_id?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "departement" */
export type Departement_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Departement_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Departement_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  region_id?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "departement" */
export enum Departement_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  RegionId = 'region_id'
}

export type Departement_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Departement_Set_Input>;
  /** filter the rows which have to be updated */
  where: Departement_Bool_Exp;
};

/** columns and relationships of "environment_area_type" */
export type Environment_Area_Type = {
  __typename?: 'environment_area_type';
  /** An object relationship */
  area_type: Area_Type;
  area_type_id: Scalars['String']['output'];
  /** An array relationship */
  environment_area_type_rubbish_type_pickeds: Array<Environment_Area_Type_Rubbish_Type_Picked>;
  /** An aggregate relationship */
  environment_area_type_rubbish_type_pickeds_aggregate: Environment_Area_Type_Rubbish_Type_Picked_Aggregate;
  /** An object relationship */
  environment_type: Environment_Type;
  environment_type_id: Scalars['String']['output'];
  id: Scalars['uuid']['output'];
  is_area?: Maybe<Scalars['Boolean']['output']>;
  is_linear?: Maybe<Scalars['Boolean']['output']>;
};


/** columns and relationships of "environment_area_type" */
export type Environment_Area_TypeEnvironment_Area_Type_Rubbish_Type_PickedsArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};


/** columns and relationships of "environment_area_type" */
export type Environment_Area_TypeEnvironment_Area_Type_Rubbish_Type_Pickeds_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};

/** aggregated selection of "environment_area_type" */
export type Environment_Area_Type_Aggregate = {
  __typename?: 'environment_area_type_aggregate';
  aggregate?: Maybe<Environment_Area_Type_Aggregate_Fields>;
  nodes: Array<Environment_Area_Type>;
};

export type Environment_Area_Type_Aggregate_Bool_Exp = {
  bool_and?: InputMaybe<Environment_Area_Type_Aggregate_Bool_Exp_Bool_And>;
  bool_or?: InputMaybe<Environment_Area_Type_Aggregate_Bool_Exp_Bool_Or>;
  count?: InputMaybe<Environment_Area_Type_Aggregate_Bool_Exp_Count>;
};

export type Environment_Area_Type_Aggregate_Bool_Exp_Bool_And = {
  arguments: Environment_Area_Type_Select_Column_Environment_Area_Type_Aggregate_Bool_Exp_Bool_And_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Environment_Area_Type_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Environment_Area_Type_Aggregate_Bool_Exp_Bool_Or = {
  arguments: Environment_Area_Type_Select_Column_Environment_Area_Type_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Environment_Area_Type_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Environment_Area_Type_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Environment_Area_Type_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "environment_area_type" */
export type Environment_Area_Type_Aggregate_Fields = {
  __typename?: 'environment_area_type_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Environment_Area_Type_Max_Fields>;
  min?: Maybe<Environment_Area_Type_Min_Fields>;
};


/** aggregate fields of "environment_area_type" */
export type Environment_Area_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "environment_area_type" */
export type Environment_Area_Type_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Environment_Area_Type_Max_Order_By>;
  min?: InputMaybe<Environment_Area_Type_Min_Order_By>;
};

/** input type for inserting array relation for remote table "environment_area_type" */
export type Environment_Area_Type_Arr_Rel_Insert_Input = {
  data: Array<Environment_Area_Type_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Environment_Area_Type_On_Conflict>;
};

/** Boolean expression to filter rows from the table "environment_area_type". All fields are combined with a logical 'AND'. */
export type Environment_Area_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Environment_Area_Type_Bool_Exp>>;
  _not?: InputMaybe<Environment_Area_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Environment_Area_Type_Bool_Exp>>;
  area_type?: InputMaybe<Area_Type_Bool_Exp>;
  area_type_id?: InputMaybe<String_Comparison_Exp>;
  environment_area_type_rubbish_type_pickeds?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
  environment_area_type_rubbish_type_pickeds_aggregate?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Bool_Exp>;
  environment_type?: InputMaybe<Environment_Type_Bool_Exp>;
  environment_type_id?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  is_area?: InputMaybe<Boolean_Comparison_Exp>;
  is_linear?: InputMaybe<Boolean_Comparison_Exp>;
};

/** unique or primary key constraints on table "environment_area_type" */
export enum Environment_Area_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  EnvironmentAreaTypeIdKey = 'environment_area_type_id_key',
  /** unique or primary key constraint on columns "id" */
  EnvironmentAreaTypePkey = 'environment_area_type_pkey'
}

/** input type for inserting data into table "environment_area_type" */
export type Environment_Area_Type_Insert_Input = {
  area_type?: InputMaybe<Area_Type_Obj_Rel_Insert_Input>;
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  environment_area_type_rubbish_type_pickeds?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Arr_Rel_Insert_Input>;
  environment_type?: InputMaybe<Environment_Type_Obj_Rel_Insert_Input>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  is_area?: InputMaybe<Scalars['Boolean']['input']>;
  is_linear?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate max on columns */
export type Environment_Area_Type_Max_Fields = {
  __typename?: 'environment_area_type_max_fields';
  area_type_id?: Maybe<Scalars['String']['output']>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
};

/** order by max() on columns of table "environment_area_type" */
export type Environment_Area_Type_Max_Order_By = {
  area_type_id?: InputMaybe<Order_By>;
  environment_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Environment_Area_Type_Min_Fields = {
  __typename?: 'environment_area_type_min_fields';
  area_type_id?: Maybe<Scalars['String']['output']>;
  environment_type_id?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
};

/** order by min() on columns of table "environment_area_type" */
export type Environment_Area_Type_Min_Order_By = {
  area_type_id?: InputMaybe<Order_By>;
  environment_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "environment_area_type" */
export type Environment_Area_Type_Mutation_Response = {
  __typename?: 'environment_area_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Environment_Area_Type>;
};

/** input type for inserting object relation for remote table "environment_area_type" */
export type Environment_Area_Type_Obj_Rel_Insert_Input = {
  data: Environment_Area_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Environment_Area_Type_On_Conflict>;
};

/** on_conflict condition type for table "environment_area_type" */
export type Environment_Area_Type_On_Conflict = {
  constraint: Environment_Area_Type_Constraint;
  update_columns?: Array<Environment_Area_Type_Update_Column>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "environment_area_type". */
export type Environment_Area_Type_Order_By = {
  area_type?: InputMaybe<Area_Type_Order_By>;
  area_type_id?: InputMaybe<Order_By>;
  environment_area_type_rubbish_type_pickeds_aggregate?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Order_By>;
  environment_type?: InputMaybe<Environment_Type_Order_By>;
  environment_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  is_area?: InputMaybe<Order_By>;
  is_linear?: InputMaybe<Order_By>;
};

/** primary key columns input for table: environment_area_type */
export type Environment_Area_Type_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** columns and relationships of "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked = {
  __typename?: 'environment_area_type_rubbish_type_picked';
  /** An object relationship */
  environment_area_type: Environment_Area_Type;
  environment_area_type_id: Scalars['uuid']['output'];
  /** An object relationship */
  rubbish_type_picked: Rubbish_Type_Picked;
  rubbish_type_picked_id: Scalars['String']['output'];
};

/** aggregated selection of "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Aggregate = {
  __typename?: 'environment_area_type_rubbish_type_picked_aggregate';
  aggregate?: Maybe<Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Fields>;
  nodes: Array<Environment_Area_Type_Rubbish_Type_Picked>;
};

export type Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Bool_Exp = {
  count?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Bool_Exp_Count>;
};

export type Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Fields = {
  __typename?: 'environment_area_type_rubbish_type_picked_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Environment_Area_Type_Rubbish_Type_Picked_Max_Fields>;
  min?: Maybe<Environment_Area_Type_Rubbish_Type_Picked_Min_Fields>;
};


/** aggregate fields of "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Max_Order_By>;
  min?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Min_Order_By>;
};

/** input type for inserting array relation for remote table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Arr_Rel_Insert_Input = {
  data: Array<Environment_Area_Type_Rubbish_Type_Picked_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_On_Conflict>;
};

/** Boolean expression to filter rows from the table "environment_area_type_rubbish_type_picked". All fields are combined with a logical 'AND'. */
export type Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp = {
  _and?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>>;
  _not?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
  _or?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>>;
  environment_area_type?: InputMaybe<Environment_Area_Type_Bool_Exp>;
  environment_area_type_id?: InputMaybe<Uuid_Comparison_Exp>;
  rubbish_type_picked?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
  rubbish_type_picked_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "environment_area_type_rubbish_type_picked" */
export enum Environment_Area_Type_Rubbish_Type_Picked_Constraint {
  /** unique or primary key constraint on columns "rubbish_type_picked_id", "environment_area_type_id" */
  EnvironmentAreaTypeRubbishTypePickedPkey = 'environment_area_type_rubbish_type_picked_pkey'
}

/** input type for inserting data into table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Insert_Input = {
  environment_area_type?: InputMaybe<Environment_Area_Type_Obj_Rel_Insert_Input>;
  environment_area_type_id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_type_picked?: InputMaybe<Rubbish_Type_Picked_Obj_Rel_Insert_Input>;
  rubbish_type_picked_id?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Environment_Area_Type_Rubbish_Type_Picked_Max_Fields = {
  __typename?: 'environment_area_type_rubbish_type_picked_max_fields';
  environment_area_type_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_type_picked_id?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Max_Order_By = {
  environment_area_type_id?: InputMaybe<Order_By>;
  rubbish_type_picked_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Environment_Area_Type_Rubbish_Type_Picked_Min_Fields = {
  __typename?: 'environment_area_type_rubbish_type_picked_min_fields';
  environment_area_type_id?: Maybe<Scalars['uuid']['output']>;
  rubbish_type_picked_id?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Min_Order_By = {
  environment_area_type_id?: InputMaybe<Order_By>;
  rubbish_type_picked_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Mutation_Response = {
  __typename?: 'environment_area_type_rubbish_type_picked_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Environment_Area_Type_Rubbish_Type_Picked>;
};

/** on_conflict condition type for table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_On_Conflict = {
  constraint: Environment_Area_Type_Rubbish_Type_Picked_Constraint;
  update_columns?: Array<Environment_Area_Type_Rubbish_Type_Picked_Update_Column>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};

/** Ordering options when selecting data from "environment_area_type_rubbish_type_picked". */
export type Environment_Area_Type_Rubbish_Type_Picked_Order_By = {
  environment_area_type?: InputMaybe<Environment_Area_Type_Order_By>;
  environment_area_type_id?: InputMaybe<Order_By>;
  rubbish_type_picked?: InputMaybe<Rubbish_Type_Picked_Order_By>;
  rubbish_type_picked_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: environment_area_type_rubbish_type_picked */
export type Environment_Area_Type_Rubbish_Type_Picked_Pk_Columns_Input = {
  environment_area_type_id: Scalars['uuid']['input'];
  rubbish_type_picked_id: Scalars['String']['input'];
};

/** select columns of table "environment_area_type_rubbish_type_picked" */
export enum Environment_Area_Type_Rubbish_Type_Picked_Select_Column {
  /** column name */
  EnvironmentAreaTypeId = 'environment_area_type_id',
  /** column name */
  RubbishTypePickedId = 'rubbish_type_picked_id'
}

/** input type for updating data in table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Set_Input = {
  environment_area_type_id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_type_picked_id?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "environment_area_type_rubbish_type_picked" */
export type Environment_Area_Type_Rubbish_Type_Picked_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Environment_Area_Type_Rubbish_Type_Picked_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Environment_Area_Type_Rubbish_Type_Picked_Stream_Cursor_Value_Input = {
  environment_area_type_id?: InputMaybe<Scalars['uuid']['input']>;
  rubbish_type_picked_id?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "environment_area_type_rubbish_type_picked" */
export enum Environment_Area_Type_Rubbish_Type_Picked_Update_Column {
  /** column name */
  EnvironmentAreaTypeId = 'environment_area_type_id',
  /** column name */
  RubbishTypePickedId = 'rubbish_type_picked_id'
}

export type Environment_Area_Type_Rubbish_Type_Picked_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Set_Input>;
  /** filter the rows which have to be updated */
  where: Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp;
};

/** select columns of table "environment_area_type" */
export enum Environment_Area_Type_Select_Column {
  /** column name */
  AreaTypeId = 'area_type_id',
  /** column name */
  EnvironmentTypeId = 'environment_type_id',
  /** column name */
  Id = 'id',
  /** column name */
  IsArea = 'is_area',
  /** column name */
  IsLinear = 'is_linear'
}

/** select "environment_area_type_aggregate_bool_exp_bool_and_arguments_columns" columns of table "environment_area_type" */
export enum Environment_Area_Type_Select_Column_Environment_Area_Type_Aggregate_Bool_Exp_Bool_And_Arguments_Columns {
  /** column name */
  IsArea = 'is_area',
  /** column name */
  IsLinear = 'is_linear'
}

/** select "environment_area_type_aggregate_bool_exp_bool_or_arguments_columns" columns of table "environment_area_type" */
export enum Environment_Area_Type_Select_Column_Environment_Area_Type_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns {
  /** column name */
  IsArea = 'is_area',
  /** column name */
  IsLinear = 'is_linear'
}

/** input type for updating data in table "environment_area_type" */
export type Environment_Area_Type_Set_Input = {
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  is_area?: InputMaybe<Scalars['Boolean']['input']>;
  is_linear?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Streaming cursor of the table "environment_area_type" */
export type Environment_Area_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Environment_Area_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Environment_Area_Type_Stream_Cursor_Value_Input = {
  area_type_id?: InputMaybe<Scalars['String']['input']>;
  environment_type_id?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  is_area?: InputMaybe<Scalars['Boolean']['input']>;
  is_linear?: InputMaybe<Scalars['Boolean']['input']>;
};

/** update columns of table "environment_area_type" */
export enum Environment_Area_Type_Update_Column {
  /** column name */
  AreaTypeId = 'area_type_id',
  /** column name */
  EnvironmentTypeId = 'environment_type_id',
  /** column name */
  Id = 'id',
  /** column name */
  IsArea = 'is_area',
  /** column name */
  IsLinear = 'is_linear'
}

export type Environment_Area_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Environment_Area_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Environment_Area_Type_Bool_Exp;
};

/** columns and relationships of "environment_type" */
export type Environment_Type = {
  __typename?: 'environment_type';
  /** An array relationship */
  environment_area_types: Array<Environment_Area_Type>;
  /** An aggregate relationship */
  environment_area_types_aggregate: Environment_Area_Type_Aggregate;
  id: Scalars['String']['output'];
  id_zds?: Maybe<Scalars['String']['output']>;
  label: Scalars['String']['output'];
};


/** columns and relationships of "environment_type" */
export type Environment_TypeEnvironment_Area_TypesArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};


/** columns and relationships of "environment_type" */
export type Environment_TypeEnvironment_Area_Types_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};

/** aggregated selection of "environment_type" */
export type Environment_Type_Aggregate = {
  __typename?: 'environment_type_aggregate';
  aggregate?: Maybe<Environment_Type_Aggregate_Fields>;
  nodes: Array<Environment_Type>;
};

/** aggregate fields of "environment_type" */
export type Environment_Type_Aggregate_Fields = {
  __typename?: 'environment_type_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Environment_Type_Max_Fields>;
  min?: Maybe<Environment_Type_Min_Fields>;
};


/** aggregate fields of "environment_type" */
export type Environment_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Environment_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "environment_type". All fields are combined with a logical 'AND'. */
export type Environment_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Environment_Type_Bool_Exp>>;
  _not?: InputMaybe<Environment_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Environment_Type_Bool_Exp>>;
  environment_area_types?: InputMaybe<Environment_Area_Type_Bool_Exp>;
  environment_area_types_aggregate?: InputMaybe<Environment_Area_Type_Aggregate_Bool_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  id_zds?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "environment_type" */
export enum Environment_Type_Constraint {
  /** unique or primary key constraint on columns "label" */
  EnvironmentTypeLabelKey = 'environment_type_label_key',
  /** unique or primary key constraint on columns "id" */
  EnvironmentTypePkey = 'environment_type_pkey'
}

/** input type for inserting data into table "environment_type" */
export type Environment_Type_Insert_Input = {
  environment_area_types?: InputMaybe<Environment_Area_Type_Arr_Rel_Insert_Input>;
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Environment_Type_Max_Fields = {
  __typename?: 'environment_type_max_fields';
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
};

/** aggregate min on columns */
export type Environment_Type_Min_Fields = {
  __typename?: 'environment_type_min_fields';
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
};

/** response of any mutation on the table "environment_type" */
export type Environment_Type_Mutation_Response = {
  __typename?: 'environment_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Environment_Type>;
};

/** input type for inserting object relation for remote table "environment_type" */
export type Environment_Type_Obj_Rel_Insert_Input = {
  data: Environment_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Environment_Type_On_Conflict>;
};

/** on_conflict condition type for table "environment_type" */
export type Environment_Type_On_Conflict = {
  constraint: Environment_Type_Constraint;
  update_columns?: Array<Environment_Type_Update_Column>;
  where?: InputMaybe<Environment_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "environment_type". */
export type Environment_Type_Order_By = {
  environment_area_types_aggregate?: InputMaybe<Environment_Area_Type_Aggregate_Order_By>;
  id?: InputMaybe<Order_By>;
  id_zds?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
};

/** primary key columns input for table: environment_type */
export type Environment_Type_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "environment_type" */
export enum Environment_Type_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  Label = 'label'
}

/** input type for updating data in table "environment_type" */
export type Environment_Type_Set_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "environment_type" */
export type Environment_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Environment_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Environment_Type_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "environment_type" */
export enum Environment_Type_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  Label = 'label'
}

export type Environment_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Environment_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Environment_Type_Bool_Exp;
};

export type Geography_Cast_Exp = {
  geometry?: InputMaybe<Geometry_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "geography". All fields are combined with logical 'AND'. */
export type Geography_Comparison_Exp = {
  _cast?: InputMaybe<Geography_Cast_Exp>;
  _eq?: InputMaybe<Scalars['geography']['input']>;
  _gt?: InputMaybe<Scalars['geography']['input']>;
  _gte?: InputMaybe<Scalars['geography']['input']>;
  _in?: InputMaybe<Array<Scalars['geography']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['geography']['input']>;
  _lte?: InputMaybe<Scalars['geography']['input']>;
  _neq?: InputMaybe<Scalars['geography']['input']>;
  _nin?: InputMaybe<Array<Scalars['geography']['input']>>;
  /** is the column within a given distance from the given geography value */
  _st_d_within?: InputMaybe<St_D_Within_Geography_Input>;
  /** does the column spatially intersect the given geography value */
  _st_intersects?: InputMaybe<Scalars['geography']['input']>;
};

export type Geometry_Cast_Exp = {
  geography?: InputMaybe<Geography_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "geometry". All fields are combined with logical 'AND'. */
export type Geometry_Comparison_Exp = {
  _cast?: InputMaybe<Geometry_Cast_Exp>;
  _eq?: InputMaybe<Scalars['geometry']['input']>;
  _gt?: InputMaybe<Scalars['geometry']['input']>;
  _gte?: InputMaybe<Scalars['geometry']['input']>;
  _in?: InputMaybe<Array<Scalars['geometry']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['geometry']['input']>;
  _lte?: InputMaybe<Scalars['geometry']['input']>;
  _neq?: InputMaybe<Scalars['geometry']['input']>;
  _nin?: InputMaybe<Array<Scalars['geometry']['input']>>;
  /** is the column within a given 3D distance from the given geometry value */
  _st_3d_d_within?: InputMaybe<St_D_Within_Input>;
  /** does the column spatially intersect the given geometry value in 3D */
  _st_3d_intersects?: InputMaybe<Scalars['geometry']['input']>;
  /** does the column contain the given geometry value */
  _st_contains?: InputMaybe<Scalars['geometry']['input']>;
  /** does the column cross the given geometry value */
  _st_crosses?: InputMaybe<Scalars['geometry']['input']>;
  /** is the column within a given distance from the given geometry value */
  _st_d_within?: InputMaybe<St_D_Within_Input>;
  /** is the column equal to given geometry value (directionality is ignored) */
  _st_equals?: InputMaybe<Scalars['geometry']['input']>;
  /** does the column spatially intersect the given geometry value */
  _st_intersects?: InputMaybe<Scalars['geometry']['input']>;
  /** does the column 'spatially overlap' (intersect but not completely contain) the given geometry value */
  _st_overlaps?: InputMaybe<Scalars['geometry']['input']>;
  /** does the column have atleast one point in common with the given geometry value */
  _st_touches?: InputMaybe<Scalars['geometry']['input']>;
  /** is the column contained in the given geometry value */
  _st_within?: InputMaybe<Scalars['geometry']['input']>;
};

export type Jsonb_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
export type Jsonb_Comparison_Exp = {
  _cast?: InputMaybe<Jsonb_Cast_Exp>;
  /** is the column contained in the given json value */
  _contained_in?: InputMaybe<Scalars['jsonb']['input']>;
  /** does the column contain the given json value at the top level */
  _contains?: InputMaybe<Scalars['jsonb']['input']>;
  _eq?: InputMaybe<Scalars['jsonb']['input']>;
  _gt?: InputMaybe<Scalars['jsonb']['input']>;
  _gte?: InputMaybe<Scalars['jsonb']['input']>;
  /** does the string exist as a top-level key in the column */
  _has_key?: InputMaybe<Scalars['String']['input']>;
  /** do all of these strings exist as top-level keys in the column */
  _has_keys_all?: InputMaybe<Array<Scalars['String']['input']>>;
  /** do any of these strings exist as top-level keys in the column */
  _has_keys_any?: InputMaybe<Array<Scalars['String']['input']>>;
  _in?: InputMaybe<Array<Scalars['jsonb']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['jsonb']['input']>;
  _lte?: InputMaybe<Scalars['jsonb']['input']>;
  _neq?: InputMaybe<Scalars['jsonb']['input']>;
  _nin?: InputMaybe<Array<Scalars['jsonb']['input']>>;
};

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** delete data from the table: "area_type" */
  delete_area_type?: Maybe<Area_Type_Mutation_Response>;
  /** delete single row from the table: "area_type" */
  delete_area_type_by_pk?: Maybe<Area_Type>;
  /** delete data from the table: "brand" */
  delete_brand?: Maybe<Brand_Mutation_Response>;
  /** delete single row from the table: "brand" */
  delete_brand_by_pk?: Maybe<Brand>;
  /** delete data from the table: "campaign" */
  delete_campaign?: Maybe<Campaign_Mutation_Response>;
  /** delete single row from the table: "campaign" */
  delete_campaign_by_pk?: Maybe<Campaign>;
  /** delete data from the table: "campaign_user" */
  delete_campaign_user?: Maybe<Campaign_User_Mutation_Response>;
  /** delete single row from the table: "campaign_user" */
  delete_campaign_user_by_pk?: Maybe<Campaign_User>;
  /** delete data from the table: "characterization_level" */
  delete_characterization_level?: Maybe<Characterization_Level_Mutation_Response>;
  /** delete single row from the table: "characterization_level" */
  delete_characterization_level_by_pk?: Maybe<Characterization_Level>;
  /** delete data from the table: "city" */
  delete_city?: Maybe<City_Mutation_Response>;
  /** delete single row from the table: "city" */
  delete_city_by_pk?: Maybe<City>;
  /** delete data from the table: "cleanup" */
  delete_cleanup?: Maybe<Cleanup_Mutation_Response>;
  /** delete data from the table: "cleanup_area" */
  delete_cleanup_area?: Maybe<Cleanup_Area_Mutation_Response>;
  /** delete single row from the table: "cleanup_area" */
  delete_cleanup_area_by_pk?: Maybe<Cleanup_Area>;
  /** delete single row from the table: "cleanup" */
  delete_cleanup_by_pk?: Maybe<Cleanup>;
  /** delete data from the table: "cleanup_campaign" */
  delete_cleanup_campaign?: Maybe<Cleanup_Campaign_Mutation_Response>;
  /** delete single row from the table: "cleanup_campaign" */
  delete_cleanup_campaign_by_pk?: Maybe<Cleanup_Campaign>;
  /** delete data from the table: "cleanup_form" */
  delete_cleanup_form?: Maybe<Cleanup_Form_Mutation_Response>;
  /** delete single row from the table: "cleanup_form" */
  delete_cleanup_form_by_pk?: Maybe<Cleanup_Form>;
  /** delete data from the table: "cleanup_form_doi" */
  delete_cleanup_form_doi?: Maybe<Cleanup_Form_Doi_Mutation_Response>;
  /** delete single row from the table: "cleanup_form_doi" */
  delete_cleanup_form_doi_by_pk?: Maybe<Cleanup_Form_Doi>;
  /** delete data from the table: "cleanup_form_poi" */
  delete_cleanup_form_poi?: Maybe<Cleanup_Form_Poi_Mutation_Response>;
  /** delete single row from the table: "cleanup_form_poi" */
  delete_cleanup_form_poi_by_pk?: Maybe<Cleanup_Form_Poi>;
  /** delete data from the table: "cleanup_form_rubbish" */
  delete_cleanup_form_rubbish?: Maybe<Cleanup_Form_Rubbish_Mutation_Response>;
  /** delete single row from the table: "cleanup_form_rubbish" */
  delete_cleanup_form_rubbish_by_pk?: Maybe<Cleanup_Form_Rubbish>;
  /** delete data from the table: "cleanup_form_rubbish_detail" */
  delete_cleanup_form_rubbish_detail?: Maybe<Cleanup_Form_Rubbish_Detail_Mutation_Response>;
  /** delete single row from the table: "cleanup_form_rubbish_detail" */
  delete_cleanup_form_rubbish_detail_by_pk?: Maybe<Cleanup_Form_Rubbish_Detail>;
  /** delete data from the table: "cleanup_partner" */
  delete_cleanup_partner?: Maybe<Cleanup_Partner_Mutation_Response>;
  /** delete single row from the table: "cleanup_partner" */
  delete_cleanup_partner_by_pk?: Maybe<Cleanup_Partner>;
  /** delete data from the table: "cleanup_photo" */
  delete_cleanup_photo?: Maybe<Cleanup_Photo_Mutation_Response>;
  /** delete single row from the table: "cleanup_photo" */
  delete_cleanup_photo_by_pk?: Maybe<Cleanup_Photo>;
  /** delete data from the table: "cleanup_place" */
  delete_cleanup_place?: Maybe<Cleanup_Place_Mutation_Response>;
  /** delete single row from the table: "cleanup_place" */
  delete_cleanup_place_by_pk?: Maybe<Cleanup_Place>;
  /** delete data from the table: "cleanup_type" */
  delete_cleanup_type?: Maybe<Cleanup_Type_Mutation_Response>;
  /** delete single row from the table: "cleanup_type" */
  delete_cleanup_type_by_pk?: Maybe<Cleanup_Type>;
  /** delete data from the table: "cleanup_type_rubbish" */
  delete_cleanup_type_rubbish?: Maybe<Cleanup_Type_Rubbish_Mutation_Response>;
  /** delete single row from the table: "cleanup_type_rubbish" */
  delete_cleanup_type_rubbish_by_pk?: Maybe<Cleanup_Type_Rubbish>;
  /** delete data from the table: "departement" */
  delete_departement?: Maybe<Departement_Mutation_Response>;
  /** delete single row from the table: "departement" */
  delete_departement_by_pk?: Maybe<Departement>;
  /** delete data from the table: "environment_area_type" */
  delete_environment_area_type?: Maybe<Environment_Area_Type_Mutation_Response>;
  /** delete single row from the table: "environment_area_type" */
  delete_environment_area_type_by_pk?: Maybe<Environment_Area_Type>;
  /** delete data from the table: "environment_area_type_rubbish_type_picked" */
  delete_environment_area_type_rubbish_type_picked?: Maybe<Environment_Area_Type_Rubbish_Type_Picked_Mutation_Response>;
  /** delete single row from the table: "environment_area_type_rubbish_type_picked" */
  delete_environment_area_type_rubbish_type_picked_by_pk?: Maybe<Environment_Area_Type_Rubbish_Type_Picked>;
  /** delete data from the table: "environment_type" */
  delete_environment_type?: Maybe<Environment_Type_Mutation_Response>;
  /** delete single row from the table: "environment_type" */
  delete_environment_type_by_pk?: Maybe<Environment_Type>;
  /** delete data from the table: "partner" */
  delete_partner?: Maybe<Partner_Mutation_Response>;
  /** delete single row from the table: "partner" */
  delete_partner_by_pk?: Maybe<Partner>;
  /** delete data from the table: "poi_type" */
  delete_poi_type?: Maybe<Poi_Type_Mutation_Response>;
  /** delete single row from the table: "poi_type" */
  delete_poi_type_by_pk?: Maybe<Poi_Type>;
  /** delete data from the table: "region" */
  delete_region?: Maybe<Region_Mutation_Response>;
  /** delete single row from the table: "region" */
  delete_region_by_pk?: Maybe<Region>;
  /** delete data from the table: "resource" */
  delete_resource?: Maybe<Resource_Mutation_Response>;
  /** delete single row from the table: "resource" */
  delete_resource_by_pk?: Maybe<Resource>;
  /** delete data from the table: "rubbish" */
  delete_rubbish?: Maybe<Rubbish_Mutation_Response>;
  /** delete single row from the table: "rubbish" */
  delete_rubbish_by_pk?: Maybe<Rubbish>;
  /** delete data from the table: "rubbish_category" */
  delete_rubbish_category?: Maybe<Rubbish_Category_Mutation_Response>;
  /** delete single row from the table: "rubbish_category" */
  delete_rubbish_category_by_pk?: Maybe<Rubbish_Category>;
  /** delete data from the table: "rubbish_rubbish_tag" */
  delete_rubbish_rubbish_tag?: Maybe<Rubbish_Rubbish_Tag_Mutation_Response>;
  /** delete single row from the table: "rubbish_rubbish_tag" */
  delete_rubbish_rubbish_tag_by_pk?: Maybe<Rubbish_Rubbish_Tag>;
  /** delete data from the table: "rubbish_tag" */
  delete_rubbish_tag?: Maybe<Rubbish_Tag_Mutation_Response>;
  /** delete single row from the table: "rubbish_tag" */
  delete_rubbish_tag_by_pk?: Maybe<Rubbish_Tag>;
  /** delete data from the table: "rubbish_type_picked" */
  delete_rubbish_type_picked?: Maybe<Rubbish_Type_Picked_Mutation_Response>;
  /** delete single row from the table: "rubbish_type_picked" */
  delete_rubbish_type_picked_by_pk?: Maybe<Rubbish_Type_Picked>;
  /** insert data into the table: "area_type" */
  insert_area_type?: Maybe<Area_Type_Mutation_Response>;
  /** insert a single row into the table: "area_type" */
  insert_area_type_one?: Maybe<Area_Type>;
  /** insert data into the table: "brand" */
  insert_brand?: Maybe<Brand_Mutation_Response>;
  /** insert a single row into the table: "brand" */
  insert_brand_one?: Maybe<Brand>;
  /** insert data into the table: "campaign" */
  insert_campaign?: Maybe<Campaign_Mutation_Response>;
  /** insert a single row into the table: "campaign" */
  insert_campaign_one?: Maybe<Campaign>;
  /** insert data into the table: "campaign_user" */
  insert_campaign_user?: Maybe<Campaign_User_Mutation_Response>;
  /** insert a single row into the table: "campaign_user" */
  insert_campaign_user_one?: Maybe<Campaign_User>;
  /** insert data into the table: "characterization_level" */
  insert_characterization_level?: Maybe<Characterization_Level_Mutation_Response>;
  /** insert a single row into the table: "characterization_level" */
  insert_characterization_level_one?: Maybe<Characterization_Level>;
  /** insert data into the table: "city" */
  insert_city?: Maybe<City_Mutation_Response>;
  /** insert a single row into the table: "city" */
  insert_city_one?: Maybe<City>;
  /** insert data into the table: "cleanup" */
  insert_cleanup?: Maybe<Cleanup_Mutation_Response>;
  /** insert data into the table: "cleanup_area" */
  insert_cleanup_area?: Maybe<Cleanup_Area_Mutation_Response>;
  /** insert a single row into the table: "cleanup_area" */
  insert_cleanup_area_one?: Maybe<Cleanup_Area>;
  /** insert data into the table: "cleanup_campaign" */
  insert_cleanup_campaign?: Maybe<Cleanup_Campaign_Mutation_Response>;
  /** insert a single row into the table: "cleanup_campaign" */
  insert_cleanup_campaign_one?: Maybe<Cleanup_Campaign>;
  /** insert data into the table: "cleanup_form" */
  insert_cleanup_form?: Maybe<Cleanup_Form_Mutation_Response>;
  /** insert data into the table: "cleanup_form_doi" */
  insert_cleanup_form_doi?: Maybe<Cleanup_Form_Doi_Mutation_Response>;
  /** insert a single row into the table: "cleanup_form_doi" */
  insert_cleanup_form_doi_one?: Maybe<Cleanup_Form_Doi>;
  /** insert a single row into the table: "cleanup_form" */
  insert_cleanup_form_one?: Maybe<Cleanup_Form>;
  /** insert data into the table: "cleanup_form_poi" */
  insert_cleanup_form_poi?: Maybe<Cleanup_Form_Poi_Mutation_Response>;
  /** insert a single row into the table: "cleanup_form_poi" */
  insert_cleanup_form_poi_one?: Maybe<Cleanup_Form_Poi>;
  /** insert data into the table: "cleanup_form_rubbish" */
  insert_cleanup_form_rubbish?: Maybe<Cleanup_Form_Rubbish_Mutation_Response>;
  /** insert data into the table: "cleanup_form_rubbish_detail" */
  insert_cleanup_form_rubbish_detail?: Maybe<Cleanup_Form_Rubbish_Detail_Mutation_Response>;
  /** insert a single row into the table: "cleanup_form_rubbish_detail" */
  insert_cleanup_form_rubbish_detail_one?: Maybe<Cleanup_Form_Rubbish_Detail>;
  /** insert a single row into the table: "cleanup_form_rubbish" */
  insert_cleanup_form_rubbish_one?: Maybe<Cleanup_Form_Rubbish>;
  /** insert a single row into the table: "cleanup" */
  insert_cleanup_one?: Maybe<Cleanup>;
  /** insert data into the table: "cleanup_partner" */
  insert_cleanup_partner?: Maybe<Cleanup_Partner_Mutation_Response>;
  /** insert a single row into the table: "cleanup_partner" */
  insert_cleanup_partner_one?: Maybe<Cleanup_Partner>;
  /** insert data into the table: "cleanup_photo" */
  insert_cleanup_photo?: Maybe<Cleanup_Photo_Mutation_Response>;
  /** insert a single row into the table: "cleanup_photo" */
  insert_cleanup_photo_one?: Maybe<Cleanup_Photo>;
  /** insert data into the table: "cleanup_place" */
  insert_cleanup_place?: Maybe<Cleanup_Place_Mutation_Response>;
  /** insert a single row into the table: "cleanup_place" */
  insert_cleanup_place_one?: Maybe<Cleanup_Place>;
  /** insert data into the table: "cleanup_type" */
  insert_cleanup_type?: Maybe<Cleanup_Type_Mutation_Response>;
  /** insert a single row into the table: "cleanup_type" */
  insert_cleanup_type_one?: Maybe<Cleanup_Type>;
  /** insert data into the table: "cleanup_type_rubbish" */
  insert_cleanup_type_rubbish?: Maybe<Cleanup_Type_Rubbish_Mutation_Response>;
  /** insert a single row into the table: "cleanup_type_rubbish" */
  insert_cleanup_type_rubbish_one?: Maybe<Cleanup_Type_Rubbish>;
  /** insert data into the table: "departement" */
  insert_departement?: Maybe<Departement_Mutation_Response>;
  /** insert a single row into the table: "departement" */
  insert_departement_one?: Maybe<Departement>;
  /** insert data into the table: "environment_area_type" */
  insert_environment_area_type?: Maybe<Environment_Area_Type_Mutation_Response>;
  /** insert a single row into the table: "environment_area_type" */
  insert_environment_area_type_one?: Maybe<Environment_Area_Type>;
  /** insert data into the table: "environment_area_type_rubbish_type_picked" */
  insert_environment_area_type_rubbish_type_picked?: Maybe<Environment_Area_Type_Rubbish_Type_Picked_Mutation_Response>;
  /** insert a single row into the table: "environment_area_type_rubbish_type_picked" */
  insert_environment_area_type_rubbish_type_picked_one?: Maybe<Environment_Area_Type_Rubbish_Type_Picked>;
  /** insert data into the table: "environment_type" */
  insert_environment_type?: Maybe<Environment_Type_Mutation_Response>;
  /** insert a single row into the table: "environment_type" */
  insert_environment_type_one?: Maybe<Environment_Type>;
  /** insert data into the table: "partner" */
  insert_partner?: Maybe<Partner_Mutation_Response>;
  /** insert a single row into the table: "partner" */
  insert_partner_one?: Maybe<Partner>;
  /** insert data into the table: "poi_type" */
  insert_poi_type?: Maybe<Poi_Type_Mutation_Response>;
  /** insert a single row into the table: "poi_type" */
  insert_poi_type_one?: Maybe<Poi_Type>;
  /** insert data into the table: "region" */
  insert_region?: Maybe<Region_Mutation_Response>;
  /** insert a single row into the table: "region" */
  insert_region_one?: Maybe<Region>;
  /** insert data into the table: "resource" */
  insert_resource?: Maybe<Resource_Mutation_Response>;
  /** insert a single row into the table: "resource" */
  insert_resource_one?: Maybe<Resource>;
  /** insert data into the table: "rubbish" */
  insert_rubbish?: Maybe<Rubbish_Mutation_Response>;
  /** insert data into the table: "rubbish_category" */
  insert_rubbish_category?: Maybe<Rubbish_Category_Mutation_Response>;
  /** insert a single row into the table: "rubbish_category" */
  insert_rubbish_category_one?: Maybe<Rubbish_Category>;
  /** insert a single row into the table: "rubbish" */
  insert_rubbish_one?: Maybe<Rubbish>;
  /** insert data into the table: "rubbish_rubbish_tag" */
  insert_rubbish_rubbish_tag?: Maybe<Rubbish_Rubbish_Tag_Mutation_Response>;
  /** insert a single row into the table: "rubbish_rubbish_tag" */
  insert_rubbish_rubbish_tag_one?: Maybe<Rubbish_Rubbish_Tag>;
  /** insert data into the table: "rubbish_tag" */
  insert_rubbish_tag?: Maybe<Rubbish_Tag_Mutation_Response>;
  /** insert a single row into the table: "rubbish_tag" */
  insert_rubbish_tag_one?: Maybe<Rubbish_Tag>;
  /** insert data into the table: "rubbish_type_picked" */
  insert_rubbish_type_picked?: Maybe<Rubbish_Type_Picked_Mutation_Response>;
  /** insert a single row into the table: "rubbish_type_picked" */
  insert_rubbish_type_picked_one?: Maybe<Rubbish_Type_Picked>;
  /** update data of the table: "area_type" */
  update_area_type?: Maybe<Area_Type_Mutation_Response>;
  /** update single row of the table: "area_type" */
  update_area_type_by_pk?: Maybe<Area_Type>;
  /** update multiples rows of table: "area_type" */
  update_area_type_many?: Maybe<Array<Maybe<Area_Type_Mutation_Response>>>;
  /** update data of the table: "brand" */
  update_brand?: Maybe<Brand_Mutation_Response>;
  /** update single row of the table: "brand" */
  update_brand_by_pk?: Maybe<Brand>;
  /** update multiples rows of table: "brand" */
  update_brand_many?: Maybe<Array<Maybe<Brand_Mutation_Response>>>;
  /** update data of the table: "campaign" */
  update_campaign?: Maybe<Campaign_Mutation_Response>;
  /** update single row of the table: "campaign" */
  update_campaign_by_pk?: Maybe<Campaign>;
  /** update multiples rows of table: "campaign" */
  update_campaign_many?: Maybe<Array<Maybe<Campaign_Mutation_Response>>>;
  /** update data of the table: "campaign_user" */
  update_campaign_user?: Maybe<Campaign_User_Mutation_Response>;
  /** update single row of the table: "campaign_user" */
  update_campaign_user_by_pk?: Maybe<Campaign_User>;
  /** update multiples rows of table: "campaign_user" */
  update_campaign_user_many?: Maybe<Array<Maybe<Campaign_User_Mutation_Response>>>;
  /** update data of the table: "characterization_level" */
  update_characterization_level?: Maybe<Characterization_Level_Mutation_Response>;
  /** update single row of the table: "characterization_level" */
  update_characterization_level_by_pk?: Maybe<Characterization_Level>;
  /** update multiples rows of table: "characterization_level" */
  update_characterization_level_many?: Maybe<Array<Maybe<Characterization_Level_Mutation_Response>>>;
  /** update data of the table: "city" */
  update_city?: Maybe<City_Mutation_Response>;
  /** update single row of the table: "city" */
  update_city_by_pk?: Maybe<City>;
  /** update multiples rows of table: "city" */
  update_city_many?: Maybe<Array<Maybe<City_Mutation_Response>>>;
  /** update data of the table: "cleanup" */
  update_cleanup?: Maybe<Cleanup_Mutation_Response>;
  /** update data of the table: "cleanup_area" */
  update_cleanup_area?: Maybe<Cleanup_Area_Mutation_Response>;
  /** update single row of the table: "cleanup_area" */
  update_cleanup_area_by_pk?: Maybe<Cleanup_Area>;
  /** update multiples rows of table: "cleanup_area" */
  update_cleanup_area_many?: Maybe<Array<Maybe<Cleanup_Area_Mutation_Response>>>;
  /** update single row of the table: "cleanup" */
  update_cleanup_by_pk?: Maybe<Cleanup>;
  /** update data of the table: "cleanup_campaign" */
  update_cleanup_campaign?: Maybe<Cleanup_Campaign_Mutation_Response>;
  /** update single row of the table: "cleanup_campaign" */
  update_cleanup_campaign_by_pk?: Maybe<Cleanup_Campaign>;
  /** update multiples rows of table: "cleanup_campaign" */
  update_cleanup_campaign_many?: Maybe<Array<Maybe<Cleanup_Campaign_Mutation_Response>>>;
  /** update data of the table: "cleanup_form" */
  update_cleanup_form?: Maybe<Cleanup_Form_Mutation_Response>;
  /** update single row of the table: "cleanup_form" */
  update_cleanup_form_by_pk?: Maybe<Cleanup_Form>;
  /** update data of the table: "cleanup_form_doi" */
  update_cleanup_form_doi?: Maybe<Cleanup_Form_Doi_Mutation_Response>;
  /** update single row of the table: "cleanup_form_doi" */
  update_cleanup_form_doi_by_pk?: Maybe<Cleanup_Form_Doi>;
  /** update multiples rows of table: "cleanup_form_doi" */
  update_cleanup_form_doi_many?: Maybe<Array<Maybe<Cleanup_Form_Doi_Mutation_Response>>>;
  /** update multiples rows of table: "cleanup_form" */
  update_cleanup_form_many?: Maybe<Array<Maybe<Cleanup_Form_Mutation_Response>>>;
  /** update data of the table: "cleanup_form_poi" */
  update_cleanup_form_poi?: Maybe<Cleanup_Form_Poi_Mutation_Response>;
  /** update single row of the table: "cleanup_form_poi" */
  update_cleanup_form_poi_by_pk?: Maybe<Cleanup_Form_Poi>;
  /** update multiples rows of table: "cleanup_form_poi" */
  update_cleanup_form_poi_many?: Maybe<Array<Maybe<Cleanup_Form_Poi_Mutation_Response>>>;
  /** update data of the table: "cleanup_form_rubbish" */
  update_cleanup_form_rubbish?: Maybe<Cleanup_Form_Rubbish_Mutation_Response>;
  /** update single row of the table: "cleanup_form_rubbish" */
  update_cleanup_form_rubbish_by_pk?: Maybe<Cleanup_Form_Rubbish>;
  /** update data of the table: "cleanup_form_rubbish_detail" */
  update_cleanup_form_rubbish_detail?: Maybe<Cleanup_Form_Rubbish_Detail_Mutation_Response>;
  /** update single row of the table: "cleanup_form_rubbish_detail" */
  update_cleanup_form_rubbish_detail_by_pk?: Maybe<Cleanup_Form_Rubbish_Detail>;
  /** update multiples rows of table: "cleanup_form_rubbish_detail" */
  update_cleanup_form_rubbish_detail_many?: Maybe<Array<Maybe<Cleanup_Form_Rubbish_Detail_Mutation_Response>>>;
  /** update multiples rows of table: "cleanup_form_rubbish" */
  update_cleanup_form_rubbish_many?: Maybe<Array<Maybe<Cleanup_Form_Rubbish_Mutation_Response>>>;
  /** update multiples rows of table: "cleanup" */
  update_cleanup_many?: Maybe<Array<Maybe<Cleanup_Mutation_Response>>>;
  /** update data of the table: "cleanup_partner" */
  update_cleanup_partner?: Maybe<Cleanup_Partner_Mutation_Response>;
  /** update single row of the table: "cleanup_partner" */
  update_cleanup_partner_by_pk?: Maybe<Cleanup_Partner>;
  /** update multiples rows of table: "cleanup_partner" */
  update_cleanup_partner_many?: Maybe<Array<Maybe<Cleanup_Partner_Mutation_Response>>>;
  /** update data of the table: "cleanup_photo" */
  update_cleanup_photo?: Maybe<Cleanup_Photo_Mutation_Response>;
  /** update single row of the table: "cleanup_photo" */
  update_cleanup_photo_by_pk?: Maybe<Cleanup_Photo>;
  /** update multiples rows of table: "cleanup_photo" */
  update_cleanup_photo_many?: Maybe<Array<Maybe<Cleanup_Photo_Mutation_Response>>>;
  /** update data of the table: "cleanup_place" */
  update_cleanup_place?: Maybe<Cleanup_Place_Mutation_Response>;
  /** update single row of the table: "cleanup_place" */
  update_cleanup_place_by_pk?: Maybe<Cleanup_Place>;
  /** update multiples rows of table: "cleanup_place" */
  update_cleanup_place_many?: Maybe<Array<Maybe<Cleanup_Place_Mutation_Response>>>;
  /** update data of the table: "cleanup_type" */
  update_cleanup_type?: Maybe<Cleanup_Type_Mutation_Response>;
  /** update single row of the table: "cleanup_type" */
  update_cleanup_type_by_pk?: Maybe<Cleanup_Type>;
  /** update multiples rows of table: "cleanup_type" */
  update_cleanup_type_many?: Maybe<Array<Maybe<Cleanup_Type_Mutation_Response>>>;
  /** update data of the table: "cleanup_type_rubbish" */
  update_cleanup_type_rubbish?: Maybe<Cleanup_Type_Rubbish_Mutation_Response>;
  /** update single row of the table: "cleanup_type_rubbish" */
  update_cleanup_type_rubbish_by_pk?: Maybe<Cleanup_Type_Rubbish>;
  /** update multiples rows of table: "cleanup_type_rubbish" */
  update_cleanup_type_rubbish_many?: Maybe<Array<Maybe<Cleanup_Type_Rubbish_Mutation_Response>>>;
  /** update data of the table: "departement" */
  update_departement?: Maybe<Departement_Mutation_Response>;
  /** update single row of the table: "departement" */
  update_departement_by_pk?: Maybe<Departement>;
  /** update multiples rows of table: "departement" */
  update_departement_many?: Maybe<Array<Maybe<Departement_Mutation_Response>>>;
  /** update data of the table: "environment_area_type" */
  update_environment_area_type?: Maybe<Environment_Area_Type_Mutation_Response>;
  /** update single row of the table: "environment_area_type" */
  update_environment_area_type_by_pk?: Maybe<Environment_Area_Type>;
  /** update multiples rows of table: "environment_area_type" */
  update_environment_area_type_many?: Maybe<Array<Maybe<Environment_Area_Type_Mutation_Response>>>;
  /** update data of the table: "environment_area_type_rubbish_type_picked" */
  update_environment_area_type_rubbish_type_picked?: Maybe<Environment_Area_Type_Rubbish_Type_Picked_Mutation_Response>;
  /** update single row of the table: "environment_area_type_rubbish_type_picked" */
  update_environment_area_type_rubbish_type_picked_by_pk?: Maybe<Environment_Area_Type_Rubbish_Type_Picked>;
  /** update multiples rows of table: "environment_area_type_rubbish_type_picked" */
  update_environment_area_type_rubbish_type_picked_many?: Maybe<Array<Maybe<Environment_Area_Type_Rubbish_Type_Picked_Mutation_Response>>>;
  /** update data of the table: "environment_type" */
  update_environment_type?: Maybe<Environment_Type_Mutation_Response>;
  /** update single row of the table: "environment_type" */
  update_environment_type_by_pk?: Maybe<Environment_Type>;
  /** update multiples rows of table: "environment_type" */
  update_environment_type_many?: Maybe<Array<Maybe<Environment_Type_Mutation_Response>>>;
  /** update data of the table: "partner" */
  update_partner?: Maybe<Partner_Mutation_Response>;
  /** update single row of the table: "partner" */
  update_partner_by_pk?: Maybe<Partner>;
  /** update multiples rows of table: "partner" */
  update_partner_many?: Maybe<Array<Maybe<Partner_Mutation_Response>>>;
  /** update data of the table: "poi_type" */
  update_poi_type?: Maybe<Poi_Type_Mutation_Response>;
  /** update single row of the table: "poi_type" */
  update_poi_type_by_pk?: Maybe<Poi_Type>;
  /** update multiples rows of table: "poi_type" */
  update_poi_type_many?: Maybe<Array<Maybe<Poi_Type_Mutation_Response>>>;
  /** update data of the table: "region" */
  update_region?: Maybe<Region_Mutation_Response>;
  /** update single row of the table: "region" */
  update_region_by_pk?: Maybe<Region>;
  /** update multiples rows of table: "region" */
  update_region_many?: Maybe<Array<Maybe<Region_Mutation_Response>>>;
  /** update data of the table: "resource" */
  update_resource?: Maybe<Resource_Mutation_Response>;
  /** update single row of the table: "resource" */
  update_resource_by_pk?: Maybe<Resource>;
  /** update multiples rows of table: "resource" */
  update_resource_many?: Maybe<Array<Maybe<Resource_Mutation_Response>>>;
  /** update data of the table: "rubbish" */
  update_rubbish?: Maybe<Rubbish_Mutation_Response>;
  /** update single row of the table: "rubbish" */
  update_rubbish_by_pk?: Maybe<Rubbish>;
  /** update data of the table: "rubbish_category" */
  update_rubbish_category?: Maybe<Rubbish_Category_Mutation_Response>;
  /** update single row of the table: "rubbish_category" */
  update_rubbish_category_by_pk?: Maybe<Rubbish_Category>;
  /** update multiples rows of table: "rubbish_category" */
  update_rubbish_category_many?: Maybe<Array<Maybe<Rubbish_Category_Mutation_Response>>>;
  /** update multiples rows of table: "rubbish" */
  update_rubbish_many?: Maybe<Array<Maybe<Rubbish_Mutation_Response>>>;
  /** update data of the table: "rubbish_rubbish_tag" */
  update_rubbish_rubbish_tag?: Maybe<Rubbish_Rubbish_Tag_Mutation_Response>;
  /** update single row of the table: "rubbish_rubbish_tag" */
  update_rubbish_rubbish_tag_by_pk?: Maybe<Rubbish_Rubbish_Tag>;
  /** update multiples rows of table: "rubbish_rubbish_tag" */
  update_rubbish_rubbish_tag_many?: Maybe<Array<Maybe<Rubbish_Rubbish_Tag_Mutation_Response>>>;
  /** update data of the table: "rubbish_tag" */
  update_rubbish_tag?: Maybe<Rubbish_Tag_Mutation_Response>;
  /** update single row of the table: "rubbish_tag" */
  update_rubbish_tag_by_pk?: Maybe<Rubbish_Tag>;
  /** update multiples rows of table: "rubbish_tag" */
  update_rubbish_tag_many?: Maybe<Array<Maybe<Rubbish_Tag_Mutation_Response>>>;
  /** update data of the table: "rubbish_type_picked" */
  update_rubbish_type_picked?: Maybe<Rubbish_Type_Picked_Mutation_Response>;
  /** update single row of the table: "rubbish_type_picked" */
  update_rubbish_type_picked_by_pk?: Maybe<Rubbish_Type_Picked>;
  /** update multiples rows of table: "rubbish_type_picked" */
  update_rubbish_type_picked_many?: Maybe<Array<Maybe<Rubbish_Type_Picked_Mutation_Response>>>;
};


/** mutation root */
export type Mutation_RootDelete_Area_TypeArgs = {
  where: Area_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Area_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_BrandArgs = {
  where: Brand_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Brand_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_CampaignArgs = {
  where: Campaign_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Campaign_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Campaign_UserArgs = {
  where: Campaign_User_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Campaign_User_By_PkArgs = {
  campaign_id: Scalars['String']['input'];
  user_view_id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Characterization_LevelArgs = {
  where: Characterization_Level_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Characterization_Level_By_PkArgs = {
  id: Scalars['Int']['input'];
};


/** mutation root */
export type Mutation_RootDelete_CityArgs = {
  where: City_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_City_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_CleanupArgs = {
  where: Cleanup_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_AreaArgs = {
  where: Cleanup_Area_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Area_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_CampaignArgs = {
  where: Cleanup_Campaign_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Campaign_By_PkArgs = {
  campaign_id: Scalars['String']['input'];
  cleanup_id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_FormArgs = {
  where: Cleanup_Form_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_DoiArgs = {
  where: Cleanup_Form_Doi_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_Doi_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_PoiArgs = {
  where: Cleanup_Form_Poi_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_Poi_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_RubbishArgs = {
  where: Cleanup_Form_Rubbish_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_Rubbish_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_Rubbish_DetailArgs = {
  where: Cleanup_Form_Rubbish_Detail_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Form_Rubbish_Detail_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_PartnerArgs = {
  where: Cleanup_Partner_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Partner_By_PkArgs = {
  cleanup_id: Scalars['uuid']['input'];
  partner_id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_PhotoArgs = {
  where: Cleanup_Photo_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Photo_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_PlaceArgs = {
  where: Cleanup_Place_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Place_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_TypeArgs = {
  where: Cleanup_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Type_RubbishArgs = {
  where: Cleanup_Type_Rubbish_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Cleanup_Type_Rubbish_By_PkArgs = {
  cleanup_type_id: Scalars['String']['input'];
  rubbish_id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_DepartementArgs = {
  where: Departement_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Departement_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Environment_Area_TypeArgs = {
  where: Environment_Area_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Environment_Area_Type_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Environment_Area_Type_Rubbish_Type_PickedArgs = {
  where: Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Environment_Area_Type_Rubbish_Type_Picked_By_PkArgs = {
  environment_area_type_id: Scalars['uuid']['input'];
  rubbish_type_picked_id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Environment_TypeArgs = {
  where: Environment_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Environment_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_PartnerArgs = {
  where: Partner_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Partner_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Poi_TypeArgs = {
  where: Poi_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Poi_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_RegionArgs = {
  where: Region_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Region_By_PkArgs = {
  slug: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_ResourceArgs = {
  where: Resource_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Resource_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


/** mutation root */
export type Mutation_RootDelete_RubbishArgs = {
  where: Rubbish_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_CategoryArgs = {
  where: Rubbish_Category_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_Category_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_Rubbish_TagArgs = {
  where: Rubbish_Rubbish_Tag_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_Rubbish_Tag_By_PkArgs = {
  rubbish_id: Scalars['String']['input'];
  rubbish_tag_id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_TagArgs = {
  where: Rubbish_Tag_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_Tag_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_Type_PickedArgs = {
  where: Rubbish_Type_Picked_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Rubbish_Type_Picked_By_PkArgs = {
  id: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootInsert_Area_TypeArgs = {
  objects: Array<Area_Type_Insert_Input>;
  on_conflict?: InputMaybe<Area_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Area_Type_OneArgs = {
  object: Area_Type_Insert_Input;
  on_conflict?: InputMaybe<Area_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_BrandArgs = {
  objects: Array<Brand_Insert_Input>;
  on_conflict?: InputMaybe<Brand_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Brand_OneArgs = {
  object: Brand_Insert_Input;
  on_conflict?: InputMaybe<Brand_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_CampaignArgs = {
  objects: Array<Campaign_Insert_Input>;
  on_conflict?: InputMaybe<Campaign_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Campaign_OneArgs = {
  object: Campaign_Insert_Input;
  on_conflict?: InputMaybe<Campaign_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Campaign_UserArgs = {
  objects: Array<Campaign_User_Insert_Input>;
  on_conflict?: InputMaybe<Campaign_User_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Campaign_User_OneArgs = {
  object: Campaign_User_Insert_Input;
  on_conflict?: InputMaybe<Campaign_User_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Characterization_LevelArgs = {
  objects: Array<Characterization_Level_Insert_Input>;
  on_conflict?: InputMaybe<Characterization_Level_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Characterization_Level_OneArgs = {
  object: Characterization_Level_Insert_Input;
  on_conflict?: InputMaybe<Characterization_Level_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_CityArgs = {
  objects: Array<City_Insert_Input>;
  on_conflict?: InputMaybe<City_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_City_OneArgs = {
  object: City_Insert_Input;
  on_conflict?: InputMaybe<City_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_CleanupArgs = {
  objects: Array<Cleanup_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_AreaArgs = {
  objects: Array<Cleanup_Area_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Area_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Area_OneArgs = {
  object: Cleanup_Area_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Area_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_CampaignArgs = {
  objects: Array<Cleanup_Campaign_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Campaign_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Campaign_OneArgs = {
  object: Cleanup_Campaign_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Campaign_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_FormArgs = {
  objects: Array<Cleanup_Form_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Form_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_DoiArgs = {
  objects: Array<Cleanup_Form_Doi_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Form_Doi_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_Doi_OneArgs = {
  object: Cleanup_Form_Doi_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Form_Doi_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_OneArgs = {
  object: Cleanup_Form_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Form_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_PoiArgs = {
  objects: Array<Cleanup_Form_Poi_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Form_Poi_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_Poi_OneArgs = {
  object: Cleanup_Form_Poi_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Form_Poi_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_RubbishArgs = {
  objects: Array<Cleanup_Form_Rubbish_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Form_Rubbish_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_Rubbish_DetailArgs = {
  objects: Array<Cleanup_Form_Rubbish_Detail_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Form_Rubbish_Detail_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_Rubbish_Detail_OneArgs = {
  object: Cleanup_Form_Rubbish_Detail_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Form_Rubbish_Detail_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Form_Rubbish_OneArgs = {
  object: Cleanup_Form_Rubbish_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Form_Rubbish_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_OneArgs = {
  object: Cleanup_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_PartnerArgs = {
  objects: Array<Cleanup_Partner_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Partner_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Partner_OneArgs = {
  object: Cleanup_Partner_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Partner_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_PhotoArgs = {
  objects: Array<Cleanup_Photo_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Photo_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Photo_OneArgs = {
  object: Cleanup_Photo_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Photo_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_PlaceArgs = {
  objects: Array<Cleanup_Place_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Place_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Place_OneArgs = {
  object: Cleanup_Place_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Place_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_TypeArgs = {
  objects: Array<Cleanup_Type_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Type_OneArgs = {
  object: Cleanup_Type_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Type_RubbishArgs = {
  objects: Array<Cleanup_Type_Rubbish_Insert_Input>;
  on_conflict?: InputMaybe<Cleanup_Type_Rubbish_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Cleanup_Type_Rubbish_OneArgs = {
  object: Cleanup_Type_Rubbish_Insert_Input;
  on_conflict?: InputMaybe<Cleanup_Type_Rubbish_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_DepartementArgs = {
  objects: Array<Departement_Insert_Input>;
  on_conflict?: InputMaybe<Departement_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Departement_OneArgs = {
  object: Departement_Insert_Input;
  on_conflict?: InputMaybe<Departement_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Environment_Area_TypeArgs = {
  objects: Array<Environment_Area_Type_Insert_Input>;
  on_conflict?: InputMaybe<Environment_Area_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Environment_Area_Type_OneArgs = {
  object: Environment_Area_Type_Insert_Input;
  on_conflict?: InputMaybe<Environment_Area_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Environment_Area_Type_Rubbish_Type_PickedArgs = {
  objects: Array<Environment_Area_Type_Rubbish_Type_Picked_Insert_Input>;
  on_conflict?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Environment_Area_Type_Rubbish_Type_Picked_OneArgs = {
  object: Environment_Area_Type_Rubbish_Type_Picked_Insert_Input;
  on_conflict?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Environment_TypeArgs = {
  objects: Array<Environment_Type_Insert_Input>;
  on_conflict?: InputMaybe<Environment_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Environment_Type_OneArgs = {
  object: Environment_Type_Insert_Input;
  on_conflict?: InputMaybe<Environment_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_PartnerArgs = {
  objects: Array<Partner_Insert_Input>;
  on_conflict?: InputMaybe<Partner_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Partner_OneArgs = {
  object: Partner_Insert_Input;
  on_conflict?: InputMaybe<Partner_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Poi_TypeArgs = {
  objects: Array<Poi_Type_Insert_Input>;
  on_conflict?: InputMaybe<Poi_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Poi_Type_OneArgs = {
  object: Poi_Type_Insert_Input;
  on_conflict?: InputMaybe<Poi_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_RegionArgs = {
  objects: Array<Region_Insert_Input>;
  on_conflict?: InputMaybe<Region_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Region_OneArgs = {
  object: Region_Insert_Input;
  on_conflict?: InputMaybe<Region_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ResourceArgs = {
  objects: Array<Resource_Insert_Input>;
  on_conflict?: InputMaybe<Resource_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Resource_OneArgs = {
  object: Resource_Insert_Input;
  on_conflict?: InputMaybe<Resource_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_RubbishArgs = {
  objects: Array<Rubbish_Insert_Input>;
  on_conflict?: InputMaybe<Rubbish_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_CategoryArgs = {
  objects: Array<Rubbish_Category_Insert_Input>;
  on_conflict?: InputMaybe<Rubbish_Category_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_Category_OneArgs = {
  object: Rubbish_Category_Insert_Input;
  on_conflict?: InputMaybe<Rubbish_Category_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_OneArgs = {
  object: Rubbish_Insert_Input;
  on_conflict?: InputMaybe<Rubbish_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_Rubbish_TagArgs = {
  objects: Array<Rubbish_Rubbish_Tag_Insert_Input>;
  on_conflict?: InputMaybe<Rubbish_Rubbish_Tag_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_Rubbish_Tag_OneArgs = {
  object: Rubbish_Rubbish_Tag_Insert_Input;
  on_conflict?: InputMaybe<Rubbish_Rubbish_Tag_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_TagArgs = {
  objects: Array<Rubbish_Tag_Insert_Input>;
  on_conflict?: InputMaybe<Rubbish_Tag_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_Tag_OneArgs = {
  object: Rubbish_Tag_Insert_Input;
  on_conflict?: InputMaybe<Rubbish_Tag_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_Type_PickedArgs = {
  objects: Array<Rubbish_Type_Picked_Insert_Input>;
  on_conflict?: InputMaybe<Rubbish_Type_Picked_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Rubbish_Type_Picked_OneArgs = {
  object: Rubbish_Type_Picked_Insert_Input;
  on_conflict?: InputMaybe<Rubbish_Type_Picked_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_Area_TypeArgs = {
  _set?: InputMaybe<Area_Type_Set_Input>;
  where: Area_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Area_Type_By_PkArgs = {
  _set?: InputMaybe<Area_Type_Set_Input>;
  pk_columns: Area_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Area_Type_ManyArgs = {
  updates: Array<Area_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_BrandArgs = {
  _set?: InputMaybe<Brand_Set_Input>;
  where: Brand_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Brand_By_PkArgs = {
  _set?: InputMaybe<Brand_Set_Input>;
  pk_columns: Brand_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Brand_ManyArgs = {
  updates: Array<Brand_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_CampaignArgs = {
  _set?: InputMaybe<Campaign_Set_Input>;
  where: Campaign_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Campaign_By_PkArgs = {
  _set?: InputMaybe<Campaign_Set_Input>;
  pk_columns: Campaign_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Campaign_ManyArgs = {
  updates: Array<Campaign_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Campaign_UserArgs = {
  _set?: InputMaybe<Campaign_User_Set_Input>;
  where: Campaign_User_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Campaign_User_By_PkArgs = {
  _set?: InputMaybe<Campaign_User_Set_Input>;
  pk_columns: Campaign_User_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Campaign_User_ManyArgs = {
  updates: Array<Campaign_User_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Characterization_LevelArgs = {
  _inc?: InputMaybe<Characterization_Level_Inc_Input>;
  _set?: InputMaybe<Characterization_Level_Set_Input>;
  where: Characterization_Level_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Characterization_Level_By_PkArgs = {
  _inc?: InputMaybe<Characterization_Level_Inc_Input>;
  _set?: InputMaybe<Characterization_Level_Set_Input>;
  pk_columns: Characterization_Level_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Characterization_Level_ManyArgs = {
  updates: Array<Characterization_Level_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_CityArgs = {
  _set?: InputMaybe<City_Set_Input>;
  where: City_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_City_By_PkArgs = {
  _set?: InputMaybe<City_Set_Input>;
  pk_columns: City_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_City_ManyArgs = {
  updates: Array<City_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_CleanupArgs = {
  _append?: InputMaybe<Cleanup_Append_Input>;
  _delete_at_path?: InputMaybe<Cleanup_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Cleanup_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Cleanup_Delete_Key_Input>;
  _inc?: InputMaybe<Cleanup_Inc_Input>;
  _prepend?: InputMaybe<Cleanup_Prepend_Input>;
  _set?: InputMaybe<Cleanup_Set_Input>;
  where: Cleanup_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_AreaArgs = {
  _append?: InputMaybe<Cleanup_Area_Append_Input>;
  _delete_at_path?: InputMaybe<Cleanup_Area_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Cleanup_Area_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Cleanup_Area_Delete_Key_Input>;
  _inc?: InputMaybe<Cleanup_Area_Inc_Input>;
  _prepend?: InputMaybe<Cleanup_Area_Prepend_Input>;
  _set?: InputMaybe<Cleanup_Area_Set_Input>;
  where: Cleanup_Area_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Area_By_PkArgs = {
  _append?: InputMaybe<Cleanup_Area_Append_Input>;
  _delete_at_path?: InputMaybe<Cleanup_Area_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Cleanup_Area_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Cleanup_Area_Delete_Key_Input>;
  _inc?: InputMaybe<Cleanup_Area_Inc_Input>;
  _prepend?: InputMaybe<Cleanup_Area_Prepend_Input>;
  _set?: InputMaybe<Cleanup_Area_Set_Input>;
  pk_columns: Cleanup_Area_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Area_ManyArgs = {
  updates: Array<Cleanup_Area_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_By_PkArgs = {
  _append?: InputMaybe<Cleanup_Append_Input>;
  _delete_at_path?: InputMaybe<Cleanup_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Cleanup_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Cleanup_Delete_Key_Input>;
  _inc?: InputMaybe<Cleanup_Inc_Input>;
  _prepend?: InputMaybe<Cleanup_Prepend_Input>;
  _set?: InputMaybe<Cleanup_Set_Input>;
  pk_columns: Cleanup_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_CampaignArgs = {
  _set?: InputMaybe<Cleanup_Campaign_Set_Input>;
  where: Cleanup_Campaign_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Campaign_By_PkArgs = {
  _set?: InputMaybe<Cleanup_Campaign_Set_Input>;
  pk_columns: Cleanup_Campaign_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Campaign_ManyArgs = {
  updates: Array<Cleanup_Campaign_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_FormArgs = {
  _inc?: InputMaybe<Cleanup_Form_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Set_Input>;
  where: Cleanup_Form_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_By_PkArgs = {
  _inc?: InputMaybe<Cleanup_Form_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Set_Input>;
  pk_columns: Cleanup_Form_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_DoiArgs = {
  _set?: InputMaybe<Cleanup_Form_Doi_Set_Input>;
  where: Cleanup_Form_Doi_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Doi_By_PkArgs = {
  _set?: InputMaybe<Cleanup_Form_Doi_Set_Input>;
  pk_columns: Cleanup_Form_Doi_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Doi_ManyArgs = {
  updates: Array<Cleanup_Form_Doi_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_ManyArgs = {
  updates: Array<Cleanup_Form_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_PoiArgs = {
  _inc?: InputMaybe<Cleanup_Form_Poi_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Poi_Set_Input>;
  where: Cleanup_Form_Poi_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Poi_By_PkArgs = {
  _inc?: InputMaybe<Cleanup_Form_Poi_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Poi_Set_Input>;
  pk_columns: Cleanup_Form_Poi_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Poi_ManyArgs = {
  updates: Array<Cleanup_Form_Poi_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_RubbishArgs = {
  _inc?: InputMaybe<Cleanup_Form_Rubbish_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Rubbish_Set_Input>;
  where: Cleanup_Form_Rubbish_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Rubbish_By_PkArgs = {
  _inc?: InputMaybe<Cleanup_Form_Rubbish_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Rubbish_Set_Input>;
  pk_columns: Cleanup_Form_Rubbish_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Rubbish_DetailArgs = {
  _inc?: InputMaybe<Cleanup_Form_Rubbish_Detail_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Rubbish_Detail_Set_Input>;
  where: Cleanup_Form_Rubbish_Detail_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Rubbish_Detail_By_PkArgs = {
  _inc?: InputMaybe<Cleanup_Form_Rubbish_Detail_Inc_Input>;
  _set?: InputMaybe<Cleanup_Form_Rubbish_Detail_Set_Input>;
  pk_columns: Cleanup_Form_Rubbish_Detail_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Rubbish_Detail_ManyArgs = {
  updates: Array<Cleanup_Form_Rubbish_Detail_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Form_Rubbish_ManyArgs = {
  updates: Array<Cleanup_Form_Rubbish_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_ManyArgs = {
  updates: Array<Cleanup_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_PartnerArgs = {
  _set?: InputMaybe<Cleanup_Partner_Set_Input>;
  where: Cleanup_Partner_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Partner_By_PkArgs = {
  _set?: InputMaybe<Cleanup_Partner_Set_Input>;
  pk_columns: Cleanup_Partner_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Partner_ManyArgs = {
  updates: Array<Cleanup_Partner_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_PhotoArgs = {
  _inc?: InputMaybe<Cleanup_Photo_Inc_Input>;
  _set?: InputMaybe<Cleanup_Photo_Set_Input>;
  where: Cleanup_Photo_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Photo_By_PkArgs = {
  _inc?: InputMaybe<Cleanup_Photo_Inc_Input>;
  _set?: InputMaybe<Cleanup_Photo_Set_Input>;
  pk_columns: Cleanup_Photo_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Photo_ManyArgs = {
  updates: Array<Cleanup_Photo_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_PlaceArgs = {
  _set?: InputMaybe<Cleanup_Place_Set_Input>;
  where: Cleanup_Place_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Place_By_PkArgs = {
  _set?: InputMaybe<Cleanup_Place_Set_Input>;
  pk_columns: Cleanup_Place_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Place_ManyArgs = {
  updates: Array<Cleanup_Place_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_TypeArgs = {
  _inc?: InputMaybe<Cleanup_Type_Inc_Input>;
  _set?: InputMaybe<Cleanup_Type_Set_Input>;
  where: Cleanup_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Type_By_PkArgs = {
  _inc?: InputMaybe<Cleanup_Type_Inc_Input>;
  _set?: InputMaybe<Cleanup_Type_Set_Input>;
  pk_columns: Cleanup_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Type_ManyArgs = {
  updates: Array<Cleanup_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Type_RubbishArgs = {
  _set?: InputMaybe<Cleanup_Type_Rubbish_Set_Input>;
  where: Cleanup_Type_Rubbish_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Type_Rubbish_By_PkArgs = {
  _set?: InputMaybe<Cleanup_Type_Rubbish_Set_Input>;
  pk_columns: Cleanup_Type_Rubbish_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Cleanup_Type_Rubbish_ManyArgs = {
  updates: Array<Cleanup_Type_Rubbish_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_DepartementArgs = {
  _set?: InputMaybe<Departement_Set_Input>;
  where: Departement_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Departement_By_PkArgs = {
  _set?: InputMaybe<Departement_Set_Input>;
  pk_columns: Departement_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Departement_ManyArgs = {
  updates: Array<Departement_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Area_TypeArgs = {
  _set?: InputMaybe<Environment_Area_Type_Set_Input>;
  where: Environment_Area_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Area_Type_By_PkArgs = {
  _set?: InputMaybe<Environment_Area_Type_Set_Input>;
  pk_columns: Environment_Area_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Area_Type_ManyArgs = {
  updates: Array<Environment_Area_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Area_Type_Rubbish_Type_PickedArgs = {
  _set?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Set_Input>;
  where: Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Area_Type_Rubbish_Type_Picked_By_PkArgs = {
  _set?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Set_Input>;
  pk_columns: Environment_Area_Type_Rubbish_Type_Picked_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Area_Type_Rubbish_Type_Picked_ManyArgs = {
  updates: Array<Environment_Area_Type_Rubbish_Type_Picked_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_TypeArgs = {
  _set?: InputMaybe<Environment_Type_Set_Input>;
  where: Environment_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Type_By_PkArgs = {
  _set?: InputMaybe<Environment_Type_Set_Input>;
  pk_columns: Environment_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Environment_Type_ManyArgs = {
  updates: Array<Environment_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_PartnerArgs = {
  _set?: InputMaybe<Partner_Set_Input>;
  where: Partner_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Partner_By_PkArgs = {
  _set?: InputMaybe<Partner_Set_Input>;
  pk_columns: Partner_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Partner_ManyArgs = {
  updates: Array<Partner_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Poi_TypeArgs = {
  _set?: InputMaybe<Poi_Type_Set_Input>;
  where: Poi_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Poi_Type_By_PkArgs = {
  _set?: InputMaybe<Poi_Type_Set_Input>;
  pk_columns: Poi_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Poi_Type_ManyArgs = {
  updates: Array<Poi_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_RegionArgs = {
  _set?: InputMaybe<Region_Set_Input>;
  where: Region_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Region_By_PkArgs = {
  _set?: InputMaybe<Region_Set_Input>;
  pk_columns: Region_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Region_ManyArgs = {
  updates: Array<Region_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_ResourceArgs = {
  _set?: InputMaybe<Resource_Set_Input>;
  where: Resource_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Resource_By_PkArgs = {
  _set?: InputMaybe<Resource_Set_Input>;
  pk_columns: Resource_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Resource_ManyArgs = {
  updates: Array<Resource_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_RubbishArgs = {
  _set?: InputMaybe<Rubbish_Set_Input>;
  where: Rubbish_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_By_PkArgs = {
  _set?: InputMaybe<Rubbish_Set_Input>;
  pk_columns: Rubbish_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_CategoryArgs = {
  _inc?: InputMaybe<Rubbish_Category_Inc_Input>;
  _set?: InputMaybe<Rubbish_Category_Set_Input>;
  where: Rubbish_Category_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Category_By_PkArgs = {
  _inc?: InputMaybe<Rubbish_Category_Inc_Input>;
  _set?: InputMaybe<Rubbish_Category_Set_Input>;
  pk_columns: Rubbish_Category_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Category_ManyArgs = {
  updates: Array<Rubbish_Category_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_ManyArgs = {
  updates: Array<Rubbish_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Rubbish_TagArgs = {
  _set?: InputMaybe<Rubbish_Rubbish_Tag_Set_Input>;
  where: Rubbish_Rubbish_Tag_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Rubbish_Tag_By_PkArgs = {
  _set?: InputMaybe<Rubbish_Rubbish_Tag_Set_Input>;
  pk_columns: Rubbish_Rubbish_Tag_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Rubbish_Tag_ManyArgs = {
  updates: Array<Rubbish_Rubbish_Tag_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_TagArgs = {
  _set?: InputMaybe<Rubbish_Tag_Set_Input>;
  where: Rubbish_Tag_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Tag_By_PkArgs = {
  _set?: InputMaybe<Rubbish_Tag_Set_Input>;
  pk_columns: Rubbish_Tag_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Tag_ManyArgs = {
  updates: Array<Rubbish_Tag_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Type_PickedArgs = {
  _set?: InputMaybe<Rubbish_Type_Picked_Set_Input>;
  where: Rubbish_Type_Picked_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Type_Picked_By_PkArgs = {
  _set?: InputMaybe<Rubbish_Type_Picked_Set_Input>;
  pk_columns: Rubbish_Type_Picked_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Rubbish_Type_Picked_ManyArgs = {
  updates: Array<Rubbish_Type_Picked_Updates>;
};

/** Boolean expression to compare columns of type "numeric". All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['numeric']['input']>;
  _gt?: InputMaybe<Scalars['numeric']['input']>;
  _gte?: InputMaybe<Scalars['numeric']['input']>;
  _in?: InputMaybe<Array<Scalars['numeric']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['numeric']['input']>;
  _lte?: InputMaybe<Scalars['numeric']['input']>;
  _neq?: InputMaybe<Scalars['numeric']['input']>;
  _nin?: InputMaybe<Array<Scalars['numeric']['input']>>;
};

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = 'asc',
  /** in ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in descending order, nulls first */
  Desc = 'desc',
  /** in descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** columns and relationships of "partner" */
export type Partner = {
  __typename?: 'partner';
  /** An array relationship */
  cleanup_partners: Array<Cleanup_Partner>;
  /** An aggregate relationship */
  cleanup_partners_aggregate: Cleanup_Partner_Aggregate;
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['String']['output'];
  label: Scalars['String']['output'];
  logo?: Maybe<Scalars['String']['output']>;
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "partner" */
export type PartnerCleanup_PartnersArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


/** columns and relationships of "partner" */
export type PartnerCleanup_Partners_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};

/** aggregated selection of "partner" */
export type Partner_Aggregate = {
  __typename?: 'partner_aggregate';
  aggregate?: Maybe<Partner_Aggregate_Fields>;
  nodes: Array<Partner>;
};

/** aggregate fields of "partner" */
export type Partner_Aggregate_Fields = {
  __typename?: 'partner_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Partner_Max_Fields>;
  min?: Maybe<Partner_Min_Fields>;
};


/** aggregate fields of "partner" */
export type Partner_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Partner_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "partner". All fields are combined with a logical 'AND'. */
export type Partner_Bool_Exp = {
  _and?: InputMaybe<Array<Partner_Bool_Exp>>;
  _not?: InputMaybe<Partner_Bool_Exp>;
  _or?: InputMaybe<Array<Partner_Bool_Exp>>;
  cleanup_partners?: InputMaybe<Cleanup_Partner_Bool_Exp>;
  cleanup_partners_aggregate?: InputMaybe<Cleanup_Partner_Aggregate_Bool_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  logo?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "partner" */
export enum Partner_Constraint {
  /** unique or primary key constraint on columns "label" */
  PartnerLabelKey = 'partner_label_key',
  /** unique or primary key constraint on columns "id" */
  PartnerPkey = 'partner_pkey'
}

/** input type for inserting data into table "partner" */
export type Partner_Insert_Input = {
  cleanup_partners?: InputMaybe<Cleanup_Partner_Arr_Rel_Insert_Input>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Partner_Max_Fields = {
  __typename?: 'partner_max_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  logo?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Partner_Min_Fields = {
  __typename?: 'partner_min_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  logo?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "partner" */
export type Partner_Mutation_Response = {
  __typename?: 'partner_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Partner>;
};

/** input type for inserting object relation for remote table "partner" */
export type Partner_Obj_Rel_Insert_Input = {
  data: Partner_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Partner_On_Conflict>;
};

/** on_conflict condition type for table "partner" */
export type Partner_On_Conflict = {
  constraint: Partner_Constraint;
  update_columns?: Array<Partner_Update_Column>;
  where?: InputMaybe<Partner_Bool_Exp>;
};

/** Ordering options when selecting data from "partner". */
export type Partner_Order_By = {
  cleanup_partners_aggregate?: InputMaybe<Cleanup_Partner_Aggregate_Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  logo?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: partner */
export type Partner_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "partner" */
export enum Partner_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Logo = 'logo',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "partner" */
export type Partner_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "partner" */
export type Partner_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Partner_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Partner_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "partner" */
export enum Partner_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Logo = 'logo',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Partner_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Partner_Set_Input>;
  /** filter the rows which have to be updated */
  where: Partner_Bool_Exp;
};

/** columns and relationships of "poi_type" */
export type Poi_Type = {
  __typename?: 'poi_type';
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['String']['output'];
  label?: Maybe<Scalars['String']['output']>;
  logo?: Maybe<Scalars['String']['output']>;
  updated_at: Scalars['timestamptz']['output'];
};

/** aggregated selection of "poi_type" */
export type Poi_Type_Aggregate = {
  __typename?: 'poi_type_aggregate';
  aggregate?: Maybe<Poi_Type_Aggregate_Fields>;
  nodes: Array<Poi_Type>;
};

/** aggregate fields of "poi_type" */
export type Poi_Type_Aggregate_Fields = {
  __typename?: 'poi_type_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Poi_Type_Max_Fields>;
  min?: Maybe<Poi_Type_Min_Fields>;
};


/** aggregate fields of "poi_type" */
export type Poi_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Poi_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "poi_type". All fields are combined with a logical 'AND'. */
export type Poi_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Poi_Type_Bool_Exp>>;
  _not?: InputMaybe<Poi_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Poi_Type_Bool_Exp>>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  logo?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "poi_type" */
export enum Poi_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  PoiTypeFk = 'poi_type_fk'
}

/** input type for inserting data into table "poi_type" */
export type Poi_Type_Insert_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Poi_Type_Max_Fields = {
  __typename?: 'poi_type_max_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  logo?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Poi_Type_Min_Fields = {
  __typename?: 'poi_type_min_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  logo?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "poi_type" */
export type Poi_Type_Mutation_Response = {
  __typename?: 'poi_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Poi_Type>;
};

/** input type for inserting object relation for remote table "poi_type" */
export type Poi_Type_Obj_Rel_Insert_Input = {
  data: Poi_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Poi_Type_On_Conflict>;
};

/** on_conflict condition type for table "poi_type" */
export type Poi_Type_On_Conflict = {
  constraint: Poi_Type_Constraint;
  update_columns?: Array<Poi_Type_Update_Column>;
  where?: InputMaybe<Poi_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "poi_type". */
export type Poi_Type_Order_By = {
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  logo?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: poi_type */
export type Poi_Type_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "poi_type" */
export enum Poi_Type_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Logo = 'logo',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "poi_type" */
export type Poi_Type_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "poi_type" */
export type Poi_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Poi_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Poi_Type_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "poi_type" */
export enum Poi_Type_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Logo = 'logo',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Poi_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Poi_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Poi_Type_Bool_Exp;
};

export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "area_type" */
  area_type: Array<Area_Type>;
  /** fetch aggregated fields from the table: "area_type" */
  area_type_aggregate: Area_Type_Aggregate;
  /** fetch data from the table: "area_type" using primary key columns */
  area_type_by_pk?: Maybe<Area_Type>;
  /** fetch data from the table: "brand" */
  brand: Array<Brand>;
  /** fetch aggregated fields from the table: "brand" */
  brand_aggregate: Brand_Aggregate;
  /** fetch data from the table: "brand" using primary key columns */
  brand_by_pk?: Maybe<Brand>;
  /** fetch data from the table: "campaign" */
  campaign: Array<Campaign>;
  /** fetch aggregated fields from the table: "campaign" */
  campaign_aggregate: Campaign_Aggregate;
  /** fetch data from the table: "campaign" using primary key columns */
  campaign_by_pk?: Maybe<Campaign>;
  /** fetch data from the table: "campaign_rubbish_metrics_view" */
  campaign_rubbish_metrics_view: Array<Campaign_Rubbish_Metrics_View>;
  /** fetch aggregated fields from the table: "campaign_rubbish_metrics_view" */
  campaign_rubbish_metrics_view_aggregate: Campaign_Rubbish_Metrics_View_Aggregate;
  /** fetch data from the table: "campaign_user" */
  campaign_user: Array<Campaign_User>;
  /** fetch aggregated fields from the table: "campaign_user" */
  campaign_user_aggregate: Campaign_User_Aggregate;
  /** fetch data from the table: "campaign_user" using primary key columns */
  campaign_user_by_pk?: Maybe<Campaign_User>;
  /** fetch data from the table: "characterization_level" */
  characterization_level: Array<Characterization_Level>;
  /** fetch aggregated fields from the table: "characterization_level" */
  characterization_level_aggregate: Characterization_Level_Aggregate;
  /** fetch data from the table: "characterization_level" using primary key columns */
  characterization_level_by_pk?: Maybe<Characterization_Level>;
  /** fetch data from the table: "city" */
  city: Array<City>;
  /** fetch aggregated fields from the table: "city" */
  city_aggregate: City_Aggregate;
  /** fetch data from the table: "city" using primary key columns */
  city_by_pk?: Maybe<City>;
  /** fetch data from the table: "cleanup" */
  cleanup: Array<Cleanup>;
  /** fetch data from the table: "cleanup_agg_rubbish_category_view" */
  cleanup_agg_rubbish_category_view: Array<Cleanup_Agg_Rubbish_Category_View>;
  /** fetch aggregated fields from the table: "cleanup_agg_rubbish_category_view" */
  cleanup_agg_rubbish_category_view_aggregate: Cleanup_Agg_Rubbish_Category_View_Aggregate;
  /** fetch data from the table: "cleanup_agg_rubbish_tag_view" */
  cleanup_agg_rubbish_tag_view: Array<Cleanup_Agg_Rubbish_Tag_View>;
  /** fetch aggregated fields from the table: "cleanup_agg_rubbish_tag_view" */
  cleanup_agg_rubbish_tag_view_aggregate: Cleanup_Agg_Rubbish_Tag_View_Aggregate;
  /** fetch data from the table: "cleanup_agg_rubbish_view" */
  cleanup_agg_rubbish_view: Array<Cleanup_Agg_Rubbish_View>;
  /** fetch aggregated fields from the table: "cleanup_agg_rubbish_view" */
  cleanup_agg_rubbish_view_aggregate: Cleanup_Agg_Rubbish_View_Aggregate;
  /** fetch aggregated fields from the table: "cleanup" */
  cleanup_aggregate: Cleanup_Aggregate;
  /** fetch data from the table: "cleanup_area" */
  cleanup_area: Array<Cleanup_Area>;
  /** fetch aggregated fields from the table: "cleanup_area" */
  cleanup_area_aggregate: Cleanup_Area_Aggregate;
  /** fetch data from the table: "cleanup_area" using primary key columns */
  cleanup_area_by_pk?: Maybe<Cleanup_Area>;
  /** fetch data from the table: "cleanup" using primary key columns */
  cleanup_by_pk?: Maybe<Cleanup>;
  /** fetch data from the table: "cleanup_campaign" */
  cleanup_campaign: Array<Cleanup_Campaign>;
  /** fetch aggregated fields from the table: "cleanup_campaign" */
  cleanup_campaign_aggregate: Cleanup_Campaign_Aggregate;
  /** fetch data from the table: "cleanup_campaign" using primary key columns */
  cleanup_campaign_by_pk?: Maybe<Cleanup_Campaign>;
  /** fetch data from the table: "cleanup_form" */
  cleanup_form: Array<Cleanup_Form>;
  /** fetch aggregated fields from the table: "cleanup_form" */
  cleanup_form_aggregate: Cleanup_Form_Aggregate;
  /** fetch data from the table: "cleanup_form" using primary key columns */
  cleanup_form_by_pk?: Maybe<Cleanup_Form>;
  /** fetch data from the table: "cleanup_form_doi" */
  cleanup_form_doi: Array<Cleanup_Form_Doi>;
  /** fetch aggregated fields from the table: "cleanup_form_doi" */
  cleanup_form_doi_aggregate: Cleanup_Form_Doi_Aggregate;
  /** fetch data from the table: "cleanup_form_doi" using primary key columns */
  cleanup_form_doi_by_pk?: Maybe<Cleanup_Form_Doi>;
  /** fetch data from the table: "cleanup_form_poi" */
  cleanup_form_poi: Array<Cleanup_Form_Poi>;
  /** fetch aggregated fields from the table: "cleanup_form_poi" */
  cleanup_form_poi_aggregate: Cleanup_Form_Poi_Aggregate;
  /** fetch data from the table: "cleanup_form_poi" using primary key columns */
  cleanup_form_poi_by_pk?: Maybe<Cleanup_Form_Poi>;
  /** fetch data from the table: "cleanup_form_rubbish" */
  cleanup_form_rubbish: Array<Cleanup_Form_Rubbish>;
  /** fetch aggregated fields from the table: "cleanup_form_rubbish" */
  cleanup_form_rubbish_aggregate: Cleanup_Form_Rubbish_Aggregate;
  /** fetch data from the table: "cleanup_form_rubbish" using primary key columns */
  cleanup_form_rubbish_by_pk?: Maybe<Cleanup_Form_Rubbish>;
  /** fetch data from the table: "cleanup_form_rubbish_detail" */
  cleanup_form_rubbish_detail: Array<Cleanup_Form_Rubbish_Detail>;
  /** fetch aggregated fields from the table: "cleanup_form_rubbish_detail" */
  cleanup_form_rubbish_detail_aggregate: Cleanup_Form_Rubbish_Detail_Aggregate;
  /** fetch data from the table: "cleanup_form_rubbish_detail" using primary key columns */
  cleanup_form_rubbish_detail_by_pk?: Maybe<Cleanup_Form_Rubbish_Detail>;
  /** fetch data from the table: "cleanup_partner" */
  cleanup_partner: Array<Cleanup_Partner>;
  /** fetch aggregated fields from the table: "cleanup_partner" */
  cleanup_partner_aggregate: Cleanup_Partner_Aggregate;
  /** fetch data from the table: "cleanup_partner" using primary key columns */
  cleanup_partner_by_pk?: Maybe<Cleanup_Partner>;
  /** fetch data from the table: "cleanup_photo" */
  cleanup_photo: Array<Cleanup_Photo>;
  /** fetch aggregated fields from the table: "cleanup_photo" */
  cleanup_photo_aggregate: Cleanup_Photo_Aggregate;
  /** fetch data from the table: "cleanup_photo" using primary key columns */
  cleanup_photo_by_pk?: Maybe<Cleanup_Photo>;
  /** fetch data from the table: "cleanup_place" */
  cleanup_place: Array<Cleanup_Place>;
  /** fetch aggregated fields from the table: "cleanup_place" */
  cleanup_place_aggregate: Cleanup_Place_Aggregate;
  /** fetch data from the table: "cleanup_place" using primary key columns */
  cleanup_place_by_pk?: Maybe<Cleanup_Place>;
  /** fetch data from the table: "cleanup_type" */
  cleanup_type: Array<Cleanup_Type>;
  /** fetch aggregated fields from the table: "cleanup_type" */
  cleanup_type_aggregate: Cleanup_Type_Aggregate;
  /** fetch data from the table: "cleanup_type" using primary key columns */
  cleanup_type_by_pk?: Maybe<Cleanup_Type>;
  /** fetch data from the table: "cleanup_type_rubbish" */
  cleanup_type_rubbish: Array<Cleanup_Type_Rubbish>;
  /** fetch aggregated fields from the table: "cleanup_type_rubbish" */
  cleanup_type_rubbish_aggregate: Cleanup_Type_Rubbish_Aggregate;
  /** fetch data from the table: "cleanup_type_rubbish" using primary key columns */
  cleanup_type_rubbish_by_pk?: Maybe<Cleanup_Type_Rubbish>;
  /** fetch data from the table: "cleanup_view" */
  cleanup_view: Array<Cleanup_View>;
  /** fetch aggregated fields from the table: "cleanup_view" */
  cleanup_view_aggregate: Cleanup_View_Aggregate;
  /** fetch data from the table: "departement" */
  departement: Array<Departement>;
  /** fetch aggregated fields from the table: "departement" */
  departement_aggregate: Departement_Aggregate;
  /** fetch data from the table: "departement" using primary key columns */
  departement_by_pk?: Maybe<Departement>;
  /** fetch data from the table: "environment_area_type" */
  environment_area_type: Array<Environment_Area_Type>;
  /** fetch aggregated fields from the table: "environment_area_type" */
  environment_area_type_aggregate: Environment_Area_Type_Aggregate;
  /** fetch data from the table: "environment_area_type" using primary key columns */
  environment_area_type_by_pk?: Maybe<Environment_Area_Type>;
  /** fetch data from the table: "environment_area_type_rubbish_type_picked" */
  environment_area_type_rubbish_type_picked: Array<Environment_Area_Type_Rubbish_Type_Picked>;
  /** fetch aggregated fields from the table: "environment_area_type_rubbish_type_picked" */
  environment_area_type_rubbish_type_picked_aggregate: Environment_Area_Type_Rubbish_Type_Picked_Aggregate;
  /** fetch data from the table: "environment_area_type_rubbish_type_picked" using primary key columns */
  environment_area_type_rubbish_type_picked_by_pk?: Maybe<Environment_Area_Type_Rubbish_Type_Picked>;
  /** fetch data from the table: "environment_type" */
  environment_type: Array<Environment_Type>;
  /** fetch aggregated fields from the table: "environment_type" */
  environment_type_aggregate: Environment_Type_Aggregate;
  /** fetch data from the table: "environment_type" using primary key columns */
  environment_type_by_pk?: Maybe<Environment_Type>;
  /** fetch data from the table: "partner" */
  partner: Array<Partner>;
  /** fetch aggregated fields from the table: "partner" */
  partner_aggregate: Partner_Aggregate;
  /** fetch data from the table: "partner" using primary key columns */
  partner_by_pk?: Maybe<Partner>;
  /** fetch data from the table: "poi_type" */
  poi_type: Array<Poi_Type>;
  /** fetch aggregated fields from the table: "poi_type" */
  poi_type_aggregate: Poi_Type_Aggregate;
  /** fetch data from the table: "poi_type" using primary key columns */
  poi_type_by_pk?: Maybe<Poi_Type>;
  /** fetch data from the table: "region" */
  region: Array<Region>;
  /** fetch aggregated fields from the table: "region" */
  region_aggregate: Region_Aggregate;
  /** fetch data from the table: "region" using primary key columns */
  region_by_pk?: Maybe<Region>;
  /** fetch data from the table: "resource" */
  resource: Array<Resource>;
  /** fetch aggregated fields from the table: "resource" */
  resource_aggregate: Resource_Aggregate;
  /** fetch data from the table: "resource" using primary key columns */
  resource_by_pk?: Maybe<Resource>;
  /** fetch data from the table: "rubbish" */
  rubbish: Array<Rubbish>;
  /** fetch aggregated fields from the table: "rubbish" */
  rubbish_aggregate: Rubbish_Aggregate;
  /** fetch data from the table: "rubbish" using primary key columns */
  rubbish_by_pk?: Maybe<Rubbish>;
  /** fetch data from the table: "rubbish_category" */
  rubbish_category: Array<Rubbish_Category>;
  /** fetch aggregated fields from the table: "rubbish_category" */
  rubbish_category_aggregate: Rubbish_Category_Aggregate;
  /** fetch data from the table: "rubbish_category" using primary key columns */
  rubbish_category_by_pk?: Maybe<Rubbish_Category>;
  rubbish_metrics_campaign_query: Array<Rubbish_Metrics_Model>;
  rubbish_metrics_cleanup_place_query: Array<Rubbish_Metrics_Model>;
  rubbish_metrics_cleanup_query: Array<Rubbish_Metrics_Model>;
  /** fetch data from the table: "rubbish_rubbish_tag" */
  rubbish_rubbish_tag: Array<Rubbish_Rubbish_Tag>;
  /** fetch aggregated fields from the table: "rubbish_rubbish_tag" */
  rubbish_rubbish_tag_aggregate: Rubbish_Rubbish_Tag_Aggregate;
  /** fetch data from the table: "rubbish_rubbish_tag" using primary key columns */
  rubbish_rubbish_tag_by_pk?: Maybe<Rubbish_Rubbish_Tag>;
  /** fetch data from the table: "rubbish_tag" */
  rubbish_tag: Array<Rubbish_Tag>;
  /** fetch aggregated fields from the table: "rubbish_tag" */
  rubbish_tag_aggregate: Rubbish_Tag_Aggregate;
  /** fetch data from the table: "rubbish_tag" using primary key columns */
  rubbish_tag_by_pk?: Maybe<Rubbish_Tag>;
  rubbish_tag_metrics_query: Array<Rubbish_Tag_Metrics_Model>;
  /** fetch data from the table: "rubbish_type_picked" */
  rubbish_type_picked: Array<Rubbish_Type_Picked>;
  /** fetch aggregated fields from the table: "rubbish_type_picked" */
  rubbish_type_picked_aggregate: Rubbish_Type_Picked_Aggregate;
  /** fetch data from the table: "rubbish_type_picked" using primary key columns */
  rubbish_type_picked_by_pk?: Maybe<Rubbish_Type_Picked>;
  /** fetch data from the table: "user_view" */
  user_view: Array<User_View>;
  /** fetch aggregated fields from the table: "user_view" */
  user_view_aggregate: User_View_Aggregate;
};


export type Query_RootArea_TypeArgs = {
  distinct_on?: InputMaybe<Array<Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Area_Type_Order_By>>;
  where?: InputMaybe<Area_Type_Bool_Exp>;
};


export type Query_RootArea_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Area_Type_Order_By>>;
  where?: InputMaybe<Area_Type_Bool_Exp>;
};


export type Query_RootArea_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootBrandArgs = {
  distinct_on?: InputMaybe<Array<Brand_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Brand_Order_By>>;
  where?: InputMaybe<Brand_Bool_Exp>;
};


export type Query_RootBrand_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Brand_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Brand_Order_By>>;
  where?: InputMaybe<Brand_Bool_Exp>;
};


export type Query_RootBrand_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCampaignArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Order_By>>;
  where?: InputMaybe<Campaign_Bool_Exp>;
};


export type Query_RootCampaign_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Order_By>>;
  where?: InputMaybe<Campaign_Bool_Exp>;
};


export type Query_RootCampaign_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCampaign_Rubbish_Metrics_ViewArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Order_By>>;
  where?: InputMaybe<Campaign_Rubbish_Metrics_View_Bool_Exp>;
};


export type Query_RootCampaign_Rubbish_Metrics_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Order_By>>;
  where?: InputMaybe<Campaign_Rubbish_Metrics_View_Bool_Exp>;
};


export type Query_RootCampaign_UserArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


export type Query_RootCampaign_User_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


export type Query_RootCampaign_User_By_PkArgs = {
  campaign_id: Scalars['String']['input'];
  user_view_id: Scalars['uuid']['input'];
};


export type Query_RootCharacterization_LevelArgs = {
  distinct_on?: InputMaybe<Array<Characterization_Level_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Characterization_Level_Order_By>>;
  where?: InputMaybe<Characterization_Level_Bool_Exp>;
};


export type Query_RootCharacterization_Level_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Characterization_Level_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Characterization_Level_Order_By>>;
  where?: InputMaybe<Characterization_Level_Bool_Exp>;
};


export type Query_RootCharacterization_Level_By_PkArgs = {
  id: Scalars['Int']['input'];
};


export type Query_RootCityArgs = {
  distinct_on?: InputMaybe<Array<City_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<City_Order_By>>;
  where?: InputMaybe<City_Bool_Exp>;
};


export type Query_RootCity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<City_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<City_Order_By>>;
  where?: InputMaybe<City_Bool_Exp>;
};


export type Query_RootCity_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCleanupArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};


export type Query_RootCleanup_Agg_Rubbish_Category_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
};


export type Query_RootCleanup_Agg_Rubbish_Category_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
};


export type Query_RootCleanup_Agg_Rubbish_Tag_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
};


export type Query_RootCleanup_Agg_Rubbish_Tag_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
};


export type Query_RootCleanup_Agg_Rubbish_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_View_Bool_Exp>;
};


export type Query_RootCleanup_Agg_Rubbish_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_View_Bool_Exp>;
};


export type Query_RootCleanup_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};


export type Query_RootCleanup_AreaArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Area_Order_By>>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};


export type Query_RootCleanup_Area_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Area_Order_By>>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};


export type Query_RootCleanup_Area_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_CampaignArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


export type Query_RootCleanup_Campaign_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


export type Query_RootCleanup_Campaign_By_PkArgs = {
  campaign_id: Scalars['String']['input'];
  cleanup_id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_FormArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


export type Query_RootCleanup_Form_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


export type Query_RootCleanup_Form_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_Form_DoiArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Doi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};


export type Query_RootCleanup_Form_Doi_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Doi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};


export type Query_RootCleanup_Form_Doi_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_Form_PoiArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Poi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};


export type Query_RootCleanup_Form_Poi_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Poi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};


export type Query_RootCleanup_Form_Poi_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_Form_RubbishArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


export type Query_RootCleanup_Form_Rubbish_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


export type Query_RootCleanup_Form_Rubbish_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_Form_Rubbish_DetailArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


export type Query_RootCleanup_Form_Rubbish_Detail_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


export type Query_RootCleanup_Form_Rubbish_Detail_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_PartnerArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


export type Query_RootCleanup_Partner_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


export type Query_RootCleanup_Partner_By_PkArgs = {
  cleanup_id: Scalars['uuid']['input'];
  partner_id: Scalars['String']['input'];
};


export type Query_RootCleanup_PhotoArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


export type Query_RootCleanup_Photo_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


export type Query_RootCleanup_Photo_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootCleanup_PlaceArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Place_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Place_Order_By>>;
  where?: InputMaybe<Cleanup_Place_Bool_Exp>;
};


export type Query_RootCleanup_Place_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Place_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Place_Order_By>>;
  where?: InputMaybe<Cleanup_Place_Bool_Exp>;
};


export type Query_RootCleanup_Place_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCleanup_TypeArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Bool_Exp>;
};


export type Query_RootCleanup_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Bool_Exp>;
};


export type Query_RootCleanup_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCleanup_Type_RubbishArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


export type Query_RootCleanup_Type_Rubbish_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


export type Query_RootCleanup_Type_Rubbish_By_PkArgs = {
  cleanup_type_id: Scalars['String']['input'];
  rubbish_id: Scalars['String']['input'];
};


export type Query_RootCleanup_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_View_Order_By>>;
  where?: InputMaybe<Cleanup_View_Bool_Exp>;
};


export type Query_RootCleanup_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_View_Order_By>>;
  where?: InputMaybe<Cleanup_View_Bool_Exp>;
};


export type Query_RootDepartementArgs = {
  distinct_on?: InputMaybe<Array<Departement_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Departement_Order_By>>;
  where?: InputMaybe<Departement_Bool_Exp>;
};


export type Query_RootDepartement_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Departement_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Departement_Order_By>>;
  where?: InputMaybe<Departement_Bool_Exp>;
};


export type Query_RootDepartement_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootEnvironment_Area_TypeArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};


export type Query_RootEnvironment_Area_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};


export type Query_RootEnvironment_Area_Type_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootEnvironment_Area_Type_Rubbish_Type_PickedArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};


export type Query_RootEnvironment_Area_Type_Rubbish_Type_Picked_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};


export type Query_RootEnvironment_Area_Type_Rubbish_Type_Picked_By_PkArgs = {
  environment_area_type_id: Scalars['uuid']['input'];
  rubbish_type_picked_id: Scalars['String']['input'];
};


export type Query_RootEnvironment_TypeArgs = {
  distinct_on?: InputMaybe<Array<Environment_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Type_Order_By>>;
  where?: InputMaybe<Environment_Type_Bool_Exp>;
};


export type Query_RootEnvironment_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Type_Order_By>>;
  where?: InputMaybe<Environment_Type_Bool_Exp>;
};


export type Query_RootEnvironment_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootPartnerArgs = {
  distinct_on?: InputMaybe<Array<Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Partner_Order_By>>;
  where?: InputMaybe<Partner_Bool_Exp>;
};


export type Query_RootPartner_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Partner_Order_By>>;
  where?: InputMaybe<Partner_Bool_Exp>;
};


export type Query_RootPartner_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootPoi_TypeArgs = {
  distinct_on?: InputMaybe<Array<Poi_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Poi_Type_Order_By>>;
  where?: InputMaybe<Poi_Type_Bool_Exp>;
};


export type Query_RootPoi_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Poi_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Poi_Type_Order_By>>;
  where?: InputMaybe<Poi_Type_Bool_Exp>;
};


export type Query_RootPoi_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootRegionArgs = {
  distinct_on?: InputMaybe<Array<Region_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Region_Order_By>>;
  where?: InputMaybe<Region_Bool_Exp>;
};


export type Query_RootRegion_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Region_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Region_Order_By>>;
  where?: InputMaybe<Region_Bool_Exp>;
};


export type Query_RootRegion_By_PkArgs = {
  slug: Scalars['String']['input'];
};


export type Query_RootResourceArgs = {
  distinct_on?: InputMaybe<Array<Resource_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Resource_Order_By>>;
  where?: InputMaybe<Resource_Bool_Exp>;
};


export type Query_RootResource_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Resource_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Resource_Order_By>>;
  where?: InputMaybe<Resource_Bool_Exp>;
};


export type Query_RootResource_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Query_RootRubbishArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


export type Query_RootRubbish_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


export type Query_RootRubbish_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootRubbish_CategoryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Category_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Category_Order_By>>;
  where?: InputMaybe<Rubbish_Category_Bool_Exp>;
};


export type Query_RootRubbish_Category_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Category_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Category_Order_By>>;
  where?: InputMaybe<Rubbish_Category_Bool_Exp>;
};


export type Query_RootRubbish_Category_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootRubbish_Metrics_Campaign_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Query_RootRubbish_Metrics_Cleanup_Place_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Query_RootRubbish_Metrics_Cleanup_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Query_RootRubbish_Rubbish_TagArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};


export type Query_RootRubbish_Rubbish_Tag_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};


export type Query_RootRubbish_Rubbish_Tag_By_PkArgs = {
  rubbish_id: Scalars['String']['input'];
  rubbish_tag_id: Scalars['String']['input'];
};


export type Query_RootRubbish_TagArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Tag_Bool_Exp>;
};


export type Query_RootRubbish_Tag_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Tag_Bool_Exp>;
};


export type Query_RootRubbish_Tag_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootRubbish_Tag_Metrics_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Tag_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Tag_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Tag_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Query_RootRubbish_Type_PickedArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
};


export type Query_RootRubbish_Type_Picked_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
};


export type Query_RootRubbish_Type_Picked_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootUser_ViewArgs = {
  distinct_on?: InputMaybe<Array<User_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<User_View_Order_By>>;
  where?: InputMaybe<User_View_Bool_Exp>;
};


export type Query_RootUser_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<User_View_Order_By>>;
  where?: InputMaybe<User_View_Bool_Exp>;
};

/** columns and relationships of "region" */
export type Region = {
  __typename?: 'region';
  code: Scalars['String']['output'];
  /** An array relationship */
  departements: Array<Departement>;
  /** An aggregate relationship */
  departements_aggregate: Departement_Aggregate;
  label?: Maybe<Scalars['String']['output']>;
  slug: Scalars['String']['output'];
};


/** columns and relationships of "region" */
export type RegionDepartementsArgs = {
  distinct_on?: InputMaybe<Array<Departement_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Departement_Order_By>>;
  where?: InputMaybe<Departement_Bool_Exp>;
};


/** columns and relationships of "region" */
export type RegionDepartements_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Departement_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Departement_Order_By>>;
  where?: InputMaybe<Departement_Bool_Exp>;
};

/** aggregated selection of "region" */
export type Region_Aggregate = {
  __typename?: 'region_aggregate';
  aggregate?: Maybe<Region_Aggregate_Fields>;
  nodes: Array<Region>;
};

/** aggregate fields of "region" */
export type Region_Aggregate_Fields = {
  __typename?: 'region_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Region_Max_Fields>;
  min?: Maybe<Region_Min_Fields>;
};


/** aggregate fields of "region" */
export type Region_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Region_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "region". All fields are combined with a logical 'AND'. */
export type Region_Bool_Exp = {
  _and?: InputMaybe<Array<Region_Bool_Exp>>;
  _not?: InputMaybe<Region_Bool_Exp>;
  _or?: InputMaybe<Array<Region_Bool_Exp>>;
  code?: InputMaybe<String_Comparison_Exp>;
  departements?: InputMaybe<Departement_Bool_Exp>;
  departements_aggregate?: InputMaybe<Departement_Aggregate_Bool_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "region" */
export enum Region_Constraint {
  /** unique or primary key constraint on columns "slug" */
  RegionPk = 'region_pk'
}

/** input type for inserting data into table "region" */
export type Region_Insert_Input = {
  code?: InputMaybe<Scalars['String']['input']>;
  departements?: InputMaybe<Departement_Arr_Rel_Insert_Input>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Region_Max_Fields = {
  __typename?: 'region_max_fields';
  code?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
};

/** aggregate min on columns */
export type Region_Min_Fields = {
  __typename?: 'region_min_fields';
  code?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  slug?: Maybe<Scalars['String']['output']>;
};

/** response of any mutation on the table "region" */
export type Region_Mutation_Response = {
  __typename?: 'region_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Region>;
};

/** input type for inserting object relation for remote table "region" */
export type Region_Obj_Rel_Insert_Input = {
  data: Region_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Region_On_Conflict>;
};

/** on_conflict condition type for table "region" */
export type Region_On_Conflict = {
  constraint: Region_Constraint;
  update_columns?: Array<Region_Update_Column>;
  where?: InputMaybe<Region_Bool_Exp>;
};

/** Ordering options when selecting data from "region". */
export type Region_Order_By = {
  code?: InputMaybe<Order_By>;
  departements_aggregate?: InputMaybe<Departement_Aggregate_Order_By>;
  label?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: region */
export type Region_Pk_Columns_Input = {
  slug: Scalars['String']['input'];
};

/** select columns of table "region" */
export enum Region_Select_Column {
  /** column name */
  Code = 'code',
  /** column name */
  Label = 'label',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "region" */
export type Region_Set_Input = {
  code?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "region" */
export type Region_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Region_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Region_Stream_Cursor_Value_Input = {
  code?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "region" */
export enum Region_Update_Column {
  /** column name */
  Code = 'code',
  /** column name */
  Label = 'label',
  /** column name */
  Slug = 'slug'
}

export type Region_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Region_Set_Input>;
  /** filter the rows which have to be updated */
  where: Region_Bool_Exp;
};

/** columns and relationships of "resource" */
export type Resource = {
  __typename?: 'resource';
  created_at: Scalars['timestamptz']['output'];
  description?: Maybe<Scalars['String']['output']>;
  filename?: Maybe<Scalars['String']['output']>;
  id: Scalars['uuid']['output'];
  label?: Maybe<Scalars['String']['output']>;
  updated_at: Scalars['timestamptz']['output'];
};

/** aggregated selection of "resource" */
export type Resource_Aggregate = {
  __typename?: 'resource_aggregate';
  aggregate?: Maybe<Resource_Aggregate_Fields>;
  nodes: Array<Resource>;
};

/** aggregate fields of "resource" */
export type Resource_Aggregate_Fields = {
  __typename?: 'resource_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Resource_Max_Fields>;
  min?: Maybe<Resource_Min_Fields>;
};


/** aggregate fields of "resource" */
export type Resource_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Resource_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "resource". All fields are combined with a logical 'AND'. */
export type Resource_Bool_Exp = {
  _and?: InputMaybe<Array<Resource_Bool_Exp>>;
  _not?: InputMaybe<Resource_Bool_Exp>;
  _or?: InputMaybe<Array<Resource_Bool_Exp>>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  description?: InputMaybe<String_Comparison_Exp>;
  filename?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "resource" */
export enum Resource_Constraint {
  /** unique or primary key constraint on columns "id" */
  ResourcePkey = 'resource_pkey'
}

/** input type for inserting data into table "resource" */
export type Resource_Insert_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Resource_Max_Fields = {
  __typename?: 'resource_max_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  filename?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Resource_Min_Fields = {
  __typename?: 'resource_min_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  filename?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "resource" */
export type Resource_Mutation_Response = {
  __typename?: 'resource_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Resource>;
};

/** on_conflict condition type for table "resource" */
export type Resource_On_Conflict = {
  constraint: Resource_Constraint;
  update_columns?: Array<Resource_Update_Column>;
  where?: InputMaybe<Resource_Bool_Exp>;
};

/** Ordering options when selecting data from "resource". */
export type Resource_Order_By = {
  created_at?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  filename?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: resource */
export type Resource_Pk_Columns_Input = {
  id: Scalars['uuid']['input'];
};

/** select columns of table "resource" */
export enum Resource_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  Filename = 'filename',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "resource" */
export type Resource_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "resource" */
export type Resource_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Resource_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Resource_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "resource" */
export enum Resource_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  Filename = 'filename',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Resource_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Resource_Set_Input>;
  /** filter the rows which have to be updated */
  where: Resource_Bool_Exp;
};

/** columns and relationships of "rubbish" */
export type Rubbish = {
  __typename?: 'rubbish';
  /** An array relationship */
  children: Array<Rubbish>;
  /** An aggregate relationship */
  children_aggregate: Rubbish_Aggregate;
  /** An array relationship */
  cleanup_form_rubbish_details: Array<Cleanup_Form_Rubbish_Detail>;
  /** An aggregate relationship */
  cleanup_form_rubbish_details_aggregate: Cleanup_Form_Rubbish_Detail_Aggregate;
  /** An array relationship */
  cleanup_form_rubbishes: Array<Cleanup_Form_Rubbish>;
  /** An aggregate relationship */
  cleanup_form_rubbishes_aggregate: Cleanup_Form_Rubbish_Aggregate;
  /** An array relationship */
  cleanup_type_rubbishes: Array<Cleanup_Type_Rubbish>;
  /** An aggregate relationship */
  cleanup_type_rubbishes_aggregate: Cleanup_Type_Rubbish_Aggregate;
  comment?: Maybe<Scalars['String']['output']>;
  created_at: Scalars['timestamptz']['output'];
  icon?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  id_zds?: Maybe<Scalars['String']['output']>;
  is_support_brand?: Maybe<Scalars['Boolean']['output']>;
  is_support_doi?: Maybe<Scalars['Boolean']['output']>;
  label: Scalars['String']['output'];
  /** An object relationship */
  parent?: Maybe<Rubbish>;
  parent_id?: Maybe<Scalars['String']['output']>;
  /** A computed field, executes function "rubbish_ancestors" */
  rubbish_ancestors?: Maybe<Array<Rubbish>>;
  /** An object relationship */
  rubbish_category?: Maybe<Rubbish_Category>;
  rubbish_category_id?: Maybe<Scalars['String']['output']>;
  /** A computed field, executes function "rubbish_descendants" */
  rubbish_descendants?: Maybe<Array<Rubbish>>;
  /** An array relationship */
  tags: Array<Rubbish_Rubbish_Tag>;
  /** An aggregate relationship */
  tags_aggregate: Rubbish_Rubbish_Tag_Aggregate;
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "rubbish" */
export type RubbishChildrenArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishChildren_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishCleanup_Form_Rubbish_DetailsArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishCleanup_Form_Rubbish_Details_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishCleanup_Form_RubbishesArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishCleanup_Form_Rubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishCleanup_Type_RubbishesArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishCleanup_Type_Rubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishRubbish_AncestorsArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishRubbish_DescendantsArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishTagsArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};


/** columns and relationships of "rubbish" */
export type RubbishTags_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};

/** aggregated selection of "rubbish" */
export type Rubbish_Aggregate = {
  __typename?: 'rubbish_aggregate';
  aggregate?: Maybe<Rubbish_Aggregate_Fields>;
  nodes: Array<Rubbish>;
};

export type Rubbish_Aggregate_Bool_Exp = {
  bool_and?: InputMaybe<Rubbish_Aggregate_Bool_Exp_Bool_And>;
  bool_or?: InputMaybe<Rubbish_Aggregate_Bool_Exp_Bool_Or>;
  count?: InputMaybe<Rubbish_Aggregate_Bool_Exp_Count>;
};

export type Rubbish_Aggregate_Bool_Exp_Bool_And = {
  arguments: Rubbish_Select_Column_Rubbish_Aggregate_Bool_Exp_Bool_And_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Rubbish_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Rubbish_Aggregate_Bool_Exp_Bool_Or = {
  arguments: Rubbish_Select_Column_Rubbish_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Rubbish_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Rubbish_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Rubbish_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Rubbish_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "rubbish" */
export type Rubbish_Aggregate_Fields = {
  __typename?: 'rubbish_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Rubbish_Max_Fields>;
  min?: Maybe<Rubbish_Min_Fields>;
};


/** aggregate fields of "rubbish" */
export type Rubbish_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Rubbish_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "rubbish" */
export type Rubbish_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Rubbish_Max_Order_By>;
  min?: InputMaybe<Rubbish_Min_Order_By>;
};

/** input type for inserting array relation for remote table "rubbish" */
export type Rubbish_Arr_Rel_Insert_Input = {
  data: Array<Rubbish_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Rubbish_On_Conflict>;
};

/** Boolean expression to filter rows from the table "rubbish". All fields are combined with a logical 'AND'. */
export type Rubbish_Bool_Exp = {
  _and?: InputMaybe<Array<Rubbish_Bool_Exp>>;
  _not?: InputMaybe<Rubbish_Bool_Exp>;
  _or?: InputMaybe<Array<Rubbish_Bool_Exp>>;
  children?: InputMaybe<Rubbish_Bool_Exp>;
  children_aggregate?: InputMaybe<Rubbish_Aggregate_Bool_Exp>;
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Bool_Exp>;
  cleanup_form_rubbishes?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
  cleanup_form_rubbishes_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Aggregate_Bool_Exp>;
  cleanup_type_rubbishes?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
  cleanup_type_rubbishes_aggregate?: InputMaybe<Cleanup_Type_Rubbish_Aggregate_Bool_Exp>;
  comment?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  icon?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  id_zds?: InputMaybe<String_Comparison_Exp>;
  is_support_brand?: InputMaybe<Boolean_Comparison_Exp>;
  is_support_doi?: InputMaybe<Boolean_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  parent?: InputMaybe<Rubbish_Bool_Exp>;
  parent_id?: InputMaybe<String_Comparison_Exp>;
  rubbish_ancestors?: InputMaybe<Rubbish_Bool_Exp>;
  rubbish_category?: InputMaybe<Rubbish_Category_Bool_Exp>;
  rubbish_category_id?: InputMaybe<String_Comparison_Exp>;
  rubbish_descendants?: InputMaybe<Rubbish_Bool_Exp>;
  tags?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
  tags_aggregate?: InputMaybe<Rubbish_Rubbish_Tag_Aggregate_Bool_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** columns and relationships of "rubbish_category" */
export type Rubbish_Category = {
  __typename?: 'rubbish_category';
  color?: Maybe<Scalars['String']['output']>;
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['String']['output'];
  label: Scalars['String']['output'];
  rank?: Maybe<Scalars['Int']['output']>;
  /** An array relationship */
  rubbishes: Array<Rubbish>;
  /** An aggregate relationship */
  rubbishes_aggregate: Rubbish_Aggregate;
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "rubbish_category" */
export type Rubbish_CategoryRubbishesArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


/** columns and relationships of "rubbish_category" */
export type Rubbish_CategoryRubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};

/** aggregated selection of "rubbish_category" */
export type Rubbish_Category_Aggregate = {
  __typename?: 'rubbish_category_aggregate';
  aggregate?: Maybe<Rubbish_Category_Aggregate_Fields>;
  nodes: Array<Rubbish_Category>;
};

/** aggregate fields of "rubbish_category" */
export type Rubbish_Category_Aggregate_Fields = {
  __typename?: 'rubbish_category_aggregate_fields';
  avg?: Maybe<Rubbish_Category_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<Rubbish_Category_Max_Fields>;
  min?: Maybe<Rubbish_Category_Min_Fields>;
  stddev?: Maybe<Rubbish_Category_Stddev_Fields>;
  stddev_pop?: Maybe<Rubbish_Category_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Rubbish_Category_Stddev_Samp_Fields>;
  sum?: Maybe<Rubbish_Category_Sum_Fields>;
  var_pop?: Maybe<Rubbish_Category_Var_Pop_Fields>;
  var_samp?: Maybe<Rubbish_Category_Var_Samp_Fields>;
  variance?: Maybe<Rubbish_Category_Variance_Fields>;
};


/** aggregate fields of "rubbish_category" */
export type Rubbish_Category_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Rubbish_Category_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type Rubbish_Category_Avg_Fields = {
  __typename?: 'rubbish_category_avg_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "rubbish_category". All fields are combined with a logical 'AND'. */
export type Rubbish_Category_Bool_Exp = {
  _and?: InputMaybe<Array<Rubbish_Category_Bool_Exp>>;
  _not?: InputMaybe<Rubbish_Category_Bool_Exp>;
  _or?: InputMaybe<Array<Rubbish_Category_Bool_Exp>>;
  color?: InputMaybe<String_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  rank?: InputMaybe<Int_Comparison_Exp>;
  rubbishes?: InputMaybe<Rubbish_Bool_Exp>;
  rubbishes_aggregate?: InputMaybe<Rubbish_Aggregate_Bool_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "rubbish_category" */
export enum Rubbish_Category_Constraint {
  /** unique or primary key constraint on columns "label" */
  RubbishCategoryLabelKey = 'rubbish_category_label_key',
  /** unique or primary key constraint on columns "id" */
  RubbishCategoryPkey = 'rubbish_category_pkey'
}

/** input type for incrementing numeric columns in table "rubbish_category" */
export type Rubbish_Category_Inc_Input = {
  rank?: InputMaybe<Scalars['Int']['input']>;
};

/** input type for inserting data into table "rubbish_category" */
export type Rubbish_Category_Insert_Input = {
  color?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  rank?: InputMaybe<Scalars['Int']['input']>;
  rubbishes?: InputMaybe<Rubbish_Arr_Rel_Insert_Input>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Rubbish_Category_Max_Fields = {
  __typename?: 'rubbish_category_max_fields';
  color?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  rank?: Maybe<Scalars['Int']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type Rubbish_Category_Min_Fields = {
  __typename?: 'rubbish_category_min_fields';
  color?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  rank?: Maybe<Scalars['Int']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "rubbish_category" */
export type Rubbish_Category_Mutation_Response = {
  __typename?: 'rubbish_category_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Rubbish_Category>;
};

/** input type for inserting object relation for remote table "rubbish_category" */
export type Rubbish_Category_Obj_Rel_Insert_Input = {
  data: Rubbish_Category_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Rubbish_Category_On_Conflict>;
};

/** on_conflict condition type for table "rubbish_category" */
export type Rubbish_Category_On_Conflict = {
  constraint: Rubbish_Category_Constraint;
  update_columns?: Array<Rubbish_Category_Update_Column>;
  where?: InputMaybe<Rubbish_Category_Bool_Exp>;
};

/** Ordering options when selecting data from "rubbish_category". */
export type Rubbish_Category_Order_By = {
  color?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  rank?: InputMaybe<Order_By>;
  rubbishes_aggregate?: InputMaybe<Rubbish_Aggregate_Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: rubbish_category */
export type Rubbish_Category_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "rubbish_category" */
export enum Rubbish_Category_Select_Column {
  /** column name */
  Color = 'color',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Rank = 'rank',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "rubbish_category" */
export type Rubbish_Category_Set_Input = {
  color?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  rank?: InputMaybe<Scalars['Int']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate stddev on columns */
export type Rubbish_Category_Stddev_Fields = {
  __typename?: 'rubbish_category_stddev_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_pop on columns */
export type Rubbish_Category_Stddev_Pop_Fields = {
  __typename?: 'rubbish_category_stddev_pop_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_samp on columns */
export type Rubbish_Category_Stddev_Samp_Fields = {
  __typename?: 'rubbish_category_stddev_samp_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "rubbish_category" */
export type Rubbish_Category_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Rubbish_Category_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Rubbish_Category_Stream_Cursor_Value_Input = {
  color?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  rank?: InputMaybe<Scalars['Int']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type Rubbish_Category_Sum_Fields = {
  __typename?: 'rubbish_category_sum_fields';
  rank?: Maybe<Scalars['Int']['output']>;
};

/** update columns of table "rubbish_category" */
export enum Rubbish_Category_Update_Column {
  /** column name */
  Color = 'color',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Rank = 'rank',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Rubbish_Category_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Rubbish_Category_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Rubbish_Category_Set_Input>;
  /** filter the rows which have to be updated */
  where: Rubbish_Category_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Rubbish_Category_Var_Pop_Fields = {
  __typename?: 'rubbish_category_var_pop_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** aggregate var_samp on columns */
export type Rubbish_Category_Var_Samp_Fields = {
  __typename?: 'rubbish_category_var_samp_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type Rubbish_Category_Variance_Fields = {
  __typename?: 'rubbish_category_variance_fields';
  rank?: Maybe<Scalars['Float']['output']>;
};

/** unique or primary key constraints on table "rubbish" */
export enum Rubbish_Constraint {
  /** unique or primary key constraint on columns "label" */
  RubbishLabelKey = 'rubbish_label_key',
  /** unique or primary key constraint on columns "id" */
  RubbishPkey = 'rubbish_pkey'
}

/** input type for inserting data into table "rubbish" */
export type Rubbish_Insert_Input = {
  children?: InputMaybe<Rubbish_Arr_Rel_Insert_Input>;
  cleanup_form_rubbish_details?: InputMaybe<Cleanup_Form_Rubbish_Detail_Arr_Rel_Insert_Input>;
  cleanup_form_rubbishes?: InputMaybe<Cleanup_Form_Rubbish_Arr_Rel_Insert_Input>;
  cleanup_type_rubbishes?: InputMaybe<Cleanup_Type_Rubbish_Arr_Rel_Insert_Input>;
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  icon?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  is_support_brand?: InputMaybe<Scalars['Boolean']['input']>;
  is_support_doi?: InputMaybe<Scalars['Boolean']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  parent?: InputMaybe<Rubbish_Obj_Rel_Insert_Input>;
  parent_id?: InputMaybe<Scalars['String']['input']>;
  rubbish_category?: InputMaybe<Rubbish_Category_Obj_Rel_Insert_Input>;
  rubbish_category_id?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Rubbish_Rubbish_Tag_Arr_Rel_Insert_Input>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Rubbish_Max_Fields = {
  __typename?: 'rubbish_max_fields';
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  icon?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  parent_id?: Maybe<Scalars['String']['output']>;
  rubbish_category_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by max() on columns of table "rubbish" */
export type Rubbish_Max_Order_By = {
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  icon?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  id_zds?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  parent_id?: InputMaybe<Order_By>;
  rubbish_category_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

export type Rubbish_Metrics_Model = {
  __typename?: 'rubbish_metrics_model';
  id: Scalars['String']['output'];
  label: Scalars['String']['output'];
  quantity: Scalars['bigint']['output'];
  rubbish_id: Scalars['String']['output'];
  rubbish_label: Scalars['String']['output'];
  volume: Scalars['numeric']['output'];
  weight: Scalars['numeric']['output'];
};

/** Boolean expression to filter rows from the logical model for "rubbish_metrics_model". All fields are combined with a logical 'AND'. */
export type Rubbish_Metrics_Model_Bool_Exp_Bool_Exp = {
  _and?: InputMaybe<Array<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>>;
  _not?: InputMaybe<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>;
  _or?: InputMaybe<Array<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  quantity?: InputMaybe<Bigint_Comparison_Exp>;
  rubbish_id?: InputMaybe<String_Comparison_Exp>;
  rubbish_label?: InputMaybe<String_Comparison_Exp>;
  volume?: InputMaybe<Numeric_Comparison_Exp>;
  weight?: InputMaybe<Numeric_Comparison_Exp>;
};

export enum Rubbish_Metrics_Model_Enum_Name {
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  RubbishLabel = 'rubbish_label',
  /** column name */
  Volume = 'volume',
  /** column name */
  Weight = 'weight'
}

/** Ordering options when selecting data from "rubbish_metrics_model". */
export type Rubbish_Metrics_Model_Order_By = {
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  rubbish_label?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Rubbish_Min_Fields = {
  __typename?: 'rubbish_min_fields';
  comment?: Maybe<Scalars['String']['output']>;
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  icon?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  parent_id?: Maybe<Scalars['String']['output']>;
  rubbish_category_id?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by min() on columns of table "rubbish" */
export type Rubbish_Min_Order_By = {
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  icon?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  id_zds?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  parent_id?: InputMaybe<Order_By>;
  rubbish_category_id?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "rubbish" */
export type Rubbish_Mutation_Response = {
  __typename?: 'rubbish_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Rubbish>;
};

/** input type for inserting object relation for remote table "rubbish" */
export type Rubbish_Obj_Rel_Insert_Input = {
  data: Rubbish_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Rubbish_On_Conflict>;
};

/** on_conflict condition type for table "rubbish" */
export type Rubbish_On_Conflict = {
  constraint: Rubbish_Constraint;
  update_columns?: Array<Rubbish_Update_Column>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};

/** Ordering options when selecting data from "rubbish". */
export type Rubbish_Order_By = {
  children_aggregate?: InputMaybe<Rubbish_Aggregate_Order_By>;
  cleanup_form_rubbish_details_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Detail_Aggregate_Order_By>;
  cleanup_form_rubbishes_aggregate?: InputMaybe<Cleanup_Form_Rubbish_Aggregate_Order_By>;
  cleanup_type_rubbishes_aggregate?: InputMaybe<Cleanup_Type_Rubbish_Aggregate_Order_By>;
  comment?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  icon?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  id_zds?: InputMaybe<Order_By>;
  is_support_brand?: InputMaybe<Order_By>;
  is_support_doi?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  parent?: InputMaybe<Rubbish_Order_By>;
  parent_id?: InputMaybe<Order_By>;
  rubbish_ancestors_aggregate?: InputMaybe<Rubbish_Aggregate_Order_By>;
  rubbish_category?: InputMaybe<Rubbish_Category_Order_By>;
  rubbish_category_id?: InputMaybe<Order_By>;
  rubbish_descendants_aggregate?: InputMaybe<Rubbish_Aggregate_Order_By>;
  tags_aggregate?: InputMaybe<Rubbish_Rubbish_Tag_Aggregate_Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: rubbish */
export type Rubbish_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** columns and relationships of "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag = {
  __typename?: 'rubbish_rubbish_tag';
  /** An object relationship */
  rubbish: Rubbish;
  rubbish_id: Scalars['String']['output'];
  /** An object relationship */
  rubbish_tag: Rubbish_Tag;
  rubbish_tag_id: Scalars['String']['output'];
};

/** aggregated selection of "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Aggregate = {
  __typename?: 'rubbish_rubbish_tag_aggregate';
  aggregate?: Maybe<Rubbish_Rubbish_Tag_Aggregate_Fields>;
  nodes: Array<Rubbish_Rubbish_Tag>;
};

export type Rubbish_Rubbish_Tag_Aggregate_Bool_Exp = {
  count?: InputMaybe<Rubbish_Rubbish_Tag_Aggregate_Bool_Exp_Count>;
};

export type Rubbish_Rubbish_Tag_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Aggregate_Fields = {
  __typename?: 'rubbish_rubbish_tag_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Rubbish_Rubbish_Tag_Max_Fields>;
  min?: Maybe<Rubbish_Rubbish_Tag_Min_Fields>;
};


/** aggregate fields of "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Rubbish_Rubbish_Tag_Max_Order_By>;
  min?: InputMaybe<Rubbish_Rubbish_Tag_Min_Order_By>;
};

/** input type for inserting array relation for remote table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Arr_Rel_Insert_Input = {
  data: Array<Rubbish_Rubbish_Tag_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Rubbish_Rubbish_Tag_On_Conflict>;
};

/** Boolean expression to filter rows from the table "rubbish_rubbish_tag". All fields are combined with a logical 'AND'. */
export type Rubbish_Rubbish_Tag_Bool_Exp = {
  _and?: InputMaybe<Array<Rubbish_Rubbish_Tag_Bool_Exp>>;
  _not?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
  _or?: InputMaybe<Array<Rubbish_Rubbish_Tag_Bool_Exp>>;
  rubbish?: InputMaybe<Rubbish_Bool_Exp>;
  rubbish_id?: InputMaybe<String_Comparison_Exp>;
  rubbish_tag?: InputMaybe<Rubbish_Tag_Bool_Exp>;
  rubbish_tag_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "rubbish_rubbish_tag" */
export enum Rubbish_Rubbish_Tag_Constraint {
  /** unique or primary key constraint on columns "rubbish_tag_id", "rubbish_id" */
  RubbishRubbishTagPkey = 'rubbish_rubbish_tag_pkey'
}

/** input type for inserting data into table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Insert_Input = {
  rubbish?: InputMaybe<Rubbish_Obj_Rel_Insert_Input>;
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  rubbish_tag?: InputMaybe<Rubbish_Tag_Obj_Rel_Insert_Input>;
  rubbish_tag_id?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Rubbish_Rubbish_Tag_Max_Fields = {
  __typename?: 'rubbish_rubbish_tag_max_fields';
  rubbish_id?: Maybe<Scalars['String']['output']>;
  rubbish_tag_id?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Max_Order_By = {
  rubbish_id?: InputMaybe<Order_By>;
  rubbish_tag_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Rubbish_Rubbish_Tag_Min_Fields = {
  __typename?: 'rubbish_rubbish_tag_min_fields';
  rubbish_id?: Maybe<Scalars['String']['output']>;
  rubbish_tag_id?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Min_Order_By = {
  rubbish_id?: InputMaybe<Order_By>;
  rubbish_tag_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Mutation_Response = {
  __typename?: 'rubbish_rubbish_tag_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Rubbish_Rubbish_Tag>;
};

/** on_conflict condition type for table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_On_Conflict = {
  constraint: Rubbish_Rubbish_Tag_Constraint;
  update_columns?: Array<Rubbish_Rubbish_Tag_Update_Column>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};

/** Ordering options when selecting data from "rubbish_rubbish_tag". */
export type Rubbish_Rubbish_Tag_Order_By = {
  rubbish?: InputMaybe<Rubbish_Order_By>;
  rubbish_id?: InputMaybe<Order_By>;
  rubbish_tag?: InputMaybe<Rubbish_Tag_Order_By>;
  rubbish_tag_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: rubbish_rubbish_tag */
export type Rubbish_Rubbish_Tag_Pk_Columns_Input = {
  rubbish_id: Scalars['String']['input'];
  rubbish_tag_id: Scalars['String']['input'];
};

/** select columns of table "rubbish_rubbish_tag" */
export enum Rubbish_Rubbish_Tag_Select_Column {
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  RubbishTagId = 'rubbish_tag_id'
}

/** input type for updating data in table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Set_Input = {
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  rubbish_tag_id?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "rubbish_rubbish_tag" */
export type Rubbish_Rubbish_Tag_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Rubbish_Rubbish_Tag_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Rubbish_Rubbish_Tag_Stream_Cursor_Value_Input = {
  rubbish_id?: InputMaybe<Scalars['String']['input']>;
  rubbish_tag_id?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "rubbish_rubbish_tag" */
export enum Rubbish_Rubbish_Tag_Update_Column {
  /** column name */
  RubbishId = 'rubbish_id',
  /** column name */
  RubbishTagId = 'rubbish_tag_id'
}

export type Rubbish_Rubbish_Tag_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Rubbish_Rubbish_Tag_Set_Input>;
  /** filter the rows which have to be updated */
  where: Rubbish_Rubbish_Tag_Bool_Exp;
};

/** select columns of table "rubbish" */
export enum Rubbish_Select_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Icon = 'icon',
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  IsSupportBrand = 'is_support_brand',
  /** column name */
  IsSupportDoi = 'is_support_doi',
  /** column name */
  Label = 'label',
  /** column name */
  ParentId = 'parent_id',
  /** column name */
  RubbishCategoryId = 'rubbish_category_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** select "rubbish_aggregate_bool_exp_bool_and_arguments_columns" columns of table "rubbish" */
export enum Rubbish_Select_Column_Rubbish_Aggregate_Bool_Exp_Bool_And_Arguments_Columns {
  /** column name */
  IsSupportBrand = 'is_support_brand',
  /** column name */
  IsSupportDoi = 'is_support_doi'
}

/** select "rubbish_aggregate_bool_exp_bool_or_arguments_columns" columns of table "rubbish" */
export enum Rubbish_Select_Column_Rubbish_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns {
  /** column name */
  IsSupportBrand = 'is_support_brand',
  /** column name */
  IsSupportDoi = 'is_support_doi'
}

/** input type for updating data in table "rubbish" */
export type Rubbish_Set_Input = {
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  icon?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  is_support_brand?: InputMaybe<Scalars['Boolean']['input']>;
  is_support_doi?: InputMaybe<Scalars['Boolean']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  parent_id?: InputMaybe<Scalars['String']['input']>;
  rubbish_category_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "rubbish" */
export type Rubbish_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Rubbish_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Rubbish_Stream_Cursor_Value_Input = {
  comment?: InputMaybe<Scalars['String']['input']>;
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  icon?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['String']['input']>;
  is_support_brand?: InputMaybe<Scalars['Boolean']['input']>;
  is_support_doi?: InputMaybe<Scalars['Boolean']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  parent_id?: InputMaybe<Scalars['String']['input']>;
  rubbish_category_id?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** columns and relationships of "rubbish_tag" */
export type Rubbish_Tag = {
  __typename?: 'rubbish_tag';
  created_at: Scalars['timestamptz']['output'];
  id: Scalars['String']['output'];
  label: Scalars['String']['output'];
  /** An array relationship */
  rubbishes: Array<Rubbish_Rubbish_Tag>;
  /** An aggregate relationship */
  rubbishes_aggregate: Rubbish_Rubbish_Tag_Aggregate;
  updated_at: Scalars['timestamptz']['output'];
};


/** columns and relationships of "rubbish_tag" */
export type Rubbish_TagRubbishesArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};


/** columns and relationships of "rubbish_tag" */
export type Rubbish_TagRubbishes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};

/** aggregated selection of "rubbish_tag" */
export type Rubbish_Tag_Aggregate = {
  __typename?: 'rubbish_tag_aggregate';
  aggregate?: Maybe<Rubbish_Tag_Aggregate_Fields>;
  nodes: Array<Rubbish_Tag>;
};

/** aggregate fields of "rubbish_tag" */
export type Rubbish_Tag_Aggregate_Fields = {
  __typename?: 'rubbish_tag_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Rubbish_Tag_Max_Fields>;
  min?: Maybe<Rubbish_Tag_Min_Fields>;
};


/** aggregate fields of "rubbish_tag" */
export type Rubbish_Tag_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Rubbish_Tag_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "rubbish_tag". All fields are combined with a logical 'AND'. */
export type Rubbish_Tag_Bool_Exp = {
  _and?: InputMaybe<Array<Rubbish_Tag_Bool_Exp>>;
  _not?: InputMaybe<Rubbish_Tag_Bool_Exp>;
  _or?: InputMaybe<Array<Rubbish_Tag_Bool_Exp>>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
  rubbishes?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
  rubbishes_aggregate?: InputMaybe<Rubbish_Rubbish_Tag_Aggregate_Bool_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "rubbish_tag" */
export enum Rubbish_Tag_Constraint {
  /** unique or primary key constraint on columns "label" */
  RubbishTagLabelKey = 'rubbish_tag_label_key',
  /** unique or primary key constraint on columns "id" */
  RubbishTagPkey = 'rubbish_tag_pkey'
}

/** input type for inserting data into table "rubbish_tag" */
export type Rubbish_Tag_Insert_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  rubbishes?: InputMaybe<Rubbish_Rubbish_Tag_Arr_Rel_Insert_Input>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate max on columns */
export type Rubbish_Tag_Max_Fields = {
  __typename?: 'rubbish_tag_max_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

export type Rubbish_Tag_Metrics_Model = {
  __typename?: 'rubbish_tag_metrics_model';
  cleanup_id: Scalars['uuid']['output'];
  quantity: Scalars['bigint']['output'];
  rubbish_tag_id: Scalars['String']['output'];
  rubbish_tag_label: Scalars['String']['output'];
  volume: Scalars['numeric']['output'];
  weight: Scalars['numeric']['output'];
};

/** Boolean expression to filter rows from the logical model for "rubbish_tag_metrics_model". All fields are combined with a logical 'AND'. */
export type Rubbish_Tag_Metrics_Model_Bool_Exp_Bool_Exp = {
  _and?: InputMaybe<Array<Rubbish_Tag_Metrics_Model_Bool_Exp_Bool_Exp>>;
  _not?: InputMaybe<Rubbish_Tag_Metrics_Model_Bool_Exp_Bool_Exp>;
  _or?: InputMaybe<Array<Rubbish_Tag_Metrics_Model_Bool_Exp_Bool_Exp>>;
  cleanup_id?: InputMaybe<Uuid_Comparison_Exp>;
  quantity?: InputMaybe<Bigint_Comparison_Exp>;
  rubbish_tag_id?: InputMaybe<String_Comparison_Exp>;
  rubbish_tag_label?: InputMaybe<String_Comparison_Exp>;
  volume?: InputMaybe<Numeric_Comparison_Exp>;
  weight?: InputMaybe<Numeric_Comparison_Exp>;
};

export enum Rubbish_Tag_Metrics_Model_Enum_Name {
  /** column name */
  CleanupId = 'cleanup_id',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  RubbishTagId = 'rubbish_tag_id',
  /** column name */
  RubbishTagLabel = 'rubbish_tag_label',
  /** column name */
  Volume = 'volume',
  /** column name */
  Weight = 'weight'
}

/** Ordering options when selecting data from "rubbish_tag_metrics_model". */
export type Rubbish_Tag_Metrics_Model_Order_By = {
  cleanup_id?: InputMaybe<Order_By>;
  quantity?: InputMaybe<Order_By>;
  rubbish_tag_id?: InputMaybe<Order_By>;
  rubbish_tag_label?: InputMaybe<Order_By>;
  volume?: InputMaybe<Order_By>;
  weight?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Rubbish_Tag_Min_Fields = {
  __typename?: 'rubbish_tag_min_fields';
  created_at?: Maybe<Scalars['timestamptz']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  label?: Maybe<Scalars['String']['output']>;
  updated_at?: Maybe<Scalars['timestamptz']['output']>;
};

/** response of any mutation on the table "rubbish_tag" */
export type Rubbish_Tag_Mutation_Response = {
  __typename?: 'rubbish_tag_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Rubbish_Tag>;
};

/** input type for inserting object relation for remote table "rubbish_tag" */
export type Rubbish_Tag_Obj_Rel_Insert_Input = {
  data: Rubbish_Tag_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Rubbish_Tag_On_Conflict>;
};

/** on_conflict condition type for table "rubbish_tag" */
export type Rubbish_Tag_On_Conflict = {
  constraint: Rubbish_Tag_Constraint;
  update_columns?: Array<Rubbish_Tag_Update_Column>;
  where?: InputMaybe<Rubbish_Tag_Bool_Exp>;
};

/** Ordering options when selecting data from "rubbish_tag". */
export type Rubbish_Tag_Order_By = {
  created_at?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
  rubbishes_aggregate?: InputMaybe<Rubbish_Rubbish_Tag_Aggregate_Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: rubbish_tag */
export type Rubbish_Tag_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "rubbish_tag" */
export enum Rubbish_Tag_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "rubbish_tag" */
export type Rubbish_Tag_Set_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** Streaming cursor of the table "rubbish_tag" */
export type Rubbish_Tag_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Rubbish_Tag_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Rubbish_Tag_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars['timestamptz']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updated_at?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** update columns of table "rubbish_tag" */
export enum Rubbish_Tag_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Label = 'label',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Rubbish_Tag_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Rubbish_Tag_Set_Input>;
  /** filter the rows which have to be updated */
  where: Rubbish_Tag_Bool_Exp;
};

/** columns and relationships of "rubbish_type_picked" */
export type Rubbish_Type_Picked = {
  __typename?: 'rubbish_type_picked';
  /** An array relationship */
  environment_area_type_rubbish_type_pickeds: Array<Environment_Area_Type_Rubbish_Type_Picked>;
  /** An aggregate relationship */
  environment_area_type_rubbish_type_pickeds_aggregate: Environment_Area_Type_Rubbish_Type_Picked_Aggregate;
  id: Scalars['String']['output'];
  id_zds: Scalars['uuid']['output'];
  label?: Maybe<Scalars['String']['output']>;
};


/** columns and relationships of "rubbish_type_picked" */
export type Rubbish_Type_PickedEnvironment_Area_Type_Rubbish_Type_PickedsArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};


/** columns and relationships of "rubbish_type_picked" */
export type Rubbish_Type_PickedEnvironment_Area_Type_Rubbish_Type_Pickeds_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};

/** aggregated selection of "rubbish_type_picked" */
export type Rubbish_Type_Picked_Aggregate = {
  __typename?: 'rubbish_type_picked_aggregate';
  aggregate?: Maybe<Rubbish_Type_Picked_Aggregate_Fields>;
  nodes: Array<Rubbish_Type_Picked>;
};

/** aggregate fields of "rubbish_type_picked" */
export type Rubbish_Type_Picked_Aggregate_Fields = {
  __typename?: 'rubbish_type_picked_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Rubbish_Type_Picked_Max_Fields>;
  min?: Maybe<Rubbish_Type_Picked_Min_Fields>;
};


/** aggregate fields of "rubbish_type_picked" */
export type Rubbish_Type_Picked_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Rubbish_Type_Picked_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "rubbish_type_picked". All fields are combined with a logical 'AND'. */
export type Rubbish_Type_Picked_Bool_Exp = {
  _and?: InputMaybe<Array<Rubbish_Type_Picked_Bool_Exp>>;
  _not?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
  _or?: InputMaybe<Array<Rubbish_Type_Picked_Bool_Exp>>;
  environment_area_type_rubbish_type_pickeds?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
  environment_area_type_rubbish_type_pickeds_aggregate?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Bool_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  id_zds?: InputMaybe<Uuid_Comparison_Exp>;
  label?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "rubbish_type_picked" */
export enum Rubbish_Type_Picked_Constraint {
  /** unique or primary key constraint on columns "id" */
  RubbishTypePickedPkey = 'rubbish_type_picked_pkey'
}

/** input type for inserting data into table "rubbish_type_picked" */
export type Rubbish_Type_Picked_Insert_Input = {
  environment_area_type_rubbish_type_pickeds?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Arr_Rel_Insert_Input>;
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type Rubbish_Type_Picked_Max_Fields = {
  __typename?: 'rubbish_type_picked_max_fields';
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
};

/** aggregate min on columns */
export type Rubbish_Type_Picked_Min_Fields = {
  __typename?: 'rubbish_type_picked_min_fields';
  id?: Maybe<Scalars['String']['output']>;
  id_zds?: Maybe<Scalars['uuid']['output']>;
  label?: Maybe<Scalars['String']['output']>;
};

/** response of any mutation on the table "rubbish_type_picked" */
export type Rubbish_Type_Picked_Mutation_Response = {
  __typename?: 'rubbish_type_picked_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int']['output'];
  /** data from the rows affected by the mutation */
  returning: Array<Rubbish_Type_Picked>;
};

/** input type for inserting object relation for remote table "rubbish_type_picked" */
export type Rubbish_Type_Picked_Obj_Rel_Insert_Input = {
  data: Rubbish_Type_Picked_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Rubbish_Type_Picked_On_Conflict>;
};

/** on_conflict condition type for table "rubbish_type_picked" */
export type Rubbish_Type_Picked_On_Conflict = {
  constraint: Rubbish_Type_Picked_Constraint;
  update_columns?: Array<Rubbish_Type_Picked_Update_Column>;
  where?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
};

/** Ordering options when selecting data from "rubbish_type_picked". */
export type Rubbish_Type_Picked_Order_By = {
  environment_area_type_rubbish_type_pickeds_aggregate?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Aggregate_Order_By>;
  id?: InputMaybe<Order_By>;
  id_zds?: InputMaybe<Order_By>;
  label?: InputMaybe<Order_By>;
};

/** primary key columns input for table: rubbish_type_picked */
export type Rubbish_Type_Picked_Pk_Columns_Input = {
  id: Scalars['String']['input'];
};

/** select columns of table "rubbish_type_picked" */
export enum Rubbish_Type_Picked_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  Label = 'label'
}

/** input type for updating data in table "rubbish_type_picked" */
export type Rubbish_Type_Picked_Set_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** Streaming cursor of the table "rubbish_type_picked" */
export type Rubbish_Type_Picked_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Rubbish_Type_Picked_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Rubbish_Type_Picked_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['String']['input']>;
  id_zds?: InputMaybe<Scalars['uuid']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
};

/** update columns of table "rubbish_type_picked" */
export enum Rubbish_Type_Picked_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  Label = 'label'
}

export type Rubbish_Type_Picked_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Rubbish_Type_Picked_Set_Input>;
  /** filter the rows which have to be updated */
  where: Rubbish_Type_Picked_Bool_Exp;
};

/** update columns of table "rubbish" */
export enum Rubbish_Update_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Icon = 'icon',
  /** column name */
  Id = 'id',
  /** column name */
  IdZds = 'id_zds',
  /** column name */
  IsSupportBrand = 'is_support_brand',
  /** column name */
  IsSupportDoi = 'is_support_doi',
  /** column name */
  Label = 'label',
  /** column name */
  ParentId = 'parent_id',
  /** column name */
  RubbishCategoryId = 'rubbish_category_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Rubbish_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Rubbish_Set_Input>;
  /** filter the rows which have to be updated */
  where: Rubbish_Bool_Exp;
};

export type St_D_Within_Geography_Input = {
  distance: Scalars['Float']['input'];
  from: Scalars['geography']['input'];
  use_spheroid?: InputMaybe<Scalars['Boolean']['input']>;
};

export type St_D_Within_Input = {
  distance: Scalars['Float']['input'];
  from: Scalars['geometry']['input'];
};

export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "area_type" */
  area_type: Array<Area_Type>;
  /** fetch aggregated fields from the table: "area_type" */
  area_type_aggregate: Area_Type_Aggregate;
  /** fetch data from the table: "area_type" using primary key columns */
  area_type_by_pk?: Maybe<Area_Type>;
  /** fetch data from the table in a streaming manner: "area_type" */
  area_type_stream: Array<Area_Type>;
  /** fetch data from the table: "brand" */
  brand: Array<Brand>;
  /** fetch aggregated fields from the table: "brand" */
  brand_aggregate: Brand_Aggregate;
  /** fetch data from the table: "brand" using primary key columns */
  brand_by_pk?: Maybe<Brand>;
  /** fetch data from the table in a streaming manner: "brand" */
  brand_stream: Array<Brand>;
  /** fetch data from the table: "campaign" */
  campaign: Array<Campaign>;
  /** fetch aggregated fields from the table: "campaign" */
  campaign_aggregate: Campaign_Aggregate;
  /** fetch data from the table: "campaign" using primary key columns */
  campaign_by_pk?: Maybe<Campaign>;
  /** fetch data from the table: "campaign_rubbish_metrics_view" */
  campaign_rubbish_metrics_view: Array<Campaign_Rubbish_Metrics_View>;
  /** fetch aggregated fields from the table: "campaign_rubbish_metrics_view" */
  campaign_rubbish_metrics_view_aggregate: Campaign_Rubbish_Metrics_View_Aggregate;
  /** fetch data from the table in a streaming manner: "campaign_rubbish_metrics_view" */
  campaign_rubbish_metrics_view_stream: Array<Campaign_Rubbish_Metrics_View>;
  /** fetch data from the table in a streaming manner: "campaign" */
  campaign_stream: Array<Campaign>;
  /** fetch data from the table: "campaign_user" */
  campaign_user: Array<Campaign_User>;
  /** fetch aggregated fields from the table: "campaign_user" */
  campaign_user_aggregate: Campaign_User_Aggregate;
  /** fetch data from the table: "campaign_user" using primary key columns */
  campaign_user_by_pk?: Maybe<Campaign_User>;
  /** fetch data from the table in a streaming manner: "campaign_user" */
  campaign_user_stream: Array<Campaign_User>;
  /** fetch data from the table: "characterization_level" */
  characterization_level: Array<Characterization_Level>;
  /** fetch aggregated fields from the table: "characterization_level" */
  characterization_level_aggregate: Characterization_Level_Aggregate;
  /** fetch data from the table: "characterization_level" using primary key columns */
  characterization_level_by_pk?: Maybe<Characterization_Level>;
  /** fetch data from the table in a streaming manner: "characterization_level" */
  characterization_level_stream: Array<Characterization_Level>;
  /** fetch data from the table: "city" */
  city: Array<City>;
  /** fetch aggregated fields from the table: "city" */
  city_aggregate: City_Aggregate;
  /** fetch data from the table: "city" using primary key columns */
  city_by_pk?: Maybe<City>;
  /** fetch data from the table in a streaming manner: "city" */
  city_stream: Array<City>;
  /** fetch data from the table: "cleanup" */
  cleanup: Array<Cleanup>;
  /** fetch data from the table: "cleanup_agg_rubbish_category_view" */
  cleanup_agg_rubbish_category_view: Array<Cleanup_Agg_Rubbish_Category_View>;
  /** fetch aggregated fields from the table: "cleanup_agg_rubbish_category_view" */
  cleanup_agg_rubbish_category_view_aggregate: Cleanup_Agg_Rubbish_Category_View_Aggregate;
  /** fetch data from the table in a streaming manner: "cleanup_agg_rubbish_category_view" */
  cleanup_agg_rubbish_category_view_stream: Array<Cleanup_Agg_Rubbish_Category_View>;
  /** fetch data from the table: "cleanup_agg_rubbish_tag_view" */
  cleanup_agg_rubbish_tag_view: Array<Cleanup_Agg_Rubbish_Tag_View>;
  /** fetch aggregated fields from the table: "cleanup_agg_rubbish_tag_view" */
  cleanup_agg_rubbish_tag_view_aggregate: Cleanup_Agg_Rubbish_Tag_View_Aggregate;
  /** fetch data from the table in a streaming manner: "cleanup_agg_rubbish_tag_view" */
  cleanup_agg_rubbish_tag_view_stream: Array<Cleanup_Agg_Rubbish_Tag_View>;
  /** fetch data from the table: "cleanup_agg_rubbish_view" */
  cleanup_agg_rubbish_view: Array<Cleanup_Agg_Rubbish_View>;
  /** fetch aggregated fields from the table: "cleanup_agg_rubbish_view" */
  cleanup_agg_rubbish_view_aggregate: Cleanup_Agg_Rubbish_View_Aggregate;
  /** fetch data from the table in a streaming manner: "cleanup_agg_rubbish_view" */
  cleanup_agg_rubbish_view_stream: Array<Cleanup_Agg_Rubbish_View>;
  /** fetch aggregated fields from the table: "cleanup" */
  cleanup_aggregate: Cleanup_Aggregate;
  /** fetch data from the table: "cleanup_area" */
  cleanup_area: Array<Cleanup_Area>;
  /** fetch aggregated fields from the table: "cleanup_area" */
  cleanup_area_aggregate: Cleanup_Area_Aggregate;
  /** fetch data from the table: "cleanup_area" using primary key columns */
  cleanup_area_by_pk?: Maybe<Cleanup_Area>;
  /** fetch data from the table in a streaming manner: "cleanup_area" */
  cleanup_area_stream: Array<Cleanup_Area>;
  /** fetch data from the table: "cleanup" using primary key columns */
  cleanup_by_pk?: Maybe<Cleanup>;
  /** fetch data from the table: "cleanup_campaign" */
  cleanup_campaign: Array<Cleanup_Campaign>;
  /** fetch aggregated fields from the table: "cleanup_campaign" */
  cleanup_campaign_aggregate: Cleanup_Campaign_Aggregate;
  /** fetch data from the table: "cleanup_campaign" using primary key columns */
  cleanup_campaign_by_pk?: Maybe<Cleanup_Campaign>;
  /** fetch data from the table in a streaming manner: "cleanup_campaign" */
  cleanup_campaign_stream: Array<Cleanup_Campaign>;
  /** fetch data from the table: "cleanup_form" */
  cleanup_form: Array<Cleanup_Form>;
  /** fetch aggregated fields from the table: "cleanup_form" */
  cleanup_form_aggregate: Cleanup_Form_Aggregate;
  /** fetch data from the table: "cleanup_form" using primary key columns */
  cleanup_form_by_pk?: Maybe<Cleanup_Form>;
  /** fetch data from the table: "cleanup_form_doi" */
  cleanup_form_doi: Array<Cleanup_Form_Doi>;
  /** fetch aggregated fields from the table: "cleanup_form_doi" */
  cleanup_form_doi_aggregate: Cleanup_Form_Doi_Aggregate;
  /** fetch data from the table: "cleanup_form_doi" using primary key columns */
  cleanup_form_doi_by_pk?: Maybe<Cleanup_Form_Doi>;
  /** fetch data from the table in a streaming manner: "cleanup_form_doi" */
  cleanup_form_doi_stream: Array<Cleanup_Form_Doi>;
  /** fetch data from the table: "cleanup_form_poi" */
  cleanup_form_poi: Array<Cleanup_Form_Poi>;
  /** fetch aggregated fields from the table: "cleanup_form_poi" */
  cleanup_form_poi_aggregate: Cleanup_Form_Poi_Aggregate;
  /** fetch data from the table: "cleanup_form_poi" using primary key columns */
  cleanup_form_poi_by_pk?: Maybe<Cleanup_Form_Poi>;
  /** fetch data from the table in a streaming manner: "cleanup_form_poi" */
  cleanup_form_poi_stream: Array<Cleanup_Form_Poi>;
  /** fetch data from the table: "cleanup_form_rubbish" */
  cleanup_form_rubbish: Array<Cleanup_Form_Rubbish>;
  /** fetch aggregated fields from the table: "cleanup_form_rubbish" */
  cleanup_form_rubbish_aggregate: Cleanup_Form_Rubbish_Aggregate;
  /** fetch data from the table: "cleanup_form_rubbish" using primary key columns */
  cleanup_form_rubbish_by_pk?: Maybe<Cleanup_Form_Rubbish>;
  /** fetch data from the table: "cleanup_form_rubbish_detail" */
  cleanup_form_rubbish_detail: Array<Cleanup_Form_Rubbish_Detail>;
  /** fetch aggregated fields from the table: "cleanup_form_rubbish_detail" */
  cleanup_form_rubbish_detail_aggregate: Cleanup_Form_Rubbish_Detail_Aggregate;
  /** fetch data from the table: "cleanup_form_rubbish_detail" using primary key columns */
  cleanup_form_rubbish_detail_by_pk?: Maybe<Cleanup_Form_Rubbish_Detail>;
  /** fetch data from the table in a streaming manner: "cleanup_form_rubbish_detail" */
  cleanup_form_rubbish_detail_stream: Array<Cleanup_Form_Rubbish_Detail>;
  /** fetch data from the table in a streaming manner: "cleanup_form_rubbish" */
  cleanup_form_rubbish_stream: Array<Cleanup_Form_Rubbish>;
  /** fetch data from the table in a streaming manner: "cleanup_form" */
  cleanup_form_stream: Array<Cleanup_Form>;
  /** fetch data from the table: "cleanup_partner" */
  cleanup_partner: Array<Cleanup_Partner>;
  /** fetch aggregated fields from the table: "cleanup_partner" */
  cleanup_partner_aggregate: Cleanup_Partner_Aggregate;
  /** fetch data from the table: "cleanup_partner" using primary key columns */
  cleanup_partner_by_pk?: Maybe<Cleanup_Partner>;
  /** fetch data from the table in a streaming manner: "cleanup_partner" */
  cleanup_partner_stream: Array<Cleanup_Partner>;
  /** fetch data from the table: "cleanup_photo" */
  cleanup_photo: Array<Cleanup_Photo>;
  /** fetch aggregated fields from the table: "cleanup_photo" */
  cleanup_photo_aggregate: Cleanup_Photo_Aggregate;
  /** fetch data from the table: "cleanup_photo" using primary key columns */
  cleanup_photo_by_pk?: Maybe<Cleanup_Photo>;
  /** fetch data from the table in a streaming manner: "cleanup_photo" */
  cleanup_photo_stream: Array<Cleanup_Photo>;
  /** fetch data from the table: "cleanup_place" */
  cleanup_place: Array<Cleanup_Place>;
  /** fetch aggregated fields from the table: "cleanup_place" */
  cleanup_place_aggregate: Cleanup_Place_Aggregate;
  /** fetch data from the table: "cleanup_place" using primary key columns */
  cleanup_place_by_pk?: Maybe<Cleanup_Place>;
  /** fetch data from the table in a streaming manner: "cleanup_place" */
  cleanup_place_stream: Array<Cleanup_Place>;
  /** fetch data from the table in a streaming manner: "cleanup" */
  cleanup_stream: Array<Cleanup>;
  /** fetch data from the table: "cleanup_type" */
  cleanup_type: Array<Cleanup_Type>;
  /** fetch aggregated fields from the table: "cleanup_type" */
  cleanup_type_aggregate: Cleanup_Type_Aggregate;
  /** fetch data from the table: "cleanup_type" using primary key columns */
  cleanup_type_by_pk?: Maybe<Cleanup_Type>;
  /** fetch data from the table: "cleanup_type_rubbish" */
  cleanup_type_rubbish: Array<Cleanup_Type_Rubbish>;
  /** fetch aggregated fields from the table: "cleanup_type_rubbish" */
  cleanup_type_rubbish_aggregate: Cleanup_Type_Rubbish_Aggregate;
  /** fetch data from the table: "cleanup_type_rubbish" using primary key columns */
  cleanup_type_rubbish_by_pk?: Maybe<Cleanup_Type_Rubbish>;
  /** fetch data from the table in a streaming manner: "cleanup_type_rubbish" */
  cleanup_type_rubbish_stream: Array<Cleanup_Type_Rubbish>;
  /** fetch data from the table in a streaming manner: "cleanup_type" */
  cleanup_type_stream: Array<Cleanup_Type>;
  /** fetch data from the table: "cleanup_view" */
  cleanup_view: Array<Cleanup_View>;
  /** fetch aggregated fields from the table: "cleanup_view" */
  cleanup_view_aggregate: Cleanup_View_Aggregate;
  /** fetch data from the table in a streaming manner: "cleanup_view" */
  cleanup_view_stream: Array<Cleanup_View>;
  /** fetch data from the table: "departement" */
  departement: Array<Departement>;
  /** fetch aggregated fields from the table: "departement" */
  departement_aggregate: Departement_Aggregate;
  /** fetch data from the table: "departement" using primary key columns */
  departement_by_pk?: Maybe<Departement>;
  /** fetch data from the table in a streaming manner: "departement" */
  departement_stream: Array<Departement>;
  /** fetch data from the table: "environment_area_type" */
  environment_area_type: Array<Environment_Area_Type>;
  /** fetch aggregated fields from the table: "environment_area_type" */
  environment_area_type_aggregate: Environment_Area_Type_Aggregate;
  /** fetch data from the table: "environment_area_type" using primary key columns */
  environment_area_type_by_pk?: Maybe<Environment_Area_Type>;
  /** fetch data from the table: "environment_area_type_rubbish_type_picked" */
  environment_area_type_rubbish_type_picked: Array<Environment_Area_Type_Rubbish_Type_Picked>;
  /** fetch aggregated fields from the table: "environment_area_type_rubbish_type_picked" */
  environment_area_type_rubbish_type_picked_aggregate: Environment_Area_Type_Rubbish_Type_Picked_Aggregate;
  /** fetch data from the table: "environment_area_type_rubbish_type_picked" using primary key columns */
  environment_area_type_rubbish_type_picked_by_pk?: Maybe<Environment_Area_Type_Rubbish_Type_Picked>;
  /** fetch data from the table in a streaming manner: "environment_area_type_rubbish_type_picked" */
  environment_area_type_rubbish_type_picked_stream: Array<Environment_Area_Type_Rubbish_Type_Picked>;
  /** fetch data from the table in a streaming manner: "environment_area_type" */
  environment_area_type_stream: Array<Environment_Area_Type>;
  /** fetch data from the table: "environment_type" */
  environment_type: Array<Environment_Type>;
  /** fetch aggregated fields from the table: "environment_type" */
  environment_type_aggregate: Environment_Type_Aggregate;
  /** fetch data from the table: "environment_type" using primary key columns */
  environment_type_by_pk?: Maybe<Environment_Type>;
  /** fetch data from the table in a streaming manner: "environment_type" */
  environment_type_stream: Array<Environment_Type>;
  /** fetch data from the table: "partner" */
  partner: Array<Partner>;
  /** fetch aggregated fields from the table: "partner" */
  partner_aggregate: Partner_Aggregate;
  /** fetch data from the table: "partner" using primary key columns */
  partner_by_pk?: Maybe<Partner>;
  /** fetch data from the table in a streaming manner: "partner" */
  partner_stream: Array<Partner>;
  /** fetch data from the table: "poi_type" */
  poi_type: Array<Poi_Type>;
  /** fetch aggregated fields from the table: "poi_type" */
  poi_type_aggregate: Poi_Type_Aggregate;
  /** fetch data from the table: "poi_type" using primary key columns */
  poi_type_by_pk?: Maybe<Poi_Type>;
  /** fetch data from the table in a streaming manner: "poi_type" */
  poi_type_stream: Array<Poi_Type>;
  /** fetch data from the table: "region" */
  region: Array<Region>;
  /** fetch aggregated fields from the table: "region" */
  region_aggregate: Region_Aggregate;
  /** fetch data from the table: "region" using primary key columns */
  region_by_pk?: Maybe<Region>;
  /** fetch data from the table in a streaming manner: "region" */
  region_stream: Array<Region>;
  /** fetch data from the table: "resource" */
  resource: Array<Resource>;
  /** fetch aggregated fields from the table: "resource" */
  resource_aggregate: Resource_Aggregate;
  /** fetch data from the table: "resource" using primary key columns */
  resource_by_pk?: Maybe<Resource>;
  /** fetch data from the table in a streaming manner: "resource" */
  resource_stream: Array<Resource>;
  /** fetch data from the table: "rubbish" */
  rubbish: Array<Rubbish>;
  /** fetch aggregated fields from the table: "rubbish" */
  rubbish_aggregate: Rubbish_Aggregate;
  /** fetch data from the table: "rubbish" using primary key columns */
  rubbish_by_pk?: Maybe<Rubbish>;
  /** fetch data from the table: "rubbish_category" */
  rubbish_category: Array<Rubbish_Category>;
  /** fetch aggregated fields from the table: "rubbish_category" */
  rubbish_category_aggregate: Rubbish_Category_Aggregate;
  /** fetch data from the table: "rubbish_category" using primary key columns */
  rubbish_category_by_pk?: Maybe<Rubbish_Category>;
  /** fetch data from the table in a streaming manner: "rubbish_category" */
  rubbish_category_stream: Array<Rubbish_Category>;
  rubbish_metrics_campaign_query: Array<Rubbish_Metrics_Model>;
  rubbish_metrics_cleanup_place_query: Array<Rubbish_Metrics_Model>;
  rubbish_metrics_cleanup_query: Array<Rubbish_Metrics_Model>;
  /** fetch data from the table: "rubbish_rubbish_tag" */
  rubbish_rubbish_tag: Array<Rubbish_Rubbish_Tag>;
  /** fetch aggregated fields from the table: "rubbish_rubbish_tag" */
  rubbish_rubbish_tag_aggregate: Rubbish_Rubbish_Tag_Aggregate;
  /** fetch data from the table: "rubbish_rubbish_tag" using primary key columns */
  rubbish_rubbish_tag_by_pk?: Maybe<Rubbish_Rubbish_Tag>;
  /** fetch data from the table in a streaming manner: "rubbish_rubbish_tag" */
  rubbish_rubbish_tag_stream: Array<Rubbish_Rubbish_Tag>;
  /** fetch data from the table in a streaming manner: "rubbish" */
  rubbish_stream: Array<Rubbish>;
  /** fetch data from the table: "rubbish_tag" */
  rubbish_tag: Array<Rubbish_Tag>;
  /** fetch aggregated fields from the table: "rubbish_tag" */
  rubbish_tag_aggregate: Rubbish_Tag_Aggregate;
  /** fetch data from the table: "rubbish_tag" using primary key columns */
  rubbish_tag_by_pk?: Maybe<Rubbish_Tag>;
  rubbish_tag_metrics_query: Array<Rubbish_Tag_Metrics_Model>;
  /** fetch data from the table in a streaming manner: "rubbish_tag" */
  rubbish_tag_stream: Array<Rubbish_Tag>;
  /** fetch data from the table: "rubbish_type_picked" */
  rubbish_type_picked: Array<Rubbish_Type_Picked>;
  /** fetch aggregated fields from the table: "rubbish_type_picked" */
  rubbish_type_picked_aggregate: Rubbish_Type_Picked_Aggregate;
  /** fetch data from the table: "rubbish_type_picked" using primary key columns */
  rubbish_type_picked_by_pk?: Maybe<Rubbish_Type_Picked>;
  /** fetch data from the table in a streaming manner: "rubbish_type_picked" */
  rubbish_type_picked_stream: Array<Rubbish_Type_Picked>;
  /** fetch data from the table: "user_view" */
  user_view: Array<User_View>;
  /** fetch aggregated fields from the table: "user_view" */
  user_view_aggregate: User_View_Aggregate;
  /** fetch data from the table in a streaming manner: "user_view" */
  user_view_stream: Array<User_View>;
};


export type Subscription_RootArea_TypeArgs = {
  distinct_on?: InputMaybe<Array<Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Area_Type_Order_By>>;
  where?: InputMaybe<Area_Type_Bool_Exp>;
};


export type Subscription_RootArea_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Area_Type_Order_By>>;
  where?: InputMaybe<Area_Type_Bool_Exp>;
};


export type Subscription_RootArea_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootArea_Type_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Area_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Area_Type_Bool_Exp>;
};


export type Subscription_RootBrandArgs = {
  distinct_on?: InputMaybe<Array<Brand_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Brand_Order_By>>;
  where?: InputMaybe<Brand_Bool_Exp>;
};


export type Subscription_RootBrand_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Brand_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Brand_Order_By>>;
  where?: InputMaybe<Brand_Bool_Exp>;
};


export type Subscription_RootBrand_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootBrand_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Brand_Stream_Cursor_Input>>;
  where?: InputMaybe<Brand_Bool_Exp>;
};


export type Subscription_RootCampaignArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Order_By>>;
  where?: InputMaybe<Campaign_Bool_Exp>;
};


export type Subscription_RootCampaign_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Order_By>>;
  where?: InputMaybe<Campaign_Bool_Exp>;
};


export type Subscription_RootCampaign_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootCampaign_Rubbish_Metrics_ViewArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Order_By>>;
  where?: InputMaybe<Campaign_Rubbish_Metrics_View_Bool_Exp>;
};


export type Subscription_RootCampaign_Rubbish_Metrics_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_Rubbish_Metrics_View_Order_By>>;
  where?: InputMaybe<Campaign_Rubbish_Metrics_View_Bool_Exp>;
};


export type Subscription_RootCampaign_Rubbish_Metrics_View_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Campaign_Rubbish_Metrics_View_Stream_Cursor_Input>>;
  where?: InputMaybe<Campaign_Rubbish_Metrics_View_Bool_Exp>;
};


export type Subscription_RootCampaign_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Campaign_Stream_Cursor_Input>>;
  where?: InputMaybe<Campaign_Bool_Exp>;
};


export type Subscription_RootCampaign_UserArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


export type Subscription_RootCampaign_User_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


export type Subscription_RootCampaign_User_By_PkArgs = {
  campaign_id: Scalars['String']['input'];
  user_view_id: Scalars['uuid']['input'];
};


export type Subscription_RootCampaign_User_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Campaign_User_Stream_Cursor_Input>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


export type Subscription_RootCharacterization_LevelArgs = {
  distinct_on?: InputMaybe<Array<Characterization_Level_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Characterization_Level_Order_By>>;
  where?: InputMaybe<Characterization_Level_Bool_Exp>;
};


export type Subscription_RootCharacterization_Level_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Characterization_Level_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Characterization_Level_Order_By>>;
  where?: InputMaybe<Characterization_Level_Bool_Exp>;
};


export type Subscription_RootCharacterization_Level_By_PkArgs = {
  id: Scalars['Int']['input'];
};


export type Subscription_RootCharacterization_Level_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Characterization_Level_Stream_Cursor_Input>>;
  where?: InputMaybe<Characterization_Level_Bool_Exp>;
};


export type Subscription_RootCityArgs = {
  distinct_on?: InputMaybe<Array<City_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<City_Order_By>>;
  where?: InputMaybe<City_Bool_Exp>;
};


export type Subscription_RootCity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<City_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<City_Order_By>>;
  where?: InputMaybe<City_Bool_Exp>;
};


export type Subscription_RootCity_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootCity_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<City_Stream_Cursor_Input>>;
  where?: InputMaybe<City_Bool_Exp>;
};


export type Subscription_RootCleanupArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_Category_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_Category_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Category_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_Category_View_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Agg_Rubbish_Category_View_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Category_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_Tag_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_Tag_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_Tag_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_Tag_View_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_Tag_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Agg_Rubbish_View_Order_By>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_View_Bool_Exp>;
};


export type Subscription_RootCleanup_Agg_Rubbish_View_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Agg_Rubbish_View_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Agg_Rubbish_View_Bool_Exp>;
};


export type Subscription_RootCleanup_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Order_By>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};


export type Subscription_RootCleanup_AreaArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Area_Order_By>>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};


export type Subscription_RootCleanup_Area_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Area_Order_By>>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};


export type Subscription_RootCleanup_Area_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Area_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Area_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Area_Bool_Exp>;
};


export type Subscription_RootCleanup_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_CampaignArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


export type Subscription_RootCleanup_Campaign_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Campaign_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Campaign_Order_By>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


export type Subscription_RootCleanup_Campaign_By_PkArgs = {
  campaign_id: Scalars['String']['input'];
  cleanup_id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Campaign_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Campaign_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Campaign_Bool_Exp>;
};


export type Subscription_RootCleanup_FormArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Form_DoiArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Doi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Doi_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Doi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Doi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Doi_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Form_Doi_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Form_Doi_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Form_Doi_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_PoiArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Poi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Poi_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Poi_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Poi_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Poi_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Form_Poi_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Form_Poi_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Form_Poi_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_RubbishArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Rubbish_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Rubbish_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Form_Rubbish_DetailArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Rubbish_Detail_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Form_Rubbish_Detail_Order_By>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Rubbish_Detail_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Form_Rubbish_Detail_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Form_Rubbish_Detail_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Detail_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_Rubbish_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Form_Rubbish_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Form_Rubbish_Bool_Exp>;
};


export type Subscription_RootCleanup_Form_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Form_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Form_Bool_Exp>;
};


export type Subscription_RootCleanup_PartnerArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


export type Subscription_RootCleanup_Partner_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Partner_Order_By>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


export type Subscription_RootCleanup_Partner_By_PkArgs = {
  cleanup_id: Scalars['uuid']['input'];
  partner_id: Scalars['String']['input'];
};


export type Subscription_RootCleanup_Partner_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Partner_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Partner_Bool_Exp>;
};


export type Subscription_RootCleanup_PhotoArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


export type Subscription_RootCleanup_Photo_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Photo_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Photo_Order_By>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


export type Subscription_RootCleanup_Photo_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootCleanup_Photo_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Photo_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Photo_Bool_Exp>;
};


export type Subscription_RootCleanup_PlaceArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Place_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Place_Order_By>>;
  where?: InputMaybe<Cleanup_Place_Bool_Exp>;
};


export type Subscription_RootCleanup_Place_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Place_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Place_Order_By>>;
  where?: InputMaybe<Cleanup_Place_Bool_Exp>;
};


export type Subscription_RootCleanup_Place_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootCleanup_Place_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Place_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Place_Bool_Exp>;
};


export type Subscription_RootCleanup_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Bool_Exp>;
};


export type Subscription_RootCleanup_TypeArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Bool_Exp>;
};


export type Subscription_RootCleanup_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Bool_Exp>;
};


export type Subscription_RootCleanup_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootCleanup_Type_RubbishArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


export type Subscription_RootCleanup_Type_Rubbish_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_Type_Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_Type_Rubbish_Order_By>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


export type Subscription_RootCleanup_Type_Rubbish_By_PkArgs = {
  cleanup_type_id: Scalars['String']['input'];
  rubbish_id: Scalars['String']['input'];
};


export type Subscription_RootCleanup_Type_Rubbish_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Type_Rubbish_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Type_Rubbish_Bool_Exp>;
};


export type Subscription_RootCleanup_Type_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_Type_Bool_Exp>;
};


export type Subscription_RootCleanup_ViewArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_View_Order_By>>;
  where?: InputMaybe<Cleanup_View_Bool_Exp>;
};


export type Subscription_RootCleanup_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cleanup_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Cleanup_View_Order_By>>;
  where?: InputMaybe<Cleanup_View_Bool_Exp>;
};


export type Subscription_RootCleanup_View_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Cleanup_View_Stream_Cursor_Input>>;
  where?: InputMaybe<Cleanup_View_Bool_Exp>;
};


export type Subscription_RootDepartementArgs = {
  distinct_on?: InputMaybe<Array<Departement_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Departement_Order_By>>;
  where?: InputMaybe<Departement_Bool_Exp>;
};


export type Subscription_RootDepartement_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Departement_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Departement_Order_By>>;
  where?: InputMaybe<Departement_Bool_Exp>;
};


export type Subscription_RootDepartement_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootDepartement_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Departement_Stream_Cursor_Input>>;
  where?: InputMaybe<Departement_Bool_Exp>;
};


export type Subscription_RootEnvironment_Area_TypeArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};


export type Subscription_RootEnvironment_Area_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};


export type Subscription_RootEnvironment_Area_Type_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootEnvironment_Area_Type_Rubbish_Type_PickedArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};


export type Subscription_RootEnvironment_Area_Type_Rubbish_Type_Picked_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Area_Type_Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};


export type Subscription_RootEnvironment_Area_Type_Rubbish_Type_Picked_By_PkArgs = {
  environment_area_type_id: Scalars['uuid']['input'];
  rubbish_type_picked_id: Scalars['String']['input'];
};


export type Subscription_RootEnvironment_Area_Type_Rubbish_Type_Picked_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Stream_Cursor_Input>>;
  where?: InputMaybe<Environment_Area_Type_Rubbish_Type_Picked_Bool_Exp>;
};


export type Subscription_RootEnvironment_Area_Type_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Environment_Area_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Environment_Area_Type_Bool_Exp>;
};


export type Subscription_RootEnvironment_TypeArgs = {
  distinct_on?: InputMaybe<Array<Environment_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Type_Order_By>>;
  where?: InputMaybe<Environment_Type_Bool_Exp>;
};


export type Subscription_RootEnvironment_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Environment_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Environment_Type_Order_By>>;
  where?: InputMaybe<Environment_Type_Bool_Exp>;
};


export type Subscription_RootEnvironment_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootEnvironment_Type_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Environment_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Environment_Type_Bool_Exp>;
};


export type Subscription_RootPartnerArgs = {
  distinct_on?: InputMaybe<Array<Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Partner_Order_By>>;
  where?: InputMaybe<Partner_Bool_Exp>;
};


export type Subscription_RootPartner_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Partner_Order_By>>;
  where?: InputMaybe<Partner_Bool_Exp>;
};


export type Subscription_RootPartner_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootPartner_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Partner_Stream_Cursor_Input>>;
  where?: InputMaybe<Partner_Bool_Exp>;
};


export type Subscription_RootPoi_TypeArgs = {
  distinct_on?: InputMaybe<Array<Poi_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Poi_Type_Order_By>>;
  where?: InputMaybe<Poi_Type_Bool_Exp>;
};


export type Subscription_RootPoi_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Poi_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Poi_Type_Order_By>>;
  where?: InputMaybe<Poi_Type_Bool_Exp>;
};


export type Subscription_RootPoi_Type_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootPoi_Type_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Poi_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Poi_Type_Bool_Exp>;
};


export type Subscription_RootRegionArgs = {
  distinct_on?: InputMaybe<Array<Region_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Region_Order_By>>;
  where?: InputMaybe<Region_Bool_Exp>;
};


export type Subscription_RootRegion_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Region_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Region_Order_By>>;
  where?: InputMaybe<Region_Bool_Exp>;
};


export type Subscription_RootRegion_By_PkArgs = {
  slug: Scalars['String']['input'];
};


export type Subscription_RootRegion_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Region_Stream_Cursor_Input>>;
  where?: InputMaybe<Region_Bool_Exp>;
};


export type Subscription_RootResourceArgs = {
  distinct_on?: InputMaybe<Array<Resource_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Resource_Order_By>>;
  where?: InputMaybe<Resource_Bool_Exp>;
};


export type Subscription_RootResource_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Resource_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Resource_Order_By>>;
  where?: InputMaybe<Resource_Bool_Exp>;
};


export type Subscription_RootResource_By_PkArgs = {
  id: Scalars['uuid']['input'];
};


export type Subscription_RootResource_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Resource_Stream_Cursor_Input>>;
  where?: InputMaybe<Resource_Bool_Exp>;
};


export type Subscription_RootRubbishArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


export type Subscription_RootRubbish_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Order_By>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


export type Subscription_RootRubbish_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootRubbish_CategoryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Category_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Category_Order_By>>;
  where?: InputMaybe<Rubbish_Category_Bool_Exp>;
};


export type Subscription_RootRubbish_Category_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Category_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Category_Order_By>>;
  where?: InputMaybe<Rubbish_Category_Bool_Exp>;
};


export type Subscription_RootRubbish_Category_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootRubbish_Category_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Rubbish_Category_Stream_Cursor_Input>>;
  where?: InputMaybe<Rubbish_Category_Bool_Exp>;
};


export type Subscription_RootRubbish_Metrics_Campaign_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Subscription_RootRubbish_Metrics_Cleanup_Place_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Subscription_RootRubbish_Metrics_Cleanup_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Subscription_RootRubbish_Rubbish_TagArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};


export type Subscription_RootRubbish_Rubbish_Tag_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};


export type Subscription_RootRubbish_Rubbish_Tag_By_PkArgs = {
  rubbish_id: Scalars['String']['input'];
  rubbish_tag_id: Scalars['String']['input'];
};


export type Subscription_RootRubbish_Rubbish_Tag_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Rubbish_Rubbish_Tag_Stream_Cursor_Input>>;
  where?: InputMaybe<Rubbish_Rubbish_Tag_Bool_Exp>;
};


export type Subscription_RootRubbish_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Rubbish_Stream_Cursor_Input>>;
  where?: InputMaybe<Rubbish_Bool_Exp>;
};


export type Subscription_RootRubbish_TagArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Tag_Bool_Exp>;
};


export type Subscription_RootRubbish_Tag_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Tag_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Tag_Order_By>>;
  where?: InputMaybe<Rubbish_Tag_Bool_Exp>;
};


export type Subscription_RootRubbish_Tag_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootRubbish_Tag_Metrics_QueryArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Tag_Metrics_Model_Enum_Name>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Tag_Metrics_Model_Order_By>>;
  where?: InputMaybe<Rubbish_Tag_Metrics_Model_Bool_Exp_Bool_Exp>;
};


export type Subscription_RootRubbish_Tag_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Rubbish_Tag_Stream_Cursor_Input>>;
  where?: InputMaybe<Rubbish_Tag_Bool_Exp>;
};


export type Subscription_RootRubbish_Type_PickedArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
};


export type Subscription_RootRubbish_Type_Picked_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Rubbish_Type_Picked_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Rubbish_Type_Picked_Order_By>>;
  where?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
};


export type Subscription_RootRubbish_Type_Picked_By_PkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootRubbish_Type_Picked_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Rubbish_Type_Picked_Stream_Cursor_Input>>;
  where?: InputMaybe<Rubbish_Type_Picked_Bool_Exp>;
};


export type Subscription_RootUser_ViewArgs = {
  distinct_on?: InputMaybe<Array<User_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<User_View_Order_By>>;
  where?: InputMaybe<User_View_Bool_Exp>;
};


export type Subscription_RootUser_View_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_View_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<User_View_Order_By>>;
  where?: InputMaybe<User_View_Bool_Exp>;
};


export type Subscription_RootUser_View_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<User_View_Stream_Cursor_Input>>;
  where?: InputMaybe<User_View_Bool_Exp>;
};

/** Boolean expression to compare columns of type "time". All fields are combined with logical 'AND'. */
export type Time_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['time']['input']>;
  _gt?: InputMaybe<Scalars['time']['input']>;
  _gte?: InputMaybe<Scalars['time']['input']>;
  _in?: InputMaybe<Array<Scalars['time']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['time']['input']>;
  _lte?: InputMaybe<Scalars['time']['input']>;
  _neq?: InputMaybe<Scalars['time']['input']>;
  _nin?: InputMaybe<Array<Scalars['time']['input']>>;
};

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['timestamptz']['input']>;
  _gt?: InputMaybe<Scalars['timestamptz']['input']>;
  _gte?: InputMaybe<Scalars['timestamptz']['input']>;
  _in?: InputMaybe<Array<Scalars['timestamptz']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['timestamptz']['input']>;
  _lte?: InputMaybe<Scalars['timestamptz']['input']>;
  _neq?: InputMaybe<Scalars['timestamptz']['input']>;
  _nin?: InputMaybe<Array<Scalars['timestamptz']['input']>>;
};

/** columns and relationships of "user_view" */
export type User_View = {
  __typename?: 'user_view';
  /** An array relationship */
  campaign_users: Array<Campaign_User>;
  /** An aggregate relationship */
  campaign_users_aggregate: Campaign_User_Aggregate;
  created_timestamp?: Maybe<Scalars['bigint']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  email_constraint?: Maybe<Scalars['String']['output']>;
  email_verified?: Maybe<Scalars['Boolean']['output']>;
  enabled?: Maybe<Scalars['Boolean']['output']>;
  federation_link?: Maybe<Scalars['String']['output']>;
  first_name?: Maybe<Scalars['String']['output']>;
  fullname?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  last_name?: Maybe<Scalars['String']['output']>;
  not_before?: Maybe<Scalars['Int']['output']>;
  realm_id?: Maybe<Scalars['String']['output']>;
  role_id?: Maybe<Scalars['String']['output']>;
  role_name?: Maybe<Scalars['String']['output']>;
  service_account_client_link?: Maybe<Scalars['String']['output']>;
  username?: Maybe<Scalars['String']['output']>;
};


/** columns and relationships of "user_view" */
export type User_ViewCampaign_UsersArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};


/** columns and relationships of "user_view" */
export type User_ViewCampaign_Users_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Campaign_User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Campaign_User_Order_By>>;
  where?: InputMaybe<Campaign_User_Bool_Exp>;
};

/** aggregated selection of "user_view" */
export type User_View_Aggregate = {
  __typename?: 'user_view_aggregate';
  aggregate?: Maybe<User_View_Aggregate_Fields>;
  nodes: Array<User_View>;
};

/** aggregate fields of "user_view" */
export type User_View_Aggregate_Fields = {
  __typename?: 'user_view_aggregate_fields';
  avg?: Maybe<User_View_Avg_Fields>;
  count: Scalars['Int']['output'];
  max?: Maybe<User_View_Max_Fields>;
  min?: Maybe<User_View_Min_Fields>;
  stddev?: Maybe<User_View_Stddev_Fields>;
  stddev_pop?: Maybe<User_View_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<User_View_Stddev_Samp_Fields>;
  sum?: Maybe<User_View_Sum_Fields>;
  var_pop?: Maybe<User_View_Var_Pop_Fields>;
  var_samp?: Maybe<User_View_Var_Samp_Fields>;
  variance?: Maybe<User_View_Variance_Fields>;
};


/** aggregate fields of "user_view" */
export type User_View_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<User_View_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type User_View_Avg_Fields = {
  __typename?: 'user_view_avg_fields';
  created_timestamp?: Maybe<Scalars['Float']['output']>;
  not_before?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "user_view". All fields are combined with a logical 'AND'. */
export type User_View_Bool_Exp = {
  _and?: InputMaybe<Array<User_View_Bool_Exp>>;
  _not?: InputMaybe<User_View_Bool_Exp>;
  _or?: InputMaybe<Array<User_View_Bool_Exp>>;
  campaign_users?: InputMaybe<Campaign_User_Bool_Exp>;
  campaign_users_aggregate?: InputMaybe<Campaign_User_Aggregate_Bool_Exp>;
  created_timestamp?: InputMaybe<Bigint_Comparison_Exp>;
  email?: InputMaybe<String_Comparison_Exp>;
  email_constraint?: InputMaybe<String_Comparison_Exp>;
  email_verified?: InputMaybe<Boolean_Comparison_Exp>;
  enabled?: InputMaybe<Boolean_Comparison_Exp>;
  federation_link?: InputMaybe<String_Comparison_Exp>;
  first_name?: InputMaybe<String_Comparison_Exp>;
  fullname?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  last_name?: InputMaybe<String_Comparison_Exp>;
  not_before?: InputMaybe<Int_Comparison_Exp>;
  realm_id?: InputMaybe<String_Comparison_Exp>;
  role_id?: InputMaybe<String_Comparison_Exp>;
  role_name?: InputMaybe<String_Comparison_Exp>;
  service_account_client_link?: InputMaybe<String_Comparison_Exp>;
  username?: InputMaybe<String_Comparison_Exp>;
};

/** input type for inserting data into table "user_view" */
export type User_View_Insert_Input = {
  campaign_users?: InputMaybe<Campaign_User_Arr_Rel_Insert_Input>;
  created_timestamp?: InputMaybe<Scalars['bigint']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  email_constraint?: InputMaybe<Scalars['String']['input']>;
  email_verified?: InputMaybe<Scalars['Boolean']['input']>;
  enabled?: InputMaybe<Scalars['Boolean']['input']>;
  federation_link?: InputMaybe<Scalars['String']['input']>;
  first_name?: InputMaybe<Scalars['String']['input']>;
  fullname?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  last_name?: InputMaybe<Scalars['String']['input']>;
  not_before?: InputMaybe<Scalars['Int']['input']>;
  realm_id?: InputMaybe<Scalars['String']['input']>;
  role_id?: InputMaybe<Scalars['String']['input']>;
  role_name?: InputMaybe<Scalars['String']['input']>;
  service_account_client_link?: InputMaybe<Scalars['String']['input']>;
  username?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate max on columns */
export type User_View_Max_Fields = {
  __typename?: 'user_view_max_fields';
  created_timestamp?: Maybe<Scalars['bigint']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  email_constraint?: Maybe<Scalars['String']['output']>;
  federation_link?: Maybe<Scalars['String']['output']>;
  first_name?: Maybe<Scalars['String']['output']>;
  fullname?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  last_name?: Maybe<Scalars['String']['output']>;
  not_before?: Maybe<Scalars['Int']['output']>;
  realm_id?: Maybe<Scalars['String']['output']>;
  role_id?: Maybe<Scalars['String']['output']>;
  role_name?: Maybe<Scalars['String']['output']>;
  service_account_client_link?: Maybe<Scalars['String']['output']>;
  username?: Maybe<Scalars['String']['output']>;
};

/** aggregate min on columns */
export type User_View_Min_Fields = {
  __typename?: 'user_view_min_fields';
  created_timestamp?: Maybe<Scalars['bigint']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  email_constraint?: Maybe<Scalars['String']['output']>;
  federation_link?: Maybe<Scalars['String']['output']>;
  first_name?: Maybe<Scalars['String']['output']>;
  fullname?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['uuid']['output']>;
  last_name?: Maybe<Scalars['String']['output']>;
  not_before?: Maybe<Scalars['Int']['output']>;
  realm_id?: Maybe<Scalars['String']['output']>;
  role_id?: Maybe<Scalars['String']['output']>;
  role_name?: Maybe<Scalars['String']['output']>;
  service_account_client_link?: Maybe<Scalars['String']['output']>;
  username?: Maybe<Scalars['String']['output']>;
};

/** input type for inserting object relation for remote table "user_view" */
export type User_View_Obj_Rel_Insert_Input = {
  data: User_View_Insert_Input;
};

/** Ordering options when selecting data from "user_view". */
export type User_View_Order_By = {
  campaign_users_aggregate?: InputMaybe<Campaign_User_Aggregate_Order_By>;
  created_timestamp?: InputMaybe<Order_By>;
  email?: InputMaybe<Order_By>;
  email_constraint?: InputMaybe<Order_By>;
  email_verified?: InputMaybe<Order_By>;
  enabled?: InputMaybe<Order_By>;
  federation_link?: InputMaybe<Order_By>;
  first_name?: InputMaybe<Order_By>;
  fullname?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  last_name?: InputMaybe<Order_By>;
  not_before?: InputMaybe<Order_By>;
  realm_id?: InputMaybe<Order_By>;
  role_id?: InputMaybe<Order_By>;
  role_name?: InputMaybe<Order_By>;
  service_account_client_link?: InputMaybe<Order_By>;
  username?: InputMaybe<Order_By>;
};

/** select columns of table "user_view" */
export enum User_View_Select_Column {
  /** column name */
  CreatedTimestamp = 'created_timestamp',
  /** column name */
  Email = 'email',
  /** column name */
  EmailConstraint = 'email_constraint',
  /** column name */
  EmailVerified = 'email_verified',
  /** column name */
  Enabled = 'enabled',
  /** column name */
  FederationLink = 'federation_link',
  /** column name */
  FirstName = 'first_name',
  /** column name */
  Fullname = 'fullname',
  /** column name */
  Id = 'id',
  /** column name */
  LastName = 'last_name',
  /** column name */
  NotBefore = 'not_before',
  /** column name */
  RealmId = 'realm_id',
  /** column name */
  RoleId = 'role_id',
  /** column name */
  RoleName = 'role_name',
  /** column name */
  ServiceAccountClientLink = 'service_account_client_link',
  /** column name */
  Username = 'username'
}

/** aggregate stddev on columns */
export type User_View_Stddev_Fields = {
  __typename?: 'user_view_stddev_fields';
  created_timestamp?: Maybe<Scalars['Float']['output']>;
  not_before?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_pop on columns */
export type User_View_Stddev_Pop_Fields = {
  __typename?: 'user_view_stddev_pop_fields';
  created_timestamp?: Maybe<Scalars['Float']['output']>;
  not_before?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddev_samp on columns */
export type User_View_Stddev_Samp_Fields = {
  __typename?: 'user_view_stddev_samp_fields';
  created_timestamp?: Maybe<Scalars['Float']['output']>;
  not_before?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "user_view" */
export type User_View_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: User_View_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type User_View_Stream_Cursor_Value_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']['input']>;
  email?: InputMaybe<Scalars['String']['input']>;
  email_constraint?: InputMaybe<Scalars['String']['input']>;
  email_verified?: InputMaybe<Scalars['Boolean']['input']>;
  enabled?: InputMaybe<Scalars['Boolean']['input']>;
  federation_link?: InputMaybe<Scalars['String']['input']>;
  first_name?: InputMaybe<Scalars['String']['input']>;
  fullname?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['uuid']['input']>;
  last_name?: InputMaybe<Scalars['String']['input']>;
  not_before?: InputMaybe<Scalars['Int']['input']>;
  realm_id?: InputMaybe<Scalars['String']['input']>;
  role_id?: InputMaybe<Scalars['String']['input']>;
  role_name?: InputMaybe<Scalars['String']['input']>;
  service_account_client_link?: InputMaybe<Scalars['String']['input']>;
  username?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type User_View_Sum_Fields = {
  __typename?: 'user_view_sum_fields';
  created_timestamp?: Maybe<Scalars['bigint']['output']>;
  not_before?: Maybe<Scalars['Int']['output']>;
};

/** aggregate var_pop on columns */
export type User_View_Var_Pop_Fields = {
  __typename?: 'user_view_var_pop_fields';
  created_timestamp?: Maybe<Scalars['Float']['output']>;
  not_before?: Maybe<Scalars['Float']['output']>;
};

/** aggregate var_samp on columns */
export type User_View_Var_Samp_Fields = {
  __typename?: 'user_view_var_samp_fields';
  created_timestamp?: Maybe<Scalars['Float']['output']>;
  not_before?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type User_View_Variance_Fields = {
  __typename?: 'user_view_variance_fields';
  created_timestamp?: Maybe<Scalars['Float']['output']>;
  not_before?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['uuid']['input']>;
  _gt?: InputMaybe<Scalars['uuid']['input']>;
  _gte?: InputMaybe<Scalars['uuid']['input']>;
  _in?: InputMaybe<Array<Scalars['uuid']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['uuid']['input']>;
  _lte?: InputMaybe<Scalars['uuid']['input']>;
  _neq?: InputMaybe<Scalars['uuid']['input']>;
  _nin?: InputMaybe<Array<Scalars['uuid']['input']>>;
};
