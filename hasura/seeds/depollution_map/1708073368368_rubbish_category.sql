INSERT INTO public.rubbish_category (id,"label",color) VALUES
	 ('glass','Verre','#6CA080'),
	 ('metal','Métal  ','#6F699B'),
	 ('other_waste','Autres déchets','#C5A882'),
	 ('paper_board','Papier et carton','#F5C234'),
	 ('plastic','Plastique','#51B8CB'),
	 ('textile','Textile','#C25553'),
	 ('wood','Bois','#DCB18F');