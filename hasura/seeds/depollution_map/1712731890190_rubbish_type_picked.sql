SET check_function_bodies = false;
INSERT INTO rubbish_type_picked (id, id_zds, label) VALUES ('present_ground_abandoned', '7c96c580-6985-48bf-b4de-f2cd41509a74', 'Présent au sol (abandonné)');
INSERT INTO rubbish_type_picked (id, id_zds, label) VALUES ('failed', 'd5badf12-8fbb-4daa-92c4-f9d1f0c667cf', 'Echoué');
INSERT INTO rubbish_type_picked (id, id_zds, label) VALUES ('present_ground_abandoned_failed', 'c7381604-ee86-4dce-9476-1951b1efb391', 'Présent au sol (abandonné) et échoué');
INSERT INTO rubbish_type_picked (id, id_zds, label) VALUES ('bottom', 'c5c9b6b7-8147-4dba-a11e-64c171ee033e', 'Fond');
INSERT INTO rubbish_type_picked (id, id_zds, label) VALUES ('floating', '443c9f2d-da72-42ae-9ff5-8f2facd49b8f', 'Flottant');
INSERT INTO rubbish_type_picked (id, id_zds, label) VALUES ('floating_bottom', '8b9c7caa-d826-40bb-bfba-7e2e4b1ec98f', 'Flottant et Fond');