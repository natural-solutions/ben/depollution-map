SET check_function_bodies = false;
INSERT INTO public.characterization_level (id, created_at, updated_at, label) VALUES (0, '2024-01-26 16:17:22.69066+00', '2024-01-26 16:17:22.69066+00', 'Niveau 0');
INSERT INTO public.characterization_level (id, created_at, updated_at, label) VALUES (1, '2024-01-26 16:17:22.69066+00', '2024-01-26 16:17:22.69066+00', 'Niveau 1');
INSERT INTO public.characterization_level (id, created_at, updated_at, label) VALUES (2, '2024-01-26 16:17:22.69066+00', '2024-01-26 16:17:22.69066+00', 'Niveau 2');
INSERT INTO public.characterization_level (id, created_at, updated_at, label) VALUES (3, '2024-01-26 16:17:22.69066+00', '2024-01-26 16:17:22.69066+00', 'Niveau 3');
