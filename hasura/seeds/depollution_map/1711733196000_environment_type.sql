INSERT INTO public.environment_type (id,"label",id_zds) VALUES
	 ('watercourse','Cours d''eau','0e6c0232-dd31-4ad2-9617-d13485480ef9'),
	 ('lake_marsh','Lac et marais','15f80e5b-6e91-416d-a509-6c98f0aa29cf'),
	 ('lagoon_pond','Lagune et étang côtier','0e3a1f9b-33b5-4c4b-8b52-d403cec6919d'),
	 ('coastal_terrestrial','Littoral (terrestre)','08b669e5-c807-40d3-806e-5d3191c09dfc'),
	 ('sea_ocean','Mer-océans','014539f4-b8b0-4a9c-a209-993a13a147aa'),
	 ('mountain','Montagne','2985e147-53eb-48ff-aea9-9f2126d0ce83'),
	 ('natural_rural_area','Zone naturelle ou rurale (hors littoral et montagne)','232285f1-a36c-4c20-8cd6-00ad21fc5c37'),
	 ('urban_area','Zone urbaine','167b5f65-221c-40c5-afc2-d3e397738691');