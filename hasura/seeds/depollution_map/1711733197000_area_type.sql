INSERT INTO public.area_type (id,"label",id_zds) VALUES
	 ('other_natural_area','Autre espace naturel','f5d03de6-f3fb-4595-bd68-071daf453953'),
	 ('natural_bank','Berges naturelles (hors plages)','bb845e39-5543-46db-b835-b42c616ad62e'),
	 ('canals_salt_pans','Canaux et salin','9f98ff48-e509-4d59-976b-98ddec122c6b'),
	 ('meadow','Champ et prairie','fc91294b-445b-4610-addd-1884f7b4c3ca'),
	 ('field_meadow_landegarrigue_maquis','Champ - Prairie - Lande garrigue maquis','f048d0df-1271-4bb2-b21a-c6d58024e4ad'),
	 ('dike_structure','Digue et ouvrage','56ef88d1-a127-4312-b4fc-e97804a756b3'),
	 ('dune','Dune','5a5b9691-71eb-4b85-911d-a4f70576b5ac'),
	 ('natural_area_beach_back','Espace naturel en arrière plage / côte (calanque/collines...)','6f482aa4-fc6e-4efc-9955-3eb14d881775'),
	 ('forest','Forêt','df6eb7ca-bc7a-42b3-9bb1-fe94c619b27f'),
	 ('watercourse_bed','Lit du cours d''eau (fond et/ou surface)','af5b532d-e85f-41c3-8370-a36bc53e4d51');
INSERT INTO public.area_type (id,"label",id_zds) VALUES
	 ('mangrove','Mangrove','6ce08387-ba56-4147-b2e0-7dcd7630bc1c'),
	 ('sea_ocean','Mer / océan','3061c0a1-abc6-474b-b219-2a55879ed482'),
	 ('urban_park','Parc urbain - Jardin public','794a1ace-8ef3-4c28-b3cd-9ee8673b889e'),
	 ('parking','Parking','7c099b82-5a3a-42da-adc2-acc909023b93'),
	 ('ski_track','Piste de ski','f8c597eb-6a72-40f5-8a11-63ace1adb75f'),
	 ('beach','Plage (sable/galets/gravillons)','4214888a-8f13-428c-8371-23084b7fee3e'),
	 ('water_body','Plan d''eau (fond et/ou surface)','950570b8-1dd3-4a6e-97ef-87d94b501db7'),
	 ('port','Port','459dd694-d67a-4a05-8f52-ea85a8195edc'),
	 ('port_dam','Port - Barrage','cb7517d1-e63f-499d-9016-8499cb5b6ae1'),
	 ('port_dam_lock','Port - Barrage - Écluse','c853cac4-2c1f-4f0f-8919-113ab122abe6');
INSERT INTO public.area_type (id,"label",id_zds) VALUES
	 ('quay_dykes_structures','Quai - Digues - ouvrages','bbb12717-2e41-440c-b45b-bc2d2f39321b'),
	 ('rock_creek','Rocher et crique','3be65b46-97af-485b-8bf3-cbef5e8fb4e9'),
	 ('road','Route','791cc753-9ddb-4b51-838f-fec661ac8841'),
	 ('road_street_square','Route - Rue - Place','d52ba08d-ef95-40aa-93df-ad8590a398f5'),
	 ('trail_path','Sentier et chemin','774d7a3a-bd7d-474e-a327-4e9bc0f8f352'),
	 ('ski_resort','Station de ski','fb9de3f5-ff7e-4199-8a53-fb34d9f4ada6'),
	 ('railway_track','Voie de chemin de fer','cbcddc23-6c92-42e9-9a78-fa93e0e6f24d');