DROP VIEW "public"."cleanup_view";

ALTER TABLE "public"."campaign_user" ALTER COLUMN "user_view_id" TYPE text;
ALTER TABLE "public"."cleanup" ALTER COLUMN "owner_id" TYPE text;
ALTER TABLE "public"."cleanup_form" ALTER COLUMN "owner_id" TYPE text;

CREATE OR REPLACE VIEW "public"."cleanup_view"
AS SELECT c.id,
    c.created_at,
    c.updated_at,
    c.label,
    c.briefing_date,
    c.total_area,
    c.start_at,
    c.start_point,
    c.city_id,
    c.status,
    c.area_type_id,
    c.environment_type_id,
    c.cleanup_type_id,
    c.cleanup_place_id,
    c.owner_id,
    c.description,
    c.total_linear,
    c.total_weight,
    c.total_volume,
    c.total_participants,
    carv_butts.sum_quantity AS total_butts
   FROM cleanup c
     LEFT JOIN cleanup_agg_rubbish_view carv_butts ON carv_butts.cleanup_id = c.id AND carv_butts.rubbish_id = 'butts'::text;