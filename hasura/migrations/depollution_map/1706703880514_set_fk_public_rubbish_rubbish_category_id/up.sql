alter table "public"."rubbish"
  add constraint "rubbish_rubbish_category_id_fkey"
  foreign key ("rubbish_category_id")
  references "public"."rubbish_category"
  ("id") on update restrict on delete restrict;
