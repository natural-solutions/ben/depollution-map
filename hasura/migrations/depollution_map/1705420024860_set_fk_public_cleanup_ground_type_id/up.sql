alter table "public"."cleanup"
  add constraint "cleanup_ground_type_id_fkey"
  foreign key ("ground_type_id")
  references "public"."ground_type"
  ("id") on update restrict on delete restrict;
