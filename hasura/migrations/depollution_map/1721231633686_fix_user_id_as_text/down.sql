DROP VIEW public.user_view;
CREATE OR REPLACE VIEW public.user_view
AS SELECT ue.id::uuid AS id,
    ue.email,
    ue.email_constraint,
    ue.email_verified,
    ue.enabled,
    ue.federation_link,
    ue.first_name,
    ue.last_name,
    ue.realm_id,
    ue.username,
    ue.created_timestamp,
    ue.service_account_client_link,
    ue.not_before,
    concat(ue.last_name, ' ', ue.first_name) AS fullname,
    ur.id AS role_id,
    ur.name AS role_name
   FROM keycloak.user_entity ue
     LEFT JOIN LATERAL ( SELECT kr.id,
            kr.name
           FROM keycloak.user_role_mapping urm
             JOIN keycloak.keycloak_role kr ON kr.id::text = urm.role_id::text
          WHERE urm.user_id::text = ue.id::text AND (kr.name::text = ANY (ARRAY['admin'::character varying::text, 'editor'::character varying::text]))
         LIMIT 1) ur ON true
  WHERE ue.realm_id::text = 'depollution-map'::text;