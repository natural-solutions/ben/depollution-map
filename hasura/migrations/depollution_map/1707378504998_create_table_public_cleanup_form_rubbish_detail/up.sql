CREATE TABLE "public"."cleanup_form_rubbish_detail" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "cleanup_form_id" uuid NOT NULL, "rubbish_id" text NOT NULL, "quantity" integer, "weight" numeric, "volume" integer, "cleanup_area_id" uuid, "brand_id" text, PRIMARY KEY ("id") , FOREIGN KEY ("brand_id") REFERENCES "public"."brand"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("cleanup_form_id") REFERENCES "public"."cleanup_form"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("cleanup_area_id") REFERENCES "public"."cleanup_area"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("rubbish_id") REFERENCES "public"."rubbish"("id") ON UPDATE restrict ON DELETE restrict);
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_cleanup_form_rubbish_detail_updated_at"
BEFORE UPDATE ON "public"."cleanup_form_rubbish_detail"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_cleanup_form_rubbish_detail_updated_at" ON "public"."cleanup_form_rubbish_detail"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
