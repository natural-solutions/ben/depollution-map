CREATE OR REPLACE VIEW public.user_view
AS SELECT ue.id::uuid AS id,
    ue.email,
    ue.email_constraint,
    ue.email_verified,
    ue.enabled,
    ue.federation_link,
    ue.first_name,
    ue.last_name,
    ue.realm_id,
    ue.username,
    ue.created_timestamp,
    ue.service_account_client_link,
    ue.not_before,
    concat(ue.last_name, ' ', ue.first_name) AS fullname, 
    ur.id as role_id, 
    ur."name" as role_name
   FROM keycloak.user_entity ue
LEFT JOIN LATERAL (
    SELECT kr.id, kr."name" 
    FROM keycloak.user_role_mapping urm
    join keycloak.keycloak_role kr on kr.id = urm.role_id 
    WHERE urm.user_id = ue.id::text  and kr."name" in ('admin','editor')
    LIMIT 1
) ur ON true
WHERE ue.realm_id::text = 'depollution-map'::text;
