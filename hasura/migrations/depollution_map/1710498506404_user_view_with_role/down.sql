DROP VIEW "public"."user_view";
CREATE OR REPLACE VIEW public.user_view
AS SELECT CAST(ue.id as UUID) as id,
    ue.email,
    ue.email_constraint,
    ue.email_verified,
    ue.enabled,
    ue.federation_link,
    ue.first_name,
    ue.last_name,
    ue.realm_id,
    ue.username,
    ue.created_timestamp,
    ue.service_account_client_link,
    ue.not_before,
    concat(ue.last_name, ' ', ue.first_name) AS fullname
FROM keycloak.user_entity ue
WHERE ue.realm_id::text = 'depollution-map'::text;