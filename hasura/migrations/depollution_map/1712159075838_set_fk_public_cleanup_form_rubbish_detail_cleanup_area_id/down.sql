alter table "public"."cleanup_form_rubbish_detail" drop constraint "cleanup_form_rubbish_detail_cleanup_area_id_fkey",
  add constraint "cleanup_form_rubbish_detail_cleanup_area_id_fkey"
  foreign key ("cleanup_area_id")
  references "public"."cleanup_area"
  ("id") on update restrict on delete restrict;
