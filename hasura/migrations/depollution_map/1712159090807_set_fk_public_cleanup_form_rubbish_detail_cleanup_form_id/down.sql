alter table "public"."cleanup_form_rubbish_detail" drop constraint "cleanup_form_rubbish_detail_cleanup_form_id_fkey",
  add constraint "cleanup_form_rubbish_detail_cleanup_form_id_fkey"
  foreign key ("cleanup_form_id")
  references "public"."cleanup_form"
  ("id") on update restrict on delete restrict;
