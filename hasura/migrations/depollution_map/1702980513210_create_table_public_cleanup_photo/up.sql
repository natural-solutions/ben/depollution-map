CREATE TABLE "public"."cleanup_photo" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "filename" text NOT NULL, "comment" text, "cleanup_id" uuid NOT NULL, "cleanup_area_id" uuid, PRIMARY KEY ("id") , FOREIGN KEY ("cleanup_id") REFERENCES "public"."cleanup"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("cleanup_area_id") REFERENCES "public"."cleanup_area"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("filename"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_cleanup_photo_updated_at"
BEFORE UPDATE ON "public"."cleanup_photo"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_cleanup_photo_updated_at" ON "public"."cleanup_photo"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
