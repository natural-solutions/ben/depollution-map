alter table "public"."rubbish_rubbish_tag"
  add constraint "rubbish_rubbish_tag_rubbish_tag_id_fkey"
  foreign key ("rubbish_tag_id")
  references "public"."rubbish_tag"
  ("id") on update restrict on delete restrict;
