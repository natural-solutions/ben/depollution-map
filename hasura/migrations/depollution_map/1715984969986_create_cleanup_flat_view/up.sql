CREATE INDEX cleanup_form_cleanup_id ON cleanup_form (cleanup_id);
CREATE INDEX cleanup_form_rubbish_cleanup_form_id ON cleanup_form_rubbish (cleanup_form_id);
CREATE INDEX cleanup_form_rubbish_rubbish_id ON cleanup_form_rubbish (rubbish_id);

create or replace view cleanup_view as
SELECT c.*,
    carv_butts.sum_quantity as total_butts
FROM cleanup c
JOIN cleanup_agg_rubbish_view carv_butts ON carv_butts.cleanup_id = c.id AND carv_butts.rubbish_id = 'butts';