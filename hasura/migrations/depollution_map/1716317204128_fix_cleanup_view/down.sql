drop view cleanup_view;

create or replace view cleanup_view as
SELECT c.*,
    carv_butts.sum_quantity as total_butts
FROM cleanup c
JOIN cleanup_agg_rubbish_view carv_butts ON carv_butts.cleanup_id = c.id AND carv_butts.rubbish_id = 'butts';