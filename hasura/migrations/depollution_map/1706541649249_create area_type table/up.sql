CREATE TABLE
	public.area_type (
		id text NOT NULL,
		label text NOT NULL,
		CONSTRAINT area_type_label_key UNIQUE (label),
		CONSTRAINT area_type_pkey PRIMARY KEY (id)
	);

INSERT INTO
	public.area_type (id, label)
VALUES
	('natural_bank', 'Berges naturelles (hors plages)'),
	(
		'watercourse_bed',
		'Lit du cours d"eau (fond et/ou surface)'
	),
	('urban_park', 'Parc urbain - Jardin public'),
	('beach', 'Plage (sable/galets/gravillons)'),
	('port_dam_lock', 'Port - Barrage - Écluse'),
	(
		'quay_dykes_structures',
		'Quai - Digues - ouvrages'
	),
	('water_body', 'Plan d"eau (fond et/ou surface)'),
	('port_dam', 'Port - Barrage'),
	('canals_salt_pans', 'Canaux et salin'),
	('dike_structure', 'Digue et ouvrage');

INSERT INTO
	public.area_type (id, label)
VALUES
	(
		'natural_area_beach_back',
		'Espace naturel en arrière plage / côte (calanque/collines...)'
	),
	('parking', 'Parking'),
	('port', 'Port'),
	('rock_creek', 'Rocher et crique'),
	('road', 'Route'),
	('trail_path', 'Sentier et chemin'),
	('dune', 'Dune'),
	('mangrove', 'Mangrove'),
	('sea_ocean', 'Mer / océan'),
	('other_natural_area', 'Autre espace naturel');

INSERT INTO
	public.area_type (id, label)
VALUES
	('meadow', 'Champ et prairie'),
	('forest', 'Forêt'),
	('ski_track', 'Piste de ski'),
	('ski_resort', 'Station de ski'),
	(
		'field_meadow_landegarrigue_maquis',
		'Champ - Prairie - Lande garrigue maquis'
	),
	('railway_track', 'Voie de chemin de fer'),
	('road_street_square', 'Route - Rue - Place');
