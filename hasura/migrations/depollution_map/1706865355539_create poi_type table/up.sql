create table poi_type (
	id text,
	label text,
	logo text,
	constraint poi_type_fk primary key (id)
);
