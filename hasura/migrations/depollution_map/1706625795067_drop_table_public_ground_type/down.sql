CREATE TABLE public.ground_type (
	id text NOT NULL,
	created_at timestamptz NOT NULL DEFAULT now(),
	updated_at timestamptz NOT NULL DEFAULT now(),
	label text NULL,
	CONSTRAINT ground_type_label_key null,
	CONSTRAINT ground_type_pkey PRIMARY KEY(id)
);