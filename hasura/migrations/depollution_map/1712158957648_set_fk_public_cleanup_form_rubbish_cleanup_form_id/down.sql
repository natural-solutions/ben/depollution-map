alter table "public"."cleanup_form_rubbish" drop constraint "cleanup_form_rubbish_cleanup_form_id_fkey",
  add constraint "cleanup_form_rubbish_cleanup_form_id_fkey"
  foreign key ("cleanup_form_id")
  references "public"."cleanup_form"
  ("id") on update restrict on delete restrict;
