CREATE TABLE "public"."rubbish" ("id" text NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "label" text NOT NULL, "material_id" text, "comment" text, "icon" text, PRIMARY KEY ("id") , FOREIGN KEY ("material_id") REFERENCES "public"."material"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("label"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_rubbish_updated_at"
BEFORE UPDATE ON "public"."rubbish"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_rubbish_updated_at" ON "public"."rubbish"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
