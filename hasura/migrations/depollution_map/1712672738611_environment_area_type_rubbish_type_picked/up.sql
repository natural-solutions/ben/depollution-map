create table environment_area_type_rubbish_type_picked
(
    environment_area_type_id uuid         not null
        constraint environment_area_type_rubbish_type_picked_environment_fk
            references environment_area_type,
    rubbish_type_id          varchar(255) not null
        constraint environment_area_type_rubbish_type_picked_rubbish_fk
            references rubbish_type_picked
);
