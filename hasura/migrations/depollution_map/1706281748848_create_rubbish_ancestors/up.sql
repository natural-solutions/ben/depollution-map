CREATE OR REPLACE FUNCTION public.rubbish_ancestors(rubbish_row rubbish, hasura_session json)
 RETURNS SETOF rubbish
 LANGUAGE plpgsql
 STABLE
AS $function$
BEGIN
  IF hasura_session->>'x-hasura-role' = 'admin' THEN
    RETURN QUERY
    WITH RECURSIVE rubbish_recursive AS (
      SELECT * FROM rubbish WHERE id = rubbish_row.parent_id
      UNION ALL
      SELECT r.* FROM rubbish r
      JOIN rubbish_recursive rr ON r.id = rr.parent_id
    )
    SELECT * FROM rubbish_recursive WHERE id != rubbish_row.id;
  END IF;

  RETURN;
END;
$function$;
