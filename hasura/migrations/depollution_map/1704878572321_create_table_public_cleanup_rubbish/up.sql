CREATE TABLE "public"."cleanup_rubbish" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "cleanup_id" uuid, "rubbish_id" text, "quantity" integer, "weight" numeric, "volume" integer, "label" text, "comment" text, "brand_id" text, "cleanup_area_id" uuid, PRIMARY KEY ("id") , FOREIGN KEY ("cleanup_id") REFERENCES "public"."cleanup"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("rubbish_id") REFERENCES "public"."rubbish"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("brand_id") REFERENCES "public"."brand"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("cleanup_area_id") REFERENCES "public"."cleanup_area"("id") ON UPDATE restrict ON DELETE restrict);
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_cleanup_rubbish_updated_at"
BEFORE UPDATE ON "public"."cleanup_rubbish"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_cleanup_rubbish_updated_at" ON "public"."cleanup_rubbish"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
