alter table "public"."cleanup_photo" drop constraint "cleanup_photo_cleanup_area_id_fkey",
  add constraint "cleanup_photo_cleanup_area_id_fkey"
  foreign key ("cleanup_area_id")
  references "public"."cleanup_area"
  ("id") on update restrict on delete cascade;
