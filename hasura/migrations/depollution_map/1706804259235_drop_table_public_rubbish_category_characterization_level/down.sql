CREATE TABLE "public"."rubbish_category_characterization_level" ("rubbish_category_id" text NOT NULL, "characterization_level_id" integer NOT NULL, PRIMARY KEY ("rubbish_category_id","characterization_level_id") , FOREIGN KEY ("rubbish_category_id") REFERENCES "public"."rubbish_category"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("characterization_level_id") REFERENCES "public"."characterization_level"("id") ON UPDATE restrict ON DELETE restrict);
alter table "public"."rubbish_category_characterization_level" add column "has_qty" boolean
 null;
alter table "public"."rubbish_category_characterization_level" add column "has_wt_vol" boolean
 null;
