ALTER TABLE cleanup_campaign
DROP CONSTRAINT IF EXISTS cleanup_campaign_cleanup_id_fkey;

ALTER TABLE cleanup_campaign
ADD CONSTRAINT cleanup_campaign_cleanup_id_fkey
FOREIGN KEY (cleanup_id)
REFERENCES cleanup (id)
ON DELETE CASCADE;
