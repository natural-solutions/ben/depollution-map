alter table "public"."cleanup_form_rubbish" drop constraint "cleanup_rubbish_cleanup_area_id_fkey",
  add constraint "cleanup_form_rubbish_cleanup_area_id_fkey"
  foreign key ("cleanup_area_id")
  references "public"."cleanup_area"
  ("id") on update restrict on delete cascade;
