alter table "public"."cleanup_area" drop constraint "cleanup_area_cleanup_id_fkey",
  add constraint "cleanup_area_cleanup_id_fkey"
  foreign key ("cleanup_id")
  references "public"."cleanup"
  ("id") on update restrict on delete restrict;
