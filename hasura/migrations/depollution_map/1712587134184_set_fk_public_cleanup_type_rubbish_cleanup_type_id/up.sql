alter table "public"."cleanup_type_rubbish" drop constraint "cleanup_type_rubbish_cleanup_type_id_fkey",
  add constraint "cleanup_type_rubbish_cleanup_type_id_fkey"
  foreign key ("cleanup_type_id")
  references "public"."cleanup_type"
  ("id") on update restrict on delete cascade;
