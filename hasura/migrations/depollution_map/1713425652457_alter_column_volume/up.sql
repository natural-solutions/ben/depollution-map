DROP VIEW "public"."campaign_rubbish_metrics_view";
ALTER TABLE "public"."cleanup_form_rubbish" ALTER COLUMN "volume" TYPE numeric;
ALTER TABLE "public"."cleanup_form_rubbish_detail" ALTER COLUMN "volume" TYPE numeric;

CREATE OR REPLACE VIEW "public"."campaign_rubbish_metrics_view" AS 
SELECT c.id AS campaign_id,
  r.rubbish_category_id AS category_id,
  sum(cfr.quantity) AS sum_qty,
  sum(cfr.weight) AS sum_weight,
  sum(cfr.volume) AS sum_volume
  FROM (((((campaign c
    LEFT JOIN cleanup_campaign cc ON ((cc.campaign_id = c.id)))
    LEFT JOIN cleanup c2 ON ((c2.id = cc.cleanup_id)))
    LEFT JOIN cleanup_form cf ON ((cf.cleanup_id = c2.id)))
    LEFT JOIN cleanup_form_rubbish cfr ON ((cfr.cleanup_form_id = cf.id)))
    LEFT JOIN rubbish r ON ((r.id = cfr.rubbish_id)))
WHERE (r.rubbish_category_id = ANY (ARRAY['plastic'::text, 'metal'::text, 'glass'::text, 'fabric'::text, 'paper_board'::text, 'wood'::text]))
GROUP BY c.id, r.rubbish_category_id;