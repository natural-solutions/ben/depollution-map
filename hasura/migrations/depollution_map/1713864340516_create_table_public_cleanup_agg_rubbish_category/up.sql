CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."cleanup_agg_rubbish_category" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "cleanup_id" uuid NOT NULL, "rubbish_category_id" text NOT NULL, "sum_quantity" integer, "sum_weight" numeric, "sum_volume" numeric, PRIMARY KEY ("id") , FOREIGN KEY ("cleanup_id") REFERENCES "public"."cleanup"("id") ON UPDATE restrict ON DELETE cascade, FOREIGN KEY ("rubbish_category_id") REFERENCES "public"."rubbish_category"("id") ON UPDATE restrict ON DELETE restrict);

insert into "public"."cleanup_agg_rubbish_category" (cleanup_id, rubbish_category_id, sum_quantity, sum_weight, sum_volume)
select c.id, 
r.rubbish_category_id, 
sum(cfr.quantity), 
sum(cfr.weight), 
sum(cfr.volume)
from "public"."cleanup" c 
join "public"."cleanup_form" cf on cf.cleanup_id = c.id 
join "public"."cleanup_form_rubbish" cfr on cfr.cleanup_form_id = cf.id
join "public"."rubbish" r on r.id = cfr.rubbish_id
group by c.id, r.rubbish_category_id;