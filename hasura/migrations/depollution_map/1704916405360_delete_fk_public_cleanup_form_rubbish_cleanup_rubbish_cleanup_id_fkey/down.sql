alter table "public"."cleanup_form_rubbish"
  add constraint "cleanup_rubbish_cleanup_id_fkey"
  foreign key ("cleanup_id")
  references "public"."cleanup"
  ("id") on update restrict on delete restrict;
