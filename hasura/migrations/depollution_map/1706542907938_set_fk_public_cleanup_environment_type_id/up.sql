alter table "public"."cleanup"
  add constraint "cleanup_environment_type_id_fkey"
  foreign key ("environment_type_id")
  references "public"."environment_type"
  ("id") on update restrict on delete restrict;
