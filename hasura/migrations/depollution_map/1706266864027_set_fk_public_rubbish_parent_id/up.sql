alter table "public"."rubbish"
  add constraint "rubbish_parent_id_fkey"
  foreign key ("parent_id")
  references "public"."rubbish"
  ("id") on update restrict on delete restrict;
