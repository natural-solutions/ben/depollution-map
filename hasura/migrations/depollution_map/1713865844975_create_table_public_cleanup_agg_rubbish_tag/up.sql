CREATE TABLE "public"."cleanup_agg_rubbish_tag" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "cleanup_id" uuid NOT NULL, "rubbish_tag_id" text NOT NULL, "sum_quantity" integer, "sum_weight" numeric, "sum_volume" numeric, PRIMARY KEY ("id") , FOREIGN KEY ("cleanup_id") REFERENCES "public"."cleanup"("id") ON UPDATE restrict ON DELETE cascade, FOREIGN KEY ("rubbish_tag_id") REFERENCES "public"."rubbish_tag"("id") ON UPDATE restrict ON DELETE restrict);

insert into "public"."cleanup_agg_rubbish_tag" (cleanup_id, rubbish_tag_id, sum_quantity, sum_weight, sum_volume)
select c.id as cleanup_id, 
rrt.rubbish_tag_id, 
sum(cfr.quantity) as sum_quantity, 
sum(cfr.weight) as sum_weight, 
sum(cfr.volume) as sum_volume
from "public"."cleanup" c 
join "public"."cleanup_form" cf on cf.cleanup_id = c.id 
join "public"."cleanup_form_rubbish" cfr on cfr.cleanup_form_id = cf.id
join "public"."rubbish_rubbish_tag" rrt on rrt.rubbish_id = cfr.rubbish_id
group by c.id, rrt.rubbish_tag_id;