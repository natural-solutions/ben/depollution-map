alter table "public"."cleanup"
  add constraint "cleanup_campaign_id_fkey"
  foreign key (campaign_id)
  references "public"."campaign"
  (id) on update restrict on delete restrict;
alter table "public"."cleanup" alter column "campaign_id" drop not null;
alter table "public"."cleanup" add column "campaign_id" uuid;
