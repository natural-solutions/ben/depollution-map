alter table "public"."cleanup" add column "total_weight" numeric null;

update "public"."cleanup" c set total_weight = (
	select sum(cfr.weight) from "public"."cleanup_form" cf 
	join cleanup_form_rubbish cfr on cfr.cleanup_form_id = cf.id
	where cf.cleanup_id = c.id
);