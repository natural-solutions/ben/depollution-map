alter table "public"."cleanup_photo" drop constraint "cleanup_photo_cleanup_id_fkey",
  add constraint "cleanup_photo_cleanup_id_fkey"
  foreign key ("cleanup_id")
  references "public"."cleanup"
  ("id") on update restrict on delete cascade;
