alter table "public"."campaign" add column "manager_id" uuid;
alter table "public"."campaign" alter column "manager_id" drop not null;
