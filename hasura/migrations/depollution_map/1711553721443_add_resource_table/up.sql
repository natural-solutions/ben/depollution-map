CREATE TABLE public.resource (
	id uuid NOT NULL DEFAULT gen_random_uuid(),
	"label" text,
	filename text,
	description text,
	created_at timestamptz DEFAULT now() NOT NULL,
	updated_at timestamptz DEFAULT now() NOT NULL,
	CONSTRAINT resource_pkey PRIMARY KEY (id)
);
