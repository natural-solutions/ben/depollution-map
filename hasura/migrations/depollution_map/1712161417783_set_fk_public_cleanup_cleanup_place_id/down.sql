alter table "public"."cleanup" drop constraint "cleanup_cleanup_place_id_fkey",
  add constraint "cleanup_cleanup_place_id_fkey"
  foreign key ("cleanup_place_id")
  references "public"."cleanup_place"
  ("id") on update restrict on delete restrict;
