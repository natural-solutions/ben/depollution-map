alter table "public"."cleanup_partner" drop constraint "cleanup_partner_cleanup_id_fkey",
  add constraint "cleanup_partner_cleanup_id_fkey"
  foreign key ("cleanup_id")
  references "public"."cleanup"
  ("id") on update restrict on delete restrict;
