alter table "public"."campaign"
  add constraint "campaign_default_cleanup_type_id_fkey"
  foreign key ("default_cleanup_type_id")
  references "public"."cleanup_type"
  ("id") on update restrict on delete restrict;
