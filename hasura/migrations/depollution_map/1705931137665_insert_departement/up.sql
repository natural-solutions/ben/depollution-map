INSERT INTO public.departement (id,label,region_id) VALUES
	 ('976','Mayotte','mayotte'),
	 ('975','Saint-Pierre-et-Miquelon','collectivites_doutre_mer'),
	 ('977','Saint-Barthélemy','collectivites_doutre_mer'),
	 ('978','Saint-Martin','collectivites_doutre_mer'),
	 ('984','Terres australes et antarctiques françaises','collectivites_doutre_mer'),
	 ('986','Wallis et Futuna','collectivites_doutre_mer'),
	 ('987','Polynésie française','collectivites_doutre_mer'),
	 ('988','Nouvelle-Calédonie','collectivites_doutre_mer'),
	 ('989','Île de Clipperton','collectivites_doutre_mer');
