alter table "public"."cleanup_form"
  add constraint "cleanup_form_rubbish_type_picked_id_fkey"
  foreign key ("rubbish_type_picked_id")
  references "public"."rubbish_type_picked"
  ("id") on update restrict on delete restrict;
