alter table "public"."cleanup_form" drop constraint "cleanup_form_cleanup_id_fkey",
  add constraint "cleanup_form_cleanup_id_fkey"
  foreign key ("cleanup_id")
  references "public"."cleanup"
  ("id") on update restrict on delete restrict;
