TRUNCATE TABLE public.campaign CONTINUE IDENTITY CASCADE;
ALTER TABLE "public"."campaign" ALTER COLUMN "id" TYPE uuid;
alter table "public"."campaign" alter column "id" set default gen_random_uuid();
