DROP VIEW public.cleanup_agg_rubbish_tag_view;
DROP VIEW public.cleanup_agg_rubbish_category_view;

CREATE TABLE cleanup_agg_rubbish_tag (
	id uuid NOT NULL DEFAULT gen_random_uuid(),
	cleanup_id uuid NOT NULL,
	rubbish_tag_id text NOT NULL,
	sum_quantity int4 NULL,
	sum_weight numeric NULL,
	sum_volume numeric NULL,
	CONSTRAINT cleanup_agg_rubbish_tag_pkey PRIMARY KEY (id)
);

ALTER TABLE public.cleanup_agg_rubbish_tag ADD CONSTRAINT cleanup_agg_rubbish_tag_cleanup_id_fkey FOREIGN KEY (cleanup_id) REFERENCES cleanup(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE public.cleanup_agg_rubbish_tag ADD CONSTRAINT cleanup_agg_rubbish_tag_rubbish_tag_id_fkey FOREIGN KEY (rubbish_tag_id) REFERENCES rubbish_tag(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE cleanup_agg_rubbish_category (
	id uuid NOT NULL DEFAULT gen_random_uuid(),
	cleanup_id uuid NOT NULL,
	rubbish_category_id text NOT NULL,
	sum_quantity int4 NULL,
	sum_weight numeric NULL,
	sum_volume numeric NULL,
	ratio_weight numeric NULL,
	CONSTRAINT cleanup_agg_rubbish_category_pkey PRIMARY KEY (id)
);

ALTER TABLE public.cleanup_agg_rubbish_category ADD CONSTRAINT cleanup_agg_rubbish_category_cleanup_id_fkey FOREIGN KEY (cleanup_id) REFERENCES cleanup(id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE public.cleanup_agg_rubbish_category ADD CONSTRAINT cleanup_agg_rubbish_category_rubbish_category_id_fkey FOREIGN KEY (rubbish_category_id) REFERENCES rubbish_category(id) ON DELETE RESTRICT ON UPDATE RESTRICT;