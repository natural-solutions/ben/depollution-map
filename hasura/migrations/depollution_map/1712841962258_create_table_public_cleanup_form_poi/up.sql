CREATE TABLE "public"."cleanup_form_poi" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "cleanup_form_id" uuid NOT NULL, "cleanup_area_id" uuid NOT NULL, "poi_type_id" text NOT NULL, "nb" integer NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("cleanup_form_id") REFERENCES "public"."cleanup_form"("id") ON UPDATE restrict ON DELETE cascade, FOREIGN KEY ("cleanup_area_id") REFERENCES "public"."cleanup_area"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("poi_type_id") REFERENCES "public"."poi_type"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("cleanup_form_id", "cleanup_area_id", "poi_type_id"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_cleanup_form_poi_updated_at"
BEFORE UPDATE ON "public"."cleanup_form_poi"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_cleanup_form_poi_updated_at" ON "public"."cleanup_form_poi"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
