CREATE TABLE public.city (
	id int NOT NULL,
	departement_id varchar(3) NULL,
	label text NULL,
	coords public.geometry(point, 4326) NULL,
	CONSTRAINT city_pk PRIMARY KEY (id),
	CONSTRAINT city_departement_fk FOREIGN KEY (departement_id) REFERENCES public.departement(id)
);
