alter table "public"."cleanup_type_rubbish" drop constraint "cleanup_type_rubbish_rubbish_id_fkey",
  add constraint "cleanup_type_rubbish_rubbish_id_fkey"
  foreign key ("rubbish_id")
  references "public"."rubbish"
  ("label") on update restrict on delete restrict;
