-- public.environment_area_type definition
-- Drop table
-- DROP TABLE public.environment_area_type;
CREATE TABLE
	public.environment_area_type (
		area_type_id text NOT NULL,
		environment_type_id text NOT NULL,
		CONSTRAINT environment_area_type_pkey PRIMARY KEY (area_type_id, environment_type_id)
	);

-- public.environment_area_type foreign keys
ALTER TABLE public.environment_area_type ADD CONSTRAINT area_type_fkey FOREIGN KEY (area_type_id) REFERENCES public.area_type (id);

ALTER TABLE public.environment_area_type ADD CONSTRAINT environment_type_fkey FOREIGN KEY (environment_type_id) REFERENCES public.environment_type (id);

-- Insert
INSERT INTO
	public.environment_area_type (area_type_id, environment_type_id)
VALUES
	('natural_bank', 'watercourse'),
	('watercourse_bed', 'watercourse'),
	('urban_park', 'watercourse'),
	('beach', 'watercourse'),
	('port_dam_lock', 'watercourse'),
	('quay_dykes_structures', 'watercourse'),
	('natural_bank', 'lake_marsh'),
	('beach', 'lake_marsh'),
	('water_body', 'lake_marsh'),
	('port_dam', 'lake_marsh');

INSERT INTO
	public.environment_area_type (area_type_id, environment_type_id)
VALUES
	('quay_dykes_structures', 'lake_marsh'),
	('canals_salt_pans', 'lagoon_pond'),
	('dike_structure', 'lagoon_pond'),
	('natural_area_beach_back', 'lagoon_pond'),
	('urban_park', 'lagoon_pond'),
	('parking', 'lagoon_pond'),
	('beach', 'lagoon_pond'),
	('water_body', 'lagoon_pond'),
	('port', 'lagoon_pond'),
	('rock_creek', 'lagoon_pond');

INSERT INTO
	public.environment_area_type (area_type_id, environment_type_id)
VALUES
	('road', 'lagoon_pond'),
	('trail_path', 'lagoon_pond'),
	('dike_structure', 'coastal_terrestrial'),
	('dune', 'coastal_terrestrial'),
	('natural_area_beach_back', 'coastal_terrestrial'),
	('mangrove', 'coastal_terrestrial'),
	('urban_park', 'coastal_terrestrial'),
	('parking', 'coastal_terrestrial'),
	('beach', 'coastal_terrestrial'),
	('port', 'coastal_terrestrial');

INSERT INTO
	public.environment_area_type (area_type_id, environment_type_id)
VALUES
	('rock_creek', 'coastal_terrestrial'),
	('road', 'coastal_terrestrial'),
	('trail_path', 'coastal_terrestrial'),
	('sea_ocean', 'sea_ocean'),
	('other_natural_area', 'mountain'),
	('meadow', 'mountain'),
	('forest', 'mountain'),
	('parking', 'mountain'),
	('ski_track', 'mountain'),
	('road', 'mountain');

INSERT INTO
	public.environment_area_type (area_type_id, environment_type_id)
VALUES
	('trail_path', 'mountain'),
	('ski_resort', 'mountain'),
	('other_natural_area', 'natural_rural_area'),
	(
		'field_meadow_landegarrigue_maquis',
		'natural_rural_area'
	),
	('forest', 'natural_rural_area'),
	('parking', 'natural_rural_area'),
	('road', 'natural_rural_area'),
	('trail_path', 'natural_rural_area'),
	('railway_track', 'natural_rural_area'),
	('urban_park', 'urban_area');

INSERT INTO
	public.environment_area_type (area_type_id, environment_type_id)
VALUES
	('parking', 'urban_area'),
	('road_street_square', 'urban_area'),
	('railway_track', 'urban_area');
