CREATE TABLE "public"."characterization_level" ("id" integer NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "label" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("label"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_characterization_level_updated_at"
BEFORE UPDATE ON "public"."characterization_level"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_characterization_level_updated_at" ON "public"."characterization_level"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';

INSERT INTO public.characterization_level (id,"label") VALUES
  (1,'Niveau 1'),
  (2,'Niveau 2'),
  (3,'Niveau 3'),
  (4,'Niveau 4');