alter table "public"."cleanup_campaign"
  add constraint "cleanup_campaign_campaign_id_fkey"
  foreign key ("campaign_id")
  references "public"."campaign"
  ("id") on update restrict on delete restrict;
