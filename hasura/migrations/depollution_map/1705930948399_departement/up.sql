CREATE TABLE public.departement (
	id varchar(3) NULL,
	label varchar(255) NULL,
	region_id varchar(255) NULL,
	CONSTRAINT departement_pk PRIMARY KEY (id),
	CONSTRAINT departement_region_pk FOREIGN KEY (region_id) REFERENCES region(slug)
);
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('01','Ain','auvergne_rhone_alpes'),
	 ('02','Aisne','hauts_de_france'),
	 ('03','Allier','auvergne_rhone_alpes'),
	 ('04','Alpes-de-Haute-Provence','provence_alpes_cote_dazur'),
	 ('05','Hautes-Alpes','provence_alpes_cote_dazur'),
	 ('06','Alpes-Maritimes','provence_alpes_cote_dazur'),
	 ('07','Ardèche','auvergne_rhone_alpes'),
	 ('08','Ardennes','grand_est'),
	 ('09','Ariège','occitanie'),
	 ('10','Aube','grand_est');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('11','Aude','occitanie'),
	 ('12','Aveyron','occitanie'),
	 ('13','Bouches-du-Rhône','provence_alpes_cote_dazur'),
	 ('14','Calvados','normandie'),
	 ('15','Cantal','auvergne_rhone_alpes'),
	 ('16','Charente','nouvelle_aquitaine'),
	 ('17','Charente-Maritime','nouvelle_aquitaine'),
	 ('18','Cher','centre_val_de_loire'),
	 ('19','Corrèze','nouvelle_aquitaine'),
	 ('21','Côte-d Or','bourgogne_franche_comte');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('22','Côtes-d Armor','bretagne'),
	 ('23','Creuse','nouvelle_aquitaine'),
	 ('24','Dordogne','nouvelle_aquitaine'),
	 ('25','Doubs','bourgogne_franche_comte'),
	 ('26','Drôme','auvergne_rhone_alpes'),
	 ('27','Eure','normandie'),
	 ('28','Eure-et-Loir','centre_val_de_loire'),
	 ('29','Finistère','bretagne'),
	 ('2A','Corse-du-Sud','corse'),
	 ('2B','Haute-Corse','corse');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('30','Gard','occitanie'),
	 ('31','Haute-Garonne','occitanie'),
	 ('32','Gers','occitanie'),
	 ('33','Gironde','nouvelle_aquitaine'),
	 ('34','Hérault','occitanie'),
	 ('35','Ille-et-Vilaine','bretagne'),
	 ('36','Indre','centre_val_de_loire'),
	 ('37','Indre-et-Loire','centre_val_de_loire'),
	 ('38','Isère','auvergne_rhone_alpes'),
	 ('39','Jura','bourgogne_franche_comte');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('40','Landes','nouvelle_aquitaine'),
	 ('41','Loir-et-Cher','centre_val_de_loire'),
	 ('42','Loire','auvergne_rhone_alpes'),
	 ('43','Haute-Loire','auvergne_rhone_alpes'),
	 ('44','Loire-Atlantique','pays_de_la_loire'),
	 ('45','Loiret','centre_val_de_loire'),
	 ('46','Lot','occitanie'),
	 ('47','Lot-et-Garonne','nouvelle_aquitaine'),
	 ('48','Lozère','occitanie'),
	 ('49','Maine-et-Loire','pays_de_la_loire');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('50','Manche','normandie'),
	 ('51','Marne','grand_est'),
	 ('52','Haute-Marne','grand_est'),
	 ('53','Mayenne','pays_de_la_loire'),
	 ('54','Meurthe-et-Moselle','grand_est'),
	 ('55','Meuse','grand_est'),
	 ('56','Morbihan','bretagne'),
	 ('57','Moselle','grand_est'),
	 ('58','Nièvre','bourgogne_franche_comte'),
	 ('59','Nord','hauts_de_france');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('60','Oise','hauts_de_france'),
	 ('61','Orne','normandie'),
	 ('62','Pas-de-Calais','hauts_de_france'),
	 ('63','Puy-de-Dôme','auvergne_rhone_alpes'),
	 ('64','Pyrénées-Atlantiques','nouvelle_aquitaine'),
	 ('65','Hautes-Pyrénées','occitanie'),
	 ('66','Pyrénées-Orientales','occitanie'),
	 ('67','Bas-Rhin','grand_est'),
	 ('68','Haut-Rhin','grand_est'),
	 ('69','Rhône','auvergne_rhone_alpes');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('70','Haute-Saône','bourgogne_franche_comte'),
	 ('71','Saône-et-Loire','bourgogne_franche_comte'),
	 ('72','Sarthe','pays_de_la_loire'),
	 ('73','Savoie','auvergne_rhone_alpes'),
	 ('74','Haute-Savoie','auvergne_rhone_alpes'),
	 ('75','Paris','ile_de_france'),
	 ('76','Seine-Maritime','normandie'),
	 ('77','Seine-et-Marne','ile_de_france'),
	 ('78','Yvelines','ile_de_france'),
	 ('79','Deux-Sèvres','nouvelle_aquitaine');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('80','Somme','hauts_de_france'),
	 ('81','Tarn','occitanie'),
	 ('82','Tarn-et-Garonne','occitanie'),
	 ('83','Var','provence_alpes_cote_dazur'),
	 ('84','Vaucluse','provence_alpes_cote_dazur'),
	 ('85','Vendée','pays_de_la_loire'),
	 ('86','Vienne','nouvelle_aquitaine'),
	 ('87','Haute-Vienne','nouvelle_aquitaine'),
	 ('88','Vosges','grand_est'),
	 ('89','Yonne','bourgogne_franche_comte');
INSERT INTO public.departement (id,label,region_id) VALUES
	 ('90','Territoire de Belfort','bourgogne_franche_comte'),
	 ('91','Essonne','ile_de_france'),
	 ('92','Hauts-de-Seine','ile_de_france'),
	 ('93','Seine-Saint-Denis','ile_de_france'),
	 ('94','Val-de-Marne','ile_de_france'),
	 ('95','Val-d Oise','ile_de_france'),
	 ('971','Guadeloupe','guadeloupe'),
	 ('972','Martinique','martinique'),
	 ('973','Guyane','guyane'),
	 ('974','La Réunion','la_reunion');
