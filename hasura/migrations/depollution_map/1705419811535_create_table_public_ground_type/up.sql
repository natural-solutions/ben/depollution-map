CREATE TABLE "public"."ground_type" ("id" text NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "label" text, PRIMARY KEY ("id") , UNIQUE ("label"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_ground_type_updated_at"
BEFORE UPDATE ON "public"."ground_type"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_ground_type_updated_at" ON "public"."ground_type"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
