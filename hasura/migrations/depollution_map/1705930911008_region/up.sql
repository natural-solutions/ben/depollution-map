-- public.region definition
-- Drop table
-- DROP TABLE public.region;

CREATE TABLE
	public.region (
		code varchar(3) NOT NULL,
		label varchar(255) NULL,
		slug varchar(255) NOT NULL,
		CONSTRAINT region_pk PRIMARY KEY (slug)
	);

-- Insert

INSERT INTO
	public.region (code, label, slug)
VALUES
	('01', 'Guadeloupe', 'guadeloupe'),
	('02', 'Martinique', 'martinique'),
	('03', 'Guyane', 'guyane'),
	('04', 'La Réunion', 'la_reunion'),
	('06', 'Mayotte', 'mayotte'),
	('11', 'Île-de-France', 'ile_de_france'),
	(
		'24',
		'Centre-Val de Loire',
		'centre_val_de_loire'
	),
	(
		'27',
		'Bourgogne-Franche-Comté',
		'bourgogne_franche_comte'
	),
	('28', 'Normandie', 'normandie'),
	('32', 'Hauts-de-France', 'hauts_de_france');

INSERT INTO
	public.region (code, label, slug)
VALUES
	('44', 'Grand Est', 'grand_est'),
	('52', 'Pays de la Loire', 'pays_de_la_loire'),
	('53', 'Bretagne', 'bretagne'),
	('75', 'Nouvelle-Aquitaine', 'nouvelle_aquitaine'),
	('76', 'Occitanie', 'occitanie'),
	(
		'84',
		'Auvergne-Rhône-Alpes',
		'auvergne_rhone_alpes'
	),
	(
		'93',
		'Provence-Alpes-Côte d Azur',
		'provence_alpes_cote_dazur'
	),
	('94', 'Corse', 'corse'),
	(
		'COM',
		'Collectivités d Outre-Mer',
		'collectivites_doutre_mer'
	);
