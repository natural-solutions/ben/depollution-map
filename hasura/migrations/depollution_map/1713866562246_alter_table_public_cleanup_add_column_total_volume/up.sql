alter table "public"."cleanup" add column "total_volume" numeric null;

update "public"."cleanup" c set total_volume = (
	select sum(cfr.volume) from "public"."cleanup_form" cf 
	join cleanup_form_rubbish cfr on cfr.cleanup_form_id = cf.id
	where cf.cleanup_id = c.id
);