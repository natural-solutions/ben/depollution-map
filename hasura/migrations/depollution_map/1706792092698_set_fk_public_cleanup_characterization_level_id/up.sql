alter table "public"."cleanup"
  add constraint "cleanup_characterization_level_id_fkey"
  foreign key ("characterization_level_id")
  references "public"."characterization_level"
  ("id") on update restrict on delete restrict;
