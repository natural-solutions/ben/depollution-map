alter table "public"."cleanup" add column "total_participants" integer null;

update "public"."cleanup" c set total_participants = (
	select sum(cf.volunteers_number) + sum(cf.external_volunteers_number) 
	from "public"."cleanup_form" cf where cf.cleanup_id = c.id 
);