CREATE TABLE "public"."rubbish_type_picked"
(
    id varchar(255) not null
        constraint rubbish_type_picked_pkey
            primary key,
    id_merterre uuid not null,
    label       text
);
