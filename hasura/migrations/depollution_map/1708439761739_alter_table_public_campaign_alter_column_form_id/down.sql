alter table "public"."campaign" rename column "default_cleanup_type_id" to "form_id";
ALTER TABLE "public"."campaign" ALTER COLUMN "form_id" TYPE uuid;
