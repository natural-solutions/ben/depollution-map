alter table "public"."rubbish_rubbish_tag"
  add constraint "rubbish_rubbish_tag_rubbish_id_fkey"
  foreign key ("rubbish_id")
  references "public"."rubbish"
  ("id") on update restrict on delete restrict;
