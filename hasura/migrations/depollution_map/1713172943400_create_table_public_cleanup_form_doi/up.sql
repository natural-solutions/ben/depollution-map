CREATE TABLE "public"."cleanup_form_doi" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "cleanup_form_id" uuid NOT NULL, "rubbish_id" text NOT NULL, "expiration" date NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("cleanup_form_id") REFERENCES "public"."cleanup_form"("id") ON UPDATE restrict ON DELETE cascade, FOREIGN KEY ("rubbish_id") REFERENCES "public"."rubbish"("id") ON UPDATE restrict ON DELETE restrict);
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_cleanup_form_doi_updated_at"
BEFORE UPDATE ON "public"."cleanup_form_doi"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_cleanup_form_doi_updated_at" ON "public"."cleanup_form_doi"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
