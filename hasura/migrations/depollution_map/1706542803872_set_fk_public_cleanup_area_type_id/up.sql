alter table "public"."cleanup"
  add constraint "cleanup_area_type_id_fkey"
  foreign key ("area_type_id")
  references "public"."area_type"
  ("id") on update restrict on delete restrict;
