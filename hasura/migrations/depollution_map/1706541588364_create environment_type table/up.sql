CREATE TABLE
	public.environment_type (
		id text NOT NULL,
		label text NOT NULL,
		CONSTRAINT environment_type_label_key UNIQUE (label),
		CONSTRAINT environment_type_pkey PRIMARY KEY (id)
	);

INSERT INTO
	public.environment_type (id, label)
VALUES
	('watercourse', 'Cours d"eau'),
	('lake_marsh', 'Lac et marais'),
	('lagoon_pond', 'Lagune et étang côtier'),
	('coastal_terrestrial', 'Littoral (terrestre)'),
	('sea_ocean', 'Mer-océans'),
	('mountain', 'Montagne'),
	(
		'natural_rural_area',
		'Zone naturelle ou rurale (hors littoral et montagne)'
	),
	('urban_area', 'Zone urbaine');
