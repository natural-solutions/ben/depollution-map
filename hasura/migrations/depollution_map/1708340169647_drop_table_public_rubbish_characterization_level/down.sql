CREATE TABLE rubbish_characterization_level (
	rubbish_id text NOT NULL,
	characterization_level_id int4 NOT NULL,
	has_qty bool NULL,
	has_wt_vol bool NULL,
	CONSTRAINT rubbish_charaterization_level_pkey PRIMARY KEY (rubbish_id,characterization_level_id)
);


-- public.rubbish_characterization_level foreign keys

ALTER TABLE public.rubbish_characterization_level ADD CONSTRAINT rubbish_charaterization_level_charaterization_level_id_fkey FOREIGN KEY (characterization_level_id) REFERENCES characterization_level(id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE public.rubbish_characterization_level ADD CONSTRAINT rubbish_charaterization_level_rubbish_id_fkey FOREIGN KEY (rubbish_id) REFERENCES rubbish(id) ON DELETE RESTRICT ON UPDATE RESTRICT;