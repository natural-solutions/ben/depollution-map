alter table "public"."cleanup_photo" drop constraint "cleanup_photo_filename_key";
alter table "public"."cleanup_photo" add constraint "cleanup_photo_filename_cleanup_id_key" unique ("filename", "cleanup_id");
