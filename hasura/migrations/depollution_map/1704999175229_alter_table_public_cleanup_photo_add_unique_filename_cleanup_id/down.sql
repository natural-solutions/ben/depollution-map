alter table "public"."cleanup_photo" drop constraint "cleanup_photo_filename_cleanup_id_key";
alter table "public"."cleanup_photo" add constraint "cleanup_photo_filename_key" unique ("filename");
