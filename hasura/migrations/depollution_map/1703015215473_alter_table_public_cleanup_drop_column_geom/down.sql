alter table "public"."cleanup" add column "geom" jsonb;
alter table "public"."cleanup" alter column "geom" drop not null;
