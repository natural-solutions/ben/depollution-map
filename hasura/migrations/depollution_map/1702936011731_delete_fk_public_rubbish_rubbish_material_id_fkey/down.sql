alter table "public"."rubbish"
  add constraint "rubbish_material_id_fkey"
  foreign key ("material_id")
  references "public"."material"
  ("id") on update restrict on delete restrict;
