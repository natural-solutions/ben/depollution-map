alter table "public"."cleanup"
  add constraint "cleanup_cleanup_type_id_fkey"
  foreign key ("cleanup_type_id")
  references "public"."cleanup_type"
  ("id") on update restrict on delete restrict;
