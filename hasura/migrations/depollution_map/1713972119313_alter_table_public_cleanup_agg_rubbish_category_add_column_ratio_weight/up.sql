alter table "public"."cleanup_agg_rubbish_category" add column "ratio_weight" numeric null;

update cleanup_agg_rubbish_category carc set ratio_weight = round(carc.sum_weight / (
	select c.total_weight from cleanup c where c.id = carc.cleanup_id
), 2);