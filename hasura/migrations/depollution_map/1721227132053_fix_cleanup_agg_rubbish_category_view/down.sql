DROP VIEW public.cleanup_agg_rubbish_category_view;
CREATE OR REPLACE VIEW public.cleanup_agg_rubbish_category_view
AS WITH cleanup_totals AS (
         SELECT cf.cleanup_id,
            sum(cfr.weight) AS total_weight
           FROM cleanup_form cf
             JOIN cleanup_form_rubbish cfr ON cfr.cleanup_form_id = cf.id
          GROUP BY cf.cleanup_id
        ), category_totals AS (
         SELECT c.id AS cleanup_id,
            r.rubbish_category_id,
            sum(cfr.quantity) AS sum_quantity,
            sum(cfr.weight) AS sum_weight,
            sum(cfr.volume) AS sum_volume
           FROM cleanup c
             JOIN cleanup_form cf ON cf.cleanup_id = c.id
             JOIN cleanup_form_rubbish cfr ON cfr.cleanup_form_id = cf.id
             JOIN rubbish r ON r.id = cfr.rubbish_id
          GROUP BY c.id, r.rubbish_category_id
        )
 SELECT ct.cleanup_id,
    ct.rubbish_category_id,
    ct.sum_quantity,
    ct.sum_weight,
    ct.sum_volume,
    ct.sum_weight / ct2.total_weight AS ratio_weight
   FROM category_totals ct
     JOIN cleanup_totals ct2 ON ct.cleanup_id = ct2.cleanup_id;