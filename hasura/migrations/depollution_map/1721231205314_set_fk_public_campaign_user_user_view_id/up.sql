alter table "public"."campaign_user"
  add constraint "campaign_user_user_view_id_fkey"
  foreign key ("user_view_id")
  references "keycloak"."user_entity"
  ("id") on update restrict on delete cascade;

alter table "public"."cleanup"
  add constraint "cleanup_owner_id_fkey"
  foreign key ("owner_id")
  references "keycloak"."user_entity"
  ("id") on update restrict on delete set null;

alter table "public"."cleanup_form"
  add constraint "cleanup_form_owner_id_fkey"
  foreign key ("owner_id")
  references "keycloak"."user_entity"
  ("id") on update restrict on delete set null;