CREATE OR replace view campaign_rubbish_metrics_view as
    SELECT c.id AS campaign_id,
        r.rubbish_category_id as category_id,
        sum(cfr.quantity) AS sum_qty,
        sum(cfr.weight) AS sum_weight,
        sum(cfr.volume) AS sum_volume
   FROM campaign c
        LEFT JOIN cleanup_campaign cc ON cc.campaign_id = c.id
        LEFT JOIN cleanup c2 ON c2.id = cc.cleanup_id
        LEFT JOIN cleanup_form cf ON cf.cleanup_id = c2.id
        LEFT JOIN cleanup_form_rubbish cfr ON cfr.cleanup_form_id = cf.id
        left join rubbish r on r.id = cfr.rubbish_id
    WHERE r.rubbish_category_id in ('plastic', 'metal', 'glass', 'fabric', 'paper_board', 'wood')
    GROUP BY c.id, r.rubbish_category_id;
