CREATE OR REPLACE VIEW public.cleanup_agg_rubbish_view 
AS SELECT cf.cleanup_id, cfr.rubbish_id , SUM(cfr.quantity) AS sum_quantity
    FROM cleanup_form cf
    JOIN cleanup_form_rubbish cfr ON cf.id = cfr.cleanup_form_id
    GROUP BY cf.cleanup_id, cfr.rubbish_id;
