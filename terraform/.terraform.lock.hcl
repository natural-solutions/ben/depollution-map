# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/aminueza/minio" {
  version     = "1.8.0"
  constraints = "1.8.0"
  hashes = [
    "h1:yiZmm80beCiou62KdAzLhsOkh6eN/58kKxDIgCBRC7s=",
    "zh:05ca43fe23bc4157bf451ef89e675901f3e865e4ee9f5a5022b509c035ac790c",
    "zh:0a62f4b51262db8c5a4d306f47ad5b72c07d5ff7fead6fd428d9440c5ea650e8",
    "zh:19424b4891cf121347a4836f178c952b4f1606c6218a44665ed9ae6544d43c47",
    "zh:3ef56b11a664ee4a60ae6799cff1836561ac5839ae1bc6e351d7b6ce6856282f",
    "zh:6b649cab9e3976b982cd8d257dd95eaddc126c28ca714367637d8ef255adad12",
    "zh:84651a5696c45c93264f05fb08152f38c5b513fde7b55b90ad7a231f567ac3a9",
    "zh:aa8aaa9f9c55c968180215bf06765507496c8b5edc4d50883d7f82a63e4ac4c2",
    "zh:b5b60f6ffc50fc14855574fff8436e21049920a4093e5cc346db818c88063b0f",
    "zh:b5e321d7e9ca37607c78fe378fd51cf9d9481c4684bd55c577ada3ec6bfc09d3",
    "zh:bdff198b55adca280f46a135affe30eb94a3693ab1bcd3798b1a610909baf149",
    "zh:cb3af1fcd1b245d09fd81c57ec055ea925ca336b5d14a745bb05f86b543c422f",
    "zh:cd3bd0f2c797eaf3d2dd9157363c373a12f326b0f6b10a05173cf300fc4cafc9",
  ]
}
