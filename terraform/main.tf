terraform {
  required_providers {
    minio = {
      source = "aminueza/minio"
      version = "1.8.0"
    }
  }
}

resource "minio_s3_bucket" "upload" {
  bucket = "upload"
}

resource "minio_s3_bucket" "storage" {
  bucket = "storage"
}

resource "minio_s3_bucket" "cache" {
  bucket = "cache"
}

resource "minio_s3_bucket_policy" "upload" {
  bucket = minio_s3_bucket.upload.bucket
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": ["*"]
      },
      "Action": ["s3:GetBucketLocation", "s3:ListBucket"],
      "Resource": ["arn:aws:s3:::upload"]
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": ["*"]
      },
      "Action": ["s3:GetObject"],
      "Resource": ["arn:aws:s3:::upload/*"]
    }
  ]
}
EOF
}