const fs = require("fs");

/* if (process.argv.length !== 4) {
  console.error("Expected at least four argument !");
  process.exit(1);
} */

const date = Date.now();
const rubbishCategorySeedName = `../hasura/seeds/depollution_map/${date}_rubbish_category.sql`;
const rubbishSeedName = `../hasura/seeds/depollution_map/${date}_rubbish.sql`;

function writeInFile(fileName, data) {
  fs.appendFileSync(fileName, data, (err, _) => {
    if (err) {
      console.error(`ERR writing in file : ${err.message}`);
    }
  });
}

try {
  //const file = fs.readFileSync(process.argv[2], "utf-8");
  const file = fs.readFileSync("../docs/rubbish_categories.csv", "utf-8");
  const lines = file.split("\n");

  for (const line of lines) {
    let [id, label] = line.split(",");

    if (id && id !== "id") {
      label = label
        .replaceAll("…", "...")
        .replaceAll(/'|’/g, "''")
        .replaceAll('"', "");

      writeInFile(
        rubbishCategorySeedName,
        `insert into rubbish_category (id, label) values ('${id}', '${label}');\n`
      );
    }
  }
} catch (error) {
  console.error(`ERR open file : ${error.message}`);
  process.exit(1);
}

try {
  //const file = fs.readFileSync(process.argv[3], "utf-8");
  const file = fs.readFileSync("../docs/rubbishes.csv", "utf-8");
  const lines = file.split("\n");
  const regex = /(?:^|,)(?=(?:(?:[^"]*"){2})*[^"]*$)/;

  for (const line of lines) {
    let [
      id,
      label,
      rubbish_category_id,
      is_brand,
      is_doi,
      qty_level_1,
      qty_level_2,
      qty_level_3,
      qty_butts,
      wt_vol_level_1,
      wt_vol_level_2,
      wt_vol_level_3,
      wt_vol_butts,
    ] = line.split(regex);

    const is_support_brand = is_brand === "x";
    const is_support_doi = is_doi === "x";

    if (id && id !== "id") {
      label = label
        .replaceAll("…", "...")
        .replaceAll(/'|’/g, "''")
        .replaceAll('"', "");

      writeInFile(
        rubbishSeedName,
        `insert into rubbish (id, label, rubbish_category_id, is_support_brand, is_support_doi) ` +
          `values ('${id}', '${label}', '${rubbish_category_id}', ${is_support_brand}, ${is_support_doi});\n`
      );

      const quantities = [qty_level_1, qty_level_2, qty_level_3, qty_butts];
      const wt_vol_ = [
        wt_vol_level_1,
        wt_vol_level_2,
        wt_vol_level_3,
        wt_vol_butts,
      ];
      const categoryNames = ["level_1", "level_2", "level_3", "butts"];

      for (let index = 0; index < 4; index += 1) {
        const has_qty = quantities[index] ? true : null;
        const has_wt_vol = wt_vol_[index] ? true : null;

        writeInFile(
          rubbishSeedName,
          `insert into cleanup_type_rubbish ` +
            `(cleanup_type_id, rubbish_id, has_qty, has_wt_vol) ` +
            `values ('${categoryNames[index]}', '${id}', ${has_qty}, ${has_wt_vol});\n`
        );
      }
    }
  }
} catch (error) {
  console.error(`ERR open file : ${error.message}`);
  process.exit(1);
}
