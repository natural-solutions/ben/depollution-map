#!/usr/bin/env bash

set -e

source ./docker/.env

./scripts/clean.sh

npm install
(cd app && npm install)

./scripts/docker.sh up --build -d
./scripts/terraform.sh

sudo ./scripts/hostctl add domains "${PROJECT}" "${DOMAIN}"