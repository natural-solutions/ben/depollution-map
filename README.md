## User Perms

### If the app need a new user role:

- Add this role in Keycloak
- In /app/src/utils/user.ts
  - Declare it in the enum
  - Put it into the array, in "herarchical" order
- In Hasura
  - add an inherited role just like the existing public role  
    (In order to give this new role at least the same perms as a public user)
  - Configure perms for each tables

## HOW TO ?

### Launch the script rubbish_insert.js

This script generate seeds for insert rubbishes & Co. from 2 CSV present in /docs
`cd ./scripts && node ./rubbish_insert.js`

### Apply a seed on prod 
`./scripts/docker.sh exec hasura bash -c "hasura-cli  seed apply --endpoint http://localhost:8080 -f 1708100404747_rubbish.sql"`
